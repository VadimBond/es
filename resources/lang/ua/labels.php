<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all'     => 'Всі',
        'yes'     => 'Так',
        'no'      => 'Ні',
        'custom'  => 'Користувальницький',
        'actions' => 'Дії',
        'active'  => 'Активний',
        'buttons' => [
            'save'   => 'Зберегти',
            'update' => 'Оновити',
        ],
        'hide'              => 'Сховати',
        'inactive'          => 'Неактивний',
        'none'              => 'Відсутнє',
        'show'              => 'Показати',
        'toggle_navigation' => 'Переключити навігацію',
    ],

    'backend' => [
        'profile_updated' => 'Ваш профіль був оновлений.',
        'access'          => [
            'roles' => [
                'create'     => 'Створити роль',
                'edit'       => 'Редагувати роль',
                'management' => 'Управління ролями',

                'table' => [
                    'number_of_users' => 'Кількість користувачів',
                    'permissions'     => 'Дозволи',
                    'role'            => 'Роль',
                    'sort'            => 'Сортувати',
                    'total'           => 'Всього ролей',
                ],
            ],

            'permissions' => [
                'create'     => 'Створити дозвіл',
                'edit'       => 'Редагувати дозволи',
                'management' => 'Управління правами',

                'table' => [
                    'permission'   => 'Дозвіл',
                    'display_name' => 'Відображувана назва',
                    'sort'         => 'Сортувати',
                    'status'       => 'Статус',
                    'total'        => 'role total|roles total',
                ],
            ],

            'users' => [
                'active'              => 'Активні користувачі',
                'all_permissions'     => 'Усі права доступу',
                'change_password'     => 'Змінити пароль',
                'change_password_for' => 'Змінити пароль для :user',
                'create'              => 'Створити користувача',
                'deactivated'         => 'Деактивовані користувачі',
                'deleted'             => 'Видалені користувачі',
                'edit'                => 'Редагувати користувача',
                'edit-profile'        => 'Редагувати профіль',
                'management'          => 'Управління користувачами',
                'no_permissions'      => 'Немає прав доступу',
                'no_roles'            => 'Немає ролей для встановлення.',
                'permissions'         => 'Дозволи',

                'table' => [
                    'confirmed'      => 'Підтверджений',
                    'created'        => 'Створений',
                    'email'          => 'E-mail',
                    'id'             => 'ID',
                    'last_updated'   => 'Останнє оновлення',
                    'first_name'     => 'Ім`я',
                    'last_name'      => 'Прізвище',
                    'no_deactivated' => 'Нема деактивованих користувачів',
                    'no_deleted'     => 'Нема видалених користувачів',
                    'roles'          => 'Ролі',
                    'total'          => 'user total|users total',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Огляд',
                        'history'  => 'Історія',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar'       => 'Аватар',
                            'confirmed'    => 'Підтверджений',
                            'created_at'   => 'Створено ',
                            'deleted_at'   => 'Видалено ',
                            'email'        => 'E-mail',
                            'last_updated' => 'Останнє оновлення',
                            'name'         => 'Ім`я',
                            'status'       => 'Статус',
                        ],
                    ],
                ],

                'view' => 'Переглянути користувача',
            ],
        ],

        'pages' => [
            'create'     => 'Створити сторінку',
            'edit'       => 'Редагувати сторінку',
            'management' => 'Управління сторінками',
            'title'      => 'Сторінки',

            'table' => [
                'title'     => 'Назва',
                'status'    => 'Статус',
                'createdat' => 'Створений ',
                'updatedat' => 'Оновлений ',
                'createdby' => 'Створений ',
                'all'       => 'Всі',
            ],
        ],

        'blogcategories' => [
            'create'     => 'Створити категорію блогів',
            'edit'       => 'Редагувати категорію блогу',
            'management' => 'Управління категорією блогів',
            'title'      => 'Категорія блогів',

            'table' => [
                'title'     => 'Категорія блогів',
                'status'    => 'Статус',
                'createdat' => 'Створено ',
                'createdby' => 'Створений ',
                'all'       => 'Всі',
            ],
        ],

        'blogtags' => [
            'create'     => 'Створити тег блогів',
            'edit'       => 'Редагувати тег блогів',
            'management' => 'Управління тегами блогів',
            'title'      => 'Теги блогів',

            'table' => [
                'title'     => 'Тег блогів',
                'status'    => 'Статус',
                'createdat' => 'Створено ',
                'createdby' => 'Створений ',
                'all'       => 'Всі',
            ],
        ],

        'blogs' => [
            'create'     => 'Створити блог',
            'edit'       => 'Редагувати блог',
            'management' => 'Блог Управління',
            'title'      => 'Блоги',

            'table' => [
                'title'     => 'Блог',
                'publish'   => 'PublishDateTime',
                'status'    => 'Статус',
                'createdat' => 'Створено ',
                'createdby' => 'Створений ',
                'all'       => 'All',
            ],
        ],

        'emailtemplates' => [
            'create'     => 'Створити шаблон електронної пошти',
            'edit'       => 'Редагувати шаблон електронної пошти',
            'management' => 'Управління шаблонами електронної пошти',
            'title'      => 'Шаблони електронної пошти',

            'table' => [
                'title'     => 'Назва',
                'subject'   => 'Тема',
                'status'    => 'Статус',
                'createdat' => 'Створено ',
                'updatedat' => 'Оновлено',
                'all'       => 'Всі',
            ],
        ],

        'settings' => [
            'edit'           => 'Змінити налаштування',
            'management'     => 'Налаштування управління',
            'title'          => 'Налаштування',
            'seo'            => 'Налаштування SEO',
            'companydetails' => 'Контактна інформація компанії',
            'mail'           => 'Параметри пошти',
            'footer'         => 'Параметри нижнього колонтитула',
            'terms'          => 'Умови налаштування',
            'google'         => 'Google Analytics код для відстежування',
        ],

        'faqs' => [
            'create'     => 'Створити довідку',
            'edit'       => 'Редагувати довідку',
            'management' => 'Управління довідкою',
            'title'      => 'Довідка',

            'table' => [
                'title'     => 'Довідка',
                'publish'   => 'Дата публікації',
                'status'    => 'Статус',
                'createdat' => 'Дата створення',
                'createdby' => 'Створений',
                'answers'    => 'Варіанти відповіді',
                'questions'  => 'Питання',
                'updatedat' => 'Дата оновлення',
                'all'       => 'Всі',
            ],
        ],

        'menus' => [
            'create'     => 'Створити меню',
            'edit'       => 'Редагувати меню',
            'management' => 'Управління меню',
            'title'      => 'Меню',

            'table' => [
                'name'      => 'Ім`я',
                'type'      => 'Тип',
                'createdat' => 'Створено ',
                'createdby' => 'Створений ',
                'all'       => 'Всі',
            ],
            'field' => [
                'name'      => 'Ім`я',
                'type'      => 'Тип',
                'items'     => 'Пункти меню',
                'url'       => 'URL',
                'url_type'  => 'Тип URL',
                'url_types' => [
                  'route'  => 'Маршрут',
                  'static' => 'Статичний',
                ],
                'open_in_new_tab'    => 'Відкрити посилання в новій вкладці',
                'view_permission_id' => 'Дозвіл',
                'icon'               => 'Icon Class',
                'icon_title'         => 'Font Awesome Class. eg. fa-edit',
            ],
        ],

        'modules' => [
            'create'     => 'Створити модуль',
            'management' => 'Управління модулем',
            'title'      => 'Модуль',
            'edit'       => 'Редагування модулю',

            'table' => [
                'name'               => 'Назва модуля',
                'url'                => 'Module View Route',
                'view_permission_id' => 'View Permission',
                'created_by'         => 'Створено',
            ],

            'form' => [
                'name'                  => 'Ім\'я модуля',
                'url'                   => 'View Route',
                'view_permission_id'    => 'View Permission',
                'directory_name'        => 'Ім\'я директорії',
                'namespace'             => 'Namespace',
                'model_name'            => 'Model Name',
                'controller_name'       => 'Controller &nbsp;Name',
                'resource_controller'   => 'Resourceful Controller',
                'table_controller_name' => 'Controller &nbsp;Name',
                'table_name'            => 'Ім\'я таблиці',
                'route_name'            => 'Route Name',
                'route_controller_name' => 'Controller &nbsp;Name',
                'resource_route'        => 'Resourceful Routes',
                'views_directory'       => 'Directory &nbsp;&nbsp;&nbsp;Name',
                'index_file'            => 'Індекс',
                'create_file'           => 'Створити',
                'edit_file'             => 'Редагування',
                'form_file'             => 'Назва форми',
                'repo_name'             => 'Repository Name',
                'event'                 => 'Назва події',
            ],
        ],
    ],

    'frontend' => [

        'auth' => [
            'login_box_title'    => 'Увійти',
            'login_button'       => 'Увійти',
            'login_with'         => 'Login with :social_media',
            'register_box_title' => 'Зареєструватися',
            'register_button'    => 'Зареєструватися',
            'remember_me'        => 'Запам`ятати мене',
        ],

        'passwords' => [
            'forgot_password'                 => 'Забули свій пароль?',
            'reset_password_box_title'        => 'Скинути пароль',
            'reset_password_button'           => 'Скинути пароль',
            'send_password_reset_link_button' => 'Відправити посилання для зміни паролю',
        ],

        'macros' => [
            'country' => [
                'alpha'   => 'Country Alpha Codes',
                'alpha2'  => 'Country Alpha 2 Codes',
                'alpha3'  => 'Country Alpha 3 Codes',
                'numeric' => 'Country Numeric Codes',
            ],

            'macro_examples' => 'Macro Examples',

            'state' => [
                'mexico' => 'Mexico State List',
                'us'     => [
                    'us'       => 'US States',
                    'outlying' => 'US Outlying Territories',
                    'armed'    => 'US Armed Forces',
                ],
            ],

            'territories' => [
                'canada' => 'Canada Province & Territories List',
            ],

            'timezone' => 'Timezone',
        ],

        'user' => [
            'passwords' => [
                'change' => 'Змінити пароль',
            ],

            'profile' => [
                'avatar'             => 'Аватар',
                'created_at'         => 'Створено',
                'edit_information'   => 'Змінити інформацію',
                'email'              => 'E-mail',
                'last_updated'       => 'Останнє оновлення',
                'first_name'         => 'Ім`я',
                'last_name'          => 'Прізвище',
                'address'            => 'Адреса',
                'state'              => 'Країна',
                'city'               => 'Місто',
                'zipcode'            => 'Zip Code',
                'ssn'                => 'SSN',
                'update_information' => 'Оновити інформацію',
            ],
        ],

    ],
];
