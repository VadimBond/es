<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'        => 'The :attribute must be accepted.',
    'active_url'      => ' :attribute не є валідним значенням URL.',
    'after'           => 'The :attribute must be a date after :date.',
    'after_or_equal'  => 'The :attribute must be a date after or equal to :date.',
    'alpha'           => 'Поле :attribute має містити лише літери.',
    'alpha_dash'      => 'The :attribute має містити тільки літери, цифри та тире.',
    'alpha_num'       => 'The :attribute має містити тільки літери ча цифри.',
    'array'           => ' :attribute має бути масивом.',
    'before'          => 'The :attribute must be a date before :date.',
    'before_or_equal' => ' :attribute має бути ідентичним до :date.',
    'between'         => [
        'numeric' => ':attribute має бути між :min та :max.',
        'file'    => ':attribute має бути :min and :max kb.',
        'string'  => ':attribute маємати між :min та :max символів.',
        'array'   => ':attribute має бути між :min і :max елементами.',
    ],
    'boolean'        => ' :attribute поле має бути true та false.',
    'confirmed'      => 'Підтвердження :attribute не збігається.',
    'date'           => ':attribute не валдіне значення.',
    'date_format'    => ':attribute не відповідає формату :format.',
    'different'      => ' :attribute і :other мають бути різними.',
    'digits'         => ' :attribute має бути :digits числом.',
    'digits_between' => 'Значення :attribute має бути між числами :min та :max .',
    'dimensions'     => ' :attribute сає невалідні розміри зображення.',
    'distinct'       => 'Атрибут :attribute має дублювання.',
    'email'          => 'The :attribute має бути валідним значенням пошти.',
    'exists'         => 'Вибраний :attribute не валідний.',
    'file'           => ':attribute має бути файлом.',
    'filled'         => 'The :attribute field must have a value.',
    'image'          => ':attribute має бути зображенням.',
    'in'             => 'вибраний атрибут :attribute невалідний.',
    'in_array'       => ' :attribute поле не існує в :other.',
    'integer'        => 'The :attribute має бути цілим числом.',
    'ip'             => 'The :attribute must be a valid IP address.',
    'json'           => 'The :attribute must be a valid JSON string.',
    'max'            => [
        'numeric' => ' :attribute не має бути більшим за :max.',
        'file'    => ' :attribute має бути на білье ніж :max kb.',
        'string'  => ' :attribute має бути не більше ніж :max символів.',
        'array'   => ' :attribute має містити не більше ніж :max елементів.',
    ],
    'mimes'     => ' :attribute має бути файлом типу: :values.',
    'mimetypes' => ' :attribute має бути файлом типу: :values.',
    'min'       => [
        'numeric' => ' :attribute має бути не меншим за :min.',
        'file'    => ' :attribute має бути як мінімум :min kb.',
        'string'  => ' :attribute має мати не менше ніж :min знаків.',
        'array'   => ' :attribute має мати не менше ніж :min елементів.',
    ],
    'not_in'               => 'Вибраний :attribute не валідний.',
    'numeric'              => ' :attribute має бути числом.',
    'present'              => 'Поле :attribute має бути заповнено.',
    'regex'                => ' :attribute має невалідний формат.',
    'required'             => ':attribute поле є обов\'язковим',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => ' :attribute має бути розміру :size.',
        'file'    => ' :attribute має бути :size kb.',
        'string'  => ' :attribute має мати :size символів.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'   => ' :attribute має бути строкою.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique'   => ' :attribute вже прийнято.',
    'uploaded' => ' :attribute не вдалося завантажити.',
    'url'      => ' :attribute формат не є валідним.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [

        'backend' => [
            'access' => [
                'permissions' => [
                    'associated_roles' => 'Пов\'язані ролі',
                    'dependencies'     => 'Залежності',
                    'display_name'     => 'Display Name',
                    'group'            => 'Група',
                    'group_sort'       => 'Group Sort',
                    'name'             => 'Ім\'я',
                    'sort'             => 'Порядок',

                    'groups' => [
                        'name' => 'Group Name',
                    ],

                    'name'   => 'Name',
                    'system' => 'System?',
                ],

                'roles' => [
                    'associated_permissions' => 'Пов\'язані дозволи',
                    'name'                   => 'Ім\'я',
                    'sort'                   => 'Сортувати',
                    'active'                 => 'Активний',
                ],

                'users' => [
                    'active'                  => 'Активний',
                    'associated_roles'        => 'Пов\'язані ролі',
                    'confirmed'               => 'Підтверджено',
                    'email'                   => 'Адреса електронної пошти',
                    'firstName'               => 'Ім\'я',
                    'lastName'                => 'Прізвище',
                    'other_permissions'       => 'Інші дозволи',
                    'old_password'            => 'Старий пароль',
                    'password'                => 'Новий пароль',
                    'password_confirmation'   => 'Нове підтвердження пароля',
                    'send_confirmation_email' => 'Надіслати підтвердження електронною поштою',
                ],
            ],
            'pages' => [
                'title'           => 'Назва',
                'description'     => 'Опис',
                'cannonical_link' => 'Cannonical Link',
                'seo_title'       => 'SEO Title',
                'seo_keyword'     => 'SEO Keyword',
                'seo_description' => 'SEO Description',
                'is_active'       => 'Активний',
            ],

            'emailtemplates' => [
                'title'       => 'Заголовок',
                'type'        => 'Тип',
                'subject'     => 'Предмет',
                'body'        => 'Тіло',
                'placeholder' => 'Placeholder',
                'is_active'   => 'Активний',
            ],

            'blogcategories' => [
                'title'     => 'Категорія блогу',
                'is_active' => 'Активний',
            ],

            'blogtags' => [
                'title'     => 'Тег блогу',
                'is_active' => 'Активний',
            ],

            'blogs' => [
                'title'            => 'Назва блогу',
                'category'         => 'Категорія блогів',
                'publish'          => 'Опублікувати',
                'image'            => 'Популярні зображення',
                'content'          => 'Вміст',
                'tags'             => 'Теги',
                'meta-title'       => 'Мета Назва',
                'slug'             => 'Slug',
                'cannonical_link'  => 'Cannonical Link',
                'meta_keyword'     => 'Meta Keyword',
                'meta_description' => 'Meta Description',
                'status'           => 'Статус',
            ],

            'settings' => [
                'sitelogo'        => 'Логотип сайту',
                'favicon'         => 'Значок сайту',
                'metatitle'       => 'Мета Назва',
                'metakeyword'     => 'Мета-ключові слова',
                'metadescription' => 'Мета опис',
                'companydetails'  => [
                    'address'       => 'Адреса компанії',
                    'contactnumber' => 'Контактний номер',
                ],
                'mail' => [
                    'fromname'  => 'Від імені',
                    'fromemail' => 'З електронної пошти',
                ],
                'footer' => [
                    'text'      => 'Нижній текст',
                    'copyright' => 'Авторський текст',
                ],
                'termscondition' => [
                    'terms'      => 'Правила та умови',
                    'disclaimer' => 'Disclaimer',
                ],
                'google' => [
                    'analytic' => 'Google Analytics',
                ],
            ],
            'faqs' => [
                    'questions' => 'Питання',
                    'answers'   => 'Відповідь',
                    'status'   => 'Статус',
            ],
        ],

        'frontend' => [
            'register-user' => [
                'email'                     => 'Електронна пошта',
                'firstName'                 => 'Ім\'я',
                'lastName'                  => 'Прізвище',
                'password'                  => 'Пароль',
                'address'                   => 'Адреса',
                'country'                   => 'Країна',
                'state'                     => 'Область',
                'city'                      => 'Місто',
                'zipcode'                   => 'Zip Code',
                'ssn'                       => 'SSN',
                'password_confirmation'     => 'Підтвердження паролю',
                'old_password'              => 'Старий пароль',
                'new_password'              => 'Новий пароль',
                'new_password_confirmation' => 'Підтвердження нового пароля',
                'terms_and_conditions'      => 'terms and conditions',
            ],
        ],
    ],

    'api' => [
        'login' => [
            'email_required'                => 'Будь ласка, введіть e-mail',
            'valid_email'                   => 'Будь ласка, введіть дійсний e-mail.',
            'password_required'             => 'Будь ласка, введіть пароль.',
            'username_password_didnt_match' => 'Будь ласка, введіть дійсні облікові дані.',
        ],

        'forgotpassword' => [
            'email_required'  => 'Будь ласка введіть email',
            'valid_email'     => 'Будь ласка, введіть дійсну адресу електронної пошти.',
            'email_not_valid' => 'Електронна пошта, яку ви ввели, не реєстрована.',
        ],

        'resetpassword' => [
            'email_required'            => 'Необхідно ввести електронну адресу',
            'valid_email'               => 'Необхідно ввести правильну електронну адресу.',
            'password_required'         => 'Необхідно ввести пароль.',
            'password_confirmed'        => 'Паролі не співпадають.',
            'token_required'            => 'Введіть токен.',
            'confirm_password_required' => 'Введіть підтвердження паролю.',
            'token_not_valid'           => 'Невірний токен.',
            'email_not_valid'           => 'Невірна електронна адреса.',
        ],
        'register' => [
            'state_required' => 'Необхідно вказати країну.',
            'city_required'  => 'Необхідно вказати місто.',
        ],
        'confirmaccount' => [
           'already_confirmed' => 'Аккаунт вже підтверджений.',
           'invalid_otp'       => 'Please enter valid otp.',
            'invalid_email'    => 'Невірний формат електронної адреси',
        ],
    ],

];
