<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in buttons throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'users' => [
                'activate'           => 'Активувати',
                'change_password'    => 'Змінити пароль',
                'clear_session'      => 'Очистити сеанс',
                'deactivate'         => 'Деактивувати',
                'delete_permanently' => 'Видалити перманентно',
                'login_as'           => 'Увійдіть як :user',
                'resend_email'       => 'Повторно вишліть підтвердження на електронну пошту',

                'restore_user'       => 'Відновити користувача',
            ],
        ],
    ],

    'emails' => [
        'auth' => [
            'confirm_account' => 'Підтвердження аккаунту',
            'reset_password'  => 'Скинути пароль',
        ],
    ],

    'general' => [
        'cancel'   => 'Скасувати',
        'continue' => 'Продовжити',
        'preview'  => 'Попередній перегляд',
        'crud' => [
            'create' => 'Створити',
            'add'    => 'Додати',
            'delete' => 'Видалити',
            'edit'   => 'Змінити',
            'update' => 'Оновити',
            'view'   => 'Перегляд',
        ],

        'save' => 'Зберегти',
        'view' => 'Перегляд',
    ],
];
