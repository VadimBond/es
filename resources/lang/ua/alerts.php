<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'roles' => [
            'created' => 'Роль була успішно створена.',
            'deleted' => 'Роль була успішно видалена.',
            'updated' => 'Роль була успішно оновлена.',
        ],

        'permissions' => [
            'created' => 'Дозвіл успішно створено.',
            'deleted' => 'Дозвіл успішно видалено.',
            'updated' => 'Дозвіл успішно оновлено.',

        ],

//        'confirmation_email'  => 'A new confirmation e-mail has been sent to the address on file.',
        'users' => [

            'created'             => 'Новий користувач успішно створений',
            'deleted'             => 'Користувач успішно видалений.',
            'deleted_permanently' => 'Користувач видалений назавжди.',
            'restored'            => 'Користувач успішно відновлений.',
            'session_cleared'     => "Сессія користувача успішно очищена.",
            'updated'             => 'Користувач успішно змінений.',
            'updated_password'    => "Пароль успішно змінено.",
        ],

//        'pages' => [
//            'created' => 'The Page was successfully created.',
//            'deleted' => 'The Page was successfully deleted.',
//            'updated' => 'The Page was successfully updated.',
//        ],
//
//        'blogcategories' => [
//            'created' => 'The Blog Category was successfully created.',
//            'deleted' => 'The Blog Category was successfully deleted.',
//            'updated' => 'The Blog Category was successfully updated.',
//        ],
//
//        'blogtags' => [
//            'created' => 'The Blog Tag was successfully created.',
//            'deleted' => 'The Blog Tag was successfully deleted.',
//            'updated' => 'The Blog Tag was successfully updated.',
//        ],

//        'blogs' => [
//            'created' => 'The Blog was successfully created.',
//            'deleted' => 'The Blog was successfully deleted.',
//            'updated' => 'The Blog was successfully updated.',
//        ],
//
//        'emailtemplates' => [
//            'deleted' => 'The Email Template was successfully deleted.',
//            'updated' => 'The Email Template was successfully updated.',
//        ],

        'settings' => [
            'updated' => 'Налаштування успішно змінені.',
        ],
//        'faqs' => [
//            'created' => 'The Faq was successfully created.',
//            'deleted' => 'The Faq was successfully deleted.',
//            'updated' => 'The Faq was successfully updated.',
//        ],

        'menus' => [
            'created' => 'Меню успішно створено.',
            'deleted' => 'Меню успішно видалено.',
            'updated' => 'Меню успішно оновлено.',

        ],
    ],
];
