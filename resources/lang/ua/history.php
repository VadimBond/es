<?php

return [

    /*
    |--------------------------------------------------------------------------
    | History Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain strings associated to the
    | system adding lines to the history table.
    |
    */

    'backend' => [
        'none'            => 'Історії не існує.',
        'none_for_type'   => 'Для цього типу немає історії.',
        'none_for_entity' => 'Історія відсутня для :entity.',
        'recent_history'  => 'Недавня історія',

        'roles' => [
            'created' => 'створив роль',
            'deleted' => 'видалив роль',
            'updated' => 'оновив роль',
        ],
        'permissions' => [
            'created' => 'створив дозвіл',
            'deleted' => 'видалив дозвіл',
            'updated' => 'оновив дозвіл',
        ],
        'pages' => [
            'created' => 'створив сторінку CMS',
            'deleted' => 'видалив CMS сторінку',
            'updated' => 'оновив CMS сторінку',
        ],
        'blogcategories' => [
            'created' => 'створив категорію блогів',
            'deleted' => 'видалив категорію блогів',
            'updated' => 'оновив категорію блогів',
        ],
        'blogtags' => [
            'created' => 'створив тег блогів',
            'deleted' => 'видалив тег блогів',
            'updated' => 'оновив тег блогів',
        ],
        'blocks' => [
            'created' => 'створив блок',
            'deleted' => 'видалив блок',
            'updated' => 'змінив блок',
        ],
        'questions' => [
            'created' => 'створив питання',
            'deleted' => 'видалив питання',
            'updated' => 'змінив питання',
        ],
        'department' => [
            'created' => 'створив кафедру',
            'deleted' => 'видалив кафедру',
            'updated' => 'змінив кафедру',
        ],
        'speciality' => [
            'created' => 'створив спеціальність',
            'deleted' => 'видалив спеціальність',
            'updated' => 'змінив спеціальність',
        ],
        'emailtemplates' => [
            'deleted' => 'видалив шаблон електронної пошти',
            'updated' => 'оновив шаблон електронної пошти',
        ],
        'users' => [
            'changed_password'    => 'змінив пароль для користувача',
            'created'             => 'створив користувача',
            'deactivated'         => 'деактивував користувача',
            'deleted'             => 'видалив користувача',
            'permanently_deleted' => 'остаточно видалив користувача',
            'updated'             => 'оновив користувача',
            'reactivated'         => 'повторно активував користувача',
            'restored'            => 'відновив користувача',
        ],
        'verify' => [
            'send_speciality' => 'відправив на верифікацію спеціальність',
            'decline_speciality' => 'відравив на доопрацювання',
            'reverse_speciality' => 'відмінив верифікацію',
            'approved_speciality' => 'верифікував спеціальність',

        ],
        'blanks' => [
            'delete_generated' => 'видалив згенерований бланк варіант ',
            'generate' => 'згенерував бланк варіант ',
            'edit' => 'змінив бланк зі спеціальності '

        ],

        'clone' => 'клонував спеціальність',
        'edit_blank' => 'змінив бланк',

    ],
];
