<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'        => 'Дані не спавпадають.',
    'general_error' => 'У вас недостатньо прав для даної дії.',

    'socialite'     => [
        'unacceptable' => ':provider не є прийнятним типом входу.',
    ],

    'throttle' => 'Перевищення максимальної к-ті спроб входу. Спробуйте ввійти через :seconds сек.',
    'unknown'  => 'Сталася помилка',
];
