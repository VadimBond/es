<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Strings Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in strings throughout the system.
    | Regardless where it is placed, a string can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'users' => [
                'delete_user_confirm'  => 'Ви впевнені, що хочете видалити цього користувача назавжди? Буду помилка при посиланні на цей ідентифікатор користувача. Це буде виконано на ваши ризик. Це не може бути скасоване.',
                'if_confirmed_off'     => '(Якщо підтвердження вимкнено)',
                'restore_user_confirm' => 'Відновити цього користувача у вихідному стані?',
            ],
        ],

        'dashboard' => [
            'title'   => 'Адміністративна панель управління',
            'welcome' => 'Вітаємо',
        ],

        'general' => [
            'all_rights_reserved' => 'Всі права захищені.',
            'are_you_sure'        => 'Ви впевнені, що хочете це зробити?',
            'boilerplate_link'    => 'Laravel AdminPanel',
            'continue'            => 'Продовжити',
            'member_since'        => 'Учасник з',
            'minutes'             => ' хвилини',
            'search_placeholder'  => 'Знайти...',
            'timeout'             => 'Ви автоматично вийшли, оскільки у вас не було активності ',

            'see_all' => [
                'messages'      => 'Переглянути всі повідомлення',
                'notifications' => 'Переглянути все',
                'tasks'         => 'Переглянути всі завдання',
            ],

            'status' => [
                'online'  => 'Online',
                'offline' => 'Offline',
            ],

            'you_have' => [
                'messages'      => '{0} У вас немає повідомлень|{1} У вас 1 повідомлення|[2,Inf] У вас :number повідомлення',
                'notifications' => '{0} У вас немає сповіщень|{1} У вас 1 сповіщення|[2,Inf] У вас :number сповіщень',
                'tasks'         => '{0} У вас емає завдань|{1} У вас 1 завдання|[2,Inf] У вас :number задання',
            ],
        ],

        'search' => [
            'empty'      => 'Будь ласка, введіть пошуковий термін.',
            'incomplete' => 'Ви повинні написати свою пошукову логіку для цієї системи.',
            'title'      => 'Результати пошуку',
            'results'    => 'Результати пошуку для :query',
        ],

        'welcome' => '<p>This is the AdminLTE theme by <a href="https://almsaeedstudio.com/" target="_blank">https://almsaeedstudio.com/</a>. This is a stripped down version with only the necessary styles and scripts to get it running. Download the full version to start adding components to your dashboard.</p>
<p>All the functionality is for show with the exception of the <strong>Access Management</strong> to the left. This boilerplate comes with a fully functional access control library to manage users/roles/permissions.</p>
<p>Keep in mind it is a work in progress and their may be bugs or other issues I have not come across. I will do my best to fix them as I receive them.</p>
<p>Hope you enjoy all of the work I have put into this. Please visit the <a href="https://github.com/rappasoft/laravel-5-boilerplate" target="_blank">GitHub</a> page for more information and report any <a href="https://github.com/rappasoft/Laravel-5-Boilerplate/issues" target="_blank">issues here</a>.</p>
<p><strong>This project is very demanding to keep up with given the rate at which the master Laravel branch changes, so any help is appreciated.</strong></p>
<p>- Viral Solani</p>',
    ],

    'emails' => [
        'auth' => [
            'error'                   => 'Упс!',
            'greeting'                => 'Привіт!',
            'regards'                 => 'З повагою,',
            'trouble_clicking_button' => 'Якщо у вас виникають проблеми, натисніть на кнопку ":action_text" кнопка, скопіюйте та вставте URL нижче в свій веб-браузер:',
            'thank_you_for_using_app' => 'Дякуємо за використання нашої програми!',

            'password_reset_subject'    => 'Скинути пароль',
            'password_cause_of_email'   => 'Ви отримали цей електронний лист, оскільки ми отримали запит на скидання пароля для вашого облікового запису.',
            'password_if_not_requested' => 'Якщо ви не надіслали запит на скидання пароля, подальші дії не потрібні.',
            'reset_password'            => 'Натисніть тут, щоб скинути свій пароль',

            'click_to_confirm' => 'Натисніть тут, щоб підтвердити свій обліковий запис:',
        ],
    ],

    'frontend' => [
        'test' => 'Test',

        'tests' => [
            'based_on' => [
                'permission' => 'Permission Based - ',
                'role'       => 'Role Based - ',
            ],

            'js_injected_from_controller' => 'Javascript Injected from a Controller',

            'using_blade_extensions' => 'Using Blade Extensions',

            'using_access_helper' => [
                'array_permissions'     => 'Using Access Helper with Array of Permission Names or ID\'s where the user does have to possess all.',
                'array_permissions_not' => 'Using Access Helper with Array of Permission Names or ID\'s where the user does not have to possess all.',
                'array_roles'           => 'Using Access Helper with Array of Role Names or ID\'s where the user does have to possess all.',
                'array_roles_not'       => 'Using Access Helper with Array of Role Names or ID\'s where the user does not have to possess all.',
                'permission_id'         => 'Using Access Helper with Permission ID',
                'permission_name'       => 'Using Access Helper with Permission Name',
                'role_id'               => 'Using Access Helper with Role ID',
                'role_name'             => 'Using Access Helper with Role Name',
            ],

            'view_console_it_works'          => 'View console, you should see \'it works!\' which is coming from FrontendController@index',
            'you_can_see_because'            => 'You can see this because you have the role of \':role\'!',
            'you_can_see_because_permission' => 'You can see this because you have the permission of \':permission\'!',
        ],

        'user' => [
            'change_email_notice'  => 'При зміні пошти ви будете розлогінені поки не підтвердите нову пошту.',
            'email_changed_notice' => 'Для входу підтвердіть пошту',
            'profile_updated'      => 'Акаунт підтверджено',
            'password_updated'     => 'Пароль змінено',
        ],

        'welcome_to' => 'Ласкаво просимо до :place',
    ],
];
