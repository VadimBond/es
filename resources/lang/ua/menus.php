<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'title' => 'Адміністрування',

            'roles' => [
                'all'        => 'Всі ролі',
                'create'     => 'Створити роль',
                'edit'       => 'Редагувати роль',
                'management' => 'Управління ролями',
                'main'       => 'Ролі',
            ],

            'permissions' => [
                'all'        => 'Усі права доступу',
                'create'     => 'Створити права доступу',
                'edit'       => 'Редагувати права доступу',
                'management' => 'Управління правами доступу',
                'main'       => 'Права доступу',
            ],

            'users' => [
                'all'             => 'Всі користувачі',
                'change-password' => 'Змінити пароль',
                'create'          => 'Створити користувача',
                'deactivated'     => 'Деактивовані користувачі',
                'deleted'         => 'Видалені користувачі',
                'edit'            => 'Редагувати користувача',
                'main'            => 'Користувачі',
                'view'            => 'Переглянути користувача',
            ],
        ],

        'log-viewer' => [
            'main'      => 'Журнал перегляду',
            'dashboard' => 'Панель приладів',
            'logs'      => 'Logs',
        ],

        'sidebar' => [
            'dashboard' => 'Аудит',
            'general'   => 'Головна',
            'system'    => 'Система',
        ],

        'pages' => [
            'all'        => 'Всі сторінки',
            'create'     => 'Створити сторінку',
            'edit'       => 'Редагувати сторінку',
            'management' => 'Управління сторінками',
            'main'       => 'Сторінки',
        ],

        'blogs' => [
            'all'        => 'Весь блог',
            'create'     => 'Створити блог',
            'edit'       => 'Редагувати блог',
            'management' => 'Блог Управління',
            'main'       => 'Блоги',
        ],

        'blogcategories' => [
            'all'        => 'Усі категорії блогів',
            'create'     => 'Створити категорію блогів',
            'edit'       => 'Редагувати категорію блогів',
            'management' => 'Управління категорією блогів',
            'main'       => 'Сторінки CMS',
        ],

        'blogtags' => [
            'all'        => 'Усі теги блогів',
            'create'     => 'Створити тег блогів',
            'edit'       => 'Редагувати тег блогів',
            'management' => 'Управління тегами блогів',
            'main'       => 'Теги блогів',
        ],

        'blog' => [
            'all'        => 'Усі сторінки блогів',
            'create'     => 'Створити сторінку блогів',
            'edit'       => 'Редагувати сторінку блогів',
            'management' => 'Управління блогами',
            'main'       => 'Сторінки блогів',
        ],

        'faqs' => [
            'all'        => 'Всі FAQ сторінки',
            'create'     => 'Створити FAQ сторінку',
            'edit'       => 'Редагувати FAQ сторінку',
            'management' => 'Управління FAQ',
            'main'       => 'Faq сторінки',
        ],

        'emailtemplates' => [
            'all'        => 'Усі шаблони електронної пошти',
            'create'     => 'Створити шаблон електронної пошти',
            'edit'       => 'Редагувати шаблон електронної пошти',
            'management' => 'Управління шаблонами електронної пошти',
            'main'       => 'Шаблон електронної пошти',
        ],

        'settings' => [
            'all'        => 'Усі налаштування',
            'create'     => 'Створити налаштування',
            'edit'       => 'Змінити налаштування',
            'management' => 'Налаштування управління',
            'main'       => 'Налаштування',
        ],

        'menus' => [
            'all'        => 'Усі меню',
            'create'     => 'Створити меню',
            'edit'       => 'Редагувати меню',
            'management' => 'Управління меню',
            'main'       => 'Меню',
        ],

        'modules' => [
            'all'        => 'Всі модулі сторінки',
            'create'     => 'Створити модуль сторінки',
            'management' => 'Управління модулями',
            'main'       => 'Сторінки модулів',
        ],
    ],

    'language-picker' => [
        'language' => 'Мова',
        /*
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
            'ar'    => 'Арабська',
            'da'    => 'Датська',
            'de'    => 'Німецька',
            'el'    => 'Грецька',
            'en'    => 'Англійська',
            'es'    => 'Іспанська',
            'fr'    => 'Французька',
            'id'    => 'Індонезійська',
            'it'    => 'Італійська',
            'nl'    => 'Голландська',
            'pt_BR' => 'Бразильська португальська',
            'ru'    => 'Російська',
            'sv'    => 'Шведська',
            'th'    => 'Тайська',
        ],
    ],
];
