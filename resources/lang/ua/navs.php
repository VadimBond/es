<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home'   => 'Головна сторінка',
        'logout' => 'Вийти з системи',
    ],

    'frontend' => [
        'dashboard' => 'Панель приладів',
        'dashboard_omu' => 'Представник ОМУ',
        'login'     => 'Увійти',
        'macros'    => 'Макроси',
        'register'  => 'Зареєструватися',

        'user' => [
            'account'         => 'Мій акаунт',
            'administration'  => 'Адміністрування',
            'change_password' => 'Змінити пароль',
            'my_information'  => 'Інформація про мене',
            'profile'         => 'Профіль',
        ],
    ],
];
