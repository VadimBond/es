<!--Action Button-->
    @if(Active::checkUriPattern('admin/access/role'))
        <div class="btn-group">
          <button type="button" class="btn btn-warning btn-flat dropdown-toggle" data-toggle="dropdown">Експорт даних
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li id="copyButton"><a href="#"><i class="fa fa-clone"></i>Зкопіювати</a></li>
            <li id="excelButton"><a href="#"><i class="fa fa-file-excel-o"></i> Excel</a></li>
            <li id="printButton"><a href="#"><i class="fa fa-print"></i>Роздрукувати</a></li>
          </ul>
        </div>
    @endif
<!--Action Button-->
<div class="btn-group">
  <button type="button" class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">Дії
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a href="{{route('admin.access.role.index')}}"><i class="fa fa-list-ul"></i> {{trans('menus.backend.access.roles.all')}}</a></li>
    @permission('create-role')
    <li><a href="{{route('admin.access.role.create')}}"><i class="fa fa-plus"></i> {{trans('menus.backend.access.roles.create')}}</a></li>
    @endauth
  </ul>
</div>