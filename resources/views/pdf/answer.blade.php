<!DOCTYPE html>
<html lang="en">
	<head>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<meta charset="UTF-8">
		<title>Document</title>

		<link rel="stylesheet" href="{{ asset('/css/pdfStyles/law.css') }}">
	</head>
	<body>
		<main>
			<div class="answerBlank clearfix">
				<h2 class="upperCase">Сумський державний університет</h2>
				<div class="subscriptionBlock">

				</div>
			</div>
			<div class="title">
				<h3 class="upperCase">Шаблон відповіді</h3>
				<p>{{ $data['title'] }}</p>
				<p>{{ $data['purpose'] }}</p>
			</div>
			<p class="variant">Варіант № {{ $data['variant'] }}</p>
			<div>
				@php($j = 1)
				@foreach($data['blocks'] as $blockId => $block)

					<div class="checkTable fLeft" style="width: {{ $block->tableNmb * 28 }}px;">
						<table>
							<tr>
								<th colspan="{{ $block->tableNmb }}">Блок {{$block->block_name}}</th>
							</tr>
							@foreach($data['questions'] as $idQuestion => $currQuestions)
								@if($idQuestion === $block->id)
									@for($indexQuestion = 0; $indexQuestion <= count($currQuestions); $indexQuestion++)
										<tr>
											@for($indexAnswer = 0; $indexAnswer <= count($currQuestions[0]->answers); $indexAnswer++)
												@if($indexQuestion === 0)
													@if($indexAnswer === 0)
														<td>№</td>
													@else
														<td>{{ $data['letters'][$indexAnswer - 1] }}</td>
													@endif
												@else
													@if($indexAnswer === 0)
														<td>{{ $j }}</td>
														@php($j++)
													@else
														<td>
															@foreach($currQuestions as $indexCurQuestion => $curQuestion)
																@if($indexCurQuestion === $indexQuestion - 1)
																	@foreach($curQuestion->answers as $indexInnerAnswer => $innerAnswer)
																		@if($indexInnerAnswer === $indexAnswer - 1)
																			@if($innerAnswer->answer_right == 1)
																				&#9679;
																			@endif
																		@endif
																	@endforeach
																@endif
															@endforeach
														</td>
													@endif
												@endif
											@endfor
										</tr>
									@endfor
								@endif
							@endforeach
						</table>
					</div>
				@endforeach
			</div>
			<div class="clearfix signBlock">
				<div class="position">Відповідальний секретар приймальної комісії</div>
				<div style="float:right;width:30%;">
					<div class="subscribeBlock">(підпис)</div>
				</div>

			</div>
		</main>
        <htmlpagefooter name="footer">
            <p class="documentNmb">Документ № {{ $data['cypter'] }}</p>
            <p class="nmbPage">{PAGENO}</p>
        </htmlpagefooter>
	</body>
</html>