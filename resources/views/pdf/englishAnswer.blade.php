<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <meta charset="UTF-8">
    <title>Document</title>


    <link rel="stylesheet" href="{{ asset('/css/pdfStyles/law.css') }}">
</head>
<body>
<main>
<div class="answerBlank clearfix">
    <h2 class="upperCase">Сумський державний університет</h2>
    <div class="subscriptionBlock">
        <p > &nbsp; </p>
    </div>
</div>
    <div class="title">
        <h3 class="upperCase">Шаблон відповіді</h3>
        <p>{{ $data['title'] }}</p>
        <p>{{ $data['purpose'] }}</p>
    </div>
    <p class="variant">Варіант №  {{ $data['variant'] }}</p>
    <div>
        @php($q = 1)
        @php($countAns = 0)
        @foreach($data['blocks'] as $blockId => $block)


            <div class="checkTable fLeft" style="width: {{ $block->tableNmb * 28 }}px;">
                <table>
                    <tr>
                        <th colspan="{{ $block->tableNmb }}">{{$block->block_name}}</th>
                    </tr>
                    @foreach($data['english_config'] as $config)
                        @if($config->block_id === $block->id)
                            @if($config->answer_list == 1 && $config->true_false == 0 && $config->text == 1)
                                <tr>
                                    <td>№</td>
                                    @for($i = 0; $i < $block->countAnswers + $config->extra_answers; $i++)
                                        <td>{{ $data['letters'][$i] }} </td>
                                    @endfor
                                </tr>

                                @foreach($block->english_text as $text)


                                    @foreach($data['english_questions'] as $question)
                                        @if($question->id_english_question == $text->id && $q <= $block->countAnswers)
                                            <tr>
                                                <td>{{$q}}</td>
                                                @php($ans = "A")
                                                @for($j = 0; $j < $block->countAnswers + $config->extra_answers; $j++)
                                                    <td>
                                                        @if( $question->right_answer == $ans)
                                                            &#9711;
                                                        @endif
                                                    </td>
                                                    @php($ans++)
                                                @endfor
                                                @php($q++)
                                            </tr>
                                        @endif
                                    @endforeach

                                @endforeach

                            @endif

                            @if($config->answer_list == 1 && $config->true_false == 1)
                                <tr>
                                    <td>№</td>
                                    <td>T</td>
                                    <td>F</td>
                                </tr>
                                @foreach($block->english_text as $text)


                                    @foreach($data['english_questions'] as $question)
                                        @if($question->id_english_question == $text->id)
                                            <tr>
                                                <td>{{$q}}</td>
                                                <td>
                                                    @if( $question->right_answer == "1")
                                                        &#9711;
                                                    @endif
                                                </td>
                                                <td>
                                                    @if( $question->right_answer == "0")
                                                        &#9711;
                                                    @endif
                                                </td>
                                                @php($q++)
                                            </tr>
                                        @endif
                                    @endforeach

                                @endforeach
                            @endif

                            @if($config->answer_list == 0 && $config->text == 0)
                                <tr>
                                    <td>№</td>
                                    @for($i = 0; $i < $block->countAnswers; $i++)
                                        <td>{{ $data['letters'][$i] }} </td>
                                    @endfor
                                </tr>
                                @foreach($block->questions as $idQuestion => $currQuestions)
                                    @if($currQuestions->block_id === $block->id)
                                        @php($countAns += $block->countAnswers)
                                        @if($countAns> 18)
                                            @php($secondRow = 1)
                                        @endif
                                        <tr>  <td>{{ $q }}</td>
                                            @php($q++)
                                            @foreach($data['english_answers'] as $indexAnswer => $answer)
                                                @if($answer->id_english_question == $currQuestions->id)
                                                    @if($answer->answer_right == 1)
                                                        <td>&#9711;</td>
                                                    @else
                                                        <td></td>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </tr>
                                    @endif
                                @endforeach
                            @endif

                            @if($config->answer_list == 0 && $config->text == 1)
                                <tr>
                                    <td>№</td>
                                    @for($i = 0; $i < 4; $i++)
                                        <td>{{ $data['letters'][$i] }}</td>
                                    @endfor
                                </tr>
                                @foreach($block->english_text as $text)

                                    @foreach($data['english_questions'] as $question)
                                        @if($question->id_english_question == $text->id)
                                            <tr>    <td>{{ $q }}</td>
                                                @php($q++)
                                                @foreach($data['english_answers'] as $indexAnswer => $answer)
                                                    @if($answer->id_english_question == $question->id)
                                                        @if($answer->answer_right == 1)
                                                            <td>&#9711;</td>
                                                        @else
                                                            <td></td>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </tr>
                                        @endif
                                    @endforeach

                                @endforeach
                            @endif

                            @if($config->answer_list == 1 && $config->text == 0)
                                @foreach ($block->connection as $connection)
                                    <tr>
                                        <td>№</td>
                                        @for($i = 0; $i < sizeof($connection->answers); $i++)
                                            <td>{{ $data['letters'][$i] }} </td>
                                        @endfor
                                    </tr>
                                    @foreach($connection->questions as $question)
                                        <tr>
                                            <td>{{$q}}</td>
                                            @php($ans = "A")
                                            @for($j = 0; $j < $block->countAnswers + $config->extra_answers; $j++)
                                                <td>
                                                    @if( $question->right_answer == $ans)
                                                        &#9711;
                                                    @endif
                                                </td>
                                                @php($ans++)
                                            @endfor
                                            @php($q++)
                                        </tr>
                                    @endforeach
                                @endforeach
                            @endif

                        @endif
                    @endforeach
                    @foreach($block->questions as $idQuestion => $currQuestions)
                        @if($idQuestion === $block->id)
                            @for($indexQuestion = 0; $indexQuestion <= count($currQuestions); $indexQuestion++)
                                <tr>
                                    @for($indexAnswer = 0; $indexAnswer <= count($currQuestions[0]->answers); $indexAnswer++)
                                        @if($indexQuestion === 0)
                                            @if($indexAnswer === 0)
                                                <td>№</td>
                                            @else
                                                <td>{{ $data['letters'][$indexAnswer - 1] }}</td>
                                            @endif
                                        @else
                                            @if($indexAnswer === 0)
                                                <td>{{ $i }}</td>
                                                @php($i++)
                                            @else
                                                <td>
                                                    @foreach($currQuestions as $indexCurQuestion => $curQuestion)
                                                        @if($indexCurQuestion === $indexQuestion - 1)
                                                            @foreach($curQuestion->answers as $indexInnerAnswer => $innerAnswer)
                                                                @if($indexInnerAnswer === $indexAnswer - 1)
                                                                    @if($innerAnswer->answer_right == 1)
                                                                        &#9711;
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                </td>
                                            @endif
                                        @endif
                                    @endfor
                                </tr>
                            @endfor
                        @endif
                    @endforeach
                </table>
            </div>
        @endforeach
    </div>
<div class="clearfix signBlock">
    <div class="position">Відповідальний секретар приймальної комісії</div>
    <div style="float:right;width:30%;">
        <div class="subscribeBlock">(підпис)</div>
    </div>

</div>
</main>
<htmlpagefooter name="footer">
    <p class="documentNmb">Документ № {{ $data['cypter'] }} - згенеровано автоматично ES</p>
    <p class="nmbPage">{PAGENO}</p>
</htmlpagefooter>
</body>
</html>