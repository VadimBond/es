<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <meta charset="UTF-8">
    <title>Document</title>


    <link rel="stylesheet" href="{{ asset('/css/pdfStyles/law.css') }}">
</head>
<body>
<main>
    <div class="clearfix">
        <h2 class="upperCase">Приклад генерування пдф блоку</h2>
    </div>

    <h3>
        @if($data['block']->type == 1)
            Task 1
        @else
            Блок
        @endif
        {{$data['block']->block_name}}<br>
        @if($data['block']->task_title != null)
            <div class="row">
            {{$data['block']->task_title}}
            </div>
        @endif

    </h3>

    @if($data['block']->url_image !="0")
        <img class="image imageQuestion" src="{{$data['block']->url_image}}" alt="block image">
    @endif

    <ul class="listNone">
        @foreach($data['questions'] as $idQuestion => $currQuestions)
            @if($idQuestion === $data['block']->id)
                @foreach($currQuestions as $question)
                        <li class="questionText">
                        {{ $loop->iteration }}. {{$question->text_question}}
                        </li>
                    <li class="bottomLine">
                            @foreach($question->answers as $answer)
                            <p style =" margin-left:20px; text-indent:20px;">{!! $answer->text_answer !!}</p>
                        @endforeach
                    </li>
                @endforeach
            @endif
        @endforeach
    </ul>

</main>

</body>
</html>