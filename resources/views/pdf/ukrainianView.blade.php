<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <meta charset="UTF-8">
    <title>Document</title>


    <link rel="stylesheet" href="{{ asset('/css/pdfStyles/law.css') }}">
</head>
<body>
<main>
    <div class="clearfix">
        <h2 class="upperCase">Сумський державний університет</h2>
        <div class="subscriptionBlock clearfix">
            <p class="upperCase">Затверджую</p>
            <p>Голова приймальної комісії</p>
            <span>_______________</span>
            <p>____  ___________  20 ____ р.</p>
        </div>
    </div>
    <div class="title">
        <h3 class="upperCase">Екзаменаційне завдання</h3>
        <p>{{ $data['title'] }}</p>
    </div>
    <p class="variant">Варіант №  {{ $data['variant'] }}</p>
    Зміст завдання<br>
    <div style = "margin-left:20px;">{{$data['block_name']}}</div>
    <p style = " text-indent:20px;">
            {!! $data['question_text'] !!}
    </p>
    <div class="clearfix signBlock">
        <div class="position">Відповідальний секретар приймальної комісії</div>
        <div style="float:right;width:30%;">
            <div class="subscribeBlock">(підпис)</div>
        </div>

    </div>
    <pagebreak></pagebreak>

    <div class="answerBlank clearfix">
        <h2 class="upperCase">Сумський державний університет</h2>
        <div class="subscriptionBlock">
        </div>
    </div>
    <div class="title">
        <h3 class="upperCase">Шаблон відповіді</h3>

    </div>
    <p class="variant">Варіант №  {{ $data['variant'] }}</p>
        <p style=" text-indent:20px;">
            {!! $data['answer_text'] !!}
        </p>
    <div class="clearfix signBlock">
        <div class="position">Відповідальний секретар приймальної комісії</div>
        <div style="float:right;width:30%;">
            <div class="subscribeBlock">(підпис)</div>
        </div>

    </div>
</main>
<htmlpagefooter name="footer">
    <p class="documentNmb">Документ № {{ $data['cypter'] }} - згенеровано автоматично ES</p>
    <p class="nmbPage">{PAGENO}</p>
</htmlpagefooter>
</body>
</html>