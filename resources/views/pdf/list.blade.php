<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <meta charset="UTF-8">
    <title>Список завдань</title>


    <link rel="stylesheet" href="{{ asset('/css/pdfStyles/law.css') }}">
</head>
<body>
<main>
    <div class="clearfix">
        <h2 class="upperCase">Сумський державний університет</h2>
        <div class="subscriptionBlock clearfix">
            <p class="upperCase">Затверджую</p>
            <p>Голова приймальної комісії</p>
            <span>_______________</span>
            <p>____  ___________  20 ____ р.</p>
        </div>
    </div>
    <div class="title">
        <h3 class="upperCase">Список завдань</h3>
    </div>
    @php($j = 1)
    @if(count($data['blocks']) > 0)
        @foreach($data['blocks'] as $block)
            <h3>
                @if($block->type == 1) Task {{ $loop->iteration }} @elseif($block->type == 0) Блок @endif {{$block->block_name}}<br>
                @if($block->task_title != null)
                    <div class = "row"><b>
                            {{$block->task_title}}
                        </b>
                    </div>
                @endif
            </h3>
            @if($block->type == 1)
                <div class = "row">
                    @php($i = $j)
                    @php($text=$block->blank_text)
                    @while(strpos($text, '(XX)') !== false)
                        @php ($text = preg_replace('/(XX)/', $i,$text, 1))
                        @php($i++)
                    @endwhile
                    {!! $text !!}
                </div>
            @endif
            @if($block->url_image != "0")
                <img class="image imageQuestion" src="{{$block->url_image}}" alt="block image">
            @endif

            <ul class="listNone">
                @if( count($data['questions']) > 0)
                    @foreach($data['questions'] as $idQuestion => $currQuestions)
                        @if($idQuestion === $block->id)
                            @foreach($currQuestions as $question)
                                <li class="questionText">
                                    {{ $j }}. {!! $question->text_question !!}
                                    @php($j++)
                                </li>
                                <li class="bottomLine">
                                    <ul class="answersList">
                                        @foreach($question->answers as $answer)
                                            <li>
                                                <div>
                                                    @if($answer->answer_right)
                                                        <b>{!! $answer->text_answer !!}</b>
                                                    @else
                                                        <p>{!! $answer->text_answer !!}</p>
                                                    @endif
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                        @endif
                    @endforeach
                @else
                    <h3 class="textCenter">Питання до блоку відсутні</h3>
                @endif
            </ul>
        @endforeach
    @else
        <h3 class="textCenter">Блоки відсутні</h3>
    @endif

    <div class="clearfix signBlock">
        <div class="position">Голова приймальної комісії</div>
        <div style="float:right;width:30%;">
            <div class="subscribeBlock">(підпис)</div>
        </div>

    </div>
</main>
</body>
</html>