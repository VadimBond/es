<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <meta charset="UTF-8">
    <title>Список завдань</title>

    <link rel="stylesheet" href="{{ asset('/css/pdfStyles/law.css') }}">
</head>
<body>
<main>
    <div class="clearfix">
        <h2 class="upperCase">Сумський державний університет</h2>
        <div class="subscriptionBlock clearfix">
            <p class="upperCase">Затверджую</p>
            <p>Голова приймальної комісії</p>
            <span>_______________</span>
            <p>____  ___________  20 ____ р.</p>
        </div>
    </div>
    <div class="title">
        <h3 class="upperCase">Екзаменаційне завдання</h3>
        <p>{{ $data['title'] }}</p>
        <p>{{ $data['purpose'] }}</p>
    </div>
    <p class="variant">Варіант №  {{ $data['variant'] }}</p>
    @php($q = 1)
    @foreach($data['blocks'] as $block)
        <h3> {{$block->block_name}}<br>
            <div class = "row">
                <b>
                    {{$block->task_title}}
                </b>
            </div>
        </h3>


        <ul class="listNone">
            @foreach ($data['english_config'] as $conf)
                @if($conf->block_id == $block->id)
                    @foreach ($block->english_text as $text)
                        @if($conf->answer_list == 1 && $conf->text == 1 )
                            @foreach ($data['english_questions'] as $question)
                                @if($question->id_english_question == $text->id)
                                    <li class = "question_name" style="margin-left:10px">
                                        @if($conf->true_false == 0)
                                            {!! $question->right_answer !!}
                                        @else {!! $q !!}_____
                                        @php($q++)
                                        @endif {!!$question->text_question !!}

                                    </li>
                                @endif
                            @endforeach
                        @endif
                        <li class = "question_name">
                            @php($textie=$text->text)
                            @while(strpos($textie, 'NNN') !== false)
                                @php ($textie = preg_replace('/NNN/', $q.'_____',$textie, 1))
                                @php($q++)
                            @endwhile
                            {!! $textie !!}
                        </li>
                        @if($conf->answer_list == 0 && $conf->text == 1)
                            @foreach ($data['english_questions'] as $question)
                                @if($question->id_english_question == $text->id)
                                    <li class = "question_name">
                                        <div class = "answerEngNumb" > {!! $q !!} </div>
                                        @if($question->text_question != ' ')
                                            {!! $question->text_question !!}
                                            <br>
                                        @endif
                                        <ul class="listNone">
                                            @php($q++)
                                            @php($ans = "A")
                                            @foreach ($data['english_answers'] as $answer)
                                                @if($answer->id_english_question == $question->id)
                                                    <li>
                                                        {!! $ans !!} {!! $answer->text_answer !!}
                                                        @php($ans++)
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </li>

                                @endif
                            @endforeach
                        @endif
                        @foreach ($data['english_questions'] as $question)
                            @if($question->block_id == $block->id)
                                <li class = "question_name">
                                    <div class = "answerEngNumb" > {!! $q !!}</div>

                                    @php($q++)
                                    @php($ans = "A")
                                    @foreach ($data['english_answers'] as $answer)
                                        @if($answer->id_english_question == $question->id)
                                            <div class = "englishAnsStep" >
                                                {!! $ans !!} {!! $answer->text_answer !!}
                                                @php($ans++)
                                            </div>
                                        @endif
                                    @endforeach
                                </li>
                            @endif
                        @endforeach


                    @endforeach

                    @if($conf->text == 0 && $conf->answer_list == 0)

                        @foreach ($block->questions as $question)
                            <li class = "question_name">
                                <div class = "answerEngNumb" > {!! $q !!} </div>
                                @if($question->text_question != ' ')
                                    {!! $question->text_question !!}
                                    <br>
                                @endif
                                <ul class="listNone">
                                    @php($q++)
                                    @php($ans = "A")
                                    @foreach ($data['english_answers'] as $answer)
                                        @if($answer->id_english_question == $question->id)
                                            <li>
                                                {!! $ans !!} {!! $answer->text_answer !!}
                                                @php($ans++)
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                    @else
                        @php($ans = "A")

                        <table class = "taskTable"><tr>
                                @foreach ($block->connection as $connection)
                                    @foreach ($connection->answers as $currAnswer)

                                        <td> {!! $currAnswer->text_question !!} </td>
                                    @endforeach
                                @endforeach
                            </tr>
                            <tr>
                                @foreach ($block->connection as $connection)
                                    @foreach ($connection->answers as $currAnswer)
                                        <th>{!! $ans !!}</th>
                                        @php($ans++)
                                    @endforeach
                                @endforeach
                            </tr>
                        </table>
                        <li class = "question_name">
                            @foreach ($block->connection as $connection)
                                @foreach ($connection->questions as $currQuestion)

                                    <div class = "englishAnsStep"> {!! $q !!} {!! $currQuestion->text_question !!} </div>
                                    @php($q++)
                                @endforeach
                            @endforeach
                        </li>
                    @endif
                    <hr class = "line">
                @endif
            @endforeach
        </ul>
    @endforeach
    <br>
    <div class="clearfix signBlock">
        <div class="position">Відповідальний секретар приймальної комісії</div>
        <div style="float:right;width:30%;">
            <div class="subscribeBlock">(підпис)</div>
        </div>

    </div>
    <pagebreak></pagebreak>


    <div class="answerBlank">
        <h2 class="upperCase">Сумський державний університет</h2>
        <div class="subscriptionBlock">
            <p>Шифр __________</p>
        </div>
    </div>
    <div class="title">
        <h3 class="upperCase">Аркуш відповіді</h3>
        <p>{{ $data['title'] }}</p>
        <p>{{ $data['purpose'] }}</p>
    </div>
    <p class="variant">Варіант №____</p>
    <div>
        @php($q = 1)
        @php($maxRow = 0)
        @php($countAns = 0)
        @php($secondRow = 0)
        @php($secondMax = 0)
        @foreach($data['blocks'] as $blockId => $block)
            @php($countCurr = 0)
            <div class="checkTable fLeft" style="width: {{ $block->tableNmb * 28 }}px;">
                <table>
                    <tr>
                        <th colspan="{{ $block->tableNmb }}">{{$block->block_name}}</th>
                    </tr>
                    @php($countAns += $block->tableNmb)
                    @if($countAns> 18)
                        @php($secondRow = 1)
                    @endif
                    @foreach($data['english_config'] as $config)
                        @if($config->block_id === $block->id)
                            @if($config->answer_list == 1 && $config->true_false == 0 && $config->text == 1)
                                <tr>
                                    <td>№</td>
                                    @for($i = 0; $i < $block->tableNmb -1; $i++)
                                        <td>{{ $data['letters'][$i] }} </td>
                                    @endfor
                                </tr>
                                @for($i = 1; $i <= $block->countAnswers; $i++)
                                    <tr>
                                        <td>{{$q}}</td>
                                        @for($j = 0; $j < $block->countAnswers + $config->extra_answers; $j++)
                                            <td></td>
                                        @endfor
                                        @php($q++)
                                    </tr>
                                @endfor
                                @php($countCurr += $block->countAnswers)

                            @endif

                            @if($config->answer_list == 1 && $config->true_false == 1)
                                <tr>
                                    <td>№</td>
                                    <td>T</td>
                                    <td>F</td>
                                </tr>
                                @for($i = 1; $i <= $block->countAnswers ; $i++)
                                    <tr>
                                        <td>{{$q}}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @php($q++)
                                @endfor
                                @php($countCurr += $block->countAnswers)

                            @endif

                            @if($config->answer_list == 0 && $config->text == 0)
                                <tr>
                                    <td>№</td>
                                    @for($i = 0; $i < $block->countAnswers; $i++)
                                        <td>{{ $data['letters'][$i] }} </td>
                                    @endfor
                                </tr>

                                @foreach($block->questions as $idQuestion => $currQuestions)
                                    @if($currQuestions->block_id === $block->id)

                                        <tr>  <td>{{ $q }}</td>
                                            @php($q++)
                                            @foreach($data['english_answers'] as $indexAnswer => $answer)
                                                @if($answer->id_english_question == $currQuestions->id)
                                                    <td></td>
                                                @endif
                                            @endforeach
                                        </tr>
                                    @endif
                                @endforeach
                                @php($countCurr += sizeof($block->questions))

                            @endif

                            @if($config->answer_list == 0 && $config->text == 1)
                                <tr>
                                    <td>№</td>
                                    @for($i = 0; $i < 4; $i++)
                                        <td>{{ $data['letters'][$i] }} </td>
                                    @endfor
                                </tr>
                                @for($i = 1; $i <= $block->countAnswers; $i++)
                                    <tr>
                                        <td>{{$q}}</td>
                                        @for($j = 0; $j < 4; $j++)
                                            <td></td>
                                        @endfor
                                        @php($q++)
                                    </tr>
                                @endfor
                                @php($countCurr += $block->countAnswers)

                            @endif

                            @if($config->answer_list == 1 && $config->text == 0)
                                <tr>
                                    <td>№</td>
                                    @foreach ($block->connection as $connection)
                                        @for($i = 0; $i < sizeof($connection->answers); $i++)
                                            <td>{{ $data['letters'][$i] }} </td>
                                        @endfor

                                </tr>
                                @for($i = 1; $i <= sizeof($connection->questions); $i++)
                                    <tr>
                                        <td>{{$q}}</td>
                                        @for($j = 0; $j < sizeof($connection->answers); $j++)
                                            <td></td>
                                        @endfor
                                        @php($q++)
                                    </tr>
                                @endfor
                                @php($countCurr += sizeof($connection->questions))

                                @endforeach
                            @endif

                            @foreach($data['english_questions'] as $idQuestion => $currQuestions)

                                @if($currQuestions->id === $block->id)
                                    @php($countCurr = count($currQuestions))
                                    @for($indexQuestion = 0; $indexQuestion <= count($currQuestions); $indexQuestion++)
                                        @php($countCurr += $block->countAnswers)

                                        <tr>
                                            @for($indexAnswer = 0; $indexAnswer <= count($currQuestions[0]->answers); $indexAnswer++)
                                                @if($indexQuestion === 0)
                                                    @if($indexAnswer === 0)
                                                        <td>№</td>
                                                    @else
                                                        <td>{{ $data['letters'][$indexAnswer - 1] }}</td>
                                                    @endif
                                                @else
                                                    @if($indexAnswer === 0)
                                                        <td>{{ $i }}</td>
                                                        @php($i++)
                                                    @else
                                                        <td></td>
                                                    @endif
                                                @endif
                                            @endfor
                                        </tr>
                                    @endfor
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                    @if($secondRow == 0)
                        @if($countCurr > $maxRow)
                            @php($maxRow = $countCurr)
                        @endif
                    @else
                        @if($countCurr > $secondMax)
                            @php($secondMax = $countCurr)
                        @endif
                    @endif
                </table>
            </div>
        @endforeach
    </div>

    <div class="attentionBlock">
        <div class="fLeft attentionText">
            <p>
                <b>УВАГА!!! </b> Завдання мають кілька варіанів відповідей, серед яких лише один правильний.
                Виберіть правильний, на Вашу думку, варіант та позначте його, як показано на зразку.
                <b>Кількість виправлень вплиаває на загальну
                    оцінку роботи!
                </b>
            </p>
        </div>
        <div class="exampleFill">
            <table>
                <tbody>
                <tr>
                    <th>№</th>
                    <th>a</th>
                    <th>b</th>
                    <th>c</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td></td>
                    <td>X</td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    @if((count($data['commissioners']) + ($maxRow+$secondMax) *1.5) > 18 || $countAns > 36)
        <pagebreak></pagebreak>
    @endif
    <div class="clearfix">
        <div class="underLinedBlock">
            <p>Кількість правильних відповідей - </p>
        </div>
        <div class="underLinedBlock">
            <p>Кількість балів за них - </p>
        </div>
    </div>
    <div class="clearfix">
        <div class="underLinedBlock">
            <p>Кількість виправлень - </p>
        </div>
        <div class="underLinedBlock">
            <p>Знято балів за виправлення - </p>
        </div>
    </div>

    <div class="clearfix resultContainer">
        <div class="resultBlock titleBlock">Всього балів <br> з врахуванням знятих -</div>
        <div class="resultBlock">
            <p class="score">Оцінка - </p>
            <p class="scoreField">(числом та прописом)</p>
        </div>
    </div>


    @foreach($data['commissioners'] as $commissioner)
        <div class="clearfix signBlock">
            <div class="position">{{ $commissioner->commissioner_position }}</div>
            <div class="subscribeContainer">
                <div class="subscribeBlock">(підпис)</div>
            </div>
            <div class="initials">{{ $commissioner->commissioner_name }}</div>
        </div>
    @endforeach
</main>
<htmlpagefooter name="footer">
    <p class="documentNmb">Документ № {{ $data['cypter'] }} - згенеровано автоматично ES</p>
    <p class="nmbPage">{PAGENO}</p>
</htmlpagefooter>
</body>
</html>