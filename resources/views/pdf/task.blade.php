<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<meta charset="UTF-8">
	<title>Document</title>


	<link rel="stylesheet" href="{{ asset('/css/pdfStyles/law.css') }}">
</head>
<body>
	<main>
		<div class="clearfix">
			<h2 class="upperCase">Сумський державний університет</h2>
			<div class="subscriptionBlock clearfix">
				<p class="upperCase">Затверджую</p>
				<p>Голова приймальної комісії</p>
				<span>_______________</span>
				<p>____  ___________  20 ____ р.</p>
			</div>
		</div>
		<div class="title">
			<h3 class="upperCase">Екзаменаційне завдання</h3>
			<p>{{ $data['title'] }}</p>
			<p>{{ $data['purpose'] }}</p>
		</div>
		<p class="variant">Варіант № {{ $data['variant'] }}</p>
		@php($j = 1)
		@if(count($data['blocks']) > 0)
			@foreach($data['blocks'] as $block)
				<h3>@if($block->type == 1) Завдання {{ $loop->iteration }} @else Блок @endif {{$block->block_name}}<br>
					@if($block->task_title != null)
						<div class = "row"><b>
								{{$block->task_title}}
							</b>
						</div>
					@endif
				</h3>
				@if($block->type == 1)
					<div class = "row">
						@php($i = $j)
						@php($text=$block->blank_text)
						@while(strpos($text, '(XX)') !== false)
							@php ($text = preg_replace('/(XX)/', $i,$text, 1))
							@php($i++)
						@endwhile
						{{$text}}
					</div>
				@endif
				@if($block->url_image != "0")
					<div class="responsiveImg" style="background: url('{{ $block->url_image }}') no-repeat;"></div>
				@endif
				<ul class="listNone">
					@if( count($data['questions']) > 0)
						@foreach($data['questions'] as $idQuestion => $currQuestions)
							@if($idQuestion === $block->id)
								@foreach($currQuestions as $question)
									<li class="questionText">
										{{ $j }}. {!! $question->text_question !!}
										@php($j++)
									</li>
									<li class="bottomLine">
										<ul class="answersList">
											@foreach($question->answers as $answer)
												<li>
													<div>
														<p>{!! $answer->text_answer !!}</p>
													</div>
												</li>
											@endforeach
										</ul>
									</li>
								@endforeach
							@endif
						@endforeach
					@else
						<h3 class="textCenter">Питання до блоку відсутні</h3>
					@endif
				</ul>
			@endforeach
		@else
			<h3 class="textCenter">Блоки відсутні</h3>
		@endif
		<div class="clearfix signBlock">
			<div class="position">Відповідальний секретар приймальної комісії</div>
			<div style="float:right;width:30%;">
				<div class="subscribeBlock">(підпис)</div>
			</div>

		</div>

		<pagebreak></pagebreak>


		<div class="answerBlank">
			<h2 class="upperCase">Сумський державний університет</h2>
			<div class="subscriptionBlock">
				<p>Шифр {{ $data['number'] }}</p>
			</div>
		</div>
		<div class="title">
			<h3 class="upperCase">Аркуш відповіді</h3>
			<p>{{ $data['title'] }}</p>
			<p>{{ $data['purpose'] }}</p>
		</div>
		<p class="variant">Варіант № {{ $data['variant'] }}</p>
		<div>
			@php($i = 1)
			@php($maxRow = 0)
			@php($countAns = 0)
			@php($secondRow = 0)
			@php($secondMax = 0)
			@foreach($data['blocks'] as $blockId => $block)
				<div class="checkTable fLeft" style="width: {{ $block->tableNmb * 28 }}px;">
					<table>
						<tr>
							<th colspan="{{ $block->tableNmb }}">@if($data['type'] == 2) Task {{ $loop->iteration }} @else Блок @endif {{$block->block_name}}</th>
						</tr>

						@foreach($data['questions'] as $idQuestion => $currQuestions)
							@if($idQuestion === $block->id)
								@for($indexQuestion = 0; $indexQuestion <= count($currQuestions); $indexQuestion++)
									@php($countAns += count($currQuestions[0]->answers))
									@if(($countAns)> 18)
										@php($secondRow = 1)
									@endif
									<tr>
										@for($indexAnswer = 0; $indexAnswer <= count($currQuestions[0]->answers); $indexAnswer++)
											@if($indexQuestion === 0)
												@if($indexAnswer === 0)
													<td>№</td>
												@else
													<td>{{ $data['letters'][$indexAnswer - 1] }}</td>
												@endif
											@else
												@if($indexAnswer === 0)
													<td>{{ $i }}</td>
													@php($i++)
												@else
													<td></td>
												@endif
											@endif
										@endfor
									</tr>
								@endfor
							@endif
							@if($secondRow == 0)
								@if((count($currQuestions)) > $maxRow)
									@php($maxRow = (count($currQuestions)))
								@endif
							@else
								@if((count($currQuestions)) > $secondMax)
									@php($secondMax = (count($currQuestions)))
								@endif
							@endif
						@endforeach
					</table>
				</div>
			@endforeach
		</div>
		<div class="attentionBlock">
			<div class="fLeft attentionText">
				<p>
					<b>УВАГА!!! </b> Завдання мають кілька варіанів відповідей, серед яких лише один правильний.
					Виберіть правильний, на Вашу думку, варіант та позначте його, як показано на зразку.
					<b>Кількість виправлень вплиаває на загальну
						оцінку роботи!
					</b>
				</p>
			</div>
			<div class="exampleFill">
				<table>
					<tbody>
						<tr>
							<th>№</th>
							<th>a</th>
							<th>b</th>
							<th>c</th>
						</tr>
						<tr>
							<td>1</td>
							<td></td>
							<td>X</td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="clearfix">
			<div class="underLinedBlock">
				<p>Кількість правильних відповідей - </p>
			</div>
			<div class="underLinedBlock">
				<p>Кількість балів за них - </p>
			</div>
		</div>
		<div class="clearfix">
			<div class="underLinedBlock">
				<p>Кількість виправлень - </p>
			</div>
			<div class="underLinedBlock">
				<p>Знято балів за виправлення - </p>
			</div>
		</div>

		<div class="clearfix resultContainer">
			<div class="resultBlock titleBlock">Всього балів <br> з врахуванням знятих -</div>
			<div class="resultBlock">
				<p class="score">Оцінка - </p>
				<p class="scoreField">(числом та прописом)</p>
			</div>
		</div>

		@foreach($data['commissioners'] as $commissioner)
			<div class="clearfix signBlock">
				<div class="position">{{ $commissioner->commissioner_position }}</div>
				<div class="subscribeContainer">
					<div class="subscribeBlock">(підпис)</div>
				</div>
				<div class="initials">{{ $commissioner->commissioner_name }}</div>
			</div>
		@endforeach
	</main>
	<htmlpagefooter name="footer">
		<p class="documentNmb">Документ № {{ $data['cypter'] }} - згенеровано автоматично ES</p>
		<p class="nmbPage">{PAGENO}</p>
	</htmlpagefooter>
</body>
</html>
