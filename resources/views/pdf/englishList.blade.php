<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <meta charset="UTF-8">
    <title>Список завдань</title>

    <link rel="stylesheet" href="{{ asset('/css/pdfStyles/law.css') }}">
</head>
<body>
<main>
    <div class="clearfix">
        <h2 class="upperCase">Сумський державний університет</h2>
        <div class="subscriptionBlock clearfix">
            <p class="upperCase">Затверджую</p>
            <p>Голова приймальної комісії</p>
            <span>_______________</span>
            <p>____  ___________  20 ____ р.</p>
        </div>
    </div>

    <h3 class="upperCase">Список завдань</h3>
    @php($q=1)
    @foreach($data['english_blocks'] as $block)
        <h3> {{$block->block_name}}<br>
            <div class = "row">
                <b>
                    {{$block->task_title}}
                </b>
            </div>
        </h3>


        <ul class="listNone">
            @foreach ($data['english_config'] as $conf)
                @if($conf->block_id == $block->id)
                    @foreach ($block->english_text as $text)
                        @if($text->block_id == $block->id)
                            @if($conf->answer_list == 1 && $conf->text == 1 )
                                @php($j = 1)
                                @foreach ($text->english_questions as $question)
                                    @if($question->id_english_question == $text->id)
                                        <li class = "question_name" style="margin-left:10px">
                                            @if($conf->true_false == 0)
                                                {!! $question->right_answer !!}
                                            @else {!! $j !!}_____
                                            @php($j++)
                                            @endif {!!$question->text_question !!}

                                        </li>
                                    @endif
                                @endforeach
                                <li class = "question_name">
                                    @php($i = 1)
                                    @php($text=$text->text)
                                    @while(strpos($text, 'NNN') !== false)
                                        @php ($text = preg_replace('/NNN/', $i.'_____',$text, 1))
                                        @php($i++)
                                    @endwhile
                                    {!! $text !!}

                                </li>
                            @else
                                <li class = "question_name">
                                    @php($i = 1)
                                    @php($textie=$text->text)
                                    @while(strpos($textie, 'NNN') !== false)
                                        @php ($textie = preg_replace('/NNN/', $i.'_____',$textie, 1))
                                        @php($i++)
                                    @endwhile
                                    {!! $textie !!}
                                </li>
                                @php($j = 1)
                                @foreach ($text->english_questions as $question)
                                    @if($question->id_english_question == $text->id)
                                        <li class = "question_name">
                                            <div class = "answerEngNumb" > {!! $j !!} </div>
                                            @if($question->text_question != ' ')
                                                {!! $question->text_question !!}
                                                <br>
                                            @endif
                                            <ul class="listNone">
                                                @php($j++)
                                                @php($ans = "A")
                                                @foreach ($question->answers as $answer)
                                                        <li>
                                                            {!! $ans !!} {!! $answer->text_answer !!}
                                                            @php($ans++)
                                                        </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endif
                                @endforeach

                            @endif
                            <hr class = "line">
                        @endif

                    @endforeach


                    @if($conf->text == 0 && $conf->answer_list == 0)
                        @php($j = 1)
                        @foreach ($block->questions as $question)
                                <li class = "question_name">
                                    <div class = "answerEngNumb" > {!! $j !!} </div>
                                    @if($question->text_question != ' ')
                                        {!! $question->text_question !!}
                                        <br>
                                    @endif
                                    <ul class="listNone">
                                        @php($j++)
                                        @php($ans = "A")
                                        @foreach ($question->answers as $answer)
                                                <li>
                                                    {!! $ans !!} {!! $answer->text_answer !!}
                                                    @php($ans++)
                                                </li>
                                        @endforeach
                                    </ul>
                                </li>
                        @endforeach
                    @elseif($conf->text == 0 && $conf->answer_list == 1)
                        @foreach ($block->connections as $connection)
                            @if($connection->block_id == $block->id)
                                @php($j = 1)
                                @php($ans = "A")

                                <table class = "taskTable"><tr>
                                        @foreach ($connection->answers as $currAnswer)

                                            <td> {!! $currAnswer->text_question !!} </td>
                                        @endforeach
                                    </tr>
                                    <tr>
                                        @foreach ($connection->answers as $currAnswer)
                                            <th>{!! $ans !!}</th>
                                            @php($ans++)
                                        @endforeach
                                    </tr>
                                </table>
                                <li class = "question_name">
                                    @foreach ($connection->questions as $currQuestion)

                                        <div class = "englishAnsStep"> {!! $j !!} {!! $currQuestion->text_question !!} </div>
                                        @php($j++)
                                    @endforeach
                                </li>


                            @endif
                        @endforeach
                    @endif

                @endif
            @endforeach
        </ul>
    @endforeach
    <div class="clearfix signBlock">
        <div class="position">Голова приймальної комісії</div>
        <div style="float:right;width:30%;">
            <div class="subscribeBlock">(підпис)</div>
        </div>

    </div>
</main>
</body>
</html>