<header class="navbar navbar-default">
    <div class="header-top">
        <div class="container flex flex-space">
            <div class="header-width">
                <div>Система формування</div>
                <div>екзаменаційних завдань</div>
            </div>
            <div class="header-logo">
                &nbsp;
            </div>
            <div class="header-width">
                <span>Сумський державний</span>
                <h1>УНІВЕРСИТЕТ</h1>
            </div>
        </div>
    </div>
    <div class="collapse navbar-collapse header-bottom">
        <div class="container">
            <ul class="nav navbar-nav navbar-right">
                @if (config('locale.status') && count(config('locale.languages')) > 1)
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ trans('menus.language-picker.language') }}
                            <span class="caret"></span>
                        </a>

                        @include('includes.partials.lang')
                    </li>
                @endif


                    @php($admin = 0)
                @if ($logged_in_user)
                        @role('1')
                        @php($admin = 1)
                        <li>{{ link_to_route('admin.dashboard', trans('navs.frontend.user.administration')) }}</li>
                    @endauth

                    @role('2')
                        @if(Request::is('/') and $admin === 0)
                            <li>{{ link_to_route('frontend.omu.index', trans('navs.frontend.dashboard')) }}</li>
                        @endif
                    @endauth
                    @role('3')
                        @if(Request::is('/')and $admin === 0)
                            <li>{{ link_to_route('frontend.user.dashboard', trans('navs.frontend.dashboard')) }}</li>
                        @endif
                    @endauth

                @endif

                @if (! $logged_in_user)
                    <li>{{ link_to_route('frontend.auth.login', trans('navs.frontend.login')) }}</li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ $logged_in_user->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">

                            <li>{{ link_to_route('frontend.user.account', trans('navs.frontend.user.account')) }}</li>
                            <li>{{ link_to_route('frontend.auth.logout', trans('navs.general.logout')) }}</li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</header>