@extends('frontend.layouts.app')

@section('content')
    <div class="row">

        <div class="col-xs-12 ">

            <div class=" panel-default account-panel">
                <div class="account-heading">{{ trans('navs.frontend.user.account') }}</div>

                <div class="panel-body">

                    <div role="tabpanel">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active" id="li-profile">
                                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" class="tabs">{{ trans('navs.frontend.user.profile') }}</a>
                            </li>
                        </ul>

                        <div class="tab-content user-info-table">

                            <div role="tabpanel" class="tab-pane mt-30 active " id="profile">
                                @include('frontend.user.account.tabs.profile')
                            </div><!--tab panel profile-->

                            @include('frontend.user.account.upload-photo-modal')

                        </div><!--tab content-->

                    </div><!--tab panel-->

                </div><!--panel body-->

            </div><!-- panel -->

        </div><!-- col-xs-12 -->

    </div><!-- row -->
@endsection