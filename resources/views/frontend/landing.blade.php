@extends('frontend.layouts.app')



@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                        <div class="btn-group" role="group">
                            <button type="button" id = "dep" class="btn btn-primary btn-menu" data-toggle="tab" href="#panel1" >
                                Розробник завдань
                            </button>
                        </div>
                        <div class="btn-group" role="group">
                            <button type="button" id = "omu" class="btn btn-primary btn-custom" data-toggle="tab" href="#panel2" >Уповноважений представник ОМУ</button>
                        </div>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading"><label>Вітаємо Вас на сайті «Системи контролю екзаменаційних завдань» </label></div>
                    <div class="panel-body">
                        Система формування екзаменаційних завдань (СФЕЗ) призначена для формування екзаменаційних завдань.
                        У результаті роботи системи, буде сформована задана кількість унікальних варіантів методом вибірки
                        із загальної бази завдань певної кафедри, після погодження цієї бази уповноваженим представником ОМУ.
                    </div>
                </div>
                <div class="tab-content buttom_css custom">
                    <div id="panel1" class="tab-pane fade in active">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <label >Інструкція розробника завдань:</label>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li>зазначає назву блоку</li>
                                    <li>зазначає вагу кожного завдання (кількість балів а окреме завдання
                                        або певного блоку) а також кількість завдань з яких буде формуватись блок;
                                    </li>
                                    <li>
                                        системою передбачено обмеження на роботу з кожним блоком лише однією особою;
                                    </li>
                                    <li>
                                        для завдань кожної спеціальності зазначається прізвище ім'я побатькові та посада
                                        голови екзаменаційної (фахової, атестаційної тощо) комісії, членів комісії
                                        згідно з відповідним наказом;
                                    </li>
                                    <li>
                                        розробник завдань відповідає за правильність заповнення та унікальність завдань.
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div id="panel2" class="tab-pane fade">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <label >Інструкція для роботи з системою уповноваженому представнику ОМУ:</label>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li> уповноважений представник ОМУ верифікує банк даних екзаменаційних завдань
                                        спеціальності (БД) після отримання підпису
                                        начальника ОМУ та проректора з  навчально-педагогічної роботи;
                                    </li>
                                    <li>верифікує після затверджеення в установленому порядку та звіряє версії документів</li>
                                    <li>
                                        після затвердження БД у встановленому порядку уповноважена особа ОМУ контролює
                                        відповідність версій електронного і друкованого документів
                                        та верифікує електронний документ;
                                    </li>
                                    <li>
                                        після верифікації доступ до модифікації електронного документу блокується;
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
            </div>

        </div>
    </div>
@endsection