@extends('frontend.layouts.app')

@section('after-styles')
	{{ Html::style('css/omu.css') }}
	{{ Html::style('css/edit-blank.css') }}
@endsection

@section('content')
	<div class="row" id="all">

		<div class="col-xs-12">

			<div class="panel panel-default">
				<div class="panel-heading">{{ trans('navs.frontend.dashboard_omu') }}</div>
				<div class="control-buttons-block">
					<div class="btn-group btn-group-justified" role="group" aria-label="..." >
						<div class="btn-group" role="group">
							@if($logged_in_user->id_cafedres==null)
								<a href="/omu" type="button" class="btn btn-info">Повернутися</a>
							@endif
						</div>
					</div>
				</div>
				<div class="panel-body">
					<input id="id_speciality" type="hidden" value="{{$data['idSpeciality']}}" />
					<input id="type_speciality" type="hidden" value="{{$data['type']}}" />
					@if($data['type'] != 1)
						@foreach ($data['blocks'] as $block)

							@if($block->id_speciality ==  $data['idSpeciality'])
								<div class="verifyTableBlock editBlockOmu task-block">
									<ol class="block_name">Назва блоку
										<input class="sel_block blockName" value='{{$block->block_name}}' data-id="{{$block->id}}" data-prev-content="{{$block->block_name}}">
										@if($data['type'] == 2)
											@if($block['type']==1)
												<ul> <textarea class="sel_block blankText text_area_edit"  style="font-weight: normal;" maxlength="1000">{{$block['blank_text']}}</textarea></ul>
												<ul>Текст завдання: <input class="sel_block taskTitle" value="{{$block['task_title']}}"></ul>
											@else
												<ul><input class="hidden blankText" value=''></ul>
											@endif
										@endif
										@php($questionCounter=1)
										@foreach ($data['questions'] as $question)
											@if($question->id_block == $block->id)
												<li class="question_name">Питання №<span  class="questId" >{{ $questionCounter }}:</span>
													<div class="imageWidthContainer im_regculation" data-prev-width="{{ $question->image_width }}">
														<div class="row col-xs-10">
															<span>Вкажіть ширину зображень:</span>
															@foreach($data['imagesWidthChoices'] as $widthChoice)
																<label><input
																			type="radio"
																			name="image-width-radio-{{$question->id}}"
																			value="{{$widthChoice}}"
																			@if( $question->image_width === 'auto') disabled @endif
																			@if($widthChoice  == $question->image_width)
																			checked
																			@endif
																	>
																	{{$widthChoice}}px
																</label>
															@endforeach
															<label>
																<input
																		type="radio"
																		class="customWithRadio"
																		value="custom"
																		name="image-width-radio-{{$question->id}}"
																		aria-label="image-with"
																		@if( $question->image_width === 'auto') disabled @endif
																		@if(!in_array($question->image_width, $data['imagesWidthChoices']))
																		checked
																		@endif
																>
															</label>
															<input
																	type="number"
																	class="form-control customWidthInput"
																	aria-label="custom-image-with"
																	min="10"
																	max="350"
																	@if( $question->image_width === 'auto') disabled @endif
																	@if(!in_array($question->image_width, $data['imagesWidthChoices']))
																	value="{{$question->image_width}}"
																	@else
																	disabled
																	@endif
															>
															<button
																	class="changeWidthBtn btn btn-info"
																	@if(in_array($question->image_width, $data['imagesWidthChoices'])) disabled @endif
																	@if( $question->image_width === 'auto') disabled @endif
															>
																Змінити
															</button>
															<button
																	class="enableAutoWidth btn {{ $question->image_width === 'auto' ? 'btn-success' : 'btn-danger' }}"
															>
																Auto/{{ $question->image_width === 'auto' ? 'on' : 'off' }}
															</button>
														</div>
													</div>
													@php($questionCounter++)
													<div>
														<div class="popupImageConvertor">
															<div class="popup-container">
																<h3 class="resize-title">Вставка зображення</h3>
																<div class="resizer" >
																	<span class="addition">ctrl+v</span>
																</div>
																<button class="resizer-result" class="btn btn-info">Отримати зображення</button>
																<button class="closePopup">&#10006;</button>
															</div>
														</div>
														<textarea class="multiblock questionContent" data-id="{{$question->id}}" data-prev-content="{{$question->text_question}}">{!! $question->text_question !!}</textarea>
													</div>
													<ul class="block_name">
														@php($answerCounter=1)

														@foreach ($data['answers'] as $answer)
															@if($answer->id_question == $question->id)
																<div class="row " style="margin:10px  0;">
																	<div class="answerDiv" >
																		<li class="answer">
																			Варіант відповіді №<span class="ansId" >{{$answerCounter}}:</span>
																			@php($answerCounter++)
																			<div>
																				<div class="popupImageConvertor">
																					<div class="popup-container">
																						<h3 class="resize-title">Вставка зображення</h3>
																						<div class="resizer" >
																							<span class="addition">ctrl+v</span>
																						</div>
																						<button class="btn btn-info resizer-result">Отримати зображення</button>
																						<button class="closePopup">&#10006;</button>
																					</div>
																				</div>
																				<textarea class="multiblock answerContent" data-id="{{ $answer->id }}" data-prev-content="{{$answer->text_answer}}">{!! $answer->text_answer !!}</textarea>
																			</div>
																		</li>
																	</div>
																</div>

															@endif
														@endforeach
													</ul>
													<hr class="line">

											@endif
										@endforeach
									</ol>
								</div>
							@endif

						@endforeach
					@else
						@foreach ($data['blocks'] as $block)
							@if($block->id_speciality ==  $data['idSpeciality'])
								<div class="verifyTableBlock editBlockOmu col-xs-12 task-block">
									<ol>
										<label>Текст завдання </label>
										<input class="sel_block blockName" value='{{$block->block_name}}' data-id="{{ $block->id }}" data-prev-content="{{ $block->block_name }}">
										@foreach ($data['questions'] as $question)
											@if($question->id_block == $block->id)
												<li class="question_name">
													<div class="imageWidthContainer im_regculation" data-prev-width="{{ $question->image_width }}">
														<div class="row col-xs-10">
															<span>Вкажіть ширину зображень:</span>
															@foreach($data['imagesWidthChoices'] as $widthChoice)
																<label><input
																			type="radio"
																			name="image-width-radio-{{$question->id}}"
																			value="{{$widthChoice}}"
																			@if( $question->image_width === 'auto') disabled @endif
																			@if($widthChoice  == $question->image_width)
																			checked
																			@endif
																	>
																	{{$widthChoice}}px
																</label>
															@endforeach
															<label><input
																		type="radio"
																		class="customWithRadio"
																		value="custom"
																		name="image-width-radio-{{$question->id}}"
																		aria-label="image-with"
																		@if( $question->image_width === 'auto') disabled @endif
																		@if(!in_array($question->image_width, $data['imagesWidthChoices']))
																		checked
																		@endif
																></label>
															<input
																	type="number"
																	class="form-control customWidthInput"
																	aria-label="custom-image-with"
																	min="10"
																	max="350"
																	@if( $question->image_width === 'auto') disabled @endif
																	@if(!in_array($question->image_width, $data['imagesWidthChoices']))
																	value="{{$question->image_width}}"
																	@else
																	disabled
																	@endif

															>
															<button class="changeWidthBtn btn btn-info"
																	@if(in_array($question->image_width, $data['imagesWidthChoices'])) disabled @endif
																	@if( $question->image_width === 'auto') disabled @endif
															>
																Змінити
															</button>
															<button
																	class="enableAutoWidth btn {{ $question->image_width === 'auto' ? 'btn-success' : 'btn-danger' }}"
															>
																Auto/{{ $question->image_width === 'auto' ? 'on' : 'off' }}
															</button>
														</div>
													</div>

													<div class="container question-answer-block-ua">
                                                    <span class="col-xs-6">
                                                        <label>Питання</label>
															<div class="popupImageConvertor">
																<div class="popup-container">
																	<h3 class="resize-title">Вставка зображення</h3>
																	<div class="resizer" >
																		<span class="addition">ctrl+v</span>
																	</div>
																	<button class="resizer-result" class="btn btn-info">Отримати зображення</button>
																	<button class="closePopup">&#10006;</button>
																</div>
															</div>
															<textarea class="sel_block questionContent text_area_edit multiblock" maxlength="1000" data-id="{{$question->id}}" data-prev-content="{{$question->text_question}}">{{$question->text_question}}</textarea></span>

														@foreach ($data['answers'] as $answer)
															@if($answer->id_question == $question->id)
																<span class="col-xs-6">
																<label>Відповідь</label>
																<div class="popupImageConvertor">
																	<div class="popup-container">
																		<h3 class="resize-title">Вставка зображення</h3>
																		<div class="resizer" >
																			<span class="addition">ctrl+v</span>
																		</div>
																		<button class="resizer-result" class="btn btn-info">Отримати зображення</button>
																		<button class="closePopup">&#10006;</button>
																	</div>
																</div>
																<textarea class="sel_block answerContent text_area_edit multiblock"  maxlength="1000" data-id="{{$answer->id}}" data-prev-content="{{$answer->text_answer}}">{{$answer->text_answer}}</textarea></span>

															@endif
														@endforeach
														<hr class="line" />
													</div>

												</li>
											@endif
										@endforeach
									</ol>
								</div>
							@endif
						@endforeach
					@endif
						<div class="verifyTableBlock col-xs-12" id="comm">  <label>Члени комісії </label>
							@foreach ($data['comms'] as $comm)
								<div class="category-block" style="display: block;" data-id="{{$comm->id}}">
									<div class="row" style="margin-bottom: 5px">
										<div class="col-xs-12 form-inline" style="width: 100%">
											<input class="item-category form-control commPosition" style="width: 45%" type="text" required placeholder="Посада члена комісії" value="{{$comm->commissioner_position}}">
											<input class="item-category form-control commName" style="width: 45%" type="text" required placeholder="ПІБ члена комісії" value="{{$comm->commissioner_name}}">
											<span  class="category-add-comm" data-toggle="tooltip" title="Додати члена комісії">
	                                                <button type="button"  class="btn btn-default">
	                                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
	                                                </button>
											</span>
											<span class="category-delete-comm" data-toggle="tooltip" title="Видалити члена комісії" style="display: none;">
	                                                <button type="button" class="btn btn-default">
	                                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
	                                                </button>
	                                            </span>
										</div>
									</div>
								</div>
							@endforeach
							@if($data['comms'] == '[]')
								<div class="category-block">
									<div class="row" style="margin-bottom: 5px">
										<div class="col-xs-12 form-inline" style="width: 100%">
											<input class="item-category form-control commPosition" style="width: 45%" type="text" required placeholder="Посада члена комісії">
											<input class="item-category form-control commName" style="width: 45%" type="text" required placeholder="ПІБ члена комісії">
											<span  class="category-add-comm" data-toggle="tooltip" title="Додати члена комісії">
	                                            <button type="button"  class="btn btn-default">
	                                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
	                                            </button>
	                                        </span>
											<span class="category-delete-comm" data-toggle="tooltip" title="Видалити члена комісії" style="display: none;">
	                                            <button type="button" class="btn btn-default">
	                                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
	                                            </button>
	                                        </span>
										</div>
									</div>
								</div>
							@endif
						</div>
				</div>
				<div class="control-buttons-block">
					<div id="statuses" class="btn-group btn-group-justified" role="group" aria-label="...">
						<div class="btn-group" role="group">
							<button type="button" class="btn btn-success">Підтвердити</button>
						</div>
					</div>
					<br>
					<div id="back" class="btn-group btn-group-justified" role="group" aria-label="...">
						<div class="btn-group" role="group">
							<a href="/omu" type="button" class="btn btn-info">Повернутися</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="bookshelf_wrapper" style="display:none">
		<ul class="books_list">
			<li class="book_item first"></li>
			<li class="book_item second"></li>
			<li class="book_item third"></li>
			<li class="book_item fourth"></li>
			<li class="book_item fifth"></li>
			<li class="book_item sixth"></li>
		</ul>
		<div class="shelf"></div>
	</div>
	<div class="hideBehind" style="display:none"></div>

@endsection
@section('after-scripts')
	{{ Html::script(mix('js/pages/edit-blank.js')) }}
@endsection