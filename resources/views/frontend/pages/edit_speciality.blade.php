@extends('frontend.layouts.app')

@section('after-styles')
{{ Html::style('css/omu.css') }}
@endsection

@section('content')
    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-default">
                <div class="omu-block">Представник ОМУ</div>

                <div class="padding-block">

                    <div class="">
                        <div class="row">
                            <div class="col-xs-12 shadow-block padding-block">
                                <div class="padding-body">

                                    @if($data['type']==1)
                                        <label>Кафедра {{$data['depName']}}</label>

                                        <input  id = "dep" value='{{$data['idDep']}}' class = "hidden">
                                                @endif
                                    <div>Назва спеціальності*</div>
                                    <div><input id = "name" class = "sel_block" value = '{{$data['name']}}'></div>
                                    <div>Номер*</div>
                                    <div><input id = "number" class = "sel_block" value = '{{$data['number']}}'></div>
                                    @if($data['type']!=1)
                                    <div>Кафедра*</div>
                                    <div>   <select id = "dep" class=" sel_block">
                                            @foreach ($data['cafedres'] as $cafedre)
                                                @if($cafedre->id == $data['depId'])
                                                    <option class="blacked" selected value={{ $cafedre->id }}>{{ $cafedre->name_cafedres }}</option>
                                                @else
                                                    <option class="blacked" value={{ $cafedre->id }}>{{ $cafedre->name_cafedres }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    @else
                                    <div>Тип спеціальності (зверніть увагу на те, що тип не можна буде змінити після створення)</div>
                                    <div>
                                        <select id = "typeSpec" class=" sel_block">
                                            <option selected value=0>Звичайна</option>
                                            <option value=1>Українська (текст)</option>
                                            <option value = 2>English</option>
                                        </select>
                                    </div>
                                    @endif
                                    <div>Заголовок бланку (наприклад: фахового вступного випробування при прийомі на перший курс ...)*</div>
                                    <div><input id = "title" class = "sel_block" value = '{{$data['title']}}'></div>
                                    <div>Призначення бланку (наприклад: для здобуття ступеня "баклавр" зі спеціальності ...)*</div>
                                    <div><input id = "reason" class = "sel_block" value = '{{$data['reason']}}'></div>

                                    <div id = "saving" class="btn-group btn-group-justified" role="group" aria-label="...">
                                        <div class="btn-group spec" role="group">
                                            <button type="button" class="btn save-btn" data-id="{{$data['id']}}">Зберегти зміни</button>
                                        </div>
                                    </div>
                                    <div class="btn-group return-block" role="group">
                                        <a href="/omu" type="button" class="btn return-btn">Повернутись назад</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('after-scripts')
    {{ Html::script(mix('js/pages/edit-speciality.js')) }}
@endsection
