@extends('frontend.layouts.app')

@section('after-styles')
    {{ Html::style('css/omu.css') }}
@endsection

@section('content')

    <div class="row" id = "all">

        <div class="col-xs-12">

            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('navs.frontend.dashboard_omu') }}</div>
                                
                    <div class="panel-default" id="view_task">
                        <div class="panel-body">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#panel1">Створення кафедр та спеціальностей</a></li>
                                <li><a data-toggle="tab" href="#panel2">Робота з користувачами </a></li>
                                <li><a data-toggle="tab" href="#panel3">Верифікація спеціальностей</a></li>
                                <li><a data-toggle="tab" href="#panel4">Перегляд згенерованих бланків</a></li>
                            </ul>

                            <div class="tab-content buttom_css">
                                <div id="panel1" class="tab-pane fade in active shadow-block">

                                    <table id = "depTable" class = "repTable createTable">
                                        @if(sizeof($data['cafedres']) == 0 )
                                            <tr><td> Кафедри відсутні</td></tr>
                                        @else
                                            <tr>
                                                <th>Назва кафедри</th>
                                            </tr>
                                        @endif
                                        @foreach ($data['cafedres'] as $cafedre)
                                            <tr>
                                                <td><div >{{ $cafedre->name_cafedres }}</div>
                                                    <input class=" sel_block hideSelect">
                                                </td>
                                                <td class = tableButtons>
                                                     <span class="category-pencil" data-toggle="tooltip" title = "Редагувати кафедру" >
                                                             <a href="/edit-department/{{ $cafedre->id}}" type= "button" class="btn btn-default">
                                                                <span class="glyphicon glyphicon-pencil" aria-hidden="true" ></span>
                                                                <span style = "display:none;" class="glyphicon glyphicon-floppy-disk" aria-hidden="true" ></span>
                                                            </a>
                                                     </span>
                                                    <span class="category-trash r_margin" data-toggle="tooltip" title="Видалити кафедру">
                                                        <button type="button" class="btn btn-default">
                                                            <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                                                        </button>
                                                    </span>
                                                </td>
                                                <td style = "display:none" class = "depart_id">{{ $cafedre->id }}</td>
                                            </tr>
                                        @endforeach
                                        <tr class="omu_add1">
                                            <td>
                                                <div>+ Додати кафедру</div>
                                            </td>
                                        </tr>
                                    </table>

                                    <label>Оберіть кафедру</label>
                                    <select id="depSelect" class="sel_block w-95" >
                                        @foreach ($data['cafedres'] as $cafedre)
                                            <option class="blacked" value={{ $cafedre->id }}>{{ $cafedre->name_cafedres }}</option>
                                        @endforeach
                                    </select>


                                    <label id = "selected_spec_name"></label>

                                    <table id = "specTable" class = "repTable createTable">

                                    </table>

                                </div>
                                <div id="panel2" class="tab-pane fade ">
                                    <div class="verifyTableBlock">
                                        <table id = "repTable" class = "repTable">
                                        </table>
                                        <div class="btn-group btn-group-justified button-add-user" role="group" aria-label="...">
                                        <div class="btn-group" role="group">
                                            <a href="/edit-representative/0" type="button" id = "specAdd" class="btn btn-default">Додати користувача</a>
                                        </div>
                                    </div>
                                    </div>


                                </div>
                                <div id="panel3" class="tab-pane fade">
                                    <div class="verifyTableBlock panel-body">
                                        <table id = "verifyTable" class = "repTable">

                                        </table>
                                    </div>
                                </div>


                                <div id="panel4" class="tab-pane fade">
                                    <div class="verifyTableBlock">
                                        <table id="variant-blank-table" class="repTable">
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--col-md-8-->
            </div><!-- panel -->
        </div><!-- col-md-10 -->

    <div id="showPdf" class="blurContainer">
        <div id="pdf_set" class="pdfPopUp">
            <div id="pdfMenu" class="pdfmenu">
                <div id="x">
                    X
                </div>
            </div>
            <div id="generateBlock">
                <div class="mid">
                    <label for="variants"> Введіть кількість варіантів: </label>
                    <input id="variants" type="number" min="0" max="30" class="inp"></div>
                <div class="mid">
                    <button class="btn btn-default" id="generatePdf">Згенерувати</button>
                </div>
            </div>
            <div class="mid">
                <button type="button" id="resetTasks" class="btn hidden clearBtn" >Очистити історію вже згенерованих питань</button>
            </div>
        </div>
    </div>
    <div id="confirmContainer" class="blurContainer">
        <div id="confirm" class="confirmPopup"></div>
    </div>

    <div id="listBlankPopup" class="popup hidden">
        <div class="outer" id="close-btn">
                <div class="inner">
                    <label>Back</label>
                </div>
            </div>
        <table class="table table-striped listBlanks">
            <thead>
                <tr>
                    <th class="not-white">№ варіанту</th>
                    <th class="not-white" colspan="4">Дії</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <div id="nmbToDownloadBlank" class="popup hidden">
        <form class="nmbBlanksForm">
            <label>К-ть копій варіантів:</label>
            <input class="form-control" type="number" min="1" max="30">
            <button class="btn btn-default btn-info" type="submit">Завантажити</button>
        </form>
    </div>

    <div id="blur-screen" class="hidden">
    </div>
    <iframe id="pdf-frame" class="hidden"></iframe>
    <div class="bookshelf_wrapper" >
        <ul class="books_list">
            <li class="book_item first"></li>
            <li class="book_item second"></li>
            <li class="book_item third"></li>
            <li class="book_item fourth"></li>
            <li class="book_item fifth"></li>
            <li class="book_item sixth"></li>
        </ul>
        <div class="shelf"></div>
    </div>
    <div class = "hideBehind" ></div>
    <script>
    </script>
@endsection



@section('after-scripts')
    {{ Html::script(mix('js/pages/omu.js')) }}
@endsection