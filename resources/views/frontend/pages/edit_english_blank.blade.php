@extends('frontend.layouts.app')

@section('after-styles')
	{{ Html::style('css/omu.css') }}
	{{ Html::style('css/edit-blank.css') }}
	{{ Html::style('css/representative.css') }}
@endsection

@section('content')
	<div id="edit-container all" class="row" data-id-speciality="{{$data['idSpeciality']}}">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">{{ trans('navs.frontend.dashboard_omu') }}</div>
				<div class="control-buttons-block">
					<div class="btn-group btn-group-justified" role="group" aria-label="back">
						<div class="btn-group" role="group">
							@if($logged_in_user->id_cafedres==null)
								<a href="/omu" type="button" class="btn btn-info">Повернутися</a>
							@endif
						</div>
					</div>
				</div>
				<div class="panel-body">
					@php($choices = $data['imagesWidthChoices'])
					@if(count($data['blocks']) > 0)
						<form action="">
							@foreach($data['blocks'] as $block)
								<div class="verifyTableBlock editBlockOmu col-xs-12 task-block">
									<ol class="block_name">{{$block['block_name']}} <br>
										Умова завдання:  <br>
										{{$block['block_title']}}
                                        @php($settings = $block['settings'])

								@php($title = $block['title'])
								@php($questions = $block['questions'])
								@php($answers = $block['answers'])
								@php($connections = $block['connections'])
								@if($settings->answer_list)
									@if($settings->text)
										@php($i = 0)
										@foreach($questions as $question)
											@php($imageWidth = $question->image_width)
												<li class="question_name">
													<div class="text-container" data-id="{{$question->id}}" data-prev-value='{{ $question->text }}'>
														<h2>Текст до питання:</h2>
														<div class="imageWidthContainer im_regculation" data-prev-image-width="{{$imageWidth}}">
															<div class="row col-xs-12">
																<span>Вкажіть ширину зображень:</span>
																@foreach($data['imagesWidthChoices'] as $widthChoice)
																	<label><input
																				type="radio"
																				name="image-width-radio-{{$question->id}}"
																				value="{{$widthChoice}}"
																				@if( $question->image_width === 'auto') disabled @endif
																				@if($widthChoice  == $question->image_width)
																				checked
																				@endif
																		>
																		{{$widthChoice}}px
																	</label>
																@endforeach
																<label>
																	<input
																			type="radio"
																			class="customWithRadio"
																			value="custom"
																			name="image-width-radio-{{$question->id}}"
																			aria-label="image-with"
																			@if( $question->image_width === 'auto') disabled @endif
																			@if(!in_array($question->image_width, $data['imagesWidthChoices']))
																			checked
																			@endif
																	>
																</label>
																<input
																		type="number"
																		class="form-control customWidthInput"
																		aria-label="custom-image-with"
																		min="10"
																		max="350"
																		@if( $question->image_width === 'auto') disabled @endif
																		@if(!in_array($question->image_width, $data['imagesWidthChoices']))
																		value="{{$question->image_width}}"
																		@else
																		disabled
																		@endif
																>
																<button
																		class="changeWidthBtn btn btn-info"
																		@if(in_array($question->image_width, $data['imagesWidthChoices'])) disabled @endif
																		@if( $question->image_width === 'auto') disabled @endif
																>
																	Змінити
																</button>
																<button
																		class="enableAutoWidth btn {{ $question->image_width === 'auto' ? 'btn-success' : 'btn-danger' }}"
																>
																	Auto/{{ $question->image_width === 'auto' ? 'on' : 'off' }}
																</button>
															</div>
														</div>
														<div class="row" style="margin-bottom: 5px">
															<div class="col-xs-12 form-inline" style="width: 100%">
																<div class="popupImageConvertor">
																	<div class="popup-container">
																		<h3 class="resize-title">Вставка зображення</h3>
																		<div class="resizer" >
																			<span class="addition">ctrl+v</span>
																		</div>
																		<button class="btn btn-info resizer-result">Отримати зображення</button>
																		<button class="closePopup">&#10006;</button>
																	</div>
																</div>
																<textarea class="multiblock text-question answerContent">{!! $question->text !!}</textarea>
															</div>
														</div>
													</div>
													<div class="list-container question_name">
														@if(count($answers[$i]) > 0)
															<table>
																<thead>
																<tr>
																	<th>№</th>
																	<th>Текст питання</th>
																	<th class="change-content-col"></th>
																</tr>
																</thead>
																<tbody>
																	@if($settings->true_false)
																		@php($temp = 0)
																		@foreach($answers[$i] as $answer)
																			<tr data-id="{{ $answer->id }}">
																				<td class="index-col">{{++$temp}}</td>
																				<td class="textCol" data-prev-value="{{$answer->text_question}}">
																					<input type="text" maxlength="100" value="{{$answer->text_question}}" required>
																				</td>
																				<td class="positionCol" data-prev-value="{{$answer->position}}">
																					<select>
																						<option value="1" {{$answer->position == 1 ? 'selected="selected"' : ''}}>Так</option>
																						<option value="0" {{$answer->position == 0 ? 'selected="selected"' : ''}}>Ні</option>
																					</select>
																				</td>
																			</tr>
																		@endforeach
																	@else
																		@php($temp = 'A')
																		@foreach($answers[$i] as $answer)
																			<tr data-id="{{ $answer->id }}">
																				<td class="index-col">{{$temp++}}</td>
																				<td class="textCol" data-prev-value="{{$answer->text_question}}">
																					<input type="text" maxlength="100" value="{{$answer->text_question}}" required>
																				</td>
																				<td class="positionCol" data-prev-value="{{$answer->position}}">
																					<select data-previous-val={{$answer->position}}>
																						@foreach($answers[$i] as $currAnswer)
																							<option
																									value="{{$currAnswer->position}}"
																									{{$currAnswer->position == $answer->position ? 'selected="selected"' : ''}}
																							>
																								{{str_replace('extra-answer', 'Додаткове питання', $currAnswer->position)}}
																							</option>
																						@endforeach
																					</select>
																				</td>
																			</tr>
																		@endforeach
																	@endif
															</tbody>
														</table>
													@endif
												</div>
											</li>
										@php($i++)
										@endforeach
									@else
										@for($z = 0; $z < sizeof($connections); $z++)
											@php($temp = 'A')

											<div class="list-with-answers-block question_name">
												<div class="list-container ">
													<table>
														<thead>
														<tr>
															<th>№</th>
															<th>Варіант</th>
														</tr>
														</thead>
														<tbody>
															@if(count($questions[$z]) > 0)
																@foreach($questions[$z] as $question)
																	<tr data-id="{{$question->id}}">
																		<td>{{$temp++}}</td>
																		<td class="textCol" data-prev-value="{{$question->text_question}}">
																			<input type="text" maxlength="100" value="{{$question->text_question}}" required>
																		</td>
																	</tr>
																@endforeach
															@else
																<tr>
																	<td colspan="3">Дані відсутні</td>
																</tr>
															@endif
														</tbody>
													</table>
												</div>
												<div class="list-answers-container ">
													<table>
														<thead>
														<tr>
															<th>№</th>
															<th>Текст питання</th>
															<th class="change-content-col">Відповідний номер в таблиці</th>
														</tr>
														</thead>
														<tbody>
															@if(count($answers[$z]) > 0)
																@php($temp = 0)
																@foreach($answers[$z] as $answer)
																	<tr data-id="{{$answer->id}}" >
																		<td class="indexCol">{{++$temp}}</td>
																		<td class="textCol" data-prev-value="{{$answer->text_question}}">
																			<input type="text" maxlength="100" value="{{$answer->text_question}}" required>
																		</td>
																		<td class="positionCol" data-prev-value="{{$answer->right_answer}}">

																			<select>
																				@for($j = 1, $letter = 'A'; $j <= $block['count']; $j++, $letter++)
																					<option value="{{$letter}}" {{$answer->position == $letter ? 'selected="selected"' : ''}}>{{$letter}}</option>
																				@endfor
																			</select>
																		</td>
																	</tr>
																@endforeach
															@else
																<tr>
																	<td colspan="3">
																		Варіанти відповідей відсутні
																	</td>
																</tr>
															@endif
														</tbody>
													</table>
												</div>
											</div>
										@endfor
									@endif
								@elseif($settings->text)
									@php($i = 0)
									@foreach($title as $one)
										@php($imageWidth = $one->image_width)
										<li class="text-with-question question_name">
											<div class="text-container" data-id="{{$question->id}}" data-prev-value='{!! $one->text !!}'>
												<h2>Текст до питання:</h2>
												<div class="imageWidthContainer im_regculation" data-prev-image-width="{{$imageWidth}}">
													<div class="row col-xs-12">
														<span>Вкажіть ширину зображень:</span>
														<label>
															<input
																	type="radio"
																	name="image-width-radio-title-{{$one->id}}"
																	value="50"
																	{{$imageWidth == 50 ? 'checked' : ''}}
																	@if( $imageWidth === 'auto') disabled @endif
															>
															50px
														</label>
														<label>
															<input
																	type="radio"
																	name="image-width-radio-title-{{$one->id}}"
																	value="75" {{$imageWidth == 75 ? 'checked' : ''}}
																	@if( $imageWidth === 'auto') disabled @endif
															>
															75px
														</label>
														<label>
															<input
																	type="radio"
																	name="image-width-radio-title-{{$one->id}}"
																	value="100" {{$imageWidth == 100 ? 'checked' : ''}}
																	@if( $imageWidth === 'auto') disabled @endif
															>
															100px
														</label>
														<label>
															<input
																	type="radio"
																	name="image-width-radio-title-{{$one->id}}"
																	value="125"
																	{{$imageWidth == 125 ? 'checked' : ''}}
																	@if( $imageWidth === 'auto') disabled @endif
															>
															125px
														</label>
														<label>
															<input
																	type="radio"
																	name="image-width-radio-title-{{$one->id}}"
																	value="150"
																	{{$imageWidth == 150 ? 'checked' : ''}}
																	@if( $imageWidth === 'auto') disabled @endif
															>
															150px
														</label>
														<label>
															<input
																type="radio"
																class="customWithRadio"
																value="custom"
																name="image-width-radio-title-{{$one->id}}"
																aria-label="image-width"
																{{in_array($imageWidth, $choices) ? '' : 'checked'}}
																@if( $imageWidth === 'auto') disabled @endif
															>
														</label>
														<input
															type="number"
															class="form-control customWidthInput"
															aria-label="custom-image-with"
															min="10"
															max="350"
															{{in_array($imageWidth, $choices) ? 'disabled' : ''}}
															value={{in_array($imageWidth, $choices) ? '' : $imageWidth}}
															@if( $imageWidth === 'auto') disabled @endif

														>
														<button
															class="changeWidthBtn btn btn-info"
															{{in_array($imageWidth, $choices) ? 'disabled' : ''}}
															@if( $imageWidth === 'auto') disabled @endif
														>
															Змінити
														</button>
														<button
																class="enableAutoWidth btn {{ $imageWidth === 'auto' ? 'btn-success' : 'btn-danger' }}"
														>
															Auto/{{ $imageWidth === 'auto' ? 'on' : 'off' }}
														</button>
													</div>
												</div>
												<div class="row" style="margin-bottom: 5px">
													<div class="col-xs-12 form-inline" style="width: 100%">
														<div class="popupImageConvertor">
															<div class="popup-container">
																<h3 class="resize-title">Вставка зображення</h3>
																<div class="resizer" >
																	<span class="addition">ctrl+v</span>
																</div>
																<button class="btn btn-info resizer-result">Отримати зображення</button>
																<button class="closePopup">&#10006;</button>
															</div>
														</div>
														<textarea class="multiblock text-question answerContent" data-id={{$one->id}}>{{$one->text}}</textarea>
													</div>
												</div>
											</div>
											<div class="questions-container">
												@php($ans = 0)
												@foreach($questions[$i] as $question)
													@php($imageWidth = $question->image_width)
													<div class="category-block question_name" data-question-id="{{$question->id}}" data-prev-question-value='{!! $question->text_question !!}'>
														<div class="imageWidthContainer im_regculation" data-prev-image-width="{{$imageWidth}}">
															<div class="row col-xs-12">
																<span>Вкажіть ширину зображень:</span>
																@foreach($data['imagesWidthChoices'] as $widthChoice)
																	<label><input
																				type="radio"
																				name="image-width-radio-{{$question->id}}"
																				value="{{$widthChoice}}"
																				@if( $question->image_width === 'auto') disabled @endif
																				@if($widthChoice  == $question->image_width)
																				checked
																				@endif
																		>
																		{{$widthChoice}}px
																	</label>
																@endforeach
																<label>
																	<input
																			type="radio"
																			class="customWithRadio"
																			value="custom"
																			name="image-width-radio-{{$question->id}}"
																			aria-label="image-with"
																			@if( $question->image_width === 'auto') disabled @endif
																			@if(!in_array($question->image_width, $data['imagesWidthChoices']))
																			checked
																			@endif
																	>
																</label>
																<input
																		type="number"
																		class="form-control customWidthInput"
																		aria-label="custom-image-with"
																		min="10"
																		max="350"
																		@if( $question->image_width === 'auto') disabled @endif
																		@if(!in_array($question->image_width, $data['imagesWidthChoices']))
																		value="{{$question->image_width}}"
																		@else
																		disabled
																		@endif
																>
																<button
																		class="changeWidthBtn btn btn-info"
																		@if(in_array($question->image_width, $data['imagesWidthChoices'])) disabled @endif
																		@if( $question->image_width === 'auto') disabled @endif
																>
																	Змінити
																</button>
																<button
																		class="enableAutoWidth btn {{ $question->image_width === 'auto' ? 'btn-success' : 'btn-danger' }}"
																>
																	Auto/{{ $question->image_width === 'auto' ? 'on' : 'off' }}
																</button>
															</div>
														</div>
														<div class="row category" style="margin-bottom: 5px">
															<div class="col-xs-12 form-inline" style="width: 100%">
																<div class="popupImageConvertor">
																	<div class="popup-container">
																		<h3 class="resize-title">Вставка зображення</h3>
																		<div class="resizer" >
																			<span class="addition">ctrl+v</span>
																		</div>
																		<button class="btn btn-info resizer-result btn btn-info">Отримати зображення</button>
																		<button class="closePopup">&#10006;</button>
																	</div>
																</div>
																<textarea class="multiblock question-name answerContent">{!! $question->text_question !!}</textarea>
															</div>
														</div>
														<div class="items first-answers-block">
															@foreach($answers[$i][$ans] as $answer)
																<div class="row item answer-item"
																	 data-answer-id="{{$answer->id}}"
																	 data-is-checked="{{$answer->answer_right}}"
																	 data-prev-answer-value='{!! $answer->text_answer !!}'
																	 style="margin-bottom: 5px;"
																>
																	<div class="col-xs-12 form-inline" style="width: 100%">
																		<div class="answer-item ">
																			<span data-toggle="tooltip" title="Вірна відповідь">
																				<input type="checkbox" name="cat-radio" class="correct-answer" {{$answer->answer_right ? 'checked' : ''}}>
																			</span>
																			<div class="popupImageConvertor">
																				<div class="popup-container">
																					<h3 class="resize-title">Вставка зображення</h3>
																					<div class="resizer">
																						<span class="addition">ctrl+v</span>
																					</div>
																					<button class="resizer-result">Отримати зображення</button>
																					<button class="closePopup">✖</button>
																				</div>
																			</div>
																			<textarea class="multiblock item-name answerContent">{!! $answer->text_answer !!}</textarea>
																		</div>
																	</div>
																</div>
															@endforeach
														</div>
													</div>
													@php($ans++)
												@endforeach
											</div>
										</li>
										@php($i++)
									@endforeach
								@else
									<div class="question-block">
										<div class="question-container">
											@php($ans = 0)
											@foreach($questions as $question)
												@php($imageWidth = $question->image_width)
												<div class="category-block question_name" data-question-id="{{$question->id}}" data-prev-question-value='{!! $question->text_question !!}'>
													<div class="imageWidthContainer im_regculation" data-prev-image-width="{{$imageWidth}}">
														<div class="row col-xs-12">
															<span>Вкажіть ширину зображень:</span>
															@foreach($data['imagesWidthChoices'] as $widthChoice)
																<label><input
																			type="radio"
																			name="image-width-radio-{{$question->id}}"
																			value="{{$widthChoice}}"
																			@if( $question->image_width === 'auto') disabled @endif
																			@if($widthChoice  == $question->image_width)
																			checked
																			@endif
																	>
																	{{$widthChoice}}px
																</label>
															@endforeach
															<label>
																<input
																		type="radio"
																		class="customWithRadio"
																		value="custom"
																		name="image-width-radio-{{$question->id}}"
																		aria-label="image-with"
																		@if( $question->image_width === 'auto') disabled @endif
																		@if(!in_array($question->image_width, $data['imagesWidthChoices']))
																		checked
																		@endif
																>
															</label>
															<input
																	type="number"
																	class="form-control customWidthInput"
																	aria-label="custom-image-with"
																	min="10"
																	max="350"
																	@if( $question->image_width === 'auto') disabled @endif
																	@if(!in_array($question->image_width, $data['imagesWidthChoices']))
																	value="{{$question->image_width}}"
																	@else
																	disabled
																	@endif
															>
															<button
																	class="changeWidthBtn btn btn-info"
																	@if(in_array($question->image_width, $data['imagesWidthChoices'])) disabled @endif
																	@if( $question->image_width === 'auto') disabled @endif
															>
																Змінити
															</button>
															<button
																	class="enableAutoWidth btn {{ $question->image_width === 'auto' ? 'btn-success' : 'btn-danger' }}"
															>
																Auto/{{ $question->image_width === 'auto' ? 'on' : 'off' }}
															</button>
														</div>
													</div>
													<div class="row category" style="margin-bottom: 5px">
														<div class="col-xs-12 form-inline" style="width: 100%">
															<div class="popupImageConvertor">
																<div class="popup-container">
																	<h3 class="resize-title">Вставка зображення</h3>
																	<div class="resizer" >
																		<span class="addition">ctrl+v</span>
																	</div>
																	<button class="btn btn-info resizer-result btn btn-info">Отримати зображення</button>
																	<button class="closePopup">&#10006;</button>
																</div>
															</div>
															<textarea class="multiblock question-name answerContent">{!! $question->text_question !!}</textarea>
														</div>
													</div>
													<div class="items first-answers-block">
														@foreach($answers[$ans] as $answer)
															<div class="row item answer-item"
																 data-answer-id="{{$answer->id}}"
																 data-is-checked="{{$answer->answer_right}}"
																 data-prev-answer-value='{!! $answer->text_answer !!}'
																 style="margin-bottom: 5px;"
															>
																<div class="col-xs-12 form-inline " style="width: 100%">
																	<div class="answer-item">
																		<span data-toggle="tooltip" title="Вірна відповідь">
																			<input type="checkbox" name="cat-radio" class="correct-answer" {{$answer->answer_right ? 'checked' : ''}}>
																		</span>
																		<div class="popupImageConvertor">
																			<div class="popup-container">
																				<h3 class="resize-title">Вставка зображення</h3>
																				<div class="resizer">
																					<span class="addition">ctrl+v</span>
																				</div>
																				<button class="resizer-result">Отримати зображення</button>
																				<button class="closePopup">✖</button>
																			</div>
																		</div>
																		<textarea class="multiblock item-name answerContent">{!! $answer->text_answer !!}</textarea>
																	</div>
																</div>
															</div>
														@endforeach
													</div>
												</div>
												@php($ans++)

											@endforeach
										</div>
									</div>
								@endif
								@if(isset($block['questions']))
										</ol>
									</div>
								@endif
							@endforeach

								<div class="verifyTableBlock col-xs-12" id="comm">  <label>Члени комісії </label>
									@foreach ($data['comms'] as $comm)
										<div class="category-block" style="display: block;" data-id="{{$comm->id}}">
											<div class="row" style="margin-bottom: 5px">
												<div class="col-xs-12 form-inline" style="width: 100%">
													<input class="item-category form-control commPosition" style="width: 45%" type="text" required placeholder="Посада члена комісії" value="{{$comm->commissioner_position}}">
													<input class="item-category form-control commName" style="width: 45%" type="text" required placeholder="ПІБ члена комісії" value="{{$comm->commissioner_name}}">
													<span  class="category-add-comm" data-toggle="tooltip" title="Додати члена комісії">
	                                                <button type="button"  class="btn btn-default">
	                                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
	                                                </button>
											</span>
													<span class="category-delete-comm" data-toggle="tooltip" title="Видалити члена комісії" style="display: none;">
	                                                <button type="button" class="btn btn-default">
	                                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
	                                                </button>
	                                            </span>
												</div>
											</div>
										</div>
									@endforeach
									@if($data['comms'] == '[]')
										<div class="category-block">
											<div class="row" style="margin-bottom: 5px">
												<div class="col-xs-12 form-inline" style="width: 100%">
													<input class="item-category form-control commPosition" style="width: 45%" type="text" required placeholder="Посада члена комісії">
													<input class="item-category form-control commName" style="width: 45%" type="text" required placeholder="ПІБ члена комісії">
													<span  class="category-add-comm" data-toggle="tooltip" title="Додати члена комісії">
	                                            <button type="button"  class="btn btn-default">
	                                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
	                                            </button>
	                                        </span>
													<span class="category-delete-comm" data-toggle="tooltip" title="Видалити члена комісії" style="display: none;">
	                                            <button type="button" class="btn btn-default">
	                                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
	                                            </button>
	                                        </span>
												</div>
											</div>
										</div>
									@endif
								</div>
								<div class="control-buttons-block">
									<div id="approve" class="btn-group btn-group-justified" role="group" aria-label="approve blank">
										<div class="btn-group" role="group">
											<button type="submit" class="btn btn-success">Підтвердити</button>
										</div>
									</div>
									<br>
									<div id="back" class="btn-group btn-group-justified" role="group" aria-label="...">
										<div class="btn-group" role="group">
											<a href="/omu" type="button" class="btn btn-info">Повернутися</a>
										</div>
									</div>
								</div>
						</form>
					@else
						<div class="alert alert-info" role="alert">
							Завдання відсутні до вибраної спеціальності.
						</div>
					@endif

				</div>
			</div>
		</div>
	</div>
	<div class="bookshelf_wrapper hidden">
		<ul class="books_list">
			<li class="book_item first"></li>
			<li class="book_item second"></li>
			<li class="book_item third"></li>
			<li class="book_item fourth"></li>
			<li class="book_item fifth"></li>
			<li class="book_item sixth"></li>
		</ul>
		<div class="shelf"></div>
	</div>
	<div class="hideBehind hidden"></div>
@endsection
@section('after-scripts')
	{{ Html::script(mix('js/pages/edit-english-blank.js')) }}
@endsection