@extends('frontend.layouts.app')


@section('after-styles')
{{ Html::style('css/omu.css') }}
@endsection

@section('content')
    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('navs.frontend.dashboard') }}</div>

                <div class="panel-body">
                    <div class="row">
                                <div class="col-xs-12">
                                    <div class="panel panel-default">
                                        <div class = "panel-heading">
                                            <div class="btn-group btn-group-justified" role="group" aria-label="..." >
                                                <div class="btn-group" role="group">
                                                    @if($logged_in_user->id_cafedres==null)
                                                        <a href="/omu" type="button" class="btn btn-info">Повернутися</a>
                                                    @else
                                                        <a href="/verification-person" type="button" class="btn btn-info">Повернутися</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <li class = "panel-body">
                                            @if($data['type'] == 2)
                                                @foreach ($data['blocks'] as $block)
                                                    @php($q = 1)
                                                    @if($block->id_speciality ==  $data['idSpeciality'])
                                                        <ol class = "block_name"> {{$block->block_name}}
                                                                    <div class = "row question_name"><b>Текст завдання: {{$block['task_title']}}</b></div>
                                                            @foreach ($data['english_config'] as $conf)
                                                                @if($conf->block_id == $block->id)
                                                                    @foreach ($data['english_text'] as $text)
                                                                        @if($text->block_id == $block->id)
                                                                            <div class = "engQuestionsDiv"> Питання {!! $q !!} </div>
                                                                            @php($q++)
                                                                            @if($conf->answer_list == 1 && $conf->text == 1 )
                                                                                @php($j = 1)
                                                                                @foreach ($data['english_questions'] as $question)
                                                                                    @if($question->id_english_question == $text->id)
                                                                                        <li class = "question_name" style="margin-left:10px">
                                                                                            @if($conf->true_false == 0)
                                                                                                {!! $question->right_answer !!}
                                                                                            @else {!! $j !!}_____
                                                                                            @php($j++)
                                                                                            @endif {!!$question->text_question !!}

                                                                                        </li>
                                                                                    @endif
                                                                                @endforeach
                                                                                <li class = "question_name">
                                                                                    @php($i = 1)
                                                                                    @php($text=$text->text)
                                                                                    @while(strpos($text, 'NNN') !== false)
                                                                                        @php ($text = preg_replace('/NNN/', $i.'_____',$text, 1))
                                                                                        @php($i++)
                                                                                    @endwhile
                                                                                    {!! $text !!}

                                                                                </li>
                                                                                @php($j = 1)
                                                                                @foreach ($data['english_questions'] as $question)
                                                                                    @if($question->block_id == $conf->id)
                                                                                        <li class = "question_name">
                                                                                            <div class = "answerEngNumb" >   {!! $j !!} </div>

                                                                                            @php($j++)
                                                                                            @php($ans = "A")
                                                                                            @foreach ($data['english_answers'] as $answer)
                                                                                                @if($answer->id_english_question == $question->id)
                                                                                                    <div class = "englishAnsStepanswerEng" >
                                                                                                        {!! $ans !!} {!! $answer->text_answer !!}
                                                                                                        @php($ans++)
                                                                                                    </div>
                                                                                                @endif
                                                                                            @endforeach
                                                                                        </li>
                                                                                    @endif
                                                                                @endforeach


                                                                            @else
                                                                                <li class = "question_name">
                                                                                    @php($i = 1)
                                                                                    @php($textie=$text->text)
                                                                                    @while(strpos($textie, 'NNN') !== false)
                                                                                        @php ($textie = preg_replace('/NNN/', $i.'_____',$textie, 1))
                                                                                        @php($i++)
                                                                                    @endwhile
                                                                                    {!! $textie !!}
                                                                                </li>
                                                                                @php($j = 1)
                                                                                @foreach ($data['english_questions'] as $question)
                                                                                    @if($question->id_english_question == $text->id)
                                                                                        <li class = "question_name">
                                                                                            <div class = "answerEngNumb" > {!! $j !!} </div>
                                                                                            @if($question->text_question != ' ')
                                                                                                {!! $question->text_question !!}
                                                                                                <br>
                                                                                            @endif
                                                                                            @php($j++)
                                                                                            @php($ans = "A")
                                                                                            @foreach ($data['english_answers'] as $answer)
                                                                                                @if($answer->id_english_question == $question->id)
                                                                                                    <div class = "englishAnsStep">
                                                                                                        {!! $ans !!} {!! $answer->text_answer !!}
                                                                                                        @php($ans++)
                                                                                                    </div>
                                                                                                @endif
                                                                                            @endforeach
                                                                                        </li>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endif

                                                                    @endforeach


                                                                        @if($conf->text == 0 && $conf->answer_list == 0)
                                                                            @php($j = 1)
                                                                            @foreach ($data['english_questions'] as $question)
                                                                                @if($question->block_id == $block->id)
                                                                                    <li class = "question_name">
                                                                                        <div class = "answerEngNumb" > {!! $j !!} </div>
                                                                                        @if($question->text_question)
                                                                                            {!! $question->text_question !!}
                                                                                            <br>
                                                                                        @endif
                                                                                        @php($j++)
                                                                                        @php($ans = "A")
                                                                                        @foreach ($data['english_answers'] as $answer)
                                                                                            @if($answer->id_english_question == $question->id)
                                                                                                <div class = "englishAnsStep">
                                                                                                    {!! $ans !!} {!! $answer->text_answer !!}
                                                                                                    @php($ans++)
                                                                                                </div>
                                                                                            @endif
                                                                                        @endforeach
                                                                                    </li>
                                                                                @endif
                                                                            @endforeach
                                                                        @else
                                                                            @foreach ($data['connections'] as $connection)
                                                                                @if($connection->block_id == $block->id)
                                                                                    @php($j = 1)
                                                                                    @php($ans = "A")

                                                                                    <table class = "taskTable"><tr>
                                                                                            @foreach ($connection->answers as $currAnswer)

                                                                                                <td> {!! $currAnswer->text_question !!} </td>
                                                                                            @endforeach
                                                                                        </tr>
                                                                                        <tr>
                                                                                            @foreach ($connection->answers as $currAnswer)
                                                                                                <th>{!! $ans !!}</th>
                                                                                                @php($ans++)
                                                                                            @endforeach
                                                                                        </tr>
                                                                                    </table>
                                                                                    <li class = "question_name">
                                                                                        @foreach ($connection->questions as $currQuestion)

                                                                                            <div class = "englishAnsStep"> {!! $j !!} {!! $currQuestion->text_question !!} </div>
                                                                                            @php($j++)
                                                                                        @endforeach
                                                                                    </li>


                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                        <hr class = "line">
                                                                @endif
                                                            @endforeach

                                                        </ol>
                                                    @endif
                                                @endforeach
                                            @elseif($data['type'] == 0)
                                            @foreach ($data['blocks'] as $block)
                                                @if($block->id_speciality ==  $data['idSpeciality'])
                                                    <ol class = "block_name"> Блок {{$block->block_name}}
                                                        @foreach ($data['questions'] as $question)
                                                            @if($question->id_block == $block->id)
                                                                <li class = "question_name">
                                                                    {!!$question->text_question !!}

                                                                    <ol class = "block_name">
                                                                        @foreach ($data['answers'] as $answer)
                                                                            @if($answer->id_question == $question->id)
                                                                                <div class = "row " style = "margin:10px  0;">
                                                                                    <div class = "answerDiv" >
                                                                                        <li type = "a" class = "answer">
                                                                                            {!! $answer->text_answer !!}
                                                                                        </li>
                                                                                    </div>

                                                                                </div>

                                                                            @endif
                                                                        @endforeach
                                                                    </ol>
                                                                    <hr class = "line">
                                                                </li>
                                                            @endif
                                                        @endforeach
                                                    </ol>
                                                @endif
                                            @endforeach
                                                @else
                                                    @foreach ($data['blocks'] as $block)
                                                        @if($block->id_speciality ==  $data['idSpeciality'])
                                                            <ol> Текст завдання<span style = "display:none" class = "blockId" >{{$block->id}} </span>
                                                                <div class = "sel_block">{{$block->block_name}}</div>
                                                                @foreach ($data['questions'] as $question)
                                                                    @if($question->id_block == $block->id)
                                                                        <div class="col-xs-6">
                                                                            <label>Питання</label>
                                                                            <p>
                                                                                {!! $question->text_question !!}
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-xs-6">
                                                                            @foreach ($data['answers'] as $answer)
                                                                                @if($answer->id_question == $question->id)
                                                                                    <label>Відповідь</label>
                                                                                    <p>
                                                                                        {!! $answer->text_answer !!}
                                                                                    </p>
                                                                                @endif
                                                                            @endforeach
                                                                            <hr class = "line">
                                                                        </div>
                                                                    @endif
                                                                @endforeach
                                                            </ol>
                                                        @endif
                                                    @endforeach

                                                @endif

                                                <div >
                                                    <div class = "commText">Члени комісії</div>
                                                    @foreach ($data['comms'] as $comm)
                                                        <div class="" style="width: 40%; display:inline-block;">
                                                            {{$comm->commissioner_position}}
                                                        </div>
                                                        <div class="" style="width: 40%;display:inline-block;">
                                                            {{$comm->commissioner_name}}
                                                        </div>
                                                    @endforeach
                                                </div>
                                        </div>
                                        </div>

                                    <div class="panel-footer">
                                        <div id = "statuses" class="btn-group btn-group-justified" role="group" aria-label="...">
                                            <div class="btn-group" role="group">
                                                <button type="button" class="btn btn-success" data-id-speciality="{{$data['idSpeciality']}}">Підтвердити</button>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <button type="button" class="btn btn-danger">На доопрацювання</button>
                                            </div>
                                        </div>
                                        <div id = "back" class="btn-group btn-group-justified" role="group" aria-label="..." style = "display:none;">
                                            <div class="btn-group" role="group">
                                                @if($logged_in_user->id_cafedres==null)
                                                    <a href="/omu" type="button" class="btn btn-info">Повернутися</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id = "pdf_set" class = "decline" data-id-speciality="{{$data['idSpeciality']}}">
        <div id = "pdfMenu" class = " pdfmenu">
            <div id = "x">
                X
            </div>
        </div>
        <label for = "reason" class = "text-mid"> Опишіть причину відмови бланку </label>
        <div class = "text-center">
            <textarea id = "reason"  class = "text_area"></textarea><br></div>
        <label class = "text-mid"> Підтвердіть відмову </label><br>
        <div class="btn-group btn-group-justified" role="group" aria-label="...">
            <div class="btn-group" role="group">
                <button id = "yes"  type="button" class="btn btn-success">Так</button>
            </div>
            <div class="btn-group" role="group">
                <button id = "no" type="button" class="btn btn-danger">Ні</button>
            </div>
        </div>

    </div>

@endsection
@section('after-scripts')
    {{ Html::script(mix('js/pages/omu.js')) }}
    {{ Html::script(mix('js/pages/view-blank.js')) }}
@endsection
