@extends('frontend.layouts.app')

@section('after-styles')
{{ Html::style('css/omu.css') }}
@endsection

@section('content')
    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-default">
                <div class="omu-block">Представник ОМУ</div>

                <div class="padding-block">

                    <div class="">
                        <div class="row">
                            <div class="col-xs-12 shadow-block padding-block">
                                <div class="padding-body">
                                    <div>Назва кафедри *</div>
                                    <div><input id = "name" class = "sel_block" value = '{{$data['department']}}'></div>
                                    <div>Адресса *</div>
                                    <div><input id = "address" class = "sel_block" value = '{{$data['address']}}'></div>
                                    <div>Контактний телефон *</div>
                                    <div><input id = "phone" class = "sel_block" value = '{{$data['phone']}}'></div>
                                    <div>Сайт</div>
                                    <div><input id = "site" class = "sel_block" value = '{{$data['site']}}'></div>

                                    <div id = "saving" class="btn-group btn-group-justified" role="group" aria-label="...">
                                        <div class="btn-group depart" role="group">
                                            <button type="button" class="btn save-btn" data-id="{{$data['id']}}">Зберегти зміни</button>
                                        </div>
                                    </div>
                                    <div class="btn-group return-block" role="group">
                                        <a href="/omu" type="button" class="btn return-btn">Повернутись назад</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('after-scripts')
    {{ Html::script(mix('js/pages/edit-department.js')) }}
@endsection