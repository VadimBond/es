@extends('frontend.layouts.app')


@section('after-styles')
	{{ Html::style(mix('css/representative.css')) }}
@endsection

@section('content')


	<div class="row" id="all">

		<div class="col-xs-12">

			<div class="panel panel-default">

				<div class="panel-body">

					@if($logged_in_user->id_cafedres!=null)

						<div class="row">

							<div class="panel " style="box-shadow: none">
								<div class="panel-heading">
									<h4>
										{{$data['cafedres'][0]->name_cafedres}}
									</h4>
									<div class="hidden">
										<p id="type">{{ $data['current_speciality']->type }}</p>
									</div>
								</div><!--panel-heading-->
								<div class="panel-body">
									<div class="col-md-12">
										<div class="row">
											<div class="col-xs-12">
												<div class="panel panel-default" id="view_task">
													<div class="panel-heading">
														<ul class="nav nav-tabs">
															<li class="active">
																<a data-toggle="tab" id="EditBlock" href="#edit-block">
																	@if($data['current_speciality']->type == "0")
																		Управління блоками
																	@elseif($data['current_speciality']->type == "2")
																		Конфігурація завдань
																	@else
																		Зміст завдання
																	@endif
																</a>
															</li>
															<li><a data-toggle="tab" id="addNewTask" href="#panel2">Додавання завдань</a></li>
															<li><a data-toggle="tab" id="EditTaskTab" href="#panel3">Редагування завдань</a></li>
															<li><a data-toggle="tab" href="#panel1">Перегляд завдань</a></li>
															<li><a data-toggle="tab" id="view" href="#viewBlock">Попередній перегляд</a></li>
															<li><a data-toggle="tab" id="verification" href="#verify">Відправка на верифікацію</a></li>
														</ul>
														<div class="main-panel">
															<div class="panel-body panel-spec">
																<h5>
																	Кафедра: {{$data['cafedres'][0]->name_cafedres}}

																</h5>

																<p style = "display:none" id="countQuestions"> @if(count($data['current_blocks']) > 0) {{$data['current_blocks'][0]->count_q_this_block}}
																	@else 0
																	@endif</p>

																<h5>Спеціальність:</h5>

																<select class="styled-select blue semi-square" id="id_speciality" >
																	@foreach ($data['name_specialitys'] as $speciality)
																		@if($data['speciality_id']==$speciality->id)
																			<option selected class="blacked" value={{ $speciality->id }}>{{ $speciality->name_speciality }}</option>
																		@else
																			<option class="blacked" value={{ $speciality->id }}>{{ $speciality->name_speciality }}</option>
																		@endif
																	@endforeach

																</select>

																<div class="count_question">Статус спеціальності - <span id="specialty_status">@if( $data['current_speciality']->status == "in_development")в розробці@elseif( $data['current_speciality']->status == "in_process")на перевірці@elseif( $data['current_speciality']->status == "approved")підтверджено@else на доопрацюванні@endif</span></div>
																<div  style="display:{{ $data['current_speciality']->status == 'declined' ? 'block' : 'none' }}" id = "statusButton">
																	<button class="btn btn-default btn-info"  data-reason="{{ $data['current_speciality']->declined_reason }}">
																		Подивитися причину відмови
																	</button>
																</div>
															</div>
														</div>

														<div class="tab-content buttom_css">

															<div id="panel1" class="tab-pane fade">
																<div class="shadow-block">
																	<ul id="blank" @if($data['current_speciality']->type == "1") style = "display:none" @endif>
																	</ul>
																	<ul id="langBlank" @if($data['current_speciality']->type != "1") style = "display:none" @endif>

																	</ul>
																</div>

																<div class="hidden_block_msg alert alert-info empty-content-list-task hideAlert" role="alert" style="display:none">
																	<p>Блоки відсутні, перейдіть на вкладку редагування та створення блоків та створіть блок</p>
																</div>

																<div id="errorNoTasks" class="hidden_block_msg alert alert-info hideAlert" role="alert" style="display:none">
																	<p>Питання відсутні</p>
																</div>


															</div>
															<div id="panel2" class="tab-pane fade">
																<input type="number" id="answersCount" class="hidden">
																<div id="addingQuestions" @if($data['current_speciality']->type == "1") style = "display:none"@endif>
																	<div class="addSelectContainer">
																		<p>Вибір блоку:</p>
																		<select id="block_selector" class="col-xs-12 form-inline sel_block">
																			@foreach($data['current_blocks'] as $block)
																				<option value="{{ $block->id }}" data-id-speciality="{{ $block->id_speciality }}" name="{{ $block->countAnswers }}">{{ $block->block_name }}</option>
																			@endforeach
																		</select>
																	</div>
																	<input id="hidden_countAnswers" type="hidden" value="">

																	<div class="hidden_block_msg alert alert-info empty-content-add-task hideAlert" role="alert">
																		<p>Блоки відсутні, перейдіть на вкладку редагування та створення блоків та створіть блок</p>
																	</div>

																	<div id="question_block" class="left">
																		<div id="categories" style="display: none;">
																			<p>Додавання питаннь:</p>
																			<form id="send-form" method="POST" class="buttom_css" enctype="multipart/form-data">
																				@csrf
																				<div class="task-container">
																					<div class="text-block" style="display: none;">
																						<h2>Текст до питання:</h2>
																						<div class="imageWidthContainer im_regculation" id="0">
																							<div class="row col-xs-10">
																								<span>Вкажіть ширину зображень:</span>
																								<label><input type="radio" name="image-width-radio-text" value="50">50px</label>
																								<label><input type="radio" name="image-width-radio-text" value="75">75px</label>
																								<label><input type="radio" name="image-width-radio-text" value="100" checked>100px</label>
																								<label><input type="radio" name="image-width-radio-text" value="125">125px</label>
																								<label><input type="radio" name="image-width-radio-text" value="150">150px</label>
																								<label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-text" aria-label="image-width"></label>
																								<input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>
																								<button class="changeWidthBtn btn btn-info" disabled>Змінити</button>
																								<button class="enableAutoWidth btn btn-danger">Auto/off</button>
																							</div>
																						</div>
																						<div class="row" style="margin-bottom: 5px">
																							<div class="col-xs-12 form-inline" style="width: 100%">
																								<div class="popupImageConvertor">
																									<div class="popup-container">
																										<h3 class="resize-title">Вставка зображення</h3>
																										<div class="resizer" >
																											<span class="addition">ctrl+v</span>
																										</div>
																										<button class="btn btn-info resizer-result">Отримати зображення</button>
																										<button class="closePopup">&#10006;</button>
																									</div>
																								</div>
																								<textarea class="multiblock text-question"></textarea>
																							</div>
																						</div>
																					</div>
																					<div class="list-answers-block" style="display: none;">
																						<table>
																							<thead>
																								<tr>
																									<th>№</th>
																									<th>Варіант</th>
																								</tr>
																							</thead>
																							<tbody></tbody>
																						</table>
																					</div>
																					<div class="list-block" style="display:none;">
																						<table>
																							<thead>
																							<tr>
																								<th>№</th>
																								<th>Текст питання</th>
																								<th class="change-content-col"></th>
																							</tr>
																							</thead>
																							<tbody></tbody>
																						</table>
																					</div>
																					<div class="questions-block">
																						<div class="category-block" style="display: block;">
																							<div class="imageWidthContainer im_regculation" id="0">
																								<div class="row col-xs-10">
																									<span>Вкажіть ширину зображень:</span>
																									<label><input type="radio" name="image-width-radio-1" value="50">50px</label>
																									<label><input type="radio" name="image-width-radio-1" value="75">75px</label>
																									<label><input type="radio" name="image-width-radio-1" value="100" checked>100px</label>
																									<label><input type="radio" name="image-width-radio-1" value="125">125px</label>
																									<label><input type="radio" name="image-width-radio-1" value="150">150px</label>
																									<label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-1" aria-label="image-with"></label>
																									<input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>
																									<button class = "changeWidthBtn btn btn-info" disabled>Змінити</button>
                                                                                                    <button class="enableAutoWidth btn btn-danger">Auto/off</button>
																								</div>
																							</div>
																							<div class="row category" style="margin-bottom: 5px">
																								<div class="col-xs-12 form-inline" style="width: 100%">
																									<div class="popupImageConvertor">
																										<div class="popup-container">
																											<h3 class="resize-title">Вставка зображення</h3>
																											<div class="resizer" >
																												<span class="addition">ctrl+v</span>
																											</div>
																											<button class="resizer-result btn btn-info" class="btn btn-info">Отримати зображення</button>
																											<button class="closePopup">&#10006;</button>
																										</div>
																									</div>
																									<textarea class="multiblock question-name"></textarea>
																									<div class="adding-btns">
				                                                                                        <span class="category-add r_margin" data-toggle="tooltip" title="Додати завдання">
				                                                                                            <button type="button" class="btn btn-default">
				                                                                                                <span aria-hidden="true" ><span class="glyphicon glyphicon-plus"></span> Додати завдання</span>
				                                                                                            </button>
				                                                                                        </span>
																										<button type="button" class="btn btn-default addNewAnswer">
																											<span class="glyphicon glyphicon-plus"></span>
																											Додати варіант
																										</button>
																									</div>
																								</div>
																							</div>
																							<div id="first_answer_item" class="items first-answers-block"></div>
																						</div>
																					</div>

																					<div class="manage-buttons-block">
																						<button class="btn btn-info add-english-task">Додати завдання</button>
																					</div>
																				</div>

																				<button class="btn btn-info center-block buttom_css" type="submit">Відправити завдання</button>
																			</form>
																		</div>

																		<div id="loadingProgress" class="hidden_block_msg alert alert-info" role="alert">
																			<div class="loaders">
																				<div class="imgText"> Зображення завантажуються</div>
																				<div class="loader circle-loader-1"></div>
																			</div>
																			<div class="progress-download">
																				<hr>
																				<p><span id="downloadImage"></span> із <span id="nmbImage"></span></p>
																			</div>
																		</div>
																	</div>
																</div>
																<form id = "addingLang" @if($data['current_speciality']->type == "0" ) style = "display:none"@endif>
																	<div id="question_block" class="left" >
																		<div id="categ">

																			<div class="category-block" style="display: block;">
																				<div class="imageWidthContainer im_regculation" id="0">
																					<div class="row col-xs-10">
																						<span>Вкажіть ширину зображень:</span>
                                                                                        <label><input type="radio" name="image-width-radio-1" value="50">50px</label>
                                                                                        <label><input type="radio" name="image-width-radio-1" value="75">75px</label>
                                                                                        <label><input type="radio" name="image-width-radio-1" value="100" checked>100px</label>
                                                                                        <label><input type="radio" name="image-width-radio-1" value="125">125px</label>
                                                                                        <label><input type="radio" name="image-width-radio-1" value="150">150px</label>
                                                                                        <label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-1" aria-label="image-with"></label>
																						<input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>
																						<button class = "changeWidthBtn btn btn-info" disabled>Змінити</button>
                                                                                        <button class="enableAutoWidth btn btn-danger">Auto/off</button>
																					</div>
																				</div>
																				<div class="row category" style="margin-bottom: 5px">
																					<div class="col-xs-12 form-inline" style="width: 100%">
																						<div class="lang-item">
																							<div class="lang-area">
																								<label>(варіант для абітурієнта)</label>
																								<div class="popupImageConvertor">
																									<div class="popup-container">
																										<h3 class="resize-title">Вставка зображення</h3>
																										<div class="resizer" >
																											<span class="addition">ctrl+v</span>
																										</div>
																										<button class="resizer-result btn btn-info">Отримати зображення</button>
																										<button class="closePopup">&#10006;</button>
																									</div>
																								</div>
																								<textarea class="multiblock questionsLang form-control text_area_lang" placeholder="Текст питання"></textarea>
																							</div>
																							<div class="lang-area">
																								<label>(варіант для перевірки)</label>
																								<div class="popupImageConvertor">
																									<div class="popup-container">
																										<h3 class="resize-title">Вставка зображення</h3>
																										<div class="resizer" >
																											<span class="addition">ctrl+v</span>

																										</div>
																										<button class="resizer-result btn btn-info">Отримати зображення</button>
																										<button class="closePopup">&#10006;</button>

																									</div>
																								</div>
																								<textarea class="multiblock answersLang form-control text_area_lang" placeholder="Текст відповіді"></textarea>
																							</div>
																						</div>

																						<div class="btn-space">
                                                                                        <span class="category-add" data-toggle="tooltip" title="Додати завдання">
                                                                                            <button type="button" class="btn btn-default">
                                                                                                <span class="glyphicon glyphicon-plus" aria-hidden="true"> Додати завдання</span>
                                                                                            </button>
                                                                                        </span>

																							<span class="category-delete" data-toggle="tooltip" title="Видалити питання" style="display: none;">
                                                                                          <button type="button" class="btn btn-default">
                                                                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"> Видалити питання</span>
                                                                                          </button>
                                                                                        </span>
																						</div>
																					</div>
																				</div>
																			</div>
																			<button class="btn btn-info center-block buttom_css" type="submit">Відправити завдання</button>
																		</div>
																	</div>
																</form>
																<div id="success-block" class="hidden_block_msg alert alert-success" role="alert">
																	<p class="text-center">Дякуємо, відповіді збережені!</p>
																</div>
																<div id="loadingProgress" class="hidden_block_msg alert alert-info" role="alert">
																	<div class="loaders">
																		<div class="imgText"> Зображення завантажуються</div>
																		<div class="loader circle-loader-1"></div>
																	</div>
																	<div class="progress-download">
																		<hr>
																		<p><span id="downloadImage"></span> із <span id="nmbImage"></span></p>
																	</div>
																</div>

																<div class="approvedDivShow" style = "display:none">Бланк підтвердженно. Зміни недоступні.
																	<button class="btn btn-info clone-speciality button">Клонувати</button>
																</div>
																<div class="DivShow" style = "display:none">Бланк на перевірці. Зміни недоступні.
																	<button class="btn btn-info clone-speciality button">Клонувати</button>
																</div>

															</div>
															<div id="panel3" class="tab-pane fade">
																<div id="editingQuestions" @if($data['current_speciality']->type == "1") style = "display:none"@endif>
																	<form action="/" method="POST" class="edit-task-select">
																		<div class="col-xs-12 form-inline">
																			<p>Вибір блоку:</p>
																			<select id="block_selector_for_edit"  style="width: 83%;" class="item-category form-control r_margin">
																				@foreach($data['current_blocks'] as $block)
																					<option value="{{ $block->id }}" data-id-speciality="{{ $block->id_speciality }}" name = "{{$block->countAnswers}}">{{ $block->block_name }}</option>
																				@endforeach
																			</select>
																		</div>

																		<div id="questions_edit_block_elements" class="col-xs-12 form-inline">
																			<p>Вибір питання:</p>
																			<select id="tasks_selector_for_edit" style="width: 83%; margin-bottom: 5px" class="item-category form-control r_margin" ></select>
																			<span class="category-pencil" data-toggle="tooltip" title="Редагувати завдання">
			                                                                    <button type="button" class="btn btn-default edit-task-button">
			                                                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
			                                                                    </button>
			                                                                </span>
																			<span class="category-trash r_margin" data-toggle="tooltip" title="Видалити завдання">
			                                                                    <button type="button" class="btn btn-default delete-task-button">
			                                                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
			                                                                    </button>
			                                                                </span>

																		</div>
																	</form>
                                                                    <form id="form_edit_task" style="display: none;" enctype="multipart/form-data">
                                                                        @csrf
																		<div class="task-container">
																			<div class="text-block" style="display: none;">
																				<h2>Текст до питання:</h2>
																				<div class="imageWidthContainer im_regculation" id="0">
																					<div class="row col-xs-10">
																						<span>Вкажіть ширину зображень:</span>
																						<label><input type="radio" name="image-width-radio-text" value="50">50px</label>
																						<label><input type="radio" name="image-width-radio-text" value="75">75px</label>
																						<label><input type="radio" name="image-width-radio-text" value="100" checked>100px</label>
																						<label><input type="radio" name="image-width-radio-text" value="125">125px</label>
																						<label><input type="radio" name="image-width-radio-text" value="150">150px</label>
																						<label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-text" aria-label="image-width"></label>
																						<input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>
																						<button class = "changeWidthBtn btn btn-info" disabled>Змінити</button>
                                                                                        <button class="enableAutoWidth btn btn-danger">Auto/off</button>
																					</div>
																				</div>
																				<div class="category row" style="margin-bottom: 5px">
																					<div class="col-xs-12 form-inline" style="width: 100%">
																						<div class="popupImageConvertor">
																							<div class="popup-container">
																								<h3 class="resize-title">Вставка зображення</h3>
																								<div class="resizer" >
																									<span class="addition">ctrl+v</span>
																								</div>
																								<button class="btn btn-info resizer-result">Отримати зображення</button>
																								<button class="closePopup">&#10006;</button>
																							</div>
																						</div>
																						<textarea class="multiblock text-question"></textarea>
																					</div>
																				</div>
																			</div>
																			<div class="list-answers-block" style="display: none;">
																				<table>
																					<thead>
																					<tr>
																						<th>№</th>
																						<th>Варіант</th>
																					</tr>
																					</thead>
																					<tbody></tbody>
																				</table>
																			</div>
																			<div class="list-block" style="display:none;">
																				<table>
																					<thead>
																					<tr>
																						<th>№</th>
																						<th>Текст питання</th>
																						<th class="change-content-col"></th>
																					</tr>
																					</thead>
																					<tbody></tbody>
																				</table>
																			</div>
																			<div class="questions-block">
																				<div class="category-block">
																					<div class="imageWidthContainer im_regculation">
																						<div class="row col-xs-10">
																							<span>Вкажіть ширину зображень:</span>
																							<label><input type="radio" name="image-width-radio-1" value="50">50px</label>
																							<label><input type="radio" name="image-width-radio-1" value="75">75px</label>
																							<label><input type="radio" name="image-width-radio-1" value="100" checked>100px</label>
																							<label><input type="radio" name="image-width-radio-1" value="125">125px</label>
																							<label><input type="radio" name="image-width-radio-1" value="150">150px</label>
																							<label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-1" aria-label="image-with"></label>
																							<input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>
																							<button class = "changeWidthBtn btn btn-info" disabled>Змінити</button>
																							<button class="enableAutoWidth btn btn-danger">Auto/off</button>
																						</div>
																					</div>
																					<div class="row category" style="margin-bottom: 5px; margin-left: 5px;">
																						<div class="col-xs-12 form-inline" style="width: 100%">
																							<div class="popupImageConvertor">
																								<div class="popup-container">
																									<h3 class="resize-title">Вставка зображення</h3>
																									<div class="resizer" >
																										<span class="addition">ctrl+v</span>

																									</div>

																									<button class="resizer-result btn btn-info" class="btn btn-info">Отримати зображення</button>
																									<button class="closePopup">&#10006;</button>

																								</div>
																							</div>
																							<textarea class="multiblock text-question"></textarea>
																							<div class="adding-btns">
																								<button type="button" class="btn btn-default addNewAnswer">
																									<span class="glyphicon glyphicon-plus"></span>
																									Додати варіант
																								</button>
																							</div>
																						</div>
																					</div>
																					<div id="first_edit_item" class="items first-ansvers-block"></div>
																				</div>
																			</div>
																			<div  id="answer_items_edit" class="items">
																			</div>
																		</div>
																			<button type="submit" class="btn btn-info center-block buttom_css">Зберегти зміни завдання</button>
                                                                    </form>
                                                                </div>

																<div id="lang_edit_block_element" style = "display:none" class="col-xs-12 form-inline">
																	<p>Вибір питання:</p>
																	<select id="get_lang_edit"   style="width: 83%; margin-bottom: 5px" class="item-category form-control r_margin" ></select>
																</div>

																<div id="changingLang" style = "display:none">

																	<div class="category-block" style="display: block;">
																		<div class="imageWidthContainer im_regculation" id="0">
																			<div class="row col-xs-10">
																				<span>Вкажіть ширину зображень:</span>
																				<label><input type="radio" name="image-width-radio-1" value="50">50px</label>
																				<label><input type="radio" name="image-width-radio-1" value="75">75px</label>
																				<label><input type="radio" name="image-width-radio-1" value="100" checked>100px</label>
																				<label><input type="radio" name="image-width-radio-1" value="125">125px</label>
																				<label><input type="radio" name="image-width-radio-1" value="150">150px</label>
																				<label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-1" aria-label="image-with"></label>
																				<input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>
																				<button class = "changeWidthBtn btn btn-info" disabled>Змінити</button>
                                                                                <button class="enableAutoWidth btn btn-danger">Auto/off</button>
																			</div>
																		</div>
																		<div class="row category" style="margin-bottom: 5px">
																			<div class="col-xs-12 form-inline" style="width: 100%">
																				<div class="lang-item">
																					<div class="lang-area">
																						<label>(варіант для абітурієнта)</label>
																						<div class="popupImageConvertor">
																							<div class="popup-container">
																								<h3 class="resize-title">Вставка зображення</h3>
																								<div class="resizer" >
																									<span class="addition">ctrl+v</span>

																								</div>

																								<button class="resizer-result btn btn-info" class="btn btn-info">Отримати зображення</button>
																								<button class="closePopup">&#10006;</button>

																							</div>
																						</div>


																						<textarea id="lang_question"  maxlength="1000" class="multiblock form-control text_area_lang" style="width: 43%"></textarea>
																					</div>
																					<div class="lang-area">
																						<label>(варіант для перевірки)</label>
																						<div class="popupImageConvertor">
																							<div class="popup-container">
																								<h3 class="resize-title">Вставка зображення</h3>
																								<div class="resizer" >
																									<span class="addition">ctrl+v</span>

																								</div>

																								<button class="resizer-result btn btn-info" class="btn btn-info">Отримати зображення</button>
																								<button class="closePopup">&#10006;</button>

																							</div>
																						</div>
																						<textarea id="lang_answers"  maxlength="1000" class="multiblock form-control text_area_lang" style="width: 43%"></textarea>
																					</div>
																				</div>
																				<div class="btn-space">
                                                                                            <span class="category-delete" data-toggle="tooltip" title="Видалити питання">
                                                                                              <button id="deleteLang" type="button" class="btn btn-default">
                                                                                                <span class="glyphicon glyphicon-trash" aria-hidden="true"> Видалити питання</span>
                                                                                              </button>
                                                                                            </span>
																				</div>
																			</div>
																		</div>

																	</div>

																	<button id="updateLang" class="btn btn-info center-block buttom_css" type="submit">Оновити завдання</button>

																</div>

																<div id="success-block" class="hidden_block_msg alert alert-success" role="alert">
																	<p class="text-center">Дякуємо, відповіді збережені!</p>
																</div>

																<div class="alert alert-success hidden" role="alert">
																	<p>Завдання успішно збережені</p>
																</div>


																<div class="hidden_block_msg alert alert-info empty-content-edit-task hideAlert" role="alert">
																	<p>Блоки відсутні, перейдіть на вкладку редагування та створення блоків та створіть блок</p>
																</div>

																<div class="approvedDivShow" style = "display:none">Бланк підтвердженно. Зміни недоступні.
																	<button class="btn btn-info clone-speciality button">Клонувати</button>
																</div>
																<div class="DivShow" style = "display:none">Бланк на перевірці. Зміни недоступні.
																	<button class="btn btn-info clone-speciality button">Клонувати</button>
																</div>

																<div id="langEmpty" @if($data['current_speciality']->type == "0" || (count($data['current_blocks']) > 0 && $data['current_blocks'][0]->count_q_this_block == "1")) style = "display:none" @endif>Немає питань для редагування</div>
															</div>


															<div id="edit-block" class="tab-pane fade in active">

																<div id="blockControlling" @if($data['current_speciality']->type == "1" ) style ="display:none" @endif>
																	<div class="row">
																		<div id="blockControl" class="col-xs-12 form-inline choose-container"  >
																			<label>Вибір блоку:</label>
																			<div class="flex-row">
																				<select id="choose-select"  style="width: 100%;" class="item-category form-control" >
																					@foreach($data['current_blocks'] as $block)
																						<option value="{{$block->id}}" data-id-speciality="{{ $block->id_speciality }}">{{ $block->block_name }}</option>
																					@endforeach
																					<option value="new_block">Створити новий блок</option>
																				</select>

																				<span data-toggle="tooltip" title="Редагувати блок">
                                                                                <button type="button" class="btn btn-default category-pencil" disabled>
                                                                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                                                                </button>
                                                                        </span>
																				<span class="r_margin" data-toggle="tooltip" title="Видалити блок">
                                                                                <button type="button" class="btn btn-default category-trash" disabled>
                                                                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                                                </button>
                                                                        </span>
																			</div>
																			<div class="line-dashed"></div>
																		</div>

																		<input type="hidden" id="hidden_id_speciality" name="id_speciality_block">
																		<div id="blockCreateForm" class="col-xs-12 new-block-form ">
																			<form id="create-block-form">
																				@csrf
																			</form>
																		</div>

																		<div class="col-xs-12 edit-block-container hidden">
																			<form id="edit-block-form">
																				@csrf
																			</form>
																		</div>
																	</div>
																</div>
																<div id="editLangBlock" @if($data['current_speciality']->type != "1" ) style = "display:none" @endif>
																	<div class="row form-group">
																		<label>Зміст завдання</label>
																		<input id="blockLangName" type="text" class="form-control required block-name" value="{{isset($data['current_blocks'][0]->block_name) ? $data['current_blocks'][0]->block_name : ''}}" placeholder="Назва блоку" maxlength="64" required>
																	</div>
																	<button id="updateLangBlock" class="btn btn-primary center-block">Зберегти зміни</button>
																</div>
																<div class="approvedDivShow" style = "display:none">Бланк підтвердженно. Зміни недоступні.
																	<button class="btn btn-info clone-speciality button">Клонувати</button>
																</div>
																<div class="DivShow" style = "display:none" >Бланк на перевірці. Зміни недоступні.
																	<button class="btn btn-info clone-speciality button">Клонувати</button>
																</div>

															</div>
															<div id="verify" class="tab-pane fade">
																<div  class="col-xs-12 select-margin  {{ count($data['name_specialitys']) > 0 ? '': 'hidden' }}">
																	<input type="hidden" id="hidden_id_speciality" >
																	Оберіть спеціальність
																	<select id="choose_specialty" class="item-category form-control " >
																		@foreach($data['name_specialitys'] as $speciality)
																			@if($speciality->status == "in_development" || $speciality->status == "declined" )
																				<option value="{{$speciality->id}}">{{ $speciality->name_speciality }}</option>
																			@endif
																		@endforeach
																	</select>

																</div>

																<div class="col-xs-12">
																	<table id="block_table" class="repTable" >
																	</table>
																	<div id="verify_no_questions" class="hidden alert alert-info empty-content-edit-task hideAlert" role="alert">
																		<p>Завдання відсутні</p>
																	</div>
																	<div class="blue-block hidden">&nbsp;</div>
																	<div class="hidden" id="full">
																		<div>Загальна сума балів</div>
																		<div id="fullCount"> 0 </div>
																		<span id="overCount" class="redText"></span>
																	</div>
																	<div id="comm">
																	</div>
																	<div class="btn-group sendDiv" >
																		<button id = "sendDiv" type="button" class="btn btn-warning btn-flat hidden" disabled=true title="Загальна сумма балів повинна бути рівна 100 ">
																			Відправити на перевірку представнику ОМУ
																		</button>
																	</div>

                                                            </div>
                                                        </div>
                                                    <div id="viewBlock" class="tab-pane fade">
                                                        <div id="blockView" class="col-xs-12 " >
                                                            <label>Оберіть блок</label>

                                                            <select id="choose_block_for_view"   class="item-category form-control" >
                                                                @foreach($data['blocks'] as $block)
                                                                    <option value="{{$block->id}}" data-id-speciality="{{ $block->id_speciality }}">{{ $block->block_name }}</option>
                                                                @endforeach
                                                            </select>

                                                            <button id="showView" class="btn btn-primary center-block correction">Переглянути ПДФ</button>
                                                        </div>
                                                        <div id="viewNoBlock" class="hidden_block_msg alert alert-info empty-content-add-task hideAlert" role="alert">
                                                            <p>Блоки відсутні, перейдіть на вкладку редагування та створення блоків та створіть блок</p>
                                                        </div>
                                                    </div>
                                                    </div>

													</div>

												</div>
											</div>
										</div>

									</div>

								</div>

							</div>

						</div>
						<input id="data_user_email" type="hidden" value="{{ $logged_in_user->email }}">
					@else
						<div class="row">


							<div class="col-md-4 col-md-push-8">

								<ul class="media-list">
									<li class="media">
										<div class="media-left">
											<img class="media-object" src="{{ $logged_in_user->picture }}"
											     alt="Profile picture">
										</div><!--media-left-->

										<div class="media-body">
											<h4 class="media-heading">
												{{ $logged_in_user->name }}<br/>
												<small>
													{{ $logged_in_user->email }}<br/>

													Дата реєтрації: {{ $logged_in_user->created_at->format('F jS, Y') }}
												</small>
											</h4>

											{{ link_to_route('frontend.user.account', trans('navs.frontend.user.account'), [], ['class' => 'btn btn-info btn-xs']) }}

											@permission('view-backend')
											{{ link_to_route('admin.dashboard', trans('navs.frontend.user.administration'), [], ['class' => 'btn btn-danger btn-xs']) }}
											@endauth
										</div><!--media-body-->
									</li><!--media-->
								</ul><!--media-list-->
							</div>
							<div style="text-align: left">Вашому профілю не відповідає жодна кафедра.<br>
								Зверніться до адміністратора або спробуйте <br>
								перейти до <a href="/admin/dashboard">панелі адміністрування</a> якщо <br>
								ви маєте необхідні дозволи.</div>
							@endif

						</div><!--panel body-->

				</div><!-- panel -->

			</div><!-- col-md-10 -->

		</div><!-- row -->

        <div id="declined" class="confirmPopup"><div  class=" menu">
                <div id="exit">
                    X
                </div>
            </div>
            <div class="mid"><label for="depName"> Причина відмови: </label>
                <div> <textarea id="declinedReason "></textarea></div>
            </div>
            <div class="mid"><button id="closeDeclined" >Закрити</button></div>
        </div>
    </div>
    <div id="blur-screen" class="hidden">
    </div>
    <iframe id="pdf-frame" class="hidden"></iframe>
    <div class="bookshelf_wrapper">
        <ul class="books_list">
            <li class="book_item first"></li>
            <li class="book_item second"></li>
            <li class="book_item third"></li>
            <li class="book_item fourth"></li>
            <li class="book_item fifth"></li>
            <li class="book_item sixth"></li>
        </ul>
        <div class="shelf"></div>
    </div>
    <div class="hideBehind"></div>

@endsection


@section('after-scripts')
	{{ Html::script(mix('js/representative.js')) }}
@endsection

@if(count($data['current_blocks']) != 0)
@endif