@extends('frontend.layouts.app')

@section('after-styles')
{{ Html::style('css/omu.css') }}
@endsection

@section('content')
    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('navs.frontend.dashboard') }}</div>

                <div class="panel-body">
                    <div class="row">
                        </div><!--col-md-4-->
                        <div class="">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="panel panel-default">
                                        <div class = "panel-heading">
                                            <div class="btn-group btn-group-justified" role="group" aria-label="..." >
                                                <div class="btn-group" role="group">
                                                    <a href="/omu" type="button" class="btn btn-info">Повернутися</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class = "panel-body">
                                            <div>Ім'я*</div>
                                            <div><input id = "firstName" class = "sel_block" value = '{{$data['firstName']}}'></div>
                                            <div>Прізвище*</div>
                                            <div><input id = "secondName" class = "sel_block" value = '{{$data['secondName']}}'></div>
                                            <div>Емайл*</div>
                                            <div id = "originEmail" style = "display: none;">{{$data['email']}}</div>
                                            <div><input id = "email" class = "sel_block" value = '{{$data['email']}}'></div>
                                            <div>Роль*</div>
                                            <select id = "roleVal" class=" sel_block">
                                                @foreach ($data['roles'] as $role)
                                                    @if($role->id == $data['roleId'])
                                                        <option class="blacked" selected value={{ $role->id }}>{{ $role->name }}</option>
                                                    @else
                                                        <option class="blacked" value={{ $role->id }}>{{ $role->name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            @if($data['role'] == "Представник кафедри"  )
                                                <div id = "hidden">
                                                    Кафедра<select id = "dep" class=" sel_block">
                                                        @foreach ($data['cafedres'] as $cafedre)
                                                            @if($cafedre->id == $data['dep'])
                                                                <option class="blacked" selected value={{ $cafedre->id }}>{{ $cafedre->name_cafedres }}</option>
                                                            @else
                                                                <option class="blacked" value={{ $cafedre->id }}>{{ $cafedre->name_cafedres }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            @else <div id = "hidden" style = "display: none">
                                                Кафедра<select id = "dep" class=" sel_block">
                                                    @foreach ($data['cafedres'] as $cafedre)
                                                        @if($cafedre->id == $data['dep'])
                                                            <option class="blacked" selected value={{ $cafedre->id }}>{{ $cafedre->name_cafedres }}</option>
                                                        @else
                                                            <option class="blacked" value={{ $cafedre->id }}>{{ $cafedre->name_cafedres }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            @endif
                                            <div>Новий пароль</div>
                                            <div>
                                                <input type = "password" id = "pass" class = "sel_block">
                                            </div>
                                            <div>Підтвердження пароля</div>
                                            <div>
                                                <input type = "password" id = "confirm" class = "sel_block">
                                            </div>
                                            <div class="panel-footer">
                                                <div id="saving" class="btn-group btn-group-justified" role="group" aria-label="...">
                                                    <div class="btn-group" role="group">
                                                        <button id="save" type="button" class="btn btn-success" data-id="{{$data['id']}}">Зберегти</button>
                                                        <a style = 'display: none;' id = "back" href="/omu" type="button" class="btn btn-info">Повернутися</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('after-scripts')
    {{ Html::script(mix('js/pages/edit-representative.js')) }}
@endsection