const {setWidthImage} = require('./roles/representative/images');

module.exports = () => {
	const autoWidth = $('.enableAutoWidth');

	autoWidth.off('click');
	autoWidth.on('click', function (e) {
		e.preventDefault();

		const curBtn = $(e.currentTarget),
			widthContainer = curBtn.closest('.imageWidthContainer');

		if( curBtn.hasClass('btn-danger')) {
			widthContainer.find('input').prop('disabled', true);
			widthContainer.find('.changeWidthBtn').prop('disabled', true);

			curBtn.removeClass('btn-danger');
			curBtn.addClass('btn-success');
			curBtn.text('Auto/on');

			setWidthImage('auto', widthContainer.find('input').first());
		}
		else {
			widthContainer.find('input[type="radio"]').prop('disabled', false);

			curBtn.addClass('btn-danger');
			curBtn.removeClass('btn-success');
			curBtn.text('Auto/off');

			widthContainer.find('input:radio[value="100"]').prop('checked', true);
			widthContainer.find('.customWidthInput').val('');

			setWidthImage(100, widthContainer.find('input').first());
		}
	});
};