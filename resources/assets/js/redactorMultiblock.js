function createSwal(params) {
	swal({
		...params,
		closeOnClickOutside: true,
		allowOutsideClick: true
	});
}

function redactorMultiblock() {

	let _this = this;

	$(this).htmlarea({
		// Override/Specify the Toolbar buttons to show
		toolbar: [
			["html","bold", "italic", "underline","strikethrough"],
			["justifyleft","justifycenter","justifyright"],
			[{
				css: "justifyFull",
				text: "по ширині",
				action: function (btn) {
					this.justifyFull();
				}

			}],
			[{
				css: "screen",
				text: "Додати зображення",
				action: function (btn) {
					let popupImageConvertor = $(this)[0].container.prev();
					popupImageConvertor.css('display', 'flex');

					let el = popupImageConvertor[0].querySelector('.resizer');
					let getRes = popupImageConvertor[0].querySelector('.resizer-result');
					let close = popupImageConvertor.find('.closePopup');
					close.off('click');
					close.on('click', (e) => {
						e.preventDefault();
                        el.innerHtml = '';
						popupImageConvertor.css('display', 'none');
					});

					if(el) {
						el.addEventListener('paste', e => {

							e.preventDefault();
							let clipboard = e.clipboardData;

							if (clipboard && clipboard.items) {

								let item = clipboard.items[0];

								if (item && item.type.indexOf('image/') > -1) {

									let blob = item.getAsFile();

									if (blob) {

										let reader = new FileReader();
										reader.readAsDataURL(blob);

										reader.onload = event => {
											let img = new Image();
											img.src = event.target.result;

                                            el.innerText = '';

											el.append(img);
											getRes.onclick = function (e) {

												let imageWidth = $(_this).closest('.category-block').find('input[type="radio"]:checked');

												imageWidth = typeof $(imageWidth).val() === 'undefined' ?
													$(_this).closest('.task-container').find('input[type="radio"]:checked') :
													imageWidth;

												imageWidth = typeof $(imageWidth).val() === 'undefined' ?
													$(_this).closest('.text-block').find('input[type="radio"]:checked') :
													imageWidth;

												imageWidth = typeof $(imageWidth).val() === 'undefined' ?
													$(_this).closest('.question_name').find('input[type="radio"]:checked') :
													imageWidth;

												if($(imageWidth).val() === 'custom') {
													imageWidth = $(imageWidth).parent().next();
												}

												if(imageWidth.closest('.imageWidthContainer').find('.enableAutoWidth').hasClass('btn-success')) {
													imageWidth = 'auto';
												}
												else {
													imageWidth = $(imageWidth).val();
												}

												e.preventDefault();

												imageWidth = imageWidth ? imageWidth : 10;
												img.setAttribute("style", `width:${imageWidth}px`);
												let iframe = popupImageConvertor.parent().find('.jHtmlArea iframe').contents().find('body');

												iframe.append(img);

												iframe.focus();

                                                el.innerHtml = '';

												popupImageConvertor[0].style.display = 'none';

												let parent = $(el).closest('.popup-container').parent();
												$(el).closest('.popup-container').remove();

												parent.prepend(`<div class="popup-container">
																	<h3 class="resize-title">Вставка зображення</h3>
																	<div class="resizer">
																		<span class="addition">ctrl+v</span>
																	</div>
																	<button class="resizer-result btn btn-info">Отримати зображення</button>
																	<button class="closePopup">✖</button>
																</div>`);
											};

											close.off('click');
											close.on('click', (e) => {
												let parent = $(el).closest('.popup-container').parent();
												$(el).closest('.popup-container').remove();

												parent.prepend(`<div class="popup-container">
																	<h3 class="resize-title">Вставка зображення</h3>
																	<div class="resizer">
																		<span class="addition">ctrl+v</span>
																	</div>
																	<button class="resizer-result btn btn-info">Отримати зображення</button>
																	<button class="closePopup">✖</button>
																</div>`);

												e.preventDefault();
												try {
                                                    el.innerHtml = '';
												}
												catch(err) {
													popupImageConvertor = $(this)[0].container.prev();
												}
                                                el.innerHtml = '';
												popupImageConvertor.css('display', 'none');
											});
										}
									}
								}
							}
						});
						getRes.onclick = function (e) {
							e.preventDefault();
							createSwal({
								title: 'Вставте зображення',
								type: 'error'
							});
						}
					}
					$(this).focus();
				}
			}],
			
			["subscript", "superscript"],
			["orderedlist","unorderedlist"],
		],
	});
	
	if($('#edit-block .ToolBar')){
		$('#edit-block .ToolBar .screen').closest('ul').css("display", "none");
	}
};

module.exports = {
	redactorMultiblock: redactorMultiblock
};