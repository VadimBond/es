const createSwal = (params, callback, time = 3000) => {
	swal(
		{
			...params,
			closeOnClickOutside: true,
			allowOutsideClick: true
		}, callback);

	setTimeout( () => swal.close, time);
};


module.exports = {
	createSwal: createSwal
};