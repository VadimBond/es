const imageWidthChoices = ['50', '75', '100', '125', '150'];

module.exports = {
	imageWidthChoices: imageWidthChoices
};