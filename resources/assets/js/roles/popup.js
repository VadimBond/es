function getPopup(object) {
	var object_of_deletion = "";

	if(object == "responsible") {
		object_of_deletion = "користувача?";
	}
	else if(object == "department") {
		object_of_deletion = "кафедру? Це також видалить спеціальності та верифікуючі особи, прив'язані до неї";
	}
	else if(object == "specialization") {
		object_of_deletion = "спеціальність?";
	}

	var div =
		`<div class="confirmMenu">
            <div id = "x">
                X
            </div>
        </div>
        <div class="mid">
            <label> Ви впевнені що хочете видалити  ${object_of_deletion}</label>
        </div>
        <div class="mid">
            <div class="btn-group chooseContainer" role="group" aria-label="...">
                <div class="btn-group" role="group">
                    <button type="button" id="yes" class="btn btn-success">Так</button>
                </div>
                <div class="btn-group" role="group">
                    <button type="button" id="no" class="btn btn-danger">Ні</button>
                </div>
            </div>
        </div>`;
	return div;
}

function hidePdf(name) {
	$(`#${name}`).hide();
}

function moving() {
	var mousePosition;
	var offset = [0, 0];
	var div;
	var isDown = false;
	div = document.getElementById("pdf_set");

	document.addEventListener('mouseup', function () {
		isDown = false;
	}, true);

	document.addEventListener('mousemove', function (event) {
		event.preventDefault();
		if (isDown) {
			mousePosition = {
				x: event.clientX,
				y: event.clientY
			};
			div.style.left = (mousePosition.x + offset[0]) + 'px';
			div.style.top = (mousePosition.y + offset[1] + 126) + 'px';
		}
	}, true);

	const pdfMenu = document.getElementById("pdfMenu");

	if(pdfMenu) {
		pdfMenu.addEventListener('mousedown', function (e) {
			isDown = true;
			offset = [
				div.offsetLeft - e.clientX,
				div.offsetTop - e.clientY
			];
		}, true);
	}
}

function exitPdf(e) {
	var container = $("#showPdf");
	if (!container.is(e.target) && container.has(e.target).length === 0) {
		container.hide();
	}

	$('.pdfbtn').off('click');
}

module.exports = {
	moving: moving,
	hidePdf: hidePdf,
	exitPdf: exitPdf,
	getPopup: getPopup
};