function addNewAnswer(answer, addQuestion, addAnswer, deleteQuestion, deleteAnswer, isRadio){
	var newAnswer = `<div class="row `
	if(isRadio){
	newAnswer += `item answer-item" style="margin-bottom: 5px;">
               		<div class="col-xs-12 form-inline "  style="width: 100%">`
	}
	else {
		newAnswer += `category" style="margin-bottom: 5px;">
                		<div class="col-xs-12 form-inline "  style="width: 100%">`
	}

	if(isRadio) {
		newAnswer += `<div class="answer-item">
                        <span data-toggle="tooltip" title="Вірна відповідь">
                            <input type="checkbox" name="cat-radio" class="correct-answer" `;

		if (answer != null) {
			newAnswer += answer['answer_right'] ? 'checked' : '';
		}

		newAnswer += `></span>`
	}

	newAnswer += `<div class="popupImageConvertor">
                    <div class="popup-container">
                        <h3 class="resize-title">Вставка зображення</h3>
                        <div class="resizer" >
                            <span class="addition">ctrl+v</span>
                        </div>
                        <button class="resizer-result" class="btn btn-info">Отримати зображення</button>
                        <button class="closePopup">&#10006;</button>
                    </div>
                </div>
                <textarea class="multiblock `;

	if(isRadio) {
		newAnswer += `item-name " data-id-answer="`;
	}
	else {
		newAnswer += `question-name " data-id-answer="`;
	}

	if(answer != null) {
		newAnswer += answer['id'];
	}

	newAnswer += `" name="text_answer_for_edit">`;

	if(answer != null) {
		if(typeof answer.text_answer !== 'undefined') {
			newAnswer += answer.text_answer;
		}
		else {
			newAnswer += answer.text_question;
		}
	}

	newAnswer += `</textarea>`;

	if(addQuestion) {
		newAnswer += `<div class="adding-btns">
						<span class="category-add r_margin" data-toggle="tooltip" title="Додати завдання">
							<button type="button" class="btn btn-default">
								<span aria-hidden="true"><span class="glyphicon glyphicon-plus"></span> Додати завдання</span>
							</button>
						</span>`;
	}

	if(addAnswer) {
		newAnswer += `<button type="button" class="btn btn-default addNewAnswer"><span class="glyphicon glyphicon-plus"></span> Додати варіант</button>`;
	}

	if(deleteQuestion) {
		newAnswer += `<span class="category-delete" data-toggle="tooltip" title="Видалити питання">
					  	<button type="button" class="btn btn-default">
							<span><span class="glyphicon glyphicon-trash"  aria-hidden="true"></span> Видалити питання</span>
						</button>
					  </span>
					</div>`;
	}

	if(deleteAnswer) {
		newAnswer += `<div class="delete-block">
						<button type="button" class="btn btn-default deleteAnswer" style="display: none">
							<span><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Видалити варіант</span>
						</button> 
					</div>`;
	}
    if(isRadio) {
        newAnswer += `</div>`;
    }
	newAnswer += `</div></div>`;

	return newAnswer;
}

module.exports = {
	addNewAnswer: addNewAnswer
}