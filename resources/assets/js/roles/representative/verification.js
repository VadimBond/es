const createSwal = require('./createSwal').createSwal;

function newCommDiv(id, position, name) {
	position = position != null ? position : '';
	name = name != null ? name : '';
	var div = `<div class="category-block" style="display: block;">
                <div class="row" style="margin-bottom: 5px">
                    <div class="col-xs-12 form-inline" style="width: 100%">
                        <div class = "hidden idsComm">${id}</div>
                        <div style="margin-left: 10%; margin-right: 10%; box-shadow: 0 0 10px rgba(0,0,0,0.5); padding-top: 3%; padding-bottom: 3%; margin-bottom: 3%; display: flex; justify-content: space-between">
                            <div style="margin-left: 3%; width: 100%">
                                Посада члена комісії <br>
                                <input class="item-category form-control commPosition" style="width: 90%" type="text" required value = ${position}> <br>
                                ПІБ члена комісії <br>
                                <input class="item-category form-control commName" style="width: 90%" type="text" required value = ${name}>
                            </div>
                            <div style="margin-right: 3%; margin-top: 4%">
                                <span  class = "category-add-comm" data-toggle="tooltip" title="Додати члена комісії">
                                    <button type="button"  class="btn btn-default">
                                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                    </button>
                                </span>
                                <span class="category-delete-comm" data-toggle="tooltip" title="Видалити члена комісії" style="display: none;">
                                    <button type="button" class="btn btn-default">
                                        <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div> 
                </div>`;
	return div;
}

function loadComm() {
	if($("#choose_specialty").val()) {
		var spec = $("#choose_specialty").val();

		$.ajax({
			method: 'POST',
			url: '/get-comm',
			data: {'id': spec,},
			success: function (response) {
				response.forEach(function (element) {
					const name = element.commissioner_name;
					const position = element.commissioner_position;
					const id = element.id;
					var newComm = newCommDiv(id, position, name);
					$('#comm').append(newComm);
				});
				var nmbComm = $("#comm").find('.category-block').length;

				if (nmbComm === 0) {
					var emptyComm = newCommDiv(0, null, null);

					$('#comm').append(emptyComm);
				}
				update();
			},
			error: function (error) {
				console.log(error);
			}
		});
	}
	else {
		update();
	}
}

function update() {
	var newComm = newCommDiv(0, null, null),
		rmCommBtn = $('.category-delete-comm'),
		addCommBtn = $('.category-add-comm'),
		commContainer = $('#comm');
	updateListeners();

	var nmbComm = $(commContainer).find('.category-block').length;
	if(nmbComm > 1) {
		commContainer.find('.category-delete-comm').show();
	}

	function updateListeners() {
		rmCommBtn = $('.category-delete-comm');
		addCommBtn = $('.category-add-comm');

		addCommBtn.off('click');
		addCommBtn.on('click', addComm);

		rmCommBtn.off('click');
		rmCommBtn.on('click', rmComm);
	}
	function addComm(){
		var nmbComm = $(commContainer).find('.category-block').length;

		if(nmbComm < 5) {
			$('#comm').append(newComm);
			updateListeners();
			if(nmbComm > 0) {
				commContainer.find('.category-delete-comm').show();
			}
		}
		else {
			createSwal({
				title: 'Максимальна к-ть членів комісії',
				type: 'error'
			});
		}

		if(nmbComm == 1) {
			commContainer.first().find('.category-delete-comm').show();
		}
	}

	function rmComm(){
		var nmbComm = $(commContainer).find('.category-block').length,
			comm = $(this).parent().parent().parent().parent().parent();

		if(nmbComm > 1) {
			comm.remove();
			var id = comm.find('.idsComm').text();
			if(id!=0) {
				$.ajax({
					method: 'POST',
					url: '/delete_comm',
					data: {'id' : id},
					success: function (response) {
						console.log(response);
					},
					error: function (error) {
						console.log(error);
					}
				});
			}
			updateListeners();
		}

		if(nmbComm == 2) {
			commContainer.first().find('.category-delete-comm').hide()
		}

		commContainer.find('.category-add-comm').toArray().map(el => $(el).show());
	}
}

function updateSumm(){
	if(+$(this).val() > +$(this).attr("max")){
		$(this).val($(this).attr("max"));
	}

	var sum = 0,
		count = $(this).val(),
		mark = $(this).parent().parent().find(".markPer").text();
	$(this).parent().parent().find(".summ").text((mark*count).toFixed(1));
	$('.summ').each(function() {
		sum += parseFloat($(this).text());
	});

	$("#fullCount").text(sum);

	if(sum > 100) {
		$("#overCount").text("Сума більше 100");
	}
	else {
		$("#overCount").text('');
	}

	if(sum === 100) {
		$('#sendDiv').prop("disabled", false);
	}
	else {
		$('#sendDiv').prop("disabled", true);
	}
}

function sendOmu() {

	var specId = $("#choose_specialty").val();
	var counts = new Array();
	var ids = new Array();
	var commIds = new Array();
	var commNames = new Array();
	var commPositions = new Array();
	var ifNull = 1;
	var i = 0;

	$(".inputTasks").each(function () {
		counts[i] = $(this).val();
		i++;
	});

	i = 0;
	$(".ids").each(function () {
		ids[i] = $(this).text();
		i++;
	});

	i = 0;
	$(".commPosition").each(function () {
		commPositions[i] = $(this).val();
		if(!$(this).val()) ifNull = null;
		i++;
	});

	i = 0;
	$(".commName").each(function () {
		if(!$(this).val()) ifNull = null;
		commNames[i] = $(this).val();
		i++;
	});

	i = 0;
	$(".idsComm").each(function () {
		commIds[i] = $(this).text();
		i++;
	});

	if(ifNull) {
		$('#statusButton').addClass("hidden");
		$(".blue-block").addClass("hidden");
		$("#choose_specialty option:selected").remove();
		$("#choose_specialty").val('');
		$("#block_table").html('');

		if ($("#id_speciality").val() === specId) {
			$("#specialty_status").text("на перевірці");
		}

		$("#full, #sendDiv, #comm").addClass("hidden");

		$.ajax({
			method: 'POST',
			url: '/send_specialty',
			data: {
				'specId': specId,
				'ids': ids,
				'counts': counts,
				'commId': commIds,
				'commNames': commNames,
				'commPosition': commPositions,
			},
			success: function (response) {
				createSwal({
					title: 'Відправлено на верифікацію',
					type: 'success'
				});
			},
			error: function (error) {
				createSwal({
					title: 'Помилка при відправленні на верифікацію',
					type: 'success'
				});
				console.log(error);
			}
		});
	}
	else {
		createSwal({
			title: 'Введіть дані про всіх обраних членів комісії',
			type: 'error'
		});
	}
}

module.exports = {
	loadComm: loadComm,
	updateSumm: updateSumm,
	sendOmu: sendOmu,
};