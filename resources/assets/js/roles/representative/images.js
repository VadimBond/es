const createSwal = require('./createSwal').createSwal;
const imageWidthChoices = require('../../modules/constants').imageWidthChoices;

function addListenersToImages () {
	const editImageWidthRadio = $('#panel3 .imageWidthContainer input[type="radio"]'),
		editChangeWidthBtn = $('#panel3 .changeWidthBtn');

	editImageWidthRadio.off('change');
	editImageWidthRadio.on('change', (e) => handleChangeRadio(e, 'edit-task'));
	editChangeWidthBtn.off();
	editChangeWidthBtn.on('click', (e) => {
		e.preventDefault();
		const imageWidth = $(e.currentTarget).prev().val();
		setWidthImage(imageWidth, 'edit-task', $(e.currentTarget).prev().prev().find('input'));
	});

	$('.image-buttons').off('change');
	$('.image-buttons').on('change', function () {
		const isValideType = validateImageType(this);
		if(this.value && isValideType) {
			$(this).next().addClass("color-change");
			$(this).parent().parent().next().show();
			cutTitle(this, true);
		}
		else {
			$(this).next().removeClass("color-change");
			$(this).parent().parent().next().find(".imgDel").hide();
			cutTitle(this);
		}
	});

	$('.delete_img').off('click');
	$('.delete_img').on('click', function () {
		cutTitle($(this).parent().prev().children().last().children().first());
		$(this).parent().prev().children().last().children().first().attr('data-is-change',"true");
		$(this).parent().prev().children().last().children().first().val(null);
		$(this).parent().hide();
		$(this).parent().prev().children().last().children().last().removeClass("color-change");
	});
}

function cutTitle (element, isValue = false) {
	var currentSpan = $(element).next();

	var filename = "";

	if(isValue) {
		filename = $(element).val().replace(/.*\\/, "");
	}
	else {
		filename = "Завантажити фото";
	}

	currentSpan.attr('title', filename);
}

function validateImageType(elem) {
	const filesExt = ['jpg', 'jpeg', 'png'];

	let parts = $(elem).val().split('.');
	if (filesExt.join().search(parts[parts.length - 1]) == -1) {
		$(this).val('');
		createSwal({
			title: 'Невірний формат файлу',
			type: 'error'
		});

		return false;
	}
	return true;
}

function handleChangeRadio(e) {
	const currentRadio = $(e.currentTarget),
		customWidthInput = $(currentRadio).parent().parent().find('.customWidthInput'),
		changeWidthBtn = $(currentRadio).parent().parent().find('.changeWidthBtn');

	if(currentRadio.val() === 'custom') {
		customWidthInput.attr('disabled', false);
		changeWidthBtn.attr('disabled', false);
	}
	else {
		customWidthInput.attr('disabled', true);
		changeWidthBtn.attr('disabled', true);
		setWidthImage(currentRadio.val(), currentRadio);
	}
}

function setWidthImage(widthImage, currentRadio) {
	let multiblocks = $(currentRadio).parent().parent().parent().parent().find('.multiblock');

	multiblocks.attr('data-width', widthImage);

	widthImage = widthImage ? widthImage : 10;

	[...multiblocks].forEach( (el) => {
		$(el).parent().find('.html').click();
		let value = $(el).val();
		$(el).val(value.replace(/width:(\d+|auto)/g, `width:${widthImage}`));
		$(el).parent().find('.html').click();
	});
}

function setDefaultImageWidth(imageWidth) {
	if(imageWidth === 'auto') {
		$('#panel3 .enableAutoWidth').click();
	}
	else {
		imageWidth = imageWidth ? imageWidth : 10;

		if (imageWidthChoices.includes(imageWidth)) {
			const searchRadio = $(`#panel3 .imageWidthContainer input[type="radio"][value=${imageWidth}]`);
			searchRadio.attr('checked', true);
		}
		else {
			const customWidthRadio = $('#panel3 .imageWidthContainer .customWithRadio'),
				customWidthInput = $('#panel3 .imageWidthContainer .customWidthInput'),
				changeWidthBtn = $('#panel3 .imageWidthContainer .changeWidthBtn');

			customWidthRadio.click();
			customWidthInput.attr('disabled', false);
			changeWidthBtn.attr('disabled', false);
			customWidthInput.val(imageWidth);
		}
	}
}

module.exports = {
	addListenersToImages: addListenersToImages,
	handleChangeRadio: handleChangeRadio,
	setWidthImage: setWidthImage,
	setDefaultImageWidth: setDefaultImageWidth
};