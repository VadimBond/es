const getLangQuestions = require('./manage-question').getLangQuestions;
const {loadComm, updateSumm} = require('./verification');
const {createNewBlock, createEnglishBlock, fillEditForms} = require('./manage-blocks');

function cleanValidation (element) {
    $(element).parent().removeClass("access-validate");
    $(element).parent().removeClass("not-validate");
}

function manageContentTab() {

	const currTab = $('.panel-heading .nav-tabs .active'),
		currTabHref = $(currTab).find('a').attr('href'),
		nmbBlocks = $('#choose-select').find('option:not(".hidden")').length;

	if(currTabHref === '#panel1') {
		const curInfoBlock = $('.empty-content-list-task'),
			blankContainer = $('#blank');

		$('#errorNoTasks').hide();
		if($('#type').text() === "1") {
			getLangShow();
		}
		else {
			$('#errorNoTasks').hide();
			if (nmbBlocks > 1) {
				blankContainer.show();
				curInfoBlock.hide();
				getDataBlank();
			}
			else {
				blankContainer.hide();
				curInfoBlock.show();
				
			}
		}
	}
	else if(currTabHref === '#panel2') {
		/**** clean validation ****/

		cleanValidation($('#categ').find('.category-block').find(".questionsLang"));
        cleanValidation($('#categ').find('.category-block').find(".answersLang"));
        cleanValidation($('#categories .category').find('.question-name'));
        $('#categ').find('.category-block').find(".validate-alert").remove();
        $('#categories .category').find(".validate-alert").remove();
        $('#send-form .items .item').find(".validate-alert").remove();

		$('#addingQuestions').hide();
		$('#addingLang').hide();
		$('.approvedDivShow').hide();
		$('.DivShow').hide();

		if ($('#specialty_status').text() === "підтверджено" ) {
			$('.approvedDivShow').show();
		}
		else if ( $('#specialty_status').text() === "на перевірці" ) {
			$('.DivShow').show();
		}
		else {
			if($('#type').text() == "1") {
				$('#addingLang').show();
			}
			else {
				$('#addingQuestions').show();
			}
			const selectContainer = $('.addSelectContainer'),
				chooseBlockSelect = $('#block_selector'),
				categoriesContainer = $('#categories'),
				curInfoBlock = $('.empty-content-add-task');

			if (nmbBlocks > 1) {
				chooseBlockSelect.val('');
				categoriesContainer.hide();
				if($('#type').text() == "0") {
					selectContainer.show();
				}
				curInfoBlock.hide();
			}
			else {
				categoriesContainer.hide();
				selectContainer.hide();
				curInfoBlock.show();
			}
			$('#panel2 .category-delete').hide();

			$('#limit-questions').addClass('hidden');
			$('#categ .category-delete').hide();
		}
	}
	else if(currTabHref === '#panel3') {

		let curInfoQuestion = $('#panel3 .empty-task');
		$('#lang_edit_block_element').hide();
		$('#editingQuestions').hide();
		$('.approvedDivShow').hide();
		$('.DivShow').hide();
		$('#langEmpty').hide();
		$('#langAnswerEdit').hide();
		$('#questions_edit_block_elements').hide();
		$('#form_edit_task').hide();
		$('#changingLang').hide();

		if ($('#specialty_status').text() === "підтверджено") {
			$('.approvedDivShow').show();
		}
		else if ( $('#specialty_status').text() === "на перевірці") {
			$('.DivShow').show();
		}
		else {

			const editTaskSelect = $('#panel3 .edit-task-select'),
				curInfoBlock = $('.empty-content-edit-task');

			if($('#type').text() == "1") {

				$('#lang_edit_block_element').show();
				getLangQuestions();
			}
			else {

				$('#editingQuestions').show();
				curInfoQuestion.addClass('hidden');
			}

			$('#block_selector_for_edit').val('');

			if (nmbBlocks > 1 && $('#type').text() != "1") {
				editTaskSelect.show();
				curInfoBlock.hide();
			}
			else if($('#type').text() != "1") {
				editTaskSelect.hide();
				curInfoBlock.show();
			}
		}
	}
	else if(currTabHref === '#edit-block') {

		const createBlockForm = $('#create-block-form'),
			editBlockContainer = $('.edit-block-container'),
			selectBlock = $('#choose-select');

		editBlockContainer.addClass('hidden');
		selectBlock.val('');

		$('#blockControl [data-toggle]').hide();

		$('#taskTitle').hide();
		$('#taskTitleEdit').hide();
		$('#engTextEdit').hide();
		$('#engText').hide();
		$('#blockControlling').hide();
		$('#blockCreateForm').hide();
		$('#editLangBlock').hide();
		$('.approvedDivShow').hide();
		$('.DivShow').hide();

		if ($('#specialty_status').text() === "підтверджено" ) {
			$('.approvedDivShow').show();
		}
		else if ( $('#specialty_status').text() === "на перевірці" ) {
			$('.DivShow').show();
		}
		else {
			if($('#type').text() == "1") {
				$('#editLangBlock').show();
			}
			else {
				const typeSpeciality = $('#type').text();
				createBlockForm.off('submit');
				if(typeSpeciality == "0") {
					createBlockForm.on('submit', e => createNewBlock(e));
				}
				else {
					createBlockForm.on('submit', e => createEnglishBlock(e));
				}
				fillEditForms(typeSpeciality);

				$('#blockControlling').show();
			}

		}
	}
	else if(currTabHref === '#verify') {
		$('#verify_no_questions').addClass('hidden');
		$('#comm').addClass('hidden');
		$('#choose_specialty').off('change', updateBlockTable);
		$('#choose_specialty').on('change', updateBlockTable);
		$('#block_table').addClass('hidden');
		$('#choose_specialty').val('');
		$("#full").addClass("hidden");
		$("#sendDiv").addClass("hidden");
		$('.blue-block').addClass('hidden');
	}
	else if(currTabHref === '#viewBlock') {
		$('#blockView').hide();
		$('#viewNoBlock').hide();
		updateViewSelect($('#id_speciality').val());
	}

	$(document).find('.bookshelf_wrapper').hide();
	$(document).find('.hideBehind').hide();
	$('#all').css("opacity", "1");
}

function updateBlockTable(){
	$("#comm,#sendDiv").addClass("hidden");
	$("#full").addClass("hidden");

	$.ajax({
		method: 'POST',
		url: '/get-speciality-param',
		data: {'id' : $("#choose_specialty").val()},
		success: function (response) {
			var table = $("#block_table");

			if(response.type == 1 && response.questions > 0) {

				$("#full").removeClass("hidden");
				$('#verify_no_questions').addClass('hidden');
				$("#comm,#sendDiv").removeClass("hidden");
				$("#comm").find('.category-block').remove();
				$('#sendDiv').prop("disabled", false);
				loadComm();
				$("#block_table").addClass("hidden");
				$(".blue-block").addClass("hidden");
				$("#full").addClass("hidden");
			}
			else if(response.type == 1) {
				$('#verify_no_questions').removeClass('hidden');
				$('#sendDiv').prop("disabled", true);
				$('#full').addClass("hidden");
				$("#block_table").addClass("hidden");
				$(".blue-block").addClass("hidden");
				$("#comm,#sendDiv").removeClass("hidden");
				$("#comm").find('.category-block').remove();
				$("#full").addClass("hidden");

				loadComm();
			}
			else if (response.questions > 0 || response.english_questions > 0 || response.connections > 0) {

				$("#full").removeClass("hidden");
				$('#verify_no_questions').addClass('hidden');
				$("#full").removeClass("hidden");
				$('#sendDiv').prop("disabled", true);
				$("#overCount").text("");

				$("#block_table").removeClass("hidden");
				$(".blue-block").removeClass("hidden");
				$("#full,#comm,#sendDiv").removeClass("hidden");
				$("#comm").find('.category-block').remove();
				table.html('');
				table.append(`<tr>
                                <th>Назва блоку</th>
                                <th>Кількість заповнених завдань в блоці</th>
                                <th>Кількість балів за завдання</th>
                                <th>Введіть кількість завдань в білеті</th>
                                <th>Сума балів за блок</th>
                            </tr>`);
				var spec = $("#choose_specialty").val();
				$.ajax({
					method: 'POST',
					url: '/get-stuff',

					success: function (response) {

						response.blocks.forEach(function (element) {
							if (element.id_speciality == spec) {

								var countFilledQuestions = 0;
								response.questions.forEach(function (el) {
									if (el.id_block == element.id) {
										countFilledQuestions++;
									}
								});

								response.english_questions.forEach(function (el) {
									if (el.block_id == element.id) {
										countFilledQuestions++;
									}
								});

								response.english_text.forEach(function (el) {
									if (el.block_id == element.id) {
										countFilledQuestions++;
									}
								});


								response.connection.forEach(function (el) {
									if (el.block_id == element.id) {
										countFilledQuestions++;
									}
								});

								if (element.type == "2") {
									table.append(`<tr> <td class = "hidden"><div class = "ids">${element.id}</div></td>
                                                <td><div>${element.block_name}</div></td>
                                                 <td><div>${countFilledQuestions}</div></td>
                                                 <td><div class = "markPer">${element.weight_question}</div></td>" +
                                                 <td><input type = "number" min = "0" max = "1" value = "${element.count_chosed_questions}" class = "inputTasks"></td>" +
                                                 <td><div class = "summ">${element.weight_question * element.count_chosed_questions} </div></td> 
                                             </tr>`);
								}
								else {
									table.append(`<tr> <td class = "hidden"><div class = "ids">${element.id}</div></td>
                                                <td><div>${element.block_name}</div></td>
                                                 <td><div>${countFilledQuestions}</div></td>
                                                 <td><div class = "markPer">${element.weight_question}</div></td>" +
                                                 <td><input type = "number" min = "0" max = "${countFilledQuestions}" value = "${element.count_chosed_questions}" class = "inputTasks"></td>" +
                                                 <td><div class = "summ">${element.weight_question * element.count_chosed_questions} </div></td> 
                                             </tr>`);
								}
							}
							$(".inputTasks").off('input');
							$(".inputTasks").on('input', updateSumm);
						});

						var sum = 0;

						$('.summ').each(function () {
							sum += parseFloat($(this).text());
						});

						$("#fullCount").text(sum);

						if (sum === 100) {
							$('#sendDiv').prop("disabled", false);
						}
						else {
							$('#sendDiv').prop("disabled", true);
						}
					},
					error: function (error) {
						console.log(error);
					}
				});
				loadComm();
			}
			else {
				
				$("#full").addClass("hidden");
				$(".blue-block").addClass("hidden");

				table.html('');

				$('#verify_no_questions').removeClass('hidden');

			}
		},
		error: function (error) {
			console.log(error);
		}
	});
}

function updateViewSelect(id){
	$.ajax({
		url: '/get_blocks',
		method: 'POST',
		data: {'id':id},
		success(response) {
			if(response.length > 0) {
				$('#blockView').show();

				var select = $('#choose_block_for_view');
				select.text('');
				response.forEach(function (block) {
					var newOption = `<option value="${block.id}" >${block.block_name}</option>`;
					select.append(newOption);
				});
				select.val('');
			}
			else {
				$('#viewNoBlock').show();
			}
		},
		error: function (error) {
			console.log(error);
		}
	});
}

function filterOptionSelect(select) {
	const idSpeciality = $('#id_speciality').val();

	let options = $(select).find("option:not([value='new_block'])"),
		nmbCurrentOptions = 0;

	options.map( (index, option) => {
		const specialityId = $(option).attr('data-id-speciality');

		if(specialityId === idSpeciality) {
			$(option).removeClass('hidden');
			nmbCurrentOptions++;
		}
		else {
			$(option).addClass('hidden');
		}
	});

	$(select).val('');
}

function getDataBlank() {
	const specialityId = $('#id_speciality').val();

	$.ajax({
		url: '/get-speciality-tasks',
		method: 'post',
		data: {
			id: specialityId,
			type: $('#type').text()
		},
		success(response) {
			renderBlank(response);

			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");
		},
		error(err) {
			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");

			console.log(err);
		},
		complete() {
			const currTab = $('.panel-heading .nav-tabs .active'),
			currTabHref = $(currTab).find('a').attr('href');

			if(currTabHref === '#panel1') {
				$('#panel1').show();
			}
		}
	});
}

function renderEnglish(data,id) {
	let result = "",
		q = 1;

	for (let conf of data.english_config) {
		if (conf.block_id == id) {
			let j = 1;

			for (let text of data.english_text) {
				if (text.block_id == id) {
					result += `<div class = "engQuestionsDiv"> Питання ${q} </div>`;
					q++;

					if (conf.answer_list == 1 && conf.text == 1) {

						for (let question of data.english_questions) {
							if (question.id_english_question == text.id) {

								result += `<li class = "question_name" style="margin-left:70px">`;

								if (conf.true_false == 0) {
									result += question.right_answer;
								} else {
									result += `${j}_____`;
									j++;
								}

								result += ` ${question.text_question} </li>`;
							}
						}

						j = 1;
						let i = 1,
							english_text = text.text;

						result += `<li class = "question_name">`;

						while (english_text.indexOf('NNN') !== -1) {
							english_text = english_text.replace(/NNN/, `${i}_____`);
							i++;
						}

						result += `${english_text}</li>`;
						j = 1;

						for (let question of data.english_questions) {
							if (question.id_english_question == id) {
								j++;

								for (let answer of data.english_answers) {
									if (answer.id_english_question == question.id) {
										let contentAnswer = answer.answer_right ?
											`<b>${answer.text_answer}</b>` :
											answer.text_answer;

										result += `<div class = "englishAnsStep" >${contentAnswer}</div>`;
									}
								}
								result += `</li>`;
							}
						}

						j = 1;
					} else if (conf.text == 1 && conf.answer_list == 0) {

						let english_text = text.text;

						result += `<li class = "question_name">`;
						let i = 1;

						while (english_text.indexOf('NNN') != -1) {
							english_text = english_text.replace(/NNN/, `${i}_____`);
							i++;
						}
						result += `${english_text}</li>`;

						j = 1;
						for (let question of data.english_questions) {
							if (question.id_english_question == text.id) {
								result += `<li class = "question_name"> <div class = "answerEngNumb" > ${j} </div> ${question.text_question}`;
								j++;

								let ans = "A";

								for (let answer of data.english_answers) {
									if (answer.id_english_question == question.id) {

										let contentAnswer = answer.answer_right ?
											`<b>${ans} ${answer.text_answer}</b>` :
											`${ans} ${answer.text_answer}`;

										result += `<div class = "englishAnsStep" >${contentAnswer}</div>`;
										ans = String.fromCharCode(ans.charCodeAt(0) + 1);
									}
								}

								result += `</li>`;
							}
						}
						j = 1;
					}
				}
			}

			if (conf.text == 0 && conf.answer_list == 0) {

				j = 1;

				for (let question of data.english_questions) {
					if (question.block_id == id) {
						result += `<li class = "question_name"> <div class = "answerEngNumb" > ${j} </div> ${question.text_question}`;
						j++;
						let ans = "A";

						for (let answer of data.english_answers) {
							if (answer.id_english_question == question.id) {
								let contentAnswer = answer.answer_right ?
									`<b>${ans} ${answer.text_answer}</b>` :
									`${ans} ${answer.text_answer}`;

								result += `<div class = "englishAnsStep">${contentAnswer}</div>`;
								ans = String.fromCharCode(ans.charCodeAt(0) + 1);
							}
						}
						result += `</li>`;
					}
				}
			}
			else if (conf.answer_list == 1 && conf.text == 0) {

				for (let connection of data.connections) {
					if (connection.block_id == id) {
						j = 1;
						let ans = "A",
							row = `<tr>`;

						result += `<table class = "taskTable"><tr>`;

						for (let currAnswer of connection['answers']) {
							result += `<td> ${currAnswer.text_question} </td>`;
							row += `<th>${ans}</th>`;
							ans = String.fromCharCode(ans.charCodeAt(0) + 1);
						}

						result += `</tr>` + row + `</tr></table>`;
						result += `<li class = "question_name">`;

						for (let currQuestion of connection['questions']) {
							result += `<div class = "englishAnsStep"> ${j} ${currQuestion.text_question} </div>`;
							j++;
						}

						result += `</li>`;
					}
				}
			}
		}
	}
	return result;
}

function renderBlank(data) {
	const blankContainer = $('#blank');

	blankContainer.html('');

	let blocks = data.blocks;

	for(let block of blocks) {
		blankContainer.append(`<li><h2>Блок: ${block['block_name']}</h2></li>`);

		if(block.task_title != null) {
			blankContainer.append(`<li><b>Умова завдання: ${block['task_title']}</b></li>`);
		}

		if(block.type == "1") {
			blankContainer.append(`<li>${block['blank_text']}</li>`);
		}

		if(block.url_image != '0') {
			blankContainer.append(`<li><b> </b><div class="image-container">
                                                        <div class="responsiveImg" style="background-image: url('${block['url_image']}')"></div>
            </div></li>`);
		}

		let questions = [];

		if ($('#type').text() == "2") {
			let blockyBlock = renderEnglish(data,block.id);
			blankContainer.append(blockyBlock);
		}
		else if (block['questions'].length) {
			blankContainer.append(`<ol class="questions questions-block-${block['id']}"></ol>`);
			let question = '';

			for (let currQuestion of block['questions']) {
				question += `<li>
                                <p>${currQuestion['text_question']}</p>
                             </li>`;

				question += '<ul class="answers">';

				for (let answer of currQuestion['answers']) {
					question +=  answer['answer_right'] ? `<b><li> <p>${answer['text_answer']}</p></li></b>` : `<li> <p>${answer['text_answer']}</p></li></p>`;
				}
				question += '</ul>';
			}

			questions.push(question);

			blankContainer.find(`.questions-block-${block['id']}`).append(questions);
		}
		else {
			blankContainer.append('<li class="no-content">Питання до даного блоку відсутні</li>');
		}
	}
}

function getLangShow() {
	var spec_id = $('#id_speciality').val();

	$.ajax({
		url: '/get-ukrainian-tasks',
		method: 'post',
		data: {spec_id: spec_id},
		success(response) {
			var questions = "";

			$.ajax({
				url: '/set_lang_block',
				method: 'POST',
				data: {'id':spec_id},
				success(res) {
					const blankContainer = $('#langBlank');
					if(response.answers.length === 0) {
						$('#errorNoTasks').show();
						blankContainer.hide();
					}
					else {
						$('#errorNoTasks').hide();
						blankContainer.show();
						blankContainer.html('');

						questions += "<h2>" + res + "</h2>";

						for (let i = 0; i < response.size; i++) {
							questions += `<ol class = "row"><span class="form-group col-xs-6">
                                        <label>Питання</label>
                                        <p>${response.questions[i].text_question}</p>
                                    </span>
                                    <span class="form-group col-xs-6">
                                        <label>Відповідь</label>
                                        <p>${response.answers[i]}</p>
                                    </span></ol>`;
						}

						$('#get_lang_edit').val('');
						blankContainer.append(questions);
					}
				},
				error: function (error) {
					console.log(error);
				}
			});

		},
		error(err) {
			console.log(err);
		},
		complete() {
			var tab = $('.panel-heading .nav-tabs .active').find('a').attr('href');
			if(tab === '#panel1') {
				$('#panel1').show();
			}
		}
	});
}

module.exports = {
	manageContentTab: manageContentTab,
	filterOptionSelect: filterOptionSelect
};