const getSettings = require('./getSettings');
const {renderFirstAnswers} = require('../renderFirstQuestion');
const {
	listColContentChoices,
	newRowAnswerList,
	newRowList,
	newManageRowBlock,
	booleanAnswer,
	newTaskContainer,
	newTextBlock,
	newListAnswerBlock,
	newListBlock,
	questionContainer,
	newTestBlock,
	manageButtonsBlock,
	questionMultiblock
} = require('./taskElements');
const resetEnglishTasks = require('./resetTasks');
const {redactorMultiblock} = require('../../../redactorMultiblock');
const {setWidthImage, handleChangeRadio} = require('../images');
const getImageSrc = require('../getImageSrc');
const {imageWidthChoices} = require('../../../modules/constants');
const createSwal = require('../createSwal').createSwal;
const setWidthListeners = require('../../../SetAutoWidth');

let settings = null;

const renderEnglishTask = () => {

	resetEnglishTasks();
	const manageButtonsBlock = $('#panel2 .manage-buttons-block');
	manageButtonsBlock.show();

	$('#panel2 .task-container .adding-btns').hide();
	$('#panel2 .task-container').show();
	$('#categories').find('p').show();
	$('#categories').find('button').last().show();

	if($('#panel2 .text-block').is(":visible")) {
		$('#panel2 .text-block').find('.html').click();
		$('#panel2 .text-block').find('.multiblock').val('');
		$('#panel2 .text-block').find('.html').click();
	}

	if($('#panel2 .questions-block').is(":visible")) {
		$('#panel2 .questions-block').find('.html').click();
		$('#panel2 .questions-block').find('.multiblock').val('');
		$('#panel2 .questions-block').find('.html').click();
	}

	settings = getSettings();
	return settings.then( (result) => {
		let addEnglishTaskBtn = $('#panel2 .add-english-task'),
			{isText, isListAnswer, isBooleanAnswers, nmbQuestions, nmbAnswers, extraAnswers} = result,
			container, textContainer, listContainer, listAnswerContainer, questionContainer, answersContainer, addNewAnswerBtn;

		const activeTab = $('#block_selector').is(':visible') ? 'add' : 'edit';

		if(activeTab === 'add') {
			container = $('#panel2 #categories');
			textContainer = $('#panel2 .text-block');
			listContainer = $('#panel2 .list-block');
			questionContainer = $('#panel2 .category-block');
			answersContainer = $('#panel2 #first_answer_item');
			addNewAnswerBtn = $('#panel2 #categories .addNewAnswer');
			listAnswerContainer = $('#panel2 .list-answers-block');

			let imageWidthContainer = textContainer.find('.imageWidthContainer').first();

			imageWidthContainer.next().remove();
			imageWidthContainer.after(questionMultiblock);
			imageWidthContainer.next().find('.text-question').on('click', redactorMultiblock);

			imageWidthContainer = questionContainer.find('.imageWidthContainer').first();

			imageWidthContainer.next().remove();
			imageWidthContainer.after(questionMultiblock);
			imageWidthContainer.next().find('.text-question').on('click', redactorMultiblock);
		}
		else {
			container = $('#panel3 #editingQuestions');
			textContainer = $('#panel3 .text-block');
			listContainer = $('#panel3 .list-block');
			questionContainer = $('#panel3 .category-block');
			answersContainer = $('#panel3 #first_edit_item');
			addNewAnswerBtn = $('#panel3 #editingQuestions .addNewAnswer');
			listAnswerContainer = $('#panel3 .list-answers-block');

			$('#panel3 button[type="submit"]').show();
			$('#panel3 .questions-block').show();
		}

		container.show();
		answersContainer.empty();

		if(!isText) {
			textContainer.hide();
			listContainer.hide();

			if (isListAnswer) {
				const tableAnswersRows = $(listAnswerContainer).find('table tbody');

				tableAnswersRows.empty();
				for(let indexRow = 0; indexRow < nmbQuestions; indexRow++) {
					tableAnswersRows.append(newRowAnswerList(indexRow));
				}
				listAnswerContainer.show();

				let contentCol = 'list',
					tableQuestionsRows = $(listContainer).find('table tbody');

				listContainer.find('.change-content-col').text(listColContentChoices[contentCol]);

				tableQuestionsRows.empty();

				const initialIndexRow = 0;

				if(!isBooleanAnswers && $('#block_selector_for_edit').is(':visible')) {
					let number = parseInt($('#tasks_selector_for_edit option:selected').attr('data-width'));

					for(let i = 0; i < number; i++) {
						tableQuestionsRows.append(newRowList(nmbQuestions, initialIndexRow, isBooleanAnswers, 0,'askii'));
					}
				}
				else {
					tableQuestionsRows.append(newRowList(nmbQuestions, initialIndexRow, isBooleanAnswers, 0,'askii'));
				}

				tableQuestionsRows.append(newManageRowBlock);
				listContainer.show();
			}
		}
		else {
			textContainer.show();
			const textQuestion = $('#panel2 .text-block .html');

			if($(textQuestion).is(':visible')) {
				textQuestion.click();
				$('#panel2 .text-question').val('');
				textQuestion.click();
			}

			if(isListAnswer) {
				let contentCol = isBooleanAnswers ? 'bool' : 'default',
					tableRows = $(listContainer).find('table tbody');

				listContainer.find('.change-content-col').text(listColContentChoices[contentCol]);

				tableRows.empty();
				let indexRow = 0;
				for(indexRow; indexRow < nmbQuestions + extraAnswers; indexRow++) {
					tableRows.append(newRowList(nmbQuestions, indexRow, isBooleanAnswers, extraAnswers));
				}

				listContainer.show();
			}
			else {
				listContainer.hide();
			}
		}

		// just for displaying questions
		if(!isListAnswer) {
			let nmbAnswersToRender = isText ? nmbAnswers : nmbQuestions;

			questionContainer.show();

			if (isBooleanAnswers) {
				addNewAnswerBtn.hide();
				answersContainer.append(booleanAnswer);
			}
			else {
				addNewAnswerBtn.show();
				renderFirstAnswers(nmbAnswersToRender, answersContainer);
			}

			if(isText) {
				$('#panel2 .category-block:first').find('.category').remove();
				$('#panel2 .category-block:first').find('.imageWidthContainer').after(questionMultiblock);
				$('#panel2 .category-block:first').find('.text-question').on('click', redactorMultiblock);

				let questionBlock = $('#panel2 .questions-block');

				if($('#block_selector_for_edit').is(':visible')) {
					questionBlock = $('#panel3 .questions-block');
				}
				// render nmbQuestions - 1 questions
				for(let indexQuestion = 1; indexQuestion < nmbQuestions; indexQuestion++) {
					let testBlock = $(newTestBlock(activeTab)),
						answersContainer = testBlock.find('.answers-container');

					questionBlock.append(testBlock);

					if (isBooleanAnswers) {
						answersContainer.append(booleanAnswer);
					}
					else {
						renderFirstAnswers(nmbAnswersToRender, answersContainer);
					}
				}
			}
		}
		else {
			questionContainer.hide();
		}

		addEnglishTaskBtn.show();
		updateListeners();

		$('#panel2 input[type="radio"][value="100"]').prop('checked', true);

		if($('.question-name').closest('.category-block').is(':visible')) {
			const questionBlock = $('#panel2 .questions-block:first .html');
			questionBlock.click();
			$('#panel2 .questions-block:first .question-name').val('');
			questionBlock.click();
		}
	});
};

const addEnglishTask = (e) => {
	e.preventDefault();
	const activeTab = 'add';

	settings.then( (result) => {
		const lastTask = $('#panel2 .task-container:last'),
			rmEnglishBtns = $('#panel2 .rm-english-task'),
			{isText, isListAnswer, isBooleanAnswers, nmbQuestions, nmbAnswers, extraAnswers} = result;

		let newTask = $(newTaskContainer);

		if(isText) {
			newTask.append(newTextBlock());

			if(isListAnswer) {
				let listContainer = $(newListBlock);
				newTask.append(listContainer);

				let contentCol = isBooleanAnswers ? 'bool' : 'default',
					tableRows = $(listContainer).find('table tbody');

				listContainer.find('.change-content-col').text(listColContentChoices[contentCol]);

				let indexRow = 0;
				for(indexRow; indexRow < nmbQuestions + extraAnswers; indexRow++) {
					tableRows.append(newRowList(nmbQuestions, indexRow, isBooleanAnswers, extraAnswers));
				}
			}
		}
		else {
			if(isListAnswer) {
				const listAnswerContainer = $(newListAnswerBlock);
				newTask.append(listAnswerContainer);

				const tableAnswersRows = $(listAnswerContainer).find('table tbody');

				for(let indexRow = 0; indexRow < nmbQuestions; indexRow++) {
					tableAnswersRows.append(newRowAnswerList(indexRow));
				}

				let listContainer = $(newListBlock),
					contentCol = 'list',
					tableQuestionsRows = $(listContainer).find('table tbody');

				newTask.append(listContainer);

				listContainer.find('.change-content-col').text(listColContentChoices[contentCol]);

				const initialIndexRow = 0;
				tableQuestionsRows.append(newRowList(nmbQuestions, initialIndexRow, isBooleanAnswers, 0, 'askii'));
				tableQuestionsRows.append(newManageRowBlock);
			}
		}

		if(!isListAnswer) {
			let testContainer = $(questionContainer),
				testBlock = $(newTestBlock(activeTab, newTask)),
				answersContainer = testBlock.find('.answers-container'),
				nmbAnswersToRender = isText ? nmbAnswers : nmbQuestions;

			testContainer.append(testBlock);
			newTask.append(testContainer);

			if (isBooleanAnswers) {
				answersContainer.append(booleanAnswer);
			}
			else {
				renderFirstAnswers(nmbAnswersToRender, answersContainer);
			}

			if(isText) {
				for(let indexQuestion = 1; indexQuestion < nmbQuestions; indexQuestion++) {
					let testContainer = $(questionContainer),
						testBlock = $(newTestBlock(activeTab, newTask)),
						answersContainer = testBlock.find('.answers-container');
					testContainer.append(testBlock);
					newTask.append(testContainer);
					if (isBooleanAnswers) {
						answersContainer.append(booleanAnswer);
					}
					else {
						renderFirstAnswers(nmbAnswersToRender, answersContainer);
					}
				}
			}
		}

		newTask.append(manageButtonsBlock);
		lastTask.after(newTask);

		rmEnglishBtns.show();

		updateListeners();
	});
};

const rmEnglishTask = (e) => {
	e.preventDefault();

	const countTasks = $('#panel2 .task-container').length,
		rmTaskBtn = $('#panel2 .task-container .rm-english-task');

	if(countTasks >= 2) {
		const tasksContainer = [...$('#panel2 .task-container')],
			currBtn = $(e.currentTarget),
			currTaskContainer = tasksContainer.filter( (taskContainer) => {
				return $(taskContainer).find(currBtn).length > 0
			});

		$(currTaskContainer).remove();
	}

	if(countTasks <= 2) {
		rmTaskBtn.hide();
	}
};

function validateEnglish() {
	let taskContainers = $('.task-container');
	let boxRight = 0;
	let countRight = 0;
	let valRight = 0;
	let questionRight = 0;
	let questionType = 0;
	let filledQuestions = 0;
	let question;

	let textItems = $('.task-container .item-name');
	let boxes = 0;

	for(let area of taskContainers) {
		let Containers = $(area).find('.category-block:visible');
		let currentCount = 0;
		question = 0;
		let text = $(area).find('.text-block:visible .text-question');

		if(text.length > 0) {
			let countNNN = ($(text).val().match(/NNN/g) || []).length;

			if ($('#answersCount').val() != countNNN) {
				currentCount = 1;
				countRight = 1;
			}

			if ($(text).val() === '') {
				valRight = 1;
			}
		}

		for (let container of Containers) {
			let anyBoxesChecked = false;

			$(container).find('input[type="checkbox"]').each(function () {
				boxes++;
				if ($(this).is(":checked")) {
					anyBoxesChecked = true;
				}
			});

			if (!anyBoxesChecked && boxes > 0) {
				boxRight = 1;
			}

			let questionText = $(container).find('.category  .question-name, .category  .text-question');

			if(questionText) {

				if ($(questionText).val() == '') {
					question = 1;
				}
				if ($(questionText).val()) {
					filledQuestions = 1;
				}
			}
		}

		let questions = $(Containers).find('.category .question-name, .category  .text-question');

		if (question == 1 && currentCount == 1) {
			questionRight = 1;
		}

		if(questions.length > 0 && text.val()) {
			questionType = 1;
		}
		else if(questions.length > 0 && !text.val()) {
			questionType = 2;
		}
	}

	for(let area of textItems) {
		if($(area).val() == '') {
			valRight = 1;
		}
	}

	let errorMessage = "Неправильно заповнені завдання:\n";

	if(countRight == 1 && questionType == 0 && $('#answersCount').val() != "0") {
		errorMessage += "- в тексті має бути присутня необхідна кількість вставок питань('NNN')\n";
	}

	if(valRight == 1) {
		errorMessage += "- заповніть всі необхідні поля\n";
	}

	if(boxRight == 1) {
		errorMessage += "- оберіть правильний варіант для усіх завдань\n";
	}

	if(question == 1 && filledQuestions == 1){
		errorMessage += "- заповніть усі умови завдань\n";
	}

	if(questionRight == 1 && questionType == 1 ) {
		errorMessage += "- вставте необхідну кількість питань в текст АБО заповніть умови завдань";
	}

	if((countRight == 1 && questionType == 0 && $('#answersCount').val() != "0") || valRight == 1 || boxRight == 1 || (questionRight == 1 && questionType == 1) || (question == 1 && filledQuestions == 1)) {
		createSwal({
			title: errorMessage,
			type: 'error'
		});
		return false;
	}
	else {
		return true;
	}
}

const saveEnglishTask = (e) => {
	settings.then((result) => {
		let valid = validateEnglish();

		if(valid) {
			$(document).find('.bookshelf_wrapper').show();
			$(document).find('.hideBehind').show();
			$('#all').css("opacity", "0.5");

			const tasks = $(e.currentTarget).find('.task-container'),
				idBlock = $('#block_selector').val(),
				{isText, isListAnswer, isBooleanAnswers} = result;

			let resultToSend = [];
			for (let task of tasks) {
				let textBlock, listBlockRows,
					taskToSend = {idBlock};

				if (isText) {
					textBlock = $(task).find('.text-block');
					let imageWidthContainer = $(textBlock).find('.imageWidthContainer'),
						checkedCheckbox = imageWidthContainer.find('input[type="radio"]:checked').val(),
						imageWidth = checkedCheckbox !== 'custom' ?
							checkedCheckbox :
							imageWidthContainer.find('.customWidthInput').val(),
						[content, images] = getImageSrc(textBlock.find('.text-question').val());

					if(textBlock.find('.enableAutoWidth').hasClass('btn-success')) {
						imageWidth = 'auto';
					}

					taskToSend.textData = {
						content,
						images,
						imageWidth
					};

					if (isListAnswer) {
						listBlockRows = $(task).find('.list-block table tbody tr');

						taskToSend.listData = [...listBlockRows].map((row) => {
							let textColData = $(row).find('.textCol input'),
								indexLetter = $(row).find('td:first').text(),
								resultCol = $(row).find('.resultCol'),
								keyName = isBooleanAnswers ? 'boolCol' : 'selectCol',
								resultColData = {indexLetter},
								dataKey = isBooleanAnswers ? 'isChecked' : 'position';

							resultColData[dataKey] = resultCol.find('select').val();

							return {
								textCol: textColData.val(),
								[keyName]: resultColData
							};
						});
					}
				}
				else {
					if (isListAnswer) {
						let listBlockRows, listAnswerRows;

						listAnswerRows = $(task).find('.list-answers-block table tbody tr');

						taskToSend.listAnswerData = [...listAnswerRows].map((row) => {
							let indexLetter = $(row).find('td:first').text(),
								textData = $(row).find('.textCol input').val();

							return {
								indexLetter,
								textData,
							}
						});

						listBlockRows = $(task).find('.list-block table tbody tr:not(.adding-row)');

						taskToSend.listData = [...listBlockRows].map((row) => {
							let textColData = $(row).find('.textCol input'),
								indexLetter = $(row).find('td:first').text(),
								resultCol = $(row).find('.resultCol'),
								keyName = 'selectCol',
								resultColData = {indexLetter};

							resultColData.position = resultCol.find('select').val();

							return {
								textCol: textColData.val(),
								[keyName]: resultColData
							};
						});
					}
				}

				if (!isListAnswer) {
					let questionData = {},
						questions = $(task).find('.category-block');

					if (isBooleanAnswers) {
						questionData = [...questions].map((question) => {
							let questionTextarea = $(question).find('.question-name'),
								booleanAnswer = $(question).find('.items .booleanAnswer'),
								imageWidthContainer = $(question).find('.imageWidthContainer'),
								checkedCheckbox = imageWidthContainer.find('input[type="radio"]:checked').val(),
								imageWidth = checkedCheckbox !== 'custom' ?
									checkedCheckbox :
									imageWidthContainer.find('.customWidthInput').val();

							if($(question).find('.enableAutoWidth').hasClass('btn-success')) {
								imageWidth = 'auto';
							}

							return {
								question: getImageSrc(questionTextarea.val()),
								isCorrect: booleanAnswer.find('input[type="checkbox"]').is(':checked') ? 1 : 0,
								imageWidth
							}
						});
					}
					else {
						questionData = [...questions].map((question) => {
							let questionTextarea = $(question).find('.question-name'),
								answers = $(question).find('.items .item'),
								imageWidthContainer = $(question).find('.imageWidthContainer'),
								checkedCheckbox = imageWidthContainer.find('input[type="radio"]:checked').val(),
								imageWidth = checkedCheckbox !== 'custom' ?
									checkedCheckbox :
									imageWidthContainer.find('.customWidthInput').val();

							if($(question).find('.enableAutoWidth').hasClass('btn-success')) {
								imageWidth = 'auto';
							}

							if(typeof questionTextarea.val() === 'undefined') {
								questionTextarea = $(question).find('.text-question');
							}

							return {
								question: getImageSrc(questionTextarea.val()),
								answers: [...answers].map((answer) => {
									let answerTextarea = $(answer).find('.item-name'),
										answerCheckbox = $(answer).find('.correct-answer');

									return {
										answer: getImageSrc(answerTextarea.val()),
										isChecked: answerCheckbox.is(':checked') ? 1 : 0
									}
								}),
								imageWidth
							}
						});
					}

					taskToSend.questions = questionData;
				}
				resultToSend.push(taskToSend)
			}

			let taskPromises = resultToSend.map( (taskData) => {
				return $.ajax({
					url: '/create-english-task',
					method: 'post',
					data: taskData,
				});
			});

			Promise.all(taskPromises)
				.then( () => {
					createSwal({
						title: 'Завдання успішно збережено',
						type: 'success'
					});

					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					$('#send-form button').prop('disabled', false);

					$('.task-container').hide();
					$('#categories').find('p').hide();
					$('#categories').find('button').last().hide();
					$('#block_selector').val('');

					resetEnglishTasks();
				})
				.catch( (err) => {
					createSwal({
						title: 'Помилка при сбереженні завдання',
						type: 'error'
					});

					$('#send-form button').prop('disabled', false);

					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					console.log(err);
				});
		}
		else {
			$('#send-form button').prop('disabled', false);
		}
	});
};

const onChangeListSelect = (e) => {
	let curSelect =  $(e.currentTarget),
		prevVal = curSelect.attr('data-previous-val'),
		curVal = curSelect.val(),
		prevSelect = curSelect.closest('tbody').find(`select[data-previous-val="${curVal}"]`);

	const booleanContent = ['Ні', 'Так', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'];
	if(!booleanContent.includes($(curSelect).find('option:selected').text().trim())) {
		curSelect.find(`option[value="${curVal}"]`).prop('selected', true);
		prevSelect.find(`option[value="${prevVal}"]`).prop('selected', true);

		curSelect.attr('data-previous-val', curVal);
		prevSelect.attr('data-previous-val', prevVal);
	}
};

const addQuestionRow = (e, nmbQuestions, isBooleanAnswers) => {
	e.preventDefault();

	const maxRows = 15,
		currBtn = $(e.currentTarget),
		listBlocks = $(currBtn).closest('.list-block'),
		currContainer = [...listBlocks].filter( (listBlock) => $(listBlock).find(currBtn).length > 0),
		rmBtn = $(currContainer).find('.rmQuestionRow'),
		tableRows = $(currContainer).find('tbody tr:not(.adding-row)'),
		tableRowsNmb = tableRows.length;

	if(tableRowsNmb < maxRows) {
		$(currContainer).find('.adding-row').before(newRowList(nmbQuestions, tableRowsNmb, isBooleanAnswers, 0,'askii'));
	}
	else {
		createSwal({title: `Максимальна кількість питань - ${maxRows}.`, type: 'error'});
	}

	rmBtn.removeAttr('disabled');
};

const rmQuestionRow = (e) => {
	e.preventDefault();
	const currBtn = $(e.currentTarget),
		listBlocks = $(currBtn).closest('.list-block'),
		currContainer = [...listBlocks].filter( (listBlock) => $(listBlock).find(currBtn).length > 0),
		tableRows = $(currContainer).find('tbody tr:not(.adding-row)'),
		tableRowsNmb = tableRows.length,
		rowToRemove = tableRows.last();

	if(tableRowsNmb == 2) {
		currBtn.attr('disabled', true);
	}
	if(tableRowsNmb > 1) {
		$(rowToRemove).remove();
	}
};

const updateListeners = () => {
	settings.then( (result) => {
		let {isText, isListAnswer, nmbQuestions, isBooleanAnswers} = result,
			mainContainer = $('#block_selector_for_edit').is(':visible') ? $('#panel3') : $('#panel2'),
			addEnglishTaskBtn = mainContainer.find('.add-english-task'),
			rmEnglishTaskBtn = mainContainer.find('.rm-english-task'),
			listBlock = mainContainer.find('.list-block'),
			listSelect = listBlock.find('select'),
			listAddBtn = listBlock.find('.addQuestionRow'),
			listRmBtn = listBlock.find('.rmQuestionRow'),
			multiblocks = mainContainer.find('.multiblock'),
			addImageWidthRadio = $(mainContainer).find('.imageWidthContainer input[type="radio"]'),
			addChangeWidthBtn = $(mainContainer).find('.changeWidthBtn');

		addEnglishTaskBtn.off('click');
		addEnglishTaskBtn.on('click', addEnglishTask);

		rmEnglishTaskBtn.off('click');
		rmEnglishTaskBtn.on('click', rmEnglishTask);

		listSelect.off('change');
		listSelect.on('change', onChangeListSelect);

		listAddBtn.off('click');
		listAddBtn.on('click', (e) => addQuestionRow(e, nmbQuestions, isBooleanAnswers));

		listRmBtn.off('click');
		listRmBtn.on('click', rmQuestionRow);

		multiblocks.off('click');
		multiblocks.on('click', redactorMultiblock);

		addImageWidthRadio.off('change');
		addImageWidthRadio.on('change', (e) => handleChangeRadio(e));
		addChangeWidthBtn.off('click');
		addChangeWidthBtn.on('click', (e) => {
			e.preventDefault();

			const imageWidth = $(e.currentTarget).prev().val();

			setWidthImage(imageWidth, $(e.currentTarget).prev().prev().find('input'));
		});

		setWidthListeners();
	});
};

const getEngQuestions = () => {
	settings = getSettings();
	$('#tasks_selector_for_edit').html("");

	$(document).find('.bookshelf_wrapper').show();
	$(document).find('.hideBehind').show();
	$('#all').css("opacity","0.5");

	settings.then( (result) => {
		$.ajax({
			url: '/get_eng_questions',
			method: 'post',
			data: result,
			success(response) {
				if(response.length > 0) {
					const key = result['isListAnswer'] || result['isText'] ? 'text' : 'text_question';
					$('#questions_edit_block_elements [data-toggle="tooltip"] button').prop("disabled", false);

					for(let question of response) {

						let replacedText = removeHTML(question[key]);
						if(replacedText == "") {
							$('#tasks_selector_for_edit').append(`
								<option value="${question['id']}" 
									name = "image" 
									data-width="${question['image_width']}"
								>
									Зображення
								</option>
							`);
						}
						else {
							$('#tasks_selector_for_edit').append(`
								<option value="${question['id']}"
	                            	name = "normal" 
	                            	data-width="${question['image_width']}"
	                            >
									${replacedText}
								</option>
							`);
						}
					}

					$('#questions_edit_block_elements').show();
				}
				else {
					$('#questions_edit_block_elements').hide();

					createSwal({
						title: 'Питання до даного блоку відсутні',
						type: 'info'
					});
					editTaskSelect.val('');
				}

				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity","1");
			},
			error(err) {
				console.log(err);
			}
		});
	});
};

const deleteEngQuestion = () => {
	settings.then( (result) => {
		const isConfirm = confirm('Ви дійсно хочете видати дане завдання?');

		if(isConfirm) {
			$(document).find('.bookshelf_wrapper').show();
			$(document).find('.hideBehind').show();
			$('#all').css("opacity","0.5");

			let data = {
				id: $('#tasks_selector_for_edit option:selected').val(),
				settings: result
			};

			$.ajax({
				url: '/delete_eng_question',
				method: 'post',
				data: data,
				success() {
					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity","1");

					$('#form_for_change_task').hide();
					$('#tasks_selector_for_edit option:selected').remove();
					if($('#tasks_selector_for_edit option').length < 1) {
						$('#questions_edit_block_elements').hide();
					}

					createSwal({
						title: 'Завдання успішно видалено',
						type: 'success'
					});
				},
				error(err) {
					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity","1");

					createSwal({
						title: 'Помилка при видаленні завдання',
						type: 'error'
					});
					console.log(err);
				}
			});
		}
	});
};

const getEngQuestion = () => {
	settings.then( (result) => {
		let data = {
			id: $('#tasks_selector_for_edit option:selected').val(),
			settings: result
		};
		$('#panel3 .addNewAnswer').hide();
		$('#panel3 .task-container').show();

		renderEnglishTask().then( () => {
			$.ajax({
				url: '/get_eng_question',
				method: 'post',
				data: data,
				success(response) {
					let title = $('#tasks_selector_for_edit option:selected'),
						textBlock = $('#panel3 .text-block'),
						listAnswers = $('#panel3 .list-answers-block'),
						listBlock = $('#panel3 .list-block'),
						questionBlock = $('#panel3 .questions-block'),
						categoryBlock = $(questionBlock).find('.category-block'),
						settings = data['settings'],
						text = response['text'],
						question = response['question'],
						answers = response['answers'],
						imageWidth = $(title).attr('data-width');

					if(!data['settings']['isListAnswer'] || data['settings']['isText']) {
						if(imageWidth === 'auto') {
							textBlock.find('.enableAutoWidth').click();
						}
						else {
							if (imageWidthChoices.includes(imageWidth)) {
								$(`#panel3 input[type="radio"][value="${imageWidth}"]:first`).click();
							}
							else {
								$(`#panel3 input[type="radio"][value="custom"]:first`).click();
								$(`#panel3 .customWidthInput:first`).prop('disabled', false);
								$(`#panel3 .customWidthInput:first`).val(imageWidth);
								$(`#panel3 .changeWidthBtn:first`).prop('disabled', false);
							}
						}
					}

					if(settings['isListAnswer']) {
						if(settings['isText']) {
							$(textBlock).find('.category').remove();
							$(textBlock).find('.imageWidthContainer').after(questionMultiblock);
							$(textBlock).find('.text-question').on('click', redactorMultiblock);

							textBlock.find('.text-question').val(text);

							for (let i = 0; i < answers.length; i++) {
								$(listBlock.find('input[type="text"]')[i]).val(answers[i]['text_question']);
								$(listBlock.find('input[type="text"]')[i]).attr('data-id', answers[i]['id']);
							}

							for (let i = 0; i < answers.length; i++) {
								let curSelect = $(listBlock.find('select')[i]);
								curSelect.attr('data-previous-val', answers[i]['position']);

								curSelect.find(`option[value="${answers[i]['position']}"]`).attr('selected', true);
							}
						}
						else {
							let inputs = $(listAnswers).find('input');

							for(let i = 0; i < question.length; i++) {
								$(inputs[i]).val(question[i]['text_question']);
							}

							inputs = $(listBlock).find('input');

							if(inputs.length > 1) {
								$('#panel3 .rmQuestionRow').removeAttr('disabled');
							}

							for(let i = 0; i < answers.length; i++) {
								$(inputs[i]).val(answers[i]['text_question']);
								$(inputs[i]).attr('data-id', answers[i]['id']);
								$(inputs[i]).closest('tr').find('td:first').text(i + 1);
								$(listBlock.find('select')[i]).find(`option[value="${answers[i]['right_answer']}"]`).attr('selected', true);
							}
						}
					}
					else if(settings['isText']) {
						$(textBlock).find('.category').remove();
						$(textBlock).find('.imageWidthContainer').after(questionMultiblock);
						$(textBlock).find('.text-question').on('click', redactorMultiblock);

						textBlock.find('.text-question').val(text);

						for(let i = 0; i < question.length; i++) {
							if(!i) {
								$(categoryBlock[i]).find('.category').remove();
								$(categoryBlock[i]).find('.imageWidthContainer').after(questionMultiblock);
								$(categoryBlock[i]).find('.text-question').on('click', redactorMultiblock);

								$(categoryBlock[i]).find('.text-question').val(question[i]['text_question']);
								$(categoryBlock[i]).find('.text-question').attr('data-id', question[i]['id']);
							}
							else {
								$(categoryBlock[i]).find('.question-name').val(question[i]['text_question']);
								$(categoryBlock[i]).find('.question-name').attr('data-id', question[i]['id']);
							}

							imageWidth = question[i]['image_width'];

							if(imageWidth !== 'auto') {
								if (imageWidthChoices.includes(imageWidth)) {
									$(categoryBlock[i]).find(`input[type="radio"][value="${imageWidth}"]`).prop('checked', true);
								} else {
									$(categoryBlock[i]).find(`input[type="radio"][value="custom"]`).prop('checked', true);
									$(categoryBlock[i]).find(`.customWidthInput`).prop('disabled', false);
									$(categoryBlock[i]).find(`.customWidthInput`).val(imageWidth);
									$(categoryBlock[i]).find(`.changeWidthBtn`).prop('disabled', false);
								}
							}
							else {
								$(categoryBlock[i]).find('.enableAutoWidth').click();
							}

							for(let j = 0; j < answers[i].length; j++) {
								$($(categoryBlock[i]).find('.item-name')[j]).val(answers[i][j]['text_answer']);
								$($(categoryBlock[i]).find('.item-name')[j]).attr('data-id', answers[i][j]['id']);
								$($(categoryBlock[i]).find('input[type="checkbox"]')[j]).attr('checked', answers[i][j]['answer_right'] != 0);
							}
						}
					}
					else {
						$(questionBlock).find('.category').remove();
						$(questionBlock).find('.imageWidthContainer').after(questionMultiblock);
						$(questionBlock).find('.text-question').on('click', redactorMultiblock);

						if(imageWidth !== 'auto') {
							if (imageWidthChoices.includes(imageWidth)) {
								$(questionBlock).find(`input[type="radio"][value="${imageWidth}"]`).click();
							} else {
								$(questionBlock).find(`input[type="radio"][value="custom"]`).click();
								$(questionBlock).find(`.customWidthInput`).prop('disabled', false);
								$(questionBlock).find(`.customWidthInput`).val(imageWidth);
								$(questionBlock).find(`.changeWidthBtn`).prop('disabled', false);
							}
						}
						else {
							$(questionBlock).find('.enableAutoWidth').click();
						}

						$(questionBlock).find('.text-question').val(question);

						for(let i = 0; i < answers.length; i++) {
							$(questionBlock.find('.item-name')[i]).val(answers[i]['text_answer']);
							$(questionBlock.find('.item-name')[i]).attr('data-id', answers[i]['id']);
							$(questionBlock.find('input[type="checkbox"]')[i]).attr('checked', answers[i]['answer_right'] != 0);
						}
					}

				},
				error(err) {
					createSwal({
						title: 'Помилка при спробі отримання данних',
						type: 'error'
					});
					console.log(err);
				}
			})
			.then(function() {
				$('#form_edit_task').show();
				$('#panel3 .multiblock').each(function() {
					if($(this).is(':visible') || ($(this).parent().hasClass('jHtmlArea') && $(this).parent().is(':visible'))) {
						$(this).click();
					}
				})
			});
		});
	});
};

const updateEditEng = () => {
	settings = getSettings();

	let valid = validateEnglish();

	if(valid) {
		settings.then((result) => {
			$(document).find('.bookshelf_wrapper').show();
			$(document).find('.hideBehind').show();
			$('#all').css("opacity", "0.5");

			let data,
				title = $('#tasks_selector_for_edit option:selected'),
				textBlock = $('#panel3 .text-block'),
				listAnswers = $('#panel3 .list-answers-block'),
				listBlock = $('#panel3 .list-block'),
				questionBlock = $('#panel3 .questions-block'),
				imageWidth,
				block = textBlock.is(':visible') ? textBlock : questionBlock;

			if (block.find('input[type="radio"]:checked').val() !== 'custom') {
				imageWidth = block.find('input[type="radio"]:checked').val();
			} else {
				imageWidth = block.find('.customWidthInput').val();
			}

			if (block.find('.enableAutoWidth').hasClass('btn-success')) {
				imageWidth = 'auto';
			}

			if (result['isListAnswer']) {
				if (result['isText']) {
					let [content, images] = getImageSrc($(textBlock).find('.text-question').val());

					let editQuestion = {
						id: $(title).val(),
						text: {content, images},
						image_width: imageWidth
					}, editAnswers = [];

					for (let i = 0; i < $(listBlock).find('input[type="text"]').length; i++) {
						editAnswers[i] = {
							id: $($(listBlock).find('input[type="text"]')[i]).attr('data-id'),
							text_question: $($(listBlock).find('input[type="text"]')[i]).val(),
							right_answer: $($(listBlock).find('.indexCol')[i]).text(),
							position: $($(listBlock).find('select')[i]).find('option:selected').val()
						};
					}

					data = {
						settings: result,
						question: editQuestion,
						answers: editAnswers
					};
				} else {
					let editQuestion = [],
						editAnswers = [],
						inputs = $(listAnswers).find('input'),
						ids = $(title).val().split(' ');

					for (let i = 0; i < inputs.length; i++) {
						editQuestion[i] = {
							id: ids[i],
							text_question: $(inputs[i]).val()
						}
					}

					inputs = $(listBlock).find('input');

					for (let i = 0; i < inputs.length; i++) {
						editAnswers[i] = {
							id: typeof $(inputs[i]).attr('data-id') !== 'undefined' ? $(inputs[i]).attr('data-id') : null,
							text_question: $(inputs[i]).val(),
							right_answer: $($(listBlock).find('select')[i]).find('option:selected').val()
						}
					}

					data = {
						settings: result,
						question: editQuestion,
						answers: editAnswers
					};
				}
			} else if (result['isText']) {
				let [content, images] = getImageSrc($(textBlock).find('.text-question').val()),
					editTitle = {
					id: $(title).val(),
					text: {content, images},
					image_width: imageWidth
				}, editQuestion = [],
					editAnswers = [];

				for (let i = 0; i < $(questionBlock).find('.category-block').length; i++) {
					let content,
						images = null,
						categoryBlock = $(questionBlock).find('.category-block')[i],
						temp = [];

					if ($(categoryBlock).find('input[type="radio"]:checked').val() !== 'custom') {
						imageWidth = $(categoryBlock).find('input[type="radio"]:checked').val();
					} else {
						imageWidth = $(categoryBlock).find('.customWidthInput').val();
					}

					if($(categoryBlock).find('.enableAutoWidth').hasClass('btn-success')) {
						imageWidth = 'auto';
					}

					if (!i) {
						let [content, images] = getImageSrc($(categoryBlock).find('.text-question').val());

						editQuestion[i] = {
							id: $(categoryBlock).find('.text-question').attr('data-id'),
							text_question: {content, images},
							image_width: imageWidth
						};
					} else {
						[content, images] = getImageSrc($(categoryBlock).find('.question-name').val());

						editQuestion[i] = {
							id: $(categoryBlock).find('.question-name').attr('data-id'),
							text_question: {content, images},
							image_width: imageWidth
						};
					}

					for (let j = 0; j < $(categoryBlock).find('.item-name').length; j++) {
						[content, images] = getImageSrc($($(categoryBlock).find('.item-name')[j]).val());

						temp[j] = {
							id: $($(categoryBlock).find('.item-name')[j]).attr('data-id'),
							text_answer: {content, images},
							answer_right: $($(categoryBlock).find('input[type="checkbox"]')[j]).is(':checked')
						};
					}

					editAnswers[i] = temp;
				}

				data = {
					settings: result,
					title: editTitle,
					question: editQuestion,
					answers: editAnswers
				};
			} else {
				let [content, images] = getImageSrc($(questionBlock).find('.text-question').val()),
					editQuestion = {
						id: $(title).val(),
						text_question: {content, images},
						image_width: imageWidth
					},
					editAnswers = [];

				for (let i = 0; i < $(questionBlock).find('.item-name').length; i++) {
					let [content, images] = getImageSrc($($(questionBlock).find('.item-name')[i]).val());

					editAnswers[i] = {
						id: $($(questionBlock).find('.item-name')[i]).attr('data-id'),
						text_answer: {content, images},
						answer_right: $($(questionBlock).find('input[type="checkbox"]')[i]).is(':checked')
					};
				}
				data = {
					settings: result,
					question: editQuestion,
					answers: editAnswers
				};
			}
			$.ajax({
				url: '/update_edit_eng',
				method: 'post',
				data: data,
				success() {
					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					$('#form_edit_task button:submit').prop('disabled', false);

					getEngQuestions();
					resetEnglishTasks();

					createSwal({
						title: 'Завдання успішно змінено',
						type: 'success'
					});
				},
				error(err) {
					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					$('#form_edit_task button:submit').prop('disabled', false);

					createSwal({
						title: 'Помилка при збереженні змін завдання',
						type: 'error'
					});
					console.log(err);
				}
			});
		});
	}
	else {
		$('#form_edit_task button:submit').prop('disabled', false);
	}
};

function removeHTML(str) {
	if(str) {
		while(str.indexOf('<') != -1 && str.indexOf('>') != -1 && str.indexOf('>') > str.indexOf('<')) {
			str = str.slice(0, str.indexOf('<')) + str.slice(str.indexOf('>') + 1, str.length);
		}
	}
	return str;
}

module.exports = {
	renderEnglishTask: renderEnglishTask,
	saveEnglishTask: saveEnglishTask,
	getEngQuestions: getEngQuestions,
	deleteEngQuestion: deleteEngQuestion,
	getEngQuestion: getEngQuestion,
	updateEditEng: updateEditEng,
	onChangeListSelect: onChangeListSelect
};