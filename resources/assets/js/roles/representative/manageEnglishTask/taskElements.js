const newTaskContainer = `<div class="task-container"></div>`;

const booleanAnswer = `
	<div class="row booleanAnswer">
		<label>
			Правильна відповідь
			<input type="checkbox">
		</label>			
	</div>
`;

const listColContentChoices = {
	'bool': 'Відповідь вірна?',
	'list': 'Відповідний номер в таблиці',
	'default': 'Відповідний номер в тексті',
};

const fillOptions = (isBoolAnswers, nmbQuestions, indexRow, typeSymbolSelect, extraAnswers) => {
	let result = [];

	if(isBoolAnswers) {
		const basicChoice = [`<option value="1">Так</option>`, `<option value="0">Ні</option>`],
			currRow = indexRow + 1;
		result.push(basicChoice);

		let optionContent, optionValue;

		for(let index = 1; index <= extraAnswers; index++) {
			optionContent = `Додатковие питання - ${index}`;
			optionValue = `extra-answer - ${index}`;

			result.push(`
				<option 
					value="${optionValue}"
					${ currRow - nmbQuestions === index ? 'selected' : ''}
					>
						${optionContent}
					</option>`);
		}
	}
	else {
		for(let index = 1; index <= nmbQuestions + extraAnswers; index++) {
			let firstLetterAskii = 64,
				optionContent, optionValue, currIndex;
			if(index > nmbQuestions) {
				currIndex = index - nmbQuestions;

				optionContent = `Додатковие питання - ${currIndex}`;
				optionValue = `extra-answer - ${currIndex}`;
			}
			else {
				optionContent = typeSymbolSelect === 'int' ?
					index :
					String.fromCharCode(firstLetterAskii + index);
			}

			result.push(`
			<option 
				value="${optionValue ? optionValue : optionContent}"
				${ indexRow + 1 === index  ? 'selected' : ''}
			>
				${optionContent}
			</option>
		`);
		}
	}

	return result;
};

const insertContentCol = (isBoolAnswers, nmbQuestions, indexRow, typeSymbolSelect, extraAnswers) => {
	const firstLetterAskii = 65,
		currIndex = indexRow + 1;
	let prevVal, extraIndex;

	if(isBoolAnswers) {
		const indexExtraAnswers = currIndex - nmbQuestions;
		prevVal = indexExtraAnswers > 0 ?
			`extra-answer - ${indexExtraAnswers}` :
			'1';
	}
	else {
		if(indexRow >= nmbQuestions) {
			extraIndex = currIndex - nmbQuestions;
			prevVal = `extra-answer - ${extraIndex}`;
		}
		else {
			prevVal = typeSymbolSelect === 'int' ?
				currIndex :
				String.fromCharCode(firstLetterAskii + indexRow);
		}
	}

	return (`
		<select data-previous-val="${prevVal}">
			${fillOptions(isBoolAnswers, nmbQuestions, indexRow, typeSymbolSelect, extraAnswers)}
		</select>
	`);
};

const newRowAnswerList = (indexRow) => {
	const firstLetterAskii = 65,
		symbolIndex = String.fromCharCode(firstLetterAskii + indexRow);
	return (
		`<tr>
			<td>${symbolIndex}</td>
			<td class="textCol"><input type="text" maxlength="100" required></td>
		</tr>`
	);
};

const newRowList = (nmbQuestions, indexRow, isBooleanAnswers, extraAnswers, typeSymbolSelect = 'int') => {
	const firstLetterAskii = 65,
		startIndex = 1,
		symbolIndex = isBooleanAnswers || typeSymbolSelect === 'askii' ?
			startIndex + indexRow :
			String.fromCharCode(firstLetterAskii + indexRow);

	return (
		`<tr>
			<td class="indexCol">${symbolIndex}</td>
			<td class="textCol"><input type="text" maxlength="100" required></td>
			<td class="resultCol">
				${insertContentCol(isBooleanAnswers, nmbQuestions, indexRow, typeSymbolSelect, extraAnswers)}
			</td>
		</tr>`
	);
};

const newManageRowBlock = () => {
	return (
		`<tr class="adding-row">
			<td colspan="2">
				<buttton class="btn btn-default addQuestionRow">
					<i class="fas fa-plus-circle"></i>
					Додати ще один варіант		
				</buttton>
			</td>
			<td>
				<buttton class="btn btn-danger rmQuestionRow" disabled>
					<i class="fas fa-times-circle"></i>
					Видалити останній варіант		
				</buttton>
			</td>
		</tr>`);
};

const newTextBlock = () => {
	const taskIndex = $('#panel2 .task-container').length + 1;
	return (
		`<div class="text-block">
			<h2>Текст до питання:</h2>
			<div class="imageWidthContainer im_regculation" id="0">
				<div class="row col-xs-10">
					<span>Вкажіть ширину зображень:</span>
					<label><input type="radio" name="image-width-radio-text-${taskIndex}" value="50">50px</label>
					<label><input type="radio" name="image-width-radio-text-${taskIndex}" value="75">75px</label>
					<label><input type="radio" name="image-width-radio-text-${taskIndex}" value="100" checked>100px</label>
					<label><input type="radio" name="image-width-radio-text-${taskIndex}" value="125">125px</label>
					<label><input type="radio" name="image-width-radio-text-${taskIndex}" value="150">150px</label>
					<label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-text-${taskIndex}" aria-label="image-width"></label>
					<input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>
					<button class = "changeWidthBtn btn btn-info" disabled>Змінити</button>
					<button class="enableAutoWidth btn btn-danger">Auto/off</button>
				</div>
			</div>
			<div class="row category" style="margin-bottom: 5px">
				<div class="col-xs-12 form-inline" style="width: 100%">
					<div class="popupImageConvertor">
						<div class="popup-container">
							<h3 class="resize-title">Вставка зображення</h3>
							<div class="resizer" >
								<span class="addition">ctrl+v</span>
							</div>
							<button class="btn btn-info resizer-result">Отримати зображення</button>
							<button class="closePopup">&#10006;</button>
						</div>
					</div>
					<textarea class="multiblock text-question"></textarea>
				</div>
			</div>
		</div>`);
};

const newListAnswerBlock = `
	<div class="list-answers-block">
		<table>
			<thead>
				<tr>
					<th>№</th>
					<th>Варіант</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>`;


const newListBlock = `
	<div class="list-block">
		<table>
			<thead>
			<tr>
				<th>№</th>
				<th>Текст питання</th>
				<th class="change-content-col"></th>
			</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>`;

const questionContainer = `<div class="questions-block"></div>`;

const newTestBlock = (activeTab, taskContainer = null) =>  {
	/*
		find last task container and count nmb questions, we can't add question on existing task
		if it's adding new task we couldn't find new container
	*/
	let indexQuestion;

	if(activeTab === 'add') {
		const currTaskContainer = taskContainer ? taskContainer : $('#panel2 .task-container:last'),
			nmbQuestions = currTaskContainer.find('.category-block').length;

		indexQuestion = taskContainer ? $('#panel2 .category-block').length + nmbQuestions + 1: nmbQuestions + 1;
	}
	else {
		const currTaskContainer = $('#panel3 .task-container'),
			nmbQuestions = currTaskContainer.find('.questions-block .category-block').length;

		indexQuestion = nmbQuestions + 1;
	}

	return (
		`<div class="category-block">
				<div class="imageWidthContainer im_regculation">
					<div class="row col-xs-10">
						<span>Вкажіть ширину зображень:</span>
						<label><input type="radio" name="image-width-radio-${indexQuestion}" value="50">50px</label>
						<label><input type="radio" name="image-width-radio-${indexQuestion}" value="75">75px</label>
						<label><input type="radio" name="image-width-radio-${indexQuestion}" value="100" checked>100px</label>
						<label><input type="radio" name="image-width-radio-${indexQuestion}" value="125">125px</label>
						<label><input type="radio" name="image-width-radio-${indexQuestion}" value="150">150px</label>
						<label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-${indexQuestion}" aria-label="image-with"></label>
						<input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>
						<button class = "changeWidthBtn btn btn-info" disabled>Змінити</button>
						<button class="enableAutoWidth btn btn-danger">Auto/off</button>
					</div>
				</div>
				<div class="row category" style="margin-bottom: 5px">
					<div class="col-xs-12 form-inline" style="width: 100%">
						<div class="popupImageConvertor">
							<div class="popup-container">
								<h3 class="resize-title">Вставка зображення</h3>
								<div class="resizer" >
									<span class="addition">ctrl+v</span>
								</div>
								<button class="btn btn-info resizer-result">Отримати зображення</button>
								<button class="closePopup">&#10006;</button>
							</div>
						</div>
						<textarea class="multiblock question-name"></textarea>
					</div>
				</div>
			
				<div class="items answers-container"></div>
			</div>
		`
	);
};

const manageButtonsBlock = (isFirstRender = false) => {
	return `
		<div class="manage-buttons-block">
			<button class="btn btn-info add-english-task">Додати завдання</button>
			<button class="btn btn-danger rm-english-task" ${isFirstRender ? 'style="display: none;"' : ''}>Видалити завдання</button>
		</div>`;
};

const questionMultiblock = () => {
	return `<div class="row category" style="margin-bottom: 5px">
					<div class="col-xs-12 form-inline" style="width: 100%">
						<div class="popupImageConvertor">
							<div class="popup-container">
								<h3 class="resize-title">Вставка зображення</h3>
								<div class="resizer" >
									<span class="addition">ctrl+v</span>
								</div>
								<button class="btn btn-info resizer-result">Отримати зображення</button>
								<button class="closePopup">&#10006;</button>
							</div>
						</div>
						<textarea class="multiblock text-question"></textarea>
					</div>
				</div>`;
};

const normalMultiblock = () => {
	return `<div class="row category" style="margin-bottom: 5px">
					<div class="col-xs-12 form-inline" style="width: 100%">
						<div class="popupImageConvertor">
							<div class="popup-container">
								<h3 class="resize-title">Вставка зображення</h3>
								<div class="resizer" >
									<span class="addition">ctrl+v</span>
								</div>
								<button class="btn btn-info resizer-result">Отримати зображення</button>
								<button class="closePopup">&#10006;</button>
							</div>
						</div>
						<textarea class="multiblock text-question"></textarea>
						<div class="adding-btns">
							<button type="button" class="btn btn-default addNewAnswer">
								<span class="glyphicon glyphicon-plus"></span>
								Додати варіант
							</button>
						</div>
					</div>
				</div>`;
};

module.exports = {
	listColContentChoices,
	booleanAnswer,
	newTaskContainer,
	newRowAnswerList,
	newRowList,
	newManageRowBlock,
	newTextBlock,
	newListAnswerBlock,
	newListBlock,
	questionContainer,
	newTestBlock,
	manageButtonsBlock,
	questionMultiblock,
	normalMultiblock
};