module.exports = resetEnglishTasks = () => {
	$('.enableAutoWidth.btn-success').click();

	if($('#block_selector_for_edit').is(':visible')) {
		const categoryBlock = $('#panel3 .category-block:not(:first)');

		categoryBlock.remove();
		$('#form_edit_task').hide();

		$('#panel3 .list-answers-block').hide();
		$('#panel3 .list-block').hide();
		$('#panel3 button[type="submit"]').hide();
		$('#panel3 .text-block').hide();
		$('#panel3 .questions-block').hide();
		$('#panel3 .customWidthInput').attr('disabled', true);
		$('#panel3 .customWidthInput').val('');
		$('#panel3 .changeWidthBtn').attr('disabled', true);
	}
	else {
		const tasksToRemove = $('#panel2 .task-container:not(:first)'),
			textBlock = $('#panel2 .text-block'),
			listBlock = $('#panel2 .list-block'),
			rowsListToDelete = listBlock.find('table tbody tr'),
			listAnswerBlock = $('#panel2 .list-answers-block'),
			rowsListAnswerToDelete = listAnswerBlock.find('table tbody tr'),
			testBlock = $('#panel2 .category-block'),
			testQuestion = testBlock.find('.category .question-name'),
			answersContainer = testBlock.find('.items'),
			manageButtonsBlock = $('#panel2 .manage-buttons-block'),
			deleteTaskBtn = $('#panel2 .task-container .rm-english-task'),
			questionToRemove = $('#panel2 .questions-block .category-block:not(:first)');

		$('#panel2 .customWidthInput').prop('disabled', true);
		$('#panel2 .customWidthInput').val('');
		$('#panel2 .changeWidthBtn').prop('disabled', true);

		questionToRemove.remove();
		tasksToRemove.remove();
		textBlock.hide();

		listAnswerBlock.hide();
		listBlock.hide();
		rowsListToDelete.remove();
		rowsListAnswerToDelete.remove();

		answersContainer.empty();

		testQuestion.val('');
		testBlock.hide();

		manageButtonsBlock.hide();
		deleteTaskBtn.hide();
	}
};