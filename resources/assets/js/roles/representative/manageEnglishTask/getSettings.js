module.exports = getSettings = () => {
	const nmbAnswers = 4;

	let id = $('#block_selector option:selected').val();

	if($('#block_selector_for_edit').is(':visible')) {
		id = $('#block_selector_for_edit option:selected').val();
	}

	let data = {
		'block_id': id
	};

	return new Promise( (resolve) => {
		$.ajax({
			url: '/get_settings',
			method: 'post',
			data: data,
			success(response) {
				let setting = response['settings'];

				if(setting.true_false == 0 && setting.text == 1)
				$('#answersCount').val(response.countAnswers);
				else $('#answersCount').val(0);
				let englishConfiguration = {
					isText: !!setting['text'],
					isListAnswer: !!setting['answer_list'],
					isBooleanAnswers: !!setting['true_false'],
					extraAnswers: setting['extra_answers'],
					nmbQuestions: response['countAnswers'],
					nmbAnswers,
					blockId: data['block_id']
				};

				resolve(englishConfiguration);
			},
			error(err) {
				console.log(err);
			}
		});
	});
};