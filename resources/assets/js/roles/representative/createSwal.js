function createSwal(params) {
	swal({
		...params,
		closeOnClickOutside: true,
		allowOutsideClick: true
	});
}

module.exports = {
	createSwal: createSwal
};