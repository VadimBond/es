function getImageSrc(str) {
	var arraySrc = str.match(/src="(.*?)"/g);

	if(arraySrc != null) {
		var newarray =[];
		for (var i = 0; i < arraySrc.length; i++) {
			newarray.push(arraySrc[i].match(/"(.*?)"/)[1]);
		}
		str = str.replace(/src="(.*?)"/g,'src="URLPATH"');
		return [str, newarray];
	}
	else {
		return [str, ''];
	}
}

module.exports = getImageSrc;