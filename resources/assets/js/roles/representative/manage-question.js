const createSwal = require('./createSwal').createSwal;
const redactorMultiblock = require('../../redactorMultiblock').redactorMultiblock;
const addNewAnswer = require('./addNewAnswer').addNewAnswer;
const getImageSrc = require('./getImageSrc');
const {setDefaultImageWidth, setWidthImage, addListenersToImages} = require('./images');
const {renderEnglishTask, updateEditEng} = require('./manageEnglishTask/manageTasks');
const {normalMultiblock} = require('./manageEnglishTask/taskElements');
const setWidthListeners = require('../../SetAutoWidth');

const langBlock = `<div class="category-block" style="display: block;">
						<div class="imageWidthContainer im_regculation">
							<div class="row col-xs-10">
								<span>Вкажіть ширину зображень:</span>
								<label><input type="radio" name="image-width-radio-2" value="50">50px</label>
								<label><input type="radio" name="image-width-radio-2" value="75">75px</label>
								<label><input type="radio" name="image-width-radio-2" value="100" checked>100px</label>
								<label><input type="radio" name="image-width-radio-2" value="125">125px</label>
								<label><input type="radio" name="image-width-radio-2" value="150">150px</label>
								<label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-2" aria-label="image-with"></label>
								<input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>
								<button class = "changeWidthBtn btn btn-info" disabled>Змінити</button>
								<button class="enableAutoWidth btn btn-danger">Auto/off</button>
							</div>
						</div>
                        <div class="row category" style="margin-bottom: 5px">
                            <div class="col-xs-12 form-inline" style="width: 100%">
                                    <div class="lang-item">
                                        <div class="lang-area">
                                            <div class="popupImageConvertor">
                                                <div class="popup-container">
                                                    <h3 class="resize-title">Вставка зображення</h3>
                                                    <div class="resizer" >
                                                        <span class="addition">ctrl+v</span>
                                                    </div>
                    
                                                    <button class="resizer-result btn btn-info" class="btn btn-info">Вставити виділене зображення</button>
                                                    <button class="closePopup">&#10006;</button>
                                                   
                                                </div>
                                            </div>
                                    
                                            <textarea id="lang_question" maxlength="1000" class="multiblock form-control text_area_lang"></textarea>
                                         </div>
                                         <div class="lang-area">
                                            <div class="popupImageConvertor">
                                                <div class="popup-container">
                                                    <h3 class="resize-title">Вставка зображення</h3>
                                                    <div class="resizer" >
                                                        <span class="addition">ctrl+v</span>
                                                    </div>
                    
                                                    <button class="resizer-result btn btn-info" class="btn btn-info">Вставити виділене зображення</button>
                                                    <button class="closePopup">&#10006;</button>
                                                   
                                                </div>
                                            </div>
                                            <textarea id="lang_answers" maxlength="1000" class="multiblock form-control text_area_lang"></textarea>
                                        </div>
                                    </div>
                                <div class="btn-space">
                                    <span class="category-delete" data-toggle="tooltip" title="" data-original-title="Видалити питання">
                                      <button id="deleteLang" type="button" class="btn btn-default">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"> Видалити питання</span>
                                      </button>
                                    </span>
								</div>
                            </div>
                        </div>
                    </div>`;

$('#panel3 .multiblock').off('click');
$('#panel3 .multiblock').on('click', redactorMultiblock);

var countAnswers = $('#block_selector_for_edit option:selected').attr('name');

const setCustomImageWidth = () => {
	const addChangeWidthBtn = $('#panel3 .changeWidthBtn');

	addChangeWidthBtn.off('click');
	addChangeWidthBtn.on('click', (e) => {
		e.preventDefault();

		const imageWidth = $(e.currentTarget).prev().val();

		setWidthImage(imageWidth, $(e.currentTarget).prev().prev().find('input'));
	});
};

function getLangQuestions() {
	var spec_id = $('#id_speciality').val();

	$.ajax({
		url: '/get-ukrainian-tasks',
		method: 'post',
		data: {spec_id: spec_id},
		success(response) {
			$('#get_lang_edit').html('');

			if(response.size > 0) {
				for(let i = 0; i< response.size;i++) {
					var text = response.questions[i].text_question;
					text = removeHTML(text);
					if(text == "") {
						text = "Зображення";
					}

					$('#get_lang_edit').append($('<option>', {
						id: response.questions[i].text_question,
						value: response.questions[i].id,
						text: text.substr(0,120) + '...',
						name: response.answers[i],
						'data-width': response.questions[i].image_width
					}));
				}

				$('#panel3 .empty-task').addClass('hidden');
				$('#lang_edit_block_element').removeClass('hidden');
			}
			else {
				$('#langEmpty').show();
				$('#panel3 .empty-task').removeClass('hidden');
				$('#lang_edit_block_element').addClass('hidden');
			}

			$('#get_lang_edit').val('');
		},
		error(err) {
			console.log(err);
		}
	});
}

function getQuestionLang() {
	if($('#changingLang .category-block').length === 0) {
		$('#changingLang').prepend(langBlock);
		$('#changingLang .multiblock').on('click', redactorMultiblock);
		setWidthListeners();
		addListenersToImages();
	}

	setCustomImageWidth();
	$('.enableAutoWidth.btn-success').click();

	$('#changingLang').show();

	var imageWidth = $('#get_lang_edit option:selected').attr('data-width'),
		radio = $('#changingLang').find('input[type="radio"]');

	if(imageWidth !== 'auto') {
		$(radio[5]).parent().next().prop('disabled', true);
		$(radio[5]).parent().next().next().prop('disabled', true);
		$('#changingLang .customWidthInput').val('');
		switch (imageWidth) {
			case '50':
				$(radio[0]).prop('checked', true);
				break;
			case '75':
				$(radio[1]).prop('checked', true);
				break;
			case '100':
				$(radio[2]).prop('checked', true);
				break;
			case '125':
				$(radio[3]).prop('checked', true);
				break;
			case '150':
				$(radio[4]).prop('checked', true);
				break;
			default:
				$(radio[5]).prop('checked', true);
				$(radio[5]).parent().next().prop('disabled', false);
				$(radio[5]).parent().next().next().prop('disabled', false);
				$(radio[5]).parent().next().val(imageWidth);
		}
	}
	else {
		$('#changingLang .enableAutoWidth').click();
	}

	$('#lang_question').val($('#get_lang_edit option:selected').attr('id'));
	$('#lang_answers').val($('#get_lang_edit option:selected').attr("name"));
	$('#panel3 #lang_answers').click();
	$('#panel3 #lang_question').click();

	$('#panel3 .html.highlighted').click();
}

function getQuestions() {
	countAnswers = $('#block_selector_for_edit option:selected').attr('name');

	$('#tasks_selector_for_edit').html("");

	const editTaskSelect =  $('#block_selector_for_edit'),
		idBlock = editTaskSelect.val(),
		selectQuestionContainer = $('#questions_edit_block_elements');

	$.ajax({
		url: '/get-questions',
		method: 'post',
		data: {idBlock: idBlock},
		success(response) {
			if(response.length > 0) {
				$('#questions_edit_block_elements [data-toggle="tooltip"] button').prop("disabled", false);
				for(let question of response) {
					question['text_question'] = removeHTML(question['text_question']);
					if(question['text_question'] == "") {
						$('#tasks_selector_for_edit').append(`<option value="${question['id']}" name = "image" >Зображення</option>`);
					}
					else {
						$('#tasks_selector_for_edit').append(`<option value="${question['id']}" name = "normal">${question['text_question']}</option>`);
					}
				}

				selectQuestionContainer.show();
			}
			else {
				selectQuestionContainer.hide();

				createSwal({
					title: 'Питання до даного блоку відсутні',
					type: 'info'
				});
				editTaskSelect.val('');
			}

		},
		error(err) {
			createSwal({
				title: 'Помилка при спробі отримати дані блоку',
				type: 'error'
			});
		}
	});
}

function getQuestion() {
	$('#panel3 .imageWidthContainer .customWidthInput').attr('disabled', true);
	$('#panel3 .imageWidthContainer .changeWidthBtn').attr('disabled', true);
	$('#panel3 .imageWidthContainer .customWidthInput').val('');

	$('#first_edit_item').empty();

	$('#panel3 .text-block').hide();
	$('#panel3 .list-block').hide();
	$('#panel3 .list-answers-block').hide();
	$('#panel3 .category-block:not(:first)').remove();

	$('#panel3 .category-block .category').remove();
	$('#panel3 .category-block .imageWidthContainer').after(normalMultiblock);
	$('#panel3 .category-block .text-question').on('click', redactorMultiblock);
	$('#panel3 .category-block .addNewAnswer').on('click', addAnswerField);

	if($('#type') == 2) {
		$('#insert_question_thing').show();
	}
	else {
		$('#insert_question_thing').hide();
	}

	$('#questions_edit_block_elements [data-toggle="tooltip"] button').prop("disabled", false);

	const idQuestion = $("#tasks_selector_for_edit").val();

	$.ajax({
		url: '/get-question',
		method: 'post',
		data: {idQuestion: idQuestion},
		success(response) {
			const imageWidth = response.question[0].image_width;

			setDefaultImageWidth(imageWidth);

			$('#answer_items_edit').html('');
			const questionBlock = $('#form_edit_task .questions-block'),
				question = questionBlock.find('.category .text-question');

			question.val(response['question'][0].text_question);
			insertAnswers(response['answers']);

			var addAnswer = $('#panel3 .addNewAnswer');
			addAnswer.off('click');
			addAnswer.on('click', addAnswerField);
			addAnswer.show();

			$('#tasks_selector_for_edit option:selected').attr('name', response['answers'].length);

			let multiblocks = $('#panel3 .multiblock');
			multiblocks.off('click');
			multiblocks.on('click', redactorMultiblock);

			$('#form_edit_task').show();
			questionBlock.show();
			questionBlock.find('.category-block').show();

			addListenersToImages();

			$('#panel3 .text-question').click();
			$('#panel3 .item-name').click();

			setCustomImageWidth();
		},
		error(err) {
			createSwal({
				title: 'Помилка при спробі отримання данних',
				type: 'error'
			});
			console.log(err);
		}
	});

	function insertAnswers(answers) {
		const answersContainer = $('#first_edit_item');
		for(let [i,answer] of answers.entries()) {
			let ansName = answer.text_answer,
				isRadio = true;

			if(typeof answer['text_answer'] === 'undefined') {
				isRadio = false;
			}

			answersContainer.append(addNewAnswer(answer, false, !isRadio, false, isRadio, isRadio));

			const multiblocks = $('#panel3 .multiblock');
			multiblocks.off('click');
			multiblocks.on('click', redactorMultiblock);

			if(countAnswers <= i){
				answersContainer.find('.deleteAnswer').show();
			}
		}
		answersContainer.find('.deleteAnswer').off('click');
		answersContainer.find('.deleteAnswer').on('click', deleteAnswer);

		answersContainer.find('input:file').on('click', e => {
			const input = $(e.currentTarget);
			input.attr('data-is-change', true);
			input.off('click');
		});
	}
}

function addAnswerField() {
	const answersContainer = $('#answer_items_edit');
	answersContainer.append(addNewAnswer(null, false, false, false, true, true));

	const multiblocks = $('#panel3 .multiblock');
	multiblocks.off('click');
	multiblocks.on('click', redactorMultiblock);

	$('#panel3 .item-name').last().click();

	$('.deleteAnswer').show();

	answersContainer.find('.deleteAnswer').off('click');
	answersContainer.find('.deleteAnswer').on('click', deleteAnswer);
	addListenersToImages();
}

function deleteAnswer() {
	var answersContainer = $(this).parent().parent().parent().parent().parent(),
		nmbTask = answersContainer.find('.item').length - 1;

	countAnswers = $('#block_selector option:selected').attr('name');

	if($('#tasks_selector_for_edit').is(':visible')) {
		answersContainer = $(this).closest('#form_edit_task');
		countAnswers = $('#tasks_selector_for_edit option:selected').attr('name');
		nmbTask = answersContainer.find('.answer-item').length / 2 - 1;
	}

	if(countAnswers ===  (nmbTask).toString()) {
		answersContainer.find('.deleteAnswer').hide();
	}

	var id = $(this).parent().find('.item-name').attr('data-id-answer');
	$.ajax({
		url: 'delete_answer',
		method: 'post',
		data: {
			'id': id,
		},
		error(err) {
			createSwal({
				title: 'Помилка при видаленні відповіді',
				type: 'error'
			});
		}
	});

	$(this).parent().parent().parent().parent().remove();
}

function handleEditTask() {
	const editFormTask = $('#form_edit_task');
	$(editFormTask).off('submit');

	$(editFormTask).on('submit', (e) => {
		e.preventDefault();

		$('#form_edit_task button:submit').prop('disabled', true);

		const typeSpeciality = $('#type').text();

		if(typeSpeciality === '2') {
			updateEditEng();
		}
		else {
			EditTasks();
		}
		$('#form_edit_task button:submit').prop('disabled', false);
	});

	$('#updateLang').off('click');
	$('#updateLang').on('click', EditLangTask);

	$('#deleteLang').off('click');
	$('#deleteLang').on('click', DeleteLangTask);

	function DeleteLangTask() {
		const isConfirm = confirm('Ви дійсно хочете видати дане завдання?');
		if (isConfirm) {
			$(document).find('.bookshelf_wrapper').show();
			$(document).find('.hideBehind').show();
			$('#all').css("opacity", "0.5");

			$.ajax({
				url: '/delete-ukrainian-task',
				method: 'post',
				data: {
					'spec_id': $('#id_speciality').val(),
					'question_id': $('#get_lang_edit').val()
				},
				success() {
					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					createSwal({
						title: 'Завдання видалено',
						type: 'success'
					});

					$('#changingLang').hide();

					getLangQuestions();

					$('#lang_question').val('');
					$('#lang_answers').val('');
				},
				error(err) {
					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					createSwal({
						title: 'Помилка при видаленні завдання',
						type: 'error'
					});
					console.log(err);
				}
			});
		}
	}

	function EditLangTask() {
		$(document).find('.bookshelf_wrapper').show();
		$(document).find('.hideBehind').show();
		$('#all').css("opacity", "0.5");

		let questionContent = $('#lang_question').val(),
			answerContent = $('#lang_answers').val(),
			imageWidth = $('#lang_question').closest('.category-block').find('input[type="radio"]:checked');

		if($(imageWidth).val() === 'custom') {
			imageWidth = $(imageWidth).parent().next();
		}

		if($('#lang_question').closest('.category-block').find('.enableAutoWidth').hasClass('btn-success')) {
			imageWidth = 'auto';
		}
		else {
			imageWidth = $(imageWidth).val();
		}

		[questionContent, questionImages] = getImageSrc(questionContent);
		[answerContent, answerImages] = getImageSrc(answerContent);

		$.ajax({
			url: '/update-ukrainian-task',
			method: 'post',
			data: {
				'questionContent': questionContent,
				'answerContent': answerContent,
				'question_id': $('#get_lang_edit').val(),
				'questionImages': questionImages,
				'answerImages': answerImages,
				'image_width': imageWidth
			},
			success(response) {
				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");

				getLangQuestions();

				$('#changingLang').hide();

				$('#lang_question').val('');
				$('#lang_answers').val('');

				$('#get_lang_edit').val('');
				createSwal({
					title: 'Завдання оновлено',
					type: 'success'
				});
			},
			error(err) {
				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");
				createSwal({
					title: 'Помилка при запису завдання',
					type: 'error'
				});

				console.log(err);
			}
		});
	}

	function EditTasks() {
		var textQuestion = $('#form_edit_task .question-name').val(),
			answers = $('#first_edit_item .item-name'),
			answersTexts = [],
			answersImages = [],
			answersId = [];

		for(var ans of answers){
			let text = $(ans).val();

			[text, answerImage]= getImageSrc(text);
			answersTexts.push(text);
			answersImages.push(answerImage);
			answersId.push($(ans).attr('data-id-answer'));
		}

		var err = 0,
			error = 1;

		if (textQuestion == '' || textQuestion == ' ' )  {
			err = 1;
		}

		for (var i = 0; i < answersTexts.length; i++) {
			if (answersTexts[i] == ''|| answersTexts[i] == ' ') {
				err = 1;
			}
		}

		$('input[name=cat-radio]').each(function () {
			if($(this).prop('checked')) {
				error = 0;
			}
		});

		if (error === 1) {
			createSwal({
				title: 'Оберіть правильний варіант',
				type: 'error'
			});
		}
		else if (err == 1) {
			createSwal({
				title: 'Має бути заповнений текст завданнь або додане зображення',
				type: 'error'
			});
		}
		else {
			$(document).find('.bookshelf_wrapper').show();
			$(document).find('.hideBehind').show();
			$('#all').css("opacity", "0.5");

			const form = this,
				token = $(this).find('>:first-child').val(),
				questionId = $("#tasks_selector_for_edit").val();

			function saveEditedQuestion() {
				let questionBlock = $('#form_edit_task .questions-block'),
					contentData = questionBlock.find('.text-question').val(),
					checkedRadio = questionBlock.find('.imageWidthContainer input[type="radio"]:checked'),
					imageWidthInput = questionBlock.find('.imageWidthContainer .customWidthInput'),
					imageWidth = checkedRadio.val() !== 'custom' ? checkedRadio.val() :  imageWidthInput.val();

				if(questionBlock.find('.enableAutoWidth').hasClass('btn-success')) {
					imageWidth = 'auto';
				}
					
				const questionData = {
					id: questionId,
					contentData,
					imageWidth
				};

				saveElement(questionData, 'Question');
			}

			saveEditedQuestion();
			const questionBlock = $('#form_edit_task .questions-block'),
				answersData = questionBlock.find('.item-name').toArray().map(el => {

				return {contentData: $(el).val()}
			});

			const isChecked = questionBlock.find('input[type=checkbox]').toArray().map(input => {
				return $(input).is(':checked') ? 1 : 0;
			});

			let countAnswers = 0;
			const nmbAnswers = answersId.length;

			function saveEditedAnswers() {
				const dataAnswers = {
					...answersData[countAnswers],
					isChecked: isChecked[countAnswers],
					questionId
				};

				saveElement(dataAnswers, 'Answer');
				countAnswers++;
			}

			function saveElement(dataElement, elementType) {
				let url = elementType === 'Question' ? '/update-question' : '/update-answer';

				const [contentData, images] = getImageSrc(dataElement.contentData);

				const data = {
					...dataElement,
					contentData,
					images
				};

				$.ajax({
					url: url,
					method: 'post',
					data: data,
					success() {
						$('#form_edit_task button:submit').prop('disabled', false);

						if (countAnswers < nmbAnswers) {
							saveEditedAnswers();
						}
						else {
							$(document).find('.bookshelf_wrapper').hide();
							$(document).find('.hideBehind').hide();
							$('#all').css("opacity", "1");

							$('#form_edit_task').hide();

							const questionText = $('#form_edit_task .questions-block .text-question').val();

							updateEditQuestionSelect('update', questionText);

							createSwal({
								title: 'Завдання оновлено',
								type: 'success'
							});

							$('.enableAutoWidth.btn-success').click();

							$('#form_edit_task input:file').map((index, input) => {
								$(input).val('');
							})
						}
					},
					error(err) {
						$(document).find('.bookshelf_wrapper').hide();
						$(document).find('.hideBehind').hide();
						$('#all').css("opacity", "1");

						$('#form_edit_task button:submit').prop('disabled', false);

						createSwal({
							title: 'Помилка при запису завдання',
							type: 'error'
						});
					}
				});
			}
		}
	}
}

function deleteQuestion() {
	const isConfirm = confirm('Ви дійсно хочете видати дане завдання?'),
		idQuestion = $('#tasks_selector_for_edit').val();

	if(isConfirm) {
		$(document).find('.bookshelf_wrapper').show();
		$(document).find('.hideBehind').show();
		$('#all').css("opacity","0.5");

		$.ajax({
			url: '/delete-question',
			method: 'post',
			data: {
				idQuestion: idQuestion,
			},
			success() {
				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity","1");

				$('#form_edit_task').hide();
				updateEditQuestionSelect('delete');

				createSwal({
					title: 'Завдання успішно видалено',
					type: 'success'
				});
			},
			error(err){
				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity","1");

				createSwal({
					title: 'Помилка при видаленні завдання',
					type: 'error'
				});
				console.log(err);
			}
		});
	}
}

function updateEditQuestionSelect(action, data=null) {
	const select = $('#tasks_selector_for_edit'),
		selectContainer = $('#questions_edit_block_elements'),
		idQuestion = $(select).val();

	data = removeHTML(data);

	if(action === 'update') {
		if(data == '') {
			$(select).find(`option[value="${idQuestion}"]`).text('Зображення');
			$(select).find(`option[value="${idQuestion}"]`).attr('name','image');
		}
		else {
			$(select).find(`option[value="${idQuestion}"]`).text(data);
			$(select).find(`option[value="${idQuestion}"]`).attr('name','normal');
		}
	}
	else if(action === 'delete') {
		$(select).find(`option[value="${idQuestion}"]`).remove();

		const nmbOptions = select.find('option').length;
		if(nmbOptions < 1) {
			selectContainer.hide();
		}
	}
	$(select).val('');

	$('#questions_edit_block_elements [data-toggle="tooltip"] button').prop("disabled", true);
}

function removeHTML(str){
	if(str) {
		while(str.indexOf('<') != -1 && str.indexOf('>') != -1 && str.indexOf('>') > str.indexOf('<')) {
			str = str.slice(0, str.indexOf('<')) + str.slice(str.indexOf('>') + 1, str.length);
		}
	}
	return str;
}

module.exports = {
	getQuestion: getQuestion,
	getQuestions: getQuestions,
	getQuestionLang: getQuestionLang,
	getLangQuestions: getLangQuestions,
	handleEditTask: handleEditTask,
	deleteQuestion: deleteQuestion,
	addNewAnswer: addNewAnswer,
	addAnswerField: addAnswerField,
	deleteAnswer: deleteAnswer,
	removeHTML: removeHTML
};