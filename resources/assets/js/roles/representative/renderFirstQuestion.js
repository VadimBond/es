const {redactorMultiblock} = require('../../redactorMultiblock');
const {addListenersToImages} = require('./images');
const {addNewAnswer} = require('./addNewAnswer');
const {deleteAnswer} = require('./manage-question');
const {createSwal} = require('./createSwal');

function renderFirstQuestion() {
	resetTasks();

	const questionContainer = $('#categories'),
		selectBlock = $('#block_selector'),
		addingButtons = questionContainer.find('.adding-btns'),
		idBlock = $(selectBlock).val();

	$.ajax({
		url: '/get-nmb-answers',
		method: 'post',
		data: {idBlock: idBlock},
		success(response) {
			const countQuestions = response['countQuestions'],
				weightQuestion = response['weightQuestion'],
				maxNmbQuestion = response['allowedNmbNewQuestion'],
				nmbAnswers = response['nmbAnswers'];

			selectBlock.attr('data-nmb_questions', countQuestions);
			selectBlock.attr('data-weight-question', weightQuestion);
			selectBlock.attr('data-max-question', maxNmbQuestion);

			renderFirstAnswers(nmbAnswers);
			questionContainer.show();
			addingButtons.show();

			addListenersToImages();

			if($('.question-name').closest('.category-block').is(':visible')) {
				const questionBlock = $('#panel2 .questions-block:first .html');
				questionBlock.click();
				$('#panel2 .questions-block:first .question-name').val('');
				questionBlock.click();
			}
		},
		error(err) {
			createSwal({
				title: 'Помилка при спробі створенні нового завдання',
				type: 'error'
			});
			console.log(err);
		}
	});
}

function renderFirstAnswers(nmbAnswers, answersContainer = null) {
	const nmbAnswersInput = $('#hidden_countAnswers');
	nmbAnswersInput.val(nmbAnswers);

	answersContainer = answersContainer ? answersContainer : $('#first_answer_item');

	answersContainer.html('');
	for(let countAnswer = 0; countAnswer < nmbAnswers; countAnswer++) {
		answersContainer.append(addNewAnswer(null, false, false, false, true, true));
		answersContainer.find('.deleteAnswer').off('click');
		answersContainer.find('.deleteAnswer').on('click', deleteAnswer);
	}

	const multiblocks = $(answersContainer).parent().find('.multiblock');

	multiblocks.off('click');
	multiblocks.on('click', redactorMultiblock);
}

function resetTasks() {
	$('#panel2 .category-block').show();
	$('.item-category').first().val('');
	$('.category-blocks:not(:first)').remove();

	// clean eng tasks
	$('#panel2 .list-block').hide();
	$('#panel2 .text-block').hide();
	$('#panel2 .manage-buttons-block').hide();
}

module.exports = {
	renderFirstQuestion: renderFirstQuestion,
	renderFirstAnswers: renderFirstAnswers,
};