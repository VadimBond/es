const createSwal = require('./createSwal').createSwal;
const addListenersToImages = require('./images').addListenersToImages;
const updateSelectOptions = require('./updateSelectOptions').updateSelectOptions;
const redactorMultiblock = require('../../redactorMultiblock').redactorMultiblock;

var multiblocks = $('#textEnglish');
multiblocks.off('click');
multiblocks.on('click', redactorMultiblock);
multiblocks = $('#textEnglishEdit');
multiblocks.off('click');
multiblocks.on('click', redactorMultiblock);

function fillEditForms(type){
	$('#create-block-form').html('');
	$('#edit-block-form').html('');

	let normalAdd = `
		<div class="row">
			<div class="form-group col-xs-6">
				<label>Назва нового блоку</label>
				<input type="text" class="form-control required block-name"  maxlength="64" required>
			</div>
	
			<div class="form-group col-xs-3">
				<label>Кількість балів за 1 завдання</label>
				<input type="number" class="form-control required weight-task" min="0.1" max="100" step="0.1" required>
			</div>
			<div class="form-group col-xs-3">
				<label>Кількість відповідей</label>
				<input type="number" class="form-control nmb-answers"  min="1" max="6" step="1" required >
			</div>
		</div>
		<button type="submit" class="btn btn-primary center-block">Cтворити блок</button>`;

	let normalEdit = `
		<p class="questionStatistic"></p>
		<p class="answerStatistic"></p>
		<div class="row">
			<div class="form-group col-xs-6">
				<label>Назва блоку</label>
				<input type="text" class="form-control required block-name" placeholder="Назва блоку" maxlength="64" required>
			</div>
			<div class="form-group col-xs-3">
				<label>Кількість балів за 1 завдання</label>
				<input type="number" class="form-control required weight-task" placeholder="Вага" min="0.1" max="100" step="0.1" required>
			</div>
			<div class="form-group col-xs-3">
				<label>Кількість відповідей</label>
				<input type="number" class="form-control nmb-answers" placeholder="Кіл-ть відповідей" min="1" max="6" step="1" required>
			</div>
		</div>	
		<button type="submit" class="btn btn-primary center-block">Зберегти зміни</button>`;

	let englishAdd = `
		<div class="row">
			<div class="form-group col-xs-6">
				<label>Назва нового блоку</label>
				<input type="text" class="form-control required block-name"  maxlength="64" required>
			</div>

			<div class="form-group col-xs-3">
				<label>Кількість балів за 1 завдання</label>
				<input type="number" class="form-control required weight-task" min="0.1" max="100" step="0.1" required>
			</div>
			<div class="form-group col-xs-3">
				<label>Кількість відповідей</label>
				<input type="number" class="form-control nmb-answers"  min="1" max="12" step="1" required >
			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-9" id="taskTitle">
				<label>Умова завдання</label>
				<input type="text" class="form-control block-title"  maxlength="150" required>
			</div>
			<div class="form-group col-xs-3 hidden spareAnsDiv">
				<label>Кількість лишніх відповідей</label>
				<input type="number"  class="form-control spareAnswers" min="0" max="5" step="1">
			</div>
		</div>
		<div class="row" id="checkboxes">
			<div class="form-group col-xs-3" id="engText">
				<label>В завданні присутній текст</label>
				<input type="checkbox" class="form-control checkText">
			</div>
			<div class="form-group col-xs-6	">
				<label for="checkAnswers">В умові завдання присутній список відповідей</label>
				<input type="checkbox" class="form-control checkAnswers">
			</div>
			<div class="form-group col-xs-3 TFDiv hidden">
				<label for="checkTF">Питання типу true/false</label>
				<input type="checkbox" class="form-control checkTF">
			</div>
		
		</div>
		<button type="submit" class="btn btn-primary center-block">Cтворити блок</button>`;

	let englishEdit = `
		<p class="questionStatistic"></p>
		<p class="answerStatistic"></p>
		<div class="row">
			<div class="form-group col-xs-6">
				<label>Назва блоку</label>
				<input type="text" class="form-control required block-name"  maxlength="64" required>
			</div>

			<div class="form-group col-xs-3">
				<label>Кількість балів за 1 завдання</label>
				<input type="number" class="form-control required weight-task" min="0.1" max="100" step="0.1" required>
			</div>
			<div class="form-group col-xs-3 hidden">
				<label>Кількість відповідей</label>
				<input type="number" class="form-control nmb-answers"  min="1" max="12" step="1" required >
			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-9" id="taskTitle">
				<label>Умова завдання</label>
				<input type="text" class="form-control block-title required"  maxlength="150" required>
			</div>
			<div class="form-group col-xs-3 hidden spareAnsDiv">
				<label>Кількість лишніх відповідей</label>
				<input type="number"  class="form-control spareAnswers" min="0" max="5" step="1">
			</div>

		</div>
		<div class="row" id="checkboxes">
			<div class="form-group col-xs-3 hidden" id="engText">
				<label>В завданні присутній текст</label>
				<input type="checkbox" class="form-control checkText">
			</div>
			<div class="form-group col-xs-6	hidden">
				<label for="checkAnswers">В умові завдання присутній список відповідей</label>
				<input type="checkbox" class="form-control checkAnswers">
			</div>
			<div class="form-group col-xs-3 TFDiv hidden">
				<label for="checkTF">Питання типу true/false</label>
				<input type="checkbox" class="form-control checkTF">
			</div>
		</div>
		<button type="submit" class="btn btn-primary center-block">Зберегти зміни</button>`;

	if(type == '0') {
		$('#create-block-form').append(normalAdd);
		$('#edit-block-form').append(normalEdit);
	}
	else if(type == '2') {
		$('#create-block-form').append(englishAdd);
		$('#edit-block-form').append(englishEdit);
	}
	$(".checkTF").change(function() {
		if(this.checked) {
			$(".spareAnsDiv").addClass("hidden");
			$(".spareAnswers").val(0);
		}
		else {
			$(".spareAnsDiv").removeClass("hidden");
		}
	});

	$(".checkText").change(function() {
		if(this.checked && $('.checkAnswers').is(":checked")) {
			$(".TFDiv").removeClass("hidden");
			$(".spareAnsDiv").removeClass("hidden");
		}
		else {
			$(".checkTF").prop('checked', false);
			$(".TFDiv").addClass("hidden");
			$(".spareAnsDiv").addClass("hidden");
			$(".spareAnswers").val(0);
		}
	});

	$(".checkAnswers").change(function() {
		if(this.checked && $('.checkText').is(":checked")) {
			$(".TFDiv").removeClass("hidden");
			$(".spareAnsDiv").removeClass("hidden");
		}
		else {
			$(".checkTF").prop('checked', false);
			$(".TFDiv").addClass("hidden");
			$(".spareAnsDiv").addClass("hidden");
			$(".spareAnswers").val(0);
		}
	});

}

function createEnglishBlock(e) {
	$('#create-block-form').find('button').prop('disabled', true);

	e.preventDefault();

	const sendingForm = $('#create-block-form');

	$(document).find('.bookshelf_wrapper').show();
	$(document).find('.hideBehind').show();
	$('#all').css("opacity", "0.5");

	let checkText,checkAnswers,checkTF,spareAnswers;
	if(sendingForm.find('.checkText').is(":checked")) {
		checkText = 1;
	}
	else {
		checkText = 0;
	}

	if(sendingForm.find('.checkAnswers').is(":checked")) {
		checkAnswers = 1;
	}
	else {
		checkAnswers = 0;
	}

	if(sendingForm.find('.checkTF').is(":checked")) {
		checkTF = 1;
	}
	else {
		checkTF = 0;
	}

	spareAnswers = 0;
	if(checkAnswers && checkText) {
		if(sendingForm.find('.spareAnswers').val() > 0) {
			spareAnswers = sendingForm.find('.spareAnswers').val();
		}
	}

	const data = {
		title: sendingForm.find('.block-title').val(),
		specialityId: $('#id_speciality').val(),
		blockName: sendingForm.find('.block-name').val(),
		weightTask: sendingForm.find('.weight-task').val(),
		nmbAnswers: sendingForm.find('.nmb-answers').val(),
		spareAnswers: spareAnswers,
		checkText: checkText,
		checkAnswers: checkAnswers,
		checkTF: checkTF,
		token: sendingForm.find("input[name='_token']").val()
	};

	return $.ajax({
		url: '/create-english-block',
		method: 'POST',
		data: data,
		success: function (response) {
			const dataBlock = {
				blockName: data['blockName'],
				blockId: response['idBlock'],
				idSpeciality: response['idSpeciality'],
				nmbAnswers: data['nmbAnswers']
			};

			updateSelectOptions('create_block', dataBlock);
			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");

			createSwal({
				title: 'Блок успішно створено',
				type: 'success'
			});

			sendingForm.find('button').prop('disabled', false);

			cleanInputForm($('#create-block-form'));
			$('.new-block-form').addClass('hidden');
			$(".TFDiv").addClass("hidden");
			$(".spareAnsDiv").addClass("hidden");
		},
		error: function (error) {
			console.log(error);

			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");

			sendingForm.find('button').prop('disabled', false);

			createSwal({
				title: 'Помилка при створені блоку',
				type: 'error'
			});
		},
		finally() {
			cleanInputForm(sendingForm);
		}
	});
}

function blockPdf(){
    var id = $('#choose_block_for_view').val();

    $.ajax({
        url: '/generate_block_pdf',
        method: 'POST',
        data: {
            'blockId':id,
            'type':$('#type').text(),
        },
        success(response) {
            if(response.url) {
                const blurScreen = $('#blur-screen'),
                    iframe = $('#pdf-frame');

                blurScreen.removeClass('hidden');
                iframe.attr('src' ,response.url);
                iframe.removeClass('hidden');
            }
            else {
                createSwal({
                    title: 'Питання до данного блоку відсутні',
                    type: 'error'
                });
            }
        },
        error(error) {
            createSwal({
                title: 'Помилка при генеруванні ПДФ',
                type: 'error'
            });

            console.log(error);
        }
    });
}

function setLangBlock(id){
	$.ajax({
		url: '/set_lang_block',
		method: 'POST',
		data: {'id':id},
		success(response) {
			$('#blockLangName').val(response);
		},
		error(err) {
			console.log(err);
		}
	});
}

function setBlockData() {
	const blockId = $('#choose-select').val();

	addListenersToImages();

	const data = {
		'blockId': blockId
	};

	$.ajax({
		url: '/get-block',
		method: 'POST',
		data: data,
		success(response) {
			$('#textEnglishEdit').val('');
			const editBlockContainer = $('.edit-block-container'),
				editBlockForm = $('#edit-block-form');
			$(".TFDiv").addClass("hidden");
			$(".spareAnsDiv").addClass("hidden");

			updateBlockData(editBlockContainer, editBlockForm, response.data, response.english, response.block);
		},
		error() {
			createSwal({
				title: 'Помилка при спробі отримання даних блоку',
				type: 'error'
			});
		}
	});
}

function createNewBlock(e) {
	$('#create-block-form').find('button').prop('disabled', true);

	e.preventDefault();

	const sendingForm = $('#create-block-form'),
		weightTask = sendingForm.find('.weight-task').val();

	$(document).find('.bookshelf_wrapper').show();
	$(document).find('.hideBehind').show();
	$('#all').css("opacity", "0.5");

	const data = {
		title: sendingForm.find('.block-title').val(),
		specialityId: $('#id_speciality').val(),
		blockName: sendingForm.find('.block-name').val(),
		weightTask: weightTask,
		nmbAnswers: sendingForm.find('.nmb-answers').val(),
		token: sendingForm.find("input[name='_token']").val()
	};

	$.ajax({
		url: '/create-block',
		method: 'POST',
		data: data,
		success: function (response) {
			const dataBlock = {
				blockName: data['blockName'],
				blockId: response['idBlock'],
				idSpeciality: response['idSpeciality'],
				nmbAnswers: data['nmbAnswers']
			};

			updateSelectOptions('create_block', dataBlock);

			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");

			createSwal({
				title: 'Блок успішно створено',
				type: 'success'
			});

			cleanInputForm($('#create-block-form'));
			$('.new-block-form').addClass('hidden');

			sendingForm.find('button').prop('disabled', false);
		},
		error: function (error) {
			console.log(error);

			sendingForm.find('button').prop('disabled', false);

			createSwal({
				title: 'Помилка при створені блоку',
				type: 'error'
			});
		},
		finally() {
			cleanInputForm(sendingForm);
		}
	});
}

function updateBlock(e) {
	e.preventDefault();

	const editForm = $(e.currentTarget),
		chooseSelect = $('#choose-select'),
		weightTask = editForm.find('.weight-task').val();

	$(document).find('.bookshelf_wrapper').show();
	$(document).find('.hideBehind').show();
	$('#all').css("opacity","0.5");

	let type = 0, checkText, checkAnswers, checkTF;
	if(editForm.find('.checkText').is(":checked")) {
		checkText = 1;
	}
	else {
		checkText = 0;
	}
	if(editForm.find('.checkAnswers').is(":checked")) {
		checkAnswers = 1;
	}
	else {
		checkAnswers = 0;
	}
	if(editForm.find('.checkTF').is(":checked")) {
		checkTF = 1;
	}
	else {
		checkTF = 0;
	}

	const conf = {
		blockId: chooseSelect.val(),
		spareAnswers: editForm.find('.spareAnswers').val(),
		checkText: checkText,
		checkAnswers: checkAnswers,
		checkTF: checkTF,
	};

	if(checkText != 0 || checkAnswers != 0 || checkTF != 0) {
		type = 1;
	}

	const data = {
		title: editForm.find('.block-title').val(),
		type: type,
		blockId: chooseSelect.val(),
		blockName: editForm.find('.block-name').val(),
		weightTask: weightTask,
		nmbAnswers: editForm.find('.nmb-answers').val(),
		token: editForm.find("input[name='_token']").val(),
	};

	$.ajax({
		url: '/update-block',
		method: 'POST',
		data: data,
		success() {
			const editBlockContainer = $('.edit-block-container');
			updateSelectOptions('change_block', [data['blockId'], data['blockName']]);
			$('#block_selector_for_edit').find(`option[value="${data['blockId']}"]`).attr('name', data['nmbAnswers']);

			editBlockContainer.addClass('hidden');

			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");

			$('#textEnglishEdit').val('');

			$('#blockControl [data-toggle]').hide();

			createSwal({
				title: 'Дані блоку успішно змінені',
				type: 'success'
			});
			//for english
			if(data.type == 1) {
				$.ajax({
					url: '/update-conf',
					method: 'POST',
					data: conf,
					error(err) {
						console.log(err);

						createSwal({
							title: 'Помилка при спробі зміни конфігурації блоку',
							type: 'error'
						});
					}
				});
			}
		},
		error(err) {
			console.log(err);

			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");

			$('#blockControl [data-toggle]').hide();

			createSwal({
				title: 'Помилка при спробі зміни даних блоку',
				type: 'error'
			});
		},
	});
}

function deleteBlock() {
	const isConfirm = confirm('Ви дійсно хочете видалити даний блок?'),
		idSpeciality = $('#id_speciality').val();

	if(isConfirm) {
		$(document).find('.bookshelf_wrapper').show();
		$(document).find('.hideBehind').show();
		$('#all').css("opacity", "0.5");

		const idBlock = $('#choose-select').val();

		const data = {
			'idBlock': idBlock,
			'idSpeciality': idSpeciality
		};

		$.ajax({
			url: '/delete-block',
			method: 'POST',
			data: data,
			success(response) {
				const chooseBlockContainer = $('#edit-block .choose-container'),
					createNewBlockContainer = $('#edit-block .new-block-form');

				if(parseInt(response['nmbBlocks']) === 0) {
					chooseBlockContainer.addClass('hidden');
					createNewBlockContainer.removeClass('hidden');
					createNewBlockContainer.show();
				}

				$('.edit-block-container').addClass('hidden');

				updateSelectOptions('delete_block', idBlock);

				if(idBlock != null) {
					createSwal({
						title: 'Блок успішно видалено',
						type: 'success'
					});
					$('#edit-block').find("span[data-toggle='tooltip']").hide();
				}

				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");
			},
			error(err) {
				createSwal({
					title: 'Помилка при спробі видалення блоку',
					type: 'error'
				});

				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");

				console.log(err);
			}
		});
	}
}

function updateBlockData(blockContainer, form, data, english, block) {
	blockContainer.toggleClass('hidden');
	form.find('.checkText').prop('checked', false);
	form.find('.checkAnswers').prop('checked', false);
	form.find('.checkTF').prop('checked', false);

	form.find('.block-name').val(data['block_name']);
	form.find('.weight-task').val(data['weight_question']);
	form.find('.weight-task').attr('data-prev-weight', data['weight_question']);
	form.find('.nmb-answers').val(data['countAnswers']);
	form.find('.block-title').val(data['task_title']);
	form.find('.spareAnswers').val(english['extra_answers']);

	if(english['text'] == "1") {
		form.find('.checkText').prop('checked', true);
	}

	if(english['answer_list'] == "1") {
		form.find('.checkAnswers').prop('checked', true);
	}

	if(english['true_false'] == "1") {
		form.find('.checkTF').prop('checked', true);
	}

	if(block != '1') {
		form.find('.checkText').parent().removeClass('hidden');
		form.find('.checkAnswers').parent().removeClass('hidden');
		form.find('.nmb-answers').parent().removeClass('hidden');

		if(form.find('.checkText').prop('checked') && form.find('.checkAnswers').prop('checked')) {
			form.find('.checkTF').parent().removeClass('hidden');

			if(!form.find('.checkTF').prop('checked')) {
				form.find('.spareAnswers').parent().removeClass('hidden');
			}
		}
	}
	else {
		form.find('.checkText').parent().addClass('hidden');
		form.find('.checkAnswers').parent().addClass('hidden');
		form.find('.checkTF').parent().addClass('hidden');
		form.find('.spareAnswers').parent().addClass('hidden');
		form.find('.nmb-answers').parent().addClass('hidden');
	}
}

function cleanInputForm(form) {
	const inputs = $(form).find("input:not([type='hidden'])"), checks = $(form).find("input:checkbox");

	inputs.map( (index, input) => {
		$(input).val('');
	});
	checks.map( (index, check) => {
		$(check).prop('checked', false);
	});
}

module.exports = {
	createEnglishBlock:createEnglishBlock,
	blockPdf: blockPdf,
	createNewBlock: createNewBlock,
	updateBlock: updateBlock,
	setLangBlock: setLangBlock,
	setBlockData: setBlockData,
	deleteBlock: deleteBlock,
	fillEditForms: fillEditForms,
};