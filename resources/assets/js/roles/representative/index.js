const addListenersToImages = require('./images').addListenersToImages;
const {blockPdf, createNewBlock, updateBlock, setBlockData, deleteBlock, createEnglishBlock} = require('./manage-blocks');
const {openDeclined, changeSelector} = require('./change-speciality');
const {manageContentTab, filterOptionSelect} = require('./manageContentTab');
const {getQuestionLang, getQuestions, getQuestion, deleteQuestion} = require('./manage-question');
const sendOmu = require('./verification').sendOmu;
const updateLangBlock = require('./change-speciality').updateLangBlock;
const {renderFirstQuestion} = require('./renderFirstQuestion');
const {renderEnglishTask, getEngQuestions, deleteEngQuestion, getEngQuestion} = require('./manageEnglishTask/manageTasks');
const resetEnglishTasks = require('./manageEnglishTask/resetTasks');
const { createSwal } = require('./createSwal');
const setWidthListeners = require('../../SetAutoWidth');

$(document).ready(function() {
	const cloneSpecialityBtn = $('.clone-speciality');
	cloneSpecialityBtn.on('click', cloneSpeciality);

	function cloneSpeciality (e) {
		const submitBtn = $(e.currentTarget),
			verificationSelect = $('#choose_specialty');

		submitBtn.prop('disabled', true);

		$(document).find('.bookshelf_wrapper').show();
		$(document).find('.hideBehind').show();
		$('#all').css("opacity","0.5");

		let data = {
			'spec_id': $('#id_speciality').val()
		};

		$.ajax({
			url: '/clone-speciality',
			method: 'post',
			data: data,
			success(result) {
				$('#id_speciality').html($('#id_speciality').html() + '<option selected class=\"blacked\" value=\"' + result[1] + '\">' + result[2] + '</option>');

				verificationSelect.append(`<option value="${result[1]}">${result[2]}</option>`);

				changeSelector();
				createSwal({
					title: 'Спецільність успішно клоновано',
					type: 'success'
				});

			},
			error(err) {
				createSwal({
					title: 'Спецільність не клоновано',
					type: 'error'
				});
				console.log(err);
			},
			complete () {
				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");

				submitBtn.prop('disabled', false);
			}
		});
	}

	$('#id_speciality').on('change', changeSelector);
	$('#get_lang_edit').on('change', getQuestionLang);

	$('#block_selector_for_edit').on('change', () => {
		$('#form_edit_task').hide();
		if(parseInt($('#type').text()) === 2) {
			getEngQuestions();
		}
		else {
			getQuestions();
		}
	});
	$('#block_selector_for_edit').on('change', function () {
		$('#answer_items_edit').html("");
	});

	$('#tasks_selector_for_edit').on('change', () => {
		if(parseInt($('#type').text()) === 2) {
			getEngQuestion();
		}
		else {
			getQuestion();
		}
	});
	$('#questions_edit_block_elements .edit-task-button').on('click', () => {
		if(parseInt($('#type').text()) === 2) {
			getEngQuestion();
		}
		else {
			editTaskForm();
		}
	});
	$('#questions_edit_block_elements .delete-task-button').on('click', () => {
		$('#form_edit_task').hide();
		if(parseInt($('#type').text()) === 2) {
			deleteEngQuestion();
		}
		else {
			deleteQuestion();
		}
	});

	$('#sendDiv').on('click', sendOmu);

	$('#statusButton button').on('click', (e) => {
		const reason = $(e.currentTarget).attr('data-reason');
		openDeclined(reason);
	});

	setWidthListeners();

	manageContentTab();

	$(document).find('.bookshelf_wrapper').hide();
	$(document).find('.hideBehind').hide();
	$('#all').css("opacity", "1");

	const blurScreen = $('#blur-screen'),
		pdfFrame = $('#pdf-frame'),
		listBlank = $('#listBlankPopup'),
		nmbToDownloadBlank = $('#nmbToDownloadBlank');

	blurScreen.on('click', () => {
		blurScreen.addClass('hidden');
		pdfFrame.addClass('hidden');
		listBlank.addClass('hidden');
		nmbToDownloadBlank.addClass('hidden');
	});

	$('#exit, #closeDeclined').on('click', function (){
		$("#declined").hide();
	});

	$('#showView').on('click', blockPdf);

	$('#block_selector').on('change', () => {
		const typeSpeciality = $('#type').text();

		if(typeSpeciality == '2') {
			renderEnglishTask();
		}
		else {
			resetEnglishTasks();
			$('#panel2 .task-container .questions-block').show();
			renderFirstQuestion();
		}
	});

	addListenersToImages();

	$('a[data-toggle="tab"]').on('shown.bs.tab', manageContentTab);

	$('.add_question_text').off('click');
	$('.add_question_text').on('click',function() {
		var text = $(this).parent().find('textarea');
		$(text).val($(text).val() + `(XX)______`);
		if(!$('#blockCreateForm').hasClass('hidden')) {
			$('#textEnglish').click();
		}
		else {
			$('#textEnglishEdit').click();
		}
	});

	const createBlockForm = $('#create-block-form');
	createBlockForm.off('submit');
	if($('#type').text() == '0') {
		createBlockForm.on('submit', createNewBlock);
	}
	else {
		createBlockForm.on('submit', createEnglishBlock);
	}

	const editBlockSelect = $('#choose-select');
	editBlockSelect.on('change', onChangeCreateBlock);

	const editBlockBtn = $('#edit-block .category-pencil');
	editBlockBtn.on('click', setBlockData);

	const editBLockForm = $('#edit-block-form');
	editBLockForm.on('submit', e => updateBlock(e));

	var deleteBlockBtn = $('#edit-block .category-trash');
	deleteBlockBtn.on('click', deleteBlock);

    filterOptionSelect('#choose-select');

    $('#updateLangBlock').off('click');
    $('#updateLangBlock').on('click', updateLangBlock);
});

function editTaskForm() {
	$('#form_for_change_task').show();
	$('#form_for_change_block').hide();
	getQuestion();
	addListenersToImages();
}

function onChangeCreateBlock() {
	const curBlock = $(this).val(),
		createNewBlockContainer = $('.new-block-form'),
		editBlockContainer = $('.edit-block-container');

	addListenersToImages();

	if(curBlock === 'new_block') {
		createNewBlockContainer.removeClass('hidden');
		createNewBlockContainer.show();
		$('#edit-block').find("span[data-toggle='tooltip']").hide();
	}
	else {
		$('#edit-block').find("span[data-toggle='tooltip']").show();

		createNewBlockContainer.addClass('hidden');
	}
	editBlockContainer.addClass('hidden');

	const editBtn = $('#edit-block .category-pencil'),
		deleteBtn = $('#edit-block .category-trash');

	editBtn.removeAttr('disabled');
	deleteBtn.removeAttr('disabled');
}