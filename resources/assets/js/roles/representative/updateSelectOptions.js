function updateSelectOptions(action, data) {
	const addTaskSelect = $('#block_selector'),
		editTaskSelect = $('#block_selector_for_edit'),
		editBlockSelect = $('#choose-select'),
		selectArr = [addTaskSelect, editTaskSelect, editBlockSelect];

	if(action === 'create_block') {
		const isArray = Array.isArray(data);
		
		if(isArray) {
			selectArr.map( curSelect => {
				$(curSelect).empty();
			});

			for(let block of data) {
				const newOption = `<option value="${block['blockId'] }" data-id-speciality="${block['idSpeciality']}" name="${block['countAnswers']}">${block['blockName']}</option>`;
				selectArr.map( curSelect => {
					curSelect.append(newOption);
				});
			}

			editBlockSelect.append('<option value="new_block">Створити новий блок</option>');
		}
		else {
			selectArr.map( curSelect => {
		    	(curSelect).children('[value="new_block"]').remove();
			});

			const newOption = `<option value="${data['blockId'] }" data-id-speciality="${data['idSpeciality']}" name="${data['nmbAnswers']}">${data['blockName']}</option>`;

			editBlockSelect.append('<option value="new_block">Створити новий блок</option>');
			selectArr.map( curSelect => {
				if(curSelect.attr('id') === 'choose-select') {
					$(curSelect).find('option:last-of-type').before(newOption);
				}
				else {
					$(curSelect).append(newOption);
				}
			});
		}
	}
	else if(action === 'change_block') {
		selectArr.map( curSelect => {
			$(curSelect).find(`option[value=${data[0]}]`).text(data[1]);
		});
	}
	else if(action === 'delete_block') {
		selectArr.map( curSelect => {
			$(curSelect).find(`option[value=${data}]`).remove();
		});
	}
	$(editBlockSelect).val('');

	$('.addSelectContainer').show();
}

module.exports = {
	updateSelectOptions: updateSelectOptions
};