const createSwal = require('./createSwal').createSwal;
const {manageContentTab, filterOptionSelect} = require('./manageContentTab');
const handleNewTask = require('./dashboard').handleNewTask;
const updateSelectOptions = require('./updateSelectOptions').updateSelectOptions;
const setLangBlock = require('./manage-blocks').setLangBlock;

function openDeclined(reason) {
	createSwal({
		title: `Причина відмови: ${reason}`,
		type: 'info'
	});
}

function changeSelector() {
	$(document).find('.bookshelf_wrapper').show();
	$(document).find('.hideBehind').show();
	$('#all').css("opacity", "0.5");
	$('.customWidthInput').attr('disabled', true);
	$('.customWidthInput').val('');

	var user_mail = $("#data_user_email").val();
	var status_selector = $("#id_speciality option:selected").text();
	var id_speciality = $('#id_speciality').val();
	var token =  $("#send-form").children().first().val();
	const data = {
		user_mail: user_mail,
		status_selector: status_selector,
		id_speciality: id_speciality,
		token: token,
	};

	$.ajax({
		method: 'post',
		url: '/get_status',
		success: function(response){
			response.speciality.forEach(function (element) {
				if(element.id == id_speciality){

					if( element.status == "in_development") {
						$("#specialty_status").text("в розробці");
					}
					else if( element.status == "in_process") {
						$("#specialty_status").text("на перевірці");
					}
					else if( element.status == "approved") {
						$("#specialty_status").text("підтверджено");
					}
					else {
						$("#specialty_status").text("на доопрацюванні");
					}

					if(element.status == "declined") {
						$("#statusButton").show();
						$("#statusButton").attr('onClick', `openDeclined('${element.declined_reason}')`);
					}
					else {
						$("#statusButton").hide();
					}

					$('#type').text(element.type);
					if(element.type != "1") {
						$('#edit-block .choose-container').removeClass('hidden');
						if(element.type == "2")
							$('#EditBlock').text("Конфігурація завдань");
						else
						$('#EditBlock').text("Управління блоками");
						$('#langBlank').hide();
						$('#blank').show();

						$('#editLangBlock').hide();
					}
					else {
						$('#block_selector').text('');
						$('.hideAlert').hide();

						$('#EditBlock').text("Зміст завдання");
						$('#langBlank').show();
						$('#blank').hide();

						$('#editLangBlock').show();

						$('#updateLangBlock').off('click');
						$('#updateLangBlock').on('click', updateLangBlock);

						setLangBlock($('#id_speciality').val());
					}
					handleNewTask();
				}
			});
		},
		error:function (error) {
			console.log(error);
		},
		complete() {
			$.ajax({
				method: 'post',
				url: '/send_selector',
				data: data,
				success: function(result) {
					addTaskSelect = $('#block_selector').empty();
					editTaskSelect = $('#block_selector_for_edit').empty();
					if(result.status) {
						changeSpeciality(result.blocks, result.blank_status);
					}
					else if(result.error === 'cheater') {
						changeSpeciality(result.blocks, result.blank_status);
						createSwal({
							title: "Неправомірна дія",
							type: 'error'
						});
					}
				},
				error:function (error) {
					console.log(error);
					
				},
				complete() {
					manageContentTab();
				}
			});
 		}
	});
}

function updateLangBlock(){
	var id_speciality = $('#id_speciality').val();
	var newName = $('#blockLangName').val();

	$.ajax({
		url: '/update_lang_block',
		method: 'POST',
		data: {'id':id_speciality,
			'name':newName},
		success(response) {
			$('#blockLangName').val(response);
			createSwal({
				title: 'Дані блоку оновлені',
				type: 'success'
			});
		},
		error: function (error) {
			createSwal({
				title: 'Помилка при спробі зміни даних блоку',
				type: 'error'
			});
		}
	});
}

function changeSpeciality(blocks, status) {
	filterOptionSelect('#choose-select');

	const sendingButton = $('#sendDiv');

	if(status === 'in_process' || 'approved') {
		sendingButton.attr('disabled');
	}
	else {
		sendingButton.removeAttr('disabled');
		sendingButton.removeAttr('title');
	}

	if (blocks.length > 0) {
		updateSelectOptions('create_block', blocks);
	}
}

module.exports = {
    changeSelector: changeSelector,
    openDeclined: openDeclined,
    updateLangBlock: updateLangBlock
};