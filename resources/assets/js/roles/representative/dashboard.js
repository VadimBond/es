const {createSwal} = require('./createSwal');
const redactorMultiblock = require('../../redactorMultiblock').redactorMultiblock;
const handleEditTask = require('./manage-question').handleEditTask;
const addNewAnswer = require('./addNewAnswer').addNewAnswer;
const deleteAnswer = require('./manage-question').deleteAnswer;
const removeHTML = require('./manage-question').removeHTML;
const getImageSrc = require('./getImageSrc');
const {handleChangeRadio, setWidthImage} = require('./images');
const {saveEnglishTask} = require('./manageEnglishTask/manageTasks');
const setWidthListeners = require('../../SetAutoWidth');

$('#addNewTask').on('click', handleNewTask);

$('#EditTaskTab').on('click', handleEditTask);

let radio = 1;

const newTaskLang = () => {
	radio++;
	return `<div class="category-block" style="display: block;">
						<div class="imageWidthContainer im_regculation">
							<div class="row col-xs-10">
								<span>Вкажіть ширину зображень:</span>
								<label><input type="radio" name="image-width-radio-` + radio + `" value="50">50px</label>
								<label><input type="radio" name="image-width-radio-` + radio + `" value="75">75px</label>
								<label><input type="radio" name="image-width-radio-` + radio + `" value="100" checked>100px</label>
								<label><input type="radio" name="image-width-radio-` + radio + `" value="125">125px</label>
								<label><input type="radio" name="image-width-radio-` + radio + `" value="150">150px</label>
								<label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-` + radio + `" aria-label="image-with"></label>
								<input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>
								<button class = "changeWidthBtn btn btn-info" disabled>Змінити</button>
								<button class="enableAutoWidth btn btn-danger">Auto/off</button>
							</div>
						</div>
                        <div class="row category" style="margin-bottom: 5px">
                            <div class="col-xs-12 form-inline" style="width: 100%">
                                    <div class="lang-item">
                                        <div class="lang-area">
                                            <div class="popupImageConvertor">
                                                <div class="popup-container">
                                                    <h3 class="resize-title">Вставка зображення</h3>
                                                    <div class="resizer" >
                                                        <span class="addition">ctrl+v</span>
                                                    </div>
                                                    <button class="resizer-result btn btn-info" class="btn btn-info">Вставити виділене зображення</button>
                                                    <button class="closePopup">&#10006;</button>
                                                </div>
                                            </div>
                                            <textarea class="multiblock questionsLang form-control text_area_lang" placeholder="Текст питання"></textarea>
                                         </div>
                                         <div class="lang-area">
                                            <div class="popupImageConvertor">
                                                <div class="popup-container">
                                                    <h3 class="resize-title">Вставка зображення</h3>
                                                    <div class="resizer" >
                                                        <span class="addition">ctrl+v</span>
                                                    </div>
                                                    <button class="resizer-result btn btn-info" class="btn btn-info">Вставити виділене зображення</button>
                                                    <button class="closePopup">&#10006;</button>
                                                </div>
                                            </div>
                                            <textarea class="multiblock answersLang form-control text_area_lang" placeholder="Текст відповіді"></textarea>
                                        </div>
                                    </div>
                                <div class="btn-space">
                                    <span class="category-add" data-toggle="tooltip" title="Додати завдання">
                                        <button type="button" class="btn btn-default">
                                            <span class="glyphicon glyphicon-plus" aria-hidden="true"> Додати завдання</span>
                                        </button>
                                    </span>
                                    <span class="category-delete" data-toggle="tooltip" title="Видалити питання" >
                                        <button type="button" class="btn btn-default">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"> Видалити питання</span>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>`;
};

const newTask = () => {
	let arrIndex = [...$('#panel2 .questions-block input[name^="image-width-radio"]')].map((el) => {
		let name = $(el).attr('name');
		return parseInt(name.replace('image-width-radio-', ''));
	});

	let taskIndex = arrIndex.length > 0 ? Math.max(...arrIndex) + 1 : 1;

	return (`
		<div class="task-container">
			<div class="text-block" style="display: none;">
	            <h2>Текст до питання:</h2>
	            <div class="imageWidthContainer im_regculation" id="0">
	                <div class="row col-xs-10">
	                    <span>Вкажіть ширину зображень:</span>
	                    <label><input type="radio" name="image-width-radio-text" value="50">50px</label>
	                    <label><input type="radio" name="image-width-radio-text" value="75">75px</label>
	                    <label><input type="radio" name="image-width-radio-text" value="100" checked>100px</label>
	                    <label><input type="radio" name="image-width-radio-text" value="125">125px</label>
	                    <label><input type="radio" name="image-width-radio-text" value="150">150px</label>
	                    <label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-text" aria-label="image-width"></label>
	                    <input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>
	                    <button class = "changeWidthBtn btn btn-info" disabled>Змінити</button>
	                    <button class="enableAutoWidth btn btn-danger">Auto/off</button>
	                </div>
	            </div>
	            <div class="category row" style="margin-bottom: 5px">
	                <div class="col-xs-12 form-inline" style="width: 100%">
	                    <div class="popupImageConvertor">
	                        <div class="popup-container">
	                            <h3 class="resize-title">Вставка зображення</h3>
	                            <div class="resizer" >
	                                <span class="addition">ctrl+v</span>
	                            </div>
	                            <button class="btn btn-info resizer-result">Отримати зображення</button>
	                            <button class="closePopup">&#10006;</button>
	                        </div>
	                    </div>
	                    <textarea class="multiblock text-question"></textarea>
	                </div>
	            </div>
	        </div>
	        <div class="list-answers-block" style="display: none;">
	            <table>
	                <thead>
	                <tr>
	                    <th>№</th>
	                    <th>Варіант</th>
	                </tr>
	                </thead>
	                <tbody></tbody>
	            </table>
	        </div>
	        <div class="list-block" style="display:none;">
	            <table>
	                <thead>
	                <tr>
	                    <th>№</th>
	                    <th>Текст питання</th>
	                    <th class="change-content-col"></th>
	                </tr>
	                </thead>
	                <tbody></tbody>
	            </table>
	        </div>
			<div class="questions-block">
				<div class="category-block" style="display: block;">
					<div class="imageWidthContainer im_regculation">
						<div class="row col-xs-10">
							<span>Вкажіть ширину зображень:</span>
							<label><input type="radio" name="image-width-radio-${taskIndex}" value="50">50px</label>
							<label><input type="radio" name="image-width-radio-${taskIndex}" value="75">75px</label>
							<label><input type="radio" name="image-width-radio-${taskIndex}" value="100" checked>100px</label>
							<label><input type="radio" name="image-width-radio-${taskIndex}" value="125">125px</label>
							<label><input type="radio" name="image-width-radio-${taskIndex}" value="150">150px</label>
							<label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-${taskIndex}" aria-label="image-width"></label>
							<input type="number" class="form-control customWidthInput" aria-label="custom-image-width" min="10" max="350" disabled>
							<button class = "changeWidthBtn btn btn-info" disabled>Змінити</button>
							<button class="enableAutoWidth btn btn-danger">Auto/off</button>
						</div>
					</div>
					${addNewAnswer(null, true, true, true, false, false)}
					<div class="items">
						${addNewAnswer(null, false, false, false, true, true)}	
					</div>
				</div>
			</div>
		</div>`
	);
};

var newItem = addNewAnswer(null, false, false, false, true, true);

var nmbTaskUsed = 0;

function handleNewTask() {
	var addTaskBtn = $('.category-add'),
		rmTaskBtn = $('.category-delete'),
		multiblocks = $('#panel2 .multiblock'),
		select = $('#block_selector'),
		addAnswer =  $('.addNewAnswer'),
		categoriesContainer;

	if($('#type').text() == "1") {
		categoriesContainer = $('#categ');
		categoriesContainer.find('.category-block').show();
	}
	else {
		categoriesContainer = $('#categories');
	}

	updateListeners();

	$(categoriesContainer).find('.category-block').not(':first').remove();

	function addText (event) {
		$(this).parent().siblings('.savedImg').append("<p class=\"item-question-text\">" + $(this).parent().siblings(event.data.value).val() + "</p>");
		$(this).parent().siblings(event.data.value).val("");
	}

	function updateListeners() {
		addTaskBtn = $('.category-add');
		rmTaskBtn = $('.category-delete');
		addAnswer =  $('.addNewAnswer');

		multiblocks = $('#panel2 .multiblock');
		multiblocks.off('click');
		multiblocks.on('click', redactorMultiblock);

		addTaskBtn.off('click');
		addTaskBtn.on('click', addTask);

		rmTaskBtn.off('click');
		rmTaskBtn.on('click', rmTask);

		addAnswer.off('click');
		addAnswer.on('click', addAnswerField);

		$('.addNewText').off('click');
		$('.addNewText').on('click',{ value: '.item-category' }, addText);

		$('.addNewTextAns').off('click');
		$('.addNewTextAns').on('click',{ value: '.item-name' }, addText);

		const addImageWidthRadio = $('#panel2 .imageWidthContainer input[type="radio"]'),
			addChangeWidthBtn = $('#panel2 .changeWidthBtn');

		addImageWidthRadio.off();
		addImageWidthRadio.on('change', (e) => handleChangeRadio(e));
		addChangeWidthBtn.off();
		addChangeWidthBtn.on('click', (e) => {
			e.preventDefault();
			const imageWidth = $(e.currentTarget).prev().val();
			setWidthImage(imageWidth, $(e.currentTarget).prev().prev().find('input'));
		});

		setWidthListeners();
	}

	function addAnswerField() {
		var answersContainer = $(this).closest('.category-block').find('.items');

		answersContainer.append(newItem);

		$(this).closest('.category-block').find('.deleteAnswer').show();

		$('.deleteAnswer').off('click');
		$('.deleteAnswer').on('click', deleteAnswer);
		$('.addNewTextAns').off('click');
		$('.addNewTextAns').on('click', { value: '.item-name' }, addText);

		updateListeners();
	}

	function addQuestion(container) {
		var firstName = container.first().find('input[type="checkbox"]').attr('name');
		var itemsContainer =$(container).find('.items');

		for(var i = 0; i < $('#hidden_countAnswers').val() - 1; i++) {
			itemsContainer.append(newItem);
		}
		container.last().find('input[type="checkbox"]').attr('name', firstName);
	}

	function addTask() {
		var nmbTask = $(categoriesContainer).find('.category-block').length;
		var task_container = "";
		nmbTaskUsed = nmbTask;

		if(nmbTask === 1) {
			categoriesContainer.first().find('.category-delete').show();
		}

		if($('#type').text() === "1") {
			task_container = $(newTaskLang()).insertBefore($(categoriesContainer).find('.center-block'));
		}
		else {
			task_container = $(newTask()).insertBefore($(categoriesContainer).find('.center-block'));
		}

		addQuestion(task_container);
		updateListeners();
	}

	function rmTask() {
		var nmbTask = $(categoriesContainer).find('.category-block').length,
			task = $(this).closest('.category-block');

		if($('#type').text() == '0') {
			task = task.closest('.task-container');
		}

		nmbTaskUsed--;
		if(nmbTask > 1) {
			task.remove();

			updateListeners();
		}
		if(nmbTask === 2) {
			categoriesContainer.first().find('.category-delete').hide()
		}

		categoriesContainer.find('.category-add').toArray().map(el => $(el).show());
	}

	$('#send-form').off('submit');
	$('#send-form').on('submit', (e) => {
		e.preventDefault();

		$('#send-form button:submit').prop('disabled', true);

		const typeSpeciality = parseInt($('#type').text()),
			englishSpeciality = 2;

		typeSpeciality === englishSpeciality ?
			saveEnglishTask(e) :
			saveTasks(e);
	});

	$('#addingLang').off('submit');
	$('#addingLang').on('submit', (e) => {
		$('#addingLang button').prop('disabled', true);

		saveLangTask(e);
	});

	function validateQuestionsAndAnswers (element) {
		var valid = false;
		var currentValue = $(element).val().trim();
		if (currentValue.length > 1000000) {

			if($(element).hasClass('item-name')) {
				$(element).parent().parent().after("<div class=\"validate-alert\">Поле не має перевищувати 1000000 символів</div>");
			}
			else {
				$(element).parent().after("<div class=\"validate-alert\">Поле не має перевищувати 1000000 символів</div>");
			}

			$(element).parent().removeClass("access-validate");
			$(element).parent().addClass("not-validate");
		}
		else if (currentValue.length < 1) {

			$(element).parent().after("<div class=\"validate-alert\">Поле не має бути пустим</div>");

			if($(element).hasClass('item-name')) {
				$(element).removeClass("access-validate");
				$(element).addClass("not-validate");
			}
			else {
				$(element).parent().removeClass("access-validate");
				$(element).parent().addClass("not-validate");
			}
		}
		else {

			$(element).parent().removeClass("not-validate");
			$(element).parent().addClass("access-validate");
			valid = true;
		}
		return valid;
	}

	function cleanValidation (element) {
		$(element).parent().removeClass("access-validate");
		$(element).parent().removeClass("not-validate");
	}

	function saveLangTask(e) {
		e.preventDefault();
		$('#success-blocks').hide();

		$(document).find('.bookshelf_wrapper').show();
		$(document).find('.hideBehind').show();
		$('#all').css("opacity","0.5");

		var task = $(categoriesContainer).find('.category-block');
		var taskNames = [];
		var questionImages = [];

		$(task).find(".validate-alert").remove();
		var validate = true;
		var i = 0;

		$(task).find(".answersLang").click();
		$(task).find(".questionsLang").click();
		$(task).find(".answersLang").each(function () {
			if (!validateQuestionsAndAnswers(this)) {
				validate = false;
			}
		});

		i = 0;
		$(task).find(".questionsLang").each(function () {
			if (!validateQuestionsAndAnswers(this)) {
				validate = false;
			}
		});

		if (validate == false)  {
			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity","1");

			$('#addingLang button').prop('disabled', false);

			return;
		}
		else {
			cleanValidation($(task).find(".questionsLang"));
			cleanValidation($(task).find(".answersLang"));
		}

		i = 0;
		var imageWidth = [];
		$(task).find(".questionsLang").each(function () {
			let imageWidthContainer = $($(task)[i]).find('.imageWidthContainer'),
				autoImageBtn = $($(task)[i]).find('.enableAutoWidth'),
				checkedRadio =  imageWidthContainer.find('input:radio:checked'),
				imageWidthInput = imageWidthContainer.find('.customWidthInput');

			if(autoImageBtn.hasClass('btn-success')) {
				imageWidth[i] = 'auto';
			}
			else {
				imageWidth[i] = checkedRadio.val() !== 'custom' ? checkedRadio.val() : imageWidthInput.val();
			}
			taskNames[i] = $(this).val();
			[taskNames[i], questionImages[i]] = getImageSrc(taskNames[i]);

			i++;
		});

		var tasksRight = [];
		var answerImages = [];
		i = 0;

		$(task).find(".answersLang").each(function () {
			tasksRight[i] = $(this).val();
			[tasksRight[i], answerImages[i]] = getImageSrc(tasksRight[i]);
			i++;
		});

		//hide content
		$(categoriesContainer).find('.category-block').not(':first').remove();
		$(task).find(".questionsLang").val("");
		$(task).find(".answersLang").val("");
		$(task).find(".questionsLang").prev().find(' iframe').contents().find('body').text("");
		$(task).find(".answersLang").prev().find(' iframe').contents().find('body').text("");

		categoriesContainer.first().find('.category-delete').hide();
		$('#langEmpty').hide();

		let data = {
			'questions' : taskNames,
			'answers': tasksRight,
			'spec_id': $('#id_speciality').val(),
			'questionImages': questionImages,
			'answerImages': answerImages,
			'image_width': imageWidth
		};

		$.ajax({
			url: '/save-ukrainian-tasks',
			method: 'post',
			data: data,
			success() {
				createSwal({
					title: 'Завдання успішно збережено',
					type: 'success'
				});
			},
			error(err) {
				console.log(err);
			},
			complete() {
				$('#addingLang button').prop('disabled', false);
				$('#categ input:radio[value="100"]').click();
				$('#categ .customWidthInput').val('');
				$('#categ .customWidthInput').prop('disabled', true);
				$('#categ .changeWidthBtn').prop('disabled', true);
				$('#categ .enableAutoWidth.btn-success').click();

				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity","1");
			}
		});
	}

	function saveTasks(e) {
		$(document).find('.bookshelf_wrapper').show();
		$(document).find('.hideBehind').show();
		$('#all').css("opacity", "0.5");

		$('#success-blocks').hide();

		$('#categories .category').find(".validate-alert").remove();
		$('#send-form .items .item').find(".validate-alert").remove();
		var validate = true;

		$('#categories .category').each(function(){
			$(e.currentTarget).find('.question-name').click();
			if (!validateQuestionsAndAnswers($(e.currentTarget).find('.question-name'))) {
				$(e.currentTarget).find('.question-name').each(function () {
					$(this).closest('.row').find('.validate-alert:not(:first)').remove();
				});
				validate = false;
			}
		});

		let answersItems = $('#send-form .items .item .item-name');
		for(let answer of answersItems) {
			if (!validateQuestionsAndAnswers(answer)) {
				$(answer).closest('.row').find('.validate-alert:not(:first)').remove();
				validate = false;
			}
		}

		if (validate == false) {
			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity","1");

			$('#send-form button:submit').prop('disabled', false);
			return;
		}
		else {
			cleanValidation($('#categories .category').find('.question-name'));
			cleanValidation($('#send-form .items .item').find('.item-name'));
		}

		var currentQuestionCount = 0;

		var TextQuestions = [];
		var TextAnswers = [];
		var correctQuestions = [];

		var answers = [];

		var tasks = $(categoriesContainer).find('.category-block');
		var error = 1;
		var index = 0;
		var answerCount = [];
		var err = 0;

		$('#categories .category-block').each(function () {
			error = 1;
			$(e.currentTarget).find('input[name=cat-radio]').each(function () {
				if($(this).prop('checked')) {
					error = 0;
				}
			});
			if(error === 1) {
				err = 1;
			}
		});

		if(err === 1) {
			createSwal({
				title: 'Оберіть правильний варіант для всіх завдань',
				type: 'error'
			});

			$('#send-form button:submit').prop('disabled', false);

			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity","1");
		}
		else {
			for (var task of tasks) {

				var taskName = $(task).find('.item-category').val(),
					questionsElements = $(task).find('.items .item-name').toArray(),
					currentQuestions = [];
				questionsElements.forEach(function (question) {
					currentQuestions.push($(question).val());
					answers.push($(question).val());
				});

				var correctQuestion = $(task).find('input:checked').parent().parent().parent().parent().index();

				TextQuestions.push(taskName);
				TextAnswers.push(currentQuestions);

				answerCount.push(currentQuestions.length);
				correctQuestions.push(correctQuestion);
			}

			saveImages();
		}

		function saveImages() {
			var token =  $("#send-form").children().first().val();
			var questions = $('#categories .category-block');
			var questionsNmb = $(questions).length;
			var id_block =$("#block_selector").val();
			var indexQuestions = 0;
			var questionText = '';
			var questionImage = '';

			$(document).find('.bookshelf_wrapper').show();
			$(document).find('.hideBehind').show();
			$('#all').css("opacity", "0.5");

			saveQuestions();

			function saveQuestions() {
				questionText = $(questions[indexQuestions]).find('.question-name').val();

				[questionText, questionImage] = getImageSrc(questionText);

				sendImage(false);
			}

			var indexAnswers = 0,
				answers = $('#send-form .items .item'),
				answersNmb = answers.length,
				answersImage = '',
				answerText = '',
				id_question = '',
				isChecked;

			function saveAnswers() {
				answerText = $(answers[indexAnswers]).find('.item-name').val();

				isChecked = $(answers[indexAnswers]).find('input[type="checkbox"]').prop("checked") ? 1 : 0;

				[answerText, answersImage] = getImageSrc(answerText);

				sendImage(true);
			}

			var createdQuestion = [];

			function sendImage(isAnswers) {
				var data;

				if(!isAnswers) {
					let checkedRadio =  $(tasks[indexQuestions]).find('input[type="radio"]:checked'),
						imageWidthInput = $(checkedRadio).parent().parent().find('.customWidthInput'),
						imageWidth;

					if($(tasks[indexQuestions]).find('.enableAutoWidth').hasClass('btn-success')) {
						imageWidth = 'auto';
					}
					else {
						imageWidth = $(checkedRadio).val() !== 'custom' ? $(checkedRadio).val() : $(imageWidthInput).val();
					}

					data = {
						'typeElement': 'Question',
						'id_block': id_block,
						'question_text': questionText,
						'image_width': imageWidth,
						'image': questionImage,
						'token': token
					};
				}
				else {
					data = {
						'typeElement': 'Answer',
						'id_question': id_question,
						'id_block': id_block,
						'isChecked': isChecked,
						'question_text': answerText,
						'image': answersImage,
						'token': token
					};
				}

				$('#categories .category-block').hide();
				$('#categories button:submit').hide();

				$.ajax({
					url: '/save-task-element',
					method: 'POST',
					data: data,
					success: function (result) {
						if (result.id_question) {
							createdQuestion.push({
								'id': result.id_question,
								'text': result.question_text
							});

							id_question = result.id_question;
						}
					},
					error: function (err) {
						console.log(err);
					},
					complete: function () {
						$('#send-form button:submit').prop('disabled', false);

						if (isAnswers) {
							indexAnswers++;
							index++;

							if (indexAnswers < answersNmb) {
								if (index % answerCount[currentQuestionCount] == 0 && questionsNmb > 1) {
									currentQuestionCount ++;
									index = 0;
									saveQuestions();

								}
								else {
									saveAnswers();
								}
							}
							else {
								updateNewOption(createdQuestion);

								const prevVal = $(select).attr('data-max-question'),
									questionNmb = $('#categories .category-block').length,
									nmbNewTask = prevVal - questionNmb;

								$(select).attr('data-max-question', nmbNewTask);

								resetTasks();
							}

						}
						else {
							indexQuestions++;
							saveAnswers();
						}

						if($('#panel2 .task-container').length > 1) {
							$('#panel2 .task-container:first').remove();
						}
					}
				});
			}
		}
	}

	function resetTasks() {
		$('#loadingProgress').hide();
		$(categoriesContainer).find('.category-block').remove();

		const maxTasks = $('#block_selector').attr('data-max-question');

		var task_container = $(newTask()).insertBefore($(categoriesContainer).find('.center-block'));
		addQuestion(task_container);

		if(maxTasks < 1) {
			$('#limit-questions').removeClass('hidden');
			$('#categories').hide();
		}
		else if(maxTasks == 1) {
			$('#categories .category-add').hide();
		}
		else {
			$('#categories .category-add').show();
		}

		$('#categories button:submit').show();

		$('#categories .category-delete').hide();

		$('.category-block').first().find('.items').attr('id', 'first_answer_item');

		updateListeners();

		$(document).find('.bookshelf_wrapper').hide();
		$(document).find('.hideBehind').hide();
		$('#all').css("opacity", "1");

		createSwal({
			title: 'Завдання успішно збережено',
			type: 'success'
		});
	}
}

function updateNewOption(createdQuestions) {
	const choosedBlockPanel2 = $('#panel2 #block_selector').val(),
		choosedBlockPanel3 = $('#panel3 #block_selector_for_edit').val();

	if(choosedBlockPanel2 === choosedBlockPanel3) {

		const selectContainer = $('#questions_edit_block_elements'),
			questionSelect = $('#tasks_selector_for_edit');

		for(let newQuestion of createdQuestions) {
			newQuestion['text'] = removeHTML(newQuestion['text']);
			if(newQuestion['text'] == "") {
				$(questionSelect).append(`<option value="${newQuestion['id']}">Зображення</option>`);
			}
			else {
				$(questionSelect).append(`<option value="${newQuestion['id']}">${newQuestion['text']}</option>`);
			}
		}

		selectContainer.show();
	}
}

module.exports = {
	handleNewTask: handleNewTask,
	addNewAnswer: addNewAnswer,
	deleteAnswer: deleteAnswer,
	removeHTML: removeHTML
};