const {typeElements} = require('./filterChangedEnElements');
const getImageSrc = require('../representative/getImageSrc');
const createSwal = require('../../modules/createSwal').createSwal;

const getImageWidth = (container) => {
	let	imageWidth;
	const autoWidthBtn = container.find('.enableAutoWidth ');

	if(autoWidthBtn.hasClass('btn-success')) {
		imageWidth = 'auto';
	}
	else {
		const checkedRadio = container.find("input:radio:checked").val(),
			customWidthInput = container.find('.customWidthInput');
		 imageWidth = checkedRadio !== 'custom' ? checkedRadio : customWidthInput.val();
	}

	return imageWidth;
};

const getListData = (listRows) => {
	return listRows.map( (row) => {
		let id = $(row).attr('data-id'),
			textCol = $(row).find('.textCol'),
			positionCol = $(row).find('.positionCol'),
			content = textCol.find('input').val(),
			rightAnswer = positionCol.find('select').val();

		return {
			id,
			content,
			rightAnswer
		}
	});
};

const getAnswersData = (answers) => {
	return answers.map( (answer) => {
		let id = $(answer).attr('data-answer-id'),
			[content, images] = getImageSrc($(answer).find('.item-name').val()),
			isChecked = $(answer).find('.correct-answer').is(':checked') ? 1 : 0;

		return {
			id,
			content,
			images,
			isChecked
		}
	});
};

const getQuestionsData = (questions) => {
	return questions.map( (question) => {
		let imageWidthContainer = $(question).find('.imageWidthContainer'),
			answers = $(question).find('.first-answers-block .item').toArray(),
			id = $(question).attr('data-question-id'),
			imageWidth = getImageWidth(imageWidthContainer),
			[content, images] = getImageSrc($(question).find('.category .question-name').val()),
			answersData = getAnswersData(answers);
		return {
			id,
			imageWidth,
			content,
			images,
			answersData
		};
	});
};


const {textList, textQuestions, listAnswers, question} = typeElements;

const saveEnglishTasks = (tasks, commissioners, idSpeciality) => {

	$.ajax({
		'url': '/update-commissioners',
		'method': 'post',
		'data': {commissioners, idSpeciality}
	});

	const promiseTasks = tasks.map( (task) => {
		let {collection, typeElements} = task;

		switch (typeElements) {
			case textList:
				return collection.map( (element) => {
					let textContainer = $(element).find('.text-container'),
						textId = textContainer.attr('data-id'),
						imageWidthContainer = textContainer.find('.imageWidthContainer'),
						imageWidth = getImageWidth(imageWidthContainer),
						[content, images] = getImageSrc(textContainer.find('.text-question').val()),
						listRows = $(element).find('.list-container tbody tr').toArray(),
						listData = getListData(listRows);

					let dataToSend = {
						textContent: {
							content,
							images
						},
						textId,
                        idSpeciality,
						imageWidth,
						listData,
						typeElements
					};

					return $.ajax({
						'url': '/update-english-blank',
						'method': 'post',
						'data': dataToSend
					});
				});
			case textQuestions:
				return collection.map( (element) => {
					let textContainer = $(element).find('.text-container'),
						textId = textContainer.attr('data-id'),
						imageWidthContainer = textContainer.find('.imageWidthContainer'),
						imageWidth = getImageWidth(imageWidthContainer),
						[content,images] = getImageSrc(textContainer.find('.text-question').val()),
						questions = $(element).find('.questions-container .category-block').toArray(),
						questionData = getQuestionsData(questions);

					let dataToSend = {
						textContent: {
							content,
							images
						},
						textId,
						imageWidth,
                        idSpeciality,
						questionData,
						typeElements
					};

					return $.ajax({
						'url': '/update-english-blank',
						'method': 'post',
						'data': dataToSend
					})
				});
			case listAnswers:
				return collection.map( (element) => {
					let answerListContainer = $(element).find('.list-answers-container'),
						listContainer = $(element).find('.list-container'),
						answerListRows = answerListContainer.find('tbody tr').toArray(),
						listRows = listContainer.find('tbody tr').toArray(),
						listData = getListData(listRows),
						listAnswersData = answerListRows.map( (answerListRow) => {
							let textCol = $(answerListRow).find('.textCol'),
								id = $(answerListRow).attr('data-id'),
								content = textCol.find('input').val();

							return {
								id,
								content
							}
						});

					let dataToSend = {
                        idSpeciality,
						listData,
						listAnswersData,
						typeElements
					};


					return $.ajax({
						'url': '/update-english-blank',
						'method': 'post',
						'data': dataToSend
					})
				});
			case question:
				return collection.map( (element) => {
					let questions = $(element).find('.category-block').toArray(),
						questionPromises = getQuestionsData(questions).map( (question) => {
							return $.ajax({
								'url': '/update-english-blank',
								'method': 'post',
								'data': {question, typeElements, idSpeciality}
							})
						});

					return Promise.all(questionPromises);
				});
			default:
				return [];
		}
	});

	Promise.all(promiseTasks)
		.then( () => {
			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#edit-container').css("opacity","1");
			createSwal({
				title: 'Зміни збережено',
				type: 'success',
			}, () => {
				window.location.href = "/omu";
			});
		})
		.catch( (err) => {
			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#edit-container').css("opacity","1");
			createSwal({
				title: 'Виникла помилка при збереженні питань',
				type: 'error'
			});
			console.log(err);
		});
};

module.exports = saveEnglishTasks;