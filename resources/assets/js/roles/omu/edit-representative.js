const createSwal = require('../../modules/createSwal').createSwal;

window.onload = function() {
	$(".toggle-password").click(function() {

		$(this).toggleClass("fa-eye fa-eye-slash");
		var input = $($(this).attr("toggle"));
		if (input.attr("type") == "password") {
			input.attr("type", "text");
		} else {
			input.attr("type", "password");
		}
	});

	const saveBtn = $('#save');

	const id = saveBtn.attr('data-id');

	saveBtn.on('click', (e) => save_representative(e, id));

	$('#roleVal').on('change', roleChange);
};

function roleChange(){
	$("#hidden").hide();
	$("#dep").val('');
	if($("#roleVal").val() == 3||$("#roleVal").val() == 5){
		$("#hidden").show();
	}
}

function save_representative(e ,id) {

	var firstName = $("#firstName").val();
	var secondName = $("#secondName").val();
	var email = $("#email").val();
	var originEmail = $("#originEmail").text();
	var role = $("#roleVal").val();
	var pass = $("#pass").val();
	var confirm = $("#confirm").val();
	var dep = "";
	var haveSpec = 0;

	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var data = {
		'firstName' : firstName,
		'secondName' : secondName,
		'email' : email,
		'originEmail' : originEmail,
		'role' : role,
		'dep' : dep,
		'id' : id,
		'pass' : pass
	};

	if(firstName == "" || secondName == "" || email == ""|| role == "" ) {
		createSwal({
			title: "Заповніть поля",
			type: 'error'
		});
	}
	else if (pass != confirm) {
		createSwal({
			title: "Поля пароль та підтвердження пароля не співпадаюсть",
			type: 'error'
		});
	}
	else if (pass === "" || pass.length < 6) {
		createSwal({
			title: "Довжна пароля має бути більше 6 символів",
			type: 'error'
		});
	}
	else if (!re.test(email)) {
		createSwal({
			title: "Невірний емайл",
			type: 'error'
		});
	}
	else {
		if (role == 3) {
			dep = $("#dep").val();
			data['dep'] = dep;
			$.ajax({
				method: 'POST',
				url: '/get_specialties',
				success: function (response) {
					response.forEach(function (speciality) {
						if (speciality.id_cafedres == dep) {
							haveSpec = 1;
						}
					});
					if(!dep) {
						createSwal({
							title: "Оберіть кафедру",
							type: 'error'
						});
					}
					else if (haveSpec == 1) {

						if (id == 0) {
							ajaxUser('/add_representative', data);
						}
						else {
							ajaxUser('/update_representative', data);
						}
					}
					else {
						createSwal({
							title: "Не можна прив\'язати користувача до кафедри, у якої немає спеціальностей",
							type: 'error'
						});
					}
				},
				error: function (error) {
					console.log(error);
				}
			});
		}
		else {

			if (id == 0) {
				ajaxUser('/add_representative', data);
			}
			else {
				ajaxUser('/update_representative', data);
			}
		}
	}
}

function ajaxUser(url, data) {
	$('#save').prop('disabled', true);

	$.ajax({
		method: 'POST',
		url: url,
		data: {
			'id': data['id'],
			'firstName': data['firstName'],
			'secondName': data['secondName'],
			'email': data['email'],
			'lastEmail': url == '/update_representative' ? (data['email'] === data['originEmail'] ? null : "not null") : "1",
			'role': data['role'],
			'dep': data['dep'],
			'pass': data['pass'],
		},
		success: function () {
			swal({
				title: url == '/update_representative' ? "Користувач був успішно оновлений" : 'Користувач був успішно створений',
				type: "success"
			}, () => {
				$('#save').prop('disabled', false);

				window.location.href = "/omu";
			});
		},
		error: function (error) {
			$('#save').prop('disabled', false);

			console.log(error);
			swal({
				title: url == '/update_representative' ? 'Помилка при збереженні користувача' : 'Емайл вже існує',
				type: 'error'
			});

		}
	});
}