window.onload = function() {
	const saveBtn = $('#saving .depart .save-btn');
	const id = saveBtn.attr('data-id');
	saveBtn.off('click');
	saveBtn.on('click', (e) => save_department(e, id));

	$(document).ready(function(){
		$("#name").blur(function(){
			validate_departments_forms(this, 190, true);
		});

		$("#address").blur(function(){
			validate_departments_forms(this, 500, true);
		});

		$("#phone").blur(function(){
			validate_departments_forms(this, 20, true);
		});

		$("#site").blur(function(){
			validate_departments_forms(this, 300, false);
		});
	});

	function validate_departments_forms(id, valLength, req) {
		$(id).siblings().remove();
		let value = $(id).val();
		if (value.length > valLength) {
			$(id).after("<div class=\"validate-alert\">Поле не має перевищувати " + valLength + " символів</div>");
			$(id).css("border","1px solid red");
		}
		else if (value.trim() == 0 && req){
			$(id).after("<div class=\"validate-alert\">Поле не має бути пустим</div>");
			$(id).css("border","1px solid red");
		}
		else {
			$(id).css("border","1px solid green");
			return true;
		}

		return false;
	}

	function save_department(e, id) {
		const submitBtn = $(e.currentTarget);

		var name = validate_departments_forms("#name", 191, true);
		var address = validate_departments_forms("#address", 500, true);
		var phone = validate_departments_forms("#phone", 20, true);
		var site =  validate_departments_forms("#site", 300, false);

		submitBtn.prop('disabled', true);

		if(name && address && phone && site) {
			submitBtn.off('click');

			if(id == 'undefined' || id == null || id == "") {
				ajaxDepartment('/add_department');
			}
			else {
				ajaxDepartment('/update_department');
			}
		}
		else {
			submitBtn.prop('disabled', false);
		}
	}

	function ajaxDepartment(url) {
		$.ajax({
			method: 'POST',
			url: url,
			data: {
				'id': id,
				'department_name': $("#name").val(),
				'address': $("#address").val(),
				'phone': $("#phone").val(),
				'site': $("#site").val(),
			},
			success: function (response) {
				$('#saving .save-btn').prop('disabled', false);
				if (response == 'ok') {
					swal({
						title: "Кафедра була успішно " + (url == '/update_department' ? "оновлена" : "створена"),
						type: "success"
					}, function () {
						window.location.href = "/omu";
					});
				}
				else {
					swal({
						title: "Кафедра не була " + (url == '/update_department' ? "оновлена" : "створена"),
						type: "error"
					}, function () {
						window.location.href = "/omu"
					});
				}
			},
			error: function (error) {
				$('#saving .save-btn').prop('disabled', false);

				console.log(error);
			}
		});
	}
};