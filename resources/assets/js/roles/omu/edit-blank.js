const redactorMultiblock = require('../../redactorMultiblock').redactorMultiblock;
const getImageSrc = require('../representative/getImageSrc');
const createSwal = require('../../modules/createSwal').createSwal;
const {handleChangeRadio, setWidthImage} = require('../representative/images');
const setWidthListeners = require('../../SetAutoWidth');

window.onload = function() {

	$('#all').css('opacity', 1);
	const multiblocks = $('.multiblock');

	multiblocks.on('click', redactorMultiblock);

	const arrMultiblocks = [...multiblocks];
	$(document).find('.bookshelf_wrapper').show();
	arrMultiblocks.forEach( (multiblock) => {
		multiblock.click();
	});
	$(document).find('.bookshelf_wrapper').hide();

	const saveBtn = $('#statuses .btn-success');
	update();
	saveBtn.on('click', updateBlank);

	const addImageWidthRadio = $('.editBlockOmu .imageWidthContainer input[type="radio"]'),
		addChangeWidthBtn = $('.editBlockOmu .changeWidthBtn');

	addImageWidthRadio.off();
	addImageWidthRadio.on('change', (e) => handleChangeRadio(e));
	addChangeWidthBtn.off();
	addChangeWidthBtn.on('click', (e) => {
		e.preventDefault();
		const imageWidth = $(e.currentTarget).prev().val();
		setWidthImage(imageWidth, $(e.currentTarget).prev().prev().find('input'));
	});

	setWidthListeners();
};

function update() {
	var newComm = `<div class="category-block" style="display: block;">
    <div class="row" style="margin-bottom: 5px">
		<div class="col-xs-12 form-inline">
			<div class = "hidden idsComm">0</div>
			<input class="item-category form-control commPosition" style="width: 45%" type="text" required placeholder="Посада члена комісії" >
			<input class="item-category form-control commName" style="width: 45%" type="text" required placeholder="ПІБ члена комісії" >
		<span  class = "category-add-comm" data-toggle="tooltip" title="Додати члена комісії">
		<button type="button"  class="btn btn-default">
		<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
		</button>
		</span>
		<span class="category-delete-comm" data-toggle="tooltip" title="Видалити члена комісії" style="display: none;">
		<button type="button" class="btn btn-default">
		<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
			</button>
			</span>
		</div> 
    </div>`;
	var rmCommBtn = $('.category-delete-comm'),
		addCommBtn = $('.category-add-comm'),
		commContainer = $('#comm');
	updateListeners();

	var nmbComm = $(commContainer).find('.category-block').length;
	if(nmbComm > 1) {
		commContainer.find('.category-delete-comm').show();
	}

	function updateListeners() {
		rmCommBtn = $('.category-delete-comm');
		addCommBtn = $('.category-add-comm');

		addCommBtn.off('click');
		addCommBtn.on('click', addComm);

		rmCommBtn.off('click');
		rmCommBtn.on('click', rmComm);
	}

	function addComm() {

		var nmbComm = $(commContainer).find('.category-block').length;

		if(nmbComm < 5) {
			$('#comm').append(newComm);
			updateListeners();
			if(nmbComm > 0) {
				commContainer.find('.category-delete-comm').show();
			}
		}
		else {
			createSwal({
				title: 'Максимальна к-ть членів комісії',
				type: 'error'
			});
		}

		if(nmbComm == 1) {
			commContainer.first().find('.category-delete-comm').show();
		}
	}

	function rmComm(){
		var nmbComm = $(commContainer).find('.category-block').length,
			comm = $(this).parent().parent().parent();

		if(nmbComm > 1) {
			comm.remove();
			updateListeners();
		}
		if(nmbComm == 2) {
			commContainer.first().find('.category-delete-comm').hide()
		}

		// show all adding buttons
		commContainer.find('.category-add-comm').toArray().map(el => $(el).show());
	}
}

function validateQuestionsAndAnswers (element) {
	var valid = false,
		currentValue = $(element).val().trim();

	if (currentValue.length > 1000000) {
		if($(element).hasClass('item-name')) {
			$(element).parent().parent().after("<div class=\"validate-alert\">Поле не має перевищувати 1000000 символів</div>");
		} else {
			$(element).parent().after("<div class=\"validate-alert\">Поле не має перевищувати 1000000 символів</div>");
		}
		$(element).parent().removeClass("access-validate");
		$(element).parent().addClass("not-validate");
	}
	else if (currentValue.length < 1) {
		if($(element).hasClass('item-name')) {
			$(element).parent().parent().after("<div class=\"validate-alert\">Поле не має бути пустим</div>");
		} else {
			$(element).parent().after("<div class=\"validate-alert\">Поле не має бути пустим</div>");
		}
		$(element).parent().removeClass("access-validate");
		$(element).parent().addClass("not-validate");
	}
	else {
		$(element).parent().removeClass("not-validate");
		$(element).parent().addClass("access-validate");
		valid = true;
	}
	return valid;
}

function cleanValidation (element) {
	$(element).parent().removeClass("access-validate");
	$(element).parent().removeClass("not-validate");
}

function updateBlank(e) {
	const submitBtn = $(e.currentTarget);
    $(document).find('.bookshelf_wrapper').show();
    $(document).find('.hideBehind').show();
    $('#all').css("opacity","0.5");
	const typeSpeciality = $('#type_speciality').val();

	var valid = true;

	cleanValidation($('.multiblock'));

	$(".validate-alert").remove();

	$('.multiblock').each(function(){
		if (!validateQuestionsAndAnswers(this)) {
			valid = false;
		}
	});

	if (!valid) {
		$(document).find('.bookshelf_wrapper').hide();
    	$(document).find('.hideBehind').hide();
    	$('#all').css("opacity","1");
   		createSwal({
			title: 'Питання не пройшли валідацію',
			type: 'error'
		});
    	return;
	}
	submitBtn.off('click');

	const blocksContent = $('.task-block'),
		questionsContent = $('.question_name'),
		answersContent = parseInt(typeSpeciality) === 0 ? $('.answer') : $('.answerContent');

	const tableNames = {
		blocks: 'blocks',
		questions: 'questions',
		answers: 'answers'
	};
	const imageNames = {
		questions: 'Question',
		answers: 'Answer'
	};

	const filteredBlocks = filterChangedElements([...blocksContent], null, 'blocks');
	const filteredQuestions = filterChangedElements([...questionsContent], null, 'questions');
	const filteredAnswers = filterChangedElements([...answersContent], typeSpeciality);

	const elementsToSend = [
		{ elements: filteredBlocks, tableName: tableNames.blocks},
		{ elements: filteredQuestions, tableName: tableNames.questions, typeImage: imageNames.questions},
		{ elements: filteredAnswers, tableName: tableNames.answers, typeImage: imageNames.answers}
	];

	const commissionersContainers = [...$('.category-block')];
	const commissioners = commissionersContainers.map( (commissionerContainer) => {
		const id = $(commissionerContainer).attr('data-id');
		const position = $(commissionerContainer).find('.commPosition').val();
		const initials = $(commissionerContainer).find('.commName').val();

		return {id, position, initials};
	});

	const idSpeciality = $('#id_speciality').val();

	Promise.all([
			...elementsToSend.map( (collectionElements) => {
			return collectionElements.elements.length > 0 ?
				Promise.all(saveElements(collectionElements)) :
				[]
			}),
			saveCommissioners(commissioners, idSpeciality)
		])
		.then( () => {
            $(document).find('.bookshelf_wrapper').hide();
            $(document).find('.hideBehind').hide();
            $('#all').css("opacity","1");
			createSwal({
				title: 'Зміни збережено',
				type: 'success',
			}, () => {
				window.location.href = "/omu";
			});
		})
		.catch( (err) => {
            $(document).find('.bookshelf_wrapper').hide();
            $(document).find('.hideBehind').hide();
            $('#all').css("opacity","1");
			createSwal({
				title: 'Виникла помилка при збереженні питань',
				type: 'error'
			});
			console.log(err);
		});
}

function filterChangedElements(collectionElements, typeSpeciality=null, collectionType) {
	let filteredCollection;

	if(typeSpeciality) {
		filteredCollection =  collectionElements.filter( (element) => {
			let multiblock = parseInt(typeSpeciality) ===  0 ? $(element).find('.multiblock') :$(element),
				checkbox = $(element).find('.correctAnswer'),
				widthImageContainer = $(element).parent().parent().parent().parent().find('.imageWidthContainer'),
				prevImageWidth = $(widthImageContainer).attr('data-prev-width'),
				checkedWidthRadio = $(widthImageContainer).find('input[type="radio"]:checked').val(),
				customWidthInput = $(widthImageContainer).find('.customWidthInput').val(),
				currImageWidth = checkedWidthRadio === 'custom' ? customWidthInput : checkedWidthRadio,
				isChangeMultiblock = $(multiblock).attr('data-prev-content') !== $(multiblock).val(),
				isChangeCheckbox = !!parseInt($(checkbox).attr('data-prev-content')) !== $(checkbox).is(':checked'),
				isChangeImageWidth = prevImageWidth !== currImageWidth;

			return isChangeCheckbox || isChangeMultiblock || isChangeImageWidth;
		});
	}
	else {
		filteredCollection =  collectionElements.filter( (container) => {
			let element = null,
				result = false;

			if(collectionType === 'blocks') {
				element = $(container).find('.blockName');

				result = $(element).attr('data-prev-content') !== $(element).val();
			}
			else if (collectionType === 'questions') {
				element = $(container).find('.questionContent');

				let prevImageWidth = $(container).find('.imageWidthContainer').attr('data-prev-width'),
					autoImageBtn = $(container).find('.imageWidthContainer .enableAutoWidth'),
					currImageWidth;

				if(autoImageBtn.hasClass('btn-success')) {
					currImageWidth = 'auto';
				}
				else {
					let	checkedWidthRadio = $(container).find('.imageWidthContainer input[type="radio"]:checked').val(),
						customWidthInput = $(container).find('.imageWidthContainer .customWidthInput').val(),
						currImageWidth = checkedWidthRadio === 'custom' ? customWidthInput : checkedWidthRadio;
				}

				let isChangeImageWidth = prevImageWidth !== currImageWidth,
					isChangeContent = $(element).attr('data-prev-content') !== $(element).val();

				result = isChangeImageWidth || isChangeContent;
			}
			else {
				throw Error('You must specify correct parameter collectionType');
			}

			return result;
		});
	}

	return filteredCollection;
}

function saveElements(collection) {
	const typeSpeciality = $('#type_speciality').val(),
		tableName = collection.tableName,
		typeImage = collection.typeImage;

	return collection.elements.map((element) => {
		let dataToSave,
			blockId = $(document).find('.blockName').attr('data-id');
		if(tableName === 'answers' && typeSpeciality == 0) {
			let id = $(element).find('.multiblock').attr('data-id'),
				contentData = $(element).find('.multiblock').val(),
				isChecked = $(element).find('.correctAnswer').is(':checked'),
				[elementContent, images] = getImageSrc(contentData);

			dataToSave = {tableName, id, elementContent, images, typeImage, isChecked};
		}
		else {
			let id, contentData = null;

			if(tableName === 'blocks' && typeSpeciality === 1) {
				id = $(element).find('.blockName').attr('data-id');
				contentData = $(element).find('.blockName').val();

				dataToSave = {tableName, id, contentData};
			}
			else if(tableName === 'answers' && parseInt(typeSpeciality) === 1) {
				id = $(element).attr('data-id');
				contentData = $(element).val();

				let [elementContent, images] = getImageSrc(contentData);

				dataToSave = {tableName, id, elementContent, images, typeImage};
			}
			else {
				let imageWidth;
				if(tableName === 'blocks') {
					id = $(element).find('.blockName').attr('data-id');
					contentData = $(element).find('.blockName').val();
				}
				else {
					let autoWidthBtn = $(element).find('.imageWidthContainer .enableAutoWidth');
					if(autoWidthBtn.hasClass('btn-success')) {
						imageWidth = 'auto';
					}
					else {
						let checkedWidthRadio = $(element).find('.imageWidthContainer input[type="radio"]:checked').val(),
							customWidthInput = $(element).find('.imageWidthContainer .customWidthInput').val();
						imageWidth = checkedWidthRadio === 'custom' ? customWidthInput : checkedWidthRadio;
					}

					id = $(element).find('.questionContent').attr('data-id');
					contentData = $(element).find('.questionContent').val();
				}

				let [elementContent, images] = getImageSrc(contentData);

				dataToSave = {tableName, id, elementContent, images, typeImage, imageWidth};
			}
		}
        dataToSave.blockId = blockId;
		return $.ajax({
			url: '/update-blank',
			method: 'post',
			data: dataToSave,
		})
	});
}

function saveCommissioners(commissioners, idSpeciality) {
	return $.ajax({
		url: '/update-commissioners',
		method: 'post',
		data: {commissioners, idSpeciality}
	});
}

module.exports = {
	validateQuestionsAndAnswers,
	cleanValidation
};