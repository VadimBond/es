const tables = require('./tables.js');

window.onload = function() {
	const saveBtn = $('.spec .save-btn');

	const id = saveBtn.attr('data-id');

	$("#name").blur(function(){
		tables.validate_specialties_forms(this, 190, true);
	});

	$("#number").blur(function(){
		tables.validate_specialties_forms(this, 500, true);
	});

	$("#title").blur(function(){
		tables.validate_specialties_forms(this, 500, true);
	});

	$("#reason").blur(function(){
		tables.validate_specialties_forms(this, 500, true);
	});

	saveBtn.off('click');
	saveBtn.on('click', () => tables.save_speciality(id));
};
