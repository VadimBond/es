const popup = require('../popup.js');
const manage_blanks = require('./manage-blanks.js');
const generate_pdf = require('./generatePdf');
const createSwal = require('../../modules/createSwal').createSwal;


function updateGenerated(){
	var table = $("#variant-blank-table");

	$.ajax({
		method: 'POST',
		url: '/get_speciality_department',

		success: function (response) {
			table.html('');
			table.append(`<tr>
                            <th>Назва кафедри</th>
                            <th>Назва спеціальності</th>
                            <th>Дії</th>
                         </tr>`);
			response.forEach(function(element) {
				var row = "";
				row += `
                        <tr>
                            <td>
                                <div>${element.name_cafedres}</div>
                            </td>
                            <td>
                                <div>${element.name_speciality}</div>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default btn-info show-blanks" data-id-speciality="${element.id}">
                                    Перегляд варіантів
                                </button>
                                <button class="btn btn-default genListBtn" data-id-speciality="${element.id}" type="button">
                                    Список завдань
                                </button>
                            </td>
                        </tr>`;

				table.append(row);
			});

			const showBlanksBtn = $('#variant-blank-table .show-blanks'),
				genListBtn = $('#variant-blank-table .genListBtn');

			showBlanksBtn.off('click');
			showBlanksBtn.on('click', manage_blanks.manageBlanks);

			genListBtn.off('click');
			genListBtn.on('click', manage_blanks.generateList);

		},
		error: function (error) {
			console.log(error);
		}
	});
}

function updateVerifyTable(){
	var table = $("#verifyTable");

	$.ajax({
		method: 'POST',
		url: '/get_specialties',

		success: function (response) {
			table.html('');
			table.append(`<tr>
                            <th>Назва <br> спеціальності</th>
                            <th>Статус</th>
                            <th>Дата останьої <br> зміни статусу</th>
                            <th class="lastTh"></th>
                        </tr>`);
			response.forEach(function(element) {
				if(element.status != 'in_development') {
					let status = "";
					if( element.status == "in_process") {
						status = "на перевірці";
					}
					else if( element.status == "approved") {
						status = "підтверджено";
					}
					else {
						status = "на доопрацюванні";
					}

					var row = `<tr><td style = "display:none">${element.id}</td>`;
					row += ` <td><div>${element.name_speciality}</div></td>
                                <td><div>${status}</div></td>
                                 <td><div>${element.updated_at}</div></td>`;
					if(element.status == 'in_process') {
						row += ` <td class = "lastTd">                                          
                                    <a href="/view-blank/${element.id}" type= "button" class="btn btn-default table-buttn">Відкрити бланк
                                    </a>
                                    
                                    <a href="/edit-blank/${element.id}" type= "button" class="btn btn-default table-buttn">Редагувати бланк
                                    </a>
                                </td>`;
					}
					else if (element.status == 'approved') {
						row += `
                                <td class="lastTd">
                                    <button data-id="${element.id}" type="button" class="btn btn-default btn-pdf table-buttn">
                                        Екзаменаційні завдання (PDF)
                                    </button>
                                      <button data-id="${element.id}" type="button" class="btn btn-default btn-reverse table-buttn">
                                       Відмінити верифікацію
                                    </button>
                                    <a class="btn btn-default table-buttn" href="/edit-blank/${element.id}" type="button">
                                        Редагувати бланк
                                    </a>
                                </td>`;
					}
					else {
						row += `
                            <td>
                                <button class="btn btn-default btn-denied" data-declined-reason="${element.declined_reason}" type="button">
                                    Переглянути причину відмови
                                </button>
                            </td>`;
					}
					row += `</tr>`;
					table.append(row);
				}
			});

			const pdfGenBtn = $('#verifyTable .btn-pdf'),
				deniedBtn = $('#verifyTable .btn-denied'),
				reverseBtn = $('#verifyTable .btn-reverse');

			reverseBtn.off('click');
			reverseBtn.on('click', e => {
				const id = $(e.currentTarget).attr('data-id'),
					isConfirm = confirm('Відмінити верифікацію?');

				if(isConfirm) {
					reverseVerification(id);
				}
			});

			pdfGenBtn.off('click');
			pdfGenBtn.on('click', e => {
				const id = $(e.currentTarget).attr('data-id');

				generate_pdf.openPdfMenu(id);
			});

			deniedBtn.off('click');
			deniedBtn.on('click', e => {
				const declinedReason = $(e.currentTarget).attr('data-declined-reason');

				openDeclined(declinedReason)
			})

		},
		error: function (error) {
			console.log(error);
		}
	});
}

function reverseVerification(id){

	$.ajax({
		method: 'POST',
		url: '/reverse_verification',
		data: {'id' : id },
		success: function (response) {
			updateVerifyTable();
			createSwal({
				title: 'Верифікація відмінена',
				type: 'success'
			});
		},
		error: function (error) {
			createSwal({
				title: 'Помилка при відміні верифікації',
				type: 'error'
			});
		}
	});
}

function openDeclined(reason){
    createSwal({
        title: 'Причина відмови: ' + reason,
        type: 'info'
    });
}

function updateUserTable(){
	var table = $("#repTable");

	$.ajax({
		method: 'POST',
		url: '/get_data_for_table',
		success: function (response) {
			table.html('');
			table.append(`<tr>
                            <th>Ім'я</th>
                            <th>Прізвище</th>
                            <th>Роль</th>
                            <th>Email</th>
                            <th>Кафедра</th>
                       </tr>`);
			response.users.forEach(function(element) {
				if(response.id != element.id) {
					var row = "";
					row += `<tr> <td><div>${element.first_name}</div></td>
                                <td><div>${element.last_name}</div></td>
                                 <td><div>${element.name}</div></td>
                                 <td><div>${element.email}</div></td>`;
					if(element.name_cafedres != null) {
						row += `<td><div>${element.name_cafedres}</div></td>`;
					}
					else {
						row +="<td></td>";
					}

					row += `<td class = tableButtons>
                                <span class="category-pencil" data-toggle="tooltip" title="Редагувати користувача">
                                    <a href="/edit-representative/${element.id}" type = "button" class="btn btn-default">
                                        <span   class="glyphicon glyphicon-pencil" aria-hidden="true" ></span>
                                        <span  style = "display:none;"  class="glyphicon glyphicon-floppy-disk" aria-hidden="true" ></span>
                                    </a>
                                 </span>
                                <span class="category-trash r_margin" data-toggle="tooltip" title="Видалити користувача">
                                    <button  type="button" class="btn btn-default del_rep">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </button>
                                </span></td>
                            <td style = "display:none" class = "users_id" >${element.id}</td>
                        </tr>`;

					table.append(row);
				}
			});

			$('.del_rep').on('click', deleteRepres);
		},
		error: function (error) {
			console.log(error);
		}
	});
}

function selectDepChanged(){
	var table = $("#specTable");
	var depId =   $("#depSelect").val();
	table.html('');

	$.ajax({
		method: 'POST',
		url: '/get_cafedres',
		data: {},
		success: function (response) {
			response.forEach(function(cafedre) {
				if (cafedre.id == depId) {
					$("#selected_spec_name").html(`Спеціальності кафедри ${cafedre.name_cafedres}`);
				}
			});
			$.ajax({
				method: 'POST',
				url: '/get_specialties',
				data: {},
				success: function (response) {
					let count = 0;

					response.forEach(function(speciality) {
						if (speciality.id_cafedres == depId) {
							count ++;
							table.append(`<tr>        <td style = "display:none" class = "special_id"> ${speciality.id }</td>
                                <td><div>${speciality.name_speciality}</div>
                                    <input  class=" sel_block hideSelect"></td>
                                <td class = tableButtons>
                                    <span class="category-pencil" data-toggle="tooltip" title="Редагувати спеціальність" >
                                        <a  href="/edit-speciality/${speciality.id}" type= "button" class="btn btn-default" >
                                            <span   class="glyphicon glyphicon-pencil" aria-hidden="true" ></span>
                                        </a>
                                    </span>
                                    <span class="category-trash r_margin" data-toggle="tooltip" title="Видалити спеціальність">
                                        <button type="button" class="btn btn-default del_special">
                                            <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                                        </button>
                                    </span></td>
                                </tr>`);
						}
					});
					if(count === 0) {
						table.children().first().remove();
						table.prepend(`<tr><th> Спеціальності данної кафедри відсутні</th></tr>`);
					}
					else {
						table.prepend(`<tr><th>Назва</th></tr>`);
					}
					$('.del_special').on('click', deleteSpecialization);
					table.append(`<tr class="omu_add2" style="cursor: pointer"><td style="background-color: #89CA9E;"><div>+ Додати спеціальність</div></td></tr>`);

					$('.omu_add2').click(function () {
						window.location.href = `/add-speciality/${depId}`;
					});

					$('.omu_add2').hover(function () {
						$('.omu_add2 td').css('background-color', '#00a65a');
						$('.omu_add2 td').css('color', 'white');
					});

					$('.omu_add2').mouseleave(function () {
						$('.omu_add2 td').css('background-color', '#89CA9E');
						$('.omu_add2 td').css('color', 'black');
					});
				},
				error: function (error) {
					console.log(error);
				}
			});
		},
		error: function (error) {
			console.log(error);
		}
	});
}

function UpdateSelect(){
	$.ajax({
		method: 'POST',
		url: '/get_cafedres',
		data: {
		},
		success: function (response) {
			var dep =  $("#depSelect");
			dep.html('');
			response.forEach(function(cafedre) {
				var option = document.createElement("option");
				option.text = cafedre.name_cafedres;
				option.value = cafedre.id;
				dep.append(option);
			} );
			dep.val('');
		},
		error: function (error) {
			console.log(error);
		}
	});
}

function  deleteRepres(){
	$('#confirmContainer').css({display: 'flex'});
	$('#confirm').html(popup.getPopup("responsible"));

	var row = $(this).parent().parent().parent();

	$('#x, #no').on('click', function () {
		$('#confirmContainer').hide();
	});

	$('#yes').on('click', function (){
		row.remove();
		var id  = row.find(".users_id").text();
		$('#confirmContainer').hide();

		$.ajax({
			method: 'POST',
			url: '/delete_representative',
			data: {
				'id': id
			},
			success: function (response) {
				createSwal({
					title: 'Користувач успішно видалений',
					type: 'success'
				});
			},
			error: function (error) {
				console.log(error);

				createSwal({
					title: 'Виникла помилка при видаленні користувача',
					type: 'error'
				});
			}
		});

		$('#confirmContainer').hide();
	});
}

function deleteDepartment(){
	$('#confirmContainer').css({display: 'flex'});
	$('#confirm').html(popup.getPopup("department"));

	var row = $(this).parent().parent().parent();

	$('#x,#no').on('click', function () {
		$('#confirmContainer').hide();
	});

	$('#yes').on('click', function () {
		var id  = row.find('.depart_id').text();
		$('#confirmContainer').hide();

		$.ajax({
			method: 'POST',
			url: '/delete_department',
			data: {
				'id': id
			},
			success: function () {
				row.remove();
				$('#specTable').text('');
				UpdateSelect();

				$("#selected_spec_name").text("");
				updateUserTable();
				updateVerifyTable();
				updateGenerated();

				createSwal({
					title: 'Кафедра успішно видалена',
					type: 'success'
				});
			},
			error: function (error) {
				console.log(error);

				createSwal({
					title: 'Виникла помилка при видаленні кафедри',
					type: 'error'
				});
			}
		});

		var rowCount = $("#depTable tr").length;

		if(rowCount < 3){
			$("#specAdd").addClass('disabled');
		}

		$('#confirmContainer').hide();
	});
}

function validate_specialties_forms(id, valLength, req) {
	$(id).siblings().remove();
	let value = $(id).val();

	if (value.length > valLength) {
		$(id).after("<div class=\"validate-alert\">Поле не має перевищувати " + valLength + " символів</div>");
		$(id).css("border","1px solid red");
	}
	else if (value.trim() == 0 && req){
		$(id).after("<div class=\"validate-alert\">Поле не має бути пустим</div>");
		$(id).css("border","1px solid red");
	}
	else {
		$(id).css("border","1px solid green");
		return true;
	}
	return false;
}

function save_speciality(e){
	$('#saving .save-btn').prop('disabled', true);

	var id = e;
	var name = $("#name").val();
	var number = $("#number").val();
	var dep = $("#dep").val();
	var title=$("#title").val();
	var reason=$("#reason").val();
	var type =$("#typeSpec option:selected").val();

	var nameIsValid = validate_specialties_forms("#name", 190, true);
	var numberIsValid  = validate_specialties_forms("#number", 500, true);
	var titleIsValid = validate_specialties_forms("#title", 500, true);
	var reasonIsValid = validate_specialties_forms("#reason", 500, true);
	if(nameIsValid && numberIsValid && titleIsValid && reasonIsValid) {
		if(id == 0) {
			$.ajax({
				method: 'POST',
				url: '/add_specialisation',
				data: {
					'name': name,
					'number': number,
					'dep': dep,
					'title':title,
					'reason':reason,
					'type':type
				},
				success: function (response) {
					$('#saving .save-btn').prop('disabled', false);

					if(response == 'error'){
						swal({
							title: "Спеціальність не була створена",
							type: "error"
						});
					}
					swal({
						title: "Спеціальніть була успішно збережена",
						type: "success"
					}, () => {
						window.location.href = "/omu";
					});

				},
				error: function (error) {
					$('#saving .save-btn').prop('disabled', false);
					console.log(error);
				}
			});
		}
		else {
			$.ajax({
				method: 'POST',
				url: '/update_specialisation',
				data: {
					'id': id,
					'name': name,
					'number': number,
					'dep': dep,
					'title':title,
					'reason':reason
				},
				success() {
					swal({
						title: "Спеціальніть була успішно збережена",
						type: "success"
					}, () => {
						$('#saving .save-btn').prop('disabled', false);
						window.location.href = "/omu";
					});
				},
				error: function (error) {
					swal({
						title: "Спеціальність не була оновлена",
						type: "error"
					});
					$('#saving .save-btn').prop('disabled', false);
					console.log(error);
				}
			});
		}
	}
	else {
		$('#saving .save-btn').prop('disabled', false);
	}
}

function deleteSpecialization(){
	var table = $("#specTable");

	$('#confirmContainer').css({display: 'flex'});
	$('#confirm').html(popup.getPopup("specialization"));

	let row = $(this).parent().parent().parent();

	$('#x, #no').on('click', function () {
		$('#confirmContainer').hide();
	});

	$('#yes').on('click', function () {
		row.remove();
		$('#confirmContainer').hide();

		let rowCount = $("#specTable tr").length;

		if(rowCount == 2){
			table.children().first().remove();
			table.prepend(`<tр><th> Спеціальності данної кафедри відсутні</th></tр>`);
		}

		let id  = row.find(".special_id").text();

		$.ajax({
			method: 'POST',
			url: '/delete_specialisation',
			data: {
				'id': id
			},
			success: function () {
				updateUserTable();
				updateVerifyTable();
				updateGenerated();
				createSwal({
					title: 'Спеціальність успішно видалена',
					type: 'success'
				});
			},
			error: function (error) {
				console.log(error);

				createSwal({
					title: 'Виникла помилка при видаленні спеціальності',
					type: 'error'
				});
			}
		});
	});
}

module.exports = {
	updateUserTable: updateUserTable,
	updateVerifyTable: updateVerifyTable,
	updateGenerated: updateGenerated,
	selectDepChanged: selectDepChanged,
	save_speciality: save_speciality,
	deleteDepartment: deleteDepartment,
	validate_specialties_forms: validate_specialties_forms,
};