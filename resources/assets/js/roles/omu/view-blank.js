const popup = require('../popup.js');
const createSwal = require('../../modules/createSwal').createSwal;

window.onload = function() {
	$("#depSelect").val('');
	popup.moving();
	$('#x').on('click', function () {
		popup.hidePdf("pdf_set");
	});

	$(document).mouseup(function (e) {
		popup.exitPdf(e);
	});

	const approveBtn = $('.panel-footer .btn-success'),
		declineBtn = $('.panel-footer .btn-danger'),
		idSpeciality = approveBtn.attr('data-id-speciality');

	approveBtn.on('click', () => approve(idSpeciality));
	declineBtn.on('click', popDecline);
};

function popDecline() {
    $('#pdf_set').css({display : "block", left : "50%", top : "60%", transform: "translateX(-50%) translateY(-50%)", boxShadow: "0 0 100vw 100vw rgba(0, 0, 0, 0.5)"});

	$('#x,#no').on('click', function (){
		$('#pdf_set').hide();
	});

	$('#yes').on('click', function () {
		var reason = document.getElementById("reason").value;
		if(!reason) {
			createSwal({
				title: "Введіть причину",
				type: 'error'
			});
		}
		else {
			document.getElementById("statuses").style.display = "none";
			document.getElementById("back").style.display = "block";
			$('#pdf_set').hide();

			const idSpeciality = $('#pdf_set').attr('data-id-speciality');
			decline(idSpeciality, reason);
		}
	});
}

function approve(id){
    document.getElementById("statuses").style.display = "none";
    document.getElementById("back").style.display = "block";
    $.ajax({
	    method: 'POST',
        url: '/edit_status_specialty',
        data: {
            'id': id,
            'status': 'approved'
        },
        success: function (response) {
            swal({
                title: "Бланк був успішно верифікований",
                type: "success"
            }, () => {
                window.location.href = "/omu";
            });
        },
        error: function (error) {
	    	console.log(error);

            swal({
                title: "Помилка при верифікацій бланку",
                type: "error"
            });
        }
    });
}

function decline(id, reason) {
    $.ajax({
	    method: 'POST',
        url: '/edit_status_specialty',
        data: {
            'id': id,
            'status': 'declined',
            'reason':reason
        },
        success: function (response) {
            swal({
                title: "Бланк був успішно відправлений на доопрацювання",
                type: "success"
            }, () => {
                window.location.href = "/omu";
            });
        },
        error: function (error) {
	    	console.log(error);

            swal({
                title: "Помилка при верифікацій бланку",
                type: "error"
            });
        }
    });
}