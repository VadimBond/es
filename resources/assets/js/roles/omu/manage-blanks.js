const createSwal = require('../../modules/createSwal').createSwal;

function manageBlanks(e) {
	const idSpeciality = $(e.currentTarget).attr('data-id-speciality');

	$.ajax({
		url: '/get-blanks',
		method: 'post',
		data: {idSpeciality},
		success(response) {
			if(response.length > 0) {
				if(response.length > 13) {
					$('#listBlankPopup').addClass('scroll-class');
				}
				else {
					$('#listBlankPopup').removeClass('scroll-class');
				}

				renderBlankList(response);
			}
			else {
				createSwal({
					title: 'Немає згенерованих бланків',
					text: 'Перейдіть на сторінку верифікація бланків та згенеруйте бланки',
					type: 'info'
				});
			}
		},
		error(err) {
			createSwal({
				title: 'Не вдалося отримати бланки',
				type: 'error'
			});
			console.log(err);
		}
	})
}

function updateButtonsListeners() {
	const viewBlankBtn = $('.view-blank'),
		downloadTaskBtn = $('.download-task'),
		downloadAnswers = $('.download-answers'),
		deleteBlankBtn = $('.delete-blank');

	viewBlankBtn.off('click');
	downloadTaskBtn.off('click');
	downloadAnswers.off('click');
	deleteBlankBtn.off('click');

	viewBlankBtn.on('click', viewBlank);
	downloadTaskBtn.on('click', downloadBlank);
	downloadAnswers.on('click', downloadBlank);
	deleteBlankBtn.on('click', deleteBlank);
}

function renderBlankList(blankData) {
	$('#close-btn').off('click');
	$('#close-btn').on('click', removePopup);

	function removePopup() {
		let listBlank = $('#listBlankPopup');
		let deleteBlur = $('#blur-screen');
		listBlank.addClass('hidden');
		deleteBlur.addClass('hidden');
	}

	const blurScreen = $('#blur-screen'),
		popup = $('#listBlankPopup'),
		list = $('.listBlanks tbody');

	list.empty();

	for(let blank of blankData) {
		let newBlank = `
            <tr>
                <td>${blank['variant']}</td> 
                <td>
                    <button class="btn btn-default btn-info view-blank" data-id-blank="${blank['id']}" value="${blank['url_blank']}">
                        Переглянути бланк
                    </button>
                    </td> 
                <td>
                    <button class="btn btn-default btn-info download-task" data-id-blank="${blank['id']}" value="${blank['url_blank']}">
                        Завантажити завдання
                    </button>
                </td> 
                <td>
                    <button class="btn btn-default btn-info download-answers" data-id-blank="${blank['id']}" value="${blank['url_blank']}">
                        Завантажити бланк із відповідями
                    </button>
                    </td> 
                <td>
                    <button class="btn btn-default btn-warning delete-blank" data-id-blank="${blank['id']}">
                        Видалити згенерований варіант
                    </button>
                </td> 
            </tr>`;

		list.append(newBlank);
	}

	updateButtonsListeners();

	blurScreen.removeClass('hidden');
	popup.removeClass('hidden');
}

function viewBlank() {
	const button = $(this),
		listBlank = $('#listBlankPopup'),
		iframe = $('#pdf-frame');

	const baseUrl = button.attr('value'),
		url = `${baseUrl}/view.pdf`;

	iframe.attr('src' ,url);

	listBlank.addClass('hidden');
	iframe.removeClass('hidden');
}

function downloadBlank() {
	const button = $(this);

	const actionType = button.attr('class'),
		baseUrl = button.val();

	if(actionType.includes('download-task')) {
		loadBlanks('task', baseUrl);
	}
	else if (actionType.includes('download-answers')) {
		loadBlanks('answers', baseUrl);
	}
	else {
		console.log('Err');
	}
}


function loadBlanks(typeBlank,baseUrl) {

	$(document).find('.bookshelf_wrapper').show();
	$(document).find('.hideBehind').show();
	$('#all').css("opacity","0.5");

	let nmbToDownload = 1;

    let url = '';
    if (typeBlank === 'task') {
        url = `${baseUrl}/task.pdf`;
    }
    else if(typeBlank === 'answers'){
        url = `${baseUrl}/answer.pdf`;
    }

	$('body').append($('<a id="downloadLink" style="display:none;"></a>'));
	const newLink = $('#downloadLink');

	newLink.attr('href', url);
	newLink.attr('download', typeBlank);

	for(let i = 0; i < nmbToDownload; i++) {
		newLink[0].click();
	}

	$(newLink).remove();

	$(document).find('.bookshelf_wrapper').hide();
	$(document).find('.hideBehind').hide();
	$('#all').css("opacity","1");
}

function deleteBlank() {
	const button = $(this),
		idBlank = button.attr('data-id-blank');

	const isConfirmed = confirm('Ви дійсно хочете видалити даний згенерований варіант?');

	if(isConfirmed) {
		const blur = $('#blur-screen'),
			listBlanksPopup = $('#listBlankPopup');

		blur.addClass('hidden');
		listBlanksPopup.addClass('hidden');

		$.ajax({
			url: '/delete-blank',
			method: 'post',
			data: {idBlank},
			success() {
				createSwal({
					title: 'Успішно видалено',
					type: 'success'
				});
			},
			error(err) {
				createSwal({
					title: 'Помилка при спробі видалення бланку',
					type: 'error'
				});
				console.log(err);
			}
		});
	}
}

function generateList() {
	const specialityId = $(this).attr('data-id-speciality');
	const data = {
		specialityId
	};

	$.ajax({
		url: '/generateList',
		method: 'POST',
		data: data,
		success(res) {
			if(res.url) {

				const blurScreen = $('#blur-screen'),
					iframe = $('#pdf-frame');

				blurScreen.removeClass('hidden');
				iframe.attr('src' ,res.url);
				iframe.removeClass('hidden');
			}
			else {
				createSwal({
					title: `${res} завдання успішно згенеровані`,
					type: 'success'
				});
			}
		},
		error(err) {
			createSwal({
				title: 'Помилка при генерації Pdf',
				type: 'error'
			});

			console.log(err);
		}
	});
}

module.exports = {
	manageBlanks: manageBlanks,
	loadBlanks: loadBlanks,
	generateList: generateList
};
