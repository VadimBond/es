const createSwal = require('../../modules/createSwal').createSwal;

function openPdfMenu(specialityId) {
    var container = $("#showPdf");

    container.css({display: 'flex'});

    var pdfBtn = $('#generatePdf');

    $(".inp").on('input', updateVal);

    pdfBtn.off('click');
    pdfBtn.on('click', () => generatePdf(specialityId));
}

function updateVal() {
    let variants = $('#variants'),
        nmbVariants = $(variants).val(),
        maxVar = variants.attr("max");
    
    nmbVariants = +nmbVariants > +maxVar ? maxVar : nmbVariants;
    $(variants).val(nmbVariants);
}

function generatePdf (specialityId) {
    let variants = $('#variants').val();

    variants = variants ? variants : 1;

	const data = {
		variants,
		specialityId
	};
	$("#showPdf").hide();
    $(document).find('.bookshelf_wrapper').show();
    $(document).find('.hideBehind').show();
    $('#all').css("opacity","0.5");

	$.ajax({
		url: '/generatePdf',
		method: 'POST',
		data: data,
		success(res) {
            $(document).find('.bookshelf_wrapper').hide();
            $(document).find('.hideBehind').hide();
            $('#all').css("opacity","1");
			if(res.url) {
                const blurScreen = $('#blur-screen'),
                    iframe = $('#pdf-frame');

                blurScreen.removeClass('hidden');
                iframe.attr('src' ,res.url);
                iframe.removeClass('hidden');
			}
			else {
                createSwal({
                    title: `${res} завдання успішно згенеровані`,
                    type: 'success'
                });
			}
		},
		error(err) {
			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");

			const {nmbGenVariants, variantsToGen} = err.responseJSON;

			if(err.status === 400) {
				createSwal({
					title: nmbGenVariants > 0 ?
						`Було успішно згенеровано ${nmbGenVariants} із ${variantsToGen} варіантів. Недостатньо питань для генереції нових білетів` :
						'Недостатньо питань для генерації нового унікального білету',
					text: 'Необхідно очистити історію питань, які вже брали участь у генерації білету або створити нові питання',
					type: 'info'
				});
				$("#showPdf").show();

				const genBlock = $('#generateBlock');
				const resetTasks = $('#resetTasks');

				genBlock.addClass('hidden');
				resetTasks.removeClass('hidden');

				resetTasks.off('click');
				resetTasks.on('click', () => resetQuestionsHistory(specialityId));
			}
			else {
				createSwal({
					title: 'Помилка при генерації Pdf',
					type: 'error'
				});
			}
            console.log(err);
		}
	});
}

function resetQuestionsHistory(idSpeciality) {
	$.ajax({
		url: '/reset-questions-history',
		method: 'post',
		data: {idSpeciality},
		success() {
			createSwal({
				title: 'Історія генерації питань успішно очищена',
				type: 'success'
			});

			const genBlock = $('#generateBlock');
			const resetTasks = $('#resetTasks');

			genBlock.removeClass('hidden');
			resetTasks.addClass('hidden');
		},
		error(err) {
			createSwal({
				title: 'Помилка при спробі очищення історії генерації питань',
				type: 'error'
			});
			console.log(err);
		}
	})
}

module.exports = {
    openPdfMenu: openPdfMenu
};