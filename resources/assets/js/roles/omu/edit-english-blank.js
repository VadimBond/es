const redactorMultiblock = require('../../redactorMultiblock').redactorMultiblock;
const createSwal = require('../../modules/createSwal').createSwal;
const {handleChangeRadio, setWidthImage} = require('../representative/images');
const {onChangeListSelect} = require('../representative/manageEnglishTask/manageTasks');
const {filterChangedElements, typeElements} = require('./filterChangedEnElements');
const saveEnglishTasks = require('./saveEnglishTasks');
const {validateQuestionsAndAnswers, cleanValidation} = require('./edit-blank');
const setWidthListeners = require('../../SetAutoWidth');

window.onload = function() {
	const multiblocks = $('.multiblock');
	$('.list-block').find('select').on('change', onChangeListSelect);
	$('.list-container').find('select').on('change', onChangeListSelect);

	multiblocks.on('click', redactorMultiblock);
	multiblocks.click();

	const addImageWidthRadio = $('.imageWidthContainer input[type="radio"]'),
		addChangeWidthBtn = $('.changeWidthBtn');

	addImageWidthRadio.off();
	addImageWidthRadio.on('change', (e) => handleChangeRadio(e));
	addChangeWidthBtn.off();
	addChangeWidthBtn.on('click', (e) => {
		e.preventDefault();
		const imageWidth = $(e.currentTarget).prev().val();
		setWidthImage(imageWidth, $(e.currentTarget).prev().prev().find('input'));
	});
	const approveBtn = $('#approve');

	const handleSubmit = (e) => {
		var valid = true;

		cleanValidation($('.multiblock'));

		$(".validate-alert").remove();

		$('.multiblock').each(function(){
			if (!validateQuestionsAndAnswers(this)) {
				valid = false;
			}
		});

		$('input[type="text"]').removeClass('input-non-valid');

		$('.category-block').each(function () {
			if($(this).find('input[type="checkbox"]:checked').length === 0 && $(this).find('input[type="checkbox"]').length !== 0) {
				valid = false;
			}
		});

		$('.task-container input[type="text"]').each(function() {
			if($(this).val() == "") {
				valid = false;
				$(this).addClass('input-non-valid');
			}
		});

		if (!valid) {
			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity","1");
			createSwal({
				title: 'Питання не пройшли валідацію',
				type: 'error'
			});
			e.preventDefault();
			return;
		}

		$(document).find('.bookshelf_wrapper').show();
		$(document).find('.hideBehind').show();
		$('#all').css("opacity","0.5");

		e.preventDefault();

		const submitBtn = $(e.currentTarget);
		submitBtn.off('click');

		$(document).find('.bookshelf_wrapper').show();
		$(document).find('.hideBehind').show();
		$('#edit-container').css("opacity", "0.3");

		const {textList, textQuestions, listAnswers, question} = typeElements;

		const container = $('.task-container'),
			textsWithList = container.find('.text-list-block').toArray(),
			textsWithQuestions = container.find('.text-with-question').toArray(),
			listsWithAnswers = container.find('.list-with-answers-block').toArray(),
			questionsBlock = container.find('.question-block').toArray(),
			commissionersContainers = [...$('.verifyTableBlock .category-block')];

		const commissioners = commissionersContainers.map( (commissionerContainer) => {
			const id = $(commissionerContainer).attr('data-id');
			const position = $(commissionerContainer).find('.commPosition').val();
			const initials = $(commissionerContainer).find('.commName').val();

			return {id, position, initials};
		});

		const filteredTextsWithList = filterChangedElements(textsWithList, textList),
			filteredTextsWithQuestions = filterChangedElements(textsWithQuestions, textQuestions),
			filteredListAnswers = filterChangedElements(listsWithAnswers, listAnswers),
			filteredQuestions = filterChangedElements(questionsBlock, question);

		const allTasks = [
			{collection: filteredTextsWithList, typeElements: textList},
			{collection: filteredTextsWithQuestions, typeElements: textQuestions},
			{collection: filteredListAnswers, typeElements: listAnswers},
			{collection: filteredQuestions, typeElements: question},
		],
			filteredTasks = allTasks.filter( (task) => task.collection.length > 0),
			idSpeciality = $('#edit-container').attr('data-id-speciality');

		saveEnglishTasks(filteredTasks,commissioners,idSpeciality);

		$(document).find('.bookshelf_wrapper').hide();
		$(document).find('.hideBehind').hide();
		$('#all').css("opacity","1");
	};
	approveBtn.on('click', handleSubmit);
	update();

	setWidthListeners();
};
function update() {
	var newComm = `<div class="category-block" style="display: block;">
    <div class="row" style="margin-bottom: 5px">
		<div class="col-xs-12 form-inline">
			<div class = "hidden idsComm">0</div>
			<input class="item-category form-control commPosition" style="width: 45%" type="text" required placeholder="Посада члена комісії" >
			<input class="item-category form-control commName" style="width: 45%" type="text" required placeholder="ПІБ члена комісії" >
		<span  class = "category-add-comm" data-toggle="tooltip" title="Додати члена комісії">
		<button type="button"  class="btn btn-default">
		<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
		</button>
		</span>
		<span class="category-delete-comm" data-toggle="tooltip" title="Видалити члена комісії" style="display: none;">
		<button type="button" class="btn btn-default">
		<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
			</button>
			</span>
		</div> 
    </div>`;
	var rmCommBtn = $('.category-delete-comm'),
		addCommBtn = $('.category-add-comm'),
		commContainer = $('#comm');
	updateListeners();

	var nmbComm = $(commContainer).find('.category-block').length;
	if(nmbComm > 1) {
		commContainer.find('.category-delete-comm').show();
	}

	function updateListeners() {
		rmCommBtn = $('.category-delete-comm');
		addCommBtn = $('.category-add-comm');

		addCommBtn.off('click');
		addCommBtn.on('click', addComm);

		rmCommBtn.off('click');
		rmCommBtn.on('click', rmComm);
	}

	function addComm() {

		var nmbComm = $(commContainer).find('.category-block').length;

		if(nmbComm < 5) {
			$('#comm').append(newComm);
			updateListeners();
			if(nmbComm > 0) {
				commContainer.find('.category-delete-comm').show();
			}
		}
		else {
			createSwal({
				title: 'Максимальна к-ть членів комісії',
				type: 'error'
			});
		}

		if(nmbComm == 1) {
			commContainer.first().find('.category-delete-comm').show();
		}
	}

	function rmComm(){
		var nmbComm = $(commContainer).find('.category-block').length,
			comm = $(this).parent().parent().parent();

		if(nmbComm > 1) {
			comm.remove();
			updateListeners();
		}
		if(nmbComm == 2) {
			commContainer.first().find('.category-delete-comm').hide()
		}

		// show all adding buttons
		commContainer.find('.category-add-comm').toArray().map(el => $(el).show());
	}
}