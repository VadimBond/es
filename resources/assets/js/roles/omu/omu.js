window.onload = function() {
	$('#add_speciality').hide();

	const popup = require('../popup.js');
	const tables = require('./tables.js');

	$("#depSelect").val('');
	popup.moving();
	$('#x').on('click', () => {
		$('#generateBlock').removeClass('hidden');
		$('#resetTasks').addClass('hidden');

		popup.hidePdf("showPdf");
	});

	$('#exit, #closeDeclined').on('click', function (){
		$("#declined").hide();
	});


	$('a[data-toggle="tab"]').on('click', function () {
		tables.updateUserTable();
		tables.updateVerifyTable();
		tables.updateGenerated();
	});

	const blurScreen = $('#blur-screen'),
		pdfFrame = $('#pdf-frame'),
		listBlank = $('#listBlankPopup'),
		nmbToDownloadBlank = $('#nmbToDownloadBlank');

	blurScreen.on('click', () => {
		blurScreen.addClass('hidden');
		pdfFrame.addClass('hidden');
		listBlank.addClass('hidden');
		nmbToDownloadBlank.addClass('hidden');
	});

	$('#depSelect').on('change', function() {
		$('#add_speciality').show();
	});
	$('#depSelect').on('change', tables.selectDepChanged);

	$('#depTable .category-trash .btn').on('click', tables.deleteDepartment);

	$('.omu_add1').click(function(){
		window.location.href = "/new-department";
	});

	$(document).find('.bookshelf_wrapper').hide();
	$(document).find('.hideBehind').hide();
	$('#all').css("opacity", "1");
};