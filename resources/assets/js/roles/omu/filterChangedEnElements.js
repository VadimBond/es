const typeElements = {
	'textList': 'textList',
	'textQuestions': 'textQuestions',
	'listAnswers': 'listAnswers',
	'question': 'question'
};

const regex = /&(|nbsp|amp|quot|lt|gt);/g;
const entityMap = {
	"nbsp": " ",
	"amp" : "&",
	"quot": "\"",
	"lt"  : "<",
	"gt"  : ">"
};
const escapeHtml = (string) => String(string).replace(regex, (match, entity) => entityMap[entity]);

const checkChangeImageWidth = (container) => {
	const prevImageWidth = container.attr('data-prev-image-width'),
		autoWidthBtn = container.find('.enableAutoWidth ');
	let currImageWidth;

	if(autoWidthBtn.hasClass('btn-success')) {
		currImageWidth = 'auto';
	}
	else {
		const checkedRadio = container.find('input:radio:checked').val(),
			customWidthInput = container.find('.customWidthInput');

		currImageWidth = checkedRadio !== 'custom' ? checkedRadio : customWidthInput.val();
	}

	return prevImageWidth !== currImageWidth;
};

const checkChangeAnswers = (answers) => {
	return answers.some( (answer) => {
		let prevContent = escapeHtml($(answer).attr('data-prev-answer-value')),
			currContent = escapeHtml($(answer).find('.item-name').val()),
			prevStateChecked = parseInt($(answer).attr('data-is-checked')),
			currStateChecked = $(answer).find('.correct-answer').is(':checked') ? 1 : 0,
			isChangeContent = prevContent !== currContent,
			isChangeStateChecked = prevStateChecked !== currStateChecked;

		return isChangeContent || isChangeStateChecked;
	});
};

const checkChangeQuestions = (questions) => {
	return questions.some( (question) => {
		let imageWidthContainer = $(question).find('.imageWidthContainer'),
			answersContainer = $(question).find('.first-answers-block .item').toArray(),
			prevContent = escapeHtml($(question).attr('data-prev-question-value')),
			currContent = escapeHtml($(question).find('.category .question-name').val()),
			isChangeContent = prevContent !== currContent,
			isChangeImageWidth = checkChangeImageWidth(imageWidthContainer),
			isChangeAnswers = checkChangeAnswers(answersContainer);

		return isChangeImageWidth || isChangeContent || isChangeAnswers;
	});
};

const checkChangeListElements = (listRows) => {
	return listRows.some( (row) => {
		let textCol = $(row).find('.textCol'),
			positionCol = $(row).find('.positionCol'),
			prevText = textCol.attr('data-prev-value'),
			prevPosition= positionCol.attr('data-prev-value'),
			currText = textCol.find('input').val(),
			currPosition = positionCol.find('select').val(),
			isChangedText = prevText !== currText,
			isChangedPosition = prevPosition !== currPosition;

		return isChangedText || isChangedPosition;
	});
};

const filterChangedElements = (collectionElements, typeElement) => {
	const {textList, textQuestions, listAnswers, question} = typeElements;
	switch (typeElement) {
		case textList:
			return collectionElements.filter( (element) => {
				let textContainer = $(element).find('.text-container'),
					listContainer = $(element).find('.list-container'),
					listRows = listContainer.find('tbody tr').toArray(),
					imageWidthContainer = textContainer.find('.imageWidthContainer'),
					prevText = escapeHtml(textContainer.attr('data-prev-value')),
					currText = escapeHtml(textContainer.find('.multiblock').val()),
					isChangeImageWidth = checkChangeImageWidth(imageWidthContainer),
					isChangeText = prevText !== currText,
					isChangeListElements = checkChangeListElements(listRows);

				return isChangeImageWidth ||  isChangeText || isChangeListElements;
			});
		case textQuestions:
			return collectionElements.filter( (element) => {
				let textContainer = $(element).find('.text-container'),
					questionContainer = $(element).find('.questions-container'),
					questions = questionContainer.find('.category-block').toArray(),
					imageWidthContainer = textContainer.find('.imageWidthContainer'),
					prevText = escapeHtml(textContainer.attr('data-prev-value')),
					currText = escapeHtml(textContainer.find('.multiblock').val()),
					isChangeTextImageWidth = checkChangeImageWidth(imageWidthContainer),
					isChangedText = prevText !== currText,
					isChangedQuestions = checkChangeQuestions(questions);


				return isChangeTextImageWidth || isChangedText || isChangedQuestions;
			});
		case listAnswers:
			return collectionElements.filter( (element) => {
				let answerListContainer = $(element).find('.list-answers-container'),
					listContainer = $(element).find('.list-container'),
					answerListRows = answerListContainer.find('tbody tr').toArray(),
					listRows = listContainer.find('tbody tr').toArray(),
					isChangedListElements = checkChangeListElements(listRows),
					isChangedAnswerListElements = answerListRows.some( (answerListRow) => {
						let textCol = $(answerListRow).find('.textCol'),
							prevContent = textCol.attr('data-prev-value'),
							currContent = textCol.find('input').val();

						return prevContent !== currContent;
					});

				return isChangedAnswerListElements || isChangedListElements;
			});
		case question:
			return collectionElements.filter( (questionsContainer) => {
				let questions = $(questionsContainer).find('.category-block').toArray();

				return checkChangeQuestions(questions);
			});
		default:
			return [];
	}
};

module.exports = {
	filterChangedElements,
	typeElements
};