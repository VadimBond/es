<?php
use Illuminate\Support\Facades\Storage;



function saveImage ($base64, $id, $typeImage, $extension) {
	if($base64 != "0") {
		$convertedBase64 = str_replace(' ', '+', $base64);

		$image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $convertedBase64));
		$fileName = $typeImage.'_'.$id.$extension;

		Storage::disk('public')->put('blankImages/'.$fileName, $image);
		$url = Storage::url('blankImages/'.$fileName);

		return $url;
	}
	else {
		return 0;
	}
}

function deleteElementImages($elementId, $typeElement, $images) {
	$filtedImages = array_filter($images, function ($img) {
		return(strpos($img, 'data:image') === false);
	});

	$whiteList = array_map(function($imgPath) {
		$pathToremove = '/img/';
		return str_replace($pathToremove, '', $imgPath);
	}, $filtedImages);

	$fileName = $typeElement.'-'.$elementId;
	$allFiles = Storage::disk('images')->files('frontend/taskImages/');
	$filesToDelete = [];

	// remove image from array which will delete imgs.
	$filteredAllImages = array_filter($allFiles, function($imgPath) use ($whiteList){
		foreach($whiteList as $img) {
			if(strcmp($imgPath, $img) === 0) {
				return false;
			}
		}
		return true;
	});

	foreach ($filteredAllImages as $file) {
		if(strpos($file, $fileName) !== false) {

			array_push($filesToDelete, $file);
		}
	}

	Storage::disk('images')->delete($filesToDelete);
}
function saveImages ($elementId, $typeElement, $arrImages) {
	$urls = [];

	$fileName = $typeElement.'-'.$elementId;
	$allFiles = Storage::disk('images')->files('frontend/taskImages/');

	$filteredImages = array_filter($allFiles, function($imgPath) use ($fileName){
		if(strpos($imgPath, $fileName) !== false) {
			return true;
		}
		return false;
	});

	$imagesIndex = [];
	foreach ($filteredImages as $file) {
		$pathToRemove = 'frontend/taskImages/'.$fileName.'-';

		array_push($imagesIndex, intval(str_replace($pathToRemove, '', $file)));
	}

	$maxIndex = count($imagesIndex) > 0 ? max($imagesIndex) : 0;

	foreach ($arrImages as $index => $base64) {

		if (strpos($base64, 'data:image') !== false) {
			$convertedBase64 = str_replace(' ', '+', $base64);
			$image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $convertedBase64));

			$fileName = $typeElement . '-' . $elementId . '-' . ++$maxIndex;

			Storage::disk('images')->put('frontend/taskImages/' . $fileName, $image);

			$url = Storage::disk('images')->url('frontend/taskImages/' . $fileName);
		}
		else {
			$url = $base64;
		}
		array_push($urls, $url);
	}

	return $urls;
}
function insertUrlToImages($content, $urlArr) {
	$replacedContent = preg_replace_array('/URLPATH/', $urlArr, $content);
	return $replacedContent;
}

function getIdArrayFromQuery($query) {
	$arr = [];
	foreach ($query as $item) {
		array_push($arr, $item->id);
	}
	return $arr;
}