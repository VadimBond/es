<?php

namespace App\Helpers;
use App\Models\History\History;

class HistoryHelper {

	public static function add($data) {
		
		$model = new History();

		$model->user_id = $data['user_id'];
		$model->icon = $data['icon'];
		$model->class = $data['class'];
        $model->text = $data['text'];

		$model->save();
	}

}