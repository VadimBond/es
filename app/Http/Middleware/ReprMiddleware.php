<?php

namespace App\Http\Middleware;

use Closure;

class ReprMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = Auth::user()->id;
        $userRoleId = DB::table('role_user')->where('user_id', $userId)->value('role_id');
        if ($userRoleId == 3) return $next($request);
        else return redirect('/');

    }
}
