<?php

namespace App\Http\Controllers;

use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use DB;
use Mockery\Exception;
use PDF;
use App\Helpers\HistoryHelper;



use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\File;



use Illuminate\Support\Facades\Storage;

class GeneratePdfController extends Controller
{
	static function getIdFromQuery($query) {
		$arr = [];
		if(is_array($query)) {
			foreach ($query as $item) {
				array_push($arr, $item->id);
			}
		}
		return $arr;
	}
	static function resetUnusedElements ($arr, $elementsToExclude) {
		return array_filter($arr, function($element) use ($elementsToExclude) {
			return !in_array($element, $elementsToExclude);
		});
	}

    function generateBlockPdf(Request $req) {
        $blockId=$req->blockId;
        $block = DB::table('blocks')->where('id', $blockId)->get();
        $questions = [];
        $takeQuestions = DB::table('questions')
            ->where('id_block', $blockId)
            ->get(['id', 'id_block', 'text_question'])
            ->toArray();

        if (count($takeQuestions) > 0) {
            $questions{$blockId} = $takeQuestions;
        }

        $queryIdQuestions = DB::table('questions')
            ->where('id_block', $blockId)
            ->get(['id'])
            ->toArray();


        $idQuestions = self::getIdFromQuery($queryIdQuestions);

        $answers = DB::table('answers')
            ->whereIn('id_question', $idQuestions)
            ->get(['id_question', 'text_answer', 'answer_right'])
            ->groupBy('id_question')
            ->toArray();

        foreach ($questions as $questionsBlock) {
            foreach ($questionsBlock as $question) {
                $idQuestion = $question->id;
                $otherAnswers = array();
                foreach ($answers[$idQuestion] as $ans) {
                    array_push($otherAnswers, $ans);
                }
                $question->answers = $otherAnswers;
            }
        }

        $english_answers = DB::table('english_answers')->get();
        $english_config = DB::table('english_config')->where('block_id', $blockId)->get();

        $english_questions = DB::table('english_questions')->orderByRaw("CASE WHEN right_answer = 1 THEN 0 ELSE right_answer END")->orderBy('position')->get();
        $english_text = DB::table('english_text')->get();

        if(sizeof($queryIdQuestions) == 0 && sizeof($english_config) == 0) return "error";

        $connectionQuery = DB::table('english_connections')->where('block_id', $blockId)->get(['id'])->toArray();

        $connectionIds = self::getIdFromQuery($connectionQuery);

        $connections = DB::table('english_connections')->where('block_id', $blockId)->get();

        $questionsConnection = DB::table('english_questions')->whereIn('english_connection_id', $connectionIds)->where('type','list')->get()->groupBy('english_connection_id')->toArray();
        $answersConnection = DB::table('english_questions')->whereIn('english_connection_id', $connectionIds)->where('type','answerList')->get()->groupBy('english_connection_id')->toArray();

        foreach ($connections as $connection) {
            $connectionId = $connection->id;

            if(isset($questionsConnection[$connectionId])) {
                $currQuestions = $questionsConnection[$connectionId];
                $connection->questions = $currQuestions;
            }
            else {
                $connection->questions = [];
            }
            if(isset($answersConnection[$connectionId])) {
                $currAnswers = $answersConnection[$connectionId];
                $connection->answers = $currAnswers;
            }
            else {
                $connection->answers = [];
            }
        }

        $data = [ 'english_answers' => $english_answers,
            'english_config' => $english_config,
            'english_questions' => $english_questions,
            'english_text' => $english_text,
            'id' => $blockId,
            'questions' => $questions,
            'block' => $block[0],
            'connections' => $connections,
        ];

        $dirPath= storage_path(sprintf('app/public/pdfBlocks/'));
        if(!File::exists($dirPath)) {
            File::makeDirectory($dirPath, $mode = 0777, true, true);
        }

        $pathToFile = sprintf('%sblock.pdf',$dirPath);

        if($req->type == 1) {
            PDF::loadView('pdf.ukrainianBlockView', compact('data'))->save($pathToFile);
        }
        else if ($req->type == 2) {
            PDF::loadView('pdf.englishBlockView', compact('data'))->save($pathToFile);
        }
        else
            PDF::loadView('pdf.blockView', compact('data'))->save($pathToFile);

        File::chmod($pathToFile, $mode = 0777);
        chown($pathToFile, 'www-data');
        chgrp($pathToFile, 'www-data');

        $currBlankPath =  Storage::url(sprintf('pdfBlocks/block.pdf' ));
        return response()->json(['url' => $currBlankPath, 'id' => $pathToFile]);
    }


    function generateList(Request $req) {
        $id_speciality = $req->specialityId;
        $type = DB::table('specialities')->where('id', $id_speciality)->value('type');
        $blocks = DB::table('blocks')->where('id_speciality', $id_speciality)->get(['id', 'block_name', 'type', 'url_image', 'task_title'])->toArray();

        $blocksForLang =  DB::table('blocks')->where('id_speciality', $id_speciality)->get();

        $queryIdBlocks = DB::table('blocks')->where('id_speciality', $id_speciality)->get(['id'])->toArray();
        $idBlocks = GeneratePdfController::getIdFromQuery($queryIdBlocks);

        $questions = [];
        foreach($blocks as $block) {
            $takeQuestions = DB::table('questions')
                ->where('id_block', $block->id)
                ->get(['id', 'id_block', 'text_question'])
                ->toArray();
            if(count($takeQuestions) > 0) {
                $questions{$block->id} = $takeQuestions;
            }
        }

        $queryIdQuestions = DB::table('questions')
            ->whereIn('id_block', $idBlocks)
            ->get(['id'])
            ->toArray();

        $idQuestions = GeneratePdfController::getIdFromQuery($queryIdQuestions);

        $answers = DB::table('answers')
            ->whereIn('id_question', $idQuestions)
            ->get(['id_question', 'text_answer', 'answer_right'])
            ->groupBy('id_question')
            ->toArray();

        foreach ($questions as $questionsBlock) {
            foreach ($questionsBlock as $question) {
                $idQuestion = $question->id;

                $currAnswers = $answers[$idQuestion];
                $question->answers = $currAnswers;
            }
        }

        foreach ($blocks as $indexBlock => $block) {
            $blockId = $block->id;
            if(array_key_exists($blockId, $questions)) {
                $objQuestions = (object)$questions;

                $nmbAnswers = count($objQuestions->{$blockId}[0]->answers) + 1;
                $block->tableNmb = $nmbAnswers;
            }
            else {
                unset($blocks[$indexBlock]);
            }
        }

		$commissioners = DB::table('commissioners')->where('speciality_id', $id_speciality)->get(['commissioner_name', 'commissioner_position'])->toArray();


            if ($type == 2) {

                $english_answers = DB::table('english_answers')->get();
                $english_config = DB::table('english_config')->get();
                $english_questions = DB::table('english_questions')->orderByRaw("CASE WHEN right_answer = 1 THEN 0 ELSE right_answer END")->orderBy('position')->get();


            foreach ($blocksForLang as $block) {

                $english_text = DB::table('english_text')->where('block_id', $block->id)->get();
                $questions = DB::table('english_questions')->where('block_id', $block->id)->get();

                foreach ($english_text as $text) {
                    $english_questions = DB::table('english_questions')->where('id_english_question', $text->id)->get()->toArray();
                    if(sizeof($english_questions) > 0)
                    $text->english_questions = $english_questions;
                    else    $text->english_questions = [];
                    foreach($english_questions as $question){
                        $answers = DB::table('english_answers')
                            ->where('id_english_question', $question->id)
                            ->get();
                        if(sizeof($answers) > 0) $question->answers = $answers;
                        else $question->answers = [];
                    }
                }
                if (sizeof($english_text) > 0) {
                    $block->english_text = $english_text;
                }
                else  $block->english_text = [];

                $english_questions = DB::table('english_questions')->where('block_id', $block->id)->get()->toArray();

                $block->english_questions = $english_questions;

                $queryIdQuestions = DB::table('english_questions')
                    ->where('block_id', $block->id)
                    ->get(['id'])
                    ->toArray();

                $idQuestions = GeneratePdfController::getIdFromQuery($queryIdQuestions);

                $answers = DB::table('english_answers')
                    ->whereIn('id_english_question', $idQuestions)
                    ->get()
                    ->groupBy('id_english_question')
                    ->toArray();

                //add answers to questions
                foreach ($questions as $question) {
                    $idQuestion = $question->id;

                    $currAnswers = $answers[$idQuestion];
                    $question->answers = $currAnswers;
                }

                $block->questions = $questions;

                $connections = DB::table('english_connections')->whereIn('block_id', $idBlocks)->get(['id'])->toArray();

                foreach ($connections as $connection) {
                    $connection->block_id = $block->id;
                    $questionsConnection = DB::table('english_questions')->where('english_connection_id', $connection->id)->where('type', 'list')->get()->toArray();
                    $answersConnection = DB::table('english_questions')->where('english_connection_id', $connection->id)->where('type', 'answerList')->get()->toArray();

                    if (sizeof($questionsConnection) > 0) {
                        $connection->questions = $questionsConnection;
                    } else {
                        $connection->questions = [];
                    }
                    if (sizeof($answersConnection) > 0) {
                        $connection->answers = $answersConnection;
                    } else {
                        $connection->answers = [];
                    }
                }


                if (sizeof($connections) > 0) {
                    $block->connections = $connections;
                } else $block->connections = [];
            }


            $letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O'];


            $data = [
                'english_blocks' => $blocksForLang,
                'questions' => $questions,
                'commissioners' => $commissioners,
                'english_answers' => $english_answers,
                'english_config' => $english_config,
                'english_questions' => $english_questions,
                'letters' => $letters,

            ];
        }
     else
         $data = [
             'blocks' => $blocks,
             'questions' => $questions,
             'commissioners' => $commissioners,
             'block' => $blocksForLang[0],
         ];


        $dirPath = storage_path('app/public/generatedPdf/list.pdf');
        if($type == 1) {
            PDF::loadView('pdf.ukrainianList', compact('data'))->save($dirPath);
        }
        else if ($type == 2) {
            PDF::loadView('pdf.englishList', compact('data'))->save($dirPath);
        }
        else
            PDF::loadView('pdf.list', compact('data'))->save($dirPath);

        File::chmod($dirPath, $mode=0777);

        $baseStorageUrl =  Storage::url('generatedPdf/list.pdf');

        return response()->json(['url' => $baseStorageUrl]);
    }

    function generate(Request $req) {
        $id_speciality = $req->specialityId;
        $generateVariants = $req->variants;

        function generateVariant($id_speciality, $usedQuestions) {
            $blankHeaderData = DB::table('specialities')->where('id', $id_speciality)->first();
            $speciality_type = $blankHeaderData->type;

	        $unserializedQuestions = $speciality_type == 2 ?
		        ['texts' => [], 'questions' => [], 'connections' => []] :
		        [];


        	if($usedQuestions) {
		        if(!is_array(unserialize($usedQuestions->questions_id))) {
                    array_push($unserializedQuestions, unserialize($usedQuestions->questions_id));
                }
		        else {
			        $unserializedQuestions = unserialize($usedQuestions->questions_id);
                }
	        }

            $blankTitle = $blankHeaderData->blank_title;
            $blankPurpose = $blankHeaderData->blank_purpose;
            $blankNumber = $blankHeaderData->number;

            $commissioners = DB::table('commissioners')->where('speciality_id', $id_speciality)->get(['commissioner_name', 'commissioner_position'])->toArray();

            if($speciality_type == 1) {
                $block = DB::table('blocks')
	                ->where('id_speciality', $id_speciality)
	                ->get(['id', 'block_name']);

                $availableQuestions = DB::table('questions')
	                ->where('id_block', $block[0]->id)
	                ->whereNotIn('id', $unserializedQuestions)
	                ->count();

                $usedQuestionsId = $unserializedQuestions;
				if($availableQuestions === 0) {
					$usedQuestionsId = [];
				}

                $questionId = DB::table('questions')
	                ->where('id_block', $block[0]->id)
	                ->whereNotIn('id', $usedQuestionsId)
	                ->inRandomOrder()
	                ->value('id');

                $question_text = DB::table('questions')->where('id', $questionId)->value('text_question');

                $answer = DB::table('answers')->where('id_question', $questionId)->value('text_answer');

                $data = [
                    'type' => $speciality_type,
                    'title' => $blankTitle,
                    'purpose' => $blankPurpose,
                    'number' => $blankNumber,
                    'question_text' => $question_text,
                    'questionsId' => $questionId,
                    'answer_text' => $answer,
                    'block_name' => $block[0]->block_name,
                    'commissioners' => $commissioners,
                ];
            }
            else if($speciality_type == 2) {
	            $usedElementsId = $unserializedQuestions;

                $english_answers = DB::table('english_answers')->get();
                $english_config = DB::table('english_config')->get();
                $english_questions = DB::table('english_questions')->orderByRaw("CASE WHEN right_answer = 1 THEN 0 ELSE right_answer END")->orderBy('position')->get();
                $blocks = DB::table('blocks')->where('id_speciality', $id_speciality)->orderBy('block_name')->where('count_chosed_questions','!=',0)->get();

                // define variables which are used for inserting in db as using elements;
                $texts_id = [];
                $questions_id = [];
                $connections_id = [];

	            $usedTextsId = $usedElementsId['texts'];
	            $usedQuestionsId = $usedElementsId['questions'];
	            $usedConnectionsId = $usedElementsId['connections'];


                foreach ($blocks as $block) {
                	$allTexts = DB::table('english_text')
		                ->where('block_id',$block->id)
		                ->get(['id'])
		                ->toArray();

                	$allTextsId = GeneratePdfController::getIdFromQuery($allTexts);
	                $english_text = [];


                	if(count($allTexts) > 0) {
		                $availableTextsId = DB::table('english_text')
			                ->whereNotIn('id', $usedTextsId)
			                ->where('block_id',$block->id)
			                ->get(['id'])
			                ->toArray();

		                $availableTexts = count($availableTextsId);

		                if($availableTexts < 1) {
			                // remove all occurrences from usedTextsId
			                $usedElementsId['texts'] = GeneratePdfController::resetUnusedElements($usedTextsId, $allTextsId);
		                }

		                $english_text = DB::table('english_text')
			                ->where('block_id',$block->id)
			                ->whereNotIn('id', $usedElementsId['texts'])
			                ->inRandomOrder()
			                ->limit(1)
			                ->get()
			                ->toArray();

		                $texts_id = array_merge($texts_id, GeneratePdfController::getIdFromQuery($english_text));

		                if(sizeof($english_text) > 0) {
			                $english_questions = DB::table('english_questions')->where('id_english_question', $english_text[0]->id)->get()->toArray();
		                }

	                }

	                $allQuestions = DB::table('english_questions')
		                ->where('block_id', $block->id)
		                ->get(['id'])
		                ->toArray();

	                $allQuestionsId = GeneratePdfController::getIdFromQuery($allQuestions);

	                $questions = [];
	                if(count($allQuestions) > 0) {
		                $availableQuestions = DB::table('english_questions')
			                ->whereNotIn('id', $usedQuestionsId)
			                ->where('block_id', $block->id)
			                ->inRandomOrder()
			                ->get()
			                ->toArray();

		                $nmbAvailableQuestions = count($availableQuestions);

		                $nmbQuestionsToTake = $block->count_chosed_questions;

		                if($nmbAvailableQuestions < $nmbQuestionsToTake) {
			                //clean && add few elements which are remaining
			                $usedElementsId['questions'] = GeneratePdfController::resetUnusedElements($usedQuestionsId, $allQuestionsId);
			                if($nmbAvailableQuestions > 0) {
			                	$availableQuestionsId = GeneratePdfController::getIdFromQuery($availableQuestions);
				                $questions = $availableQuestions;
				                $nmbQuestionsToTake = $nmbQuestionsToTake - $nmbAvailableQuestions;
				                $usedElementsId['questions'] = array_merge($usedElementsId['questions'], $availableQuestionsId);
			                }

			                $restQuestions = DB::table('english_questions')
				                ->where('block_id', $block->id)
				                ->whereNotIn('id', $usedElementsId['questions'])
				                ->inRandomOrder()
				                ->take($nmbQuestionsToTake)
				                ->get()
				                ->toArray();

			                $questions = array_merge($questions, $restQuestions);
		                }
		                else {
			                $questions = DB::table('english_questions')
				                ->whereNotIn('id', $usedQuestionsId)
				                ->where('block_id', $block->id)
				                ->inRandomOrder()
				                ->take($block->count_chosed_questions)
				                ->get()
				                ->toArray();
		                }
	                }

                    $questions_id = array_merge($questions_id, GeneratePdfController::getIdFromQuery($questions));

                    $eng_questions_numb = DB::table('english_questions')->where('block_id',$block->id)->count();
                    $extra = DB::table('english_config')->where('block_id',$block->id)->get()->toArray();

                    $currentText = DB::table('english_config')->where('block_id',$block->id)->value('text');
                    $currentList = DB::table('english_config')->where('block_id',$block->id)->value('answer_list');

                    if($currentText == '1' && $currentList == '0') {
                        $nmbAnswers = 5;
                    }
                    else {
                        $nmbAnswers = $block->countAnswers + 1 + $eng_questions_numb + $extra[0]->extra_answers;
                    }

                    $block->tableNmb = $nmbAnswers;
                    $block->english_text = $english_text;
                    $block->english_questions = $english_questions;

	                if(count($questions) > 0) {
		                $answers = DB::table('english_answers')
			                ->whereIn('id_english_question', $questions_id)
			                ->get()
			                ->groupBy('id_english_question')
			                ->toArray();

		                //add answers to questions
		                foreach ($questions as $question) {
			                $idQuestion = $question->id;

			                $currAnswers = $answers[$idQuestion];
			                $question->answers = $currAnswers;
		                }
	                }
	                $block->questions = $questions;

	                $allConnections = DB::table('english_connections')
		                ->where('block_id',$block->id)
		                ->get(['id'])
		                ->toArray();
					$allConnectionsId = GeneratePdfController::getIdFromQuery($allConnections);

                    if(count($allConnections) > 0) {
	                    $availableConnections = DB::table('english_connections')
		                    ->whereNotIn('id', $usedConnectionsId)
		                    ->where('block_id',$block->id)
		                    ->get(['id'])
		                    ->toArray();

						if(count($availableConnections) < 1) {
							$usedElementsId['connections'] = GeneratePdfController::resetUnusedElements($usedConnectionsId, $allConnectionsId);
						}

	                    $connection = DB::table('english_connections')
		                    ->whereNotIn('id', $usedElementsId['connections'])
		                    ->where('block_id',$block->id)
		                    ->inRandomOrder()
		                    ->limit(1)
		                    ->get(['id'])
		                    ->toArray();

	                    $connections_id = array_merge($questions_id, GeneratePdfController::getIdFromQuery($connection));

                        $connectionId = $connection[0]->id;

                        $questionsConnection = DB::table('english_questions')
	                        ->where('english_connection_id', $connectionId)
	                        ->where('type', 'list')
	                        ->get()
	                        ->toArray();
                        $answersConnection = DB::table('english_questions')
	                        ->where('english_connection_id', $connectionId)
	                        ->where('type', 'answerList')
	                        ->get()
	                        ->toArray();

                        if (sizeof($questionsConnection) > 0) {
                            $connection[0]->questions = $questionsConnection;
                        }
                        else {
                            $connection[0]->questions = [];
                        }

                        if (sizeof($answersConnection) > 0) {
                            $connection[0]->answers = $answersConnection;
                        }
                        else {
                            $connection[0]->answers = [];
                        }

                        $block->connection = $connection;
                    }
                    else {
                    	$block->connection = [];
                    }
                }
                $letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O'];

                $data = [
                    'blocks' => $blocks,
                    'type' => $speciality_type,
                    'title' => $blankTitle,
                    'purpose' => $blankPurpose,
                    'number' => $blankNumber,
                    'english_answers' => $english_answers,
                    'english_config' => $english_config,
                    'english_questions' => $english_questions,
                    'commissioners' => $commissioners,
	                'letters' => $letters,
	                'usedElementsId' => $usedElementsId,
	                'textsId' => $texts_id,
	                'questionsId' => $questions_id,
	                'connectionsId' => $connections_id
                ];
            }
            else {
                $blocks = DB::table('blocks')
	                ->where('id_speciality', $id_speciality)
	                ->get(['id', 'block_name', 'count_chosed_questions', 'type', 'blank_text', 'url_image', 'task_title'])
	                ->toArray();

	            $availableQuestions = DB::table('questions')
		            ->whereNotIn('id', $unserializedQuestions)
		            ->whereIn('id_block', GeneratePdfController::getIdFromQuery($blocks))
		            ->get(['id_block'])
		            ->groupBy('id_block')
		            ->toArray();

	            $availableQuestions = array_map(function ($arr) {return count($arr);}, $availableQuestions);

                $questions = [];
                $idQuestions = [];
                foreach ($blocks as $block) {
                	/*
                	    if not enough questions we get remaining questions
                	    after we clean unserializedQuestions  and put this remaining questions
                        and after we supplement array number of necessary questions.
                	*/

	                $nmbChoosedQuestions = $block->count_chosed_questions;

                	if(isset($availableQuestions[$block->id])) {
		                $nmbAvailableQuestions = $availableQuestions[$block->id];

		                if ($nmbChoosedQuestions > $nmbAvailableQuestions) {
			                // take remaining questions
			                $takeQuestions = DB::table('questions')
				                ->where('id_block', $block->id)
				                ->whereNotIn('id', $unserializedQuestions)
				                ->inRandomOrder()
				                ->get(['id', 'id_block', 'text_question'])
				                ->toArray();

			                $questions{$block->id} = $takeQuestions;
			                $idCollection = GeneratePdfController::getIdFromQuery($takeQuestions);
			                $idQuestions = array_merge($idQuestions, $idCollection);

			                //clean history
			                DB::table('generated_questions')->where('speciality_id', $id_speciality)->delete();


			                $questionsToTake = $nmbChoosedQuestions - $nmbAvailableQuestions;

			                $takeQuestions = DB::table('questions')
				                ->where('id_block', $block->id)
				                ->whereNotIn('id', $idCollection)
				                ->inRandomOrder()
				                ->take($questionsToTake)
				                ->get(['id', 'id_block', 'text_question'])
				                ->toArray();

			                $questions{$block->id} = array_merge($questions{$block->id}, $takeQuestions);
			                $idCollection = GeneratePdfController::getIdFromQuery($takeQuestions);
			                $idQuestions = array_merge($idQuestions, $idCollection);
		                }
		                else {
		                	$unserializedData = $unserializedQuestions;
			                if ($nmbChoosedQuestions == $nmbAvailableQuestions) {
				                //clean history
				                DB::table('generated_questions')->where('speciality_id', $id_speciality)->delete();

				                $unserializedData = [];
			                }

			                $takeQuestions = DB::table('questions')
				                ->where('id_block', $block->id)
				                ->whereNotIn('id', $unserializedData)
				                ->inRandomOrder()
				                ->take($nmbChoosedQuestions)
				                ->get(['id', 'id_block', 'text_question'])
				                ->toArray();

			                if (count($takeQuestions) > 0) {
				                $questions{$block->id} = $takeQuestions;

				                $idQuestions = array_merge($idQuestions, GeneratePdfController::getIdFromQuery($takeQuestions));
			                }
		                }
	                }
                	elseif(!isset($availableQuestions[$block->id]) && $nmbChoosedQuestions > 0) {
		                //clean history
		                DB::table('generated_questions')->where('speciality_id', $id_speciality)->delete();


		                $takeQuestions = DB::table('questions')
			                ->where('id_block', $block->id)
			                ->inRandomOrder()
			                ->take($nmbChoosedQuestions)
			                ->get(['id', 'id_block', 'text_question'])
			                ->toArray();

		                if (count($takeQuestions) > 0) {
			                $questions{$block->id} = $takeQuestions;

			                $idQuestions = array_merge($idQuestions, GeneratePdfController::getIdFromQuery($takeQuestions));
		                }
	                }
                }

                $answers = DB::table('answers')
                    ->whereIn('id_question', $idQuestions)
                    ->inRandomOrder()
                    ->get(['id_question', 'text_answer', 'answer_right'])
                    ->groupBy('id_question')
                    ->toArray();

                //add answers to questions
                foreach ($questions as $questionsBlock) {
                    foreach ($questionsBlock as $question) {
                        $idQuestion = $question->id;

                        $currAnswers = $answers[$idQuestion];
                        $question->answers = $currAnswers;
                    }
                }

                foreach ($blocks as $indexBlock => $block) {
                    $blockId = $block->id;
                    if (array_key_exists($blockId, $questions)) {
                        $objQuestions = (object)$questions;

                        $nmbAnswers = count($objQuestions->{$blockId}[0]->answers) + 1;
                        $block->tableNmb = $nmbAnswers;
                    } else {
                        unset($blocks[$indexBlock]);
                    }
                }

                $letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O'];
                $data = [
                    'type' => $speciality_type,
                    'title' => $blankTitle,
                    'purpose' => $blankPurpose,
                    'number' => $blankNumber,
                    'blocks' => $blocks,
	                'questionsId' => $idQuestions,
                    'questions' => $questions,
                    'letters' => $letters,
                    'commissioners' => $commissioners,
                ];

            }

            return $data;
        }

        $maxVariant = DB::table('blanks')->where('id_speciality', $id_speciality)->max('variant');
        $maxVariant = !is_null($maxVariant) ? $maxVariant : 0;

        function createViewPdf($path, $data, $speciality_type) {
            $pathToFile = sprintf('%s/view.pdf', $path);

            if($speciality_type == 1) {
	            PDF::loadView('pdf.ukrainianView', compact('data'))->save($pathToFile);
            }
            else if($speciality_type == 2) {
                PDF::loadView('pdf.englishView', compact('data'))->save($pathToFile);
            }
            else{
	            PDF::loadView('pdf.view', compact('data'))->save($pathToFile);
            }
            File::chmod($pathToFile, $mode=0777);
        }
        function createTaskPdf($path, $data,$speciality_type) {
            $pathToFile = sprintf('%s/task.pdf', $path);

            if($speciality_type == 1) {
	            PDF::loadView('pdf.ukrainianTask', compact('data'))->save($pathToFile);
            }
            else  if($speciality_type == 2) {
                PDF::loadView('pdf.englishTask', compact('data'))->save($pathToFile);
            }
            else {
	            PDF::loadView('pdf.task', compact('data'))->save($pathToFile);
            }
            File::chmod($pathToFile, $mode=0777);
        }
        function createAnswerPdf($path, $data, $speciality_type) {
            $pathToFile = sprintf('%s/answer.pdf', $path);

            if($speciality_type == 0)
                PDF::loadView('pdf.answer', compact('data'))->save($pathToFile);
            else if($speciality_type == 2)
                PDF::loadView('pdf.englishAnswer', compact('data'))->save($pathToFile);
            else  PDF::loadView('pdf.ukrainianAnswer', compact('data'))->save($pathToFile);
                File::chmod($pathToFile, $mode = 0777);
        }

        $currBlankPath = '';
        for($countIndex = 1; $countIndex <= $generateVariants; $countIndex += 1) {
            $currVariant = $maxVariant + $countIndex;

	        $usedQuestions = DB::table('generated_questions')->where('speciality_id', $id_speciality)->first();

            $data = generateVariant($id_speciality, $usedQuestions);

	        $dirPath = storage_path(sprintf('app/public/generatedPdf/%s/%s/', $id_speciality, $currVariant));

	        if(!File::exists($dirPath)) {
		        File::makeDirectory($dirPath, $mode = 0777, true, true);
	        }
            $blankHeaderData = DB::table('specialities')->where('id', $id_speciality)->first();
            $speciality_type = $blankHeaderData->type;

			if($speciality_type != 2) {
				//check for some changes
				$usedQuestions = DB::table('generated_questions')->where('speciality_id', $id_speciality)->first();

				if($usedQuestions) {
					$variants = [];

					if(!is_array(unserialize($usedQuestions->questions_id))){
						array_push($variants, unserialize($usedQuestions->questions_id));
					}
					else  {
						$variants = unserialize($usedQuestions->questions_id);
					}

					if(is_array($data['questionsId'])) {
						$variantsWithNew = array_merge($variants, $data['questionsId']);
					}
					else {
						$variantsWithNew = $variants;
						$variantsWithNew = array_push($variantsWithNew, $data['questionsId']);
					}

					DB::table('generated_questions')->where('speciality_id', $id_speciality)
						->update([
							'questions_id' => serialize($variantsWithNew)
						]);
				}
				else {
					DB::table('generated_questions')->insert(
						[
							'questions_id' => serialize($data['questionsId']),
							'speciality_id' => $id_speciality
						]
					);
				}
			}
			else {
				$variants = $data['usedElementsId'];

				if(count($data['textsId']) > 0) {
					$variants['texts'] = array_merge($variants['texts'], $data['textsId']);
				}
				if(count($data['questionsId']) > 0) {
					$variants['questions'] = array_merge($variants['questions'], $data['questionsId']);
				}
				if(count($data['connectionsId']) > 0) {
					$variants['connections'] = array_merge($variants['connections'], $data['connectionsId']);
				}

				if($usedQuestions) {
					DB::table('generated_questions')->where('speciality_id', $id_speciality)
						->update([
							'questions_id' => serialize($variants)
						]);
				}
				else {
					DB::table('generated_questions')->insert(
						[
							'questions_id' => serialize($variants),
							'speciality_id' => $id_speciality
						]
					);
				}
			}

            // generate unique number of document witch consist h,m,s,ms with no separator
            $cypter = Carbon::now('Europe/Kiev')->format('Hisu');

            $data['cypter'] = $cypter;
            $data['variant'] = $currVariant;

            $spec = DB::table('specialities')->where('id', $id_speciality)->first();
            $speciality_type = $spec->type;

            createViewPdf($dirPath, $data, $speciality_type);
            createTaskPdf($dirPath, $data, $speciality_type);
            createAnswerPdf($dirPath, $data, $speciality_type);

            $baseStorageUrl =  Storage::url(sprintf('generatedPdf/%s/%s', $id_speciality, $currVariant));

            DB::table('blanks')->insert([
                'variant' => $currVariant,
                'id_speciality' => $id_speciality,
                'cypter' => $cypter,
                'url_blank' => $baseStorageUrl,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
            // history
            $blankName = DB::table('specialities')->where('id', $id_speciality)->value('name_speciality');
            HistoryHelper::add([
                'user_id' => Auth::user()->id,
                'icon' => 'plus',
                'class' => 'bg-green',
                'text' => 'trans("history.backend.blanks.generate") '.$currVariant. ' зі спеціальності <strong>'.$blankName.'</strong>'
            ]);

            $currBlankPath = sprintf('%s/view.pdf', $baseStorageUrl);
        }

        if(intval($generateVariants) > 1) {
            return response($generateVariants);
        }
        else {
            return response()->json(['url' => $currBlankPath]);
        }
    }

    function resetQuestionsHistory(Request $req) {
		$idSpeciality = $req->idSpeciality;

		try {
			DB::table('generated_questions')->where('speciality_id', $idSpeciality)->delete();
		}
		catch (\Exception $err) {
			return response($err, 500);
		}

		return response('Ok');
    }
    function getBlanks(Request $req) {
        $idSpeciality = $req->idSpeciality;

        $variants = DB::table('blanks')->where('id_speciality', $idSpeciality)->get(['id', 'variant', 'url_blank']);

        return response($variants);
    }

    function deleteBlank(Request $req) {
        $idBlank = $req->idBlank;
        $blankName = DB::table('specialities')->join('blanks','specialities.id', '=', 'blanks.id_speciality' )->where('blanks.id', $idBlank)->value('name_speciality');
        $dataBlank = DB::table('blanks')->where('id', $idBlank)->get(['id_speciality', 'variant']);

        $dirToDelete = storage_path(sprintf('/app/public/generatedPdf/%s/%s', $dataBlank[0]->id_speciality, $dataBlank[0]->variant));

        File::deleteDirectory($dirToDelete);

        DB::table('blanks')->where('id', $idBlank)->delete();


        HistoryHelper::add([
            'user_id' => Auth::user()->id,
            'icon' => 'trash',
            'class' => 'bg-maroon',
            'text' => 'trans("history.backend.blanks.delete_generated") '.$dataBlank[0]->variant. ' зі спеціальності <strong>'.$blankName.'</strong>'
        ]);

        return response(200);
    }

    function deleteMe(Request $req) {
        function getIdArrayFromQuery($query) {
            $arr = [];
            foreach ($query as $item) {
                array_push($arr, $item->id);
            }
            return $arr;
        }

        $id_speciality = 11;

        $blocks = DB::table('blocks')->where('id_speciality', $id_speciality)->get(['id', 'block_name']);

        $queryIdBlocks = DB::table('blocks')->where('id_speciality', $id_speciality)->get(['id'])->toArray();
        $idBlocks = getIdArrayFromQuery($queryIdBlocks);

        $questions = DB::table('questions')
            ->whereIn('id_block', $idBlocks)
            ->inRandomOrder()
            ->get(['id', 'id_block', 'text_question'])
            ->groupBy('id_block')
            ->toArray();

        $queryIdQuestions = DB::table('questions')
            ->whereIn('id_block', $idBlocks)
            ->get(['id'])
            ->toArray();

        $idQuestions = getIdArrayFromQuery($queryIdQuestions);

        $answers = DB::table('answers')
            ->whereIn('id_question', $idQuestions)
            ->inRandomOrder()
            ->get(['id_question', 'text_answer', 'answer_right'])
            ->groupBy('id_question')
            ->toArray();

        //add answers to questions and convert to Array type

        foreach ($questions as $blockId => $questionsBlock) {
            foreach ($questionsBlock as $questionId => $question) {
                $idQuestion = $question->id;

                $currAnswers = $answers[$idQuestion];

                $question->answers = $currAnswers;
            }
        }

        foreach ($blocks as $block) {

            $blockId = $block->id;

            $objQuestions = (object)$questions;

            $nmbAnswers = count($objQuestions->{$blockId}[0]->answers) + 1;

            $block->tableNmb = $nmbAnswers;
        }

        $letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'];

        $data = [
            'title' => 'tit;e',
            'purpose' => 'sdgfijfdg',
            'blocks' => $blocks,
            'questions' => $questions,
            'letters' => $letters,
        ];

        return view('pdf.view', compact('data'));
    }
}