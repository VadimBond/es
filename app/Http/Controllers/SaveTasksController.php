<?php

namespace App\Http\Controllers;


use function foo\func;
use Illuminate\Http\Request;
use DB;
use App\Helpers\HistoryHelper;
use Carbon\Carbon;
use App\blocks;
use App\questions;

use Illuminate\Support\Facades\Storage;
//use phpDocumentor\Reflection\Types\Null;
use Auth;



class SaveTasksController extends Controller
{
    static function getIdFromQuery($query) {
        $arr = [];
        foreach ($query as $item) {
            array_push($arr, $item->id);
        }
        return $arr;
    }

    function deleteComm(Request $req){
        $id = $req->id;
        DB::table('commissioners')->where('id', $id)->delete();

    }
    function cloneSpeciality(Request $req) {

        $id = $req->spec_id;
        $specialisation = DB::table('specialities')->where("id", $id)->get(['name_speciality','number','id_cafedres','blank_title','blank_purpose','type'])[0];
        $new_specialisation = array('name_speciality' => $specialisation->name_speciality . '(копія)', 'number' => $specialisation->number, 'id_cafedres' => $specialisation->id_cafedres,
            'blank_title' => $specialisation->blank_title, 'blank_purpose' => $specialisation->blank_purpose, 'status' => 'in_development', 
            'type' => $specialisation->type,'created_at' => Carbon::now(), 'updated_at' => Carbon::now());
        $new_id = DB::table('specialities')->insertGetId($new_specialisation);

        $blocks = DB::table('blocks')->where("id_speciality", $id)->get(['id','block_name','weight_question','count_q_this_block','countAnswers','count_chosed_questions','type','blank_text','url_image','task_title']);
        foreach ($blocks as $block) {

            $new_block = array('block_name' => $block->block_name, 'weight_question' => $block->weight_question, 'count_q_this_block' => $block->count_q_this_block, 'id_speciality'=> $new_id,  'countAnswers' => $block->countAnswers, 'count_chosed_questions' => $block->count_chosed_questions, 'type' => $block->type, 'blank_text' => $block->blank_text, 'url_image' => $block->url_image, 'task_title' => $block->task_title, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now());
            $id_block = DB::table('blocks')->insertGetId($new_block);

            if($specialisation->type != 2) {
                $questions = DB::table('questions')->where("id_block", $block->id)->get(['id', 'text_question', 'image_width']);
                foreach ($questions as $question) {

                    $new_question = array('text_question' => $question->text_question, 'image_width' => $question->image_width, 'id_block' => $id_block, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now());
                    $question_id = DB::table('questions')->insertGetId($new_question);

                    $newText = DB::table('questions')->where('id', $question_id)->value('text_question');
                    $match = [];
                    if (preg_match("/<img src=\"\/img\/frontend\/taskImages\/([a-zA-Z])+\-[0-9]+\-[0-9]+/", $newText, $match)) {
                        foreach ($match as $m) {
                            $newText = str_replace($m, str_replace("-" . $question->id . "-", "-" . $question_id . "-", $m), $newText);
                            $file_old = substr($m, 14);
                            $file_new = substr(str_replace("-" . $question->id . "-", "-" . $question_id . "-", $m), 14);

                            if (Storage::disk('images')->exists($file_old)) {
                                Storage::disk('images')->put($file_new, Storage::disk('images')->get($file_old));
                            }
                        }
                        DB::table('questions')->where('id', $question_id)->update(['text_question' => $newText]);
                    }

                    $answers = DB::table('answers')->where("id_question", $question->id)->get(['id', 'text_answer', 'answer_right']);
                    foreach ($answers as $answer) {
                        $new_answer = array('text_answer' => $answer->text_answer, 'answer_right' => $answer->answer_right, 'id_question' => $question_id, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now());
                        $answer_id = DB::table('answers')->insertGetId($new_answer);

                        $newTextAns = DB::table('answers')->where('id', $answer_id)->value('text_answer');
                        $matchAns = [];
                        if (preg_match("/<img src=\"\/img\/frontend\/taskImages\/([a-zA-Z])+\-[0-9]+\-[0-9]+/", $newTextAns, $matchAns)) {
                            foreach ($matchAns as $mAns) {
                                $newTextAns = str_replace($mAns, str_replace("-" . $answer->id . "-", "-" . $answer_id . "-", $mAns), $newTextAns);
                                $file_oldAns = substr($mAns, 14);
                                $file_newAns = substr(str_replace("-" . $answer->id . "-", "-" . $answer_id . "-", $mAns), 14);
                                if (Storage::disk('images')->exists($file_oldAns)) {
                                    Storage::disk('images')->put($file_newAns, Storage::disk('images')->get($file_oldAns));
                                }
                            }
                            DB::table('answers')->where('id', $answer_id)->update(['text_answer' => $newTextAns]);
                        }
                    }
                }
            }
            else {
                $english_conf = DB::table('english_config')->where("block_id", $block->id)->get(['id', 'text', 'answer_list','true_false','extra_answers'])[0];
                $new_conf = array('block_id'=>$id_block,'text' => $english_conf->text, 'extra_answers' => $english_conf->extra_answers, 'answer_list' => $english_conf->answer_list, 'true_false' => $english_conf->true_false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now());
                DB::table('english_config')->insertGetId($new_conf);

                $english_texts = DB::table('english_text')->where("block_id", $block->id)->get(['id', 'text', 'image_width']);
                foreach ($english_texts as $texts) {

                    $new_text = array('text' => $texts->text, 'image_width' => $texts->image_width, 'block_id' => $id_block, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now());
                    $text_id = DB::table('english_text')->insertGetId($new_text);

                    $newText = DB::table('english_text')->where('id', $text_id)->value('text');
                    $match = [];
                    if (preg_match("/<img src=\"\/img\/frontend\/taskImages\/([a-zA-Z])+\-[0-9]+\-[0-9]+/", $newText, $match)) {
                        foreach ($match as $m) {
                            $newText = str_replace($m, str_replace("-" . $texts->id . "-", "-" . $text_id . "-", $m), $newText);
                            $file_old = substr($m, 14);
                            $file_new = substr(str_replace("-" . $texts->id . "-", "-" . $text_id . "-", $m), 14);

                            if (Storage::disk('images')->exists($file_old)) {
                                Storage::disk('images')->put($file_new, Storage::disk('images')->get($file_old));
                            }
                        }
                        DB::table('english_text')->where('id', $text_id)->update(['text' => $newText]);
                    }

                    $english_questions = DB::table('english_questions')->where("id_english_question", $texts->id)->get(['id', 'text_question', 'right_answer', 'position', 'image_width']);
                    foreach ($english_questions as $question) {
                        $new_question = array('text_question' => $question->text_question, 'right_answer' => $question->right_answer, 'position' => $question->position, 'image_width' => $question->image_width, 'id_english_question' => $text_id, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now());
                        $question_id = DB::table('english_questions')->insertGetId($new_question);

                        $newText = DB::table('english_questions')->where('id', $question_id)->value('text_question');
                        $match = [];
                        if (preg_match("/<img src=\"\/img\/frontend\/taskImages\/([a-zA-Z])+\-[0-9]+\-[0-9]+/", $newText, $match)) {
                            foreach ($match as $m) {
                                $newText = str_replace($m, str_replace("-" . $question->id . "-", "-" . $question_id . "-", $m), $newText);
                                $file_old = substr($m, 14);
                                $file_new = substr(str_replace("-" . $question->id . "-", "-" . $question_id . "-", $m), 14);

                                if (Storage::disk('images')->exists($file_old)) {
                                    Storage::disk('images')->put($file_new, Storage::disk('images')->get($file_old));
                                }

                            }
                            DB::table('english_questions')->where('id', $question_id)->update(['text_question' => $newText]);
                        }

                        $english_answers = DB::table('english_answers')->where("id_english_question", $question->id)->get(['text_answer', 'answer_right', 'id_english_question']);
                        foreach ($english_answers as $answer) {
                            $new_answer = array('text_answer' => $answer->text_answer, 'answer_right' => $answer->answer_right, 'id_english_question' => $question_id, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now());
                            $answer_id = DB::table('english_answers')->insertGetId($new_answer);

                            $newText = DB::table('english_answers')->where('id', $answer_id)->value('text_answer');
                            $match = [];
                            if (preg_match("/<img src=\"\/img\/frontend\/taskImages\/([a-zA-Z])+\-[0-9]+\-[0-9]+/", $newText, $match)) {
                                foreach ($match as $m) {
                                    $newText = str_replace($m, str_replace("-" . $answer->id . "-", "-" . $answer_id . "-", $m), $newText);
                                    $file_old = substr($m, 14);
                                    $file_new = substr(str_replace("-" . $answer->id . "-", "-" . $answer_id . "-", $m), 14);

                                    if (Storage::disk('images')->exists($file_old)) {
                                        Storage::disk('images')->put($file_new, Storage::disk('images')->get($file_old));
                                    }
                                }
                                DB::table('english_answers')->where('id', $question_id)->update(['text_answer' => $newText]);
                            }
                        }
                    }
                }

                $english_questions = DB::table('english_questions')->where("block_id", $block->id)->get(['id', 'text_question', 'right_answer', 'position', 'image_width']);
                foreach ($english_questions as $question) {
                    $new_question = array('text_question' => $question->text_question, 'right_answer' => $question->right_answer, 'position' => $question->position, 'image_width' => $question->image_width, 'block_id' => $id_block, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now());
                    $question_id = DB::table('english_questions')->insertGetId($new_question);

                    $newText = DB::table('english_questions')->where('id', $question_id)->value('text_question');
                    $match = [];
                    if (preg_match("/<img src=\"\/img\/frontend\/taskImages\/([a-zA-Z])+\-[0-9]+\-[0-9]+/", $newText, $match)) {
                        foreach ($match as $m) {
                            $newText = str_replace($m, str_replace("-" . $question->id . "-", "-" . $question_id . "-", $m), $newText);
                            $file_old = substr($m, 14);
                            $file_new = substr(str_replace("-" . $question->id . "-", "-" . $question_id . "-", $m), 14);

                            if (Storage::disk('images')->exists($file_old)) {
                                Storage::disk('images')->put($file_new, Storage::disk('images')->get($file_old));
                            }
                        }
                        DB::table('english_questions')->where('id', $question_id)->update(['text_question' => $newText]);
                    }

                    $english_answers = DB::table('english_answers')->where("id_english_question", $question->id)->get(['text_answer', 'answer_right', 'id_english_question']);
                    foreach ($english_answers as $answer) {
                        $new_answer = array('text_answer' => $answer->text_answer, 'answer_right' => $answer->answer_right, 'id_english_question' => $question_id, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now());
                        $answer_id = DB::table('english_answers')->insertGetId($new_answer);

                        $newText = DB::table('english_answers')->where('id', $answer_id)->value('text_answer');
                        $match = [];
                        if (preg_match("/<img src=\"\/img\/frontend\/taskImages\/([a-zA-Z])+\-[0-9]+\-[0-9]+/", $newText, $match)) {
                            foreach ($match as $m) {
                                $newText = str_replace($m, str_replace("-" . $answer->id . "-", "-" . $answer_id . "-", $m), $newText);
                                $file_old = substr($m, 14);
                                $file_new = substr(str_replace("-" . $answer->id . "-", "-" . $answer_id . "-", $m), 14);

                                if (Storage::disk('images')->exists($file_old)) {
                                    Storage::disk('images')->put($file_new, Storage::disk('images')->get($file_old));
                                }
                            }
                            DB::table('english_answers')->where('id', $question_id)->update(['text_answer' => $newText]);
                        }

                    }
                }

                $english_connections = DB::table('english_connections')->where("block_id", $block->id)->get(['id']);
                foreach ($english_connections as $connection) {

                    $new_connection = array('block_id' => $id_block);
                    $connection_id = DB::table('english_connections')->insertGetId($new_connection);

                    $english_questions = DB::table('english_questions')->where("english_connection_id", $connection->id)->get(['id', 'text_question', 'right_answer', 'type', 'position', 'image_width']);
                    foreach ($english_questions as $question) {
                        $new_question = array('text_question' => $question->text_question, 'type' => $question->type, 'right_answer' => $question->right_answer, 'position' => $question->position, 'image_width' => $question->image_width, 'english_connection_id' => $connection_id, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now());
                        $question_id = DB::table('english_questions')->insertGetId($new_question);

                    }
                }
            }
        }
        HistoryHelper::add([
            'user_id' => Auth::user()->id,
            'icon' => 'clone',
            'class' => 'bg-aqua',
            'text' => 'trans("history.backend.clone") <strong>'.$specialisation->name_speciality.'</strong>'
        ]);
        $response = [
            'ok',
            $new_id,
            $specialisation->name_speciality . ' (копія)'
        ];
        return $response;
    }

    function getIdArrayFromQuery($query) {
        $arr = [];
        foreach ($query as $item) {
            array_push($arr, $item->id);
        }
        return $arr;
    }

	function saveImage ($base64, $id, $typeImage, $extension) {

		if($base64 != "0") {
			$convertedBase64 = str_replace(' ', '+', $base64);

			$image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $convertedBase64));
			$fileName = $typeImage.'_'.$id.$extension;

			Storage::disk('public')->put('blankImages/'.$fileName, $image);
			$url = Storage::url('blankImages/'.$fileName);

			return $url;
		}
		else {
			return 0;
		}
	}

	function deleteElementImages($elementId, $typeElement, $images) {
		$filtedImages = array_filter($images, function ($img) {
			return(strpos($img, 'data:image') === false);
		});

		$whiteList = array_map(function($imgPath) {
			$pathToremove = '/img/';
			return str_replace($pathToremove, '', $imgPath);
		}, $filtedImages);

		$fileName = $typeElement.'-'.$elementId;
		$allFiles = Storage::disk('images')->files('frontend/taskImages/');
		$filesToDelete = [];

		// remove image from array which will delete imgs.
		$filteredAllImages = array_filter($allFiles, function($imgPath) use ($whiteList){
			foreach($whiteList as $img) {
				if(strcmp($imgPath, $img) === 0) {
					return false;
				}
			}
			return true;
		});

		foreach ($filteredAllImages as $file) {
			if(strpos($file, $fileName) !== false) {

				array_push($filesToDelete, $file);
			}
		}
        
		Storage::disk('images')->delete($filesToDelete);
	}

	function saveImages ($elementId, $typeElement, $arrImages) {
		$urls = [];

		$fileName = $typeElement.'-'.$elementId;
		$allFiles = Storage::disk('images')->files('frontend/taskImages/');

		$filteredImages = array_filter($allFiles, function($imgPath) use ($fileName){
			if(strpos($imgPath, $fileName) !== false) {
				return true;
			}
			return false;
		});

		$imagesIndex = [];
		foreach ($filteredImages as $file) {
			$pathToRemove = 'frontend/taskImages/'.$fileName.'-';

			array_push($imagesIndex, intval(str_replace($pathToRemove, '', $file)));
		}

		$maxIndex = count($imagesIndex) > 0 ? max($imagesIndex) : 0;

		foreach ($arrImages as $index => $base64) {

			if (strpos($base64, 'data:image') !== false) {
				$convertedBase64 = str_replace(' ', '+', $base64);
				$image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $convertedBase64));

				$fileName = $typeElement . '-' . $elementId . '-' . ++$maxIndex;

				Storage::disk('images')->put('frontend/taskImages/' . $fileName, $image);

				$url = Storage::disk('images')->url('frontend/taskImages/' . $fileName);
			}
			else {
				$url = $base64;
			}
			array_push($urls, $url);
		}

		return $urls;
	}

	function insertUrlToImages($content, $urlArr) {
		$replacedContent = preg_replace_array('/URLPATH/', $urlArr, $content);
		return $replacedContent;
	}

    function getStuff(){
        $connection = DB::table('english_connections')->get();

        $blocks=blocks::all();
        $questions = questions::all();
        $english_text = DB::table('english_text')->get();
        $english_questions = DB::table('english_questions')->get();
        $response = [
            'english_text' => $english_text,
            'english_questions' => $english_questions,
            'blocks' => $blocks,
            'questions' => $questions,
            'connection' => $connection,
        ];
       return $response;
    }
    function getComm(Request $req){
        $response = DB::table('commissioners')->where('speciality_id',$req->id)->get();
        return $response;
    }

    function getSpecialityTasks(Request $req) {
        $specialityId = $req->id;

        if($req->type == "2") {
            $blocks = DB::table('blocks')->where('id_speciality', $specialityId)->orderBy('block_name')->get();
        }
        else {
            $blocks = DB::table('blocks')->where('id_speciality', $specialityId)->get();
        }

        $queryIdBlocks = DB::table('blocks')->where('id_speciality', $specialityId)->get(['id'])->toArray();

        $idBlocks = self::getIdArrayFromQuery($queryIdBlocks);

        $connectionQuery = DB::table('english_connections')->whereIn('block_id', $idBlocks)->get(['id'])->toArray();

        $connectionIds = self::getIdArrayFromQuery($connectionQuery);

        $connections = DB::table('english_connections')->whereIn('block_id', $idBlocks)->get();

        $questionsConnection = DB::table('english_questions')->whereIn('english_connection_id', $connectionIds)->where('type','list')->get()->groupBy('english_connection_id')->toArray();
        $answersConnection = DB::table('english_questions')->whereIn('english_connection_id', $connectionIds)->where('type','answerList')->get()->groupBy('english_connection_id')->toArray();

        $questions = DB::table('questions')
            ->whereIn('id_block', $idBlocks)
            ->get(['id', 'id_block', 'text_question'])
            ->groupBy('id_block')
            ->toArray();

        $queryIdQuestions = DB::table('questions')
            ->whereIn('id_block', $idBlocks)
            ->get(['id'])
            ->toArray();

        $idQuestions = self::getIdArrayFromQuery($queryIdQuestions);

        $answers = DB::table('answers')
            ->whereIn('id_question', $idQuestions)
            ->get(['id_question', 'text_answer', 'answer_right'])
            ->groupBy('id_question')
            ->toArray();

        $english_answers = DB::table('english_answers')->get();
        $english_config = DB::table('english_config')->get();
        $english_questions = DB::table('english_questions')->orderByRaw("CASE WHEN right_answer = 1 THEN 0 ELSE right_answer END")->orderBy('id')->get();
        $english_text = DB::table('english_text')->get();

        foreach ($questions as $questionsBlock) {
            foreach ($questionsBlock as $question) {
                $idQuestion = $question->id;

                if(isset( $answers[$idQuestion])) {
                    $currAnswers = $answers[$idQuestion];
                    $question->answers = $currAnswers;
                }
                else {
                    $question->answers = [];
                }
            }
        }

        foreach ($blocks as $block) {
            $blockId = $block->id;

            if(isset($questions[$blockId])) {
                $currQuestions = $questions[$blockId];
                $block->questions = $currQuestions;
            }
            else {
                $block->questions = [];
            }
        }

        foreach ($connections as $connection) {
            $connectionId = $connection->id;

            if(isset($questionsConnection[$connectionId])) {
                $currQuestions = $questionsConnection[$connectionId];
                $connection->questions = $currQuestions;
            }
            else {
                $connection->questions = [];
            }
            if(isset($answersConnection[$connectionId])) {
                $currAnswers = $answersConnection[$connectionId];
                $connection->answers = $currAnswers;
            }
            else {
                $connection->answers = [];
            }
        }

        $response = [
            'blocks' => $blocks,
            'english_answers' => $english_answers,
            'english_config' => $english_config,
            'english_questions' => $english_questions,
            'english_text' => $english_text,
            'connections' => $connections,
        ];
        return response($response,200);
    }

    function getNmbAnswers(Request $req) {
        $idBlock = $req->idBlock;

        $dataBlock = DB::table('blocks')->where('id', $idBlock)->get([ 'countAnswers', 'weight_question']);

        $countQuestions = DB::table('questions')->where('id_block', $idBlock)->count();

        $response = [
            'countQuestions' => $countQuestions,
            'weightQuestion' => $dataBlock[0]->weight_question,
            'nmbAnswers' => $dataBlock[0]->countAnswers
        ];

        return response($response, 200);
    }

    function saveUkrainianTasks(Request $req) {
        $questionsContent = $req->questions;
	    $answersContent = $req->answers;
	    $questionImages = $req->questionImages;
	    $answerImages = $req->answerImages;
    	$spec_id = $req->spec_id;
    	$imageWidth = $req->image_width;
	    $block_id = DB::table('blocks')->where('id_speciality', $spec_id)->value('id');

        for($i = 0; $i < count($questionsContent); $i++) {
            $contentQuestionWithUrl = null;
            $contentAnswerWithUrl = null;

            $questionData = [
                'image_width' => $imageWidth[$i],
            	'text_question' => $questionsContent[$i],
	            'id_block' => $block_id,
	            'created_at' => Carbon::now(),
	            'updated_at' => Carbon::now()
            ];
            $question_id = DB::table('questions')->insertGetId($questionData);

	        if($questionImages[$i] && count($questionImages[$i]) > 0) {
		        $urlImages = self::saveImages($question_id, 'Question', $questionImages[$i]);

		        $contentQuestionWithUrl = self::insertUrlToImages($questionsContent[$i], $urlImages);
	        }
	        $contentQuestionWithUrl = $contentQuestionWithUrl ? $contentQuestionWithUrl : $questionsContent[$i];

	        DB::table('questions')
		        ->where('id', $question_id)
		        ->update(['text_question'=>$contentQuestionWithUrl]);

            $answerData = [
            	'text_answer' => $answersContent[$i],
	            'answer_right' => 1,
	            'id_question' => $question_id,
	            'created_at' => Carbon::now(),
	            'updated_at' => Carbon::now()

            ];

            $answer_id = DB::table('answers')->insertGetId($answerData);

	        if($answerImages[$i] && count($answerImages[$i]) > 0) {
		        $urlImages = self::saveImages($answer_id, 'Answer', $answerImages[$i]);

		        $contentAnswerWithUrl = self::insertUrlToImages($answersContent[$i], $urlImages);
	        }
	        $contentAnswerWithUrl = $contentAnswerWithUrl ? $contentAnswerWithUrl : $answersContent[$i];

	        DB::table('answers')
		        ->where('id', $answer_id)
		        ->update(['text_answer'=>$contentAnswerWithUrl]);
        }

        $questions =  count(DB::table('questions')->where('id_block', $block_id)->get());
        DB::table('blocks')->where('id_speciality', $spec_id)->update(['count_q_this_block'=>$questions, 'updated_at'=>Carbon::now()]);

	    return response(200, 200);
    }

    function getUkrainianTasks(Request $req){
        $spec_id = $req->spec_id;
        $block = DB::table('blocks')->where('id_speciality', $spec_id)->get(['id','block_name']);
        $question = DB::table('questions')->where('id_block', $block[0]->id)->get(['id','text_question', 'image_width']);
        $answer = array();

        for($i = 0; $i < count($question); $i++) {
            $answer[$i] = DB::table('answers')->where('id_question', $question[$i]->id)->value('text_answer');
        }

        $size = count($question);
        return ['questions' => $question,
                'answers' => $answer,
                'block_name' => $block[0] -> block_name,
                'size' => $size];
    }

    function updateUkrainianTask(Request $req) {
	    $questionContent = $req->questionContent;
	    $answerContent = $req->answerContent;
        $questionId = $req->question_id;
        $questionImages = $req->questionImages;
        $answerImages = $req->answerImages;
        $imageWidth = $req->image_width;

	    $contentQuestionWithUrl = null;
	    $contentAnswerWithUrl = null;

	    if($questionImages && count($questionImages) > 0) {
		    self::deleteElementImages($questionId, 'Question', $questionImages);
		    $urlImages = self::saveImages($questionId, 'Question', $questionImages);
		    $contentQuestionWithUrl = self::insertUrlToImages($questionContent, $urlImages);
	    }
	    $contentQuestionWithUrl = $contentQuestionWithUrl ? $contentQuestionWithUrl : $questionContent;

	    DB::table('questions')
		    ->where('id', $questionId)
		    ->update([
			    'text_question'=>$contentQuestionWithUrl,
			    'image_width'=>$imageWidth,
			    'updated_at'=>Carbon::now()
		    ]);

	    $answerId = DB::table('answers')->where('id_question', $questionId)->first()->id;

	    if($answerImages && count($answerImages) > 0) {
		    self::deleteElementImages($answerId, 'Answer', $answerImages);
		    $urlImages = self::saveImages($answerId, 'Answer', $answerImages);
		    $contentAnswerWithUrl = self::insertUrlToImages($answerContent, $urlImages);
	    }
	    $contentAnswerWithUrl = $contentAnswerWithUrl ? $contentAnswerWithUrl : $answerContent;

	    DB::table('answers')
		    ->where('id', $answerId)
		    ->update([
			    'text_answer'=>$contentAnswerWithUrl,
			    'updated_at'=>Carbon::now()
		    ]);

	    return response(200);
    }

    function deleteUkrainianTask(Request $req) {
        $spec_id = $req->spec_id;
        $question_id = $req->question_id;

	    self::deleteImgFromQuestions($question_id);

        DB::table('answers')->where('id_question', $question_id)->delete();
        DB::table('questions')->where('id', $question_id)->delete();

        $block_id = DB::table('blocks')->where('id_speciality', $spec_id)->value('id');
        $questions =  count(DB::table('questions')->where('id_block', $block_id)->get());

        DB::table('blocks')->where('id_speciality', $spec_id)->update(['count_q_this_block'=>$questions, 'updated_at'=>Carbon::now()]);
    }

    function getSpecialityParam(Request $req){
        $spec_id = $req -> id;
        $type = DB::table('specialities')->where('id', $spec_id)->value('type');
        $block_query = DB::table('blocks')->where('id_speciality', $spec_id)->get(['id'])->toArray();
        $block_id = self::getIdFromQuery($block_query);
        $questions =  count(DB::table('questions')->whereIn('id_block', $block_id)->get());

        $english =  count(DB::table('english_text')->whereIn('block_id', $block_id)->get()) + count(DB::table('english_questions')->whereIn('block_id', $block_id)->get());
        $connections =  count(DB::table('english_connections')->whereIn('block_id', $block_id)->get());

        return [
            'english_questions' => $english,
            'questions' => $questions,
            'type' => $type,
            'connections' => $connections
        ];
    }

    function saveNewElements(Request $req) {
        $response = null;
        $contentElement = $req->question_text;
        $typeElement = $req->typeElement;
        $images = $req->image;
        $id_block = $req->id_block;
		$dataToInsert = null;

	    if($typeElement === 'Question') {
	    	$imageWidth = $req->image_width;

		    $dataToInsert = [
			    'id_block' => $id_block,
			    'text_question' => $contentElement,
			    'image_width' => $imageWidth,
			    'created_at' => Carbon::now(),
			    'updated_at' => Carbon::now()
		    ];
            HistoryHelper::add([
                'user_id' => Auth::user()->id,
                'icon' => 'plus',
                'class' => 'bg-green',
                'text' => 'trans("history.backend.questions.created") <strong>'.$contentElement.'</strong>'
            ]);
	    }
	    else if ($typeElement === 'Answer') {
		    $answer_right = $req->isChecked;
		    $id_question = $req->id_question;

		    $dataToInsert = [
			    'id_question' => $id_question,
			    'text_answer' => $contentElement,
			    'answer_right' => $answer_right,
			    'created_at' => Carbon::now(),
			    'updated_at' => Carbon::now()
		    ];
	    }

	    if($dataToInsert) {
	    	$tableName = $typeElement === 'Question' ? 'questions' : 'answers';
	    	$fieldName = $typeElement === 'Question' ? 'text_question' : 'text_answer';

		    $id_element = DB::table($tableName)->insertGetId($dataToInsert);

		    if($images && count($images) > 0) {
				$urlImages = self::saveImages($id_element, $typeElement, $images);

				$contentWithUrl = self::insertUrlToImages($contentElement, $urlImages);

			     DB::table($tableName)
					->where('id', $id_element)
					->update([$fieldName=>$contentWithUrl, 'updated_at'=>Carbon::now()]);

			    $response = $typeElement === 'Question' ?
				    ['id_question'=>$id_element, 'question_text'=>$contentWithUrl] :
				    [];
		    }
		    else {
			    $response = $typeElement === 'Question' ?
				    ['id_question'=>$id_element, 'question_text'=>$contentElement]:
				    [];
		    }
	    }
	    else {
	    	return response('Invalid typeElement',500);
	    }

	    return response()->json($response, 200);
    }

    function getBlocks(Request $req)
    {
        $id = $req->id;

        $blockData = DB::table('blocks')->where('id_speciality', $id)->get(['id', 'block_name'])->toArray();

        return $blockData;
    }

    function getBlock(Request $req) {
        $blockId = $req->blockId ;

        $response = NULL;
        $status = 200;

        $isExist = !is_null(DB::table('blocks')->where('id', $blockId)->first());
        $isExistEng = !is_null(DB::table('english_config')->where('block_id', $blockId)->first());

        if($isExist) {
            $blockData = DB::table('blocks')->where('id', $blockId)->get(['id', 'block_name', 'weight_question', 'countAnswers','type','blank_text','task_title','url_image'])->toArray();
            if($isExistEng) {
                $englishConfig = DB::table('english_config')->where('block_id', $blockId)->get(['id', 'text', 'answer_list', 'true_false', 'extra_answers'])->toArray();

                if (sizeof(DB::table('english_connections')->where('block_id', $blockId)->get(['id'])) ||
                    sizeof(DB::table('english_questions')->where('block_id', $blockId)->get(['id'])) ||
                    sizeof(DB::table('english_text')->where('block_id', $blockId)->get(['id']))) {
                    $block = 1;
                }
                else {
                    $block = 0;
                }

                $response = [
                    'data' => $blockData[0],
                    'english' => $englishConfig[0],
                    'block' => $block
                ];
            }
            else {
                $response = [
                    'data' => $blockData[0],
                    'english' => '',
                ];
            }
        }
        else {
            $response = ['error' => 'This blocks doesn\'t exist'];
            $status = 404;
        }

        return response()->json($response, $status);
    }

    function saveBlockImage(Request $req){
        $base64 = $req->base64;
        $id = $req->id;
        $extension = $req->extension;

        $convertedBase64 = str_replace(' ', '+', $base64);

        $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $convertedBase64));
        $fileName = 'block_'.$id.$extension;

        Storage::disk('public')->put('blankImages/'.$fileName, $image);
        $url = Storage::url('blankImages/'.$fileName);

        DB::table('blocks')
            ->where('id', $id)
            ->update(['url_image'=>$url, 'updated_at'=>Carbon::now()]);
    }

    function createBlock(Request $req) {

        $title = $req->title;
        $specialityId = $req->specialityId;
        $blockName = $req->blockName;
        $weightTask = $req->weightTask;
        $nmbAnswers = $req->nmbAnswers;

        $newFieldData = [
            'task_title' => $title,
            'block_name' => $blockName,
            'weight_question' => $weightTask,
            'id_speciality' => $specialityId,
            'countAnswers' => $nmbAnswers,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        ];

        $idBlock = DB::table('blocks')->insertGetId($newFieldData);

        $response = [
            'idBlock'=> $idBlock,
            'idSpeciality'=> $specialityId
        ];
        HistoryHelper::add([
            'user_id' => Auth::user()->id,
            'icon' => 'plus',
            'class' => 'bg-green',
            'text' => 'trans("history.backend.blocks.created") <strong>'.$blockName.'</strong>'
        ]);
        return response()->json($response);
    }

    function updateConf(Request $req){
        $blockId = $req->blockId;
        $spareAnswers = $req->spareAnswers;
        $checkText = $req->checkText;
        $checkAnswers = $req->checkAnswers;
        $checkTF = $req->checkTF;

        if($checkText != 0) {
            DB::table('blocks')
                ->where('id', $blockId)
                ->update([
                    'type' => 2,
                    'updated_at' => Carbon::now()
                ]);
        }
        else {
            DB::table('blocks')
                ->where('id', $blockId)
                ->update([
                    'type' => 0,
                    'updated_at' => Carbon::now()
                ]);
        }
       
        $isExist = !is_null(DB::table('english_config')->where('block_id', $blockId)->first());

        if($isExist) {
            DB::table('english_config')
                ->where('block_id', $blockId)
                ->update([
                    'extra_answers' => $spareAnswers,
                    'text' => $checkText,
                    'answer_list' => $checkAnswers,
                    'true_false' => $checkTF,
                    'updated_at' => Carbon::now()
                ]);
        }
        else {
            DB::table('english_config')
                ->insert(['block_id', $blockId,
                    'extra_answers' => $spareAnswers,
                    'text' => $checkText,
                    'answer_list' => $checkAnswers,
                    'true_false' => $checkTF,
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now()
                ]);
        }
    }

    function updateBlock(Request $req) {
        $title = $req->title;
        $blockId = $req->blockId;
        $blockName = $req->blockName;
        $weightTask = $req->weightTask;
        $nmbAnswers = $req->nmbAnswers;
        $response = NULL;
        $status = 200;

        $isExist = !is_null(DB::table('blocks')->where('id', $blockId)->first());

        if($isExist) {

            $questionsIdQuestions =DB::table('questions')->where('id_block', $blockId)->get(['id'])->toArray();

            $questionsId = self::getIdArrayFromQuery($questionsIdQuestions);

            foreach ($questionsId as $currQuestion) {
                $countAnswer = DB::table('answers')
                    ->where('id_question', $currQuestion)
                    ->count();

                $nmbCountAnswers = $nmbAnswers - $countAnswer;

                if($nmbCountAnswers > 0) {
                    $newAnswer = [
                        'text_answer' => 'Автоматично створене питання, відредагуйте його',
                        'answer_right' => 0,
                        'id_question' => $currQuestion,
                        'created_at'=>Carbon::now(),
                        'updated_at'=>Carbon::now()
                    ];
                    $answersToInsert = array_fill(0, $nmbCountAnswers, $newAnswer);

                    DB::table('answers')->insert($answersToInsert);
                }
            }
            $oldBlockName = DB::table('blocks')->where('id', $blockId)->value('block_name');
            DB::table('blocks')
                ->where('id', $blockId)
                ->update([
                    'task_title' => $title,
                    'block_name' => $blockName,
                    'weight_question' => $weightTask,
                    'countAnswers' => $nmbAnswers,
                    'updated_at' => Carbon::now()
                ]);

        }
        else {
            $response = ['error' => 'This blocks doesn\'t exits'];
            $status = 404;
        }
        HistoryHelper::add([
            'user_id' => Auth::user()->id,
            'icon' => 'save',
            'class' => 'bg-aqua',
            'text' => 'trans("history.backend.blocks.updated") <strong>'.$oldBlockName.'</strong>'
        ]);
        return response($response, $status);
    }

    function deleteBlock(Request $req) {
        $blockId = $req->idBlock;
        $idSpeciality = $req->idSpeciality;
        $status = 200;
        $blockName = DB::table('blocks')->where('id', $blockId)->value('block_name');

        $nmbBlocks = DB::table('blocks')->where('id_speciality', $idSpeciality)->count();

        $questionIds = DB::table('questions')->where('id_block', $blockId)->get(['id']);

        // delete all question block
        $questIds = DB::table('questions')->where('id_block', $blockId)->pluck('id');
        
        for ($i=0; $i < count($questIds) ; $i++) { 
            self::deleteImgFromQuestions($questIds[$i]);
        }

        foreach ($questionIds as $id) {
            DB::table('answers')->where('id_question', $id->id)->delete();
        }

        $textIds = DB::table('english_text')->where('block_id', $blockId)->get(['id']);
        foreach ($textIds as $id) {
            $questionIds = DB::table('english_questions')->where('id_english_question', $id->id)->get(['id']);
            foreach ($questionIds as $idQ) {
                DB::table('english_answers')->where('id_english_question', $idQ->id)->delete();
            }
            DB::table('english_questions')->where('block_id', $blockId)->delete();
            DB::table('english_questions')->where('id_english_question', $id->id)->delete();
        }

        $questionIds = DB::table('english_questions')->where('block_id', $blockId)->get(['id']);
        foreach ($questionIds as $idQ) {
            DB::table('english_answers')->where('id_english_question', $idQ->id)->delete();
        }
        DB::table('english_questions')->where('block_id', $blockId)->delete();

        DB::table('english_connections')->where('block_id', $blockId)->delete();

        DB::table('english_text')->where('block_id', $blockId)->delete();

        DB::table('english_config')->where('block_id', $blockId)->delete();

        DB::table('questions')->where('id_block', $blockId)->delete();

        DB::table('blocks')->delete($blockId);

        $response = [
            'nmbBlocks' => $nmbBlocks,
        ];

        HistoryHelper::add([
            'user_id' => Auth::user()->id,
            'icon' => 'trash',
            'class' => 'bg-maroon',
            'text' => 'trans("history.backend.blocks.deleted") <strong>'.$blockName.'</strong>'
        ]);

        return response()->json($response, $status);
    }

    function updateQuestion(Request $req) {
        $id = $req->id;
        $contentData = $req->contentData;
        $images = $req->images;
        $imageWidth = $req->imageWidth;
        $typeImage = 'Question';
        $contentWithUrl = null;

        if($images && count($images) > 0) {
            self::deleteElementImages($id, $typeImage, $images);
            $urlImages = self::saveImages($id, $typeImage, $images);

            $contentWithUrl = self::insertUrlToImages($contentData, $urlImages);
        }
        $contentWithUrl = $contentWithUrl ? $contentWithUrl : $contentData;

        $nameQuestion = DB::table('questions')
            ->where('id', $id)
            ->value('text_question');

        DB::table('questions')
            ->where('id', $id)
            ->update([
                'text_question'=>$contentWithUrl,
                'image_width' =>$imageWidth,
                'updated_at'=>Carbon::now()
            ]);

        DB::table('answers')
            ->where('id_question', $id)
            ->delete();

        HistoryHelper::add([
            'user_id' => Auth::user()->id,
            'icon' => 'save',
            'class' => 'bg-aqua',
            'text' => 'trans("history.backend.questions.updated") <strong>'.$nameQuestion.'</strong>'
        ]);
        return response(200);
    }

    function updateAnswer(Request $req) {
        $contentData = $req->contentData;
        $images = $req->images;
        $isChecked = $req->isChecked;
        $typeImage = 'Answer';

        $contentWithUrl = null;

        $questionId = $req->questionId;
        $newAnswer = [
            'text_answer' => $contentData,
            'answer_right' => $isChecked,
            'id_question' => $questionId,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ];

        $id = DB::table('answers')
            ->insertGetId($newAnswer);

        if($images && count($images) > 0) {
            self::deleteElementImages($id, $typeImage, $images);
            $urlImages = self::saveImages($id, $typeImage, $images);
            $contentWithUrl = self::insertUrlToImages($contentData, $urlImages);
        }
        $contentWithUrl = $contentWithUrl ? $contentWithUrl : $contentData;

        DB::table('answers')
            ->where('id', $id)
            ->update([
                'text_answer'=>$contentWithUrl,
                'updated_at'=>Carbon::now()
            ]);

        return response(200);
    }
    
    function deleteImgFromQuestions($idQuestion) {
        // delete img
        $idAnswers = DB::table('answers')->where('id_question', $idQuestion)->pluck('id');

        $typeQ = 'Question';
        $typeA = 'Answer';
        $pathToImg = 'frontend/taskImages/';
        $fileNameQ = $pathToImg.$typeQ.'-'.$idQuestion;
        $fileNameA = [];
        for ($i = 0; $i < count($idAnswers); $i++){
            array_push($fileNameA, $pathToImg.$typeA.'-'.$idAnswers[$i]);
        }

        $allFiles = Storage::disk('images')->files('frontend/taskImages/');
        $filesToDelete = [];

        foreach ($allFiles as $file) {
            if(strpos($file, $fileNameQ) !== false) {

                array_push($filesToDelete, $file);
            }
        }
        for ($i = 0; $i < count($idAnswers); $i++) {
            foreach ($allFiles as $file) {
                if (strpos($file, $fileNameA[$i]) !== false) {

                    array_push($filesToDelete, $file);
                }
            }
        }

        for ($i = 0; $i < count($filesToDelete) ; $i++) {
            Storage::disk('images')->delete($filesToDelete[$i]);
        }
    }

    function deleteEnglishImages($idElement, $typeElement) {
	    $pathToImg = 'frontend/taskImages/';
	    $baseFileName =  $pathToImg.$typeElement.'-'.$idElement;
	    $allFiles = Storage::disk('images')->files('frontend/taskImages/');

	    $filesToDelete = [];
    	if($typeElement === 'English_Text') {
		    foreach ($allFiles as $file) {
			    if(strpos($file, $baseFileName) !== false) {
				    array_push($filesToDelete, $file);
			    }
		    }
	    }
    	else if($typeElement === 'English_Question') {
    		$answerType = 'English_Answer';

    		$idAnswers = DB::table('english_answers')->where('id_english_question', $idElement)->pluck('id');

		    $answersBaseFileName = [];
		    for ($i = 0; $i < count($idAnswers); $i++) {
			    array_push($answersBaseFileName, $pathToImg.$answerType.'-'.$idAnswers[$i]);
		    }

		    foreach ($allFiles as $file) {
			    if(strpos($file, $baseFileName) !== false) {
				    array_push($filesToDelete, $file);
			    }
		    }

		    for ($i = 0; $i < count($idAnswers); $i++) {
			    foreach ($allFiles as $file) {
				    if (strpos($file, $answersBaseFileName[$i]) !== false) {
					    array_push($filesToDelete, $file);
				    }
			    }
		    }
	    }

	    for ($indexFile = 0; $indexFile < count($filesToDelete) ; $indexFile++) {
		    Storage::disk('images')->delete($filesToDelete[$indexFile]);
	    }
    }

    function deleteQuestion(Request $req) {

        try {
            $idQuestion = $req->idQuestion;
            $nameQuestion = DB::table('questions')
                ->where('id', $req->idQuestion)
                ->value('text_question');
            self::deleteImgFromQuestions($idQuestion);

            DB::table('questions')->delete($idQuestion);

            DB::table('answers')->where('id_question', $idQuestion)->delete();

            HistoryHelper::add([
                'user_id' => Auth::user()->id,
                'icon' => 'trash',
                'class' => 'bg-maroon',
                'text' => 'trans("history.backend.questions.deleted") <strong>'.$nameQuestion.'</strong>'
            ]);
            return response(200, 200);
        }
        catch (\Exception $e) {
            return response($e->getMessage(), 404);
        }
    }

    function getQuestions(Request $req) {
        $idBlock = $req->idBlock;

        $questions = DB::table('questions')->where('id_block', $idBlock)->get(['id', 'text_question'])->toArray();
        return response()->json($questions,200);
    }

    function getQuestion(Request $req) {

        $idQuestion = $req->idQuestion;

        $answers = DB::table('answers')->where('id_question', $idQuestion)->get(['id', 'text_answer', 'answer_right'])->toArray();

        $question = DB::table('questions')->where('id', $idQuestion)->get(['text_question', 'image_width'])->toArray();

        return response()->json(['answers' => $answers, 'question' => $question],200);
    }

    function deleteAnswer(Request $req) {
        DB::table('answers')->where('id', $req->id)->delete();
    }

    function getSettings(Request $req) {
        $id = $req->block_id;
        $settings = DB::table('english_config')->where('block_id', $id)->get(['text', 'answer_list', 'true_false', 'extra_answers'])->toArray()[0];
        $countAnswers = DB::table('blocks')->where('id', $id)->value('countAnswers');
        return response()->json([
            'settings' => $settings,
            'countAnswers' => $countAnswers
        ], 200);
    }

    function getEngQuestions(Request $req) {
        $id = $req->blockId;
        $isListAnswer = $req->isListAnswer;
        $isText = $req->isText;
        $response = null;

        if($isListAnswer == 'true') {
            if($isText == 'true') {
                $response = DB::table('english_text')->where('block_id', $id)->get(['id', 'text', 'image_width'])->toArray();
            }
            else {
                $connection = DB::table('english_connections')->where('block_id', $id)->get(['id'])->toArray();
                $i = 0;
                foreach ($connection as $one) {
                    $answers = DB::table('english_questions')->where('english_connection_id', $one->id)->get(['id', 'text_question', 'type'])->toArray();

                    $listId = "";
                    $text = "";
                    $count = 0;

                    foreach ($answers as $answer) {
                        if($answer->type == 'answerList') {
                            $listId .= strval($answer->id) . " ";
                            $text .= $answer->text_question . " ";
                        }
                        else {
                            $count++;
                        }
                    }

                    $response[$i++] = [
                        'id' => $listId,
                        'text' => $text,
                        'image_width' => $count
                    ];
                }
            }
        }
        else if($isText == 'true') {
            $response = DB::table('english_text')->where('block_id', $id)->get(['id', 'text', 'image_width'])->toArray();
        }
        else {
            $response = DB::table('english_questions')->where('block_id', $id)->get(['id', 'text_question', 'image_width'])->toArray();
        }

        return response()->json($response, 200);
    }

    function  deleteEngQuestion(Request $req) {
        $id = $req->id;
        $isListAnswer = $req->settings['isListAnswer'];
        $isText = $req->settings['isText'];

        if($isListAnswer == 'true') {
            if($isText == 'true') {
	            self::deleteEnglishImages($id, 'English_Text');

                DB::table('english_questions')->where('id_english_question', $id)->delete();
                DB::table('english_text')->where('id', $id)->delete();
            }
            else {
                $first_id = explode(' ', $id);

                $list_id = DB::table('english_questions')->where('id', $first_id[0])->get(['english_connection_id'])->toArray();

                DB::table('english_questions')->where('english_connection_id', $list_id[0]->english_connection_id)->delete();
                DB::table('english_connections')->where('id', $list_id[0]->english_connection_id)->delete();
            }
        }
        else if($isText == 'true') {
            $question_id = DB::table('english_questions')->where('id_english_question', $id)->get(['id'])->toArray();
            foreach($question_id as $del_id) {
	            self::deleteEnglishImages($del_id->id, 'English_Question');

                DB::table('english_answers')->where('id_english_question', $del_id->id)->delete();
            }
            DB::table('english_questions')->where('id_english_question', $id)->delete();

	        self::deleteEnglishImages($id, 'English_Text');
            DB::table('english_text')->where('id', $id)->delete();
        }
        else {
	        self::deleteEnglishImages($id, 'English_Question');

            DB::table('english_answers')->where('id_english_question', $id)->delete();
            DB::table('english_questions')->where('id', $id)->delete();
        }
    }

    function getEngQuestion(Request $req) {
        $id = $req->id;
        $isListAnswer = $req->settings['isListAnswer'];
        $isText = $req->settings['isText'];
        $text = null;
        $question = null;
        $answers = null;

        if($isListAnswer == 'true') {
            if($isText == 'true') {
            	$text = DB::table('english_text')
					->where('id', $id)
					->value('text');

                $answers = DB::table('english_questions')->where('id_english_question', $id)->get(['id', 'text_question', 'position', 'type'])->toArray();
            }
            else {
                $first_id = explode(' ', $id);

                $list_id = DB::table('english_questions')->where('id', $first_id[0])->get(['english_connection_id'])->toArray();

                $question = DB::table('english_questions')->where([['english_connection_id', $list_id[0]->english_connection_id], ['type', 'answerList']])->get(['id', 'text_question'])->toArray();
                $answers =  DB::table('english_questions')->where([['english_connection_id', $list_id[0]->english_connection_id], ['type', 'list']])->get(['id', 'text_question', 'right_answer'])->toArray();
            }
        }
        else if($isText == 'true') {
			$text = DB::table('english_text')
				->where('id', $id)
				->value('text');

            $question = DB::table('english_questions')->where('id_english_question', $id)->get(['id', 'text_question', 'right_answer', 'position', 'image_width', 'type'])->toArray();
            $i = 0;
            foreach ($question as $currQuestion) {
                $answers[$i++] = DB::table('english_answers')->where('id_english_question', $currQuestion->id)->get(['id', 'text_answer', 'answer_right'])->toArray();
            }
        }
        else {
        	$question = DB::table('english_questions')
				->where('id', $id)
				->value('text_question');

            $answers = DB::table('english_answers')->where('id_english_question', $id)->get(['id', 'text_answer', 'answer_right'])->toArray();
        }

        return response()->json([
        	'text' => $text,
            'question' => $question,
            'answers' => $answers
        ], 200);
    }

    function updateEditEng(Request $req) {
        $isListAnswer = $req->settings['isListAnswer'];
        $isText = $req->settings['isText'];
        $question = $req->question;
        $answers = $req->answers;
	    $typeImage = 'English_Text';

        if($isListAnswer == 'true') {
           if($isText == 'true') {
           	    $questionData = $question['text'];

				$contentWithUrl = null;
				if($questionData['images'] && count($questionData['images']) > 0) {

				   self::deleteElementImages($question['id'], $typeImage, $questionData['images']);
				   $urlImages = self::saveImages($question['id'], $typeImage, $questionData['images']);

				   $contentWithUrl = self::insertUrlToImages($questionData['content'], $urlImages);
				}
				$contentWithUrl = $contentWithUrl ? $contentWithUrl : $questionData['content'];

				DB::table('english_text')
				   ->where('id', $question['id'])
				   ->update([
				       'text'=>$contentWithUrl,
				       'image_width' =>$question['image_width'],
				       'updated_at'=>Carbon::now()
				   ]);

			   foreach($answers as $answer) {
				   DB::table('english_questions')->where('id', $answer['id'])->update([
					   'text_question' => $answer['text_question'],
					   'right_answer' => $answer['right_answer'],
					   'position' => $answer['position'],
					   'updated_at' => Carbon::now()
				   ]);
			   }
           }
           else {
               foreach ($question as $one) {
                   DB::table('english_questions')->where('id', $one['id'])->update([
                       'text_question' => $one["text_question"],
                       'updated_at' => Carbon::now()
                   ]);
               }

               $connection_id = DB::table('english_questions')->where('id', $answers[0]['id'])->get(['english_connection_id'])->toArray();
               $connection_id = $connection_id[0]->english_connection_id;
               DB::table('english_questions')->where([['english_connection_id', $connection_id], ['type', 'list']])->delete();

               foreach ($answers as $answer) {
                   DB::table('english_questions')->insert([
                       'block_id' => 0,
                       'text_question' => $answer['text_question'],
                       'id_english_question' => 0,
                       'right_answer' => $answer['right_answer'],
                       'position' => '',
                       'image_width' => 150,
                       'type' => 'list',
                       'english_connection_id' => $connection_id,
                       'created_at' => Carbon::now(),
                       'updated_at' => Carbon::now()
                   ]);
               }
           }
        }
        else if($isText == 'true') {
            $title = $req->title;

	        $textData = $title['text'];

	        $contentWithUrl = null;
	        $typeImage = 'F';
	        if($textData['images'] && count($textData['images']) > 0) {
		        self::deleteElementImages($title['id'], $typeImage, $textData['images']);
		        $urlImages = self::saveImages($title['id'], $typeImage, $textData['images']);
		        $contentWithUrl = self::insertUrlToImages($textData['content'], $urlImages);
	        }
	        $contentWithUrl = $contentWithUrl ? $contentWithUrl : $textData['content'];

            DB::table('english_text')->where('id', $title['id'])->update([
                'text' => $contentWithUrl,
                'image_width' => $title['image_width'],
                'updated_at' => Carbon::now()
            ]);

            $i = 0;
            foreach ($question as $curQuestion) {
	            $questionData = $curQuestion['text_question'];
	            $contentWithUrl = null;
	            $typeImage = 'English_Question';

	            if($questionData['images'] && count($questionData['images']) > 0) {
		            self::deleteElementImages($curQuestion['id'], $typeImage, $questionData['images']);
		            $urlImages = self::saveImages($curQuestion['id'], $typeImage, $questionData['images']);

		            $contentWithUrl = self::insertUrlToImages($questionData['content'], $urlImages);
	            }

	            $contentWithUrl = $contentWithUrl ? $contentWithUrl : $questionData['content'];

                DB::table('english_questions')
	                ->where('id', $curQuestion['id'])
	                ->update([
	                    'text_question' => $contentWithUrl,
	                    'image_width' => $curQuestion['image_width'],
	                    'updated_at' => Carbon::now()
                    ]);

                foreach ($answers[$i++] as $answer) {
	                $answerData = $answer['text_answer'];

	                $contentWithUrl = null;
	                $typeImage = 'English_Answer';
	                if($answerData['images'] && count($answerData['images']) > 0) {
		                self::deleteElementImages($answer['id'], $typeImage, $answerData['images']);
		                $urlImages = self::saveImages($answer['id'], $typeImage, $answerData['images']);

		                $contentWithUrl = self::insertUrlToImages($answerData['content'], $urlImages);
	                }
	                $contentWithUrl = $contentWithUrl ? $contentWithUrl : $answerData['content'];

	                DB::table('english_answers')
		                ->where('id', $answer['id'])
		                ->update([
	                        'text_answer' => $contentWithUrl,
	                        'answer_right' => $answer['answer_right'] == 'true' ? 1 : 0,
	                        'updated_at' => Carbon::now()
                        ]);
                }
            }
        }
        else {
	        $questionData = $question['text_question'];

	        $contentWithUrl = null;
	        $typeImage = 'English_Question';
	        if($questionData['images'] && count($questionData['images']) > 0) {

		        self::deleteElementImages($question['id'], $typeImage, $questionData['images']);
		        $urlImages = self::saveImages($question['id'], $typeImage, $questionData['images']);

		        $contentWithUrl = self::insertUrlToImages($questionData['content'], $urlImages);
	        }
	        $contentWithUrl = $contentWithUrl ? $contentWithUrl : $questionData['content'];

            DB::table('english_questions')
	            ->where('id', $question['id'])
	            ->update([
	                'text_question' => $contentWithUrl,
	                'image_width' => $question['image_width'],
	                'updated_at' => Carbon::now()
	            ]);

            foreach ($answers as $answer) {
	            $answerData = $answer['text_answer'];
	            $typeImage = 'English_Answer';

	            $contentWithUrl = null;
	            if($answerData['images'] && count($answerData['images']) > 0) {

		            self::deleteElementImages($answer['id'], $typeImage, $answerData['images']);
		            $urlImages = self::saveImages($answer['id'], $typeImage, $answerData['images']);

		            $contentWithUrl = self::insertUrlToImages($answerData['content'], $urlImages);
	            }
	            $contentWithUrl = $contentWithUrl ? $contentWithUrl : $answerData['content'];

                DB::table('english_answers')
	                ->where('id', $answer['id'])
	                ->update([
	                    'text_answer' => $contentWithUrl,
	                    'answer_right' => $answer['answer_right'] == 'true' ? 1 : 0,
	                    'updated_at' => Carbon::now()
                    ]);
            }
        }
    }
}

