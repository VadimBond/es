<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\VarDumper\Cloner\Data;
use DB;
use Carbon\Carbon;

class NewBlockController extends Controller
{

    function setLangBlock(Request $req) {
        $id = $req->id;
        $name = DB::table('blocks')->where('id_speciality',$id)->value('block_name');
        return $name;
    }

    function updateLangBlock(Request $req) {
        $id = $req->id;
        $name = $req->name;
        DB::table('blocks')->where('id_speciality',$id)->update(['block_name'=>$name,'updated_at'=>Carbon::now()]);
        $new_name =  DB::table('blocks')->where('id_speciality',$id)->value('block_name');
        return $new_name;
    }

    function go_to_db(Request $req) {

        $id_speciality_block = $req->id_speciality_block;
        $name_block = $req->name_block;
        $task_weight = $req->task_weight;
        $nmb_tasks = $req->nmb_tasks;
        $count_Answers = $req->count_Answers;

        $data_first_query = array('block_name'=>$name_block, 'weight_question'=>$task_weight, 'count_q_this_block'=>$nmb_tasks, 'id_speciality'=>$id_speciality_block, 'countAnswers'=>$count_Answers, 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now());
        DB::table('blocks')->insert($data_first_query);

        return redirect('/dashboard');
    }

    static function getIdFromQuery($query) {
        $arr = [];

        foreach ($query as $item) {
            array_push($arr, $item->id);
        }

        return $arr;
    }

    function update(Request $req){
        $id_block_for_change = $req->data_block_id;
        $change_name_block = $req->change_name_block;
        $change_task_weight = $req->change_task_weight;
        $change_nmb_tasks = $req->change_nmb_tasks;

        DB::table('blocks')
            ->where('id', $id_block_for_change)
            ->update(['block_name'=>$change_name_block, 'weight_question'=>$change_task_weight,'count_q_this_block'=>$change_nmb_tasks, 'updated_at'=>Carbon::now()]);

        return redirect('/dashboard');
    }
}
