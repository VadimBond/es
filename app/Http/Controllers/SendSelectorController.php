<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class SendSelectorController extends Controller
{
    function go_to_db(Request $req){
        $specialityId = $req->id_speciality;

        $blocks = DB::table('blocks')->where('id_speciality', $specialityId)->get(['id as blockId', 'block_name as blockName', 'id_speciality as idSpeciality'])->toArray();
        $status = DB::table('specialities')->where('id', $specialityId)->value('status');

        $response = [
            'blocks' => $blocks,
            'blank_status' => $status
        ];

        $response['status'] = 'updated';
        return response()->json($response, 200);
    }
}
