<?php

namespace App\Http\Controllers\Frontend\Omu;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\HistoryHelper;
use Carbon\Carbon;
use App\blocks;
use App\questions;
use App\answers;


use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;



use Hash;
use Auth;
use DB;




class SpecialityController extends Controller
{
    public function add_speciality($id)
    {  $number = "";
        $name = "";
        $depId = "";
        $title = "";
        $reason = "";

        $depName = DB::table('cafedres')->where("id", $id)->value('name_cafedres');
        $data = [
            'depName' => $depName,
            'type' =>1,
            'id' =>0,
            'idDep' => $id,
            'name' => $name,
            'number' => $number,
            'depId' => $depId,
            'title' => $title,
            'reason' => $reason,
        ];
        return view('frontend.pages.edit_speciality', compact('data'));
    }

    public function edit_speciality($id)
    {
        $number = "";
        $name = "";
        $depId = "";
        $title = "";
        $reason = "";

        $specialities = DB::select('select * from specialities ');
        $cafedres = DB::select('select id,name_cafedres from cafedres');
        foreach ($specialities as $dep) {
            if ($dep->id == $id) {
                $name = $dep->name_speciality;
                $number = $dep->number;
                $depId = $dep->id_cafedres;
                $title = $dep->blank_title;
                $reason = $dep->blank_purpose;
            }
        }

        $data = [
            'type' =>2,
            'cafedres' => $cafedres,
            'id' => $id,
            'name' => $name,
            'number' => $number,
            'depId' => $depId,
            'title' => $title,
            'reason' => $reason,
        ];
        return view('frontend.pages.edit_speciality', compact('data'));
    }
    public function add_specialisation(Request $req)
    {
        if ($req->name && $req->number && is_numeric($req->dep) && $req->title && $req->reason && is_numeric($req->type)) {
            $specialisation_name = $req->name;
            $specialisation_number = $req->number;
            $specialisation_dep = $req->dep;
            $specialisation_title = $req->title;
            $specialisation_reason = $req->reason;
            $specialisation_type = $req->type;

            if(strlen($specialisation_name) < 191 && strlen($specialisation_number) < 501 && strlen($specialisation_title) < 501 && strlen($specialisation_reason) < 501) {
                $new_specialisation = array('name_speciality' => $specialisation_name, 'number' => $specialisation_number, 'id_cafedres' => $specialisation_dep,
                    'blank_title' => $specialisation_title,
                    'blank_purpose' => $specialisation_reason,
                    'status' => 'in_development', 'type' => $specialisation_type,'created_at' => Carbon::now(), 'updated_at' => Carbon::now());
                $id = DB::table('specialities')->insertGetId($new_specialisation);
                $response = [
                    'id' => $id,
                ];

                if($specialisation_type == 1) {
                    DB::table('blocks')->insert([
                        'block_name' => $specialisation_name,
                        'weight_question' => 100,
                        'count_q_this_block' => 0,
                        'id_speciality' => $id,
                        'countAnswers' => 0,
                        'count_chosed_questions' => 0,
                        'type' => 1,
                        'blank_text' => null,
                        'url_image' => 0,
                        'task_title' => null,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ]);
                }

                HistoryHelper::add([
                    'user_id' => Auth::user()->id,
                    'icon' => 'plus',
                    'class' => 'bg-green',
                    'text' => 'trans("history.backend.speciality.created") <strong>'.$specialisation_name.'</strong>'
                ]);
                return $response;
            }
        }
        $response = [
            'error',
        ];
        return $response;

    }

    public function update_specialisation(Request $req)
    {
        if ($req->name && $req->number && is_numeric($req->dep) && $req->title && $req->reason) {
            $specialisation_name = $req->name;
            $specialisation_id = $req->id;
            $specialisation_number = $req->number;
            $specialisation_dep = $req->dep;
            $specialisation_title = $req->title;
            $specialisation_reason = $req->reason;
            if(strlen($specialisation_name) < 191 && strlen($specialisation_number) < 501 && strlen($specialisation_title) < 501 && strlen($specialisation_reason) < 501) {
                DB::table('specialities')
                    ->where('id', $specialisation_id)
                    ->update(['name_speciality' => $specialisation_name, 'number' => $specialisation_number, 'id_cafedres' => $specialisation_dep,
                        'blank_title' => $specialisation_title,
                        'blank_purpose' => $specialisation_reason,
                        'updated_at' => Carbon::now()]);
                $response = [
                    'ok'
                ];
                HistoryHelper::add([
                    'user_id' => Auth::user()->id,
                    'icon' => 'save',
                    'class' => 'bg-aqua',
                    'text' => 'trans("history.backend.speciality.updated") <strong>'.$specialisation_name.'</strong>'
                ]);
                return response()->json($response);
            }
        }
        $response = [
            'error',
        ];
        return response()->json($response);
    }

    function deleteImgFromQuestions($idQuestion){
        // delete img

        $idAnswers = DB::table('answers')->where('id_question', $idQuestion)->pluck('id');

        $typeQ = 'Question';
        $typeA = 'Answer';
        $pathToImg = 'frontend/taskImages/';
        $fileNameQ = $pathToImg.$typeQ.'-'.$idQuestion;
        $fileNameA = [];
        for ($i = 0; $i < count($idAnswers); $i++){
            array_push($fileNameA, $pathToImg.$typeA.'-'.$idAnswers[$i]);
        }

        $allFiles = Storage::disk('images')->files('frontend/taskImages/');
        $filesToDelete = [];

        foreach ($allFiles as $file) {
            if(strpos($file, $fileNameQ) !== false   ) {

                array_push($filesToDelete, $file);
            }
        }
        for ($i = 0; $i < count($idAnswers); $i++) {
            foreach ($allFiles as $file) {
                if (strpos($file, $fileNameA[$i]) !== false) {

                    array_push($filesToDelete, $file);
                }
            }
        }

        for ($i=0; $i < count($filesToDelete) ; $i++) {
            Storage::disk('images')->delete($filesToDelete[$i]);
        }
    }

    public function delete_specialisation(Request $req)
    {

        $id_specialisation_for_delete = $req->id;

        DB::table('blanks')->where('id_speciality', $id_specialisation_for_delete)->delete();

        $dirToDelete = storage_path(sprintf('/app/public/generatedPdf/%s', $id_specialisation_for_delete));
        File::deleteDirectory($dirToDelete);


        $blocks = DB::table('blocks')->where('id_speciality',$id_specialisation_for_delete)->get();
        foreach($blocks as $block){

            $questionIds = DB::table('questions')->where('id_block', $block->id)->get(['id']);

            $questIds = DB::table('questions')->where('id_block', $block->id)->pluck('id');

            for ($i=0; $i < count($questIds) ; $i++) {
                self::deleteImgFromQuestions($questIds[$i]);
            }

            foreach ($questionIds as $id) {
                DB::table('answers')->where('id_question', $id->id)->delete();
            }

            $textIds = DB::table('english_text')->where('block_id', $block->id)->get(['id']);
            foreach ($textIds as $id) {
                $questionIds = DB::table('english_questions')->where('id_english_question', $id->id)->get(['id']);
                foreach ($questionIds as $idQ) {
                    DB::table('english_answers')->where('id_english_question', $idQ->id)->delete();
                }
                DB::table('english_questions')->where('block_id', $block->id)->delete();
                DB::table('english_questions')->where('id_english_question', $id->id)->delete();

            }

            $questionIds = DB::table('english_questions')->where('block_id', $block->id)->get(['id']);
            foreach ($questionIds as $idQ) {
                DB::table('english_answers')->where('id_english_question', $idQ->id)->delete();
            }
            DB::table('english_questions')->where('block_id', $block->id)->delete();

            DB::table('english_connections')->where('block_id', $block->id)->delete();

            DB::table('english_text')->where('block_id', $block->id)->delete();

            DB::table('english_config')->where('block_id', $block->id)->delete();

            DB::table('questions')->where('id_block', $block->id)->delete();

            DB::table('blocks')->delete($block->id);

        }

        $name_speciality = DB::table('specialities')->where('id', $id_specialisation_for_delete)->value('name_speciality');

        DB::table('specialities')->where('id', $id_specialisation_for_delete)->delete();

        $response = [
            'ok'
        ];
        HistoryHelper::add([
            'user_id' => Auth::user()->id,
            'icon' => 'trash',
            'class' => 'bg-maroon',
            'text' => 'trans("history.backend.speciality.deleted") <strong>'.$name_speciality.'</strong>'
        ]);
        return response()->json($response);
    }
    public function send_specialty(Request $req)
    {
        $specId = $req->specId;
        $block_ids = $req->ids;
        $block_counts = $req->counts;
        $commIds = $req->commId;
        $commNames = $req->commNames;
        $commPositions = $req->commPosition;
        $type = DB::table('specialities')->where('id',$specId)->value('type');
        $name_speciality = DB::table('specialities')->where('id',$specId)->value('name_speciality');

        DB::table('specialities')
            ->where('id', $specId)
            ->update(['status' => 'in_process', 'declined_reason' => null, 'updated_at' => Carbon::now()]);

        if($type != 1) {
            for ($i = 0; $i < count($block_ids); $i++) {
                DB::table('blocks')
                    ->where('id', $block_ids[$i])
                    ->update(['count_chosed_questions' => $block_counts[$i], 'updated_at' => Carbon::now()]);
            }
        }
        else {
            DB::table('blocks')
                ->where('id', $block_ids)
                ->update(['count_chosed_questions' => $block_counts, 'updated_at' => Carbon::now()]);
        }

        for ($i = 0; $i < count($commIds); $i++) {
            if($commIds[$i] != 0) {
                DB::table('commissioners')
                    ->where('id', $commIds[$i])
                    ->update(['commissioner_name' => $commNames[$i], 'commissioner_position' => $commPositions[$i], 'speciality_id' => $specId]);
            }
            else {
                DB::table('commissioners')->insert(['commissioner_name' => $commNames[$i], 'commissioner_position' => $commPositions[$i], 'speciality_id' => $specId]);
            }
        }
        $response = [
            'ok'
        ];

        // history

        HistoryHelper::add([
            'user_id' => Auth::user()->id,
            'icon' => 'plus',
            'class' => 'bg-green',
            'text' => 'trans("history.backend.verify.send_speciality") <strong>'.$name_speciality.'</strong>'
        ]);


        return response()->json($response);
    }

    public function edit_status_specialty(Request $req)
    {
        $specialty_id = $req->id;
        $specialty_status = $req->status;
        $name_speciality = DB::table('specialities')->where('id',$specialty_id)->value('name_speciality');
        if ($specialty_status == "declined") {
            $reason = $req->reason;
            $history_text = 'trans("history.backend.verify.decline_speciality") <strong>'.$name_speciality.'</strong>';
            $history_icon = 'refresh';
            $history_class = 'bg-maroon';
            DB::table('specialities')
                ->where('id', $specialty_id)
                ->update(['status' => $specialty_status, 'declined_reason' => $reason, 'updated_at' => Carbon::now()]);


        } else {

            $history_text = 'trans("history.backend.verify.approved_speciality") <strong>'.$name_speciality.'</strong>';
            $history_icon = 'thumbs-up';
            $history_class = 'bg-green';
            DB::table('specialities')
                ->where('id', $specialty_id)
                ->update(['status' => $specialty_status, 'declined_reason' => null, 'updated_at' => Carbon::now()]);
        }
        // history
        HistoryHelper::add([
            'user_id' => Auth::user()->id,
            'icon' => $history_icon,
            'class' => $history_class,
            'text' => $history_text
        ]);

        $response = [
            'ok'
        ];
        return response()->json($response);
    }

    public function get_specialties()
    {
        $response = DB::select('select * from specialities order by (
                                              case status
                                                when "approved" then 1
                                                when "in_progress" then 2
                                                when "declined" then 3
                                                end
                                                )  ');
        return $response;
    }


    function reverse_verification (Request $req){
        $name_speciality = DB::table('specialities')->where('id',$req->id)->value('name_speciality');

        DB::table('specialities')
            ->where('id', $req->id)
            ->update(['status' => 'in_development', 'updated_at' => Carbon::now()]);
        HistoryHelper::add([
            'user_id' => Auth::user()->id,
            'icon' => 'refresh',
            'class' => 'bg-maroon',
            'text' => 'trans("history.backend.verify.reverse_speciality") <strong>'. $name_speciality.'</strong>'
        ]);
    }
}


