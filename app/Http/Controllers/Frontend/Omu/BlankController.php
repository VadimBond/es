<?php

namespace App\Http\Controllers\Frontend\Omu;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\blocks;
use App\questions;
use App\answers;
use App\Helpers\HistoryHelper;

use Illuminate\Support\Facades\Storage;

use File;
use Hash;
use Auth;
use DB;

class BlankController extends Controller
{
    function getIdFromQuery($query) {
        $arr = [];
        foreach ($query as $item) {
            array_push($arr, $item->id);
        }
        return $arr;
    }

    function deleteElementImages($elementId, $typeElement, $images) {
		$filtedImages = array_filter($images, function ($img) {
			return(strpos($img, 'data:image') === false);
		});

		$whiteList = array_map(function($imgPath) {
			$pathToremove = '/img/';
			return str_replace($pathToremove, '', $imgPath);
		}, $filtedImages);

		$fileName = $typeElement.'-'.$elementId;
		$allFiles = Storage::disk('images')->files('frontend/taskImages/');
		$filesToDelete = [];

		// remove image from array which will delete imgs.
		$filteredAllImages = array_filter($allFiles, function($imgPath) use ($whiteList) {
			foreach($whiteList as $img) {
				if(strcmp($imgPath, $img) === 0) {
					return false;
				}
			}
			return true;
		});

		foreach ($filteredAllImages as $file) {
			if(strpos($file, $fileName) !== false) {
				array_push($filesToDelete, $file);
			}
		}

		Storage::disk('images')->delete($filesToDelete);
	}
	function saveImages ($elementId, $typeElement, $arrImages) {
		$urls = [];

		$fileName = $typeElement.'-'.$elementId;
		$allFiles = Storage::disk('images')->files('frontend/taskImages/');

		$filteredImages = array_filter($allFiles, function($imgPath) use ($fileName){
			if(strpos($imgPath, $fileName) !== false) {
				return true;
			}
			return false;
		});

		$imagesIndex = [];
		foreach ($filteredImages as $file) {
			$pathToRemove = 'frontend/taskImages/'.$fileName.'-';

			array_push($imagesIndex, intval(str_replace($pathToRemove, '', $file)));
		}

		$maxIndex = count($imagesIndex) > 0 ? max($imagesIndex) : 1;

		foreach ($arrImages as $index => $base64) {

			if (strpos($base64, 'data:image') !== false) {
				$convertedBase64 = str_replace(' ', '+', $base64);
				$image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $convertedBase64));

				$maxIndex += 1;
				$fileName = $typeElement . '-' . $elementId . '-' . $maxIndex;

				Storage::disk('images')->put('frontend/taskImages/' . $fileName, $image);

				$url = Storage::disk('images')->url('frontend/taskImages/' . $fileName);
			}
			else {
				$url = $base64;
			}
			array_push($urls, $url);
		}
		return $urls;
	}

	function insertUrlToImages($content, $urlArr)
	{
		$replacedContent = preg_replace_array('/URLPATH/', $urlArr, $content);
		return $replacedContent;
	}

    public function view_blank($id)
    {
        $english_answers = DB::table('english_answers')->get();
        $english_config = DB::table('english_config')->get();
        $english_questions = DB::table('english_questions')->orderByRaw("CASE WHEN right_answer = 1 THEN 0 ELSE right_answer END")->orderBy('id')->get();
        $english_text = DB::table('english_text')->get();
        $idSpeciality = $id;
        $blocks = blocks::all();
        $questions = questions::all();
        $answers = answers::all();
        $comm = DB::table('commissioners')->where('speciality_id',$id)->get();
        $type = DB::table('specialities')->where('id',$id)->value('type');

        $connectionQuery = DB::table('english_connections')->get(['id'])->toArray();

        $connectionIds = self::getIdFromQuery($connectionQuery);

        $connections = DB::table('english_connections')->get();

        $questionsConnection = DB::table('english_questions')->whereIn('english_connection_id', $connectionIds)->where('type','list')->get()->groupBy('english_connection_id')->toArray();
        $answersConnection = DB::table('english_questions')->whereIn('english_connection_id', $connectionIds)->where('type','answerList')->get()->groupBy('english_connection_id')->toArray();

        foreach ($connections as $connection) {
            $connectionId = $connection->id;

            if(isset($questionsConnection[$connectionId])) {
                $currQuestions = $questionsConnection[$connectionId];
                $connection->questions = $currQuestions;
            }
            else {
                $connection->questions = [];
            }
            if(isset($answersConnection[$connectionId])) {
                $currAnswers = $answersConnection[$connectionId];
                $connection->answers = $currAnswers;
            }
            else {
                $connection->answers = [];
            }
        }

        $data = [
            'english_answers' => $english_answers,
            'english_config' => $english_config,
            'english_questions' => $english_questions,
            'english_text' => $english_text,
            'idSpeciality' => $idSpeciality,
            'blocks' => $blocks,
            'questions' => $questions,
            'answers' => $answers,
            'comms' => $comm,
            'type' => $type,
            'connections' => $connections,
        ];

        return view('frontend.pages.view_blank', compact('data'));
    }

    public function edit_blank($id)
    {
        $idSpeciality = $id;
        $type = DB::table('specialities')->where('id',$id)->value('type');
	    $englishSpeciality = 2;
	    $pageToRender = $type != $englishSpeciality ?
		    'frontend.pages.edit_blank' :
		    'frontend.pages.edit_english_blank';
        $imagesWidthChoices = [50, 75, 100, 125, 150];

        $data = [];
        if($type !== $englishSpeciality) {
	        $blocks = blocks::all();
	        $questions = questions::all();
	        $answers = answers::all();

	        $data = [
		        'blocks' => $blocks,
		        'questions' => $questions,
		        'answers' => $answers,
		        'imagesWidthChoices' => $imagesWidthChoices,

		        'type' => $type,
	        ];
        }
        else {
            $blocks = DB::table('blocks')->where([['id_speciality', $idSpeciality], ['count_chosed_questions', '!=', 0]])->get()->toArray();
            $blocksId = self::getIdFromQuery($blocks);
            $settings = DB::table('english_config')->whereIn('block_id', $blocksId)->get(['text', 'answer_list', 'true_false', 'extra_answers'])->toArray();

            for ($i = 0; $i < sizeof($blocks); $i++) {
                $title = null;
                $questions = null;
                $answers = null;
                $connections = null;

                if ($settings[$i]->answer_list) {
                    if ($settings[$i]->text) {
                        $questions = DB::table('english_text')->where('block_id', $blocks[$i]->id)->get(['id', 'text', 'image_width'])->toArray();
                        for($j = 0; $j < sizeof($questions); $j++) {
                            $answers[$j] = DB::table('english_questions')->where('id_english_question', $questions[$j]->id)->get(['id', 'text_question', 'right_answer', 'position'])->toArray();
                        }
                    }
                    else {
                        $connections = DB::table('english_connections')->where('block_id', $blocks[$i]->id)->get(['id'])->toArray();

                        for($j = 0; $j < sizeof($connections); $j++) {
                            $questions[$j] = DB::table('english_questions')
                                ->where([['type', 'answerList'], ['english_connection_id', $connections[$j]->id]])
                                ->get(['id', 'text_question'])->toArray();
                            $answers[$j] = DB::table('english_questions')
                                ->where([['type', 'list'], ['english_connection_id', $connections[$j]->id]])
                                ->get(['id', 'text_question', 'position', 'right_answer'])->toArray();
                        }
                    }
                }
                else if ($settings[$i]->text) {
                    $title = DB::table('english_text')->where('block_id', $blocks[$i]->id)->get(['id', 'text', 'image_width'])->toArray();

                    for($j = 0; $j < sizeof($title); $j++) {
                        $questions[$j] = DB::table('english_questions')->where('id_english_question', $title[$j]->id)->get(['id', 'text_question', 'image_width'])->toArray();

                        for($k = 0; $k < sizeof($questions[$j]); $k++) {
                            $answers[$j][$k] = DB::table('english_answers')->where('id_english_question', $questions[$j][$k]->id)->get(['id', 'text_answer', 'answer_right'])->toArray();
                        }
                    }
                }
                else {
                    $questions = DB::table('english_questions')->where('block_id', $blocks[$i]->id)->get(['id', 'text_question', 'image_width'])->toArray();
                    $j = 0;
                    foreach ($questions as $one) {
                        $answers[$j++] = DB::table('english_answers')->where('id_english_question', $one->id)->get(['id', 'text_answer', 'answer_right'])->toArray();
                    }
                }

                $data['blocks'][$i] = [
                    'block_name' => $blocks[$i]->block_name,
                    'id' => $blocks[$i]->id,
                    'count' => $blocks[$i]->countAnswers,
                    'connections' => $connections,
                    'settings' => $settings[$i],
                    'block_title' => $blocks[$i]->task_title,
                    'title' => $title,
                    'questions' => $questions,
                    'answers' => $answers,
                ];
            }
        }
        $comm = DB::table('commissioners')->where('speciality_id',$id)->get();
	    $data['idSpeciality'] = $idSpeciality;
        $data['imagesWidthChoices'] = $imagesWidthChoices;
        $data['comms'] = $comm;

        return view($pageToRender, compact('data'));
    }

    public function update_blank(Request $req) {
        $elementId = $req->id;
        $contentData = $req->elementContent;
        $images = $req->images;
        $tableName = $req->tableName;

        if($images) {
            $typeImage = $req->typeImage;
            //delete all images element and replace them new
            self::deleteElementImages($elementId, $typeImage, $images);
            $urlImages = self::saveImages($elementId, $typeImage, $images);
            $contentData = self::insertUrlToImages($contentData, $urlImages);
        }

        $dataToUpdate = null;
        if($tableName === 'blocks') {
            $dataToUpdate = [
                'id' => $elementId,
                'block_name' => $contentData,
                'updated_at' => Carbon::now()
            ];
        }
        else if($tableName === 'questions') {
        	$imageWidth = $req->imageWidth;

            $dataToUpdate = [
                'id' => $elementId,
                'text_question' => $contentData,
	            'image_width' => $imageWidth,
                'updated_at' => Carbon::now()
            ];
        }
        else {
            $isChecked = $req->isChecked === 'true' ? 1 : 0;

            $dataToUpdate = [
                'id' => $elementId,
                'text_answer' => $contentData,
                'answer_right' => $isChecked,
                'updated_at' => Carbon::now()
            ];
        }

        try {
            DB::table($tableName)->where('id', $elementId)
                ->update($dataToUpdate);
        }
        catch(\Exception $err) {
            return response($err, 500);
        }
        $blankName = DB::table('specialities')->join('blocks','specialities.id', '=', 'blocks.id_speciality' )->where('blocks.id', $req->blockId)->value('name_speciality');

        HistoryHelper::add([
            'user_id' => Auth::user()->id,
            'icon' => 'save',
            'class' => 'bg-aqua',
            'text' => 'trans("history.backend.blanks.edit")  <strong>'.$blankName.'</strong>'
        ]);

        return response($req->imageWidth);
    }
    public function update_english_blank(Request $req) {
    	$typesTask = [
			'textList' => 'textList',
			'textQuestions' => 'textQuestions',
			'listAnswers' => 'listAnswers',
			'question' => 'question'
		];

    	$typeElements = $req->typeElements;

    	switch ($typeElements) {
			case $typesTask['textList']:
				$textId = $req->textId;
				$typeImage = 'English_Text';
				$imageWidth = $req->imageWidth;
				$textContent = $req->textContent;
				$contentData = $textContent['content'];
				$images = $textContent['images'];
				$listData = $req->listData;

				if($images) {
					deleteElementImages($textId, $typeImage, $images);
					$urlImages = saveImages($textId, $typeImage, $images);
					$contentData = insertUrlToImages($contentData, $urlImages);
				}

				DB::table('english_text')
					->where('id', $textId)
					->update([
						'text' => $contentData,
						'image_width' => $imageWidth,
						'updated_at' => Carbon::now()
					]);

				foreach($listData as $list) {

					DB::table('english_questions')
						->where('id', $list['id'])
						->update([
							'text_question' => $list['content'],
							'right_answer' => $list['rightAnswer'],
							'updated_at' => Carbon::now()
						]);
				}

				break;
			case $typesTask['textQuestions']:
				$textId = $req->textId;
				$textTypeImage = 'English_Text';
				$questionTypeImage = 'English_Question';
				$answerTypeImage = 'English_Answer';
				$imageWidth = $req->imageWidth;
				$textContent = $req->textContent;
				$textContentData = $textContent['content'];
				$textImages = $textContent['images'];
				$questionData = $req->questionData;

				if($textImages) {
					deleteElementImages($textId, $textTypeImage, $textImages);
					$urlImages = saveImages($textId, $textTypeImage, $textImages);
					$textContentData = insertUrlToImages($textContentData, $urlImages);
				}

				DB::table('english_text')
					->where('id', $textId)
					->update([
						'text' => $textContentData,
						'image_width' => $imageWidth,
						'updated_at' => Carbon::now()
					]);

				foreach($questionData as $question) {
					$id = $question['id'];
					$questionContent = $question['content'];
					$questionImages = $question['images'];
					if($questionImages) {
						deleteElementImages($id, $questionTypeImage, $questionImages);
						$urlImages = saveImages($id, $questionTypeImage, $questionImages);
						$questionContent = insertUrlToImages($questionContent, $urlImages);
					}

					DB::table('english_questions')
						->where('id', $id)
						->update([
							'text_question' => $questionContent,
							'image_width' => $question['imageWidth'],
							'updated_at' => Carbon::now()
						]);

					foreach ($question['answersData'] as $answer) {
						$id = $answer['id'];
						$answerContent = $answer['content'];
						$answerImages = $answer['images'];
						if($answerImages) {
							deleteElementImages($id, $answerTypeImage, $answerImages);
							$urlImages = saveImages($id, $answerTypeImage, $answerImages);
							$answerContent = insertUrlToImages($answerContent, $urlImages);
						}

						DB::table('english_answers')
							->where('id', $id)
							->update([
								'text_answer' => $answerContent,
								'answer_right' => $answer['isChecked'],
								'updated_at' => Carbon::now()
							]);
					}
				}

				break;
			case $typesTask['listAnswers']:
				$listAnswerData = $req->listAnswersData;
				$listData = $req->listData;


				foreach ($listAnswerData as $answerList) {
					DB::table('english_questions')
						->where('id', $answerList['id'])
						->update([
							'text_question' => $answerList['content'],
							'updated_at' => Carbon::now()
						]);
				}

				foreach ($listData as $list) {
					DB::table('english_questions')
						->where('id', $list['id'])
						->update([
							'text_question' => $list['content'],
							'right_answer' => $list['rightAnswer'],
							'updated_at' => Carbon::now()
						]);
				}

				break;
			case $typesTask['question']:
				$question = $req->question;
				$questionTypeImage = 'English_Question';
				$answerTypeImage = 'English_Answer';


				$id = $question['id'];
				$questionContent = $question['content'];
				$questionImages = $question['images'];
				if($questionImages) {
					deleteElementImages($id, $questionTypeImage, $questionImages);
					$urlImages = saveImages($id, $questionTypeImage, $questionImages);
					$questionContent = insertUrlToImages($questionContent, $urlImages);
				}


				DB::table('english_questions')
					->where('id', $id)
					->update([
						'text_question' => $questionContent,
						'image_width' => $question['imageWidth'],
						'updated_at' => Carbon::now()
					]);

				foreach ($question['answersData'] as $answer) {
					$id = $answer['id'];
					$answerContent = $answer['content'];
					$answerImages = $answer['images'];

					if($answerImages) {
						deleteElementImages($id, $answerTypeImage, $answerImages);
						$urlImages = saveImages($id, $answerTypeImage, $answerImages);
						$answerContent = insertUrlToImages($answerContent, $urlImages);
					}

					DB::table('english_answers')
						->where('id', $id)
						->update([
							'text_answer' => $answerContent,
							'answer_right' => $answer['isChecked'],
							'updated_at' => Carbon::now()
						]);
				}
				break;
			default:
				return response('Undeclared type element', 404);
		}

        $blankName = DB::table('specialities')->where('id', $req->idSpeciality)->value('name_speciality');

        HistoryHelper::add([
            'user_id' => Auth::user()->id,
            'icon' => 'save',
            'class' => 'bg-aqua',
            'text' => 'trans("history.backend.blanks.edit")  <strong>'.$blankName.'</strong>'
        ]);

    	return response(200, 200);
	}

}