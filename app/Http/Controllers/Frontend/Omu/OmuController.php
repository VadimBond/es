<?php

namespace App\Http\Controllers\Frontend\Omu;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\blocks;
use App\questions;
use App\answers;

use Illuminate\Support\Facades\Storage;

use File;
use Hash;
use Auth;
use DB;




class OmuController extends Controller
{
	function deleteElementImages($elementId, $typeElement, $images) {
		$filtedImages = array_filter($images, function ($img) {
			return(strpos($img, 'data:image') === false);
		});

		$whiteList = array_map(function($imgPath) {
			$pathToremove = '/img/';
			return str_replace($pathToremove, '', $imgPath);
		}, $filtedImages);

		$fileName = $typeElement.'-'.$elementId;
		$allFiles = Storage::disk('images')->files('frontend/taskImages/');
		$filesToDelete = [];

		// remove image from array which will delete imgs.
		$filteredAllImages = array_filter($allFiles, function($imgPath) use ($whiteList){
			foreach($whiteList as $img) {
				if(strcmp($imgPath, $img) === 0) {
					return false;
				}
			}
			return true;
		});

		foreach ($filteredAllImages as $file) {
			if(strpos($file, $fileName) !== false) {

				array_push($filesToDelete, $file);
			}
		}

		Storage::disk('images')->delete($filesToDelete);
	}

	function saveImages ($elementId, $typeElement, $arrImages) {
		$urls = [];

		$fileName = $typeElement.'-'.$elementId;
		$allFiles = Storage::disk('images')->files('frontend/taskImages/');

		$filteredImages = array_filter($allFiles, function($imgPath) use ($fileName) {
			if(strpos($imgPath, $fileName) !== false) {
				return true;
			}
			return false;
		});

		$imagesIndex = [];
		foreach ($filteredImages as $file) {
			$pathToRemove = 'frontend/taskImages/'.$fileName.'-';

			array_push($imagesIndex, intval(str_replace($pathToRemove, '', $file)));
		}

		$maxIndex = count($imagesIndex) > 0 ? max($imagesIndex) : 1;

		foreach ($arrImages as $index => $base64) {
			if (strpos($base64, 'data:image') !== false) {
				$convertedBase64 = str_replace(' ', '+', $base64);
				$image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $convertedBase64));

				$maxIndex += 1;
				$fileName = $typeElement . '-' . $elementId . '-' . $maxIndex;

				Storage::disk('images')->put('frontend/taskImages/' . $fileName, $image);

				$url = Storage::disk('images')->url('frontend/taskImages/' . $fileName);


			}
			else {
				$url = $base64;
			}
			array_push($urls, $url);
		}
		return $urls;
	}

	function insertUrlToImages($content, $urlArr)
	{
		$replacedContent = preg_replace_array('/URLPATH/', $urlArr, $content);
		return $replacedContent;
	}

    public function index()
    {
        if (Auth::user()) {
            $userId = Auth::user()->id;
            $userRoleId = DB::table('role_user')->where('user_id', $userId)->value('role_id');

            if ($userRoleId === 2) {

                $users = DB::select('select users.id id,roles.name,email,first_name,last_name,name_cafedres 
                                    from users 
                                      join role_user 
                                      on(users.id = role_user.user_id) 
                                      join roles
                                      on(role_user.role_id = roles.id)
                                      left join cafedres 
                                      on(users.id_cafedres = cafedres.id)
                                    where role_id in (1,2,3)');
                $specialities = DB::select('select specialities.id,declined_reason,name_speciality,name_cafedres,id_cafedres,status,specialities.updated_at
                                            from specialities 
                                              join cafedres 
                                              on(specialities.id_cafedres = cafedres.id) 
                                              order by (
                                              case status
                                                when "approved" then 1
                                                when "in_progress" then 2
                                                when "declined" then 3
                                                end
                                                )  ');


                $cafedres = DB::select('select id,name_cafedres from cafedres');
                $blocks = blocks::all();
                $questions = questions::all();
                $answers = answers::all();
                $firstDep = $dep = DB::table('cafedres')->value('id');


                $data = [
                    'user_role' => $userRoleId,
                    'cafedres' => $cafedres,
                    'users' => $users,
                    'specialities' => $specialities,
                    'blocks' => $blocks,
                    'questions' => $questions,
                    'answers' => $answers,
                    'first' => $firstDep,
                ];

                return view('frontend.pages.omu', compact('data'));
            }

        }
        return view('errors.404');
    }

    public function get_data_for_table()
    {
        $userId = Auth::user()->id;
        $users = DB::select('select users.id id,roles.name,email,first_name,last_name,name_cafedres
                                    from users 
                                      join role_user 
                                      on(users.id = role_user.user_id) 
                                      join roles
                                      on(role_user.role_id = roles.id)
                                      left join cafedres 
                                      on(users.id_cafedres = cafedres.id)
                                    where role_id in (1,2,3)');

        $response = [
            'users' => $users,
            'id' => $userId,
        ];
        return $response;
    }

    public function get_speciality_department()
    {
        $specialities = DB::select('select specialities.id,name_speciality,name_cafedres
                                            from specialities 
                                              join cafedres 
                                              on(specialities.id_cafedres = cafedres.id) 
                                              where status = "approved"  ');

        return $specialities;
    }

	public function updateCommissioners(Request $req) {
		$specialityId = $req->idSpeciality;
		$commissioners = $req->commissioners;

		// clean old user
		DB::table('commissioners')
			->where('speciality_id', $specialityId)
			->delete();


		foreach($commissioners as $commissioner) {
			if($commissioner['initials'] !== null && $commissioner['position'] !== null) {

                DB::table('commissioners')
                    ->insert([
                        'commissioner_name' => $commissioner['initials'],
                        'commissioner_position' => $commissioner['position'],
                        'speciality_id' => $specialityId
                    ]);
			}
		}

		return response('Ok');
	}
}
