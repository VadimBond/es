<?php

namespace App\Http\Controllers\Frontend\Omu;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\HistoryHelper;
use Carbon\Carbon;
use App\blocks;
use App\questions;
use App\answers;

use Illuminate\Support\Facades\Storage;

use File;
use Hash;
use Auth;
use DB;




class DepartmentController extends Controller
{
    public function edit_department($id)
    {
        $dep = DB::table('cafedres')->where("id", $id)->get()[0];
        $data = [
            'id' => $id,
            'department' => $dep->name_cafedres,
            'address' => $dep->address,
            'phone' => $dep->contact_phones,
            'site' => $dep->website,
        ];

        return view('frontend.pages.edit_department', compact('data'));
    }

    public function new_department()
    {
        $data = [
            'id' => "",
            'department' => "",
            'address' => "",
            'phone' => "",
            'site' => "",
        ];

        return view('frontend.pages.edit_department', compact('data'));
    }

    public function add_department(Request $req)
    {
        if ($req->department_name && $req->address && $req->phone) {
            $department_name = $req->department_name;
            $department_address = $req->address;
            $department_phone = $req->phone;
            $department_site = "";

            if ($req->site) {
                $department_site = $req->site;
            }

            if(strlen($department_name) < 191 && strlen($department_address) < 501 && strlen($department_phone) < 21) {
                $new_department = array('name_cafedres' => $department_name, 'address' => $department_address, 'contact_phones' => $department_phone, 'website' => $department_site, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now());
                DB::table('cafedres')->insert($new_department);
                $response = [
                    'ok'
                ];
                HistoryHelper::add([
                    'user_id' => Auth::user()->id,
                    'icon' => 'plus',
                    'class' => 'bg-green',
                    'text' => 'trans("history.backend.department.created") <strong>'.$department_name.'</strong>'
                ]);
                return response()->json($response);
            }
        }

        $response = [
            'error'
        ];

        return response()->json($response);
    }

    public function update_department(Request $req)
    {
        if ($req->department_name && $req->address && $req->phone) {
            $department_name = $req->department_name;
            $department_id = $req->id;
            $department_address = $req->address;
            $department_phone = $req->phone;
            $department_site = "";

            if ($req->site) {
                $department_site = $req->site;
            }

            if(strlen($department_name) < 191 && strlen($department_address) < 501 && strlen($department_phone) < 21) {
                DB::table('cafedres')
                    ->where('id', $department_id)
                    ->update(['name_cafedres' => $department_name, 'address' => $department_address, 'contact_phones' => $department_phone, 'website' => $department_site, 'updated_at' => Carbon::now()]);
                $response = [
                    'ok'
                ];
                HistoryHelper::add([
                    'user_id' => Auth::user()->id,
                    'icon' => 'save',
                    'class' => 'bg-aqua',
                    'text' => 'trans("history.backend.department.updated") <strong>'.$department_name.'</strong>'
                ]);
                return response()->json($response);
            }
        }

        $response = [
            'error'
        ];

        return response()->json($response);
    }

    function deleteImgFromQuestions($idQuestion){
        // delete img

        $idAnswers = DB::table('answers')->where('id_question', $idQuestion)->pluck('id');


        $typeQ = 'Question';
        $typeA = 'Answer';
        $pathToImg = 'frontend/taskImages/';
        $fileNameQ = $pathToImg.$typeQ.'-'.$idQuestion;
        $fileNameA = [];
        for ($i = 0; $i < count($idAnswers); $i++){
            array_push($fileNameA, $pathToImg.$typeA.'-'.$idAnswers[$i]);
        }

        $allFiles = Storage::disk('images')->files('frontend/taskImages/');
        $filesToDelete = [];

        foreach ($allFiles as $file) {
            if(strpos($file, $fileNameQ) !== false   ) {

                array_push($filesToDelete, $file);
            }
        }
        for ($i = 0; $i < count($idAnswers); $i++) {
            foreach ($allFiles as $file) {
                if (strpos($file, $fileNameA[$i]) !== false) {

                    array_push($filesToDelete, $file);
                }
            }
        }

        for ($i=0; $i < count($filesToDelete) ; $i++) {
            Storage::disk('images')->delete($filesToDelete[$i]);
        }
    }

    public function delete_department(Request $req)
    {
        $id_department_for_delete = $req->id;
        DB::table('users')->where('id_cafedres', $id_department_for_delete)->delete();

        $specs = DB::table('specialities')->where('id_cafedres',$id_department_for_delete)->get();
        foreach ($specs as $spec){
            $blocks = DB::table('blocks')->where('id_speciality',$spec->id)->get();
            foreach($blocks as $block){

                $questionIds = DB::table('questions')->where('id_block', $block->id)->get(['id']);

                $questIds = DB::table('questions')->where('id_block', $block->id)->pluck('id');

                for ($i=0; $i < count($questIds) ; $i++) {
                    self::deleteImgFromQuestions($questIds[$i]);
                }

                foreach ($questionIds as $id) {
                    DB::table('answers')->where('id_question', $id->id)->delete();
                }

                $textIds = DB::table('english_text')->where('block_id', $block->id)->get(['id']);
                foreach ($textIds as $id) {
                    $questionIds = DB::table('english_questions')->where('id_english_question', $id->id)->get(['id']);
                    foreach ($questionIds as $idQ) {
                        DB::table('english_answers')->where('id_english_question', $idQ->id)->delete();
                    }
                    DB::table('english_questions')->where('block_id', $block->id)->delete();
                    DB::table('english_questions')->where('id_english_question', $id->id)->delete();

                }

                $questionIds = DB::table('english_questions')->where('block_id', $block->id)->get(['id']);
                foreach ($questionIds as $idQ) {
                    DB::table('english_answers')->where('id_english_question', $idQ->id)->delete();
                }
                DB::table('english_questions')->where('block_id', $block->id)->delete();

                DB::table('english_connections')->where('block_id', $block->id)->delete();

                DB::table('english_text')->where('block_id', $block->id)->delete();

                DB::table('english_config')->where('block_id', $block->id)->delete();

                DB::table('questions')->where('id_block', $block->id)->delete();

                DB::table('blocks')->delete($block->id);

            }
        }
        DB::table('specialities')->where('id_cafedres',$id_department_for_delete)->delete();

        $department_name = DB::table('cafedres')->where('id', $id_department_for_delete)->value('name_cafedres');
        HistoryHelper::add([
            'user_id' => Auth::user()->id,
            'icon' => 'trash',
            'class' => 'bg-maroon',
            'text' => 'trans("history.backend.department.deleted") <strong>'.$department_name.'</strong>'
        ]);

        DB::table('cafedres')->where('id', $id_department_for_delete)->delete();

        return 'ok';
    }

    public function get_cafedres()
    {
        $response = DB::select('select id, name_cafedres from cafedres ');
        return $response;
    }
}