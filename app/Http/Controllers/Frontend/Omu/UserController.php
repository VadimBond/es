<?php

namespace App\Http\Controllers\Frontend\Omu;

use App\Http\Controllers\Controller;
use App\Helpers\HistoryHelper;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\blocks;
use App\questions;
use App\answers;

use Illuminate\Support\Facades\Storage;

use File;
use Hash;
use Auth;
use DB;




class UserController extends Controller
{
    public function edit_representative($id)
    {
        $firstName = "";
        $secondName = "";
        $email = "";
        $pass = "";
        $department = "";
        $roleId = "";
        $role = "";
        $roles = DB::select('select * from roles where id in (1,2,3)');
        $roleUsers = DB::select('select * from role_user ');
        $users = DB::select('select * from users ');
        $cafedres = DB::select('select id,name_cafedres from cafedres');

        foreach ($users as $user) {
            if ($user->id == $id) {
                $firstName = $user->first_name;
                $secondName = $user->last_name;
                $email = $user->email;
                $pass = $user->password;
                $department = $user->id_cafedres;
            }
        }

        foreach ($roleUsers as $rol) {
            if ($rol->user_id == $id) {
                $roleId = $rol->role_id;
            }
        }

        foreach ($roles as $rola) {
            if ($rola->id == $roleId) {
                $role = $rola->name;
            }
        }

        $data = [
            'id' => $id,
            'firstName' => $firstName,
            'secondName' => $secondName,
            'email' => $email,
            'cafedres' => $cafedres,
            'pass' => $pass,
            'dep' => $department,
            'roleId' => $roleId,
            'roles' => $roles,
            'role' => $role,
        ];
        return view('frontend.pages.edit_representative', compact('data'));
    }

    public function add_representative(Request $req)
    {
        $userId = Auth::user()->id;
        $firstName = $req->firstName;
        $secondName = $req->secondName;
        $email = $req->email;
        $roleId = $req->role;
        $dep = $req->dep;
        $pass = $req->pass;

        $new_user = array(
            'first_name' => $firstName,
            'last_name' => $secondName,
            'email' => $email,
            'password' => Hash::make($pass),
            'status' => 1,
            'confirmed' => 1,
            'created_by' => $userId,
            'updated_by' => $userId,
            'updated_at' => Carbon::now(),
            'created_at' => Carbon::now(),
            'id_cafedres' => $dep,
        );

        $user_id = DB::table('users')->insertGetId($new_user);

        $new_role_user = array('role_id' => $roleId, 'user_id' => $user_id);
        if(DB::table('role_user')->insert($new_role_user)){
            HistoryHelper::add([
                'user_id' => Auth::user()->id,
                'icon' => 'plus',
                'class' => 'bg-green',
                'text' => 'trans("history.backend.users.created") <strong>'.$firstName.' '.$secondName.'</strong>'
            ]);
        }

        $permissions = "";
        if ($roleId == 2) {
            $permissions = array('Show User Details', 'View-omu');
        }

        if ($roleId == 3) {
            $permissions = array('View Frontend');
        }

        if ($roleId != 1) {
            for ($i = 0; $i < sizeof($permissions); $i++) {
                DB::table('permission_user')->where(['user_id' => $user_id])->delete();
                $verId = DB::table('permissions')->where('display_name', $permissions[$i])->value('id');
                DB::table('permission_user')->insert(['permission_id' => $verId, 'user_id' => $user_id]);
            }
        }

        $response = [
            'ok'
        ];
        return response()->json($response);
    }

    public function update_representative(Request $req)
    {
        $updater = Auth::user()->id;
        $firstName = $req->firstName;
        $secondName = $req->secondName;
        $email = $req->email;
        $last = $req->lastEmail;
        $roleId = $req->role;
        $dep = $req->dep;
        $user_id = $req->id;
        if ($req->pass) {
            $pass = Hash::make($req->pass);

            DB::table('users')
                ->where('id', $user_id)
                ->update(['password' => $pass, 'updated_by' => $updater, 'updated_at' => Carbon::now()]);
        }

        if ($last != null) {
            DB::table('users')
                ->where('id', $user_id)
                ->update(['email' => $email, 'updated_by' => $updater, 'updated_at' => Carbon::now()]);
        }

        DB::table('users')
            ->where('id', $user_id)
            ->update(['id_cafedres' => $dep, 'first_name' => $firstName, 'last_name' => $secondName, 'updated_by' => $updater, 'updated_at' => Carbon::now()]);


        HistoryHelper::add([
            'user_id' => Auth::user()->id,
            'icon' => 'save',
            'class' => 'bg-aqua',
            'text' => 'trans("history.backend.users.updated") <strong>'.$firstName.' '.$secondName.'</strong>'
        ]);


        DB::table('role_user')
            ->where('user_id', $user_id)
            ->update(['role_id' => $roleId]);

        $permissions = "";
        if ($roleId == 2) {
            $permissions = array('Show User Details', 'View-omu');
        }

        if ($roleId == 3) {
            $permissions = array('View Frontend');
        }

        if ($roleId != 1) {
            for ($i = 0; $i < sizeof($permissions); $i++) {
                DB::table('permission_user')->where(['user_id' => $user_id])->delete();
                $verId = DB::table('permissions')->where('display_name', $permissions[$i])->value('id');
                DB::table('permission_user')->insert(['permission_id' => $verId, 'user_id' => $user_id]);
            }
        }

        $response = [
            'ok'
        ];
        return response()->json($response);
    }

    public function delete_representative(Request $req)
    {
        $id_representative_for_delete = $req->id;
        $name_deleted_user = DB::table('users')->where('id', $id_representative_for_delete)->select('first_name', 'last_name')->first();
        $name_deleted_user = $name_deleted_user->first_name.' '.$name_deleted_user->last_name;

        if(DB::table('users')->where('id', $id_representative_for_delete)->delete()){
            HistoryHelper::add([
                'user_id' => Auth::user()->id,
                'icon' => 'trash',
                'class' => 'bg-maroon',
                'text' => 'trans("history.backend.users.deleted") <strong>'.$name_deleted_user.'</strong>'
            ]);
        }
        $response = [
            'ok'
        ];
        return response()->json($response);
    }
}