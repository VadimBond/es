<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\User\DashboardViewRequest;

use Illuminate\Http\Request;

use App\blocks;
use App\questions;
use App\answers;
use Carbon\Carbon;

use DB;
use Auth;
/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(DashboardViewRequest $request) {

        if(Auth::user()) {
            $userId = Auth::user()->id;
            $userRoleId = DB::table('role_user')->where('user_id', $userId)->value('role_id');

            if($userRoleId === 3) {
                $id_cafedres = Auth::user()->id_cafedres;

                $blocks=blocks::all();
                $questions=questions::all();
                $answers=answers::all();

                $cafedres=DB::select('select id,name_cafedres from cafedres where id = :my_id', ['my_id' => $id_cafedres]);

                $name_specialitys=DB::select('select * from specialities where id_cafedres = :my_id', ['my_id' => $id_cafedres]);

                $specialityId = $name_specialitys[0]->id;
                $currSpecialityData = DB::table('specialities')->where('id', $specialityId)->get(['status', 'declined_reason','type']);

                $curr_blocks = DB::table('blocks')->where('id_speciality', $specialityId)->get(['id', 'block_name','count_q_this_block', 'id_speciality','countAnswers'])->toArray();

                $data = [
                    'cafedres'  => $cafedres,
                    'name_specialitys' => $name_specialitys,
                    'blocks'  => $blocks,
                    'current_speciality' => $currSpecialityData[0],
                    'current_blocks' => $curr_blocks,
                    'questions'  => $questions,
                    'answers'  => $answers,
                    'speciality_id' => $specialityId,
                ];

                return view('frontend.pages.representative', compact('data'));
            }
        }

        return view('errors.404');
    }

    public function get_status() {
        $speciality=DB::select('select * from specialities');
        $block=DB::select('select * from blocks');
        return $response = ['speciality' => $speciality, 'block' => $block];
    }

    function updateSelect(Request $req) {
        $specialityId = $req->id_speciality;

        $blocks = DB::table('blocks')->where('id_speciality', $specialityId)->get(['id as blockId', 'block_name as blockName', 'id_speciality as idSpeciality', 'countAnswers'])->toArray();
        $status = DB::table('specialities')->where('id', $specialityId)->value('status');
        $response = [
            'blocks' => $blocks,
            'blank_status' => $status
        ];

        $response['status'] = 'updated';
        return response()->json($response, 200);

    }

}
