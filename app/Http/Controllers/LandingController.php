<?php

namespace App\Http\Controllers;
use App\Models\Settings\Setting;

use Auth;

/**
 * Class FrontendController.
 */
class LandingController extends Controller
{
    public function index()
    {
        $settingData = Setting::first();
        $google_analytics = $settingData->google_analytics;

        return view('frontend.landing', compact('google_analytics', $google_analytics));
    }
}
