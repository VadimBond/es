<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
//use phpDocumentor\Reflection\Types\Null;

class ManageEnglishTasksController extends Controller {
    function saveImages ($elementId, $typeElement, $arrImages) {
        $urls = [];

        $fileName = $typeElement.'-'.$elementId;
        $allFiles = Storage::disk('images')->files('frontend/taskImages/');

        $filteredImages = array_filter($allFiles, function($imgPath) use ($fileName) {
            if(strpos($imgPath, $fileName) !== false) {
                return true;
            }
            return false;
        });

        $imagesIndex = [];
        foreach ($filteredImages as $file) {
            $pathToRemove = 'frontend/taskImages/'.$fileName.'-';

            array_push($imagesIndex, intval(str_replace($pathToRemove, '', $file)));
        }

        $maxIndex = count($imagesIndex) > 0 ? max($imagesIndex) : 0;

        foreach ($arrImages as $index => $base64) {

            if (strpos($base64, 'data:image') !== false) {
                $convertedBase64 = str_replace(' ', '+', $base64);
                $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $convertedBase64));

                $fileName = $typeElement . '-' . $elementId . '-' . ++$maxIndex;

                Storage::disk('images')->put('frontend/taskImages/' . $fileName, $image);

                $url = Storage::disk('images')->url('frontend/taskImages/' . $fileName);
            }
            else {
                $url = $base64;
            }
            array_push($urls, $url);
        }

        return $urls;
    }
    function insertUrlToImages($content, $urlArr) {
        $replacedContent = preg_replace_array('/URLPATH/', $urlArr, $content);
        return $replacedContent;
    }

	function createEnglishBlock(Request $req) {
		$title = $req->title;
		$specialityId = $req->specialityId;
		$blockName = $req->blockName;
		$weightTask = $req->weightTask;
		$nmbAnswers = $req->nmbAnswers;
		$spareAnswers = $req->spareAnswers;
		$checkText = $req->checkText;
		$checkAnswers = $req->checkAnswers;
		$checkTF = $req->checkTF;

        if($checkText != 0 || ($checkText == 0 && $checkAnswers == 1)) {
            $newFieldData = [
                'type' => 2,
                'task_title' => $title,
                'block_name' => $blockName,
                'weight_question' => $weightTask,
                'id_speciality' => $specialityId,
                'countAnswers' => $nmbAnswers,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        }
        else {
            $newFieldData = [
                'type' => 0,
                'task_title' => $title,
                'block_name' => $blockName,
                'weight_question' => $weightTask,
                'id_speciality' => $specialityId,
                'countAnswers' => $nmbAnswers,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        }

		$idBlock = DB::table('blocks')->insertGetId($newFieldData);

		$newConf = [
			'block_id' => $idBlock,
			'text' => $checkText,
			'answer_list' => $checkAnswers,
			'true_false' => $checkTF,
			'extra_answers' => $spareAnswers,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now()
		];

		DB::table('english_config')->insert($newConf);

		$response = [
			'idBlock' => $idBlock,
			'idSpeciality' => $specialityId
		];

		return response()->json($response);
	}

	function createEnglishQuestion(Request $req) {
        $idBlock = $req->idBlock;
		$textData = $req->textData;
		$listAnswerData = $req->listAnswerData;
		$listData = $req->listData;
		$questionsData = $req->questions;

		$contentQuestionWithUrl = null;
		$questionTextId = null;
		if($textData) {
			$questionTextId = DB::table('english_text')->insertGetId([
                'block_id' => $idBlock,
				'image_width' => $textData['imageWidth']
			]);

			if($textData['images'] && count($textData['images']) > 0) {
				$urlImages = saveImages($questionTextId, 'English_Text', $textData['images']);

				$contentQuestionWithUrl = self::insertUrlToImages($textData['content'], $urlImages);
			}
			$contentQuestionWithUrl = $contentQuestionWithUrl ? $contentQuestionWithUrl : $textData['content'];

			DB::table('english_text')->where('id', $questionTextId)
				->update([
					'text' => $contentQuestionWithUrl
				]);
		}

		$connectionId = null;
		if(isset($listAnswerData)) {
			$connectionId = DB::table('english_connections')->insertGetId([
				'block_id' => $idBlock
			]);
			foreach($listAnswerData as $row) {
				$dataToInsert = [
					'text_question' => $row['textData'],
					'right_answer' => $row['indexLetter'],
					'type' => 'answerList',
					'english_connection_id' => $connectionId,
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now()
				];

				DB::table('english_questions')->insert($dataToInsert);
			}
		}

		if(isset($listData)) {
			$dataToInsert = [];
			if(isset($listAnswerData)) {
				$dataToInsert['english_connection_id'] = $connectionId;
			}
			else {
				$dataToInsert['id_english_question'] = $questionTextId ;
			}

			foreach($listData as $row) {
				$dataToInsert['text_question'] = $row['textCol'];

				if(isset($row['selectCol'])) {
					$dataToInsert['right_answer'] = $row['selectCol']['indexLetter'];
					$dataToInsert['position'] = $row['selectCol']['position'];
				}
				else {
					$dataToInsert['right_answer'] = $row['boolCol']['indexLetter'];
					$dataToInsert['position'] = $row['boolCol']['isChecked'];
				}

				DB::table('english_questions')->insert($dataToInsert);
			}
		}
		else {
			foreach ($questionsData as $questionData) {

				$questionDataToInsert = [
					'image_width' => $questionData['imageWidth'],
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now()
				];

				if(isset($textData)) {
				    $questionDataToInsert['id_english_question'] = $questionTextId;
                }
				else {
                    $questionDataToInsert['block_id'] = $idBlock;
                }

                $questionId = DB::table('english_questions')
                    ->insertGetId($questionDataToInsert);

				$contentQuestionWithUrl = null;
				if($questionData['question'][1] && count($questionData['question'][1]) > 0) {
					$urlImages = self::saveImages($questionId, 'English_Question', $questionData['question'][1]);
					$contentQuestionWithUrl = self::insertUrlToImages($questionData['question'][0], $urlImages);
				}

				$contentQuestionWithUrl = $contentQuestionWithUrl ? $contentQuestionWithUrl : $questionData['question'][0];

				DB::table('english_questions')
					->where('id', $questionId)
					->update([
						'text_question' => $contentQuestionWithUrl
					]);

				if(isset($questionData['answers'])) {
					$answersData = $questionData['answers'];
					foreach ($answersData as $answerData) {
						$answerDataToInsert = [
							'answer_right' => $answerData['isChecked'],
							'id_english_question' => $questionId,
							'created_at' => Carbon::now(),
							'updated_at' => Carbon::now()
						];

						$answerId = DB::table('english_answers')
							->insertGetId($answerDataToInsert);


						$contentAnswerWithUrl = null;

						if($answerData['answer'][1] && count($answerData['answer'][1]) > 0) {
							$urlImages = self::saveImages($answerId, 'English_Answer', $answerData['answer'][1]);

							$contentAnswerWithUrl = self::insertUrlToImages($answerData['answer'][0], $urlImages);
						}
						$contentAnswerWithUrl = $contentAnswerWithUrl ? $contentAnswerWithUrl : $answerData['answer'][0];

						DB::table('english_answers')
							->where('id', $answerId)
							->update([
								'text_answer' => $contentAnswerWithUrl
							]);
					}
				}
				else {
					DB::table('english_answers')->insert([
						'answer_right' => $questionData['isCorrect'],
						'id_english_question' => $questionId,
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
				}
			}
		}

		return response(200, 200);
	}
}