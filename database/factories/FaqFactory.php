<?php

use App\Models\Faqs\Faq;
use Faker\Generator as Faker;

$factory->define(Faq::class, function (Faker $faker) {
    return [
        'questions'  => rtrim($faker->sentence, '.').'?',
        'answers'    => $faker->paragraph,
        'status'    => $faker->numberBetween(0, 1),
    ];
});
