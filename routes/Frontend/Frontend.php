<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */

Route::post('/get/states', 'FrontendController@getStates')->name('get.states');
Route::post('/get/cities', 'FrontendController@getCities')->name('get.cities');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */


Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        /*
         * User Dashboard Specific
         */
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        /*
         * User Account Specific
         */
        Route::get('account', 'AccountController@index')->name('account');


        Route::post('/get_status', 'DashboardController@get_status')->name('get_status');
        Route::post('/send_selector','DashboardController@updateSelect');

        /*
         * User Profile Specific
         */
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');

        /*
         * User Profile Picture
         */
        Route::patch('profile-picture/update', 'ProfileController@updateProfilePicture')->name('profile-picture.update');
    });

    Route::group(['namespace' => 'Omu', 'as' => 'omu.'], function () {
        Route::get('/omu', 'OmuController@index')->name('index')->middleware('omu');
        Route::post('/get_data_for_table', 'OmuController@get_data_for_table')->name('get_data_for_table');
        Route::post('/get_speciality_department', 'OmuController@get_speciality_department')->name('get_speciality_department');
        Route::post('/update-commissioners', 'OmuController@updateCommissioners')->name('update-commissioners');

        Route::get('/edit-department/{id}', 'DepartmentController@edit_department')->name('edit_department')->middleware('omu');
        Route::get('/new-department', 'DepartmentController@new_department')->name('new_department')->middleware('omu');
        Route::post('/add_department', 'DepartmentController@add_department')->name('add_department');
        Route::post('/delete_department', 'DepartmentController@delete_department')->name('delete_department');
        Route::post('/update_department', 'DepartmentController@update_department')->name('update_department');
        Route::post('/get_cafedres', 'DepartmentController@get_cafedres')->name('get_cafedres');

        Route::get('/edit-speciality/{id}', 'SpecialityController@edit_speciality')->name('edit_speciality')->middleware('omu');
        Route::get('/add-speciality/{id}', 'SpecialityController@add_speciality')->name('add_speciality')->middleware('omu');
        Route::post('/add_specialisation', 'SpecialityController@add_specialisation')->name('add_specialisation');
        Route::post('/delete_specialisation', 'SpecialityController@delete_specialisation')->name('delete_specialisation');
        Route::post('/update_specialisation', 'SpecialityController@update_specialisation')->name('update_specialisation');
        Route::post('/send_specialty', 'SpecialityController@send_specialty')->name('send_specialty');
        Route::post('/edit_status_specialty', 'SpecialityController@edit_status_specialty')->name('edit_status_specialty');
        Route::post('/get_specialties', 'SpecialityController@get_specialties')->name('get_specialties');
        Route::post('/reverse_verification', 'SpecialityController@reverse_verification')->name('reverse_verification');

        Route::get('/edit-representative/{id}', 'UserController@edit_representative')->name('edit_representative')->middleware('omu');
        Route::post('/add_representative', 'UserController@add_representative')->name('add_representative');
        Route::post('/delete_representative', 'UserController@delete_representative')->name('delete_representative');
        Route::post('/update_representative', 'UserController@update_representative')->name('update_representative');

        Route::get('/view-blank/{id}', 'BlankController@view_blank')->name('view_blank')->middleware('omu');
        Route::get('/edit-blank/{id}', 'BlankController@edit_blank')->name('edit_blank')->middleware('omu');
		Route::post('/update-blank', 'BlankController@update_blank')->name('update_blank');
		Route::post('/update-english-blank', 'BlankController@update_english_blank')->name('update_english_blank');
    });

});
