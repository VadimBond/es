<?php

/**
 * Global Routes
 * Routes that are used between both frontend and backend.
 */

// Switch between the included languages
Route::get('lang/{lang}', 'LanguageController@swap');

/* ----------------------------------------------------------------------- */

/*
 * Frontend Routes
 * Namespaces indicate folder structure
 */


Route::get('/', 'LandingController@index')->name('landing');

Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
    includeRouteFiles(__DIR__.'/Frontend/');
});


/* ----------------------------------------------------------------------- */

/*
 * Backend Routes
 * Namespaces indicate folder structure
 */


Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    /*
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the backend,
     * then limit the backend features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     */
    includeRouteFiles(__DIR__.'/Backend/');
});


Route::post('/clone-speciality','SaveTasksController@cloneSpeciality');

Route::post('/save-task-element', 'SaveTasksController@saveNewElements');
Route::post('/save_block_img', 'SaveTasksController@saveBlockImage');

Route::post('/delete_task','SaveTasksController@deletetask');
Route::post('/get-speciality-tasks', 'SaveTasksController@getSpecialityTasks');
Route::post('/get-nmb-answers','SaveTasksController@getNmbAnswers');

Route::post('/get-questions', 'SaveTasksController@getQuestions');
Route::post('/get-question', 'SaveTasksController@getQuestion');
Route::post('/update-question','SaveTasksController@updateQuestion');
Route::post('/update-answer','SaveTasksController@updateAnswer');
Route::post('/delete-question','SaveTasksController@deleteQuestion');

Route::post('/get-block', 'SaveTasksController@getBlock');
Route::post('/create-block', 'SaveTasksController@createBlock');
Route::post('/update-block', 'SaveTasksController@updateBlock');
Route::post('/delete-block', 'SaveTasksController@deleteBlock');
Route::post('/update-conf', 'SaveTasksController@updateConf');
Route::post('/get_blocks', 'SaveTasksController@getBlocks');

Route::post('/create-english-block', 'ManageEnglishTasksController@createEnglishBlock');
Route::post('/create-english-task', 'ManageEnglishTasksController@createEnglishQuestion');

Route::post('/get-stuff','SaveTasksController@getStuff');
Route::post('/get-comm','SaveTasksController@getComm');
Route::post('/get-speciality-param','SaveTasksController@getSpecialityParam');

Route::post('/generatePdf','GeneratePdfController@generate');
Route::post('/generateList','GeneratePdfController@generateList');
Route::post('/reset-questions-history','GeneratePdfController@resetQuestionsHistory');
Route::post('/get-blanks','GeneratePdfController@getBlanks');
Route::post('/delete-blank','GeneratePdfController@deleteBlank');
Route::post('/delete_comm', 'SaveTasksController@deleteComm');

Route::post('/get-ukrainian-tasks', 'SaveTasksController@getUkrainianTasks');
Route::post('/save-ukrainian-tasks', 'SaveTasksController@saveUkrainianTasks');
Route::post('/update-ukrainian-task', 'SaveTasksController@updateUkrainianTask');
Route::post('/delete-ukrainian-task', 'SaveTasksController@deleteUkrainianTask');

Route::post('/delete_answer', 'SaveTasksController@deleteAnswer');

Route::post('/set_lang_block','NewBlockController@setLangBlock');
Route::post('/update_lang_block','NewBlockController@updateLangBlock');
Route::post('/generate_block_pdf', 'GeneratePdfController@generateBlockPdf');

Route::post('/get_settings', 'SaveTasksController@getSettings');
Route::post('/get_eng_questions', 'SaveTasksController@getEngQuestions');
Route::post('/delete_eng_question', 'SaveTasksController@deleteEngQuestion');
Route::post('/get_eng_question', 'SaveTasksController@getEngQuestion');
Route::post('/update_edit_eng', 'SaveTasksController@updateEditEng');
