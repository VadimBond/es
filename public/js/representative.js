/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 82);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var createSwal = __webpack_require__(3).createSwal;
var imageWidthChoices = __webpack_require__(10).imageWidthChoices;

function addListenersToImages() {
	var editImageWidthRadio = $('#panel3 .imageWidthContainer input[type="radio"]'),
	    editChangeWidthBtn = $('#panel3 .changeWidthBtn');

	editImageWidthRadio.off('change');
	editImageWidthRadio.on('change', function (e) {
		return handleChangeRadio(e, 'edit-task');
	});
	editChangeWidthBtn.off();
	editChangeWidthBtn.on('click', function (e) {
		e.preventDefault();
		var imageWidth = $(e.currentTarget).prev().val();
		setWidthImage(imageWidth, 'edit-task', $(e.currentTarget).prev().prev().find('input'));
	});

	$('.image-buttons').off('change');
	$('.image-buttons').on('change', function () {
		var isValideType = validateImageType(this);
		if (this.value && isValideType) {
			$(this).next().addClass("color-change");
			$(this).parent().parent().next().show();
			cutTitle(this, true);
		} else {
			$(this).next().removeClass("color-change");
			$(this).parent().parent().next().find(".imgDel").hide();
			cutTitle(this);
		}
	});

	$('.delete_img').off('click');
	$('.delete_img').on('click', function () {
		cutTitle($(this).parent().prev().children().last().children().first());
		$(this).parent().prev().children().last().children().first().attr('data-is-change', "true");
		$(this).parent().prev().children().last().children().first().val(null);
		$(this).parent().hide();
		$(this).parent().prev().children().last().children().last().removeClass("color-change");
	});
}

function cutTitle(element) {
	var isValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

	var currentSpan = $(element).next();

	var filename = "";

	if (isValue) {
		filename = $(element).val().replace(/.*\\/, "");
	} else {
		filename = "Завантажити фото";
	}

	currentSpan.attr('title', filename);
}

function validateImageType(elem) {
	var filesExt = ['jpg', 'jpeg', 'png'];

	var parts = $(elem).val().split('.');
	if (filesExt.join().search(parts[parts.length - 1]) == -1) {
		$(this).val('');
		createSwal({
			title: 'Невірний формат файлу',
			type: 'error'
		});

		return false;
	}
	return true;
}

function handleChangeRadio(e) {
	var currentRadio = $(e.currentTarget),
	    customWidthInput = $(currentRadio).parent().parent().find('.customWidthInput'),
	    changeWidthBtn = $(currentRadio).parent().parent().find('.changeWidthBtn');

	if (currentRadio.val() === 'custom') {
		customWidthInput.attr('disabled', false);
		changeWidthBtn.attr('disabled', false);
	} else {
		customWidthInput.attr('disabled', true);
		changeWidthBtn.attr('disabled', true);
		setWidthImage(currentRadio.val(), currentRadio);
	}
}

function setWidthImage(widthImage, currentRadio) {
	var multiblocks = $(currentRadio).parent().parent().parent().parent().find('.multiblock');

	multiblocks.attr('data-width', widthImage);

	widthImage = widthImage ? widthImage : 10;

	[].concat(_toConsumableArray(multiblocks)).forEach(function (el) {
		$(el).parent().find('.html').click();
		var value = $(el).val();
		$(el).val(value.replace(/width:(\d+|auto)/g, 'width:' + widthImage));
		$(el).parent().find('.html').click();
	});
}

function setDefaultImageWidth(imageWidth) {
	if (imageWidth === 'auto') {
		$('#panel3 .enableAutoWidth').click();
	} else {
		imageWidth = imageWidth ? imageWidth : 10;

		if (imageWidthChoices.includes(imageWidth)) {
			var searchRadio = $('#panel3 .imageWidthContainer input[type="radio"][value=' + imageWidth + ']');
			searchRadio.attr('checked', true);
		} else {
			var customWidthRadio = $('#panel3 .imageWidthContainer .customWithRadio'),
			    customWidthInput = $('#panel3 .imageWidthContainer .customWidthInput'),
			    changeWidthBtn = $('#panel3 .imageWidthContainer .changeWidthBtn');

			customWidthRadio.click();
			customWidthInput.attr('disabled', false);
			changeWidthBtn.attr('disabled', false);
			customWidthInput.val(imageWidth);
		}
	}
}

module.exports = {
	addListenersToImages: addListenersToImages,
	handleChangeRadio: handleChangeRadio,
	setWidthImage: setWidthImage,
	setDefaultImageWidth: setDefaultImageWidth
};

/***/ }),

/***/ 10:
/***/ (function(module, exports) {

var imageWidthChoices = ['50', '75', '100', '125', '150'];

module.exports = {
	imageWidthChoices: imageWidthChoices
};

/***/ }),

/***/ 11:
/***/ (function(module, exports) {

function addNewAnswer(answer, addQuestion, addAnswer, deleteQuestion, deleteAnswer, isRadio) {
	var newAnswer = '<div class="row ';
	if (isRadio) {
		newAnswer += 'item answer-item" style="margin-bottom: 5px;">\n               \t\t<div class="col-xs-12 form-inline "  style="width: 100%">';
	} else {
		newAnswer += 'category" style="margin-bottom: 5px;">\n                \t\t<div class="col-xs-12 form-inline "  style="width: 100%">';
	}

	if (isRadio) {
		newAnswer += '<div class="answer-item">\n                        <span data-toggle="tooltip" title="\u0412\u0456\u0440\u043D\u0430 \u0432\u0456\u0434\u043F\u043E\u0432\u0456\u0434\u044C">\n                            <input type="checkbox" name="cat-radio" class="correct-answer" ';

		if (answer != null) {
			newAnswer += answer['answer_right'] ? 'checked' : '';
		}

		newAnswer += '></span>';
	}

	newAnswer += '<div class="popupImageConvertor">\n                    <div class="popup-container">\n                        <h3 class="resize-title">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n                        <div class="resizer" >\n                            <span class="addition">ctrl+v</span>\n                        </div>\n                        <button class="resizer-result" class="btn btn-info">\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n                        <button class="closePopup">&#10006;</button>\n                    </div>\n                </div>\n                <textarea class="multiblock ';

	if (isRadio) {
		newAnswer += 'item-name " data-id-answer="';
	} else {
		newAnswer += 'question-name " data-id-answer="';
	}

	if (answer != null) {
		newAnswer += answer['id'];
	}

	newAnswer += '" name="text_answer_for_edit">';

	if (answer != null) {
		if (typeof answer.text_answer !== 'undefined') {
			newAnswer += answer.text_answer;
		} else {
			newAnswer += answer.text_question;
		}
	}

	newAnswer += '</textarea>';

	if (addQuestion) {
		newAnswer += '<div class="adding-btns">\n\t\t\t\t\t\t<span class="category-add r_margin" data-toggle="tooltip" title="\u0414\u043E\u0434\u0430\u0442\u0438 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F">\n\t\t\t\t\t\t\t<button type="button" class="btn btn-default">\n\t\t\t\t\t\t\t\t<span aria-hidden="true"><span class="glyphicon glyphicon-plus"></span> \u0414\u043E\u0434\u0430\u0442\u0438 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F</span>\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t</span>';
	}

	if (addAnswer) {
		newAnswer += '<button type="button" class="btn btn-default addNewAnswer"><span class="glyphicon glyphicon-plus"></span> \u0414\u043E\u0434\u0430\u0442\u0438 \u0432\u0430\u0440\u0456\u0430\u043D\u0442</button>';
	}

	if (deleteQuestion) {
		newAnswer += '<span class="category-delete" data-toggle="tooltip" title="\u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u043F\u0438\u0442\u0430\u043D\u043D\u044F">\n\t\t\t\t\t  \t<button type="button" class="btn btn-default">\n\t\t\t\t\t\t\t<span><span class="glyphicon glyphicon-trash"  aria-hidden="true"></span> \u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u043F\u0438\u0442\u0430\u043D\u043D\u044F</span>\n\t\t\t\t\t\t</button>\n\t\t\t\t\t  </span>\n\t\t\t\t\t</div>';
	}

	if (deleteAnswer) {
		newAnswer += '<div class="delete-block">\n\t\t\t\t\t\t<button type="button" class="btn btn-default deleteAnswer" style="display: none">\n\t\t\t\t\t\t\t<span><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> \u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u0432\u0430\u0440\u0456\u0430\u043D\u0442</span>\n\t\t\t\t\t\t</button> \n\t\t\t\t\t</div>';
	}
	if (isRadio) {
		newAnswer += '</div>';
	}
	newAnswer += '</div></div>';

	return newAnswer;
}

module.exports = {
	addNewAnswer: addNewAnswer
};

/***/ }),

/***/ 12:
/***/ (function(module, exports, __webpack_require__) {

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var getSettings = __webpack_require__(59);

var _require = __webpack_require__(27),
    renderFirstAnswers = _require.renderFirstAnswers;

var _require2 = __webpack_require__(25),
    listColContentChoices = _require2.listColContentChoices,
    newRowAnswerList = _require2.newRowAnswerList,
    newRowList = _require2.newRowList,
    newManageRowBlock = _require2.newManageRowBlock,
    booleanAnswer = _require2.booleanAnswer,
    newTaskContainer = _require2.newTaskContainer,
    newTextBlock = _require2.newTextBlock,
    newListAnswerBlock = _require2.newListAnswerBlock,
    newListBlock = _require2.newListBlock,
    questionContainer = _require2.questionContainer,
    newTestBlock = _require2.newTestBlock,
    manageButtonsBlock = _require2.manageButtonsBlock,
    questionMultiblock = _require2.questionMultiblock;

var resetEnglishTasks = __webpack_require__(28);

var _require3 = __webpack_require__(4),
    redactorMultiblock = _require3.redactorMultiblock;

var _require4 = __webpack_require__(1),
    setWidthImage = _require4.setWidthImage,
    handleChangeRadio = _require4.handleChangeRadio;

var getImageSrc = __webpack_require__(7);

var _require5 = __webpack_require__(10),
    imageWidthChoices = _require5.imageWidthChoices;

var createSwal = __webpack_require__(3).createSwal;
var setWidthListeners = __webpack_require__(6);

var settings = null;

var renderEnglishTask = function renderEnglishTask() {

	resetEnglishTasks();
	var manageButtonsBlock = $('#panel2 .manage-buttons-block');
	manageButtonsBlock.show();

	$('#panel2 .task-container .adding-btns').hide();
	$('#panel2 .task-container').show();
	$('#categories').find('p').show();
	$('#categories').find('button').last().show();

	if ($('#panel2 .text-block').is(":visible")) {
		$('#panel2 .text-block').find('.html').click();
		$('#panel2 .text-block').find('.multiblock').val('');
		$('#panel2 .text-block').find('.html').click();
	}

	if ($('#panel2 .questions-block').is(":visible")) {
		$('#panel2 .questions-block').find('.html').click();
		$('#panel2 .questions-block').find('.multiblock').val('');
		$('#panel2 .questions-block').find('.html').click();
	}

	settings = getSettings();
	return settings.then(function (result) {
		var addEnglishTaskBtn = $('#panel2 .add-english-task'),
		    isText = result.isText,
		    isListAnswer = result.isListAnswer,
		    isBooleanAnswers = result.isBooleanAnswers,
		    nmbQuestions = result.nmbQuestions,
		    nmbAnswers = result.nmbAnswers,
		    extraAnswers = result.extraAnswers,
		    container = void 0,
		    textContainer = void 0,
		    listContainer = void 0,
		    listAnswerContainer = void 0,
		    questionContainer = void 0,
		    answersContainer = void 0,
		    addNewAnswerBtn = void 0;


		var activeTab = $('#block_selector').is(':visible') ? 'add' : 'edit';

		if (activeTab === 'add') {
			container = $('#panel2 #categories');
			textContainer = $('#panel2 .text-block');
			listContainer = $('#panel2 .list-block');
			questionContainer = $('#panel2 .category-block');
			answersContainer = $('#panel2 #first_answer_item');
			addNewAnswerBtn = $('#panel2 #categories .addNewAnswer');
			listAnswerContainer = $('#panel2 .list-answers-block');

			var imageWidthContainer = textContainer.find('.imageWidthContainer').first();

			imageWidthContainer.next().remove();
			imageWidthContainer.after(questionMultiblock);
			imageWidthContainer.next().find('.text-question').on('click', redactorMultiblock);

			imageWidthContainer = questionContainer.find('.imageWidthContainer').first();

			imageWidthContainer.next().remove();
			imageWidthContainer.after(questionMultiblock);
			imageWidthContainer.next().find('.text-question').on('click', redactorMultiblock);
		} else {
			container = $('#panel3 #editingQuestions');
			textContainer = $('#panel3 .text-block');
			listContainer = $('#panel3 .list-block');
			questionContainer = $('#panel3 .category-block');
			answersContainer = $('#panel3 #first_edit_item');
			addNewAnswerBtn = $('#panel3 #editingQuestions .addNewAnswer');
			listAnswerContainer = $('#panel3 .list-answers-block');

			$('#panel3 button[type="submit"]').show();
			$('#panel3 .questions-block').show();
		}

		container.show();
		answersContainer.empty();

		if (!isText) {
			textContainer.hide();
			listContainer.hide();

			if (isListAnswer) {
				var tableAnswersRows = $(listAnswerContainer).find('table tbody');

				tableAnswersRows.empty();
				for (var indexRow = 0; indexRow < nmbQuestions; indexRow++) {
					tableAnswersRows.append(newRowAnswerList(indexRow));
				}
				listAnswerContainer.show();

				var contentCol = 'list',
				    tableQuestionsRows = $(listContainer).find('table tbody');

				listContainer.find('.change-content-col').text(listColContentChoices[contentCol]);

				tableQuestionsRows.empty();

				var initialIndexRow = 0;

				if (!isBooleanAnswers && $('#block_selector_for_edit').is(':visible')) {
					var number = parseInt($('#tasks_selector_for_edit option:selected').attr('data-width'));

					for (var i = 0; i < number; i++) {
						tableQuestionsRows.append(newRowList(nmbQuestions, initialIndexRow, isBooleanAnswers, 0, 'askii'));
					}
				} else {
					tableQuestionsRows.append(newRowList(nmbQuestions, initialIndexRow, isBooleanAnswers, 0, 'askii'));
				}

				tableQuestionsRows.append(newManageRowBlock);
				listContainer.show();
			}
		} else {
			textContainer.show();
			var textQuestion = $('#panel2 .text-block .html');

			if ($(textQuestion).is(':visible')) {
				textQuestion.click();
				$('#panel2 .text-question').val('');
				textQuestion.click();
			}

			if (isListAnswer) {
				var _contentCol = isBooleanAnswers ? 'bool' : 'default',
				    tableRows = $(listContainer).find('table tbody');

				listContainer.find('.change-content-col').text(listColContentChoices[_contentCol]);

				tableRows.empty();
				var _indexRow = 0;
				for (_indexRow; _indexRow < nmbQuestions + extraAnswers; _indexRow++) {
					tableRows.append(newRowList(nmbQuestions, _indexRow, isBooleanAnswers, extraAnswers));
				}

				listContainer.show();
			} else {
				listContainer.hide();
			}
		}

		// just for displaying questions
		if (!isListAnswer) {
			var nmbAnswersToRender = isText ? nmbAnswers : nmbQuestions;

			questionContainer.show();

			if (isBooleanAnswers) {
				addNewAnswerBtn.hide();
				answersContainer.append(booleanAnswer);
			} else {
				addNewAnswerBtn.show();
				renderFirstAnswers(nmbAnswersToRender, answersContainer);
			}

			if (isText) {
				$('#panel2 .category-block:first').find('.category').remove();
				$('#panel2 .category-block:first').find('.imageWidthContainer').after(questionMultiblock);
				$('#panel2 .category-block:first').find('.text-question').on('click', redactorMultiblock);

				var questionBlock = $('#panel2 .questions-block');

				if ($('#block_selector_for_edit').is(':visible')) {
					questionBlock = $('#panel3 .questions-block');
				}
				// render nmbQuestions - 1 questions
				for (var indexQuestion = 1; indexQuestion < nmbQuestions; indexQuestion++) {
					var testBlock = $(newTestBlock(activeTab)),
					    _answersContainer = testBlock.find('.answers-container');

					questionBlock.append(testBlock);

					if (isBooleanAnswers) {
						_answersContainer.append(booleanAnswer);
					} else {
						renderFirstAnswers(nmbAnswersToRender, _answersContainer);
					}
				}
			}
		} else {
			questionContainer.hide();
		}

		addEnglishTaskBtn.show();
		updateListeners();

		$('#panel2 input[type="radio"][value="100"]').prop('checked', true);

		if ($('.question-name').closest('.category-block').is(':visible')) {
			var _questionBlock = $('#panel2 .questions-block:first .html');
			_questionBlock.click();
			$('#panel2 .questions-block:first .question-name').val('');
			_questionBlock.click();
		}
	});
};

var addEnglishTask = function addEnglishTask(e) {
	e.preventDefault();
	var activeTab = 'add';

	settings.then(function (result) {
		var lastTask = $('#panel2 .task-container:last'),
		    rmEnglishBtns = $('#panel2 .rm-english-task'),
		    isText = result.isText,
		    isListAnswer = result.isListAnswer,
		    isBooleanAnswers = result.isBooleanAnswers,
		    nmbQuestions = result.nmbQuestions,
		    nmbAnswers = result.nmbAnswers,
		    extraAnswers = result.extraAnswers;


		var newTask = $(newTaskContainer);

		if (isText) {
			newTask.append(newTextBlock());

			if (isListAnswer) {
				var listContainer = $(newListBlock);
				newTask.append(listContainer);

				var contentCol = isBooleanAnswers ? 'bool' : 'default',
				    tableRows = $(listContainer).find('table tbody');

				listContainer.find('.change-content-col').text(listColContentChoices[contentCol]);

				var indexRow = 0;
				for (indexRow; indexRow < nmbQuestions + extraAnswers; indexRow++) {
					tableRows.append(newRowList(nmbQuestions, indexRow, isBooleanAnswers, extraAnswers));
				}
			}
		} else {
			if (isListAnswer) {
				var listAnswerContainer = $(newListAnswerBlock);
				newTask.append(listAnswerContainer);

				var tableAnswersRows = $(listAnswerContainer).find('table tbody');

				for (var _indexRow2 = 0; _indexRow2 < nmbQuestions; _indexRow2++) {
					tableAnswersRows.append(newRowAnswerList(_indexRow2));
				}

				var _listContainer = $(newListBlock),
				    _contentCol2 = 'list',
				    tableQuestionsRows = $(_listContainer).find('table tbody');

				newTask.append(_listContainer);

				_listContainer.find('.change-content-col').text(listColContentChoices[_contentCol2]);

				var initialIndexRow = 0;
				tableQuestionsRows.append(newRowList(nmbQuestions, initialIndexRow, isBooleanAnswers, 0, 'askii'));
				tableQuestionsRows.append(newManageRowBlock);
			}
		}

		if (!isListAnswer) {
			var testContainer = $(questionContainer),
			    testBlock = $(newTestBlock(activeTab, newTask)),
			    answersContainer = testBlock.find('.answers-container'),
			    nmbAnswersToRender = isText ? nmbAnswers : nmbQuestions;

			testContainer.append(testBlock);
			newTask.append(testContainer);

			if (isBooleanAnswers) {
				answersContainer.append(booleanAnswer);
			} else {
				renderFirstAnswers(nmbAnswersToRender, answersContainer);
			}

			if (isText) {
				for (var indexQuestion = 1; indexQuestion < nmbQuestions; indexQuestion++) {
					var _testContainer = $(questionContainer),
					    _testBlock = $(newTestBlock(activeTab, newTask)),
					    _answersContainer2 = _testBlock.find('.answers-container');
					_testContainer.append(_testBlock);
					newTask.append(_testContainer);
					if (isBooleanAnswers) {
						_answersContainer2.append(booleanAnswer);
					} else {
						renderFirstAnswers(nmbAnswersToRender, _answersContainer2);
					}
				}
			}
		}

		newTask.append(manageButtonsBlock);
		lastTask.after(newTask);

		rmEnglishBtns.show();

		updateListeners();
	});
};

var rmEnglishTask = function rmEnglishTask(e) {
	e.preventDefault();

	var countTasks = $('#panel2 .task-container').length,
	    rmTaskBtn = $('#panel2 .task-container .rm-english-task');

	if (countTasks >= 2) {
		var tasksContainer = [].concat(_toConsumableArray($('#panel2 .task-container'))),
		    currBtn = $(e.currentTarget),
		    currTaskContainer = tasksContainer.filter(function (taskContainer) {
			return $(taskContainer).find(currBtn).length > 0;
		});

		$(currTaskContainer).remove();
	}

	if (countTasks <= 2) {
		rmTaskBtn.hide();
	}
};

function validateEnglish() {
	var taskContainers = $('.task-container');
	var boxRight = 0;
	var countRight = 0;
	var valRight = 0;
	var questionRight = 0;
	var questionType = 0;
	var filledQuestions = 0;
	var question = void 0;

	var textItems = $('.task-container .item-name');
	var boxes = 0;

	var _iteratorNormalCompletion = true;
	var _didIteratorError = false;
	var _iteratorError = undefined;

	try {
		for (var _iterator = taskContainers[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
			var area = _step.value;

			var Containers = $(area).find('.category-block:visible');
			var currentCount = 0;
			question = 0;
			var text = $(area).find('.text-block:visible .text-question');

			if (text.length > 0) {
				var countNNN = ($(text).val().match(/NNN/g) || []).length;

				if ($('#answersCount').val() != countNNN) {
					currentCount = 1;
					countRight = 1;
				}

				if ($(text).val() === '') {
					valRight = 1;
				}
			}

			var _iteratorNormalCompletion3 = true;
			var _didIteratorError3 = false;
			var _iteratorError3 = undefined;

			try {
				for (var _iterator3 = Containers[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var container = _step3.value;

					var anyBoxesChecked = false;

					$(container).find('input[type="checkbox"]').each(function () {
						boxes++;
						if ($(this).is(":checked")) {
							anyBoxesChecked = true;
						}
					});

					if (!anyBoxesChecked && boxes > 0) {
						boxRight = 1;
					}

					var questionText = $(container).find('.category  .question-name, .category  .text-question');

					if (questionText) {

						if ($(questionText).val() == '') {
							question = 1;
						}
						if ($(questionText).val()) {
							filledQuestions = 1;
						}
					}
				}
			} catch (err) {
				_didIteratorError3 = true;
				_iteratorError3 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError3) {
						throw _iteratorError3;
					}
				}
			}

			var questions = $(Containers).find('.category .question-name, .category  .text-question');

			if (question == 1 && currentCount == 1) {
				questionRight = 1;
			}

			if (questions.length > 0 && text.val()) {
				questionType = 1;
			} else if (questions.length > 0 && !text.val()) {
				questionType = 2;
			}
		}
	} catch (err) {
		_didIteratorError = true;
		_iteratorError = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion && _iterator.return) {
				_iterator.return();
			}
		} finally {
			if (_didIteratorError) {
				throw _iteratorError;
			}
		}
	}

	var _iteratorNormalCompletion2 = true;
	var _didIteratorError2 = false;
	var _iteratorError2 = undefined;

	try {
		for (var _iterator2 = textItems[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
			var _area = _step2.value;

			if ($(_area).val() == '') {
				valRight = 1;
			}
		}
	} catch (err) {
		_didIteratorError2 = true;
		_iteratorError2 = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion2 && _iterator2.return) {
				_iterator2.return();
			}
		} finally {
			if (_didIteratorError2) {
				throw _iteratorError2;
			}
		}
	}

	var errorMessage = "Неправильно заповнені завдання:\n";

	if (countRight == 1 && questionType == 0 && $('#answersCount').val() != "0") {
		errorMessage += "- в тексті має бути присутня необхідна кількість вставок питань('NNN')\n";
	}

	if (valRight == 1) {
		errorMessage += "- заповніть всі необхідні поля\n";
	}

	if (boxRight == 1) {
		errorMessage += "- оберіть правильний варіант для усіх завдань\n";
	}

	if (question == 1 && filledQuestions == 1) {
		errorMessage += "- заповніть усі умови завдань\n";
	}

	if (questionRight == 1 && questionType == 1) {
		errorMessage += "- вставте необхідну кількість питань в текст АБО заповніть умови завдань";
	}

	if (countRight == 1 && questionType == 0 && $('#answersCount').val() != "0" || valRight == 1 || boxRight == 1 || questionRight == 1 && questionType == 1 || question == 1 && filledQuestions == 1) {
		createSwal({
			title: errorMessage,
			type: 'error'
		});
		return false;
	} else {
		return true;
	}
}

var saveEnglishTask = function saveEnglishTask(e) {
	settings.then(function (result) {
		var valid = validateEnglish();

		if (valid) {
			(function () {
				$(document).find('.bookshelf_wrapper').show();
				$(document).find('.hideBehind').show();
				$('#all').css("opacity", "0.5");

				var tasks = $(e.currentTarget).find('.task-container'),
				    idBlock = $('#block_selector').val(),
				    isText = result.isText,
				    isListAnswer = result.isListAnswer,
				    isBooleanAnswers = result.isBooleanAnswers;


				var resultToSend = [];
				var _iteratorNormalCompletion4 = true;
				var _didIteratorError4 = false;
				var _iteratorError4 = undefined;

				try {
					for (var _iterator4 = tasks[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
						var task = _step4.value;

						var textBlock = void 0,
						    listBlockRows = void 0,
						    taskToSend = { idBlock: idBlock };

						if (isText) {
							textBlock = $(task).find('.text-block');
							var imageWidthContainer = $(textBlock).find('.imageWidthContainer'),
							    checkedCheckbox = imageWidthContainer.find('input[type="radio"]:checked').val(),
							    imageWidth = checkedCheckbox !== 'custom' ? checkedCheckbox : imageWidthContainer.find('.customWidthInput').val(),
							    _getImageSrc = getImageSrc(textBlock.find('.text-question').val()),
							    _getImageSrc2 = _slicedToArray(_getImageSrc, 2),
							    content = _getImageSrc2[0],
							    images = _getImageSrc2[1];


							if (textBlock.find('.enableAutoWidth').hasClass('btn-success')) {
								imageWidth = 'auto';
							}

							taskToSend.textData = {
								content: content,
								images: images,
								imageWidth: imageWidth
							};

							if (isListAnswer) {
								listBlockRows = $(task).find('.list-block table tbody tr');

								taskToSend.listData = [].concat(_toConsumableArray(listBlockRows)).map(function (row) {
									var textColData = $(row).find('.textCol input'),
									    indexLetter = $(row).find('td:first').text(),
									    resultCol = $(row).find('.resultCol'),
									    keyName = isBooleanAnswers ? 'boolCol' : 'selectCol',
									    resultColData = { indexLetter: indexLetter },
									    dataKey = isBooleanAnswers ? 'isChecked' : 'position';

									resultColData[dataKey] = resultCol.find('select').val();

									return _defineProperty({
										textCol: textColData.val()
									}, keyName, resultColData);
								});
							}
						} else {
							if (isListAnswer) {
								var _listBlockRows = void 0,
								    listAnswerRows = void 0;

								listAnswerRows = $(task).find('.list-answers-block table tbody tr');

								taskToSend.listAnswerData = [].concat(_toConsumableArray(listAnswerRows)).map(function (row) {
									var indexLetter = $(row).find('td:first').text(),
									    textData = $(row).find('.textCol input').val();

									return {
										indexLetter: indexLetter,
										textData: textData
									};
								});

								_listBlockRows = $(task).find('.list-block table tbody tr:not(.adding-row)');

								taskToSend.listData = [].concat(_toConsumableArray(_listBlockRows)).map(function (row) {
									var textColData = $(row).find('.textCol input'),
									    indexLetter = $(row).find('td:first').text(),
									    resultCol = $(row).find('.resultCol'),
									    keyName = 'selectCol',
									    resultColData = { indexLetter: indexLetter };

									resultColData.position = resultCol.find('select').val();

									return _defineProperty({
										textCol: textColData.val()
									}, keyName, resultColData);
								});
							}
						}

						if (!isListAnswer) {
							var questionData = {},
							    questions = $(task).find('.category-block');

							if (isBooleanAnswers) {
								questionData = [].concat(_toConsumableArray(questions)).map(function (question) {
									var questionTextarea = $(question).find('.question-name'),
									    booleanAnswer = $(question).find('.items .booleanAnswer'),
									    imageWidthContainer = $(question).find('.imageWidthContainer'),
									    checkedCheckbox = imageWidthContainer.find('input[type="radio"]:checked').val(),
									    imageWidth = checkedCheckbox !== 'custom' ? checkedCheckbox : imageWidthContainer.find('.customWidthInput').val();

									if ($(question).find('.enableAutoWidth').hasClass('btn-success')) {
										imageWidth = 'auto';
									}

									return {
										question: getImageSrc(questionTextarea.val()),
										isCorrect: booleanAnswer.find('input[type="checkbox"]').is(':checked') ? 1 : 0,
										imageWidth: imageWidth
									};
								});
							} else {
								questionData = [].concat(_toConsumableArray(questions)).map(function (question) {
									var questionTextarea = $(question).find('.question-name'),
									    answers = $(question).find('.items .item'),
									    imageWidthContainer = $(question).find('.imageWidthContainer'),
									    checkedCheckbox = imageWidthContainer.find('input[type="radio"]:checked').val(),
									    imageWidth = checkedCheckbox !== 'custom' ? checkedCheckbox : imageWidthContainer.find('.customWidthInput').val();

									if ($(question).find('.enableAutoWidth').hasClass('btn-success')) {
										imageWidth = 'auto';
									}

									if (typeof questionTextarea.val() === 'undefined') {
										questionTextarea = $(question).find('.text-question');
									}

									return {
										question: getImageSrc(questionTextarea.val()),
										answers: [].concat(_toConsumableArray(answers)).map(function (answer) {
											var answerTextarea = $(answer).find('.item-name'),
											    answerCheckbox = $(answer).find('.correct-answer');

											return {
												answer: getImageSrc(answerTextarea.val()),
												isChecked: answerCheckbox.is(':checked') ? 1 : 0
											};
										}),
										imageWidth: imageWidth
									};
								});
							}

							taskToSend.questions = questionData;
						}
						resultToSend.push(taskToSend);
					}
				} catch (err) {
					_didIteratorError4 = true;
					_iteratorError4 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion4 && _iterator4.return) {
							_iterator4.return();
						}
					} finally {
						if (_didIteratorError4) {
							throw _iteratorError4;
						}
					}
				}

				var taskPromises = resultToSend.map(function (taskData) {
					return $.ajax({
						url: '/create-english-task',
						method: 'post',
						data: taskData
					});
				});

				Promise.all(taskPromises).then(function () {
					createSwal({
						title: 'Завдання успішно збережено',
						type: 'success'
					});

					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					$('#send-form button').prop('disabled', false);

					$('.task-container').hide();
					$('#categories').find('p').hide();
					$('#categories').find('button').last().hide();
					$('#block_selector').val('');

					resetEnglishTasks();
				}).catch(function (err) {
					createSwal({
						title: 'Помилка при сбереженні завдання',
						type: 'error'
					});

					$('#send-form button').prop('disabled', false);

					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					console.log(err);
				});
			})();
		} else {
			$('#send-form button').prop('disabled', false);
		}
	});
};

var onChangeListSelect = function onChangeListSelect(e) {
	var curSelect = $(e.currentTarget),
	    prevVal = curSelect.attr('data-previous-val'),
	    curVal = curSelect.val(),
	    prevSelect = curSelect.closest('tbody').find('select[data-previous-val="' + curVal + '"]');

	var booleanContent = ['Ні', 'Так', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'];
	if (!booleanContent.includes($(curSelect).find('option:selected').text().trim())) {
		curSelect.find('option[value="' + curVal + '"]').prop('selected', true);
		prevSelect.find('option[value="' + prevVal + '"]').prop('selected', true);

		curSelect.attr('data-previous-val', curVal);
		prevSelect.attr('data-previous-val', prevVal);
	}
};

var addQuestionRow = function addQuestionRow(e, nmbQuestions, isBooleanAnswers) {
	e.preventDefault();

	var maxRows = 15,
	    currBtn = $(e.currentTarget),
	    listBlocks = $(currBtn).closest('.list-block'),
	    currContainer = [].concat(_toConsumableArray(listBlocks)).filter(function (listBlock) {
		return $(listBlock).find(currBtn).length > 0;
	}),
	    rmBtn = $(currContainer).find('.rmQuestionRow'),
	    tableRows = $(currContainer).find('tbody tr:not(.adding-row)'),
	    tableRowsNmb = tableRows.length;

	if (tableRowsNmb < maxRows) {
		$(currContainer).find('.adding-row').before(newRowList(nmbQuestions, tableRowsNmb, isBooleanAnswers, 0, 'askii'));
	} else {
		createSwal({ title: '\u041C\u0430\u043A\u0441\u0438\u043C\u0430\u043B\u044C\u043D\u0430 \u043A\u0456\u043B\u044C\u043A\u0456\u0441\u0442\u044C \u043F\u0438\u0442\u0430\u043D\u044C - ' + maxRows + '.', type: 'error' });
	}

	rmBtn.removeAttr('disabled');
};

var rmQuestionRow = function rmQuestionRow(e) {
	e.preventDefault();
	var currBtn = $(e.currentTarget),
	    listBlocks = $(currBtn).closest('.list-block'),
	    currContainer = [].concat(_toConsumableArray(listBlocks)).filter(function (listBlock) {
		return $(listBlock).find(currBtn).length > 0;
	}),
	    tableRows = $(currContainer).find('tbody tr:not(.adding-row)'),
	    tableRowsNmb = tableRows.length,
	    rowToRemove = tableRows.last();

	if (tableRowsNmb == 2) {
		currBtn.attr('disabled', true);
	}
	if (tableRowsNmb > 1) {
		$(rowToRemove).remove();
	}
};

var updateListeners = function updateListeners() {
	settings.then(function (result) {
		var isText = result.isText,
		    isListAnswer = result.isListAnswer,
		    nmbQuestions = result.nmbQuestions,
		    isBooleanAnswers = result.isBooleanAnswers,
		    mainContainer = $('#block_selector_for_edit').is(':visible') ? $('#panel3') : $('#panel2'),
		    addEnglishTaskBtn = mainContainer.find('.add-english-task'),
		    rmEnglishTaskBtn = mainContainer.find('.rm-english-task'),
		    listBlock = mainContainer.find('.list-block'),
		    listSelect = listBlock.find('select'),
		    listAddBtn = listBlock.find('.addQuestionRow'),
		    listRmBtn = listBlock.find('.rmQuestionRow'),
		    multiblocks = mainContainer.find('.multiblock'),
		    addImageWidthRadio = $(mainContainer).find('.imageWidthContainer input[type="radio"]'),
		    addChangeWidthBtn = $(mainContainer).find('.changeWidthBtn');


		addEnglishTaskBtn.off('click');
		addEnglishTaskBtn.on('click', addEnglishTask);

		rmEnglishTaskBtn.off('click');
		rmEnglishTaskBtn.on('click', rmEnglishTask);

		listSelect.off('change');
		listSelect.on('change', onChangeListSelect);

		listAddBtn.off('click');
		listAddBtn.on('click', function (e) {
			return addQuestionRow(e, nmbQuestions, isBooleanAnswers);
		});

		listRmBtn.off('click');
		listRmBtn.on('click', rmQuestionRow);

		multiblocks.off('click');
		multiblocks.on('click', redactorMultiblock);

		addImageWidthRadio.off('change');
		addImageWidthRadio.on('change', function (e) {
			return handleChangeRadio(e);
		});
		addChangeWidthBtn.off('click');
		addChangeWidthBtn.on('click', function (e) {
			e.preventDefault();

			var imageWidth = $(e.currentTarget).prev().val();

			setWidthImage(imageWidth, $(e.currentTarget).prev().prev().find('input'));
		});

		setWidthListeners();
	});
};

var getEngQuestions = function getEngQuestions() {
	settings = getSettings();
	$('#tasks_selector_for_edit').html("");

	$(document).find('.bookshelf_wrapper').show();
	$(document).find('.hideBehind').show();
	$('#all').css("opacity", "0.5");

	settings.then(function (result) {
		$.ajax({
			url: '/get_eng_questions',
			method: 'post',
			data: result,
			success: function success(response) {
				if (response.length > 0) {
					var key = result['isListAnswer'] || result['isText'] ? 'text' : 'text_question';
					$('#questions_edit_block_elements [data-toggle="tooltip"] button').prop("disabled", false);

					var _iteratorNormalCompletion5 = true;
					var _didIteratorError5 = false;
					var _iteratorError5 = undefined;

					try {
						for (var _iterator5 = response[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
							var question = _step5.value;


							var replacedText = removeHTML(question[key]);
							if (replacedText == "") {
								$('#tasks_selector_for_edit').append('\n\t\t\t\t\t\t\t\t<option value="' + question['id'] + '" \n\t\t\t\t\t\t\t\t\tname = "image" \n\t\t\t\t\t\t\t\t\tdata-width="' + question['image_width'] + '"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t\u0417\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F\n\t\t\t\t\t\t\t\t</option>\n\t\t\t\t\t\t\t');
							} else {
								$('#tasks_selector_for_edit').append('\n\t\t\t\t\t\t\t\t<option value="' + question['id'] + '"\n\t                            \tname = "normal" \n\t                            \tdata-width="' + question['image_width'] + '"\n\t                            >\n\t\t\t\t\t\t\t\t\t' + replacedText + '\n\t\t\t\t\t\t\t\t</option>\n\t\t\t\t\t\t\t');
							}
						}
					} catch (err) {
						_didIteratorError5 = true;
						_iteratorError5 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion5 && _iterator5.return) {
								_iterator5.return();
							}
						} finally {
							if (_didIteratorError5) {
								throw _iteratorError5;
							}
						}
					}

					$('#questions_edit_block_elements').show();
				} else {
					$('#questions_edit_block_elements').hide();

					createSwal({
						title: 'Питання до даного блоку відсутні',
						type: 'info'
					});
					editTaskSelect.val('');
				}

				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");
			},
			error: function error(err) {
				console.log(err);
			}
		});
	});
};

var deleteEngQuestion = function deleteEngQuestion() {
	settings.then(function (result) {
		var isConfirm = confirm('Ви дійсно хочете видати дане завдання?');

		if (isConfirm) {
			$(document).find('.bookshelf_wrapper').show();
			$(document).find('.hideBehind').show();
			$('#all').css("opacity", "0.5");

			var data = {
				id: $('#tasks_selector_for_edit option:selected').val(),
				settings: result
			};

			$.ajax({
				url: '/delete_eng_question',
				method: 'post',
				data: data,
				success: function success() {
					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					$('#form_for_change_task').hide();
					$('#tasks_selector_for_edit option:selected').remove();
					if ($('#tasks_selector_for_edit option').length < 1) {
						$('#questions_edit_block_elements').hide();
					}

					createSwal({
						title: 'Завдання успішно видалено',
						type: 'success'
					});
				},
				error: function error(err) {
					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					createSwal({
						title: 'Помилка при видаленні завдання',
						type: 'error'
					});
					console.log(err);
				}
			});
		}
	});
};

var getEngQuestion = function getEngQuestion() {
	settings.then(function (result) {
		var data = {
			id: $('#tasks_selector_for_edit option:selected').val(),
			settings: result
		};
		$('#panel3 .addNewAnswer').hide();
		$('#panel3 .task-container').show();

		renderEnglishTask().then(function () {
			$.ajax({
				url: '/get_eng_question',
				method: 'post',
				data: data,
				success: function success(response) {
					var title = $('#tasks_selector_for_edit option:selected'),
					    textBlock = $('#panel3 .text-block'),
					    listAnswers = $('#panel3 .list-answers-block'),
					    listBlock = $('#panel3 .list-block'),
					    questionBlock = $('#panel3 .questions-block'),
					    categoryBlock = $(questionBlock).find('.category-block'),
					    settings = data['settings'],
					    text = response['text'],
					    question = response['question'],
					    answers = response['answers'],
					    imageWidth = $(title).attr('data-width');

					if (!data['settings']['isListAnswer'] || data['settings']['isText']) {
						if (imageWidth === 'auto') {
							textBlock.find('.enableAutoWidth').click();
						} else {
							if (imageWidthChoices.includes(imageWidth)) {
								$('#panel3 input[type="radio"][value="' + imageWidth + '"]:first').click();
							} else {
								$('#panel3 input[type="radio"][value="custom"]:first').click();
								$('#panel3 .customWidthInput:first').prop('disabled', false);
								$('#panel3 .customWidthInput:first').val(imageWidth);
								$('#panel3 .changeWidthBtn:first').prop('disabled', false);
							}
						}
					}

					if (settings['isListAnswer']) {
						if (settings['isText']) {
							$(textBlock).find('.category').remove();
							$(textBlock).find('.imageWidthContainer').after(questionMultiblock);
							$(textBlock).find('.text-question').on('click', redactorMultiblock);

							textBlock.find('.text-question').val(text);

							for (var i = 0; i < answers.length; i++) {
								$(listBlock.find('input[type="text"]')[i]).val(answers[i]['text_question']);
								$(listBlock.find('input[type="text"]')[i]).attr('data-id', answers[i]['id']);
							}

							for (var _i = 0; _i < answers.length; _i++) {
								var curSelect = $(listBlock.find('select')[_i]);
								curSelect.attr('data-previous-val', answers[_i]['position']);

								curSelect.find('option[value="' + answers[_i]['position'] + '"]').attr('selected', true);
							}
						} else {
							var inputs = $(listAnswers).find('input');

							for (var _i2 = 0; _i2 < question.length; _i2++) {
								$(inputs[_i2]).val(question[_i2]['text_question']);
							}

							inputs = $(listBlock).find('input');

							if (inputs.length > 1) {
								$('#panel3 .rmQuestionRow').removeAttr('disabled');
							}

							for (var _i3 = 0; _i3 < answers.length; _i3++) {
								$(inputs[_i3]).val(answers[_i3]['text_question']);
								$(inputs[_i3]).attr('data-id', answers[_i3]['id']);
								$(inputs[_i3]).closest('tr').find('td:first').text(_i3 + 1);
								$(listBlock.find('select')[_i3]).find('option[value="' + answers[_i3]['right_answer'] + '"]').attr('selected', true);
							}
						}
					} else if (settings['isText']) {
						$(textBlock).find('.category').remove();
						$(textBlock).find('.imageWidthContainer').after(questionMultiblock);
						$(textBlock).find('.text-question').on('click', redactorMultiblock);

						textBlock.find('.text-question').val(text);

						for (var _i4 = 0; _i4 < question.length; _i4++) {
							if (!_i4) {
								$(categoryBlock[_i4]).find('.category').remove();
								$(categoryBlock[_i4]).find('.imageWidthContainer').after(questionMultiblock);
								$(categoryBlock[_i4]).find('.text-question').on('click', redactorMultiblock);

								$(categoryBlock[_i4]).find('.text-question').val(question[_i4]['text_question']);
								$(categoryBlock[_i4]).find('.text-question').attr('data-id', question[_i4]['id']);
							} else {
								$(categoryBlock[_i4]).find('.question-name').val(question[_i4]['text_question']);
								$(categoryBlock[_i4]).find('.question-name').attr('data-id', question[_i4]['id']);
							}

							imageWidth = question[_i4]['image_width'];

							if (imageWidth !== 'auto') {
								if (imageWidthChoices.includes(imageWidth)) {
									$(categoryBlock[_i4]).find('input[type="radio"][value="' + imageWidth + '"]').prop('checked', true);
								} else {
									$(categoryBlock[_i4]).find('input[type="radio"][value="custom"]').prop('checked', true);
									$(categoryBlock[_i4]).find('.customWidthInput').prop('disabled', false);
									$(categoryBlock[_i4]).find('.customWidthInput').val(imageWidth);
									$(categoryBlock[_i4]).find('.changeWidthBtn').prop('disabled', false);
								}
							} else {
								$(categoryBlock[_i4]).find('.enableAutoWidth').click();
							}

							for (var j = 0; j < answers[_i4].length; j++) {
								$($(categoryBlock[_i4]).find('.item-name')[j]).val(answers[_i4][j]['text_answer']);
								$($(categoryBlock[_i4]).find('.item-name')[j]).attr('data-id', answers[_i4][j]['id']);
								$($(categoryBlock[_i4]).find('input[type="checkbox"]')[j]).attr('checked', answers[_i4][j]['answer_right'] != 0);
							}
						}
					} else {
						$(questionBlock).find('.category').remove();
						$(questionBlock).find('.imageWidthContainer').after(questionMultiblock);
						$(questionBlock).find('.text-question').on('click', redactorMultiblock);

						if (imageWidth !== 'auto') {
							if (imageWidthChoices.includes(imageWidth)) {
								$(questionBlock).find('input[type="radio"][value="' + imageWidth + '"]').click();
							} else {
								$(questionBlock).find('input[type="radio"][value="custom"]').click();
								$(questionBlock).find('.customWidthInput').prop('disabled', false);
								$(questionBlock).find('.customWidthInput').val(imageWidth);
								$(questionBlock).find('.changeWidthBtn').prop('disabled', false);
							}
						} else {
							$(questionBlock).find('.enableAutoWidth').click();
						}

						$(questionBlock).find('.text-question').val(question);

						for (var _i5 = 0; _i5 < answers.length; _i5++) {
							$(questionBlock.find('.item-name')[_i5]).val(answers[_i5]['text_answer']);
							$(questionBlock.find('.item-name')[_i5]).attr('data-id', answers[_i5]['id']);
							$(questionBlock.find('input[type="checkbox"]')[_i5]).attr('checked', answers[_i5]['answer_right'] != 0);
						}
					}
				},
				error: function error(err) {
					createSwal({
						title: 'Помилка при спробі отримання данних',
						type: 'error'
					});
					console.log(err);
				}
			}).then(function () {
				$('#form_edit_task').show();
				$('#panel3 .multiblock').each(function () {
					if ($(this).is(':visible') || $(this).parent().hasClass('jHtmlArea') && $(this).parent().is(':visible')) {
						$(this).click();
					}
				});
			});
		});
	});
};

var updateEditEng = function updateEditEng() {
	settings = getSettings();

	var valid = validateEnglish();

	if (valid) {
		settings.then(function (result) {
			$(document).find('.bookshelf_wrapper').show();
			$(document).find('.hideBehind').show();
			$('#all').css("opacity", "0.5");

			var data = void 0,
			    title = $('#tasks_selector_for_edit option:selected'),
			    textBlock = $('#panel3 .text-block'),
			    listAnswers = $('#panel3 .list-answers-block'),
			    listBlock = $('#panel3 .list-block'),
			    questionBlock = $('#panel3 .questions-block'),
			    imageWidth = void 0,
			    block = textBlock.is(':visible') ? textBlock : questionBlock;

			if (block.find('input[type="radio"]:checked').val() !== 'custom') {
				imageWidth = block.find('input[type="radio"]:checked').val();
			} else {
				imageWidth = block.find('.customWidthInput').val();
			}

			if (block.find('.enableAutoWidth').hasClass('btn-success')) {
				imageWidth = 'auto';
			}

			if (result['isListAnswer']) {
				if (result['isText']) {
					var _getImageSrc3 = getImageSrc($(textBlock).find('.text-question').val()),
					    _getImageSrc4 = _slicedToArray(_getImageSrc3, 2),
					    content = _getImageSrc4[0],
					    images = _getImageSrc4[1];

					var editQuestion = {
						id: $(title).val(),
						text: { content: content, images: images },
						image_width: imageWidth
					},
					    editAnswers = [];

					for (var i = 0; i < $(listBlock).find('input[type="text"]').length; i++) {
						editAnswers[i] = {
							id: $($(listBlock).find('input[type="text"]')[i]).attr('data-id'),
							text_question: $($(listBlock).find('input[type="text"]')[i]).val(),
							right_answer: $($(listBlock).find('.indexCol')[i]).text(),
							position: $($(listBlock).find('select')[i]).find('option:selected').val()
						};
					}

					data = {
						settings: result,
						question: editQuestion,
						answers: editAnswers
					};
				} else {
					var _editQuestion = [],
					    _editAnswers = [],
					    inputs = $(listAnswers).find('input'),
					    ids = $(title).val().split(' ');

					for (var _i6 = 0; _i6 < inputs.length; _i6++) {
						_editQuestion[_i6] = {
							id: ids[_i6],
							text_question: $(inputs[_i6]).val()
						};
					}

					inputs = $(listBlock).find('input');

					for (var _i7 = 0; _i7 < inputs.length; _i7++) {
						_editAnswers[_i7] = {
							id: typeof $(inputs[_i7]).attr('data-id') !== 'undefined' ? $(inputs[_i7]).attr('data-id') : null,
							text_question: $(inputs[_i7]).val(),
							right_answer: $($(listBlock).find('select')[_i7]).find('option:selected').val()
						};
					}

					data = {
						settings: result,
						question: _editQuestion,
						answers: _editAnswers
					};
				}
			} else if (result['isText']) {
				var _getImageSrc5 = getImageSrc($(textBlock).find('.text-question').val()),
				    _getImageSrc6 = _slicedToArray(_getImageSrc5, 2),
				    _content = _getImageSrc6[0],
				    _images = _getImageSrc6[1],
				    editTitle = {
					id: $(title).val(),
					text: { content: _content, images: _images },
					image_width: imageWidth
				},
				    _editQuestion2 = [],
				    _editAnswers2 = [];

				for (var _i8 = 0; _i8 < $(questionBlock).find('.category-block').length; _i8++) {
					var _content2 = void 0,
					    _images2 = null,
					    categoryBlock = $(questionBlock).find('.category-block')[_i8],
					    temp = [];

					if ($(categoryBlock).find('input[type="radio"]:checked').val() !== 'custom') {
						imageWidth = $(categoryBlock).find('input[type="radio"]:checked').val();
					} else {
						imageWidth = $(categoryBlock).find('.customWidthInput').val();
					}

					if ($(categoryBlock).find('.enableAutoWidth').hasClass('btn-success')) {
						imageWidth = 'auto';
					}

					if (!_i8) {
						var _getImageSrc7 = getImageSrc($(categoryBlock).find('.text-question').val()),
						    _getImageSrc8 = _slicedToArray(_getImageSrc7, 2),
						    _content3 = _getImageSrc8[0],
						    _images3 = _getImageSrc8[1];

						_editQuestion2[_i8] = {
							id: $(categoryBlock).find('.text-question').attr('data-id'),
							text_question: { content: _content3, images: _images3 },
							image_width: imageWidth
						};
					} else {
						var _getImageSrc9 = getImageSrc($(categoryBlock).find('.question-name').val());

						var _getImageSrc10 = _slicedToArray(_getImageSrc9, 2);

						_content2 = _getImageSrc10[0];
						_images2 = _getImageSrc10[1];


						_editQuestion2[_i8] = {
							id: $(categoryBlock).find('.question-name').attr('data-id'),
							text_question: { content: _content2, images: _images2 },
							image_width: imageWidth
						};
					}

					for (var j = 0; j < $(categoryBlock).find('.item-name').length; j++) {
						var _getImageSrc11 = getImageSrc($($(categoryBlock).find('.item-name')[j]).val());

						var _getImageSrc12 = _slicedToArray(_getImageSrc11, 2);

						_content2 = _getImageSrc12[0];
						_images2 = _getImageSrc12[1];


						temp[j] = {
							id: $($(categoryBlock).find('.item-name')[j]).attr('data-id'),
							text_answer: { content: _content2, images: _images2 },
							answer_right: $($(categoryBlock).find('input[type="checkbox"]')[j]).is(':checked')
						};
					}

					_editAnswers2[_i8] = temp;
				}

				data = {
					settings: result,
					title: editTitle,
					question: _editQuestion2,
					answers: _editAnswers2
				};
			} else {
				var _getImageSrc13 = getImageSrc($(questionBlock).find('.text-question').val()),
				    _getImageSrc14 = _slicedToArray(_getImageSrc13, 2),
				    _content4 = _getImageSrc14[0],
				    _images4 = _getImageSrc14[1],
				    _editQuestion3 = {
					id: $(title).val(),
					text_question: { content: _content4, images: _images4 },
					image_width: imageWidth
				},
				    _editAnswers3 = [];

				for (var _i9 = 0; _i9 < $(questionBlock).find('.item-name').length; _i9++) {
					var _getImageSrc15 = getImageSrc($($(questionBlock).find('.item-name')[_i9]).val()),
					    _getImageSrc16 = _slicedToArray(_getImageSrc15, 2),
					    _content5 = _getImageSrc16[0],
					    _images5 = _getImageSrc16[1];

					_editAnswers3[_i9] = {
						id: $($(questionBlock).find('.item-name')[_i9]).attr('data-id'),
						text_answer: { content: _content5, images: _images5 },
						answer_right: $($(questionBlock).find('input[type="checkbox"]')[_i9]).is(':checked')
					};
				}
				data = {
					settings: result,
					question: _editQuestion3,
					answers: _editAnswers3
				};
			}
			$.ajax({
				url: '/update_edit_eng',
				method: 'post',
				data: data,
				success: function success() {
					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					$('#form_edit_task button:submit').prop('disabled', false);

					getEngQuestions();
					resetEnglishTasks();

					createSwal({
						title: 'Завдання успішно змінено',
						type: 'success'
					});
				},
				error: function error(err) {
					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					$('#form_edit_task button:submit').prop('disabled', false);

					createSwal({
						title: 'Помилка при збереженні змін завдання',
						type: 'error'
					});
					console.log(err);
				}
			});
		});
	} else {
		$('#form_edit_task button:submit').prop('disabled', false);
	}
};

function removeHTML(str) {
	if (str) {
		while (str.indexOf('<') != -1 && str.indexOf('>') != -1 && str.indexOf('>') > str.indexOf('<')) {
			str = str.slice(0, str.indexOf('<')) + str.slice(str.indexOf('>') + 1, str.length);
		}
	}
	return str;
}

module.exports = {
	renderEnglishTask: renderEnglishTask,
	saveEnglishTask: saveEnglishTask,
	getEngQuestions: getEngQuestions,
	deleteEngQuestion: deleteEngQuestion,
	getEngQuestion: getEngQuestion,
	updateEditEng: updateEditEng,
	onChangeListSelect: onChangeListSelect
};

/***/ }),

/***/ 25:
/***/ (function(module, exports) {

var newTaskContainer = '<div class="task-container"></div>';

var booleanAnswer = '\n\t<div class="row booleanAnswer">\n\t\t<label>\n\t\t\t\u041F\u0440\u0430\u0432\u0438\u043B\u044C\u043D\u0430 \u0432\u0456\u0434\u043F\u043E\u0432\u0456\u0434\u044C\n\t\t\t<input type="checkbox">\n\t\t</label>\t\t\t\n\t</div>\n';

var listColContentChoices = {
	'bool': 'Відповідь вірна?',
	'list': 'Відповідний номер в таблиці',
	'default': 'Відповідний номер в тексті'
};

var fillOptions = function fillOptions(isBoolAnswers, nmbQuestions, indexRow, typeSymbolSelect, extraAnswers) {
	var result = [];

	if (isBoolAnswers) {
		var basicChoice = ['<option value="1">\u0422\u0430\u043A</option>', '<option value="0">\u041D\u0456</option>'],
		    currRow = indexRow + 1;
		result.push(basicChoice);

		var optionContent = void 0,
		    optionValue = void 0;

		for (var index = 1; index <= extraAnswers; index++) {
			optionContent = '\u0414\u043E\u0434\u0430\u0442\u043A\u043E\u0432\u0438\u0435 \u043F\u0438\u0442\u0430\u043D\u043D\u044F - ' + index;
			optionValue = 'extra-answer - ' + index;

			result.push('\n\t\t\t\t<option \n\t\t\t\t\tvalue="' + optionValue + '"\n\t\t\t\t\t' + (currRow - nmbQuestions === index ? 'selected' : '') + '\n\t\t\t\t\t>\n\t\t\t\t\t\t' + optionContent + '\n\t\t\t\t\t</option>');
		}
	} else {
		for (var _index = 1; _index <= nmbQuestions + extraAnswers; _index++) {
			var firstLetterAskii = 64,
			    _optionContent = void 0,
			    _optionValue = void 0,
			    currIndex = void 0;
			if (_index > nmbQuestions) {
				currIndex = _index - nmbQuestions;

				_optionContent = '\u0414\u043E\u0434\u0430\u0442\u043A\u043E\u0432\u0438\u0435 \u043F\u0438\u0442\u0430\u043D\u043D\u044F - ' + currIndex;
				_optionValue = 'extra-answer - ' + currIndex;
			} else {
				_optionContent = typeSymbolSelect === 'int' ? _index : String.fromCharCode(firstLetterAskii + _index);
			}

			result.push('\n\t\t\t<option \n\t\t\t\tvalue="' + (_optionValue ? _optionValue : _optionContent) + '"\n\t\t\t\t' + (indexRow + 1 === _index ? 'selected' : '') + '\n\t\t\t>\n\t\t\t\t' + _optionContent + '\n\t\t\t</option>\n\t\t');
		}
	}

	return result;
};

var insertContentCol = function insertContentCol(isBoolAnswers, nmbQuestions, indexRow, typeSymbolSelect, extraAnswers) {
	var firstLetterAskii = 65,
	    currIndex = indexRow + 1;
	var prevVal = void 0,
	    extraIndex = void 0;

	if (isBoolAnswers) {
		var indexExtraAnswers = currIndex - nmbQuestions;
		prevVal = indexExtraAnswers > 0 ? 'extra-answer - ' + indexExtraAnswers : '1';
	} else {
		if (indexRow >= nmbQuestions) {
			extraIndex = currIndex - nmbQuestions;
			prevVal = 'extra-answer - ' + extraIndex;
		} else {
			prevVal = typeSymbolSelect === 'int' ? currIndex : String.fromCharCode(firstLetterAskii + indexRow);
		}
	}

	return '\n\t\t<select data-previous-val="' + prevVal + '">\n\t\t\t' + fillOptions(isBoolAnswers, nmbQuestions, indexRow, typeSymbolSelect, extraAnswers) + '\n\t\t</select>\n\t';
};

var newRowAnswerList = function newRowAnswerList(indexRow) {
	var firstLetterAskii = 65,
	    symbolIndex = String.fromCharCode(firstLetterAskii + indexRow);
	return '<tr>\n\t\t\t<td>' + symbolIndex + '</td>\n\t\t\t<td class="textCol"><input type="text" maxlength="100" required></td>\n\t\t</tr>';
};

var newRowList = function newRowList(nmbQuestions, indexRow, isBooleanAnswers, extraAnswers) {
	var typeSymbolSelect = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 'int';

	var firstLetterAskii = 65,
	    startIndex = 1,
	    symbolIndex = isBooleanAnswers || typeSymbolSelect === 'askii' ? startIndex + indexRow : String.fromCharCode(firstLetterAskii + indexRow);

	return '<tr>\n\t\t\t<td class="indexCol">' + symbolIndex + '</td>\n\t\t\t<td class="textCol"><input type="text" maxlength="100" required></td>\n\t\t\t<td class="resultCol">\n\t\t\t\t' + insertContentCol(isBooleanAnswers, nmbQuestions, indexRow, typeSymbolSelect, extraAnswers) + '\n\t\t\t</td>\n\t\t</tr>';
};

var newManageRowBlock = function newManageRowBlock() {
	return '<tr class="adding-row">\n\t\t\t<td colspan="2">\n\t\t\t\t<buttton class="btn btn-default addQuestionRow">\n\t\t\t\t\t<i class="fas fa-plus-circle"></i>\n\t\t\t\t\t\u0414\u043E\u0434\u0430\u0442\u0438 \u0449\u0435 \u043E\u0434\u0438\u043D \u0432\u0430\u0440\u0456\u0430\u043D\u0442\t\t\n\t\t\t\t</buttton>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<buttton class="btn btn-danger rmQuestionRow" disabled>\n\t\t\t\t\t<i class="fas fa-times-circle"></i>\n\t\t\t\t\t\u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u043E\u0441\u0442\u0430\u043D\u043D\u0456\u0439 \u0432\u0430\u0440\u0456\u0430\u043D\u0442\t\t\n\t\t\t\t</buttton>\n\t\t\t</td>\n\t\t</tr>';
};

var newTextBlock = function newTextBlock() {
	var taskIndex = $('#panel2 .task-container').length + 1;
	return '<div class="text-block">\n\t\t\t<h2>\u0422\u0435\u043A\u0441\u0442 \u0434\u043E \u043F\u0438\u0442\u0430\u043D\u043D\u044F:</h2>\n\t\t\t<div class="imageWidthContainer im_regculation" id="0">\n\t\t\t\t<div class="row col-xs-10">\n\t\t\t\t\t<span>\u0412\u043A\u0430\u0436\u0456\u0442\u044C \u0448\u0438\u0440\u0438\u043D\u0443 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u044C:</span>\n\t\t\t\t\t<label><input type="radio" name="image-width-radio-text-' + taskIndex + '" value="50">50px</label>\n\t\t\t\t\t<label><input type="radio" name="image-width-radio-text-' + taskIndex + '" value="75">75px</label>\n\t\t\t\t\t<label><input type="radio" name="image-width-radio-text-' + taskIndex + '" value="100" checked>100px</label>\n\t\t\t\t\t<label><input type="radio" name="image-width-radio-text-' + taskIndex + '" value="125">125px</label>\n\t\t\t\t\t<label><input type="radio" name="image-width-radio-text-' + taskIndex + '" value="150">150px</label>\n\t\t\t\t\t<label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-text-' + taskIndex + '" aria-label="image-width"></label>\n\t\t\t\t\t<input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>\n\t\t\t\t\t<button class = "changeWidthBtn btn btn-info" disabled>\u0417\u043C\u0456\u043D\u0438\u0442\u0438</button>\n\t\t\t\t\t<button class="enableAutoWidth btn btn-danger">Auto/off</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class="row category" style="margin-bottom: 5px">\n\t\t\t\t<div class="col-xs-12 form-inline" style="width: 100%">\n\t\t\t\t\t<div class="popupImageConvertor">\n\t\t\t\t\t\t<div class="popup-container">\n\t\t\t\t\t\t\t<h3 class="resize-title">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n\t\t\t\t\t\t\t<div class="resizer" >\n\t\t\t\t\t\t\t\t<span class="addition">ctrl+v</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<button class="btn btn-info resizer-result">\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n\t\t\t\t\t\t\t<button class="closePopup">&#10006;</button>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<textarea class="multiblock text-question"></textarea>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>';
};

var newListAnswerBlock = '\n\t<div class="list-answers-block">\n\t\t<table>\n\t\t\t<thead>\n\t\t\t\t<tr>\n\t\t\t\t\t<th>\u2116</th>\n\t\t\t\t\t<th>\u0412\u0430\u0440\u0456\u0430\u043D\u0442</th>\n\t\t\t\t</tr>\n\t\t\t</thead>\n\t\t\t<tbody></tbody>\n\t\t</table>\n\t</div>';

var newListBlock = '\n\t<div class="list-block">\n\t\t<table>\n\t\t\t<thead>\n\t\t\t<tr>\n\t\t\t\t<th>\u2116</th>\n\t\t\t\t<th>\u0422\u0435\u043A\u0441\u0442 \u043F\u0438\u0442\u0430\u043D\u043D\u044F</th>\n\t\t\t\t<th class="change-content-col"></th>\n\t\t\t</tr>\n\t\t\t</thead>\n\t\t\t<tbody></tbody>\n\t\t</table>\n\t</div>';

var questionContainer = '<div class="questions-block"></div>';

var newTestBlock = function newTestBlock(activeTab) {
	var taskContainer = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

	/*
 	find last task container and count nmb questions, we can't add question on existing task
 	if it's adding new task we couldn't find new container
 */
	var indexQuestion = void 0;

	if (activeTab === 'add') {
		var currTaskContainer = taskContainer ? taskContainer : $('#panel2 .task-container:last'),
		    nmbQuestions = currTaskContainer.find('.category-block').length;

		indexQuestion = taskContainer ? $('#panel2 .category-block').length + nmbQuestions + 1 : nmbQuestions + 1;
	} else {
		var _currTaskContainer = $('#panel3 .task-container'),
		    _nmbQuestions = _currTaskContainer.find('.questions-block .category-block').length;

		indexQuestion = _nmbQuestions + 1;
	}

	return '<div class="category-block">\n\t\t\t\t<div class="imageWidthContainer im_regculation">\n\t\t\t\t\t<div class="row col-xs-10">\n\t\t\t\t\t\t<span>\u0412\u043A\u0430\u0436\u0456\u0442\u044C \u0448\u0438\u0440\u0438\u043D\u0443 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u044C:</span>\n\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + indexQuestion + '" value="50">50px</label>\n\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + indexQuestion + '" value="75">75px</label>\n\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + indexQuestion + '" value="100" checked>100px</label>\n\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + indexQuestion + '" value="125">125px</label>\n\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + indexQuestion + '" value="150">150px</label>\n\t\t\t\t\t\t<label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-' + indexQuestion + '" aria-label="image-with"></label>\n\t\t\t\t\t\t<input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>\n\t\t\t\t\t\t<button class = "changeWidthBtn btn btn-info" disabled>\u0417\u043C\u0456\u043D\u0438\u0442\u0438</button>\n\t\t\t\t\t\t<button class="enableAutoWidth btn btn-danger">Auto/off</button>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class="row category" style="margin-bottom: 5px">\n\t\t\t\t\t<div class="col-xs-12 form-inline" style="width: 100%">\n\t\t\t\t\t\t<div class="popupImageConvertor">\n\t\t\t\t\t\t\t<div class="popup-container">\n\t\t\t\t\t\t\t\t<h3 class="resize-title">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n\t\t\t\t\t\t\t\t<div class="resizer" >\n\t\t\t\t\t\t\t\t\t<span class="addition">ctrl+v</span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<button class="btn btn-info resizer-result">\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n\t\t\t\t\t\t\t\t<button class="closePopup">&#10006;</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<textarea class="multiblock question-name"></textarea>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\n\t\t\t\t<div class="items answers-container"></div>\n\t\t\t</div>\n\t\t';
};

var manageButtonsBlock = function manageButtonsBlock() {
	var isFirstRender = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

	return '\n\t\t<div class="manage-buttons-block">\n\t\t\t<button class="btn btn-info add-english-task">\u0414\u043E\u0434\u0430\u0442\u0438 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F</button>\n\t\t\t<button class="btn btn-danger rm-english-task" ' + (isFirstRender ? 'style="display: none;"' : '') + '>\u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F</button>\n\t\t</div>';
};

var questionMultiblock = function questionMultiblock() {
	return '<div class="row category" style="margin-bottom: 5px">\n\t\t\t\t\t<div class="col-xs-12 form-inline" style="width: 100%">\n\t\t\t\t\t\t<div class="popupImageConvertor">\n\t\t\t\t\t\t\t<div class="popup-container">\n\t\t\t\t\t\t\t\t<h3 class="resize-title">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n\t\t\t\t\t\t\t\t<div class="resizer" >\n\t\t\t\t\t\t\t\t\t<span class="addition">ctrl+v</span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<button class="btn btn-info resizer-result">\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n\t\t\t\t\t\t\t\t<button class="closePopup">&#10006;</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<textarea class="multiblock text-question"></textarea>\n\t\t\t\t\t</div>\n\t\t\t\t</div>';
};

var normalMultiblock = function normalMultiblock() {
	return '<div class="row category" style="margin-bottom: 5px">\n\t\t\t\t\t<div class="col-xs-12 form-inline" style="width: 100%">\n\t\t\t\t\t\t<div class="popupImageConvertor">\n\t\t\t\t\t\t\t<div class="popup-container">\n\t\t\t\t\t\t\t\t<h3 class="resize-title">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n\t\t\t\t\t\t\t\t<div class="resizer" >\n\t\t\t\t\t\t\t\t\t<span class="addition">ctrl+v</span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<button class="btn btn-info resizer-result">\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n\t\t\t\t\t\t\t\t<button class="closePopup">&#10006;</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<textarea class="multiblock text-question"></textarea>\n\t\t\t\t\t\t<div class="adding-btns">\n\t\t\t\t\t\t\t<button type="button" class="btn btn-default addNewAnswer">\n\t\t\t\t\t\t\t\t<span class="glyphicon glyphicon-plus"></span>\n\t\t\t\t\t\t\t\t\u0414\u043E\u0434\u0430\u0442\u0438 \u0432\u0430\u0440\u0456\u0430\u043D\u0442\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>';
};

module.exports = {
	listColContentChoices: listColContentChoices,
	booleanAnswer: booleanAnswer,
	newTaskContainer: newTaskContainer,
	newRowAnswerList: newRowAnswerList,
	newRowList: newRowList,
	newManageRowBlock: newManageRowBlock,
	newTextBlock: newTextBlock,
	newListAnswerBlock: newListAnswerBlock,
	newListBlock: newListBlock,
	questionContainer: questionContainer,
	newTestBlock: newTestBlock,
	manageButtonsBlock: manageButtonsBlock,
	questionMultiblock: questionMultiblock,
	normalMultiblock: normalMultiblock
};

/***/ }),

/***/ 27:
/***/ (function(module, exports, __webpack_require__) {

var _require = __webpack_require__(4),
    redactorMultiblock = _require.redactorMultiblock;

var _require2 = __webpack_require__(1),
    addListenersToImages = _require2.addListenersToImages;

var _require3 = __webpack_require__(11),
    addNewAnswer = _require3.addNewAnswer;

var _require4 = __webpack_require__(8),
    deleteAnswer = _require4.deleteAnswer;

var _require5 = __webpack_require__(3),
    createSwal = _require5.createSwal;

function renderFirstQuestion() {
	resetTasks();

	var questionContainer = $('#categories'),
	    selectBlock = $('#block_selector'),
	    addingButtons = questionContainer.find('.adding-btns'),
	    idBlock = $(selectBlock).val();

	$.ajax({
		url: '/get-nmb-answers',
		method: 'post',
		data: { idBlock: idBlock },
		success: function success(response) {
			var countQuestions = response['countQuestions'],
			    weightQuestion = response['weightQuestion'],
			    maxNmbQuestion = response['allowedNmbNewQuestion'],
			    nmbAnswers = response['nmbAnswers'];

			selectBlock.attr('data-nmb_questions', countQuestions);
			selectBlock.attr('data-weight-question', weightQuestion);
			selectBlock.attr('data-max-question', maxNmbQuestion);

			renderFirstAnswers(nmbAnswers);
			questionContainer.show();
			addingButtons.show();

			addListenersToImages();

			if ($('.question-name').closest('.category-block').is(':visible')) {
				var questionBlock = $('#panel2 .questions-block:first .html');
				questionBlock.click();
				$('#panel2 .questions-block:first .question-name').val('');
				questionBlock.click();
			}
		},
		error: function error(err) {
			createSwal({
				title: 'Помилка при спробі створенні нового завдання',
				type: 'error'
			});
			console.log(err);
		}
	});
}

function renderFirstAnswers(nmbAnswers) {
	var answersContainer = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

	var nmbAnswersInput = $('#hidden_countAnswers');
	nmbAnswersInput.val(nmbAnswers);

	answersContainer = answersContainer ? answersContainer : $('#first_answer_item');

	answersContainer.html('');
	for (var countAnswer = 0; countAnswer < nmbAnswers; countAnswer++) {
		answersContainer.append(addNewAnswer(null, false, false, false, true, true));
		answersContainer.find('.deleteAnswer').off('click');
		answersContainer.find('.deleteAnswer').on('click', deleteAnswer);
	}

	var multiblocks = $(answersContainer).parent().find('.multiblock');

	multiblocks.off('click');
	multiblocks.on('click', redactorMultiblock);
}

function resetTasks() {
	$('#panel2 .category-block').show();
	$('.item-category').first().val('');
	$('.category-blocks:not(:first)').remove();

	// clean eng tasks
	$('#panel2 .list-block').hide();
	$('#panel2 .text-block').hide();
	$('#panel2 .manage-buttons-block').hide();
}

module.exports = {
	renderFirstQuestion: renderFirstQuestion,
	renderFirstAnswers: renderFirstAnswers
};

/***/ }),

/***/ 28:
/***/ (function(module, exports) {

module.exports = resetEnglishTasks = function resetEnglishTasks() {
	$('.enableAutoWidth.btn-success').click();

	if ($('#block_selector_for_edit').is(':visible')) {
		var categoryBlock = $('#panel3 .category-block:not(:first)');

		categoryBlock.remove();
		$('#form_edit_task').hide();

		$('#panel3 .list-answers-block').hide();
		$('#panel3 .list-block').hide();
		$('#panel3 button[type="submit"]').hide();
		$('#panel3 .text-block').hide();
		$('#panel3 .questions-block').hide();
		$('#panel3 .customWidthInput').attr('disabled', true);
		$('#panel3 .customWidthInput').val('');
		$('#panel3 .changeWidthBtn').attr('disabled', true);
	} else {
		var tasksToRemove = $('#panel2 .task-container:not(:first)'),
		    textBlock = $('#panel2 .text-block'),
		    listBlock = $('#panel2 .list-block'),
		    rowsListToDelete = listBlock.find('table tbody tr'),
		    listAnswerBlock = $('#panel2 .list-answers-block'),
		    rowsListAnswerToDelete = listAnswerBlock.find('table tbody tr'),
		    testBlock = $('#panel2 .category-block'),
		    testQuestion = testBlock.find('.category .question-name'),
		    answersContainer = testBlock.find('.items'),
		    manageButtonsBlock = $('#panel2 .manage-buttons-block'),
		    deleteTaskBtn = $('#panel2 .task-container .rm-english-task'),
		    questionToRemove = $('#panel2 .questions-block .category-block:not(:first)');

		$('#panel2 .customWidthInput').prop('disabled', true);
		$('#panel2 .customWidthInput').val('');
		$('#panel2 .changeWidthBtn').prop('disabled', true);

		questionToRemove.remove();
		tasksToRemove.remove();
		textBlock.hide();

		listAnswerBlock.hide();
		listBlock.hide();
		rowsListToDelete.remove();
		rowsListAnswerToDelete.remove();

		answersContainer.empty();

		testQuestion.val('');
		testBlock.hide();

		manageButtonsBlock.hide();
		deleteTaskBtn.hide();
	}
};

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function createSwal(params) {
	swal(_extends({}, params, {
		closeOnClickOutside: true,
		allowOutsideClick: true
	}));
}

module.exports = {
	createSwal: createSwal
};

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function createSwal(params) {
	swal(_extends({}, params, {
		closeOnClickOutside: true,
		allowOutsideClick: true
	}));
}

function redactorMultiblock() {

	var _this = this;

	$(this).htmlarea({
		// Override/Specify the Toolbar buttons to show
		toolbar: [["html", "bold", "italic", "underline", "strikethrough"], ["justifyleft", "justifycenter", "justifyright"], [{
			css: "justifyFull",
			text: "по ширині",
			action: function action(btn) {
				this.justifyFull();
			}

		}], [{
			css: "screen",
			text: "Додати зображення",
			action: function action(btn) {
				var _this2 = this;

				var popupImageConvertor = $(this)[0].container.prev();
				popupImageConvertor.css('display', 'flex');

				var el = popupImageConvertor[0].querySelector('.resizer');
				var getRes = popupImageConvertor[0].querySelector('.resizer-result');
				var close = popupImageConvertor.find('.closePopup');
				close.off('click');
				close.on('click', function (e) {
					e.preventDefault();
					el.innerHtml = '';
					popupImageConvertor.css('display', 'none');
				});

				if (el) {
					el.addEventListener('paste', function (e) {

						e.preventDefault();
						var clipboard = e.clipboardData;

						if (clipboard && clipboard.items) {

							var item = clipboard.items[0];

							if (item && item.type.indexOf('image/') > -1) {

								var blob = item.getAsFile();

								if (blob) {

									var reader = new FileReader();
									reader.readAsDataURL(blob);

									reader.onload = function (event) {
										var img = new Image();
										img.src = event.target.result;

										el.innerText = '';

										el.append(img);
										getRes.onclick = function (e) {

											var imageWidth = $(_this).closest('.category-block').find('input[type="radio"]:checked');

											imageWidth = typeof $(imageWidth).val() === 'undefined' ? $(_this).closest('.task-container').find('input[type="radio"]:checked') : imageWidth;

											imageWidth = typeof $(imageWidth).val() === 'undefined' ? $(_this).closest('.text-block').find('input[type="radio"]:checked') : imageWidth;

											imageWidth = typeof $(imageWidth).val() === 'undefined' ? $(_this).closest('.question_name').find('input[type="radio"]:checked') : imageWidth;

											if ($(imageWidth).val() === 'custom') {
												imageWidth = $(imageWidth).parent().next();
											}

											if (imageWidth.closest('.imageWidthContainer').find('.enableAutoWidth').hasClass('btn-success')) {
												imageWidth = 'auto';
											} else {
												imageWidth = $(imageWidth).val();
											}

											e.preventDefault();

											imageWidth = imageWidth ? imageWidth : 10;
											img.setAttribute("style", "width:" + imageWidth + "px");
											var iframe = popupImageConvertor.parent().find('.jHtmlArea iframe').contents().find('body');

											iframe.append(img);

											iframe.focus();

											el.innerHtml = '';

											popupImageConvertor[0].style.display = 'none';

											var parent = $(el).closest('.popup-container').parent();
											$(el).closest('.popup-container').remove();

											parent.prepend("<div class=\"popup-container\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h3 class=\"resize-title\">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"resizer\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"addition\">ctrl+v</span>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"resizer-result btn btn-info\">\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"closePopup\">\u2716</button>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>");
										};

										close.off('click');
										close.on('click', function (e) {
											var parent = $(el).closest('.popup-container').parent();
											$(el).closest('.popup-container').remove();

											parent.prepend("<div class=\"popup-container\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h3 class=\"resize-title\">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"resizer\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"addition\">ctrl+v</span>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"resizer-result btn btn-info\">\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"closePopup\">\u2716</button>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>");

											e.preventDefault();
											try {
												el.innerHtml = '';
											} catch (err) {
												popupImageConvertor = $(_this2)[0].container.prev();
											}
											el.innerHtml = '';
											popupImageConvertor.css('display', 'none');
										});
									};
								}
							}
						}
					});
					getRes.onclick = function (e) {
						e.preventDefault();
						createSwal({
							title: 'Вставте зображення',
							type: 'error'
						});
					};
				}
				$(this).focus();
			}
		}], ["subscript", "superscript"], ["orderedlist", "unorderedlist"]]
	});

	if ($('#edit-block .ToolBar')) {
		$('#edit-block .ToolBar .screen').closest('ul').css("display", "none");
	}
};

module.exports = {
	redactorMultiblock: redactorMultiblock
};

/***/ }),

/***/ 58:
/***/ (function(module, exports, __webpack_require__) {

var createSwal = __webpack_require__(3).createSwal;
var addListenersToImages = __webpack_require__(1).addListenersToImages;
var updateSelectOptions = __webpack_require__(64).updateSelectOptions;
var redactorMultiblock = __webpack_require__(4).redactorMultiblock;

var multiblocks = $('#textEnglish');
multiblocks.off('click');
multiblocks.on('click', redactorMultiblock);
multiblocks = $('#textEnglishEdit');
multiblocks.off('click');
multiblocks.on('click', redactorMultiblock);

function fillEditForms(type) {
	$('#create-block-form').html('');
	$('#edit-block-form').html('');

	var normalAdd = '\n\t\t<div class="row">\n\t\t\t<div class="form-group col-xs-6">\n\t\t\t\t<label>\u041D\u0430\u0437\u0432\u0430 \u043D\u043E\u0432\u043E\u0433\u043E \u0431\u043B\u043E\u043A\u0443</label>\n\t\t\t\t<input type="text" class="form-control required block-name"  maxlength="64" required>\n\t\t\t</div>\n\t\n\t\t\t<div class="form-group col-xs-3">\n\t\t\t\t<label>\u041A\u0456\u043B\u044C\u043A\u0456\u0441\u0442\u044C \u0431\u0430\u043B\u0456\u0432 \u0437\u0430 1 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F</label>\n\t\t\t\t<input type="number" class="form-control required weight-task" min="0.1" max="100" step="0.1" required>\n\t\t\t</div>\n\t\t\t<div class="form-group col-xs-3">\n\t\t\t\t<label>\u041A\u0456\u043B\u044C\u043A\u0456\u0441\u0442\u044C \u0432\u0456\u0434\u043F\u043E\u0432\u0456\u0434\u0435\u0439</label>\n\t\t\t\t<input type="number" class="form-control nmb-answers"  min="1" max="6" step="1" required >\n\t\t\t</div>\n\t\t</div>\n\t\t<button type="submit" class="btn btn-primary center-block">C\u0442\u0432\u043E\u0440\u0438\u0442\u0438 \u0431\u043B\u043E\u043A</button>';

	var normalEdit = '\n\t\t<p class="questionStatistic"></p>\n\t\t<p class="answerStatistic"></p>\n\t\t<div class="row">\n\t\t\t<div class="form-group col-xs-6">\n\t\t\t\t<label>\u041D\u0430\u0437\u0432\u0430 \u0431\u043B\u043E\u043A\u0443</label>\n\t\t\t\t<input type="text" class="form-control required block-name" placeholder="\u041D\u0430\u0437\u0432\u0430 \u0431\u043B\u043E\u043A\u0443" maxlength="64" required>\n\t\t\t</div>\n\t\t\t<div class="form-group col-xs-3">\n\t\t\t\t<label>\u041A\u0456\u043B\u044C\u043A\u0456\u0441\u0442\u044C \u0431\u0430\u043B\u0456\u0432 \u0437\u0430 1 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F</label>\n\t\t\t\t<input type="number" class="form-control required weight-task" placeholder="\u0412\u0430\u0433\u0430" min="0.1" max="100" step="0.1" required>\n\t\t\t</div>\n\t\t\t<div class="form-group col-xs-3">\n\t\t\t\t<label>\u041A\u0456\u043B\u044C\u043A\u0456\u0441\u0442\u044C \u0432\u0456\u0434\u043F\u043E\u0432\u0456\u0434\u0435\u0439</label>\n\t\t\t\t<input type="number" class="form-control nmb-answers" placeholder="\u041A\u0456\u043B-\u0442\u044C \u0432\u0456\u0434\u043F\u043E\u0432\u0456\u0434\u0435\u0439" min="1" max="6" step="1" required>\n\t\t\t</div>\n\t\t</div>\t\n\t\t<button type="submit" class="btn btn-primary center-block">\u0417\u0431\u0435\u0440\u0435\u0433\u0442\u0438 \u0437\u043C\u0456\u043D\u0438</button>';

	var englishAdd = '\n\t\t<div class="row">\n\t\t\t<div class="form-group col-xs-6">\n\t\t\t\t<label>\u041D\u0430\u0437\u0432\u0430 \u043D\u043E\u0432\u043E\u0433\u043E \u0431\u043B\u043E\u043A\u0443</label>\n\t\t\t\t<input type="text" class="form-control required block-name"  maxlength="64" required>\n\t\t\t</div>\n\n\t\t\t<div class="form-group col-xs-3">\n\t\t\t\t<label>\u041A\u0456\u043B\u044C\u043A\u0456\u0441\u0442\u044C \u0431\u0430\u043B\u0456\u0432 \u0437\u0430 1 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F</label>\n\t\t\t\t<input type="number" class="form-control required weight-task" min="0.1" max="100" step="0.1" required>\n\t\t\t</div>\n\t\t\t<div class="form-group col-xs-3">\n\t\t\t\t<label>\u041A\u0456\u043B\u044C\u043A\u0456\u0441\u0442\u044C \u0432\u0456\u0434\u043F\u043E\u0432\u0456\u0434\u0435\u0439</label>\n\t\t\t\t<input type="number" class="form-control nmb-answers"  min="1" max="12" step="1" required >\n\t\t\t</div>\n\t\t</div>\n\t\t<div class="row">\n\t\t\t<div class="form-group col-xs-9" id="taskTitle">\n\t\t\t\t<label>\u0423\u043C\u043E\u0432\u0430 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F</label>\n\t\t\t\t<input type="text" class="form-control block-title"  maxlength="150" required>\n\t\t\t</div>\n\t\t\t<div class="form-group col-xs-3 hidden spareAnsDiv">\n\t\t\t\t<label>\u041A\u0456\u043B\u044C\u043A\u0456\u0441\u0442\u044C \u043B\u0438\u0448\u043D\u0456\u0445 \u0432\u0456\u0434\u043F\u043E\u0432\u0456\u0434\u0435\u0439</label>\n\t\t\t\t<input type="number"  class="form-control spareAnswers" min="0" max="5" step="1">\n\t\t\t</div>\n\t\t</div>\n\t\t<div class="row" id="checkboxes">\n\t\t\t<div class="form-group col-xs-3" id="engText">\n\t\t\t\t<label>\u0412 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u0456 \u043F\u0440\u0438\u0441\u0443\u0442\u043D\u0456\u0439 \u0442\u0435\u043A\u0441\u0442</label>\n\t\t\t\t<input type="checkbox" class="form-control checkText">\n\t\t\t</div>\n\t\t\t<div class="form-group col-xs-6\t">\n\t\t\t\t<label for="checkAnswers">\u0412 \u0443\u043C\u043E\u0432\u0456 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F \u043F\u0440\u0438\u0441\u0443\u0442\u043D\u0456\u0439 \u0441\u043F\u0438\u0441\u043E\u043A \u0432\u0456\u0434\u043F\u043E\u0432\u0456\u0434\u0435\u0439</label>\n\t\t\t\t<input type="checkbox" class="form-control checkAnswers">\n\t\t\t</div>\n\t\t\t<div class="form-group col-xs-3 TFDiv hidden">\n\t\t\t\t<label for="checkTF">\u041F\u0438\u0442\u0430\u043D\u043D\u044F \u0442\u0438\u043F\u0443 true/false</label>\n\t\t\t\t<input type="checkbox" class="form-control checkTF">\n\t\t\t</div>\n\t\t\n\t\t</div>\n\t\t<button type="submit" class="btn btn-primary center-block">C\u0442\u0432\u043E\u0440\u0438\u0442\u0438 \u0431\u043B\u043E\u043A</button>';

	var englishEdit = '\n\t\t<p class="questionStatistic"></p>\n\t\t<p class="answerStatistic"></p>\n\t\t<div class="row">\n\t\t\t<div class="form-group col-xs-6">\n\t\t\t\t<label>\u041D\u0430\u0437\u0432\u0430 \u0431\u043B\u043E\u043A\u0443</label>\n\t\t\t\t<input type="text" class="form-control required block-name"  maxlength="64" required>\n\t\t\t</div>\n\n\t\t\t<div class="form-group col-xs-3">\n\t\t\t\t<label>\u041A\u0456\u043B\u044C\u043A\u0456\u0441\u0442\u044C \u0431\u0430\u043B\u0456\u0432 \u0437\u0430 1 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F</label>\n\t\t\t\t<input type="number" class="form-control required weight-task" min="0.1" max="100" step="0.1" required>\n\t\t\t</div>\n\t\t\t<div class="form-group col-xs-3 hidden">\n\t\t\t\t<label>\u041A\u0456\u043B\u044C\u043A\u0456\u0441\u0442\u044C \u0432\u0456\u0434\u043F\u043E\u0432\u0456\u0434\u0435\u0439</label>\n\t\t\t\t<input type="number" class="form-control nmb-answers"  min="1" max="12" step="1" required >\n\t\t\t</div>\n\t\t</div>\n\t\t<div class="row">\n\t\t\t<div class="form-group col-xs-9" id="taskTitle">\n\t\t\t\t<label>\u0423\u043C\u043E\u0432\u0430 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F</label>\n\t\t\t\t<input type="text" class="form-control block-title required"  maxlength="150" required>\n\t\t\t</div>\n\t\t\t<div class="form-group col-xs-3 hidden spareAnsDiv">\n\t\t\t\t<label>\u041A\u0456\u043B\u044C\u043A\u0456\u0441\u0442\u044C \u043B\u0438\u0448\u043D\u0456\u0445 \u0432\u0456\u0434\u043F\u043E\u0432\u0456\u0434\u0435\u0439</label>\n\t\t\t\t<input type="number"  class="form-control spareAnswers" min="0" max="5" step="1">\n\t\t\t</div>\n\n\t\t</div>\n\t\t<div class="row" id="checkboxes">\n\t\t\t<div class="form-group col-xs-3 hidden" id="engText">\n\t\t\t\t<label>\u0412 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u0456 \u043F\u0440\u0438\u0441\u0443\u0442\u043D\u0456\u0439 \u0442\u0435\u043A\u0441\u0442</label>\n\t\t\t\t<input type="checkbox" class="form-control checkText">\n\t\t\t</div>\n\t\t\t<div class="form-group col-xs-6\thidden">\n\t\t\t\t<label for="checkAnswers">\u0412 \u0443\u043C\u043E\u0432\u0456 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F \u043F\u0440\u0438\u0441\u0443\u0442\u043D\u0456\u0439 \u0441\u043F\u0438\u0441\u043E\u043A \u0432\u0456\u0434\u043F\u043E\u0432\u0456\u0434\u0435\u0439</label>\n\t\t\t\t<input type="checkbox" class="form-control checkAnswers">\n\t\t\t</div>\n\t\t\t<div class="form-group col-xs-3 TFDiv hidden">\n\t\t\t\t<label for="checkTF">\u041F\u0438\u0442\u0430\u043D\u043D\u044F \u0442\u0438\u043F\u0443 true/false</label>\n\t\t\t\t<input type="checkbox" class="form-control checkTF">\n\t\t\t</div>\n\t\t</div>\n\t\t<button type="submit" class="btn btn-primary center-block">\u0417\u0431\u0435\u0440\u0435\u0433\u0442\u0438 \u0437\u043C\u0456\u043D\u0438</button>';

	if (type == '0') {
		$('#create-block-form').append(normalAdd);
		$('#edit-block-form').append(normalEdit);
	} else if (type == '2') {
		$('#create-block-form').append(englishAdd);
		$('#edit-block-form').append(englishEdit);
	}
	$(".checkTF").change(function () {
		if (this.checked) {
			$(".spareAnsDiv").addClass("hidden");
			$(".spareAnswers").val(0);
		} else {
			$(".spareAnsDiv").removeClass("hidden");
		}
	});

	$(".checkText").change(function () {
		if (this.checked && $('.checkAnswers').is(":checked")) {
			$(".TFDiv").removeClass("hidden");
			$(".spareAnsDiv").removeClass("hidden");
		} else {
			$(".checkTF").prop('checked', false);
			$(".TFDiv").addClass("hidden");
			$(".spareAnsDiv").addClass("hidden");
			$(".spareAnswers").val(0);
		}
	});

	$(".checkAnswers").change(function () {
		if (this.checked && $('.checkText').is(":checked")) {
			$(".TFDiv").removeClass("hidden");
			$(".spareAnsDiv").removeClass("hidden");
		} else {
			$(".checkTF").prop('checked', false);
			$(".TFDiv").addClass("hidden");
			$(".spareAnsDiv").addClass("hidden");
			$(".spareAnswers").val(0);
		}
	});
}

function createEnglishBlock(e) {
	$('#create-block-form').find('button').prop('disabled', true);

	e.preventDefault();

	var sendingForm = $('#create-block-form');

	$(document).find('.bookshelf_wrapper').show();
	$(document).find('.hideBehind').show();
	$('#all').css("opacity", "0.5");

	var checkText = void 0,
	    checkAnswers = void 0,
	    checkTF = void 0,
	    spareAnswers = void 0;
	if (sendingForm.find('.checkText').is(":checked")) {
		checkText = 1;
	} else {
		checkText = 0;
	}

	if (sendingForm.find('.checkAnswers').is(":checked")) {
		checkAnswers = 1;
	} else {
		checkAnswers = 0;
	}

	if (sendingForm.find('.checkTF').is(":checked")) {
		checkTF = 1;
	} else {
		checkTF = 0;
	}

	spareAnswers = 0;
	if (checkAnswers && checkText) {
		if (sendingForm.find('.spareAnswers').val() > 0) {
			spareAnswers = sendingForm.find('.spareAnswers').val();
		}
	}

	var data = {
		title: sendingForm.find('.block-title').val(),
		specialityId: $('#id_speciality').val(),
		blockName: sendingForm.find('.block-name').val(),
		weightTask: sendingForm.find('.weight-task').val(),
		nmbAnswers: sendingForm.find('.nmb-answers').val(),
		spareAnswers: spareAnswers,
		checkText: checkText,
		checkAnswers: checkAnswers,
		checkTF: checkTF,
		token: sendingForm.find("input[name='_token']").val()
	};

	return $.ajax({
		url: '/create-english-block',
		method: 'POST',
		data: data,
		success: function success(response) {
			var dataBlock = {
				blockName: data['blockName'],
				blockId: response['idBlock'],
				idSpeciality: response['idSpeciality'],
				nmbAnswers: data['nmbAnswers']
			};

			updateSelectOptions('create_block', dataBlock);
			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");

			createSwal({
				title: 'Блок успішно створено',
				type: 'success'
			});

			sendingForm.find('button').prop('disabled', false);

			cleanInputForm($('#create-block-form'));
			$('.new-block-form').addClass('hidden');
			$(".TFDiv").addClass("hidden");
			$(".spareAnsDiv").addClass("hidden");
		},
		error: function error(_error) {
			console.log(_error);

			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");

			sendingForm.find('button').prop('disabled', false);

			createSwal({
				title: 'Помилка при створені блоку',
				type: 'error'
			});
		},
		finally: function _finally() {
			cleanInputForm(sendingForm);
		}
	});
}

function blockPdf() {
	var id = $('#choose_block_for_view').val();

	$.ajax({
		url: '/generate_block_pdf',
		method: 'POST',
		data: {
			'blockId': id,
			'type': $('#type').text()
		},
		success: function success(response) {
			if (response.url) {
				var blurScreen = $('#blur-screen'),
				    iframe = $('#pdf-frame');

				blurScreen.removeClass('hidden');
				iframe.attr('src', response.url);
				iframe.removeClass('hidden');
			} else {
				createSwal({
					title: 'Питання до данного блоку відсутні',
					type: 'error'
				});
			}
		},
		error: function error(_error2) {
			createSwal({
				title: 'Помилка при генеруванні ПДФ',
				type: 'error'
			});

			console.log(_error2);
		}
	});
}

function setLangBlock(id) {
	$.ajax({
		url: '/set_lang_block',
		method: 'POST',
		data: { 'id': id },
		success: function success(response) {
			$('#blockLangName').val(response);
		},
		error: function error(err) {
			console.log(err);
		}
	});
}

function setBlockData() {
	var blockId = $('#choose-select').val();

	addListenersToImages();

	var data = {
		'blockId': blockId
	};

	$.ajax({
		url: '/get-block',
		method: 'POST',
		data: data,
		success: function success(response) {
			$('#textEnglishEdit').val('');
			var editBlockContainer = $('.edit-block-container'),
			    editBlockForm = $('#edit-block-form');
			$(".TFDiv").addClass("hidden");
			$(".spareAnsDiv").addClass("hidden");

			updateBlockData(editBlockContainer, editBlockForm, response.data, response.english, response.block);
		},
		error: function error() {
			createSwal({
				title: 'Помилка при спробі отримання даних блоку',
				type: 'error'
			});
		}
	});
}

function createNewBlock(e) {
	$('#create-block-form').find('button').prop('disabled', true);

	e.preventDefault();

	var sendingForm = $('#create-block-form'),
	    weightTask = sendingForm.find('.weight-task').val();

	$(document).find('.bookshelf_wrapper').show();
	$(document).find('.hideBehind').show();
	$('#all').css("opacity", "0.5");

	var data = {
		title: sendingForm.find('.block-title').val(),
		specialityId: $('#id_speciality').val(),
		blockName: sendingForm.find('.block-name').val(),
		weightTask: weightTask,
		nmbAnswers: sendingForm.find('.nmb-answers').val(),
		token: sendingForm.find("input[name='_token']").val()
	};

	$.ajax({
		url: '/create-block',
		method: 'POST',
		data: data,
		success: function success(response) {
			var dataBlock = {
				blockName: data['blockName'],
				blockId: response['idBlock'],
				idSpeciality: response['idSpeciality'],
				nmbAnswers: data['nmbAnswers']
			};

			updateSelectOptions('create_block', dataBlock);

			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");

			createSwal({
				title: 'Блок успішно створено',
				type: 'success'
			});

			cleanInputForm($('#create-block-form'));
			$('.new-block-form').addClass('hidden');

			sendingForm.find('button').prop('disabled', false);
		},
		error: function error(_error3) {
			console.log(_error3);

			sendingForm.find('button').prop('disabled', false);

			createSwal({
				title: 'Помилка при створені блоку',
				type: 'error'
			});
		},
		finally: function _finally() {
			cleanInputForm(sendingForm);
		}
	});
}

function updateBlock(e) {
	e.preventDefault();

	var editForm = $(e.currentTarget),
	    chooseSelect = $('#choose-select'),
	    weightTask = editForm.find('.weight-task').val();

	$(document).find('.bookshelf_wrapper').show();
	$(document).find('.hideBehind').show();
	$('#all').css("opacity", "0.5");

	var type = 0,
	    checkText = void 0,
	    checkAnswers = void 0,
	    checkTF = void 0;
	if (editForm.find('.checkText').is(":checked")) {
		checkText = 1;
	} else {
		checkText = 0;
	}
	if (editForm.find('.checkAnswers').is(":checked")) {
		checkAnswers = 1;
	} else {
		checkAnswers = 0;
	}
	if (editForm.find('.checkTF').is(":checked")) {
		checkTF = 1;
	} else {
		checkTF = 0;
	}

	var conf = {
		blockId: chooseSelect.val(),
		spareAnswers: editForm.find('.spareAnswers').val(),
		checkText: checkText,
		checkAnswers: checkAnswers,
		checkTF: checkTF
	};

	if (checkText != 0 || checkAnswers != 0 || checkTF != 0) {
		type = 1;
	}

	var data = {
		title: editForm.find('.block-title').val(),
		type: type,
		blockId: chooseSelect.val(),
		blockName: editForm.find('.block-name').val(),
		weightTask: weightTask,
		nmbAnswers: editForm.find('.nmb-answers').val(),
		token: editForm.find("input[name='_token']").val()
	};

	$.ajax({
		url: '/update-block',
		method: 'POST',
		data: data,
		success: function success() {
			var editBlockContainer = $('.edit-block-container');
			updateSelectOptions('change_block', [data['blockId'], data['blockName']]);
			$('#block_selector_for_edit').find('option[value="' + data['blockId'] + '"]').attr('name', data['nmbAnswers']);

			editBlockContainer.addClass('hidden');

			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");

			$('#textEnglishEdit').val('');

			$('#blockControl [data-toggle]').hide();

			createSwal({
				title: 'Дані блоку успішно змінені',
				type: 'success'
			});
			//for english
			if (data.type == 1) {
				$.ajax({
					url: '/update-conf',
					method: 'POST',
					data: conf,
					error: function error(err) {
						console.log(err);

						createSwal({
							title: 'Помилка при спробі зміни конфігурації блоку',
							type: 'error'
						});
					}
				});
			}
		},
		error: function error(err) {
			console.log(err);

			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");

			$('#blockControl [data-toggle]').hide();

			createSwal({
				title: 'Помилка при спробі зміни даних блоку',
				type: 'error'
			});
		}
	});
}

function deleteBlock() {
	var isConfirm = confirm('Ви дійсно хочете видалити даний блок?'),
	    idSpeciality = $('#id_speciality').val();

	if (isConfirm) {
		$(document).find('.bookshelf_wrapper').show();
		$(document).find('.hideBehind').show();
		$('#all').css("opacity", "0.5");

		var idBlock = $('#choose-select').val();

		var data = {
			'idBlock': idBlock,
			'idSpeciality': idSpeciality
		};

		$.ajax({
			url: '/delete-block',
			method: 'POST',
			data: data,
			success: function success(response) {
				var chooseBlockContainer = $('#edit-block .choose-container'),
				    createNewBlockContainer = $('#edit-block .new-block-form');

				if (parseInt(response['nmbBlocks']) === 0) {
					chooseBlockContainer.addClass('hidden');
					createNewBlockContainer.removeClass('hidden');
					createNewBlockContainer.show();
				}

				$('.edit-block-container').addClass('hidden');

				updateSelectOptions('delete_block', idBlock);

				if (idBlock != null) {
					createSwal({
						title: 'Блок успішно видалено',
						type: 'success'
					});
					$('#edit-block').find("span[data-toggle='tooltip']").hide();
				}

				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");
			},
			error: function error(err) {
				createSwal({
					title: 'Помилка при спробі видалення блоку',
					type: 'error'
				});

				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");

				console.log(err);
			}
		});
	}
}

function updateBlockData(blockContainer, form, data, english, block) {
	blockContainer.toggleClass('hidden');
	form.find('.checkText').prop('checked', false);
	form.find('.checkAnswers').prop('checked', false);
	form.find('.checkTF').prop('checked', false);

	form.find('.block-name').val(data['block_name']);
	form.find('.weight-task').val(data['weight_question']);
	form.find('.weight-task').attr('data-prev-weight', data['weight_question']);
	form.find('.nmb-answers').val(data['countAnswers']);
	form.find('.block-title').val(data['task_title']);
	form.find('.spareAnswers').val(english['extra_answers']);

	if (english['text'] == "1") {
		form.find('.checkText').prop('checked', true);
	}

	if (english['answer_list'] == "1") {
		form.find('.checkAnswers').prop('checked', true);
	}

	if (english['true_false'] == "1") {
		form.find('.checkTF').prop('checked', true);
	}

	if (block != '1') {
		form.find('.checkText').parent().removeClass('hidden');
		form.find('.checkAnswers').parent().removeClass('hidden');
		form.find('.nmb-answers').parent().removeClass('hidden');

		if (form.find('.checkText').prop('checked') && form.find('.checkAnswers').prop('checked')) {
			form.find('.checkTF').parent().removeClass('hidden');

			if (!form.find('.checkTF').prop('checked')) {
				form.find('.spareAnswers').parent().removeClass('hidden');
			}
		}
	} else {
		form.find('.checkText').parent().addClass('hidden');
		form.find('.checkAnswers').parent().addClass('hidden');
		form.find('.checkTF').parent().addClass('hidden');
		form.find('.spareAnswers').parent().addClass('hidden');
		form.find('.nmb-answers').parent().addClass('hidden');
	}
}

function cleanInputForm(form) {
	var inputs = $(form).find("input:not([type='hidden'])"),
	    checks = $(form).find("input:checkbox");

	inputs.map(function (index, input) {
		$(input).val('');
	});
	checks.map(function (index, check) {
		$(check).prop('checked', false);
	});
}

module.exports = {
	createEnglishBlock: createEnglishBlock,
	blockPdf: blockPdf,
	createNewBlock: createNewBlock,
	updateBlock: updateBlock,
	setLangBlock: setLangBlock,
	setBlockData: setBlockData,
	deleteBlock: deleteBlock,
	fillEditForms: fillEditForms
};

/***/ }),

/***/ 59:
/***/ (function(module, exports) {

module.exports = getSettings = function getSettings() {
	var nmbAnswers = 4;

	var id = $('#block_selector option:selected').val();

	if ($('#block_selector_for_edit').is(':visible')) {
		id = $('#block_selector_for_edit option:selected').val();
	}

	var data = {
		'block_id': id
	};

	return new Promise(function (resolve) {
		$.ajax({
			url: '/get_settings',
			method: 'post',
			data: data,
			success: function success(response) {
				var setting = response['settings'];

				if (setting.true_false == 0 && setting.text == 1) $('#answersCount').val(response.countAnswers);else $('#answersCount').val(0);
				var englishConfiguration = {
					isText: !!setting['text'],
					isListAnswer: !!setting['answer_list'],
					isBooleanAnswers: !!setting['true_false'],
					extraAnswers: setting['extra_answers'],
					nmbQuestions: response['countAnswers'],
					nmbAnswers: nmbAnswers,
					blockId: data['block_id']
				};

				resolve(englishConfiguration);
			},
			error: function error(err) {
				console.log(err);
			}
		});
	});
};

/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

var _require = __webpack_require__(1),
    setWidthImage = _require.setWidthImage;

module.exports = function () {
	var autoWidth = $('.enableAutoWidth');

	autoWidth.off('click');
	autoWidth.on('click', function (e) {
		e.preventDefault();

		var curBtn = $(e.currentTarget),
		    widthContainer = curBtn.closest('.imageWidthContainer');

		if (curBtn.hasClass('btn-danger')) {
			widthContainer.find('input').prop('disabled', true);
			widthContainer.find('.changeWidthBtn').prop('disabled', true);

			curBtn.removeClass('btn-danger');
			curBtn.addClass('btn-success');
			curBtn.text('Auto/on');

			setWidthImage('auto', widthContainer.find('input').first());
		} else {
			widthContainer.find('input[type="radio"]').prop('disabled', false);

			curBtn.addClass('btn-danger');
			curBtn.removeClass('btn-success');
			curBtn.text('Auto/off');

			widthContainer.find('input:radio[value="100"]').prop('checked', true);
			widthContainer.find('.customWidthInput').val('');

			setWidthImage(100, widthContainer.find('input').first());
		}
	});
};

/***/ }),

/***/ 64:
/***/ (function(module, exports) {

function updateSelectOptions(action, data) {
	var addTaskSelect = $('#block_selector'),
	    editTaskSelect = $('#block_selector_for_edit'),
	    editBlockSelect = $('#choose-select'),
	    selectArr = [addTaskSelect, editTaskSelect, editBlockSelect];

	if (action === 'create_block') {
		var isArray = Array.isArray(data);

		if (isArray) {
			selectArr.map(function (curSelect) {
				$(curSelect).empty();
			});

			var _loop = function _loop(block) {
				var newOption = '<option value="' + block['blockId'] + '" data-id-speciality="' + block['idSpeciality'] + '" name="' + block['countAnswers'] + '">' + block['blockName'] + '</option>';
				selectArr.map(function (curSelect) {
					curSelect.append(newOption);
				});
			};

			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = data[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var block = _step.value;

					_loop(block);
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}

			editBlockSelect.append('<option value="new_block">Створити новий блок</option>');
		} else {
			selectArr.map(function (curSelect) {
				curSelect.children('[value="new_block"]').remove();
			});

			var _newOption = '<option value="' + data['blockId'] + '" data-id-speciality="' + data['idSpeciality'] + '" name="' + data['nmbAnswers'] + '">' + data['blockName'] + '</option>';

			editBlockSelect.append('<option value="new_block">Створити новий блок</option>');
			selectArr.map(function (curSelect) {
				if (curSelect.attr('id') === 'choose-select') {
					$(curSelect).find('option:last-of-type').before(_newOption);
				} else {
					$(curSelect).append(_newOption);
				}
			});
		}
	} else if (action === 'change_block') {
		selectArr.map(function (curSelect) {
			$(curSelect).find('option[value=' + data[0] + ']').text(data[1]);
		});
	} else if (action === 'delete_block') {
		selectArr.map(function (curSelect) {
			$(curSelect).find('option[value=' + data + ']').remove();
		});
	}
	$(editBlockSelect).val('');

	$('.addSelectContainer').show();
}

module.exports = {
	updateSelectOptions: updateSelectOptions
};

/***/ }),

/***/ 65:
/***/ (function(module, exports, __webpack_require__) {

var createSwal = __webpack_require__(3).createSwal;

var _require = __webpack_require__(66),
    manageContentTab = _require.manageContentTab,
    filterOptionSelect = _require.filterOptionSelect;

var handleNewTask = __webpack_require__(84).handleNewTask;
var updateSelectOptions = __webpack_require__(64).updateSelectOptions;
var setLangBlock = __webpack_require__(58).setLangBlock;

function openDeclined(reason) {
	createSwal({
		title: '\u041F\u0440\u0438\u0447\u0438\u043D\u0430 \u0432\u0456\u0434\u043C\u043E\u0432\u0438: ' + reason,
		type: 'info'
	});
}

function changeSelector() {
	$(document).find('.bookshelf_wrapper').show();
	$(document).find('.hideBehind').show();
	$('#all').css("opacity", "0.5");
	$('.customWidthInput').attr('disabled', true);
	$('.customWidthInput').val('');

	var user_mail = $("#data_user_email").val();
	var status_selector = $("#id_speciality option:selected").text();
	var id_speciality = $('#id_speciality').val();
	var token = $("#send-form").children().first().val();
	var data = {
		user_mail: user_mail,
		status_selector: status_selector,
		id_speciality: id_speciality,
		token: token
	};

	$.ajax({
		method: 'post',
		url: '/get_status',
		success: function success(response) {
			response.speciality.forEach(function (element) {
				if (element.id == id_speciality) {

					if (element.status == "in_development") {
						$("#specialty_status").text("в розробці");
					} else if (element.status == "in_process") {
						$("#specialty_status").text("на перевірці");
					} else if (element.status == "approved") {
						$("#specialty_status").text("підтверджено");
					} else {
						$("#specialty_status").text("на доопрацюванні");
					}

					if (element.status == "declined") {
						$("#statusButton").show();
						$("#statusButton").attr('onClick', 'openDeclined(\'' + element.declined_reason + '\')');
					} else {
						$("#statusButton").hide();
					}

					$('#type').text(element.type);
					if (element.type != "1") {
						$('#edit-block .choose-container').removeClass('hidden');
						if (element.type == "2") $('#EditBlock').text("Конфігурація завдань");else $('#EditBlock').text("Управління блоками");
						$('#langBlank').hide();
						$('#blank').show();

						$('#editLangBlock').hide();
					} else {
						$('#block_selector').text('');
						$('.hideAlert').hide();

						$('#EditBlock').text("Зміст завдання");
						$('#langBlank').show();
						$('#blank').hide();

						$('#editLangBlock').show();

						$('#updateLangBlock').off('click');
						$('#updateLangBlock').on('click', updateLangBlock);

						setLangBlock($('#id_speciality').val());
					}
					handleNewTask();
				}
			});
		},
		error: function error(_error) {
			console.log(_error);
		},
		complete: function complete() {
			$.ajax({
				method: 'post',
				url: '/send_selector',
				data: data,
				success: function success(result) {
					addTaskSelect = $('#block_selector').empty();
					editTaskSelect = $('#block_selector_for_edit').empty();
					if (result.status) {
						changeSpeciality(result.blocks, result.blank_status);
					} else if (result.error === 'cheater') {
						changeSpeciality(result.blocks, result.blank_status);
						createSwal({
							title: "Неправомірна дія",
							type: 'error'
						});
					}
				},
				error: function error(_error2) {
					console.log(_error2);
				},
				complete: function complete() {
					manageContentTab();
				}
			});
		}
	});
}

function updateLangBlock() {
	var id_speciality = $('#id_speciality').val();
	var newName = $('#blockLangName').val();

	$.ajax({
		url: '/update_lang_block',
		method: 'POST',
		data: { 'id': id_speciality,
			'name': newName },
		success: function success(response) {
			$('#blockLangName').val(response);
			createSwal({
				title: 'Дані блоку оновлені',
				type: 'success'
			});
		},

		error: function error(_error3) {
			createSwal({
				title: 'Помилка при спробі зміни даних блоку',
				type: 'error'
			});
		}
	});
}

function changeSpeciality(blocks, status) {
	filterOptionSelect('#choose-select');

	var sendingButton = $('#sendDiv');

	if (status === 'in_process' || 'approved') {
		sendingButton.attr('disabled');
	} else {
		sendingButton.removeAttr('disabled');
		sendingButton.removeAttr('title');
	}

	if (blocks.length > 0) {
		updateSelectOptions('create_block', blocks);
	}
}

module.exports = {
	changeSelector: changeSelector,
	openDeclined: openDeclined,
	updateLangBlock: updateLangBlock
};

/***/ }),

/***/ 66:
/***/ (function(module, exports, __webpack_require__) {

var getLangQuestions = __webpack_require__(8).getLangQuestions;

var _require = __webpack_require__(67),
    loadComm = _require.loadComm,
    updateSumm = _require.updateSumm;

var _require2 = __webpack_require__(58),
    createNewBlock = _require2.createNewBlock,
    createEnglishBlock = _require2.createEnglishBlock,
    fillEditForms = _require2.fillEditForms;

function cleanValidation(element) {
	$(element).parent().removeClass("access-validate");
	$(element).parent().removeClass("not-validate");
}

function manageContentTab() {

	var currTab = $('.panel-heading .nav-tabs .active'),
	    currTabHref = $(currTab).find('a').attr('href'),
	    nmbBlocks = $('#choose-select').find('option:not(".hidden")').length;

	if (currTabHref === '#panel1') {
		var curInfoBlock = $('.empty-content-list-task'),
		    blankContainer = $('#blank');

		$('#errorNoTasks').hide();
		if ($('#type').text() === "1") {
			getLangShow();
		} else {
			$('#errorNoTasks').hide();
			if (nmbBlocks > 1) {
				blankContainer.show();
				curInfoBlock.hide();
				getDataBlank();
			} else {
				blankContainer.hide();
				curInfoBlock.show();
			}
		}
	} else if (currTabHref === '#panel2') {
		/**** clean validation ****/

		cleanValidation($('#categ').find('.category-block').find(".questionsLang"));
		cleanValidation($('#categ').find('.category-block').find(".answersLang"));
		cleanValidation($('#categories .category').find('.question-name'));
		$('#categ').find('.category-block').find(".validate-alert").remove();
		$('#categories .category').find(".validate-alert").remove();
		$('#send-form .items .item').find(".validate-alert").remove();

		$('#addingQuestions').hide();
		$('#addingLang').hide();
		$('.approvedDivShow').hide();
		$('.DivShow').hide();

		if ($('#specialty_status').text() === "підтверджено") {
			$('.approvedDivShow').show();
		} else if ($('#specialty_status').text() === "на перевірці") {
			$('.DivShow').show();
		} else {
			if ($('#type').text() == "1") {
				$('#addingLang').show();
			} else {
				$('#addingQuestions').show();
			}
			var selectContainer = $('.addSelectContainer'),
			    chooseBlockSelect = $('#block_selector'),
			    categoriesContainer = $('#categories'),
			    _curInfoBlock = $('.empty-content-add-task');

			if (nmbBlocks > 1) {
				chooseBlockSelect.val('');
				categoriesContainer.hide();
				if ($('#type').text() == "0") {
					selectContainer.show();
				}
				_curInfoBlock.hide();
			} else {
				categoriesContainer.hide();
				selectContainer.hide();
				_curInfoBlock.show();
			}
			$('#panel2 .category-delete').hide();

			$('#limit-questions').addClass('hidden');
			$('#categ .category-delete').hide();
		}
	} else if (currTabHref === '#panel3') {

		var curInfoQuestion = $('#panel3 .empty-task');
		$('#lang_edit_block_element').hide();
		$('#editingQuestions').hide();
		$('.approvedDivShow').hide();
		$('.DivShow').hide();
		$('#langEmpty').hide();
		$('#langAnswerEdit').hide();
		$('#questions_edit_block_elements').hide();
		$('#form_edit_task').hide();
		$('#changingLang').hide();

		if ($('#specialty_status').text() === "підтверджено") {
			$('.approvedDivShow').show();
		} else if ($('#specialty_status').text() === "на перевірці") {
			$('.DivShow').show();
		} else {

			var editTaskSelect = $('#panel3 .edit-task-select'),
			    _curInfoBlock2 = $('.empty-content-edit-task');

			if ($('#type').text() == "1") {

				$('#lang_edit_block_element').show();
				getLangQuestions();
			} else {

				$('#editingQuestions').show();
				curInfoQuestion.addClass('hidden');
			}

			$('#block_selector_for_edit').val('');

			if (nmbBlocks > 1 && $('#type').text() != "1") {
				editTaskSelect.show();
				_curInfoBlock2.hide();
			} else if ($('#type').text() != "1") {
				editTaskSelect.hide();
				_curInfoBlock2.show();
			}
		}
	} else if (currTabHref === '#edit-block') {

		var createBlockForm = $('#create-block-form'),
		    editBlockContainer = $('.edit-block-container'),
		    selectBlock = $('#choose-select');

		editBlockContainer.addClass('hidden');
		selectBlock.val('');

		$('#blockControl [data-toggle]').hide();

		$('#taskTitle').hide();
		$('#taskTitleEdit').hide();
		$('#engTextEdit').hide();
		$('#engText').hide();
		$('#blockControlling').hide();
		$('#blockCreateForm').hide();
		$('#editLangBlock').hide();
		$('.approvedDivShow').hide();
		$('.DivShow').hide();

		if ($('#specialty_status').text() === "підтверджено") {
			$('.approvedDivShow').show();
		} else if ($('#specialty_status').text() === "на перевірці") {
			$('.DivShow').show();
		} else {
			if ($('#type').text() == "1") {
				$('#editLangBlock').show();
			} else {
				var typeSpeciality = $('#type').text();
				createBlockForm.off('submit');
				if (typeSpeciality == "0") {
					createBlockForm.on('submit', function (e) {
						return createNewBlock(e);
					});
				} else {
					createBlockForm.on('submit', function (e) {
						return createEnglishBlock(e);
					});
				}
				fillEditForms(typeSpeciality);

				$('#blockControlling').show();
			}
		}
	} else if (currTabHref === '#verify') {
		$('#verify_no_questions').addClass('hidden');
		$('#comm').addClass('hidden');
		$('#choose_specialty').off('change', updateBlockTable);
		$('#choose_specialty').on('change', updateBlockTable);
		$('#block_table').addClass('hidden');
		$('#choose_specialty').val('');
		$("#full").addClass("hidden");
		$("#sendDiv").addClass("hidden");
		$('.blue-block').addClass('hidden');
	} else if (currTabHref === '#viewBlock') {
		$('#blockView').hide();
		$('#viewNoBlock').hide();
		updateViewSelect($('#id_speciality').val());
	}

	$(document).find('.bookshelf_wrapper').hide();
	$(document).find('.hideBehind').hide();
	$('#all').css("opacity", "1");
}

function updateBlockTable() {
	$("#comm,#sendDiv").addClass("hidden");
	$("#full").addClass("hidden");

	$.ajax({
		method: 'POST',
		url: '/get-speciality-param',
		data: { 'id': $("#choose_specialty").val() },
		success: function success(response) {
			var table = $("#block_table");

			if (response.type == 1 && response.questions > 0) {

				$("#full").removeClass("hidden");
				$('#verify_no_questions').addClass('hidden');
				$("#comm,#sendDiv").removeClass("hidden");
				$("#comm").find('.category-block').remove();
				$('#sendDiv').prop("disabled", false);
				loadComm();
				$("#block_table").addClass("hidden");
				$(".blue-block").addClass("hidden");
				$("#full").addClass("hidden");
			} else if (response.type == 1) {
				$('#verify_no_questions').removeClass('hidden');
				$('#sendDiv').prop("disabled", true);
				$('#full').addClass("hidden");
				$("#block_table").addClass("hidden");
				$(".blue-block").addClass("hidden");
				$("#comm,#sendDiv").removeClass("hidden");
				$("#comm").find('.category-block').remove();
				$("#full").addClass("hidden");

				loadComm();
			} else if (response.questions > 0 || response.english_questions > 0 || response.connections > 0) {

				$("#full").removeClass("hidden");
				$('#verify_no_questions').addClass('hidden');
				$("#full").removeClass("hidden");
				$('#sendDiv').prop("disabled", true);
				$("#overCount").text("");

				$("#block_table").removeClass("hidden");
				$(".blue-block").removeClass("hidden");
				$("#full,#comm,#sendDiv").removeClass("hidden");
				$("#comm").find('.category-block').remove();
				table.html('');
				table.append('<tr>\n                                <th>\u041D\u0430\u0437\u0432\u0430 \u0431\u043B\u043E\u043A\u0443</th>\n                                <th>\u041A\u0456\u043B\u044C\u043A\u0456\u0441\u0442\u044C \u0437\u0430\u043F\u043E\u0432\u043D\u0435\u043D\u0438\u0445 \u0437\u0430\u0432\u0434\u0430\u043D\u044C \u0432 \u0431\u043B\u043E\u0446\u0456</th>\n                                <th>\u041A\u0456\u043B\u044C\u043A\u0456\u0441\u0442\u044C \u0431\u0430\u043B\u0456\u0432 \u0437\u0430 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F</th>\n                                <th>\u0412\u0432\u0435\u0434\u0456\u0442\u044C \u043A\u0456\u043B\u044C\u043A\u0456\u0441\u0442\u044C \u0437\u0430\u0432\u0434\u0430\u043D\u044C \u0432 \u0431\u0456\u043B\u0435\u0442\u0456</th>\n                                <th>\u0421\u0443\u043C\u0430 \u0431\u0430\u043B\u0456\u0432 \u0437\u0430 \u0431\u043B\u043E\u043A</th>\n                            </tr>');
				var spec = $("#choose_specialty").val();
				$.ajax({
					method: 'POST',
					url: '/get-stuff',

					success: function success(response) {

						response.blocks.forEach(function (element) {
							if (element.id_speciality == spec) {

								var countFilledQuestions = 0;
								response.questions.forEach(function (el) {
									if (el.id_block == element.id) {
										countFilledQuestions++;
									}
								});

								response.english_questions.forEach(function (el) {
									if (el.block_id == element.id) {
										countFilledQuestions++;
									}
								});

								response.english_text.forEach(function (el) {
									if (el.block_id == element.id) {
										countFilledQuestions++;
									}
								});

								response.connection.forEach(function (el) {
									if (el.block_id == element.id) {
										countFilledQuestions++;
									}
								});

								if (element.type == "2") {
									table.append('<tr> <td class = "hidden"><div class = "ids">' + element.id + '</div></td>\n                                                <td><div>' + element.block_name + '</div></td>\n                                                 <td><div>' + countFilledQuestions + '</div></td>\n                                                 <td><div class = "markPer">' + element.weight_question + '</div></td>" +\n                                                 <td><input type = "number" min = "0" max = "1" value = "' + element.count_chosed_questions + '" class = "inputTasks"></td>" +\n                                                 <td><div class = "summ">' + element.weight_question * element.count_chosed_questions + ' </div></td> \n                                             </tr>');
								} else {
									table.append('<tr> <td class = "hidden"><div class = "ids">' + element.id + '</div></td>\n                                                <td><div>' + element.block_name + '</div></td>\n                                                 <td><div>' + countFilledQuestions + '</div></td>\n                                                 <td><div class = "markPer">' + element.weight_question + '</div></td>" +\n                                                 <td><input type = "number" min = "0" max = "' + countFilledQuestions + '" value = "' + element.count_chosed_questions + '" class = "inputTasks"></td>" +\n                                                 <td><div class = "summ">' + element.weight_question * element.count_chosed_questions + ' </div></td> \n                                             </tr>');
								}
							}
							$(".inputTasks").off('input');
							$(".inputTasks").on('input', updateSumm);
						});

						var sum = 0;

						$('.summ').each(function () {
							sum += parseFloat($(this).text());
						});

						$("#fullCount").text(sum);

						if (sum === 100) {
							$('#sendDiv').prop("disabled", false);
						} else {
							$('#sendDiv').prop("disabled", true);
						}
					},
					error: function error(_error) {
						console.log(_error);
					}
				});
				loadComm();
			} else {

				$("#full").addClass("hidden");
				$(".blue-block").addClass("hidden");

				table.html('');

				$('#verify_no_questions').removeClass('hidden');
			}
		},
		error: function error(_error2) {
			console.log(_error2);
		}
	});
}

function updateViewSelect(id) {
	$.ajax({
		url: '/get_blocks',
		method: 'POST',
		data: { 'id': id },
		success: function success(response) {
			if (response.length > 0) {
				$('#blockView').show();

				var select = $('#choose_block_for_view');
				select.text('');
				response.forEach(function (block) {
					var newOption = '<option value="' + block.id + '" >' + block.block_name + '</option>';
					select.append(newOption);
				});
				select.val('');
			} else {
				$('#viewNoBlock').show();
			}
		},

		error: function error(_error3) {
			console.log(_error3);
		}
	});
}

function filterOptionSelect(select) {
	var idSpeciality = $('#id_speciality').val();

	var options = $(select).find("option:not([value='new_block'])"),
	    nmbCurrentOptions = 0;

	options.map(function (index, option) {
		var specialityId = $(option).attr('data-id-speciality');

		if (specialityId === idSpeciality) {
			$(option).removeClass('hidden');
			nmbCurrentOptions++;
		} else {
			$(option).addClass('hidden');
		}
	});

	$(select).val('');
}

function getDataBlank() {
	var specialityId = $('#id_speciality').val();

	$.ajax({
		url: '/get-speciality-tasks',
		method: 'post',
		data: {
			id: specialityId,
			type: $('#type').text()
		},
		success: function success(response) {
			renderBlank(response);

			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");
		},
		error: function error(err) {
			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");

			console.log(err);
		},
		complete: function complete() {
			var currTab = $('.panel-heading .nav-tabs .active'),
			    currTabHref = $(currTab).find('a').attr('href');

			if (currTabHref === '#panel1') {
				$('#panel1').show();
			}
		}
	});
}

function renderEnglish(data, id) {
	var result = "",
	    q = 1;

	var _iteratorNormalCompletion = true;
	var _didIteratorError = false;
	var _iteratorError = undefined;

	try {
		for (var _iterator = data.english_config[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
			var conf = _step.value;

			if (conf.block_id == id) {
				var j = 1;

				var _iteratorNormalCompletion2 = true;
				var _didIteratorError2 = false;
				var _iteratorError2 = undefined;

				try {
					for (var _iterator2 = data.english_text[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
						var text = _step2.value;

						if (text.block_id == id) {
							result += '<div class = "engQuestionsDiv"> \u041F\u0438\u0442\u0430\u043D\u043D\u044F ' + q + ' </div>';
							q++;

							if (conf.answer_list == 1 && conf.text == 1) {
								var _iteratorNormalCompletion8 = true;
								var _didIteratorError8 = false;
								var _iteratorError8 = undefined;

								try {

									for (var _iterator8 = data.english_questions[Symbol.iterator](), _step8; !(_iteratorNormalCompletion8 = (_step8 = _iterator8.next()).done); _iteratorNormalCompletion8 = true) {
										var _question = _step8.value;

										if (_question.id_english_question == text.id) {

											result += '<li class = "question_name" style="margin-left:70px">';

											if (conf.true_false == 0) {
												result += _question.right_answer;
											} else {
												result += j + '_____';
												j++;
											}

											result += ' ' + _question.text_question + ' </li>';
										}
									}
								} catch (err) {
									_didIteratorError8 = true;
									_iteratorError8 = err;
								} finally {
									try {
										if (!_iteratorNormalCompletion8 && _iterator8.return) {
											_iterator8.return();
										}
									} finally {
										if (_didIteratorError8) {
											throw _iteratorError8;
										}
									}
								}

								j = 1;
								var i = 1,
								    english_text = text.text;

								result += '<li class = "question_name">';

								while (english_text.indexOf('NNN') !== -1) {
									english_text = english_text.replace(/NNN/, i + '_____');
									i++;
								}

								result += english_text + '</li>';
								j = 1;

								var _iteratorNormalCompletion9 = true;
								var _didIteratorError9 = false;
								var _iteratorError9 = undefined;

								try {
									for (var _iterator9 = data.english_questions[Symbol.iterator](), _step9; !(_iteratorNormalCompletion9 = (_step9 = _iterator9.next()).done); _iteratorNormalCompletion9 = true) {
										var _question2 = _step9.value;

										if (_question2.id_english_question == id) {
											j++;

											var _iteratorNormalCompletion10 = true;
											var _didIteratorError10 = false;
											var _iteratorError10 = undefined;

											try {
												for (var _iterator10 = data.english_answers[Symbol.iterator](), _step10; !(_iteratorNormalCompletion10 = (_step10 = _iterator10.next()).done); _iteratorNormalCompletion10 = true) {
													var _answer = _step10.value;

													if (_answer.id_english_question == _question2.id) {
														var _contentAnswer = _answer.answer_right ? '<b>' + _answer.text_answer + '</b>' : _answer.text_answer;

														result += '<div class = "englishAnsStep" >' + _contentAnswer + '</div>';
													}
												}
											} catch (err) {
												_didIteratorError10 = true;
												_iteratorError10 = err;
											} finally {
												try {
													if (!_iteratorNormalCompletion10 && _iterator10.return) {
														_iterator10.return();
													}
												} finally {
													if (_didIteratorError10) {
														throw _iteratorError10;
													}
												}
											}

											result += '</li>';
										}
									}
								} catch (err) {
									_didIteratorError9 = true;
									_iteratorError9 = err;
								} finally {
									try {
										if (!_iteratorNormalCompletion9 && _iterator9.return) {
											_iterator9.return();
										}
									} finally {
										if (_didIteratorError9) {
											throw _iteratorError9;
										}
									}
								}

								j = 1;
							} else if (conf.text == 1 && conf.answer_list == 0) {

								var _english_text = text.text;

								result += '<li class = "question_name">';
								var _i = 1;

								while (_english_text.indexOf('NNN') != -1) {
									_english_text = _english_text.replace(/NNN/, _i + '_____');
									_i++;
								}
								result += _english_text + '</li>';

								j = 1;
								var _iteratorNormalCompletion11 = true;
								var _didIteratorError11 = false;
								var _iteratorError11 = undefined;

								try {
									for (var _iterator11 = data.english_questions[Symbol.iterator](), _step11; !(_iteratorNormalCompletion11 = (_step11 = _iterator11.next()).done); _iteratorNormalCompletion11 = true) {
										var _question3 = _step11.value;

										if (_question3.id_english_question == text.id) {
											result += '<li class = "question_name"> <div class = "answerEngNumb" > ' + j + ' </div> ' + _question3.text_question;
											j++;

											var _ans2 = "A";

											var _iteratorNormalCompletion12 = true;
											var _didIteratorError12 = false;
											var _iteratorError12 = undefined;

											try {
												for (var _iterator12 = data.english_answers[Symbol.iterator](), _step12; !(_iteratorNormalCompletion12 = (_step12 = _iterator12.next()).done); _iteratorNormalCompletion12 = true) {
													var _answer2 = _step12.value;

													if (_answer2.id_english_question == _question3.id) {

														var _contentAnswer2 = _answer2.answer_right ? '<b>' + _ans2 + ' ' + _answer2.text_answer + '</b>' : _ans2 + ' ' + _answer2.text_answer;

														result += '<div class = "englishAnsStep" >' + _contentAnswer2 + '</div>';
														_ans2 = String.fromCharCode(_ans2.charCodeAt(0) + 1);
													}
												}
											} catch (err) {
												_didIteratorError12 = true;
												_iteratorError12 = err;
											} finally {
												try {
													if (!_iteratorNormalCompletion12 && _iterator12.return) {
														_iterator12.return();
													}
												} finally {
													if (_didIteratorError12) {
														throw _iteratorError12;
													}
												}
											}

											result += '</li>';
										}
									}
								} catch (err) {
									_didIteratorError11 = true;
									_iteratorError11 = err;
								} finally {
									try {
										if (!_iteratorNormalCompletion11 && _iterator11.return) {
											_iterator11.return();
										}
									} finally {
										if (_didIteratorError11) {
											throw _iteratorError11;
										}
									}
								}

								j = 1;
							}
						}
					}
				} catch (err) {
					_didIteratorError2 = true;
					_iteratorError2 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion2 && _iterator2.return) {
							_iterator2.return();
						}
					} finally {
						if (_didIteratorError2) {
							throw _iteratorError2;
						}
					}
				}

				if (conf.text == 0 && conf.answer_list == 0) {

					j = 1;

					var _iteratorNormalCompletion3 = true;
					var _didIteratorError3 = false;
					var _iteratorError3 = undefined;

					try {
						for (var _iterator3 = data.english_questions[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
							var question = _step3.value;

							if (question.block_id == id) {
								result += '<li class = "question_name"> <div class = "answerEngNumb" > ' + j + ' </div> ' + question.text_question;
								j++;
								var ans = "A";

								var _iteratorNormalCompletion4 = true;
								var _didIteratorError4 = false;
								var _iteratorError4 = undefined;

								try {
									for (var _iterator4 = data.english_answers[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
										var answer = _step4.value;

										if (answer.id_english_question == question.id) {
											var contentAnswer = answer.answer_right ? '<b>' + ans + ' ' + answer.text_answer + '</b>' : ans + ' ' + answer.text_answer;

											result += '<div class = "englishAnsStep">' + contentAnswer + '</div>';
											ans = String.fromCharCode(ans.charCodeAt(0) + 1);
										}
									}
								} catch (err) {
									_didIteratorError4 = true;
									_iteratorError4 = err;
								} finally {
									try {
										if (!_iteratorNormalCompletion4 && _iterator4.return) {
											_iterator4.return();
										}
									} finally {
										if (_didIteratorError4) {
											throw _iteratorError4;
										}
									}
								}

								result += '</li>';
							}
						}
					} catch (err) {
						_didIteratorError3 = true;
						_iteratorError3 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion3 && _iterator3.return) {
								_iterator3.return();
							}
						} finally {
							if (_didIteratorError3) {
								throw _iteratorError3;
							}
						}
					}
				} else if (conf.answer_list == 1 && conf.text == 0) {
					var _iteratorNormalCompletion5 = true;
					var _didIteratorError5 = false;
					var _iteratorError5 = undefined;

					try {

						for (var _iterator5 = data.connections[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
							var connection = _step5.value;

							if (connection.block_id == id) {
								j = 1;
								var _ans = "A",
								    row = '<tr>';

								result += '<table class = "taskTable"><tr>';

								var _iteratorNormalCompletion6 = true;
								var _didIteratorError6 = false;
								var _iteratorError6 = undefined;

								try {
									for (var _iterator6 = connection['answers'][Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
										var currAnswer = _step6.value;

										result += '<td> ' + currAnswer.text_question + ' </td>';
										row += '<th>' + _ans + '</th>';
										_ans = String.fromCharCode(_ans.charCodeAt(0) + 1);
									}
								} catch (err) {
									_didIteratorError6 = true;
									_iteratorError6 = err;
								} finally {
									try {
										if (!_iteratorNormalCompletion6 && _iterator6.return) {
											_iterator6.return();
										}
									} finally {
										if (_didIteratorError6) {
											throw _iteratorError6;
										}
									}
								}

								result += '</tr>' + row + '</tr></table>';
								result += '<li class = "question_name">';

								var _iteratorNormalCompletion7 = true;
								var _didIteratorError7 = false;
								var _iteratorError7 = undefined;

								try {
									for (var _iterator7 = connection['questions'][Symbol.iterator](), _step7; !(_iteratorNormalCompletion7 = (_step7 = _iterator7.next()).done); _iteratorNormalCompletion7 = true) {
										var currQuestion = _step7.value;

										result += '<div class = "englishAnsStep"> ' + j + ' ' + currQuestion.text_question + ' </div>';
										j++;
									}
								} catch (err) {
									_didIteratorError7 = true;
									_iteratorError7 = err;
								} finally {
									try {
										if (!_iteratorNormalCompletion7 && _iterator7.return) {
											_iterator7.return();
										}
									} finally {
										if (_didIteratorError7) {
											throw _iteratorError7;
										}
									}
								}

								result += '</li>';
							}
						}
					} catch (err) {
						_didIteratorError5 = true;
						_iteratorError5 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion5 && _iterator5.return) {
								_iterator5.return();
							}
						} finally {
							if (_didIteratorError5) {
								throw _iteratorError5;
							}
						}
					}
				}
			}
		}
	} catch (err) {
		_didIteratorError = true;
		_iteratorError = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion && _iterator.return) {
				_iterator.return();
			}
		} finally {
			if (_didIteratorError) {
				throw _iteratorError;
			}
		}
	}

	return result;
}

function renderBlank(data) {
	var blankContainer = $('#blank');

	blankContainer.html('');

	var blocks = data.blocks;

	var _iteratorNormalCompletion13 = true;
	var _didIteratorError13 = false;
	var _iteratorError13 = undefined;

	try {
		for (var _iterator13 = blocks[Symbol.iterator](), _step13; !(_iteratorNormalCompletion13 = (_step13 = _iterator13.next()).done); _iteratorNormalCompletion13 = true) {
			var block = _step13.value;

			blankContainer.append('<li><h2>\u0411\u043B\u043E\u043A: ' + block['block_name'] + '</h2></li>');

			if (block.task_title != null) {
				blankContainer.append('<li><b>\u0423\u043C\u043E\u0432\u0430 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F: ' + block['task_title'] + '</b></li>');
			}

			if (block.type == "1") {
				blankContainer.append('<li>' + block['blank_text'] + '</li>');
			}

			if (block.url_image != '0') {
				blankContainer.append('<li><b> </b><div class="image-container">\n                                                        <div class="responsiveImg" style="background-image: url(\'' + block['url_image'] + '\')"></div>\n            </div></li>');
			}

			var questions = [];

			if ($('#type').text() == "2") {
				var blockyBlock = renderEnglish(data, block.id);
				blankContainer.append(blockyBlock);
			} else if (block['questions'].length) {
				blankContainer.append('<ol class="questions questions-block-' + block['id'] + '"></ol>');
				var question = '';

				var _iteratorNormalCompletion14 = true;
				var _didIteratorError14 = false;
				var _iteratorError14 = undefined;

				try {
					for (var _iterator14 = block['questions'][Symbol.iterator](), _step14; !(_iteratorNormalCompletion14 = (_step14 = _iterator14.next()).done); _iteratorNormalCompletion14 = true) {
						var currQuestion = _step14.value;

						question += '<li>\n                                <p>' + currQuestion['text_question'] + '</p>\n                             </li>';

						question += '<ul class="answers">';

						var _iteratorNormalCompletion15 = true;
						var _didIteratorError15 = false;
						var _iteratorError15 = undefined;

						try {
							for (var _iterator15 = currQuestion['answers'][Symbol.iterator](), _step15; !(_iteratorNormalCompletion15 = (_step15 = _iterator15.next()).done); _iteratorNormalCompletion15 = true) {
								var answer = _step15.value;

								question += answer['answer_right'] ? '<b><li> <p>' + answer['text_answer'] + '</p></li></b>' : '<li> <p>' + answer['text_answer'] + '</p></li></p>';
							}
						} catch (err) {
							_didIteratorError15 = true;
							_iteratorError15 = err;
						} finally {
							try {
								if (!_iteratorNormalCompletion15 && _iterator15.return) {
									_iterator15.return();
								}
							} finally {
								if (_didIteratorError15) {
									throw _iteratorError15;
								}
							}
						}

						question += '</ul>';
					}
				} catch (err) {
					_didIteratorError14 = true;
					_iteratorError14 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion14 && _iterator14.return) {
							_iterator14.return();
						}
					} finally {
						if (_didIteratorError14) {
							throw _iteratorError14;
						}
					}
				}

				questions.push(question);

				blankContainer.find('.questions-block-' + block['id']).append(questions);
			} else {
				blankContainer.append('<li class="no-content">Питання до даного блоку відсутні</li>');
			}
		}
	} catch (err) {
		_didIteratorError13 = true;
		_iteratorError13 = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion13 && _iterator13.return) {
				_iterator13.return();
			}
		} finally {
			if (_didIteratorError13) {
				throw _iteratorError13;
			}
		}
	}
}

function getLangShow() {
	var spec_id = $('#id_speciality').val();

	$.ajax({
		url: '/get-ukrainian-tasks',
		method: 'post',
		data: { spec_id: spec_id },
		success: function success(response) {
			var questions = "";

			$.ajax({
				url: '/set_lang_block',
				method: 'POST',
				data: { 'id': spec_id },
				success: function success(res) {
					var blankContainer = $('#langBlank');
					if (response.answers.length === 0) {
						$('#errorNoTasks').show();
						blankContainer.hide();
					} else {
						$('#errorNoTasks').hide();
						blankContainer.show();
						blankContainer.html('');

						questions += "<h2>" + res + "</h2>";

						for (var i = 0; i < response.size; i++) {
							questions += '<ol class = "row"><span class="form-group col-xs-6">\n                                        <label>\u041F\u0438\u0442\u0430\u043D\u043D\u044F</label>\n                                        <p>' + response.questions[i].text_question + '</p>\n                                    </span>\n                                    <span class="form-group col-xs-6">\n                                        <label>\u0412\u0456\u0434\u043F\u043E\u0432\u0456\u0434\u044C</label>\n                                        <p>' + response.answers[i] + '</p>\n                                    </span></ol>';
						}

						$('#get_lang_edit').val('');
						blankContainer.append(questions);
					}
				},

				error: function error(_error4) {
					console.log(_error4);
				}
			});
		},
		error: function error(err) {
			console.log(err);
		},
		complete: function complete() {
			var tab = $('.panel-heading .nav-tabs .active').find('a').attr('href');
			if (tab === '#panel1') {
				$('#panel1').show();
			}
		}
	});
}

module.exports = {
	manageContentTab: manageContentTab,
	filterOptionSelect: filterOptionSelect
};

/***/ }),

/***/ 67:
/***/ (function(module, exports, __webpack_require__) {

var createSwal = __webpack_require__(3).createSwal;

function newCommDiv(id, position, name) {
	position = position != null ? position : '';
	name = name != null ? name : '';
	var div = '<div class="category-block" style="display: block;">\n                <div class="row" style="margin-bottom: 5px">\n                    <div class="col-xs-12 form-inline" style="width: 100%">\n                        <div class = "hidden idsComm">' + id + '</div>\n                        <div style="margin-left: 10%; margin-right: 10%; box-shadow: 0 0 10px rgba(0,0,0,0.5); padding-top: 3%; padding-bottom: 3%; margin-bottom: 3%; display: flex; justify-content: space-between">\n                            <div style="margin-left: 3%; width: 100%">\n                                \u041F\u043E\u0441\u0430\u0434\u0430 \u0447\u043B\u0435\u043D\u0430 \u043A\u043E\u043C\u0456\u0441\u0456\u0457 <br>\n                                <input class="item-category form-control commPosition" style="width: 90%" type="text" required value = ' + position + '> <br>\n                                \u041F\u0406\u0411 \u0447\u043B\u0435\u043D\u0430 \u043A\u043E\u043C\u0456\u0441\u0456\u0457 <br>\n                                <input class="item-category form-control commName" style="width: 90%" type="text" required value = ' + name + '>\n                            </div>\n                            <div style="margin-right: 3%; margin-top: 4%">\n                                <span  class = "category-add-comm" data-toggle="tooltip" title="\u0414\u043E\u0434\u0430\u0442\u0438 \u0447\u043B\u0435\u043D\u0430 \u043A\u043E\u043C\u0456\u0441\u0456\u0457">\n                                    <button type="button"  class="btn btn-default">\n                                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>\n                                    </button>\n                                </span>\n                                <span class="category-delete-comm" data-toggle="tooltip" title="\u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u0447\u043B\u0435\u043D\u0430 \u043A\u043E\u043C\u0456\u0441\u0456\u0457" style="display: none;">\n                                    <button type="button" class="btn btn-default">\n                                        <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>\n                                    </button>\n                                </span>\n                            </div>\n                        </div>\n                    </div> \n                </div>';
	return div;
}

function loadComm() {
	if ($("#choose_specialty").val()) {
		var spec = $("#choose_specialty").val();

		$.ajax({
			method: 'POST',
			url: '/get-comm',
			data: { 'id': spec },
			success: function success(response) {
				response.forEach(function (element) {
					var name = element.commissioner_name;
					var position = element.commissioner_position;
					var id = element.id;
					var newComm = newCommDiv(id, position, name);
					$('#comm').append(newComm);
				});
				var nmbComm = $("#comm").find('.category-block').length;

				if (nmbComm === 0) {
					var emptyComm = newCommDiv(0, null, null);

					$('#comm').append(emptyComm);
				}
				update();
			},
			error: function error(_error) {
				console.log(_error);
			}
		});
	} else {
		update();
	}
}

function update() {
	var newComm = newCommDiv(0, null, null),
	    rmCommBtn = $('.category-delete-comm'),
	    addCommBtn = $('.category-add-comm'),
	    commContainer = $('#comm');
	updateListeners();

	var nmbComm = $(commContainer).find('.category-block').length;
	if (nmbComm > 1) {
		commContainer.find('.category-delete-comm').show();
	}

	function updateListeners() {
		rmCommBtn = $('.category-delete-comm');
		addCommBtn = $('.category-add-comm');

		addCommBtn.off('click');
		addCommBtn.on('click', addComm);

		rmCommBtn.off('click');
		rmCommBtn.on('click', rmComm);
	}
	function addComm() {
		var nmbComm = $(commContainer).find('.category-block').length;

		if (nmbComm < 5) {
			$('#comm').append(newComm);
			updateListeners();
			if (nmbComm > 0) {
				commContainer.find('.category-delete-comm').show();
			}
		} else {
			createSwal({
				title: 'Максимальна к-ть членів комісії',
				type: 'error'
			});
		}

		if (nmbComm == 1) {
			commContainer.first().find('.category-delete-comm').show();
		}
	}

	function rmComm() {
		var nmbComm = $(commContainer).find('.category-block').length,
		    comm = $(this).parent().parent().parent().parent().parent();

		if (nmbComm > 1) {
			comm.remove();
			var id = comm.find('.idsComm').text();
			if (id != 0) {
				$.ajax({
					method: 'POST',
					url: '/delete_comm',
					data: { 'id': id },
					success: function success(response) {
						console.log(response);
					},
					error: function error(_error2) {
						console.log(_error2);
					}
				});
			}
			updateListeners();
		}

		if (nmbComm == 2) {
			commContainer.first().find('.category-delete-comm').hide();
		}

		commContainer.find('.category-add-comm').toArray().map(function (el) {
			return $(el).show();
		});
	}
}

function updateSumm() {
	if (+$(this).val() > +$(this).attr("max")) {
		$(this).val($(this).attr("max"));
	}

	var sum = 0,
	    count = $(this).val(),
	    mark = $(this).parent().parent().find(".markPer").text();
	$(this).parent().parent().find(".summ").text((mark * count).toFixed(1));
	$('.summ').each(function () {
		sum += parseFloat($(this).text());
	});

	$("#fullCount").text(sum);

	if (sum > 100) {
		$("#overCount").text("Сума більше 100");
	} else {
		$("#overCount").text('');
	}

	if (sum === 100) {
		$('#sendDiv').prop("disabled", false);
	} else {
		$('#sendDiv').prop("disabled", true);
	}
}

function sendOmu() {

	var specId = $("#choose_specialty").val();
	var counts = new Array();
	var ids = new Array();
	var commIds = new Array();
	var commNames = new Array();
	var commPositions = new Array();
	var ifNull = 1;
	var i = 0;

	$(".inputTasks").each(function () {
		counts[i] = $(this).val();
		i++;
	});

	i = 0;
	$(".ids").each(function () {
		ids[i] = $(this).text();
		i++;
	});

	i = 0;
	$(".commPosition").each(function () {
		commPositions[i] = $(this).val();
		if (!$(this).val()) ifNull = null;
		i++;
	});

	i = 0;
	$(".commName").each(function () {
		if (!$(this).val()) ifNull = null;
		commNames[i] = $(this).val();
		i++;
	});

	i = 0;
	$(".idsComm").each(function () {
		commIds[i] = $(this).text();
		i++;
	});

	if (ifNull) {
		$('#statusButton').addClass("hidden");
		$(".blue-block").addClass("hidden");
		$("#choose_specialty option:selected").remove();
		$("#choose_specialty").val('');
		$("#block_table").html('');

		if ($("#id_speciality").val() === specId) {
			$("#specialty_status").text("на перевірці");
		}

		$("#full, #sendDiv, #comm").addClass("hidden");

		$.ajax({
			method: 'POST',
			url: '/send_specialty',
			data: {
				'specId': specId,
				'ids': ids,
				'counts': counts,
				'commId': commIds,
				'commNames': commNames,
				'commPosition': commPositions
			},
			success: function success(response) {
				createSwal({
					title: 'Відправлено на верифікацію',
					type: 'success'
				});
			},
			error: function error(_error3) {
				createSwal({
					title: 'Помилка при відправленні на верифікацію',
					type: 'success'
				});
				console.log(_error3);
			}
		});
	} else {
		createSwal({
			title: 'Введіть дані про всіх обраних членів комісії',
			type: 'error'
		});
	}
}

module.exports = {
	loadComm: loadComm,
	updateSumm: updateSumm,
	sendOmu: sendOmu
};

/***/ }),

/***/ 7:
/***/ (function(module, exports) {

function getImageSrc(str) {
	var arraySrc = str.match(/src="(.*?)"/g);

	if (arraySrc != null) {
		var newarray = [];
		for (var i = 0; i < arraySrc.length; i++) {
			newarray.push(arraySrc[i].match(/"(.*?)"/)[1]);
		}
		str = str.replace(/src="(.*?)"/g, 'src="URLPATH"');
		return [str, newarray];
	} else {
		return [str, ''];
	}
}

module.exports = getImageSrc;

/***/ }),

/***/ 8:
/***/ (function(module, exports, __webpack_require__) {

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var createSwal = __webpack_require__(3).createSwal;
var redactorMultiblock = __webpack_require__(4).redactorMultiblock;
var addNewAnswer = __webpack_require__(11).addNewAnswer;
var getImageSrc = __webpack_require__(7);

var _require = __webpack_require__(1),
    setDefaultImageWidth = _require.setDefaultImageWidth,
    setWidthImage = _require.setWidthImage,
    addListenersToImages = _require.addListenersToImages;

var _require2 = __webpack_require__(12),
    renderEnglishTask = _require2.renderEnglishTask,
    updateEditEng = _require2.updateEditEng;

var _require3 = __webpack_require__(25),
    normalMultiblock = _require3.normalMultiblock;

var setWidthListeners = __webpack_require__(6);

var langBlock = '<div class="category-block" style="display: block;">\n\t\t\t\t\t\t<div class="imageWidthContainer im_regculation">\n\t\t\t\t\t\t\t<div class="row col-xs-10">\n\t\t\t\t\t\t\t\t<span>\u0412\u043A\u0430\u0436\u0456\u0442\u044C \u0448\u0438\u0440\u0438\u043D\u0443 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u044C:</span>\n\t\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-2" value="50">50px</label>\n\t\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-2" value="75">75px</label>\n\t\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-2" value="100" checked>100px</label>\n\t\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-2" value="125">125px</label>\n\t\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-2" value="150">150px</label>\n\t\t\t\t\t\t\t\t<label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-2" aria-label="image-with"></label>\n\t\t\t\t\t\t\t\t<input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>\n\t\t\t\t\t\t\t\t<button class = "changeWidthBtn btn btn-info" disabled>\u0417\u043C\u0456\u043D\u0438\u0442\u0438</button>\n\t\t\t\t\t\t\t\t<button class="enableAutoWidth btn btn-danger">Auto/off</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n                        <div class="row category" style="margin-bottom: 5px">\n                            <div class="col-xs-12 form-inline" style="width: 100%">\n                                    <div class="lang-item">\n                                        <div class="lang-area">\n                                            <div class="popupImageConvertor">\n                                                <div class="popup-container">\n                                                    <h3 class="resize-title">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n                                                    <div class="resizer" >\n                                                        <span class="addition">ctrl+v</span>\n                                                    </div>\n                    \n                                                    <button class="resizer-result btn btn-info" class="btn btn-info">\u0412\u0441\u0442\u0430\u0432\u0438\u0442\u0438 \u0432\u0438\u0434\u0456\u043B\u0435\u043D\u0435 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n                                                    <button class="closePopup">&#10006;</button>\n                                                   \n                                                </div>\n                                            </div>\n                                    \n                                            <textarea id="lang_question" maxlength="1000" class="multiblock form-control text_area_lang"></textarea>\n                                         </div>\n                                         <div class="lang-area">\n                                            <div class="popupImageConvertor">\n                                                <div class="popup-container">\n                                                    <h3 class="resize-title">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n                                                    <div class="resizer" >\n                                                        <span class="addition">ctrl+v</span>\n                                                    </div>\n                    \n                                                    <button class="resizer-result btn btn-info" class="btn btn-info">\u0412\u0441\u0442\u0430\u0432\u0438\u0442\u0438 \u0432\u0438\u0434\u0456\u043B\u0435\u043D\u0435 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n                                                    <button class="closePopup">&#10006;</button>\n                                                   \n                                                </div>\n                                            </div>\n                                            <textarea id="lang_answers" maxlength="1000" class="multiblock form-control text_area_lang"></textarea>\n                                        </div>\n                                    </div>\n                                <div class="btn-space">\n                                    <span class="category-delete" data-toggle="tooltip" title="" data-original-title="\u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u043F\u0438\u0442\u0430\u043D\u043D\u044F">\n                                      <button id="deleteLang" type="button" class="btn btn-default">\n                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"> \u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u043F\u0438\u0442\u0430\u043D\u043D\u044F</span>\n                                      </button>\n                                    </span>\n\t\t\t\t\t\t\t\t</div>\n                            </div>\n                        </div>\n                    </div>';

$('#panel3 .multiblock').off('click');
$('#panel3 .multiblock').on('click', redactorMultiblock);

var countAnswers = $('#block_selector_for_edit option:selected').attr('name');

var setCustomImageWidth = function setCustomImageWidth() {
	var addChangeWidthBtn = $('#panel3 .changeWidthBtn');

	addChangeWidthBtn.off('click');
	addChangeWidthBtn.on('click', function (e) {
		e.preventDefault();

		var imageWidth = $(e.currentTarget).prev().val();

		setWidthImage(imageWidth, $(e.currentTarget).prev().prev().find('input'));
	});
};

function getLangQuestions() {
	var spec_id = $('#id_speciality').val();

	$.ajax({
		url: '/get-ukrainian-tasks',
		method: 'post',
		data: { spec_id: spec_id },
		success: function success(response) {
			$('#get_lang_edit').html('');

			if (response.size > 0) {
				for (var i = 0; i < response.size; i++) {
					var text = response.questions[i].text_question;
					text = removeHTML(text);
					if (text == "") {
						text = "Зображення";
					}

					$('#get_lang_edit').append($('<option>', {
						id: response.questions[i].text_question,
						value: response.questions[i].id,
						text: text.substr(0, 120) + '...',
						name: response.answers[i],
						'data-width': response.questions[i].image_width
					}));
				}

				$('#panel3 .empty-task').addClass('hidden');
				$('#lang_edit_block_element').removeClass('hidden');
			} else {
				$('#langEmpty').show();
				$('#panel3 .empty-task').removeClass('hidden');
				$('#lang_edit_block_element').addClass('hidden');
			}

			$('#get_lang_edit').val('');
		},
		error: function error(err) {
			console.log(err);
		}
	});
}

function getQuestionLang() {
	if ($('#changingLang .category-block').length === 0) {
		$('#changingLang').prepend(langBlock);
		$('#changingLang .multiblock').on('click', redactorMultiblock);
		setWidthListeners();
		addListenersToImages();
	}

	setCustomImageWidth();
	$('.enableAutoWidth.btn-success').click();

	$('#changingLang').show();

	var imageWidth = $('#get_lang_edit option:selected').attr('data-width'),
	    radio = $('#changingLang').find('input[type="radio"]');

	if (imageWidth !== 'auto') {
		$(radio[5]).parent().next().prop('disabled', true);
		$(radio[5]).parent().next().next().prop('disabled', true);
		$('#changingLang .customWidthInput').val('');
		switch (imageWidth) {
			case '50':
				$(radio[0]).prop('checked', true);
				break;
			case '75':
				$(radio[1]).prop('checked', true);
				break;
			case '100':
				$(radio[2]).prop('checked', true);
				break;
			case '125':
				$(radio[3]).prop('checked', true);
				break;
			case '150':
				$(radio[4]).prop('checked', true);
				break;
			default:
				$(radio[5]).prop('checked', true);
				$(radio[5]).parent().next().prop('disabled', false);
				$(radio[5]).parent().next().next().prop('disabled', false);
				$(radio[5]).parent().next().val(imageWidth);
		}
	} else {
		$('#changingLang .enableAutoWidth').click();
	}

	$('#lang_question').val($('#get_lang_edit option:selected').attr('id'));
	$('#lang_answers').val($('#get_lang_edit option:selected').attr("name"));
	$('#panel3 #lang_answers').click();
	$('#panel3 #lang_question').click();

	$('#panel3 .html.highlighted').click();
}

function getQuestions() {
	countAnswers = $('#block_selector_for_edit option:selected').attr('name');

	$('#tasks_selector_for_edit').html("");

	var editTaskSelect = $('#block_selector_for_edit'),
	    idBlock = editTaskSelect.val(),
	    selectQuestionContainer = $('#questions_edit_block_elements');

	$.ajax({
		url: '/get-questions',
		method: 'post',
		data: { idBlock: idBlock },
		success: function success(response) {
			if (response.length > 0) {
				$('#questions_edit_block_elements [data-toggle="tooltip"] button').prop("disabled", false);
				var _iteratorNormalCompletion = true;
				var _didIteratorError = false;
				var _iteratorError = undefined;

				try {
					for (var _iterator = response[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
						var question = _step.value;

						question['text_question'] = removeHTML(question['text_question']);
						if (question['text_question'] == "") {
							$('#tasks_selector_for_edit').append('<option value="' + question['id'] + '" name = "image" >\u0417\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</option>');
						} else {
							$('#tasks_selector_for_edit').append('<option value="' + question['id'] + '" name = "normal">' + question['text_question'] + '</option>');
						}
					}
				} catch (err) {
					_didIteratorError = true;
					_iteratorError = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion && _iterator.return) {
							_iterator.return();
						}
					} finally {
						if (_didIteratorError) {
							throw _iteratorError;
						}
					}
				}

				selectQuestionContainer.show();
			} else {
				selectQuestionContainer.hide();

				createSwal({
					title: 'Питання до даного блоку відсутні',
					type: 'info'
				});
				editTaskSelect.val('');
			}
		},
		error: function error(err) {
			createSwal({
				title: 'Помилка при спробі отримати дані блоку',
				type: 'error'
			});
		}
	});
}

function getQuestion() {
	$('#panel3 .imageWidthContainer .customWidthInput').attr('disabled', true);
	$('#panel3 .imageWidthContainer .changeWidthBtn').attr('disabled', true);
	$('#panel3 .imageWidthContainer .customWidthInput').val('');

	$('#first_edit_item').empty();

	$('#panel3 .text-block').hide();
	$('#panel3 .list-block').hide();
	$('#panel3 .list-answers-block').hide();
	$('#panel3 .category-block:not(:first)').remove();

	$('#panel3 .category-block .category').remove();
	$('#panel3 .category-block .imageWidthContainer').after(normalMultiblock);
	$('#panel3 .category-block .text-question').on('click', redactorMultiblock);
	$('#panel3 .category-block .addNewAnswer').on('click', addAnswerField);

	if ($('#type') == 2) {
		$('#insert_question_thing').show();
	} else {
		$('#insert_question_thing').hide();
	}

	$('#questions_edit_block_elements [data-toggle="tooltip"] button').prop("disabled", false);

	var idQuestion = $("#tasks_selector_for_edit").val();

	$.ajax({
		url: '/get-question',
		method: 'post',
		data: { idQuestion: idQuestion },
		success: function success(response) {
			var imageWidth = response.question[0].image_width;

			setDefaultImageWidth(imageWidth);

			$('#answer_items_edit').html('');
			var questionBlock = $('#form_edit_task .questions-block'),
			    question = questionBlock.find('.category .text-question');

			question.val(response['question'][0].text_question);
			insertAnswers(response['answers']);

			var addAnswer = $('#panel3 .addNewAnswer');
			addAnswer.off('click');
			addAnswer.on('click', addAnswerField);
			addAnswer.show();

			$('#tasks_selector_for_edit option:selected').attr('name', response['answers'].length);

			var multiblocks = $('#panel3 .multiblock');
			multiblocks.off('click');
			multiblocks.on('click', redactorMultiblock);

			$('#form_edit_task').show();
			questionBlock.show();
			questionBlock.find('.category-block').show();

			addListenersToImages();

			$('#panel3 .text-question').click();
			$('#panel3 .item-name').click();

			setCustomImageWidth();
		},
		error: function error(err) {
			createSwal({
				title: 'Помилка при спробі отримання данних',
				type: 'error'
			});
			console.log(err);
		}
	});

	function insertAnswers(answers) {
		var answersContainer = $('#first_edit_item');
		var _iteratorNormalCompletion2 = true;
		var _didIteratorError2 = false;
		var _iteratorError2 = undefined;

		try {
			for (var _iterator2 = answers.entries()[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
				var _ref = _step2.value;

				var _ref2 = _slicedToArray(_ref, 2);

				var i = _ref2[0];
				var answer = _ref2[1];

				var ansName = answer.text_answer,
				    isRadio = true;

				if (typeof answer['text_answer'] === 'undefined') {
					isRadio = false;
				}

				answersContainer.append(addNewAnswer(answer, false, !isRadio, false, isRadio, isRadio));

				var multiblocks = $('#panel3 .multiblock');
				multiblocks.off('click');
				multiblocks.on('click', redactorMultiblock);

				if (countAnswers <= i) {
					answersContainer.find('.deleteAnswer').show();
				}
			}
		} catch (err) {
			_didIteratorError2 = true;
			_iteratorError2 = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion2 && _iterator2.return) {
					_iterator2.return();
				}
			} finally {
				if (_didIteratorError2) {
					throw _iteratorError2;
				}
			}
		}

		answersContainer.find('.deleteAnswer').off('click');
		answersContainer.find('.deleteAnswer').on('click', deleteAnswer);

		answersContainer.find('input:file').on('click', function (e) {
			var input = $(e.currentTarget);
			input.attr('data-is-change', true);
			input.off('click');
		});
	}
}

function addAnswerField() {
	var answersContainer = $('#answer_items_edit');
	answersContainer.append(addNewAnswer(null, false, false, false, true, true));

	var multiblocks = $('#panel3 .multiblock');
	multiblocks.off('click');
	multiblocks.on('click', redactorMultiblock);

	$('#panel3 .item-name').last().click();

	$('.deleteAnswer').show();

	answersContainer.find('.deleteAnswer').off('click');
	answersContainer.find('.deleteAnswer').on('click', deleteAnswer);
	addListenersToImages();
}

function deleteAnswer() {
	var answersContainer = $(this).parent().parent().parent().parent().parent(),
	    nmbTask = answersContainer.find('.item').length - 1;

	countAnswers = $('#block_selector option:selected').attr('name');

	if ($('#tasks_selector_for_edit').is(':visible')) {
		answersContainer = $(this).closest('#form_edit_task');
		countAnswers = $('#tasks_selector_for_edit option:selected').attr('name');
		nmbTask = answersContainer.find('.answer-item').length / 2 - 1;
	}

	if (countAnswers === nmbTask.toString()) {
		answersContainer.find('.deleteAnswer').hide();
	}

	var id = $(this).parent().find('.item-name').attr('data-id-answer');
	$.ajax({
		url: 'delete_answer',
		method: 'post',
		data: {
			'id': id
		},
		error: function error(err) {
			createSwal({
				title: 'Помилка при видаленні відповіді',
				type: 'error'
			});
		}
	});

	$(this).parent().parent().parent().parent().remove();
}

function handleEditTask() {
	var editFormTask = $('#form_edit_task');
	$(editFormTask).off('submit');

	$(editFormTask).on('submit', function (e) {
		e.preventDefault();

		$('#form_edit_task button:submit').prop('disabled', true);

		var typeSpeciality = $('#type').text();

		if (typeSpeciality === '2') {
			updateEditEng();
		} else {
			EditTasks();
		}
		$('#form_edit_task button:submit').prop('disabled', false);
	});

	$('#updateLang').off('click');
	$('#updateLang').on('click', EditLangTask);

	$('#deleteLang').off('click');
	$('#deleteLang').on('click', DeleteLangTask);

	function DeleteLangTask() {
		var isConfirm = confirm('Ви дійсно хочете видати дане завдання?');
		if (isConfirm) {
			$(document).find('.bookshelf_wrapper').show();
			$(document).find('.hideBehind').show();
			$('#all').css("opacity", "0.5");

			$.ajax({
				url: '/delete-ukrainian-task',
				method: 'post',
				data: {
					'spec_id': $('#id_speciality').val(),
					'question_id': $('#get_lang_edit').val()
				},
				success: function success() {
					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					createSwal({
						title: 'Завдання видалено',
						type: 'success'
					});

					$('#changingLang').hide();

					getLangQuestions();

					$('#lang_question').val('');
					$('#lang_answers').val('');
				},
				error: function error(err) {
					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					createSwal({
						title: 'Помилка при видаленні завдання',
						type: 'error'
					});
					console.log(err);
				}
			});
		}
	}

	function EditLangTask() {
		$(document).find('.bookshelf_wrapper').show();
		$(document).find('.hideBehind').show();
		$('#all').css("opacity", "0.5");

		var questionContent = $('#lang_question').val(),
		    answerContent = $('#lang_answers').val(),
		    imageWidth = $('#lang_question').closest('.category-block').find('input[type="radio"]:checked');

		if ($(imageWidth).val() === 'custom') {
			imageWidth = $(imageWidth).parent().next();
		}

		if ($('#lang_question').closest('.category-block').find('.enableAutoWidth').hasClass('btn-success')) {
			imageWidth = 'auto';
		} else {
			imageWidth = $(imageWidth).val();
		}

		var _getImageSrc = getImageSrc(questionContent);

		var _getImageSrc2 = _slicedToArray(_getImageSrc, 2);

		questionContent = _getImageSrc2[0];
		questionImages = _getImageSrc2[1];

		var _getImageSrc3 = getImageSrc(answerContent);

		var _getImageSrc4 = _slicedToArray(_getImageSrc3, 2);

		answerContent = _getImageSrc4[0];
		answerImages = _getImageSrc4[1];


		$.ajax({
			url: '/update-ukrainian-task',
			method: 'post',
			data: {
				'questionContent': questionContent,
				'answerContent': answerContent,
				'question_id': $('#get_lang_edit').val(),
				'questionImages': questionImages,
				'answerImages': answerImages,
				'image_width': imageWidth
			},
			success: function success(response) {
				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");

				getLangQuestions();

				$('#changingLang').hide();

				$('#lang_question').val('');
				$('#lang_answers').val('');

				$('#get_lang_edit').val('');
				createSwal({
					title: 'Завдання оновлено',
					type: 'success'
				});
			},
			error: function error(err) {
				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");
				createSwal({
					title: 'Помилка при запису завдання',
					type: 'error'
				});

				console.log(err);
			}
		});
	}

	function EditTasks() {
		var textQuestion = $('#form_edit_task .question-name').val(),
		    answers = $('#first_edit_item .item-name'),
		    answersTexts = [],
		    answersImages = [],
		    answersId = [];

		var _iteratorNormalCompletion3 = true;
		var _didIteratorError3 = false;
		var _iteratorError3 = undefined;

		try {
			for (var _iterator3 = answers[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
				var ans = _step3.value;

				var text = $(ans).val();

				var _getImageSrc7 = getImageSrc(text);

				var _getImageSrc8 = _slicedToArray(_getImageSrc7, 2);

				text = _getImageSrc8[0];
				answerImage = _getImageSrc8[1];

				answersTexts.push(text);
				answersImages.push(answerImage);
				answersId.push($(ans).attr('data-id-answer'));
			}
		} catch (err) {
			_didIteratorError3 = true;
			_iteratorError3 = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion3 && _iterator3.return) {
					_iterator3.return();
				}
			} finally {
				if (_didIteratorError3) {
					throw _iteratorError3;
				}
			}
		}

		var err = 0,
		    error = 1;

		if (textQuestion == '' || textQuestion == ' ') {
			err = 1;
		}

		for (var i = 0; i < answersTexts.length; i++) {
			if (answersTexts[i] == '' || answersTexts[i] == ' ') {
				err = 1;
			}
		}

		$('input[name=cat-radio]').each(function () {
			if ($(this).prop('checked')) {
				error = 0;
			}
		});

		if (error === 1) {
			createSwal({
				title: 'Оберіть правильний варіант',
				type: 'error'
			});
		} else if (err == 1) {
			createSwal({
				title: 'Має бути заповнений текст завданнь або додане зображення',
				type: 'error'
			});
		} else {
			var saveEditedQuestion = function saveEditedQuestion() {
				var questionBlock = $('#form_edit_task .questions-block'),
				    contentData = questionBlock.find('.text-question').val(),
				    checkedRadio = questionBlock.find('.imageWidthContainer input[type="radio"]:checked'),
				    imageWidthInput = questionBlock.find('.imageWidthContainer .customWidthInput'),
				    imageWidth = checkedRadio.val() !== 'custom' ? checkedRadio.val() : imageWidthInput.val();

				if (questionBlock.find('.enableAutoWidth').hasClass('btn-success')) {
					imageWidth = 'auto';
				}

				var questionData = {
					id: questionId,
					contentData: contentData,
					imageWidth: imageWidth
				};

				saveElement(questionData, 'Question');
			};

			var saveEditedAnswers = function saveEditedAnswers() {
				var dataAnswers = _extends({}, answersData[_countAnswers], {
					isChecked: isChecked[_countAnswers],
					questionId: questionId
				});

				saveElement(dataAnswers, 'Answer');
				_countAnswers++;
			};

			var saveElement = function saveElement(dataElement, elementType) {
				var url = elementType === 'Question' ? '/update-question' : '/update-answer';

				var _getImageSrc5 = getImageSrc(dataElement.contentData),
				    _getImageSrc6 = _slicedToArray(_getImageSrc5, 2),
				    contentData = _getImageSrc6[0],
				    images = _getImageSrc6[1];

				var data = _extends({}, dataElement, {
					contentData: contentData,
					images: images
				});

				$.ajax({
					url: url,
					method: 'post',
					data: data,
					success: function success() {
						$('#form_edit_task button:submit').prop('disabled', false);

						if (_countAnswers < nmbAnswers) {
							saveEditedAnswers();
						} else {
							$(document).find('.bookshelf_wrapper').hide();
							$(document).find('.hideBehind').hide();
							$('#all').css("opacity", "1");

							$('#form_edit_task').hide();

							var questionText = $('#form_edit_task .questions-block .text-question').val();

							updateEditQuestionSelect('update', questionText);

							createSwal({
								title: 'Завдання оновлено',
								type: 'success'
							});

							$('.enableAutoWidth.btn-success').click();

							$('#form_edit_task input:file').map(function (index, input) {
								$(input).val('');
							});
						}
					},
					error: function error(err) {
						$(document).find('.bookshelf_wrapper').hide();
						$(document).find('.hideBehind').hide();
						$('#all').css("opacity", "1");

						$('#form_edit_task button:submit').prop('disabled', false);

						createSwal({
							title: 'Помилка при запису завдання',
							type: 'error'
						});
					}
				});
			};

			$(document).find('.bookshelf_wrapper').show();
			$(document).find('.hideBehind').show();
			$('#all').css("opacity", "0.5");

			var form = this,
			    token = $(this).find('>:first-child').val(),
			    questionId = $("#tasks_selector_for_edit").val();

			saveEditedQuestion();
			var questionBlock = $('#form_edit_task .questions-block'),
			    answersData = questionBlock.find('.item-name').toArray().map(function (el) {

				return { contentData: $(el).val() };
			});

			var isChecked = questionBlock.find('input[type=checkbox]').toArray().map(function (input) {
				return $(input).is(':checked') ? 1 : 0;
			});

			var _countAnswers = 0;
			var nmbAnswers = answersId.length;
		}
	}
}

function deleteQuestion() {
	var isConfirm = confirm('Ви дійсно хочете видати дане завдання?'),
	    idQuestion = $('#tasks_selector_for_edit').val();

	if (isConfirm) {
		$(document).find('.bookshelf_wrapper').show();
		$(document).find('.hideBehind').show();
		$('#all').css("opacity", "0.5");

		$.ajax({
			url: '/delete-question',
			method: 'post',
			data: {
				idQuestion: idQuestion
			},
			success: function success() {
				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");

				$('#form_edit_task').hide();
				updateEditQuestionSelect('delete');

				createSwal({
					title: 'Завдання успішно видалено',
					type: 'success'
				});
			},
			error: function error(err) {
				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");

				createSwal({
					title: 'Помилка при видаленні завдання',
					type: 'error'
				});
				console.log(err);
			}
		});
	}
}

function updateEditQuestionSelect(action) {
	var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

	var select = $('#tasks_selector_for_edit'),
	    selectContainer = $('#questions_edit_block_elements'),
	    idQuestion = $(select).val();

	data = removeHTML(data);

	if (action === 'update') {
		if (data == '') {
			$(select).find('option[value="' + idQuestion + '"]').text('Зображення');
			$(select).find('option[value="' + idQuestion + '"]').attr('name', 'image');
		} else {
			$(select).find('option[value="' + idQuestion + '"]').text(data);
			$(select).find('option[value="' + idQuestion + '"]').attr('name', 'normal');
		}
	} else if (action === 'delete') {
		$(select).find('option[value="' + idQuestion + '"]').remove();

		var nmbOptions = select.find('option').length;
		if (nmbOptions < 1) {
			selectContainer.hide();
		}
	}
	$(select).val('');

	$('#questions_edit_block_elements [data-toggle="tooltip"] button').prop("disabled", true);
}

function removeHTML(str) {
	if (str) {
		while (str.indexOf('<') != -1 && str.indexOf('>') != -1 && str.indexOf('>') > str.indexOf('<')) {
			str = str.slice(0, str.indexOf('<')) + str.slice(str.indexOf('>') + 1, str.length);
		}
	}
	return str;
}

module.exports = {
	getQuestion: getQuestion,
	getQuestions: getQuestions,
	getQuestionLang: getQuestionLang,
	getLangQuestions: getLangQuestions,
	handleEditTask: handleEditTask,
	deleteQuestion: deleteQuestion,
	addNewAnswer: addNewAnswer,
	addAnswerField: addAnswerField,
	deleteAnswer: deleteAnswer,
	removeHTML: removeHTML
};

/***/ }),

/***/ 82:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(83);


/***/ }),

/***/ 83:
/***/ (function(module, exports, __webpack_require__) {

var addListenersToImages = __webpack_require__(1).addListenersToImages;

var _require = __webpack_require__(58),
    blockPdf = _require.blockPdf,
    createNewBlock = _require.createNewBlock,
    updateBlock = _require.updateBlock,
    setBlockData = _require.setBlockData,
    deleteBlock = _require.deleteBlock,
    createEnglishBlock = _require.createEnglishBlock;

var _require2 = __webpack_require__(65),
    openDeclined = _require2.openDeclined,
    changeSelector = _require2.changeSelector;

var _require3 = __webpack_require__(66),
    manageContentTab = _require3.manageContentTab,
    filterOptionSelect = _require3.filterOptionSelect;

var _require4 = __webpack_require__(8),
    getQuestionLang = _require4.getQuestionLang,
    getQuestions = _require4.getQuestions,
    getQuestion = _require4.getQuestion,
    deleteQuestion = _require4.deleteQuestion;

var sendOmu = __webpack_require__(67).sendOmu;
var updateLangBlock = __webpack_require__(65).updateLangBlock;

var _require5 = __webpack_require__(27),
    renderFirstQuestion = _require5.renderFirstQuestion;

var _require6 = __webpack_require__(12),
    renderEnglishTask = _require6.renderEnglishTask,
    getEngQuestions = _require6.getEngQuestions,
    deleteEngQuestion = _require6.deleteEngQuestion,
    getEngQuestion = _require6.getEngQuestion;

var resetEnglishTasks = __webpack_require__(28);

var _require7 = __webpack_require__(3),
    createSwal = _require7.createSwal;

var setWidthListeners = __webpack_require__(6);

$(document).ready(function () {
	var cloneSpecialityBtn = $('.clone-speciality');
	cloneSpecialityBtn.on('click', cloneSpeciality);

	function cloneSpeciality(e) {
		var submitBtn = $(e.currentTarget),
		    verificationSelect = $('#choose_specialty');

		submitBtn.prop('disabled', true);

		$(document).find('.bookshelf_wrapper').show();
		$(document).find('.hideBehind').show();
		$('#all').css("opacity", "0.5");

		var data = {
			'spec_id': $('#id_speciality').val()
		};

		$.ajax({
			url: '/clone-speciality',
			method: 'post',
			data: data,
			success: function success(result) {
				$('#id_speciality').html($('#id_speciality').html() + '<option selected class=\"blacked\" value=\"' + result[1] + '\">' + result[2] + '</option>');

				verificationSelect.append('<option value="' + result[1] + '">' + result[2] + '</option>');

				changeSelector();
				createSwal({
					title: 'Спецільність успішно клоновано',
					type: 'success'
				});
			},
			error: function error(err) {
				createSwal({
					title: 'Спецільність не клоновано',
					type: 'error'
				});
				console.log(err);
			},
			complete: function complete() {
				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");

				submitBtn.prop('disabled', false);
			}
		});
	}

	$('#id_speciality').on('change', changeSelector);
	$('#get_lang_edit').on('change', getQuestionLang);

	$('#block_selector_for_edit').on('change', function () {
		$('#form_edit_task').hide();
		if (parseInt($('#type').text()) === 2) {
			getEngQuestions();
		} else {
			getQuestions();
		}
	});
	$('#block_selector_for_edit').on('change', function () {
		$('#answer_items_edit').html("");
	});

	$('#tasks_selector_for_edit').on('change', function () {
		if (parseInt($('#type').text()) === 2) {
			getEngQuestion();
		} else {
			getQuestion();
		}
	});
	$('#questions_edit_block_elements .edit-task-button').on('click', function () {
		if (parseInt($('#type').text()) === 2) {
			getEngQuestion();
		} else {
			editTaskForm();
		}
	});
	$('#questions_edit_block_elements .delete-task-button').on('click', function () {
		$('#form_edit_task').hide();
		if (parseInt($('#type').text()) === 2) {
			deleteEngQuestion();
		} else {
			deleteQuestion();
		}
	});

	$('#sendDiv').on('click', sendOmu);

	$('#statusButton button').on('click', function (e) {
		var reason = $(e.currentTarget).attr('data-reason');
		openDeclined(reason);
	});

	setWidthListeners();

	manageContentTab();

	$(document).find('.bookshelf_wrapper').hide();
	$(document).find('.hideBehind').hide();
	$('#all').css("opacity", "1");

	var blurScreen = $('#blur-screen'),
	    pdfFrame = $('#pdf-frame'),
	    listBlank = $('#listBlankPopup'),
	    nmbToDownloadBlank = $('#nmbToDownloadBlank');

	blurScreen.on('click', function () {
		blurScreen.addClass('hidden');
		pdfFrame.addClass('hidden');
		listBlank.addClass('hidden');
		nmbToDownloadBlank.addClass('hidden');
	});

	$('#exit, #closeDeclined').on('click', function () {
		$("#declined").hide();
	});

	$('#showView').on('click', blockPdf);

	$('#block_selector').on('change', function () {
		var typeSpeciality = $('#type').text();

		if (typeSpeciality == '2') {
			renderEnglishTask();
		} else {
			resetEnglishTasks();
			$('#panel2 .task-container .questions-block').show();
			renderFirstQuestion();
		}
	});

	addListenersToImages();

	$('a[data-toggle="tab"]').on('shown.bs.tab', manageContentTab);

	$('.add_question_text').off('click');
	$('.add_question_text').on('click', function () {
		var text = $(this).parent().find('textarea');
		$(text).val($(text).val() + '(XX)______');
		if (!$('#blockCreateForm').hasClass('hidden')) {
			$('#textEnglish').click();
		} else {
			$('#textEnglishEdit').click();
		}
	});

	var createBlockForm = $('#create-block-form');
	createBlockForm.off('submit');
	if ($('#type').text() == '0') {
		createBlockForm.on('submit', createNewBlock);
	} else {
		createBlockForm.on('submit', createEnglishBlock);
	}

	var editBlockSelect = $('#choose-select');
	editBlockSelect.on('change', onChangeCreateBlock);

	var editBlockBtn = $('#edit-block .category-pencil');
	editBlockBtn.on('click', setBlockData);

	var editBLockForm = $('#edit-block-form');
	editBLockForm.on('submit', function (e) {
		return updateBlock(e);
	});

	var deleteBlockBtn = $('#edit-block .category-trash');
	deleteBlockBtn.on('click', deleteBlock);

	filterOptionSelect('#choose-select');

	$('#updateLangBlock').off('click');
	$('#updateLangBlock').on('click', updateLangBlock);
});

function editTaskForm() {
	$('#form_for_change_task').show();
	$('#form_for_change_block').hide();
	getQuestion();
	addListenersToImages();
}

function onChangeCreateBlock() {
	var curBlock = $(this).val(),
	    createNewBlockContainer = $('.new-block-form'),
	    editBlockContainer = $('.edit-block-container');

	addListenersToImages();

	if (curBlock === 'new_block') {
		createNewBlockContainer.removeClass('hidden');
		createNewBlockContainer.show();
		$('#edit-block').find("span[data-toggle='tooltip']").hide();
	} else {
		$('#edit-block').find("span[data-toggle='tooltip']").show();

		createNewBlockContainer.addClass('hidden');
	}
	editBlockContainer.addClass('hidden');

	var editBtn = $('#edit-block .category-pencil'),
	    deleteBtn = $('#edit-block .category-trash');

	editBtn.removeAttr('disabled');
	deleteBtn.removeAttr('disabled');
}

/***/ }),

/***/ 84:
/***/ (function(module, exports, __webpack_require__) {

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var _require = __webpack_require__(3),
    createSwal = _require.createSwal;

var redactorMultiblock = __webpack_require__(4).redactorMultiblock;
var handleEditTask = __webpack_require__(8).handleEditTask;
var addNewAnswer = __webpack_require__(11).addNewAnswer;
var deleteAnswer = __webpack_require__(8).deleteAnswer;
var removeHTML = __webpack_require__(8).removeHTML;
var getImageSrc = __webpack_require__(7);

var _require2 = __webpack_require__(1),
    handleChangeRadio = _require2.handleChangeRadio,
    setWidthImage = _require2.setWidthImage;

var _require3 = __webpack_require__(12),
    saveEnglishTask = _require3.saveEnglishTask;

var setWidthListeners = __webpack_require__(6);

$('#addNewTask').on('click', handleNewTask);

$('#EditTaskTab').on('click', handleEditTask);

var radio = 1;

var newTaskLang = function newTaskLang() {
	radio++;
	return '<div class="category-block" style="display: block;">\n\t\t\t\t\t\t<div class="imageWidthContainer im_regculation">\n\t\t\t\t\t\t\t<div class="row col-xs-10">\n\t\t\t\t\t\t\t\t<span>\u0412\u043A\u0430\u0436\u0456\u0442\u044C \u0448\u0438\u0440\u0438\u043D\u0443 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u044C:</span>\n\t\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + radio + '" value="50">50px</label>\n\t\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + radio + '" value="75">75px</label>\n\t\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + radio + '" value="100" checked>100px</label>\n\t\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + radio + '" value="125">125px</label>\n\t\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + radio + '" value="150">150px</label>\n\t\t\t\t\t\t\t\t<label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-' + radio + '" aria-label="image-with"></label>\n\t\t\t\t\t\t\t\t<input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>\n\t\t\t\t\t\t\t\t<button class = "changeWidthBtn btn btn-info" disabled>\u0417\u043C\u0456\u043D\u0438\u0442\u0438</button>\n\t\t\t\t\t\t\t\t<button class="enableAutoWidth btn btn-danger">Auto/off</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n                        <div class="row category" style="margin-bottom: 5px">\n                            <div class="col-xs-12 form-inline" style="width: 100%">\n                                    <div class="lang-item">\n                                        <div class="lang-area">\n                                            <div class="popupImageConvertor">\n                                                <div class="popup-container">\n                                                    <h3 class="resize-title">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n                                                    <div class="resizer" >\n                                                        <span class="addition">ctrl+v</span>\n                                                    </div>\n                                                    <button class="resizer-result btn btn-info" class="btn btn-info">\u0412\u0441\u0442\u0430\u0432\u0438\u0442\u0438 \u0432\u0438\u0434\u0456\u043B\u0435\u043D\u0435 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n                                                    <button class="closePopup">&#10006;</button>\n                                                </div>\n                                            </div>\n                                            <textarea class="multiblock questionsLang form-control text_area_lang" placeholder="\u0422\u0435\u043A\u0441\u0442 \u043F\u0438\u0442\u0430\u043D\u043D\u044F"></textarea>\n                                         </div>\n                                         <div class="lang-area">\n                                            <div class="popupImageConvertor">\n                                                <div class="popup-container">\n                                                    <h3 class="resize-title">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n                                                    <div class="resizer" >\n                                                        <span class="addition">ctrl+v</span>\n                                                    </div>\n                                                    <button class="resizer-result btn btn-info" class="btn btn-info">\u0412\u0441\u0442\u0430\u0432\u0438\u0442\u0438 \u0432\u0438\u0434\u0456\u043B\u0435\u043D\u0435 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n                                                    <button class="closePopup">&#10006;</button>\n                                                </div>\n                                            </div>\n                                            <textarea class="multiblock answersLang form-control text_area_lang" placeholder="\u0422\u0435\u043A\u0441\u0442 \u0432\u0456\u0434\u043F\u043E\u0432\u0456\u0434\u0456"></textarea>\n                                        </div>\n                                    </div>\n                                <div class="btn-space">\n                                    <span class="category-add" data-toggle="tooltip" title="\u0414\u043E\u0434\u0430\u0442\u0438 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F">\n                                        <button type="button" class="btn btn-default">\n                                            <span class="glyphicon glyphicon-plus" aria-hidden="true"> \u0414\u043E\u0434\u0430\u0442\u0438 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F</span>\n                                        </button>\n                                    </span>\n                                    <span class="category-delete" data-toggle="tooltip" title="\u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u043F\u0438\u0442\u0430\u043D\u043D\u044F" >\n                                        <button type="button" class="btn btn-default">\n                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"> \u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u043F\u0438\u0442\u0430\u043D\u043D\u044F</span>\n                                        </button>\n                                    </span>\n                                </div>\n                            </div>\n                        </div>\n                    </div>';
};

var newTask = function newTask() {
	var arrIndex = [].concat(_toConsumableArray($('#panel2 .questions-block input[name^="image-width-radio"]'))).map(function (el) {
		var name = $(el).attr('name');
		return parseInt(name.replace('image-width-radio-', ''));
	});

	var taskIndex = arrIndex.length > 0 ? Math.max.apply(Math, _toConsumableArray(arrIndex)) + 1 : 1;

	return '\n\t\t<div class="task-container">\n\t\t\t<div class="text-block" style="display: none;">\n\t            <h2>\u0422\u0435\u043A\u0441\u0442 \u0434\u043E \u043F\u0438\u0442\u0430\u043D\u043D\u044F:</h2>\n\t            <div class="imageWidthContainer im_regculation" id="0">\n\t                <div class="row col-xs-10">\n\t                    <span>\u0412\u043A\u0430\u0436\u0456\u0442\u044C \u0448\u0438\u0440\u0438\u043D\u0443 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u044C:</span>\n\t                    <label><input type="radio" name="image-width-radio-text" value="50">50px</label>\n\t                    <label><input type="radio" name="image-width-radio-text" value="75">75px</label>\n\t                    <label><input type="radio" name="image-width-radio-text" value="100" checked>100px</label>\n\t                    <label><input type="radio" name="image-width-radio-text" value="125">125px</label>\n\t                    <label><input type="radio" name="image-width-radio-text" value="150">150px</label>\n\t                    <label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-text" aria-label="image-width"></label>\n\t                    <input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>\n\t                    <button class = "changeWidthBtn btn btn-info" disabled>\u0417\u043C\u0456\u043D\u0438\u0442\u0438</button>\n\t                    <button class="enableAutoWidth btn btn-danger">Auto/off</button>\n\t                </div>\n\t            </div>\n\t            <div class="category row" style="margin-bottom: 5px">\n\t                <div class="col-xs-12 form-inline" style="width: 100%">\n\t                    <div class="popupImageConvertor">\n\t                        <div class="popup-container">\n\t                            <h3 class="resize-title">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n\t                            <div class="resizer" >\n\t                                <span class="addition">ctrl+v</span>\n\t                            </div>\n\t                            <button class="btn btn-info resizer-result">\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n\t                            <button class="closePopup">&#10006;</button>\n\t                        </div>\n\t                    </div>\n\t                    <textarea class="multiblock text-question"></textarea>\n\t                </div>\n\t            </div>\n\t        </div>\n\t        <div class="list-answers-block" style="display: none;">\n\t            <table>\n\t                <thead>\n\t                <tr>\n\t                    <th>\u2116</th>\n\t                    <th>\u0412\u0430\u0440\u0456\u0430\u043D\u0442</th>\n\t                </tr>\n\t                </thead>\n\t                <tbody></tbody>\n\t            </table>\n\t        </div>\n\t        <div class="list-block" style="display:none;">\n\t            <table>\n\t                <thead>\n\t                <tr>\n\t                    <th>\u2116</th>\n\t                    <th>\u0422\u0435\u043A\u0441\u0442 \u043F\u0438\u0442\u0430\u043D\u043D\u044F</th>\n\t                    <th class="change-content-col"></th>\n\t                </tr>\n\t                </thead>\n\t                <tbody></tbody>\n\t            </table>\n\t        </div>\n\t\t\t<div class="questions-block">\n\t\t\t\t<div class="category-block" style="display: block;">\n\t\t\t\t\t<div class="imageWidthContainer im_regculation">\n\t\t\t\t\t\t<div class="row col-xs-10">\n\t\t\t\t\t\t\t<span>\u0412\u043A\u0430\u0436\u0456\u0442\u044C \u0448\u0438\u0440\u0438\u043D\u0443 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u044C:</span>\n\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + taskIndex + '" value="50">50px</label>\n\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + taskIndex + '" value="75">75px</label>\n\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + taskIndex + '" value="100" checked>100px</label>\n\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + taskIndex + '" value="125">125px</label>\n\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + taskIndex + '" value="150">150px</label>\n\t\t\t\t\t\t\t<label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-' + taskIndex + '" aria-label="image-width"></label>\n\t\t\t\t\t\t\t<input type="number" class="form-control customWidthInput" aria-label="custom-image-width" min="10" max="350" disabled>\n\t\t\t\t\t\t\t<button class = "changeWidthBtn btn btn-info" disabled>\u0417\u043C\u0456\u043D\u0438\u0442\u0438</button>\n\t\t\t\t\t\t\t<button class="enableAutoWidth btn btn-danger">Auto/off</button>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t' + addNewAnswer(null, true, true, true, false, false) + '\n\t\t\t\t\t<div class="items">\n\t\t\t\t\t\t' + addNewAnswer(null, false, false, false, true, true) + '\t\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>';
};

var newItem = addNewAnswer(null, false, false, false, true, true);

var nmbTaskUsed = 0;

function handleNewTask() {
	var addTaskBtn = $('.category-add'),
	    rmTaskBtn = $('.category-delete'),
	    multiblocks = $('#panel2 .multiblock'),
	    select = $('#block_selector'),
	    addAnswer = $('.addNewAnswer'),
	    categoriesContainer;

	if ($('#type').text() == "1") {
		categoriesContainer = $('#categ');
		categoriesContainer.find('.category-block').show();
	} else {
		categoriesContainer = $('#categories');
	}

	updateListeners();

	$(categoriesContainer).find('.category-block').not(':first').remove();

	function addText(event) {
		$(this).parent().siblings('.savedImg').append("<p class=\"item-question-text\">" + $(this).parent().siblings(event.data.value).val() + "</p>");
		$(this).parent().siblings(event.data.value).val("");
	}

	function updateListeners() {
		addTaskBtn = $('.category-add');
		rmTaskBtn = $('.category-delete');
		addAnswer = $('.addNewAnswer');

		multiblocks = $('#panel2 .multiblock');
		multiblocks.off('click');
		multiblocks.on('click', redactorMultiblock);

		addTaskBtn.off('click');
		addTaskBtn.on('click', addTask);

		rmTaskBtn.off('click');
		rmTaskBtn.on('click', rmTask);

		addAnswer.off('click');
		addAnswer.on('click', addAnswerField);

		$('.addNewText').off('click');
		$('.addNewText').on('click', { value: '.item-category' }, addText);

		$('.addNewTextAns').off('click');
		$('.addNewTextAns').on('click', { value: '.item-name' }, addText);

		var addImageWidthRadio = $('#panel2 .imageWidthContainer input[type="radio"]'),
		    addChangeWidthBtn = $('#panel2 .changeWidthBtn');

		addImageWidthRadio.off();
		addImageWidthRadio.on('change', function (e) {
			return handleChangeRadio(e);
		});
		addChangeWidthBtn.off();
		addChangeWidthBtn.on('click', function (e) {
			e.preventDefault();
			var imageWidth = $(e.currentTarget).prev().val();
			setWidthImage(imageWidth, $(e.currentTarget).prev().prev().find('input'));
		});

		setWidthListeners();
	}

	function addAnswerField() {
		var answersContainer = $(this).closest('.category-block').find('.items');

		answersContainer.append(newItem);

		$(this).closest('.category-block').find('.deleteAnswer').show();

		$('.deleteAnswer').off('click');
		$('.deleteAnswer').on('click', deleteAnswer);
		$('.addNewTextAns').off('click');
		$('.addNewTextAns').on('click', { value: '.item-name' }, addText);

		updateListeners();
	}

	function addQuestion(container) {
		var firstName = container.first().find('input[type="checkbox"]').attr('name');
		var itemsContainer = $(container).find('.items');

		for (var i = 0; i < $('#hidden_countAnswers').val() - 1; i++) {
			itemsContainer.append(newItem);
		}
		container.last().find('input[type="checkbox"]').attr('name', firstName);
	}

	function addTask() {
		var nmbTask = $(categoriesContainer).find('.category-block').length;
		var task_container = "";
		nmbTaskUsed = nmbTask;

		if (nmbTask === 1) {
			categoriesContainer.first().find('.category-delete').show();
		}

		if ($('#type').text() === "1") {
			task_container = $(newTaskLang()).insertBefore($(categoriesContainer).find('.center-block'));
		} else {
			task_container = $(newTask()).insertBefore($(categoriesContainer).find('.center-block'));
		}

		addQuestion(task_container);
		updateListeners();
	}

	function rmTask() {
		var nmbTask = $(categoriesContainer).find('.category-block').length,
		    task = $(this).closest('.category-block');

		if ($('#type').text() == '0') {
			task = task.closest('.task-container');
		}

		nmbTaskUsed--;
		if (nmbTask > 1) {
			task.remove();

			updateListeners();
		}
		if (nmbTask === 2) {
			categoriesContainer.first().find('.category-delete').hide();
		}

		categoriesContainer.find('.category-add').toArray().map(function (el) {
			return $(el).show();
		});
	}

	$('#send-form').off('submit');
	$('#send-form').on('submit', function (e) {
		e.preventDefault();

		$('#send-form button:submit').prop('disabled', true);

		var typeSpeciality = parseInt($('#type').text()),
		    englishSpeciality = 2;

		typeSpeciality === englishSpeciality ? saveEnglishTask(e) : saveTasks(e);
	});

	$('#addingLang').off('submit');
	$('#addingLang').on('submit', function (e) {
		$('#addingLang button').prop('disabled', true);

		saveLangTask(e);
	});

	function validateQuestionsAndAnswers(element) {
		var valid = false;
		var currentValue = $(element).val().trim();
		if (currentValue.length > 1000000) {

			if ($(element).hasClass('item-name')) {
				$(element).parent().parent().after("<div class=\"validate-alert\">Поле не має перевищувати 1000000 символів</div>");
			} else {
				$(element).parent().after("<div class=\"validate-alert\">Поле не має перевищувати 1000000 символів</div>");
			}

			$(element).parent().removeClass("access-validate");
			$(element).parent().addClass("not-validate");
		} else if (currentValue.length < 1) {

			$(element).parent().after("<div class=\"validate-alert\">Поле не має бути пустим</div>");

			if ($(element).hasClass('item-name')) {
				$(element).removeClass("access-validate");
				$(element).addClass("not-validate");
			} else {
				$(element).parent().removeClass("access-validate");
				$(element).parent().addClass("not-validate");
			}
		} else {

			$(element).parent().removeClass("not-validate");
			$(element).parent().addClass("access-validate");
			valid = true;
		}
		return valid;
	}

	function cleanValidation(element) {
		$(element).parent().removeClass("access-validate");
		$(element).parent().removeClass("not-validate");
	}

	function saveLangTask(e) {
		e.preventDefault();
		$('#success-blocks').hide();

		$(document).find('.bookshelf_wrapper').show();
		$(document).find('.hideBehind').show();
		$('#all').css("opacity", "0.5");

		var task = $(categoriesContainer).find('.category-block');
		var taskNames = [];
		var questionImages = [];

		$(task).find(".validate-alert").remove();
		var validate = true;
		var i = 0;

		$(task).find(".answersLang").click();
		$(task).find(".questionsLang").click();
		$(task).find(".answersLang").each(function () {
			if (!validateQuestionsAndAnswers(this)) {
				validate = false;
			}
		});

		i = 0;
		$(task).find(".questionsLang").each(function () {
			if (!validateQuestionsAndAnswers(this)) {
				validate = false;
			}
		});

		if (validate == false) {
			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");

			$('#addingLang button').prop('disabled', false);

			return;
		} else {
			cleanValidation($(task).find(".questionsLang"));
			cleanValidation($(task).find(".answersLang"));
		}

		i = 0;
		var imageWidth = [];
		$(task).find(".questionsLang").each(function () {
			var imageWidthContainer = $($(task)[i]).find('.imageWidthContainer'),
			    autoImageBtn = $($(task)[i]).find('.enableAutoWidth'),
			    checkedRadio = imageWidthContainer.find('input:radio:checked'),
			    imageWidthInput = imageWidthContainer.find('.customWidthInput');

			if (autoImageBtn.hasClass('btn-success')) {
				imageWidth[i] = 'auto';
			} else {
				imageWidth[i] = checkedRadio.val() !== 'custom' ? checkedRadio.val() : imageWidthInput.val();
			}
			taskNames[i] = $(this).val();

			var _getImageSrc = getImageSrc(taskNames[i]);

			var _getImageSrc2 = _slicedToArray(_getImageSrc, 2);

			taskNames[i] = _getImageSrc2[0];
			questionImages[i] = _getImageSrc2[1];


			i++;
		});

		var tasksRight = [];
		var answerImages = [];
		i = 0;

		$(task).find(".answersLang").each(function () {
			tasksRight[i] = $(this).val();

			var _getImageSrc3 = getImageSrc(tasksRight[i]);

			var _getImageSrc4 = _slicedToArray(_getImageSrc3, 2);

			tasksRight[i] = _getImageSrc4[0];
			answerImages[i] = _getImageSrc4[1];

			i++;
		});

		//hide content
		$(categoriesContainer).find('.category-block').not(':first').remove();
		$(task).find(".questionsLang").val("");
		$(task).find(".answersLang").val("");
		$(task).find(".questionsLang").prev().find(' iframe').contents().find('body').text("");
		$(task).find(".answersLang").prev().find(' iframe').contents().find('body').text("");

		categoriesContainer.first().find('.category-delete').hide();
		$('#langEmpty').hide();

		var data = {
			'questions': taskNames,
			'answers': tasksRight,
			'spec_id': $('#id_speciality').val(),
			'questionImages': questionImages,
			'answerImages': answerImages,
			'image_width': imageWidth
		};

		$.ajax({
			url: '/save-ukrainian-tasks',
			method: 'post',
			data: data,
			success: function success() {
				createSwal({
					title: 'Завдання успішно збережено',
					type: 'success'
				});
			},
			error: function error(err) {
				console.log(err);
			},
			complete: function complete() {
				$('#addingLang button').prop('disabled', false);
				$('#categ input:radio[value="100"]').click();
				$('#categ .customWidthInput').val('');
				$('#categ .customWidthInput').prop('disabled', true);
				$('#categ .changeWidthBtn').prop('disabled', true);
				$('#categ .enableAutoWidth.btn-success').click();

				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");
			}
		});
	}

	function saveTasks(e) {
		$(document).find('.bookshelf_wrapper').show();
		$(document).find('.hideBehind').show();
		$('#all').css("opacity", "0.5");

		$('#success-blocks').hide();

		$('#categories .category').find(".validate-alert").remove();
		$('#send-form .items .item').find(".validate-alert").remove();
		var validate = true;

		$('#categories .category').each(function () {
			$(e.currentTarget).find('.question-name').click();
			if (!validateQuestionsAndAnswers($(e.currentTarget).find('.question-name'))) {
				$(e.currentTarget).find('.question-name').each(function () {
					$(this).closest('.row').find('.validate-alert:not(:first)').remove();
				});
				validate = false;
			}
		});

		var answersItems = $('#send-form .items .item .item-name');
		var _iteratorNormalCompletion = true;
		var _didIteratorError = false;
		var _iteratorError = undefined;

		try {
			for (var _iterator = answersItems[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
				var answer = _step.value;

				if (!validateQuestionsAndAnswers(answer)) {
					$(answer).closest('.row').find('.validate-alert:not(:first)').remove();
					validate = false;
				}
			}
		} catch (err) {
			_didIteratorError = true;
			_iteratorError = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion && _iterator.return) {
					_iterator.return();
				}
			} finally {
				if (_didIteratorError) {
					throw _iteratorError;
				}
			}
		}

		if (validate == false) {
			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");

			$('#send-form button:submit').prop('disabled', false);
			return;
		} else {
			cleanValidation($('#categories .category').find('.question-name'));
			cleanValidation($('#send-form .items .item').find('.item-name'));
		}

		var currentQuestionCount = 0;

		var TextQuestions = [];
		var TextAnswers = [];
		var correctQuestions = [];

		var answers = [];

		var tasks = $(categoriesContainer).find('.category-block');
		var error = 1;
		var index = 0;
		var answerCount = [];
		var err = 0;

		$('#categories .category-block').each(function () {
			error = 1;
			$(e.currentTarget).find('input[name=cat-radio]').each(function () {
				if ($(this).prop('checked')) {
					error = 0;
				}
			});
			if (error === 1) {
				err = 1;
			}
		});

		if (err === 1) {
			createSwal({
				title: 'Оберіть правильний варіант для всіх завдань',
				type: 'error'
			});

			$('#send-form button:submit').prop('disabled', false);

			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");
		} else {
			var _iteratorNormalCompletion2 = true;
			var _didIteratorError2 = false;
			var _iteratorError2 = undefined;

			try {
				for (var _iterator2 = tasks[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
					var task = _step2.value;


					var taskName = $(task).find('.item-category').val(),
					    questionsElements = $(task).find('.items .item-name').toArray(),
					    currentQuestions = [];
					questionsElements.forEach(function (question) {
						currentQuestions.push($(question).val());
						answers.push($(question).val());
					});

					var correctQuestion = $(task).find('input:checked').parent().parent().parent().parent().index();

					TextQuestions.push(taskName);
					TextAnswers.push(currentQuestions);

					answerCount.push(currentQuestions.length);
					correctQuestions.push(correctQuestion);
				}
			} catch (err) {
				_didIteratorError2 = true;
				_iteratorError2 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion2 && _iterator2.return) {
						_iterator2.return();
					}
				} finally {
					if (_didIteratorError2) {
						throw _iteratorError2;
					}
				}
			}

			saveImages();
		}

		function saveImages() {
			var token = $("#send-form").children().first().val();
			var questions = $('#categories .category-block');
			var questionsNmb = $(questions).length;
			var id_block = $("#block_selector").val();
			var indexQuestions = 0;
			var questionText = '';
			var questionImage = '';

			$(document).find('.bookshelf_wrapper').show();
			$(document).find('.hideBehind').show();
			$('#all').css("opacity", "0.5");

			saveQuestions();

			function saveQuestions() {
				questionText = $(questions[indexQuestions]).find('.question-name').val();

				var _getImageSrc5 = getImageSrc(questionText);

				var _getImageSrc6 = _slicedToArray(_getImageSrc5, 2);

				questionText = _getImageSrc6[0];
				questionImage = _getImageSrc6[1];


				sendImage(false);
			}

			var indexAnswers = 0,
			    answers = $('#send-form .items .item'),
			    answersNmb = answers.length,
			    answersImage = '',
			    answerText = '',
			    id_question = '',
			    isChecked;

			function saveAnswers() {
				answerText = $(answers[indexAnswers]).find('.item-name').val();

				isChecked = $(answers[indexAnswers]).find('input[type="checkbox"]').prop("checked") ? 1 : 0;

				var _getImageSrc7 = getImageSrc(answerText);

				var _getImageSrc8 = _slicedToArray(_getImageSrc7, 2);

				answerText = _getImageSrc8[0];
				answersImage = _getImageSrc8[1];


				sendImage(true);
			}

			var createdQuestion = [];

			function sendImage(isAnswers) {
				var data;

				if (!isAnswers) {
					var checkedRadio = $(tasks[indexQuestions]).find('input[type="radio"]:checked'),
					    imageWidthInput = $(checkedRadio).parent().parent().find('.customWidthInput'),
					    imageWidth = void 0;

					if ($(tasks[indexQuestions]).find('.enableAutoWidth').hasClass('btn-success')) {
						imageWidth = 'auto';
					} else {
						imageWidth = $(checkedRadio).val() !== 'custom' ? $(checkedRadio).val() : $(imageWidthInput).val();
					}

					data = {
						'typeElement': 'Question',
						'id_block': id_block,
						'question_text': questionText,
						'image_width': imageWidth,
						'image': questionImage,
						'token': token
					};
				} else {
					data = {
						'typeElement': 'Answer',
						'id_question': id_question,
						'id_block': id_block,
						'isChecked': isChecked,
						'question_text': answerText,
						'image': answersImage,
						'token': token
					};
				}

				$('#categories .category-block').hide();
				$('#categories button:submit').hide();

				$.ajax({
					url: '/save-task-element',
					method: 'POST',
					data: data,
					success: function success(result) {
						if (result.id_question) {
							createdQuestion.push({
								'id': result.id_question,
								'text': result.question_text
							});

							id_question = result.id_question;
						}
					},
					error: function error(err) {
						console.log(err);
					},
					complete: function complete() {
						$('#send-form button:submit').prop('disabled', false);

						if (isAnswers) {
							indexAnswers++;
							index++;

							if (indexAnswers < answersNmb) {
								if (index % answerCount[currentQuestionCount] == 0 && questionsNmb > 1) {
									currentQuestionCount++;
									index = 0;
									saveQuestions();
								} else {
									saveAnswers();
								}
							} else {
								updateNewOption(createdQuestion);

								var prevVal = $(select).attr('data-max-question'),
								    questionNmb = $('#categories .category-block').length,
								    nmbNewTask = prevVal - questionNmb;

								$(select).attr('data-max-question', nmbNewTask);

								resetTasks();
							}
						} else {
							indexQuestions++;
							saveAnswers();
						}

						if ($('#panel2 .task-container').length > 1) {
							$('#panel2 .task-container:first').remove();
						}
					}
				});
			}
		}
	}

	function resetTasks() {
		$('#loadingProgress').hide();
		$(categoriesContainer).find('.category-block').remove();

		var maxTasks = $('#block_selector').attr('data-max-question');

		var task_container = $(newTask()).insertBefore($(categoriesContainer).find('.center-block'));
		addQuestion(task_container);

		if (maxTasks < 1) {
			$('#limit-questions').removeClass('hidden');
			$('#categories').hide();
		} else if (maxTasks == 1) {
			$('#categories .category-add').hide();
		} else {
			$('#categories .category-add').show();
		}

		$('#categories button:submit').show();

		$('#categories .category-delete').hide();

		$('.category-block').first().find('.items').attr('id', 'first_answer_item');

		updateListeners();

		$(document).find('.bookshelf_wrapper').hide();
		$(document).find('.hideBehind').hide();
		$('#all').css("opacity", "1");

		createSwal({
			title: 'Завдання успішно збережено',
			type: 'success'
		});
	}
}

function updateNewOption(createdQuestions) {
	var choosedBlockPanel2 = $('#panel2 #block_selector').val(),
	    choosedBlockPanel3 = $('#panel3 #block_selector_for_edit').val();

	if (choosedBlockPanel2 === choosedBlockPanel3) {

		var selectContainer = $('#questions_edit_block_elements'),
		    questionSelect = $('#tasks_selector_for_edit');

		var _iteratorNormalCompletion3 = true;
		var _didIteratorError3 = false;
		var _iteratorError3 = undefined;

		try {
			for (var _iterator3 = createdQuestions[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
				var newQuestion = _step3.value;

				newQuestion['text'] = removeHTML(newQuestion['text']);
				if (newQuestion['text'] == "") {
					$(questionSelect).append('<option value="' + newQuestion['id'] + '">\u0417\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</option>');
				} else {
					$(questionSelect).append('<option value="' + newQuestion['id'] + '">' + newQuestion['text'] + '</option>');
				}
			}
		} catch (err) {
			_didIteratorError3 = true;
			_iteratorError3 = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion3 && _iterator3.return) {
					_iterator3.return();
				}
			} finally {
				if (_didIteratorError3) {
					throw _iteratorError3;
				}
			}
		}

		selectContainer.show();
	}
}

module.exports = {
	handleNewTask: handleNewTask,
	addNewAnswer: addNewAnswer,
	deleteAnswer: deleteAnswer,
	removeHTML: removeHTML
};

/***/ })

/******/ });