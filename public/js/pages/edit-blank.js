/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 95);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var createSwal = __webpack_require__(3).createSwal;
var imageWidthChoices = __webpack_require__(10).imageWidthChoices;

function addListenersToImages() {
	var editImageWidthRadio = $('#panel3 .imageWidthContainer input[type="radio"]'),
	    editChangeWidthBtn = $('#panel3 .changeWidthBtn');

	editImageWidthRadio.off('change');
	editImageWidthRadio.on('change', function (e) {
		return handleChangeRadio(e, 'edit-task');
	});
	editChangeWidthBtn.off();
	editChangeWidthBtn.on('click', function (e) {
		e.preventDefault();
		var imageWidth = $(e.currentTarget).prev().val();
		setWidthImage(imageWidth, 'edit-task', $(e.currentTarget).prev().prev().find('input'));
	});

	$('.image-buttons').off('change');
	$('.image-buttons').on('change', function () {
		var isValideType = validateImageType(this);
		if (this.value && isValideType) {
			$(this).next().addClass("color-change");
			$(this).parent().parent().next().show();
			cutTitle(this, true);
		} else {
			$(this).next().removeClass("color-change");
			$(this).parent().parent().next().find(".imgDel").hide();
			cutTitle(this);
		}
	});

	$('.delete_img').off('click');
	$('.delete_img').on('click', function () {
		cutTitle($(this).parent().prev().children().last().children().first());
		$(this).parent().prev().children().last().children().first().attr('data-is-change', "true");
		$(this).parent().prev().children().last().children().first().val(null);
		$(this).parent().hide();
		$(this).parent().prev().children().last().children().last().removeClass("color-change");
	});
}

function cutTitle(element) {
	var isValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

	var currentSpan = $(element).next();

	var filename = "";

	if (isValue) {
		filename = $(element).val().replace(/.*\\/, "");
	} else {
		filename = "Завантажити фото";
	}

	currentSpan.attr('title', filename);
}

function validateImageType(elem) {
	var filesExt = ['jpg', 'jpeg', 'png'];

	var parts = $(elem).val().split('.');
	if (filesExt.join().search(parts[parts.length - 1]) == -1) {
		$(this).val('');
		createSwal({
			title: 'Невірний формат файлу',
			type: 'error'
		});

		return false;
	}
	return true;
}

function handleChangeRadio(e) {
	var currentRadio = $(e.currentTarget),
	    customWidthInput = $(currentRadio).parent().parent().find('.customWidthInput'),
	    changeWidthBtn = $(currentRadio).parent().parent().find('.changeWidthBtn');

	if (currentRadio.val() === 'custom') {
		customWidthInput.attr('disabled', false);
		changeWidthBtn.attr('disabled', false);
	} else {
		customWidthInput.attr('disabled', true);
		changeWidthBtn.attr('disabled', true);
		setWidthImage(currentRadio.val(), currentRadio);
	}
}

function setWidthImage(widthImage, currentRadio) {
	var multiblocks = $(currentRadio).parent().parent().parent().parent().find('.multiblock');

	multiblocks.attr('data-width', widthImage);

	widthImage = widthImage ? widthImage : 10;

	[].concat(_toConsumableArray(multiblocks)).forEach(function (el) {
		$(el).parent().find('.html').click();
		var value = $(el).val();
		$(el).val(value.replace(/width:(\d+|auto)/g, 'width:' + widthImage));
		$(el).parent().find('.html').click();
	});
}

function setDefaultImageWidth(imageWidth) {
	if (imageWidth === 'auto') {
		$('#panel3 .enableAutoWidth').click();
	} else {
		imageWidth = imageWidth ? imageWidth : 10;

		if (imageWidthChoices.includes(imageWidth)) {
			var searchRadio = $('#panel3 .imageWidthContainer input[type="radio"][value=' + imageWidth + ']');
			searchRadio.attr('checked', true);
		} else {
			var customWidthRadio = $('#panel3 .imageWidthContainer .customWithRadio'),
			    customWidthInput = $('#panel3 .imageWidthContainer .customWidthInput'),
			    changeWidthBtn = $('#panel3 .imageWidthContainer .changeWidthBtn');

			customWidthRadio.click();
			customWidthInput.attr('disabled', false);
			changeWidthBtn.attr('disabled', false);
			customWidthInput.val(imageWidth);
		}
	}
}

module.exports = {
	addListenersToImages: addListenersToImages,
	handleChangeRadio: handleChangeRadio,
	setWidthImage: setWidthImage,
	setDefaultImageWidth: setDefaultImageWidth
};

/***/ }),

/***/ 10:
/***/ (function(module, exports) {

var imageWidthChoices = ['50', '75', '100', '125', '150'];

module.exports = {
	imageWidthChoices: imageWidthChoices
};

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var createSwal = function createSwal(params, callback) {
	var time = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 3000;

	swal(_extends({}, params, {
		closeOnClickOutside: true,
		allowOutsideClick: true
	}), callback);

	setTimeout(function () {
		return swal.close;
	}, time);
};

module.exports = {
	createSwal: createSwal
};

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function createSwal(params) {
	swal(_extends({}, params, {
		closeOnClickOutside: true,
		allowOutsideClick: true
	}));
}

module.exports = {
	createSwal: createSwal
};

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function createSwal(params) {
	swal(_extends({}, params, {
		closeOnClickOutside: true,
		allowOutsideClick: true
	}));
}

function redactorMultiblock() {

	var _this = this;

	$(this).htmlarea({
		// Override/Specify the Toolbar buttons to show
		toolbar: [["html", "bold", "italic", "underline", "strikethrough"], ["justifyleft", "justifycenter", "justifyright"], [{
			css: "justifyFull",
			text: "по ширині",
			action: function action(btn) {
				this.justifyFull();
			}

		}], [{
			css: "screen",
			text: "Додати зображення",
			action: function action(btn) {
				var _this2 = this;

				var popupImageConvertor = $(this)[0].container.prev();
				popupImageConvertor.css('display', 'flex');

				var el = popupImageConvertor[0].querySelector('.resizer');
				var getRes = popupImageConvertor[0].querySelector('.resizer-result');
				var close = popupImageConvertor.find('.closePopup');
				close.off('click');
				close.on('click', function (e) {
					e.preventDefault();
					el.innerHtml = '';
					popupImageConvertor.css('display', 'none');
				});

				if (el) {
					el.addEventListener('paste', function (e) {

						e.preventDefault();
						var clipboard = e.clipboardData;

						if (clipboard && clipboard.items) {

							var item = clipboard.items[0];

							if (item && item.type.indexOf('image/') > -1) {

								var blob = item.getAsFile();

								if (blob) {

									var reader = new FileReader();
									reader.readAsDataURL(blob);

									reader.onload = function (event) {
										var img = new Image();
										img.src = event.target.result;

										el.innerText = '';

										el.append(img);
										getRes.onclick = function (e) {

											var imageWidth = $(_this).closest('.category-block').find('input[type="radio"]:checked');

											imageWidth = typeof $(imageWidth).val() === 'undefined' ? $(_this).closest('.task-container').find('input[type="radio"]:checked') : imageWidth;

											imageWidth = typeof $(imageWidth).val() === 'undefined' ? $(_this).closest('.text-block').find('input[type="radio"]:checked') : imageWidth;

											imageWidth = typeof $(imageWidth).val() === 'undefined' ? $(_this).closest('.question_name').find('input[type="radio"]:checked') : imageWidth;

											if ($(imageWidth).val() === 'custom') {
												imageWidth = $(imageWidth).parent().next();
											}

											if (imageWidth.closest('.imageWidthContainer').find('.enableAutoWidth').hasClass('btn-success')) {
												imageWidth = 'auto';
											} else {
												imageWidth = $(imageWidth).val();
											}

											e.preventDefault();

											imageWidth = imageWidth ? imageWidth : 10;
											img.setAttribute("style", "width:" + imageWidth + "px");
											var iframe = popupImageConvertor.parent().find('.jHtmlArea iframe').contents().find('body');

											iframe.append(img);

											iframe.focus();

											el.innerHtml = '';

											popupImageConvertor[0].style.display = 'none';

											var parent = $(el).closest('.popup-container').parent();
											$(el).closest('.popup-container').remove();

											parent.prepend("<div class=\"popup-container\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h3 class=\"resize-title\">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"resizer\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"addition\">ctrl+v</span>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"resizer-result btn btn-info\">\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"closePopup\">\u2716</button>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>");
										};

										close.off('click');
										close.on('click', function (e) {
											var parent = $(el).closest('.popup-container').parent();
											$(el).closest('.popup-container').remove();

											parent.prepend("<div class=\"popup-container\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h3 class=\"resize-title\">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"resizer\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"addition\">ctrl+v</span>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"resizer-result btn btn-info\">\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"closePopup\">\u2716</button>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>");

											e.preventDefault();
											try {
												el.innerHtml = '';
											} catch (err) {
												popupImageConvertor = $(_this2)[0].container.prev();
											}
											el.innerHtml = '';
											popupImageConvertor.css('display', 'none');
										});
									};
								}
							}
						}
					});
					getRes.onclick = function (e) {
						e.preventDefault();
						createSwal({
							title: 'Вставте зображення',
							type: 'error'
						});
					};
				}
				$(this).focus();
			}
		}], ["subscript", "superscript"], ["orderedlist", "unorderedlist"]]
	});

	if ($('#edit-block .ToolBar')) {
		$('#edit-block .ToolBar .screen').closest('ul').css("display", "none");
	}
};

module.exports = {
	redactorMultiblock: redactorMultiblock
};

/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

var _require = __webpack_require__(1),
    setWidthImage = _require.setWidthImage;

module.exports = function () {
	var autoWidth = $('.enableAutoWidth');

	autoWidth.off('click');
	autoWidth.on('click', function (e) {
		e.preventDefault();

		var curBtn = $(e.currentTarget),
		    widthContainer = curBtn.closest('.imageWidthContainer');

		if (curBtn.hasClass('btn-danger')) {
			widthContainer.find('input').prop('disabled', true);
			widthContainer.find('.changeWidthBtn').prop('disabled', true);

			curBtn.removeClass('btn-danger');
			curBtn.addClass('btn-success');
			curBtn.text('Auto/on');

			setWidthImage('auto', widthContainer.find('input').first());
		} else {
			widthContainer.find('input[type="radio"]').prop('disabled', false);

			curBtn.addClass('btn-danger');
			curBtn.removeClass('btn-success');
			curBtn.text('Auto/off');

			widthContainer.find('input:radio[value="100"]').prop('checked', true);
			widthContainer.find('.customWidthInput').val('');

			setWidthImage(100, widthContainer.find('input').first());
		}
	});
};

/***/ }),

/***/ 63:
/***/ (function(module, exports, __webpack_require__) {

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var redactorMultiblock = __webpack_require__(4).redactorMultiblock;
var getImageSrc = __webpack_require__(7);
var createSwal = __webpack_require__(2).createSwal;

var _require = __webpack_require__(1),
    handleChangeRadio = _require.handleChangeRadio,
    setWidthImage = _require.setWidthImage;

var setWidthListeners = __webpack_require__(6);

window.onload = function () {

	$('#all').css('opacity', 1);
	var multiblocks = $('.multiblock');

	multiblocks.on('click', redactorMultiblock);

	var arrMultiblocks = [].concat(_toConsumableArray(multiblocks));
	$(document).find('.bookshelf_wrapper').show();
	arrMultiblocks.forEach(function (multiblock) {
		multiblock.click();
	});
	$(document).find('.bookshelf_wrapper').hide();

	var saveBtn = $('#statuses .btn-success');
	update();
	saveBtn.on('click', updateBlank);

	var addImageWidthRadio = $('.editBlockOmu .imageWidthContainer input[type="radio"]'),
	    addChangeWidthBtn = $('.editBlockOmu .changeWidthBtn');

	addImageWidthRadio.off();
	addImageWidthRadio.on('change', function (e) {
		return handleChangeRadio(e);
	});
	addChangeWidthBtn.off();
	addChangeWidthBtn.on('click', function (e) {
		e.preventDefault();
		var imageWidth = $(e.currentTarget).prev().val();
		setWidthImage(imageWidth, $(e.currentTarget).prev().prev().find('input'));
	});

	setWidthListeners();
};

function update() {
	var newComm = '<div class="category-block" style="display: block;">\n    <div class="row" style="margin-bottom: 5px">\n\t\t<div class="col-xs-12 form-inline">\n\t\t\t<div class = "hidden idsComm">0</div>\n\t\t\t<input class="item-category form-control commPosition" style="width: 45%" type="text" required placeholder="\u041F\u043E\u0441\u0430\u0434\u0430 \u0447\u043B\u0435\u043D\u0430 \u043A\u043E\u043C\u0456\u0441\u0456\u0457" >\n\t\t\t<input class="item-category form-control commName" style="width: 45%" type="text" required placeholder="\u041F\u0406\u0411 \u0447\u043B\u0435\u043D\u0430 \u043A\u043E\u043C\u0456\u0441\u0456\u0457" >\n\t\t<span  class = "category-add-comm" data-toggle="tooltip" title="\u0414\u043E\u0434\u0430\u0442\u0438 \u0447\u043B\u0435\u043D\u0430 \u043A\u043E\u043C\u0456\u0441\u0456\u0457">\n\t\t<button type="button"  class="btn btn-default">\n\t\t<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>\n\t\t</button>\n\t\t</span>\n\t\t<span class="category-delete-comm" data-toggle="tooltip" title="\u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u0447\u043B\u0435\u043D\u0430 \u043A\u043E\u043C\u0456\u0441\u0456\u0457" style="display: none;">\n\t\t<button type="button" class="btn btn-default">\n\t\t<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>\n\t\t\t</button>\n\t\t\t</span>\n\t\t</div> \n    </div>';
	var rmCommBtn = $('.category-delete-comm'),
	    addCommBtn = $('.category-add-comm'),
	    commContainer = $('#comm');
	updateListeners();

	var nmbComm = $(commContainer).find('.category-block').length;
	if (nmbComm > 1) {
		commContainer.find('.category-delete-comm').show();
	}

	function updateListeners() {
		rmCommBtn = $('.category-delete-comm');
		addCommBtn = $('.category-add-comm');

		addCommBtn.off('click');
		addCommBtn.on('click', addComm);

		rmCommBtn.off('click');
		rmCommBtn.on('click', rmComm);
	}

	function addComm() {

		var nmbComm = $(commContainer).find('.category-block').length;

		if (nmbComm < 5) {
			$('#comm').append(newComm);
			updateListeners();
			if (nmbComm > 0) {
				commContainer.find('.category-delete-comm').show();
			}
		} else {
			createSwal({
				title: 'Максимальна к-ть членів комісії',
				type: 'error'
			});
		}

		if (nmbComm == 1) {
			commContainer.first().find('.category-delete-comm').show();
		}
	}

	function rmComm() {
		var nmbComm = $(commContainer).find('.category-block').length,
		    comm = $(this).parent().parent().parent();

		if (nmbComm > 1) {
			comm.remove();
			updateListeners();
		}
		if (nmbComm == 2) {
			commContainer.first().find('.category-delete-comm').hide();
		}

		// show all adding buttons
		commContainer.find('.category-add-comm').toArray().map(function (el) {
			return $(el).show();
		});
	}
}

function validateQuestionsAndAnswers(element) {
	var valid = false,
	    currentValue = $(element).val().trim();

	if (currentValue.length > 1000000) {
		if ($(element).hasClass('item-name')) {
			$(element).parent().parent().after("<div class=\"validate-alert\">Поле не має перевищувати 1000000 символів</div>");
		} else {
			$(element).parent().after("<div class=\"validate-alert\">Поле не має перевищувати 1000000 символів</div>");
		}
		$(element).parent().removeClass("access-validate");
		$(element).parent().addClass("not-validate");
	} else if (currentValue.length < 1) {
		if ($(element).hasClass('item-name')) {
			$(element).parent().parent().after("<div class=\"validate-alert\">Поле не має бути пустим</div>");
		} else {
			$(element).parent().after("<div class=\"validate-alert\">Поле не має бути пустим</div>");
		}
		$(element).parent().removeClass("access-validate");
		$(element).parent().addClass("not-validate");
	} else {
		$(element).parent().removeClass("not-validate");
		$(element).parent().addClass("access-validate");
		valid = true;
	}
	return valid;
}

function cleanValidation(element) {
	$(element).parent().removeClass("access-validate");
	$(element).parent().removeClass("not-validate");
}

function updateBlank(e) {
	var submitBtn = $(e.currentTarget);
	$(document).find('.bookshelf_wrapper').show();
	$(document).find('.hideBehind').show();
	$('#all').css("opacity", "0.5");
	var typeSpeciality = $('#type_speciality').val();

	var valid = true;

	cleanValidation($('.multiblock'));

	$(".validate-alert").remove();

	$('.multiblock').each(function () {
		if (!validateQuestionsAndAnswers(this)) {
			valid = false;
		}
	});

	if (!valid) {
		$(document).find('.bookshelf_wrapper').hide();
		$(document).find('.hideBehind').hide();
		$('#all').css("opacity", "1");
		createSwal({
			title: 'Питання не пройшли валідацію',
			type: 'error'
		});
		return;
	}
	submitBtn.off('click');

	var blocksContent = $('.task-block'),
	    questionsContent = $('.question_name'),
	    answersContent = parseInt(typeSpeciality) === 0 ? $('.answer') : $('.answerContent');

	var tableNames = {
		blocks: 'blocks',
		questions: 'questions',
		answers: 'answers'
	};
	var imageNames = {
		questions: 'Question',
		answers: 'Answer'
	};

	var filteredBlocks = filterChangedElements([].concat(_toConsumableArray(blocksContent)), null, 'blocks');
	var filteredQuestions = filterChangedElements([].concat(_toConsumableArray(questionsContent)), null, 'questions');
	var filteredAnswers = filterChangedElements([].concat(_toConsumableArray(answersContent)), typeSpeciality);

	var elementsToSend = [{ elements: filteredBlocks, tableName: tableNames.blocks }, { elements: filteredQuestions, tableName: tableNames.questions, typeImage: imageNames.questions }, { elements: filteredAnswers, tableName: tableNames.answers, typeImage: imageNames.answers }];

	var commissionersContainers = [].concat(_toConsumableArray($('.category-block')));
	var commissioners = commissionersContainers.map(function (commissionerContainer) {
		var id = $(commissionerContainer).attr('data-id');
		var position = $(commissionerContainer).find('.commPosition').val();
		var initials = $(commissionerContainer).find('.commName').val();

		return { id: id, position: position, initials: initials };
	});

	var idSpeciality = $('#id_speciality').val();

	Promise.all([].concat(_toConsumableArray(elementsToSend.map(function (collectionElements) {
		return collectionElements.elements.length > 0 ? Promise.all(saveElements(collectionElements)) : [];
	})), [saveCommissioners(commissioners, idSpeciality)])).then(function () {
		$(document).find('.bookshelf_wrapper').hide();
		$(document).find('.hideBehind').hide();
		$('#all').css("opacity", "1");
		createSwal({
			title: 'Зміни збережено',
			type: 'success'
		}, function () {
			window.location.href = "/omu";
		});
	}).catch(function (err) {
		$(document).find('.bookshelf_wrapper').hide();
		$(document).find('.hideBehind').hide();
		$('#all').css("opacity", "1");
		createSwal({
			title: 'Виникла помилка при збереженні питань',
			type: 'error'
		});
		console.log(err);
	});
}

function filterChangedElements(collectionElements) {
	var typeSpeciality = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
	var collectionType = arguments[2];

	var filteredCollection = void 0;

	if (typeSpeciality) {
		filteredCollection = collectionElements.filter(function (element) {
			var multiblock = parseInt(typeSpeciality) === 0 ? $(element).find('.multiblock') : $(element),
			    checkbox = $(element).find('.correctAnswer'),
			    widthImageContainer = $(element).parent().parent().parent().parent().find('.imageWidthContainer'),
			    prevImageWidth = $(widthImageContainer).attr('data-prev-width'),
			    checkedWidthRadio = $(widthImageContainer).find('input[type="radio"]:checked').val(),
			    customWidthInput = $(widthImageContainer).find('.customWidthInput').val(),
			    currImageWidth = checkedWidthRadio === 'custom' ? customWidthInput : checkedWidthRadio,
			    isChangeMultiblock = $(multiblock).attr('data-prev-content') !== $(multiblock).val(),
			    isChangeCheckbox = !!parseInt($(checkbox).attr('data-prev-content')) !== $(checkbox).is(':checked'),
			    isChangeImageWidth = prevImageWidth !== currImageWidth;

			return isChangeCheckbox || isChangeMultiblock || isChangeImageWidth;
		});
	} else {
		filteredCollection = collectionElements.filter(function (container) {
			var element = null,
			    result = false;

			if (collectionType === 'blocks') {
				element = $(container).find('.blockName');

				result = $(element).attr('data-prev-content') !== $(element).val();
			} else if (collectionType === 'questions') {
				element = $(container).find('.questionContent');

				var prevImageWidth = $(container).find('.imageWidthContainer').attr('data-prev-width'),
				    autoImageBtn = $(container).find('.imageWidthContainer .enableAutoWidth'),
				    currImageWidth = void 0;

				if (autoImageBtn.hasClass('btn-success')) {
					currImageWidth = 'auto';
				} else {
					var checkedWidthRadio = $(container).find('.imageWidthContainer input[type="radio"]:checked').val(),
					    customWidthInput = $(container).find('.imageWidthContainer .customWidthInput').val(),
					    _currImageWidth = checkedWidthRadio === 'custom' ? customWidthInput : checkedWidthRadio;
				}

				var isChangeImageWidth = prevImageWidth !== currImageWidth,
				    isChangeContent = $(element).attr('data-prev-content') !== $(element).val();

				result = isChangeImageWidth || isChangeContent;
			} else {
				throw Error('You must specify correct parameter collectionType');
			}

			return result;
		});
	}

	return filteredCollection;
}

function saveElements(collection) {
	var typeSpeciality = $('#type_speciality').val(),
	    tableName = collection.tableName,
	    typeImage = collection.typeImage;

	return collection.elements.map(function (element) {
		var dataToSave = void 0,
		    blockId = $(document).find('.blockName').attr('data-id');
		if (tableName === 'answers' && typeSpeciality == 0) {
			var id = $(element).find('.multiblock').attr('data-id'),
			    contentData = $(element).find('.multiblock').val(),
			    isChecked = $(element).find('.correctAnswer').is(':checked'),
			    _getImageSrc = getImageSrc(contentData),
			    _getImageSrc2 = _slicedToArray(_getImageSrc, 2),
			    elementContent = _getImageSrc2[0],
			    images = _getImageSrc2[1];


			dataToSave = { tableName: tableName, id: id, elementContent: elementContent, images: images, typeImage: typeImage, isChecked: isChecked };
		} else {
			var _id = void 0,
			    _contentData = null;

			if (tableName === 'blocks' && typeSpeciality === 1) {
				_id = $(element).find('.blockName').attr('data-id');
				_contentData = $(element).find('.blockName').val();

				dataToSave = { tableName: tableName, id: _id, contentData: _contentData };
			} else if (tableName === 'answers' && parseInt(typeSpeciality) === 1) {
				_id = $(element).attr('data-id');
				_contentData = $(element).val();

				var _getImageSrc3 = getImageSrc(_contentData),
				    _getImageSrc4 = _slicedToArray(_getImageSrc3, 2),
				    _elementContent = _getImageSrc4[0],
				    _images = _getImageSrc4[1];

				dataToSave = { tableName: tableName, id: _id, elementContent: _elementContent, images: _images, typeImage: typeImage };
			} else {
				var imageWidth = void 0;
				if (tableName === 'blocks') {
					_id = $(element).find('.blockName').attr('data-id');
					_contentData = $(element).find('.blockName').val();
				} else {
					var autoWidthBtn = $(element).find('.imageWidthContainer .enableAutoWidth');
					if (autoWidthBtn.hasClass('btn-success')) {
						imageWidth = 'auto';
					} else {
						var checkedWidthRadio = $(element).find('.imageWidthContainer input[type="radio"]:checked').val(),
						    customWidthInput = $(element).find('.imageWidthContainer .customWidthInput').val();
						imageWidth = checkedWidthRadio === 'custom' ? customWidthInput : checkedWidthRadio;
					}

					_id = $(element).find('.questionContent').attr('data-id');
					_contentData = $(element).find('.questionContent').val();
				}

				var _getImageSrc5 = getImageSrc(_contentData),
				    _getImageSrc6 = _slicedToArray(_getImageSrc5, 2),
				    _elementContent2 = _getImageSrc6[0],
				    _images2 = _getImageSrc6[1];

				dataToSave = { tableName: tableName, id: _id, elementContent: _elementContent2, images: _images2, typeImage: typeImage, imageWidth: imageWidth };
			}
		}
		dataToSave.blockId = blockId;
		return $.ajax({
			url: '/update-blank',
			method: 'post',
			data: dataToSave
		});
	});
}

function saveCommissioners(commissioners, idSpeciality) {
	return $.ajax({
		url: '/update-commissioners',
		method: 'post',
		data: { commissioners: commissioners, idSpeciality: idSpeciality }
	});
}

module.exports = {
	validateQuestionsAndAnswers: validateQuestionsAndAnswers,
	cleanValidation: cleanValidation
};

/***/ }),

/***/ 7:
/***/ (function(module, exports) {

function getImageSrc(str) {
	var arraySrc = str.match(/src="(.*?)"/g);

	if (arraySrc != null) {
		var newarray = [];
		for (var i = 0; i < arraySrc.length; i++) {
			newarray.push(arraySrc[i].match(/"(.*?)"/)[1]);
		}
		str = str.replace(/src="(.*?)"/g, 'src="URLPATH"');
		return [str, newarray];
	} else {
		return [str, ''];
	}
}

module.exports = getImageSrc;

/***/ }),

/***/ 95:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(63);


/***/ })

/******/ });