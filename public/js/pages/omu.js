/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 85);
/******/ })
/************************************************************************/
/******/ ({

/***/ 13:
/***/ (function(module, exports) {

function getPopup(object) {
	var object_of_deletion = "";

	if (object == "responsible") {
		object_of_deletion = "користувача?";
	} else if (object == "department") {
		object_of_deletion = "кафедру? Це також видалить спеціальності та верифікуючі особи, прив'язані до неї";
	} else if (object == "specialization") {
		object_of_deletion = "спеціальність?";
	}

	var div = "<div class=\"confirmMenu\">\n            <div id = \"x\">\n                X\n            </div>\n        </div>\n        <div class=\"mid\">\n            <label> \u0412\u0438 \u0432\u043F\u0435\u0432\u043D\u0435\u043D\u0456 \u0449\u043E \u0445\u043E\u0447\u0435\u0442\u0435 \u0432\u0438\u0434\u0430\u043B\u0438\u0442\u0438  " + object_of_deletion + "</label>\n        </div>\n        <div class=\"mid\">\n            <div class=\"btn-group chooseContainer\" role=\"group\" aria-label=\"...\">\n                <div class=\"btn-group\" role=\"group\">\n                    <button type=\"button\" id=\"yes\" class=\"btn btn-success\">\u0422\u0430\u043A</button>\n                </div>\n                <div class=\"btn-group\" role=\"group\">\n                    <button type=\"button\" id=\"no\" class=\"btn btn-danger\">\u041D\u0456</button>\n                </div>\n            </div>\n        </div>";
	return div;
}

function hidePdf(name) {
	$("#" + name).hide();
}

function moving() {
	var mousePosition;
	var offset = [0, 0];
	var div;
	var isDown = false;
	div = document.getElementById("pdf_set");

	document.addEventListener('mouseup', function () {
		isDown = false;
	}, true);

	document.addEventListener('mousemove', function (event) {
		event.preventDefault();
		if (isDown) {
			mousePosition = {
				x: event.clientX,
				y: event.clientY
			};
			div.style.left = mousePosition.x + offset[0] + 'px';
			div.style.top = mousePosition.y + offset[1] + 126 + 'px';
		}
	}, true);

	var pdfMenu = document.getElementById("pdfMenu");

	if (pdfMenu) {
		pdfMenu.addEventListener('mousedown', function (e) {
			isDown = true;
			offset = [div.offsetLeft - e.clientX, div.offsetTop - e.clientY];
		}, true);
	}
}

function exitPdf(e) {
	var container = $("#showPdf");
	if (!container.is(e.target) && container.has(e.target).length === 0) {
		container.hide();
	}

	$('.pdfbtn').off('click');
}

module.exports = {
	moving: moving,
	hidePdf: hidePdf,
	exitPdf: exitPdf,
	getPopup: getPopup
};

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var createSwal = function createSwal(params, callback) {
	var time = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 3000;

	swal(_extends({}, params, {
		closeOnClickOutside: true,
		allowOutsideClick: true
	}), callback);

	setTimeout(function () {
		return swal.close;
	}, time);
};

module.exports = {
	createSwal: createSwal
};

/***/ }),

/***/ 60:
/***/ (function(module, exports, __webpack_require__) {

var popup = __webpack_require__(13);
var manage_blanks = __webpack_require__(61);
var generate_pdf = __webpack_require__(62);
var createSwal = __webpack_require__(2).createSwal;

function updateGenerated() {
	var table = $("#variant-blank-table");

	$.ajax({
		method: 'POST',
		url: '/get_speciality_department',

		success: function success(response) {
			table.html('');
			table.append('<tr>\n                            <th>\u041D\u0430\u0437\u0432\u0430 \u043A\u0430\u0444\u0435\u0434\u0440\u0438</th>\n                            <th>\u041D\u0430\u0437\u0432\u0430 \u0441\u043F\u0435\u0446\u0456\u0430\u043B\u044C\u043D\u043E\u0441\u0442\u0456</th>\n                            <th>\u0414\u0456\u0457</th>\n                         </tr>');
			response.forEach(function (element) {
				var row = "";
				row += '\n                        <tr>\n                            <td>\n                                <div>' + element.name_cafedres + '</div>\n                            </td>\n                            <td>\n                                <div>' + element.name_speciality + '</div>\n                            </td>\n                            <td>\n                                <button type="button" class="btn btn-default btn-info show-blanks" data-id-speciality="' + element.id + '">\n                                    \u041F\u0435\u0440\u0435\u0433\u043B\u044F\u0434 \u0432\u0430\u0440\u0456\u0430\u043D\u0442\u0456\u0432\n                                </button>\n                                <button class="btn btn-default genListBtn" data-id-speciality="' + element.id + '" type="button">\n                                    \u0421\u043F\u0438\u0441\u043E\u043A \u0437\u0430\u0432\u0434\u0430\u043D\u044C\n                                </button>\n                            </td>\n                        </tr>';

				table.append(row);
			});

			var showBlanksBtn = $('#variant-blank-table .show-blanks'),
			    genListBtn = $('#variant-blank-table .genListBtn');

			showBlanksBtn.off('click');
			showBlanksBtn.on('click', manage_blanks.manageBlanks);

			genListBtn.off('click');
			genListBtn.on('click', manage_blanks.generateList);
		},
		error: function error(_error) {
			console.log(_error);
		}
	});
}

function updateVerifyTable() {
	var table = $("#verifyTable");

	$.ajax({
		method: 'POST',
		url: '/get_specialties',

		success: function success(response) {
			table.html('');
			table.append('<tr>\n                            <th>\u041D\u0430\u0437\u0432\u0430 <br> \u0441\u043F\u0435\u0446\u0456\u0430\u043B\u044C\u043D\u043E\u0441\u0442\u0456</th>\n                            <th>\u0421\u0442\u0430\u0442\u0443\u0441</th>\n                            <th>\u0414\u0430\u0442\u0430 \u043E\u0441\u0442\u0430\u043D\u044C\u043E\u0457 <br> \u0437\u043C\u0456\u043D\u0438 \u0441\u0442\u0430\u0442\u0443\u0441\u0443</th>\n                            <th class="lastTh"></th>\n                        </tr>');
			response.forEach(function (element) {
				if (element.status != 'in_development') {
					var status = "";
					if (element.status == "in_process") {
						status = "на перевірці";
					} else if (element.status == "approved") {
						status = "підтверджено";
					} else {
						status = "на доопрацюванні";
					}

					var row = '<tr><td style = "display:none">' + element.id + '</td>';
					row += ' <td><div>' + element.name_speciality + '</div></td>\n                                <td><div>' + status + '</div></td>\n                                 <td><div>' + element.updated_at + '</div></td>';
					if (element.status == 'in_process') {
						row += ' <td class = "lastTd">                                          \n                                    <a href="/view-blank/' + element.id + '" type= "button" class="btn btn-default table-buttn">\u0412\u0456\u0434\u043A\u0440\u0438\u0442\u0438 \u0431\u043B\u0430\u043D\u043A\n                                    </a>\n                                    \n                                    <a href="/edit-blank/' + element.id + '" type= "button" class="btn btn-default table-buttn">\u0420\u0435\u0434\u0430\u0433\u0443\u0432\u0430\u0442\u0438 \u0431\u043B\u0430\u043D\u043A\n                                    </a>\n                                </td>';
					} else if (element.status == 'approved') {
						row += '\n                                <td class="lastTd">\n                                    <button data-id="' + element.id + '" type="button" class="btn btn-default btn-pdf table-buttn">\n                                        \u0415\u043A\u0437\u0430\u043C\u0435\u043D\u0430\u0446\u0456\u0439\u043D\u0456 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F (PDF)\n                                    </button>\n                                      <button data-id="' + element.id + '" type="button" class="btn btn-default btn-reverse table-buttn">\n                                       \u0412\u0456\u0434\u043C\u0456\u043D\u0438\u0442\u0438 \u0432\u0435\u0440\u0438\u0444\u0456\u043A\u0430\u0446\u0456\u044E\n                                    </button>\n                                    <a class="btn btn-default table-buttn" href="/edit-blank/' + element.id + '" type="button">\n                                        \u0420\u0435\u0434\u0430\u0433\u0443\u0432\u0430\u0442\u0438 \u0431\u043B\u0430\u043D\u043A\n                                    </a>\n                                </td>';
					} else {
						row += '\n                            <td>\n                                <button class="btn btn-default btn-denied" data-declined-reason="' + element.declined_reason + '" type="button">\n                                    \u041F\u0435\u0440\u0435\u0433\u043B\u044F\u043D\u0443\u0442\u0438 \u043F\u0440\u0438\u0447\u0438\u043D\u0443 \u0432\u0456\u0434\u043C\u043E\u0432\u0438\n                                </button>\n                            </td>';
					}
					row += '</tr>';
					table.append(row);
				}
			});

			var pdfGenBtn = $('#verifyTable .btn-pdf'),
			    deniedBtn = $('#verifyTable .btn-denied'),
			    reverseBtn = $('#verifyTable .btn-reverse');

			reverseBtn.off('click');
			reverseBtn.on('click', function (e) {
				var id = $(e.currentTarget).attr('data-id'),
				    isConfirm = confirm('Відмінити верифікацію?');

				if (isConfirm) {
					reverseVerification(id);
				}
			});

			pdfGenBtn.off('click');
			pdfGenBtn.on('click', function (e) {
				var id = $(e.currentTarget).attr('data-id');

				generate_pdf.openPdfMenu(id);
			});

			deniedBtn.off('click');
			deniedBtn.on('click', function (e) {
				var declinedReason = $(e.currentTarget).attr('data-declined-reason');

				openDeclined(declinedReason);
			});
		},
		error: function error(_error2) {
			console.log(_error2);
		}
	});
}

function reverseVerification(id) {

	$.ajax({
		method: 'POST',
		url: '/reverse_verification',
		data: { 'id': id },
		success: function success(response) {
			updateVerifyTable();
			createSwal({
				title: 'Верифікація відмінена',
				type: 'success'
			});
		},
		error: function error(_error3) {
			createSwal({
				title: 'Помилка при відміні верифікації',
				type: 'error'
			});
		}
	});
}

function openDeclined(reason) {
	createSwal({
		title: 'Причина відмови: ' + reason,
		type: 'info'
	});
}

function updateUserTable() {
	var table = $("#repTable");

	$.ajax({
		method: 'POST',
		url: '/get_data_for_table',
		success: function success(response) {
			table.html('');
			table.append('<tr>\n                            <th>\u0406\u043C\'\u044F</th>\n                            <th>\u041F\u0440\u0456\u0437\u0432\u0438\u0449\u0435</th>\n                            <th>\u0420\u043E\u043B\u044C</th>\n                            <th>Email</th>\n                            <th>\u041A\u0430\u0444\u0435\u0434\u0440\u0430</th>\n                       </tr>');
			response.users.forEach(function (element) {
				if (response.id != element.id) {
					var row = "";
					row += '<tr> <td><div>' + element.first_name + '</div></td>\n                                <td><div>' + element.last_name + '</div></td>\n                                 <td><div>' + element.name + '</div></td>\n                                 <td><div>' + element.email + '</div></td>';
					if (element.name_cafedres != null) {
						row += '<td><div>' + element.name_cafedres + '</div></td>';
					} else {
						row += "<td></td>";
					}

					row += '<td class = tableButtons>\n                                <span class="category-pencil" data-toggle="tooltip" title="\u0420\u0435\u0434\u0430\u0433\u0443\u0432\u0430\u0442\u0438 \u043A\u043E\u0440\u0438\u0441\u0442\u0443\u0432\u0430\u0447\u0430">\n                                    <a href="/edit-representative/' + element.id + '" type = "button" class="btn btn-default">\n                                        <span   class="glyphicon glyphicon-pencil" aria-hidden="true" ></span>\n                                        <span  style = "display:none;"  class="glyphicon glyphicon-floppy-disk" aria-hidden="true" ></span>\n                                    </a>\n                                 </span>\n                                <span class="category-trash r_margin" data-toggle="tooltip" title="\u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u043A\u043E\u0440\u0438\u0441\u0442\u0443\u0432\u0430\u0447\u0430">\n                                    <button  type="button" class="btn btn-default del_rep">\n                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>\n                                    </button>\n                                </span></td>\n                            <td style = "display:none" class = "users_id" >' + element.id + '</td>\n                        </tr>';

					table.append(row);
				}
			});

			$('.del_rep').on('click', deleteRepres);
		},
		error: function error(_error4) {
			console.log(_error4);
		}
	});
}

function selectDepChanged() {
	var table = $("#specTable");
	var depId = $("#depSelect").val();
	table.html('');

	$.ajax({
		method: 'POST',
		url: '/get_cafedres',
		data: {},
		success: function success(response) {
			response.forEach(function (cafedre) {
				if (cafedre.id == depId) {
					$("#selected_spec_name").html('\u0421\u043F\u0435\u0446\u0456\u0430\u043B\u044C\u043D\u043E\u0441\u0442\u0456 \u043A\u0430\u0444\u0435\u0434\u0440\u0438 ' + cafedre.name_cafedres);
				}
			});
			$.ajax({
				method: 'POST',
				url: '/get_specialties',
				data: {},
				success: function success(response) {
					var count = 0;

					response.forEach(function (speciality) {
						if (speciality.id_cafedres == depId) {
							count++;
							table.append('<tr>        <td style = "display:none" class = "special_id"> ' + speciality.id + '</td>\n                                <td><div>' + speciality.name_speciality + '</div>\n                                    <input  class=" sel_block hideSelect"></td>\n                                <td class = tableButtons>\n                                    <span class="category-pencil" data-toggle="tooltip" title="\u0420\u0435\u0434\u0430\u0433\u0443\u0432\u0430\u0442\u0438 \u0441\u043F\u0435\u0446\u0456\u0430\u043B\u044C\u043D\u0456\u0441\u0442\u044C" >\n                                        <a  href="/edit-speciality/' + speciality.id + '" type= "button" class="btn btn-default" >\n                                            <span   class="glyphicon glyphicon-pencil" aria-hidden="true" ></span>\n                                        </a>\n                                    </span>\n                                    <span class="category-trash r_margin" data-toggle="tooltip" title="\u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u0441\u043F\u0435\u0446\u0456\u0430\u043B\u044C\u043D\u0456\u0441\u0442\u044C">\n                                        <button type="button" class="btn btn-default del_special">\n                                            <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>\n                                        </button>\n                                    </span></td>\n                                </tr>');
						}
					});
					if (count === 0) {
						table.children().first().remove();
						table.prepend('<tr><th> \u0421\u043F\u0435\u0446\u0456\u0430\u043B\u044C\u043D\u043E\u0441\u0442\u0456 \u0434\u0430\u043D\u043D\u043E\u0457 \u043A\u0430\u0444\u0435\u0434\u0440\u0438 \u0432\u0456\u0434\u0441\u0443\u0442\u043D\u0456</th></tr>');
					} else {
						table.prepend('<tr><th>\u041D\u0430\u0437\u0432\u0430</th></tr>');
					}
					$('.del_special').on('click', deleteSpecialization);
					table.append('<tr class="omu_add2" style="cursor: pointer"><td style="background-color: #89CA9E;"><div>+ \u0414\u043E\u0434\u0430\u0442\u0438 \u0441\u043F\u0435\u0446\u0456\u0430\u043B\u044C\u043D\u0456\u0441\u0442\u044C</div></td></tr>');

					$('.omu_add2').click(function () {
						window.location.href = '/add-speciality/' + depId;
					});

					$('.omu_add2').hover(function () {
						$('.omu_add2 td').css('background-color', '#00a65a');
						$('.omu_add2 td').css('color', 'white');
					});

					$('.omu_add2').mouseleave(function () {
						$('.omu_add2 td').css('background-color', '#89CA9E');
						$('.omu_add2 td').css('color', 'black');
					});
				},
				error: function error(_error5) {
					console.log(_error5);
				}
			});
		},
		error: function error(_error6) {
			console.log(_error6);
		}
	});
}

function UpdateSelect() {
	$.ajax({
		method: 'POST',
		url: '/get_cafedres',
		data: {},
		success: function success(response) {
			var dep = $("#depSelect");
			dep.html('');
			response.forEach(function (cafedre) {
				var option = document.createElement("option");
				option.text = cafedre.name_cafedres;
				option.value = cafedre.id;
				dep.append(option);
			});
			dep.val('');
		},
		error: function error(_error7) {
			console.log(_error7);
		}
	});
}

function deleteRepres() {
	$('#confirmContainer').css({ display: 'flex' });
	$('#confirm').html(popup.getPopup("responsible"));

	var row = $(this).parent().parent().parent();

	$('#x, #no').on('click', function () {
		$('#confirmContainer').hide();
	});

	$('#yes').on('click', function () {
		row.remove();
		var id = row.find(".users_id").text();
		$('#confirmContainer').hide();

		$.ajax({
			method: 'POST',
			url: '/delete_representative',
			data: {
				'id': id
			},
			success: function success(response) {
				createSwal({
					title: 'Користувач успішно видалений',
					type: 'success'
				});
			},
			error: function error(_error8) {
				console.log(_error8);

				createSwal({
					title: 'Виникла помилка при видаленні користувача',
					type: 'error'
				});
			}
		});

		$('#confirmContainer').hide();
	});
}

function deleteDepartment() {
	$('#confirmContainer').css({ display: 'flex' });
	$('#confirm').html(popup.getPopup("department"));

	var row = $(this).parent().parent().parent();

	$('#x,#no').on('click', function () {
		$('#confirmContainer').hide();
	});

	$('#yes').on('click', function () {
		var id = row.find('.depart_id').text();
		$('#confirmContainer').hide();

		$.ajax({
			method: 'POST',
			url: '/delete_department',
			data: {
				'id': id
			},
			success: function success() {
				row.remove();
				$('#specTable').text('');
				UpdateSelect();

				$("#selected_spec_name").text("");
				updateUserTable();
				updateVerifyTable();
				updateGenerated();

				createSwal({
					title: 'Кафедра успішно видалена',
					type: 'success'
				});
			},
			error: function error(_error9) {
				console.log(_error9);

				createSwal({
					title: 'Виникла помилка при видаленні кафедри',
					type: 'error'
				});
			}
		});

		var rowCount = $("#depTable tr").length;

		if (rowCount < 3) {
			$("#specAdd").addClass('disabled');
		}

		$('#confirmContainer').hide();
	});
}

function validate_specialties_forms(id, valLength, req) {
	$(id).siblings().remove();
	var value = $(id).val();

	if (value.length > valLength) {
		$(id).after("<div class=\"validate-alert\">Поле не має перевищувати " + valLength + " символів</div>");
		$(id).css("border", "1px solid red");
	} else if (value.trim() == 0 && req) {
		$(id).after("<div class=\"validate-alert\">Поле не має бути пустим</div>");
		$(id).css("border", "1px solid red");
	} else {
		$(id).css("border", "1px solid green");
		return true;
	}
	return false;
}

function save_speciality(e) {
	$('#saving .save-btn').prop('disabled', true);

	var id = e;
	var name = $("#name").val();
	var number = $("#number").val();
	var dep = $("#dep").val();
	var title = $("#title").val();
	var reason = $("#reason").val();
	var type = $("#typeSpec option:selected").val();

	var nameIsValid = validate_specialties_forms("#name", 190, true);
	var numberIsValid = validate_specialties_forms("#number", 500, true);
	var titleIsValid = validate_specialties_forms("#title", 500, true);
	var reasonIsValid = validate_specialties_forms("#reason", 500, true);
	if (nameIsValid && numberIsValid && titleIsValid && reasonIsValid) {
		if (id == 0) {
			$.ajax({
				method: 'POST',
				url: '/add_specialisation',
				data: {
					'name': name,
					'number': number,
					'dep': dep,
					'title': title,
					'reason': reason,
					'type': type
				},
				success: function success(response) {
					$('#saving .save-btn').prop('disabled', false);

					if (response == 'error') {
						swal({
							title: "Спеціальність не була створена",
							type: "error"
						});
					}
					swal({
						title: "Спеціальніть була успішно збережена",
						type: "success"
					}, function () {
						window.location.href = "/omu";
					});
				},
				error: function error(_error10) {
					$('#saving .save-btn').prop('disabled', false);
					console.log(_error10);
				}
			});
		} else {
			$.ajax({
				method: 'POST',
				url: '/update_specialisation',
				data: {
					'id': id,
					'name': name,
					'number': number,
					'dep': dep,
					'title': title,
					'reason': reason
				},
				success: function success() {
					swal({
						title: "Спеціальніть була успішно збережена",
						type: "success"
					}, function () {
						$('#saving .save-btn').prop('disabled', false);
						window.location.href = "/omu";
					});
				},

				error: function error(_error11) {
					swal({
						title: "Спеціальність не була оновлена",
						type: "error"
					});
					$('#saving .save-btn').prop('disabled', false);
					console.log(_error11);
				}
			});
		}
	} else {
		$('#saving .save-btn').prop('disabled', false);
	}
}

function deleteSpecialization() {
	var table = $("#specTable");

	$('#confirmContainer').css({ display: 'flex' });
	$('#confirm').html(popup.getPopup("specialization"));

	var row = $(this).parent().parent().parent();

	$('#x, #no').on('click', function () {
		$('#confirmContainer').hide();
	});

	$('#yes').on('click', function () {
		row.remove();
		$('#confirmContainer').hide();

		var rowCount = $("#specTable tr").length;

		if (rowCount == 2) {
			table.children().first().remove();
			table.prepend('<t\u0440><th> \u0421\u043F\u0435\u0446\u0456\u0430\u043B\u044C\u043D\u043E\u0441\u0442\u0456 \u0434\u0430\u043D\u043D\u043E\u0457 \u043A\u0430\u0444\u0435\u0434\u0440\u0438 \u0432\u0456\u0434\u0441\u0443\u0442\u043D\u0456</th></t\u0440>');
		}

		var id = row.find(".special_id").text();

		$.ajax({
			method: 'POST',
			url: '/delete_specialisation',
			data: {
				'id': id
			},
			success: function success() {
				updateUserTable();
				updateVerifyTable();
				updateGenerated();
				createSwal({
					title: 'Спеціальність успішно видалена',
					type: 'success'
				});
			},
			error: function error(_error12) {
				console.log(_error12);

				createSwal({
					title: 'Виникла помилка при видаленні спеціальності',
					type: 'error'
				});
			}
		});
	});
}

module.exports = {
	updateUserTable: updateUserTable,
	updateVerifyTable: updateVerifyTable,
	updateGenerated: updateGenerated,
	selectDepChanged: selectDepChanged,
	save_speciality: save_speciality,
	deleteDepartment: deleteDepartment,
	validate_specialties_forms: validate_specialties_forms
};

/***/ }),

/***/ 61:
/***/ (function(module, exports, __webpack_require__) {

var createSwal = __webpack_require__(2).createSwal;

function manageBlanks(e) {
	var idSpeciality = $(e.currentTarget).attr('data-id-speciality');

	$.ajax({
		url: '/get-blanks',
		method: 'post',
		data: { idSpeciality: idSpeciality },
		success: function success(response) {
			if (response.length > 0) {
				if (response.length > 13) {
					$('#listBlankPopup').addClass('scroll-class');
				} else {
					$('#listBlankPopup').removeClass('scroll-class');
				}

				renderBlankList(response);
			} else {
				createSwal({
					title: 'Немає згенерованих бланків',
					text: 'Перейдіть на сторінку верифікація бланків та згенеруйте бланки',
					type: 'info'
				});
			}
		},
		error: function error(err) {
			createSwal({
				title: 'Не вдалося отримати бланки',
				type: 'error'
			});
			console.log(err);
		}
	});
}

function updateButtonsListeners() {
	var viewBlankBtn = $('.view-blank'),
	    downloadTaskBtn = $('.download-task'),
	    downloadAnswers = $('.download-answers'),
	    deleteBlankBtn = $('.delete-blank');

	viewBlankBtn.off('click');
	downloadTaskBtn.off('click');
	downloadAnswers.off('click');
	deleteBlankBtn.off('click');

	viewBlankBtn.on('click', viewBlank);
	downloadTaskBtn.on('click', downloadBlank);
	downloadAnswers.on('click', downloadBlank);
	deleteBlankBtn.on('click', deleteBlank);
}

function renderBlankList(blankData) {
	$('#close-btn').off('click');
	$('#close-btn').on('click', removePopup);

	function removePopup() {
		var listBlank = $('#listBlankPopup');
		var deleteBlur = $('#blur-screen');
		listBlank.addClass('hidden');
		deleteBlur.addClass('hidden');
	}

	var blurScreen = $('#blur-screen'),
	    popup = $('#listBlankPopup'),
	    list = $('.listBlanks tbody');

	list.empty();

	var _iteratorNormalCompletion = true;
	var _didIteratorError = false;
	var _iteratorError = undefined;

	try {
		for (var _iterator = blankData[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
			var blank = _step.value;

			var newBlank = '\n            <tr>\n                <td>' + blank['variant'] + '</td> \n                <td>\n                    <button class="btn btn-default btn-info view-blank" data-id-blank="' + blank['id'] + '" value="' + blank['url_blank'] + '">\n                        \u041F\u0435\u0440\u0435\u0433\u043B\u044F\u043D\u0443\u0442\u0438 \u0431\u043B\u0430\u043D\u043A\n                    </button>\n                    </td> \n                <td>\n                    <button class="btn btn-default btn-info download-task" data-id-blank="' + blank['id'] + '" value="' + blank['url_blank'] + '">\n                        \u0417\u0430\u0432\u0430\u043D\u0442\u0430\u0436\u0438\u0442\u0438 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F\n                    </button>\n                </td> \n                <td>\n                    <button class="btn btn-default btn-info download-answers" data-id-blank="' + blank['id'] + '" value="' + blank['url_blank'] + '">\n                        \u0417\u0430\u0432\u0430\u043D\u0442\u0430\u0436\u0438\u0442\u0438 \u0431\u043B\u0430\u043D\u043A \u0456\u0437 \u0432\u0456\u0434\u043F\u043E\u0432\u0456\u0434\u044F\u043C\u0438\n                    </button>\n                    </td> \n                <td>\n                    <button class="btn btn-default btn-warning delete-blank" data-id-blank="' + blank['id'] + '">\n                        \u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u0437\u0433\u0435\u043D\u0435\u0440\u043E\u0432\u0430\u043D\u0438\u0439 \u0432\u0430\u0440\u0456\u0430\u043D\u0442\n                    </button>\n                </td> \n            </tr>';

			list.append(newBlank);
		}
	} catch (err) {
		_didIteratorError = true;
		_iteratorError = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion && _iterator.return) {
				_iterator.return();
			}
		} finally {
			if (_didIteratorError) {
				throw _iteratorError;
			}
		}
	}

	updateButtonsListeners();

	blurScreen.removeClass('hidden');
	popup.removeClass('hidden');
}

function viewBlank() {
	var button = $(this),
	    listBlank = $('#listBlankPopup'),
	    iframe = $('#pdf-frame');

	var baseUrl = button.attr('value'),
	    url = baseUrl + '/view.pdf';

	iframe.attr('src', url);

	listBlank.addClass('hidden');
	iframe.removeClass('hidden');
}

function downloadBlank() {
	var button = $(this);

	var actionType = button.attr('class'),
	    baseUrl = button.val();

	if (actionType.includes('download-task')) {
		loadBlanks('task', baseUrl);
	} else if (actionType.includes('download-answers')) {
		loadBlanks('answers', baseUrl);
	} else {
		console.log('Err');
	}
}

function loadBlanks(typeBlank, baseUrl) {

	$(document).find('.bookshelf_wrapper').show();
	$(document).find('.hideBehind').show();
	$('#all').css("opacity", "0.5");

	var nmbToDownload = 1;

	var url = '';
	if (typeBlank === 'task') {
		url = baseUrl + '/task.pdf';
	} else if (typeBlank === 'answers') {
		url = baseUrl + '/answer.pdf';
	}

	$('body').append($('<a id="downloadLink" style="display:none;"></a>'));
	var newLink = $('#downloadLink');

	newLink.attr('href', url);
	newLink.attr('download', typeBlank);

	for (var i = 0; i < nmbToDownload; i++) {
		newLink[0].click();
	}

	$(newLink).remove();

	$(document).find('.bookshelf_wrapper').hide();
	$(document).find('.hideBehind').hide();
	$('#all').css("opacity", "1");
}

function deleteBlank() {
	var button = $(this),
	    idBlank = button.attr('data-id-blank');

	var isConfirmed = confirm('Ви дійсно хочете видалити даний згенерований варіант?');

	if (isConfirmed) {
		var blur = $('#blur-screen'),
		    listBlanksPopup = $('#listBlankPopup');

		blur.addClass('hidden');
		listBlanksPopup.addClass('hidden');

		$.ajax({
			url: '/delete-blank',
			method: 'post',
			data: { idBlank: idBlank },
			success: function success() {
				createSwal({
					title: 'Успішно видалено',
					type: 'success'
				});
			},
			error: function error(err) {
				createSwal({
					title: 'Помилка при спробі видалення бланку',
					type: 'error'
				});
				console.log(err);
			}
		});
	}
}

function generateList() {
	var specialityId = $(this).attr('data-id-speciality');
	var data = {
		specialityId: specialityId
	};

	$.ajax({
		url: '/generateList',
		method: 'POST',
		data: data,
		success: function success(res) {
			if (res.url) {

				var blurScreen = $('#blur-screen'),
				    iframe = $('#pdf-frame');

				blurScreen.removeClass('hidden');
				iframe.attr('src', res.url);
				iframe.removeClass('hidden');
			} else {
				createSwal({
					title: res + ' \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F \u0443\u0441\u043F\u0456\u0448\u043D\u043E \u0437\u0433\u0435\u043D\u0435\u0440\u043E\u0432\u0430\u043D\u0456',
					type: 'success'
				});
			}
		},
		error: function error(err) {
			createSwal({
				title: 'Помилка при генерації Pdf',
				type: 'error'
			});

			console.log(err);
		}
	});
}

module.exports = {
	manageBlanks: manageBlanks,
	loadBlanks: loadBlanks,
	generateList: generateList
};

/***/ }),

/***/ 62:
/***/ (function(module, exports, __webpack_require__) {

var createSwal = __webpack_require__(2).createSwal;

function openPdfMenu(specialityId) {
	var container = $("#showPdf");

	container.css({ display: 'flex' });

	var pdfBtn = $('#generatePdf');

	$(".inp").on('input', updateVal);

	pdfBtn.off('click');
	pdfBtn.on('click', function () {
		return generatePdf(specialityId);
	});
}

function updateVal() {
	var variants = $('#variants'),
	    nmbVariants = $(variants).val(),
	    maxVar = variants.attr("max");

	nmbVariants = +nmbVariants > +maxVar ? maxVar : nmbVariants;
	$(variants).val(nmbVariants);
}

function generatePdf(specialityId) {
	var variants = $('#variants').val();

	variants = variants ? variants : 1;

	var data = {
		variants: variants,
		specialityId: specialityId
	};
	$("#showPdf").hide();
	$(document).find('.bookshelf_wrapper').show();
	$(document).find('.hideBehind').show();
	$('#all').css("opacity", "0.5");

	$.ajax({
		url: '/generatePdf',
		method: 'POST',
		data: data,
		success: function success(res) {
			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");
			if (res.url) {
				var blurScreen = $('#blur-screen'),
				    iframe = $('#pdf-frame');

				blurScreen.removeClass('hidden');
				iframe.attr('src', res.url);
				iframe.removeClass('hidden');
			} else {
				createSwal({
					title: res + ' \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F \u0443\u0441\u043F\u0456\u0448\u043D\u043E \u0437\u0433\u0435\u043D\u0435\u0440\u043E\u0432\u0430\u043D\u0456',
					type: 'success'
				});
			}
		},
		error: function error(err) {
			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");

			var _err$responseJSON = err.responseJSON,
			    nmbGenVariants = _err$responseJSON.nmbGenVariants,
			    variantsToGen = _err$responseJSON.variantsToGen;


			if (err.status === 400) {
				createSwal({
					title: nmbGenVariants > 0 ? '\u0411\u0443\u043B\u043E \u0443\u0441\u043F\u0456\u0448\u043D\u043E \u0437\u0433\u0435\u043D\u0435\u0440\u043E\u0432\u0430\u043D\u043E ' + nmbGenVariants + ' \u0456\u0437 ' + variantsToGen + ' \u0432\u0430\u0440\u0456\u0430\u043D\u0442\u0456\u0432. \u041D\u0435\u0434\u043E\u0441\u0442\u0430\u0442\u043D\u044C\u043E \u043F\u0438\u0442\u0430\u043D\u044C \u0434\u043B\u044F \u0433\u0435\u043D\u0435\u0440\u0435\u0446\u0456\u0457 \u043D\u043E\u0432\u0438\u0445 \u0431\u0456\u043B\u0435\u0442\u0456\u0432' : 'Недостатньо питань для генерації нового унікального білету',
					text: 'Необхідно очистити історію питань, які вже брали участь у генерації білету або створити нові питання',
					type: 'info'
				});
				$("#showPdf").show();

				var genBlock = $('#generateBlock');
				var resetTasks = $('#resetTasks');

				genBlock.addClass('hidden');
				resetTasks.removeClass('hidden');

				resetTasks.off('click');
				resetTasks.on('click', function () {
					return resetQuestionsHistory(specialityId);
				});
			} else {
				createSwal({
					title: 'Помилка при генерації Pdf',
					type: 'error'
				});
			}
			console.log(err);
		}
	});
}

function resetQuestionsHistory(idSpeciality) {
	$.ajax({
		url: '/reset-questions-history',
		method: 'post',
		data: { idSpeciality: idSpeciality },
		success: function success() {
			createSwal({
				title: 'Історія генерації питань успішно очищена',
				type: 'success'
			});

			var genBlock = $('#generateBlock');
			var resetTasks = $('#resetTasks');

			genBlock.removeClass('hidden');
			resetTasks.addClass('hidden');
		},
		error: function error(err) {
			createSwal({
				title: 'Помилка при спробі очищення історії генерації питань',
				type: 'error'
			});
			console.log(err);
		}
	});
}

module.exports = {
	openPdfMenu: openPdfMenu
};

/***/ }),

/***/ 85:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(86);


/***/ }),

/***/ 86:
/***/ (function(module, exports, __webpack_require__) {

window.onload = function () {
	$('#add_speciality').hide();

	var popup = __webpack_require__(13);
	var tables = __webpack_require__(60);

	$("#depSelect").val('');
	popup.moving();
	$('#x').on('click', function () {
		$('#generateBlock').removeClass('hidden');
		$('#resetTasks').addClass('hidden');

		popup.hidePdf("showPdf");
	});

	$('#exit, #closeDeclined').on('click', function () {
		$("#declined").hide();
	});

	$('a[data-toggle="tab"]').on('click', function () {
		tables.updateUserTable();
		tables.updateVerifyTable();
		tables.updateGenerated();
	});

	var blurScreen = $('#blur-screen'),
	    pdfFrame = $('#pdf-frame'),
	    listBlank = $('#listBlankPopup'),
	    nmbToDownloadBlank = $('#nmbToDownloadBlank');

	blurScreen.on('click', function () {
		blurScreen.addClass('hidden');
		pdfFrame.addClass('hidden');
		listBlank.addClass('hidden');
		nmbToDownloadBlank.addClass('hidden');
	});

	$('#depSelect').on('change', function () {
		$('#add_speciality').show();
	});
	$('#depSelect').on('change', tables.selectDepChanged);

	$('#depTable .category-trash .btn').on('click', tables.deleteDepartment);

	$('.omu_add1').click(function () {
		window.location.href = "/new-department";
	});

	$(document).find('.bookshelf_wrapper').hide();
	$(document).find('.hideBehind').hide();
	$('#all').css("opacity", "1");
};

/***/ })

/******/ });