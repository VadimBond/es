/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 87);
/******/ })
/************************************************************************/
/******/ ({

/***/ 13:
/***/ (function(module, exports) {

function getPopup(object) {
	var object_of_deletion = "";

	if (object == "responsible") {
		object_of_deletion = "користувача?";
	} else if (object == "department") {
		object_of_deletion = "кафедру? Це також видалить спеціальності та верифікуючі особи, прив'язані до неї";
	} else if (object == "specialization") {
		object_of_deletion = "спеціальність?";
	}

	var div = "<div class=\"confirmMenu\">\n            <div id = \"x\">\n                X\n            </div>\n        </div>\n        <div class=\"mid\">\n            <label> \u0412\u0438 \u0432\u043F\u0435\u0432\u043D\u0435\u043D\u0456 \u0449\u043E \u0445\u043E\u0447\u0435\u0442\u0435 \u0432\u0438\u0434\u0430\u043B\u0438\u0442\u0438  " + object_of_deletion + "</label>\n        </div>\n        <div class=\"mid\">\n            <div class=\"btn-group chooseContainer\" role=\"group\" aria-label=\"...\">\n                <div class=\"btn-group\" role=\"group\">\n                    <button type=\"button\" id=\"yes\" class=\"btn btn-success\">\u0422\u0430\u043A</button>\n                </div>\n                <div class=\"btn-group\" role=\"group\">\n                    <button type=\"button\" id=\"no\" class=\"btn btn-danger\">\u041D\u0456</button>\n                </div>\n            </div>\n        </div>";
	return div;
}

function hidePdf(name) {
	$("#" + name).hide();
}

function moving() {
	var mousePosition;
	var offset = [0, 0];
	var div;
	var isDown = false;
	div = document.getElementById("pdf_set");

	document.addEventListener('mouseup', function () {
		isDown = false;
	}, true);

	document.addEventListener('mousemove', function (event) {
		event.preventDefault();
		if (isDown) {
			mousePosition = {
				x: event.clientX,
				y: event.clientY
			};
			div.style.left = mousePosition.x + offset[0] + 'px';
			div.style.top = mousePosition.y + offset[1] + 126 + 'px';
		}
	}, true);

	var pdfMenu = document.getElementById("pdfMenu");

	if (pdfMenu) {
		pdfMenu.addEventListener('mousedown', function (e) {
			isDown = true;
			offset = [div.offsetLeft - e.clientX, div.offsetTop - e.clientY];
		}, true);
	}
}

function exitPdf(e) {
	var container = $("#showPdf");
	if (!container.is(e.target) && container.has(e.target).length === 0) {
		container.hide();
	}

	$('.pdfbtn').off('click');
}

module.exports = {
	moving: moving,
	hidePdf: hidePdf,
	exitPdf: exitPdf,
	getPopup: getPopup
};

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var createSwal = function createSwal(params, callback) {
	var time = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 3000;

	swal(_extends({}, params, {
		closeOnClickOutside: true,
		allowOutsideClick: true
	}), callback);

	setTimeout(function () {
		return swal.close;
	}, time);
};

module.exports = {
	createSwal: createSwal
};

/***/ }),

/***/ 87:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(88);


/***/ }),

/***/ 88:
/***/ (function(module, exports, __webpack_require__) {

var popup = __webpack_require__(13);
var createSwal = __webpack_require__(2).createSwal;

window.onload = function () {
    $("#depSelect").val('');
    popup.moving();
    $('#x').on('click', function () {
        popup.hidePdf("pdf_set");
    });

    $(document).mouseup(function (e) {
        popup.exitPdf(e);
    });

    var approveBtn = $('.panel-footer .btn-success'),
        declineBtn = $('.panel-footer .btn-danger'),
        idSpeciality = approveBtn.attr('data-id-speciality');

    approveBtn.on('click', function () {
        return approve(idSpeciality);
    });
    declineBtn.on('click', popDecline);
};

function popDecline() {
    $('#pdf_set').css({ display: "block", left: "50%", top: "60%", transform: "translateX(-50%) translateY(-50%)", boxShadow: "0 0 100vw 100vw rgba(0, 0, 0, 0.5)" });

    $('#x,#no').on('click', function () {
        $('#pdf_set').hide();
    });

    $('#yes').on('click', function () {
        var reason = document.getElementById("reason").value;
        if (!reason) {
            createSwal({
                title: "Введіть причину",
                type: 'error'
            });
        } else {
            document.getElementById("statuses").style.display = "none";
            document.getElementById("back").style.display = "block";
            $('#pdf_set').hide();

            var idSpeciality = $('#pdf_set').attr('data-id-speciality');
            decline(idSpeciality, reason);
        }
    });
}

function approve(id) {
    document.getElementById("statuses").style.display = "none";
    document.getElementById("back").style.display = "block";
    $.ajax({
        method: 'POST',
        url: '/edit_status_specialty',
        data: {
            'id': id,
            'status': 'approved'
        },
        success: function success(response) {
            swal({
                title: "Бланк був успішно верифікований",
                type: "success"
            }, function () {
                window.location.href = "/omu";
            });
        },
        error: function error(_error) {
            console.log(_error);

            swal({
                title: "Помилка при верифікацій бланку",
                type: "error"
            });
        }
    });
}

function decline(id, reason) {
    $.ajax({
        method: 'POST',
        url: '/edit_status_specialty',
        data: {
            'id': id,
            'status': 'declined',
            'reason': reason
        },
        success: function success(response) {
            swal({
                title: "Бланк був успішно відправлений на доопрацювання",
                type: "success"
            }, function () {
                window.location.href = "/omu";
            });
        },
        error: function error(_error2) {
            console.log(_error2);

            swal({
                title: "Помилка при верифікацій бланку",
                type: "error"
            });
        }
    });
}

/***/ })

/******/ });