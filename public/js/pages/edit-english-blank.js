/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 96);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var createSwal = __webpack_require__(3).createSwal;
var imageWidthChoices = __webpack_require__(10).imageWidthChoices;

function addListenersToImages() {
	var editImageWidthRadio = $('#panel3 .imageWidthContainer input[type="radio"]'),
	    editChangeWidthBtn = $('#panel3 .changeWidthBtn');

	editImageWidthRadio.off('change');
	editImageWidthRadio.on('change', function (e) {
		return handleChangeRadio(e, 'edit-task');
	});
	editChangeWidthBtn.off();
	editChangeWidthBtn.on('click', function (e) {
		e.preventDefault();
		var imageWidth = $(e.currentTarget).prev().val();
		setWidthImage(imageWidth, 'edit-task', $(e.currentTarget).prev().prev().find('input'));
	});

	$('.image-buttons').off('change');
	$('.image-buttons').on('change', function () {
		var isValideType = validateImageType(this);
		if (this.value && isValideType) {
			$(this).next().addClass("color-change");
			$(this).parent().parent().next().show();
			cutTitle(this, true);
		} else {
			$(this).next().removeClass("color-change");
			$(this).parent().parent().next().find(".imgDel").hide();
			cutTitle(this);
		}
	});

	$('.delete_img').off('click');
	$('.delete_img').on('click', function () {
		cutTitle($(this).parent().prev().children().last().children().first());
		$(this).parent().prev().children().last().children().first().attr('data-is-change', "true");
		$(this).parent().prev().children().last().children().first().val(null);
		$(this).parent().hide();
		$(this).parent().prev().children().last().children().last().removeClass("color-change");
	});
}

function cutTitle(element) {
	var isValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

	var currentSpan = $(element).next();

	var filename = "";

	if (isValue) {
		filename = $(element).val().replace(/.*\\/, "");
	} else {
		filename = "Завантажити фото";
	}

	currentSpan.attr('title', filename);
}

function validateImageType(elem) {
	var filesExt = ['jpg', 'jpeg', 'png'];

	var parts = $(elem).val().split('.');
	if (filesExt.join().search(parts[parts.length - 1]) == -1) {
		$(this).val('');
		createSwal({
			title: 'Невірний формат файлу',
			type: 'error'
		});

		return false;
	}
	return true;
}

function handleChangeRadio(e) {
	var currentRadio = $(e.currentTarget),
	    customWidthInput = $(currentRadio).parent().parent().find('.customWidthInput'),
	    changeWidthBtn = $(currentRadio).parent().parent().find('.changeWidthBtn');

	if (currentRadio.val() === 'custom') {
		customWidthInput.attr('disabled', false);
		changeWidthBtn.attr('disabled', false);
	} else {
		customWidthInput.attr('disabled', true);
		changeWidthBtn.attr('disabled', true);
		setWidthImage(currentRadio.val(), currentRadio);
	}
}

function setWidthImage(widthImage, currentRadio) {
	var multiblocks = $(currentRadio).parent().parent().parent().parent().find('.multiblock');

	multiblocks.attr('data-width', widthImage);

	widthImage = widthImage ? widthImage : 10;

	[].concat(_toConsumableArray(multiblocks)).forEach(function (el) {
		$(el).parent().find('.html').click();
		var value = $(el).val();
		$(el).val(value.replace(/width:(\d+|auto)/g, 'width:' + widthImage));
		$(el).parent().find('.html').click();
	});
}

function setDefaultImageWidth(imageWidth) {
	if (imageWidth === 'auto') {
		$('#panel3 .enableAutoWidth').click();
	} else {
		imageWidth = imageWidth ? imageWidth : 10;

		if (imageWidthChoices.includes(imageWidth)) {
			var searchRadio = $('#panel3 .imageWidthContainer input[type="radio"][value=' + imageWidth + ']');
			searchRadio.attr('checked', true);
		} else {
			var customWidthRadio = $('#panel3 .imageWidthContainer .customWithRadio'),
			    customWidthInput = $('#panel3 .imageWidthContainer .customWidthInput'),
			    changeWidthBtn = $('#panel3 .imageWidthContainer .changeWidthBtn');

			customWidthRadio.click();
			customWidthInput.attr('disabled', false);
			changeWidthBtn.attr('disabled', false);
			customWidthInput.val(imageWidth);
		}
	}
}

module.exports = {
	addListenersToImages: addListenersToImages,
	handleChangeRadio: handleChangeRadio,
	setWidthImage: setWidthImage,
	setDefaultImageWidth: setDefaultImageWidth
};

/***/ }),

/***/ 10:
/***/ (function(module, exports) {

var imageWidthChoices = ['50', '75', '100', '125', '150'];

module.exports = {
	imageWidthChoices: imageWidthChoices
};

/***/ }),

/***/ 11:
/***/ (function(module, exports) {

function addNewAnswer(answer, addQuestion, addAnswer, deleteQuestion, deleteAnswer, isRadio) {
	var newAnswer = '<div class="row ';
	if (isRadio) {
		newAnswer += 'item answer-item" style="margin-bottom: 5px;">\n               \t\t<div class="col-xs-12 form-inline "  style="width: 100%">';
	} else {
		newAnswer += 'category" style="margin-bottom: 5px;">\n                \t\t<div class="col-xs-12 form-inline "  style="width: 100%">';
	}

	if (isRadio) {
		newAnswer += '<div class="answer-item">\n                        <span data-toggle="tooltip" title="\u0412\u0456\u0440\u043D\u0430 \u0432\u0456\u0434\u043F\u043E\u0432\u0456\u0434\u044C">\n                            <input type="checkbox" name="cat-radio" class="correct-answer" ';

		if (answer != null) {
			newAnswer += answer['answer_right'] ? 'checked' : '';
		}

		newAnswer += '></span>';
	}

	newAnswer += '<div class="popupImageConvertor">\n                    <div class="popup-container">\n                        <h3 class="resize-title">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n                        <div class="resizer" >\n                            <span class="addition">ctrl+v</span>\n                        </div>\n                        <button class="resizer-result" class="btn btn-info">\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n                        <button class="closePopup">&#10006;</button>\n                    </div>\n                </div>\n                <textarea class="multiblock ';

	if (isRadio) {
		newAnswer += 'item-name " data-id-answer="';
	} else {
		newAnswer += 'question-name " data-id-answer="';
	}

	if (answer != null) {
		newAnswer += answer['id'];
	}

	newAnswer += '" name="text_answer_for_edit">';

	if (answer != null) {
		if (typeof answer.text_answer !== 'undefined') {
			newAnswer += answer.text_answer;
		} else {
			newAnswer += answer.text_question;
		}
	}

	newAnswer += '</textarea>';

	if (addQuestion) {
		newAnswer += '<div class="adding-btns">\n\t\t\t\t\t\t<span class="category-add r_margin" data-toggle="tooltip" title="\u0414\u043E\u0434\u0430\u0442\u0438 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F">\n\t\t\t\t\t\t\t<button type="button" class="btn btn-default">\n\t\t\t\t\t\t\t\t<span aria-hidden="true"><span class="glyphicon glyphicon-plus"></span> \u0414\u043E\u0434\u0430\u0442\u0438 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F</span>\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t</span>';
	}

	if (addAnswer) {
		newAnswer += '<button type="button" class="btn btn-default addNewAnswer"><span class="glyphicon glyphicon-plus"></span> \u0414\u043E\u0434\u0430\u0442\u0438 \u0432\u0430\u0440\u0456\u0430\u043D\u0442</button>';
	}

	if (deleteQuestion) {
		newAnswer += '<span class="category-delete" data-toggle="tooltip" title="\u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u043F\u0438\u0442\u0430\u043D\u043D\u044F">\n\t\t\t\t\t  \t<button type="button" class="btn btn-default">\n\t\t\t\t\t\t\t<span><span class="glyphicon glyphicon-trash"  aria-hidden="true"></span> \u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u043F\u0438\u0442\u0430\u043D\u043D\u044F</span>\n\t\t\t\t\t\t</button>\n\t\t\t\t\t  </span>\n\t\t\t\t\t</div>';
	}

	if (deleteAnswer) {
		newAnswer += '<div class="delete-block">\n\t\t\t\t\t\t<button type="button" class="btn btn-default deleteAnswer" style="display: none">\n\t\t\t\t\t\t\t<span><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> \u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u0432\u0430\u0440\u0456\u0430\u043D\u0442</span>\n\t\t\t\t\t\t</button> \n\t\t\t\t\t</div>';
	}
	if (isRadio) {
		newAnswer += '</div>';
	}
	newAnswer += '</div></div>';

	return newAnswer;
}

module.exports = {
	addNewAnswer: addNewAnswer
};

/***/ }),

/***/ 12:
/***/ (function(module, exports, __webpack_require__) {

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var getSettings = __webpack_require__(59);

var _require = __webpack_require__(27),
    renderFirstAnswers = _require.renderFirstAnswers;

var _require2 = __webpack_require__(25),
    listColContentChoices = _require2.listColContentChoices,
    newRowAnswerList = _require2.newRowAnswerList,
    newRowList = _require2.newRowList,
    newManageRowBlock = _require2.newManageRowBlock,
    booleanAnswer = _require2.booleanAnswer,
    newTaskContainer = _require2.newTaskContainer,
    newTextBlock = _require2.newTextBlock,
    newListAnswerBlock = _require2.newListAnswerBlock,
    newListBlock = _require2.newListBlock,
    questionContainer = _require2.questionContainer,
    newTestBlock = _require2.newTestBlock,
    manageButtonsBlock = _require2.manageButtonsBlock,
    questionMultiblock = _require2.questionMultiblock;

var resetEnglishTasks = __webpack_require__(28);

var _require3 = __webpack_require__(4),
    redactorMultiblock = _require3.redactorMultiblock;

var _require4 = __webpack_require__(1),
    setWidthImage = _require4.setWidthImage,
    handleChangeRadio = _require4.handleChangeRadio;

var getImageSrc = __webpack_require__(7);

var _require5 = __webpack_require__(10),
    imageWidthChoices = _require5.imageWidthChoices;

var createSwal = __webpack_require__(3).createSwal;
var setWidthListeners = __webpack_require__(6);

var settings = null;

var renderEnglishTask = function renderEnglishTask() {

	resetEnglishTasks();
	var manageButtonsBlock = $('#panel2 .manage-buttons-block');
	manageButtonsBlock.show();

	$('#panel2 .task-container .adding-btns').hide();
	$('#panel2 .task-container').show();
	$('#categories').find('p').show();
	$('#categories').find('button').last().show();

	if ($('#panel2 .text-block').is(":visible")) {
		$('#panel2 .text-block').find('.html').click();
		$('#panel2 .text-block').find('.multiblock').val('');
		$('#panel2 .text-block').find('.html').click();
	}

	if ($('#panel2 .questions-block').is(":visible")) {
		$('#panel2 .questions-block').find('.html').click();
		$('#panel2 .questions-block').find('.multiblock').val('');
		$('#panel2 .questions-block').find('.html').click();
	}

	settings = getSettings();
	return settings.then(function (result) {
		var addEnglishTaskBtn = $('#panel2 .add-english-task'),
		    isText = result.isText,
		    isListAnswer = result.isListAnswer,
		    isBooleanAnswers = result.isBooleanAnswers,
		    nmbQuestions = result.nmbQuestions,
		    nmbAnswers = result.nmbAnswers,
		    extraAnswers = result.extraAnswers,
		    container = void 0,
		    textContainer = void 0,
		    listContainer = void 0,
		    listAnswerContainer = void 0,
		    questionContainer = void 0,
		    answersContainer = void 0,
		    addNewAnswerBtn = void 0;


		var activeTab = $('#block_selector').is(':visible') ? 'add' : 'edit';

		if (activeTab === 'add') {
			container = $('#panel2 #categories');
			textContainer = $('#panel2 .text-block');
			listContainer = $('#panel2 .list-block');
			questionContainer = $('#panel2 .category-block');
			answersContainer = $('#panel2 #first_answer_item');
			addNewAnswerBtn = $('#panel2 #categories .addNewAnswer');
			listAnswerContainer = $('#panel2 .list-answers-block');

			var imageWidthContainer = textContainer.find('.imageWidthContainer').first();

			imageWidthContainer.next().remove();
			imageWidthContainer.after(questionMultiblock);
			imageWidthContainer.next().find('.text-question').on('click', redactorMultiblock);

			imageWidthContainer = questionContainer.find('.imageWidthContainer').first();

			imageWidthContainer.next().remove();
			imageWidthContainer.after(questionMultiblock);
			imageWidthContainer.next().find('.text-question').on('click', redactorMultiblock);
		} else {
			container = $('#panel3 #editingQuestions');
			textContainer = $('#panel3 .text-block');
			listContainer = $('#panel3 .list-block');
			questionContainer = $('#panel3 .category-block');
			answersContainer = $('#panel3 #first_edit_item');
			addNewAnswerBtn = $('#panel3 #editingQuestions .addNewAnswer');
			listAnswerContainer = $('#panel3 .list-answers-block');

			$('#panel3 button[type="submit"]').show();
			$('#panel3 .questions-block').show();
		}

		container.show();
		answersContainer.empty();

		if (!isText) {
			textContainer.hide();
			listContainer.hide();

			if (isListAnswer) {
				var tableAnswersRows = $(listAnswerContainer).find('table tbody');

				tableAnswersRows.empty();
				for (var indexRow = 0; indexRow < nmbQuestions; indexRow++) {
					tableAnswersRows.append(newRowAnswerList(indexRow));
				}
				listAnswerContainer.show();

				var contentCol = 'list',
				    tableQuestionsRows = $(listContainer).find('table tbody');

				listContainer.find('.change-content-col').text(listColContentChoices[contentCol]);

				tableQuestionsRows.empty();

				var initialIndexRow = 0;

				if (!isBooleanAnswers && $('#block_selector_for_edit').is(':visible')) {
					var number = parseInt($('#tasks_selector_for_edit option:selected').attr('data-width'));

					for (var i = 0; i < number; i++) {
						tableQuestionsRows.append(newRowList(nmbQuestions, initialIndexRow, isBooleanAnswers, 0, 'askii'));
					}
				} else {
					tableQuestionsRows.append(newRowList(nmbQuestions, initialIndexRow, isBooleanAnswers, 0, 'askii'));
				}

				tableQuestionsRows.append(newManageRowBlock);
				listContainer.show();
			}
		} else {
			textContainer.show();
			var textQuestion = $('#panel2 .text-block .html');

			if ($(textQuestion).is(':visible')) {
				textQuestion.click();
				$('#panel2 .text-question').val('');
				textQuestion.click();
			}

			if (isListAnswer) {
				var _contentCol = isBooleanAnswers ? 'bool' : 'default',
				    tableRows = $(listContainer).find('table tbody');

				listContainer.find('.change-content-col').text(listColContentChoices[_contentCol]);

				tableRows.empty();
				var _indexRow = 0;
				for (_indexRow; _indexRow < nmbQuestions + extraAnswers; _indexRow++) {
					tableRows.append(newRowList(nmbQuestions, _indexRow, isBooleanAnswers, extraAnswers));
				}

				listContainer.show();
			} else {
				listContainer.hide();
			}
		}

		// just for displaying questions
		if (!isListAnswer) {
			var nmbAnswersToRender = isText ? nmbAnswers : nmbQuestions;

			questionContainer.show();

			if (isBooleanAnswers) {
				addNewAnswerBtn.hide();
				answersContainer.append(booleanAnswer);
			} else {
				addNewAnswerBtn.show();
				renderFirstAnswers(nmbAnswersToRender, answersContainer);
			}

			if (isText) {
				$('#panel2 .category-block:first').find('.category').remove();
				$('#panel2 .category-block:first').find('.imageWidthContainer').after(questionMultiblock);
				$('#panel2 .category-block:first').find('.text-question').on('click', redactorMultiblock);

				var questionBlock = $('#panel2 .questions-block');

				if ($('#block_selector_for_edit').is(':visible')) {
					questionBlock = $('#panel3 .questions-block');
				}
				// render nmbQuestions - 1 questions
				for (var indexQuestion = 1; indexQuestion < nmbQuestions; indexQuestion++) {
					var testBlock = $(newTestBlock(activeTab)),
					    _answersContainer = testBlock.find('.answers-container');

					questionBlock.append(testBlock);

					if (isBooleanAnswers) {
						_answersContainer.append(booleanAnswer);
					} else {
						renderFirstAnswers(nmbAnswersToRender, _answersContainer);
					}
				}
			}
		} else {
			questionContainer.hide();
		}

		addEnglishTaskBtn.show();
		updateListeners();

		$('#panel2 input[type="radio"][value="100"]').prop('checked', true);

		if ($('.question-name').closest('.category-block').is(':visible')) {
			var _questionBlock = $('#panel2 .questions-block:first .html');
			_questionBlock.click();
			$('#panel2 .questions-block:first .question-name').val('');
			_questionBlock.click();
		}
	});
};

var addEnglishTask = function addEnglishTask(e) {
	e.preventDefault();
	var activeTab = 'add';

	settings.then(function (result) {
		var lastTask = $('#panel2 .task-container:last'),
		    rmEnglishBtns = $('#panel2 .rm-english-task'),
		    isText = result.isText,
		    isListAnswer = result.isListAnswer,
		    isBooleanAnswers = result.isBooleanAnswers,
		    nmbQuestions = result.nmbQuestions,
		    nmbAnswers = result.nmbAnswers,
		    extraAnswers = result.extraAnswers;


		var newTask = $(newTaskContainer);

		if (isText) {
			newTask.append(newTextBlock());

			if (isListAnswer) {
				var listContainer = $(newListBlock);
				newTask.append(listContainer);

				var contentCol = isBooleanAnswers ? 'bool' : 'default',
				    tableRows = $(listContainer).find('table tbody');

				listContainer.find('.change-content-col').text(listColContentChoices[contentCol]);

				var indexRow = 0;
				for (indexRow; indexRow < nmbQuestions + extraAnswers; indexRow++) {
					tableRows.append(newRowList(nmbQuestions, indexRow, isBooleanAnswers, extraAnswers));
				}
			}
		} else {
			if (isListAnswer) {
				var listAnswerContainer = $(newListAnswerBlock);
				newTask.append(listAnswerContainer);

				var tableAnswersRows = $(listAnswerContainer).find('table tbody');

				for (var _indexRow2 = 0; _indexRow2 < nmbQuestions; _indexRow2++) {
					tableAnswersRows.append(newRowAnswerList(_indexRow2));
				}

				var _listContainer = $(newListBlock),
				    _contentCol2 = 'list',
				    tableQuestionsRows = $(_listContainer).find('table tbody');

				newTask.append(_listContainer);

				_listContainer.find('.change-content-col').text(listColContentChoices[_contentCol2]);

				var initialIndexRow = 0;
				tableQuestionsRows.append(newRowList(nmbQuestions, initialIndexRow, isBooleanAnswers, 0, 'askii'));
				tableQuestionsRows.append(newManageRowBlock);
			}
		}

		if (!isListAnswer) {
			var testContainer = $(questionContainer),
			    testBlock = $(newTestBlock(activeTab, newTask)),
			    answersContainer = testBlock.find('.answers-container'),
			    nmbAnswersToRender = isText ? nmbAnswers : nmbQuestions;

			testContainer.append(testBlock);
			newTask.append(testContainer);

			if (isBooleanAnswers) {
				answersContainer.append(booleanAnswer);
			} else {
				renderFirstAnswers(nmbAnswersToRender, answersContainer);
			}

			if (isText) {
				for (var indexQuestion = 1; indexQuestion < nmbQuestions; indexQuestion++) {
					var _testContainer = $(questionContainer),
					    _testBlock = $(newTestBlock(activeTab, newTask)),
					    _answersContainer2 = _testBlock.find('.answers-container');
					_testContainer.append(_testBlock);
					newTask.append(_testContainer);
					if (isBooleanAnswers) {
						_answersContainer2.append(booleanAnswer);
					} else {
						renderFirstAnswers(nmbAnswersToRender, _answersContainer2);
					}
				}
			}
		}

		newTask.append(manageButtonsBlock);
		lastTask.after(newTask);

		rmEnglishBtns.show();

		updateListeners();
	});
};

var rmEnglishTask = function rmEnglishTask(e) {
	e.preventDefault();

	var countTasks = $('#panel2 .task-container').length,
	    rmTaskBtn = $('#panel2 .task-container .rm-english-task');

	if (countTasks >= 2) {
		var tasksContainer = [].concat(_toConsumableArray($('#panel2 .task-container'))),
		    currBtn = $(e.currentTarget),
		    currTaskContainer = tasksContainer.filter(function (taskContainer) {
			return $(taskContainer).find(currBtn).length > 0;
		});

		$(currTaskContainer).remove();
	}

	if (countTasks <= 2) {
		rmTaskBtn.hide();
	}
};

function validateEnglish() {
	var taskContainers = $('.task-container');
	var boxRight = 0;
	var countRight = 0;
	var valRight = 0;
	var questionRight = 0;
	var questionType = 0;
	var filledQuestions = 0;
	var question = void 0;

	var textItems = $('.task-container .item-name');
	var boxes = 0;

	var _iteratorNormalCompletion = true;
	var _didIteratorError = false;
	var _iteratorError = undefined;

	try {
		for (var _iterator = taskContainers[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
			var area = _step.value;

			var Containers = $(area).find('.category-block:visible');
			var currentCount = 0;
			question = 0;
			var text = $(area).find('.text-block:visible .text-question');

			if (text.length > 0) {
				var countNNN = ($(text).val().match(/NNN/g) || []).length;

				if ($('#answersCount').val() != countNNN) {
					currentCount = 1;
					countRight = 1;
				}

				if ($(text).val() === '') {
					valRight = 1;
				}
			}

			var _iteratorNormalCompletion3 = true;
			var _didIteratorError3 = false;
			var _iteratorError3 = undefined;

			try {
				for (var _iterator3 = Containers[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var container = _step3.value;

					var anyBoxesChecked = false;

					$(container).find('input[type="checkbox"]').each(function () {
						boxes++;
						if ($(this).is(":checked")) {
							anyBoxesChecked = true;
						}
					});

					if (!anyBoxesChecked && boxes > 0) {
						boxRight = 1;
					}

					var questionText = $(container).find('.category  .question-name, .category  .text-question');

					if (questionText) {

						if ($(questionText).val() == '') {
							question = 1;
						}
						if ($(questionText).val()) {
							filledQuestions = 1;
						}
					}
				}
			} catch (err) {
				_didIteratorError3 = true;
				_iteratorError3 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError3) {
						throw _iteratorError3;
					}
				}
			}

			var questions = $(Containers).find('.category .question-name, .category  .text-question');

			if (question == 1 && currentCount == 1) {
				questionRight = 1;
			}

			if (questions.length > 0 && text.val()) {
				questionType = 1;
			} else if (questions.length > 0 && !text.val()) {
				questionType = 2;
			}
		}
	} catch (err) {
		_didIteratorError = true;
		_iteratorError = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion && _iterator.return) {
				_iterator.return();
			}
		} finally {
			if (_didIteratorError) {
				throw _iteratorError;
			}
		}
	}

	var _iteratorNormalCompletion2 = true;
	var _didIteratorError2 = false;
	var _iteratorError2 = undefined;

	try {
		for (var _iterator2 = textItems[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
			var _area = _step2.value;

			if ($(_area).val() == '') {
				valRight = 1;
			}
		}
	} catch (err) {
		_didIteratorError2 = true;
		_iteratorError2 = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion2 && _iterator2.return) {
				_iterator2.return();
			}
		} finally {
			if (_didIteratorError2) {
				throw _iteratorError2;
			}
		}
	}

	var errorMessage = "Неправильно заповнені завдання:\n";

	if (countRight == 1 && questionType == 0 && $('#answersCount').val() != "0") {
		errorMessage += "- в тексті має бути присутня необхідна кількість вставок питань('NNN')\n";
	}

	if (valRight == 1) {
		errorMessage += "- заповніть всі необхідні поля\n";
	}

	if (boxRight == 1) {
		errorMessage += "- оберіть правильний варіант для усіх завдань\n";
	}

	if (question == 1 && filledQuestions == 1) {
		errorMessage += "- заповніть усі умови завдань\n";
	}

	if (questionRight == 1 && questionType == 1) {
		errorMessage += "- вставте необхідну кількість питань в текст АБО заповніть умови завдань";
	}

	if (countRight == 1 && questionType == 0 && $('#answersCount').val() != "0" || valRight == 1 || boxRight == 1 || questionRight == 1 && questionType == 1 || question == 1 && filledQuestions == 1) {
		createSwal({
			title: errorMessage,
			type: 'error'
		});
		return false;
	} else {
		return true;
	}
}

var saveEnglishTask = function saveEnglishTask(e) {
	settings.then(function (result) {
		var valid = validateEnglish();

		if (valid) {
			(function () {
				$(document).find('.bookshelf_wrapper').show();
				$(document).find('.hideBehind').show();
				$('#all').css("opacity", "0.5");

				var tasks = $(e.currentTarget).find('.task-container'),
				    idBlock = $('#block_selector').val(),
				    isText = result.isText,
				    isListAnswer = result.isListAnswer,
				    isBooleanAnswers = result.isBooleanAnswers;


				var resultToSend = [];
				var _iteratorNormalCompletion4 = true;
				var _didIteratorError4 = false;
				var _iteratorError4 = undefined;

				try {
					for (var _iterator4 = tasks[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
						var task = _step4.value;

						var textBlock = void 0,
						    listBlockRows = void 0,
						    taskToSend = { idBlock: idBlock };

						if (isText) {
							textBlock = $(task).find('.text-block');
							var imageWidthContainer = $(textBlock).find('.imageWidthContainer'),
							    checkedCheckbox = imageWidthContainer.find('input[type="radio"]:checked').val(),
							    imageWidth = checkedCheckbox !== 'custom' ? checkedCheckbox : imageWidthContainer.find('.customWidthInput').val(),
							    _getImageSrc = getImageSrc(textBlock.find('.text-question').val()),
							    _getImageSrc2 = _slicedToArray(_getImageSrc, 2),
							    content = _getImageSrc2[0],
							    images = _getImageSrc2[1];


							if (textBlock.find('.enableAutoWidth').hasClass('btn-success')) {
								imageWidth = 'auto';
							}

							taskToSend.textData = {
								content: content,
								images: images,
								imageWidth: imageWidth
							};

							if (isListAnswer) {
								listBlockRows = $(task).find('.list-block table tbody tr');

								taskToSend.listData = [].concat(_toConsumableArray(listBlockRows)).map(function (row) {
									var textColData = $(row).find('.textCol input'),
									    indexLetter = $(row).find('td:first').text(),
									    resultCol = $(row).find('.resultCol'),
									    keyName = isBooleanAnswers ? 'boolCol' : 'selectCol',
									    resultColData = { indexLetter: indexLetter },
									    dataKey = isBooleanAnswers ? 'isChecked' : 'position';

									resultColData[dataKey] = resultCol.find('select').val();

									return _defineProperty({
										textCol: textColData.val()
									}, keyName, resultColData);
								});
							}
						} else {
							if (isListAnswer) {
								var _listBlockRows = void 0,
								    listAnswerRows = void 0;

								listAnswerRows = $(task).find('.list-answers-block table tbody tr');

								taskToSend.listAnswerData = [].concat(_toConsumableArray(listAnswerRows)).map(function (row) {
									var indexLetter = $(row).find('td:first').text(),
									    textData = $(row).find('.textCol input').val();

									return {
										indexLetter: indexLetter,
										textData: textData
									};
								});

								_listBlockRows = $(task).find('.list-block table tbody tr:not(.adding-row)');

								taskToSend.listData = [].concat(_toConsumableArray(_listBlockRows)).map(function (row) {
									var textColData = $(row).find('.textCol input'),
									    indexLetter = $(row).find('td:first').text(),
									    resultCol = $(row).find('.resultCol'),
									    keyName = 'selectCol',
									    resultColData = { indexLetter: indexLetter };

									resultColData.position = resultCol.find('select').val();

									return _defineProperty({
										textCol: textColData.val()
									}, keyName, resultColData);
								});
							}
						}

						if (!isListAnswer) {
							var questionData = {},
							    questions = $(task).find('.category-block');

							if (isBooleanAnswers) {
								questionData = [].concat(_toConsumableArray(questions)).map(function (question) {
									var questionTextarea = $(question).find('.question-name'),
									    booleanAnswer = $(question).find('.items .booleanAnswer'),
									    imageWidthContainer = $(question).find('.imageWidthContainer'),
									    checkedCheckbox = imageWidthContainer.find('input[type="radio"]:checked').val(),
									    imageWidth = checkedCheckbox !== 'custom' ? checkedCheckbox : imageWidthContainer.find('.customWidthInput').val();

									if ($(question).find('.enableAutoWidth').hasClass('btn-success')) {
										imageWidth = 'auto';
									}

									return {
										question: getImageSrc(questionTextarea.val()),
										isCorrect: booleanAnswer.find('input[type="checkbox"]').is(':checked') ? 1 : 0,
										imageWidth: imageWidth
									};
								});
							} else {
								questionData = [].concat(_toConsumableArray(questions)).map(function (question) {
									var questionTextarea = $(question).find('.question-name'),
									    answers = $(question).find('.items .item'),
									    imageWidthContainer = $(question).find('.imageWidthContainer'),
									    checkedCheckbox = imageWidthContainer.find('input[type="radio"]:checked').val(),
									    imageWidth = checkedCheckbox !== 'custom' ? checkedCheckbox : imageWidthContainer.find('.customWidthInput').val();

									if ($(question).find('.enableAutoWidth').hasClass('btn-success')) {
										imageWidth = 'auto';
									}

									if (typeof questionTextarea.val() === 'undefined') {
										questionTextarea = $(question).find('.text-question');
									}

									return {
										question: getImageSrc(questionTextarea.val()),
										answers: [].concat(_toConsumableArray(answers)).map(function (answer) {
											var answerTextarea = $(answer).find('.item-name'),
											    answerCheckbox = $(answer).find('.correct-answer');

											return {
												answer: getImageSrc(answerTextarea.val()),
												isChecked: answerCheckbox.is(':checked') ? 1 : 0
											};
										}),
										imageWidth: imageWidth
									};
								});
							}

							taskToSend.questions = questionData;
						}
						resultToSend.push(taskToSend);
					}
				} catch (err) {
					_didIteratorError4 = true;
					_iteratorError4 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion4 && _iterator4.return) {
							_iterator4.return();
						}
					} finally {
						if (_didIteratorError4) {
							throw _iteratorError4;
						}
					}
				}

				var taskPromises = resultToSend.map(function (taskData) {
					return $.ajax({
						url: '/create-english-task',
						method: 'post',
						data: taskData
					});
				});

				Promise.all(taskPromises).then(function () {
					createSwal({
						title: 'Завдання успішно збережено',
						type: 'success'
					});

					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					$('#send-form button').prop('disabled', false);

					$('.task-container').hide();
					$('#categories').find('p').hide();
					$('#categories').find('button').last().hide();
					$('#block_selector').val('');

					resetEnglishTasks();
				}).catch(function (err) {
					createSwal({
						title: 'Помилка при сбереженні завдання',
						type: 'error'
					});

					$('#send-form button').prop('disabled', false);

					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					console.log(err);
				});
			})();
		} else {
			$('#send-form button').prop('disabled', false);
		}
	});
};

var onChangeListSelect = function onChangeListSelect(e) {
	var curSelect = $(e.currentTarget),
	    prevVal = curSelect.attr('data-previous-val'),
	    curVal = curSelect.val(),
	    prevSelect = curSelect.closest('tbody').find('select[data-previous-val="' + curVal + '"]');

	var booleanContent = ['Ні', 'Так', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'];
	if (!booleanContent.includes($(curSelect).find('option:selected').text().trim())) {
		curSelect.find('option[value="' + curVal + '"]').prop('selected', true);
		prevSelect.find('option[value="' + prevVal + '"]').prop('selected', true);

		curSelect.attr('data-previous-val', curVal);
		prevSelect.attr('data-previous-val', prevVal);
	}
};

var addQuestionRow = function addQuestionRow(e, nmbQuestions, isBooleanAnswers) {
	e.preventDefault();

	var maxRows = 15,
	    currBtn = $(e.currentTarget),
	    listBlocks = $(currBtn).closest('.list-block'),
	    currContainer = [].concat(_toConsumableArray(listBlocks)).filter(function (listBlock) {
		return $(listBlock).find(currBtn).length > 0;
	}),
	    rmBtn = $(currContainer).find('.rmQuestionRow'),
	    tableRows = $(currContainer).find('tbody tr:not(.adding-row)'),
	    tableRowsNmb = tableRows.length;

	if (tableRowsNmb < maxRows) {
		$(currContainer).find('.adding-row').before(newRowList(nmbQuestions, tableRowsNmb, isBooleanAnswers, 0, 'askii'));
	} else {
		createSwal({ title: '\u041C\u0430\u043A\u0441\u0438\u043C\u0430\u043B\u044C\u043D\u0430 \u043A\u0456\u043B\u044C\u043A\u0456\u0441\u0442\u044C \u043F\u0438\u0442\u0430\u043D\u044C - ' + maxRows + '.', type: 'error' });
	}

	rmBtn.removeAttr('disabled');
};

var rmQuestionRow = function rmQuestionRow(e) {
	e.preventDefault();
	var currBtn = $(e.currentTarget),
	    listBlocks = $(currBtn).closest('.list-block'),
	    currContainer = [].concat(_toConsumableArray(listBlocks)).filter(function (listBlock) {
		return $(listBlock).find(currBtn).length > 0;
	}),
	    tableRows = $(currContainer).find('tbody tr:not(.adding-row)'),
	    tableRowsNmb = tableRows.length,
	    rowToRemove = tableRows.last();

	if (tableRowsNmb == 2) {
		currBtn.attr('disabled', true);
	}
	if (tableRowsNmb > 1) {
		$(rowToRemove).remove();
	}
};

var updateListeners = function updateListeners() {
	settings.then(function (result) {
		var isText = result.isText,
		    isListAnswer = result.isListAnswer,
		    nmbQuestions = result.nmbQuestions,
		    isBooleanAnswers = result.isBooleanAnswers,
		    mainContainer = $('#block_selector_for_edit').is(':visible') ? $('#panel3') : $('#panel2'),
		    addEnglishTaskBtn = mainContainer.find('.add-english-task'),
		    rmEnglishTaskBtn = mainContainer.find('.rm-english-task'),
		    listBlock = mainContainer.find('.list-block'),
		    listSelect = listBlock.find('select'),
		    listAddBtn = listBlock.find('.addQuestionRow'),
		    listRmBtn = listBlock.find('.rmQuestionRow'),
		    multiblocks = mainContainer.find('.multiblock'),
		    addImageWidthRadio = $(mainContainer).find('.imageWidthContainer input[type="radio"]'),
		    addChangeWidthBtn = $(mainContainer).find('.changeWidthBtn');


		addEnglishTaskBtn.off('click');
		addEnglishTaskBtn.on('click', addEnglishTask);

		rmEnglishTaskBtn.off('click');
		rmEnglishTaskBtn.on('click', rmEnglishTask);

		listSelect.off('change');
		listSelect.on('change', onChangeListSelect);

		listAddBtn.off('click');
		listAddBtn.on('click', function (e) {
			return addQuestionRow(e, nmbQuestions, isBooleanAnswers);
		});

		listRmBtn.off('click');
		listRmBtn.on('click', rmQuestionRow);

		multiblocks.off('click');
		multiblocks.on('click', redactorMultiblock);

		addImageWidthRadio.off('change');
		addImageWidthRadio.on('change', function (e) {
			return handleChangeRadio(e);
		});
		addChangeWidthBtn.off('click');
		addChangeWidthBtn.on('click', function (e) {
			e.preventDefault();

			var imageWidth = $(e.currentTarget).prev().val();

			setWidthImage(imageWidth, $(e.currentTarget).prev().prev().find('input'));
		});

		setWidthListeners();
	});
};

var getEngQuestions = function getEngQuestions() {
	settings = getSettings();
	$('#tasks_selector_for_edit').html("");

	$(document).find('.bookshelf_wrapper').show();
	$(document).find('.hideBehind').show();
	$('#all').css("opacity", "0.5");

	settings.then(function (result) {
		$.ajax({
			url: '/get_eng_questions',
			method: 'post',
			data: result,
			success: function success(response) {
				if (response.length > 0) {
					var key = result['isListAnswer'] || result['isText'] ? 'text' : 'text_question';
					$('#questions_edit_block_elements [data-toggle="tooltip"] button').prop("disabled", false);

					var _iteratorNormalCompletion5 = true;
					var _didIteratorError5 = false;
					var _iteratorError5 = undefined;

					try {
						for (var _iterator5 = response[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
							var question = _step5.value;


							var replacedText = removeHTML(question[key]);
							if (replacedText == "") {
								$('#tasks_selector_for_edit').append('\n\t\t\t\t\t\t\t\t<option value="' + question['id'] + '" \n\t\t\t\t\t\t\t\t\tname = "image" \n\t\t\t\t\t\t\t\t\tdata-width="' + question['image_width'] + '"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t\u0417\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F\n\t\t\t\t\t\t\t\t</option>\n\t\t\t\t\t\t\t');
							} else {
								$('#tasks_selector_for_edit').append('\n\t\t\t\t\t\t\t\t<option value="' + question['id'] + '"\n\t                            \tname = "normal" \n\t                            \tdata-width="' + question['image_width'] + '"\n\t                            >\n\t\t\t\t\t\t\t\t\t' + replacedText + '\n\t\t\t\t\t\t\t\t</option>\n\t\t\t\t\t\t\t');
							}
						}
					} catch (err) {
						_didIteratorError5 = true;
						_iteratorError5 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion5 && _iterator5.return) {
								_iterator5.return();
							}
						} finally {
							if (_didIteratorError5) {
								throw _iteratorError5;
							}
						}
					}

					$('#questions_edit_block_elements').show();
				} else {
					$('#questions_edit_block_elements').hide();

					createSwal({
						title: 'Питання до даного блоку відсутні',
						type: 'info'
					});
					editTaskSelect.val('');
				}

				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");
			},
			error: function error(err) {
				console.log(err);
			}
		});
	});
};

var deleteEngQuestion = function deleteEngQuestion() {
	settings.then(function (result) {
		var isConfirm = confirm('Ви дійсно хочете видати дане завдання?');

		if (isConfirm) {
			$(document).find('.bookshelf_wrapper').show();
			$(document).find('.hideBehind').show();
			$('#all').css("opacity", "0.5");

			var data = {
				id: $('#tasks_selector_for_edit option:selected').val(),
				settings: result
			};

			$.ajax({
				url: '/delete_eng_question',
				method: 'post',
				data: data,
				success: function success() {
					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					$('#form_for_change_task').hide();
					$('#tasks_selector_for_edit option:selected').remove();
					if ($('#tasks_selector_for_edit option').length < 1) {
						$('#questions_edit_block_elements').hide();
					}

					createSwal({
						title: 'Завдання успішно видалено',
						type: 'success'
					});
				},
				error: function error(err) {
					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					createSwal({
						title: 'Помилка при видаленні завдання',
						type: 'error'
					});
					console.log(err);
				}
			});
		}
	});
};

var getEngQuestion = function getEngQuestion() {
	settings.then(function (result) {
		var data = {
			id: $('#tasks_selector_for_edit option:selected').val(),
			settings: result
		};
		$('#panel3 .addNewAnswer').hide();
		$('#panel3 .task-container').show();

		renderEnglishTask().then(function () {
			$.ajax({
				url: '/get_eng_question',
				method: 'post',
				data: data,
				success: function success(response) {
					var title = $('#tasks_selector_for_edit option:selected'),
					    textBlock = $('#panel3 .text-block'),
					    listAnswers = $('#panel3 .list-answers-block'),
					    listBlock = $('#panel3 .list-block'),
					    questionBlock = $('#panel3 .questions-block'),
					    categoryBlock = $(questionBlock).find('.category-block'),
					    settings = data['settings'],
					    text = response['text'],
					    question = response['question'],
					    answers = response['answers'],
					    imageWidth = $(title).attr('data-width');

					if (!data['settings']['isListAnswer'] || data['settings']['isText']) {
						if (imageWidth === 'auto') {
							textBlock.find('.enableAutoWidth').click();
						} else {
							if (imageWidthChoices.includes(imageWidth)) {
								$('#panel3 input[type="radio"][value="' + imageWidth + '"]:first').click();
							} else {
								$('#panel3 input[type="radio"][value="custom"]:first').click();
								$('#panel3 .customWidthInput:first').prop('disabled', false);
								$('#panel3 .customWidthInput:first').val(imageWidth);
								$('#panel3 .changeWidthBtn:first').prop('disabled', false);
							}
						}
					}

					if (settings['isListAnswer']) {
						if (settings['isText']) {
							$(textBlock).find('.category').remove();
							$(textBlock).find('.imageWidthContainer').after(questionMultiblock);
							$(textBlock).find('.text-question').on('click', redactorMultiblock);

							textBlock.find('.text-question').val(text);

							for (var i = 0; i < answers.length; i++) {
								$(listBlock.find('input[type="text"]')[i]).val(answers[i]['text_question']);
								$(listBlock.find('input[type="text"]')[i]).attr('data-id', answers[i]['id']);
							}

							for (var _i = 0; _i < answers.length; _i++) {
								var curSelect = $(listBlock.find('select')[_i]);
								curSelect.attr('data-previous-val', answers[_i]['position']);

								curSelect.find('option[value="' + answers[_i]['position'] + '"]').attr('selected', true);
							}
						} else {
							var inputs = $(listAnswers).find('input');

							for (var _i2 = 0; _i2 < question.length; _i2++) {
								$(inputs[_i2]).val(question[_i2]['text_question']);
							}

							inputs = $(listBlock).find('input');

							if (inputs.length > 1) {
								$('#panel3 .rmQuestionRow').removeAttr('disabled');
							}

							for (var _i3 = 0; _i3 < answers.length; _i3++) {
								$(inputs[_i3]).val(answers[_i3]['text_question']);
								$(inputs[_i3]).attr('data-id', answers[_i3]['id']);
								$(inputs[_i3]).closest('tr').find('td:first').text(_i3 + 1);
								$(listBlock.find('select')[_i3]).find('option[value="' + answers[_i3]['right_answer'] + '"]').attr('selected', true);
							}
						}
					} else if (settings['isText']) {
						$(textBlock).find('.category').remove();
						$(textBlock).find('.imageWidthContainer').after(questionMultiblock);
						$(textBlock).find('.text-question').on('click', redactorMultiblock);

						textBlock.find('.text-question').val(text);

						for (var _i4 = 0; _i4 < question.length; _i4++) {
							if (!_i4) {
								$(categoryBlock[_i4]).find('.category').remove();
								$(categoryBlock[_i4]).find('.imageWidthContainer').after(questionMultiblock);
								$(categoryBlock[_i4]).find('.text-question').on('click', redactorMultiblock);

								$(categoryBlock[_i4]).find('.text-question').val(question[_i4]['text_question']);
								$(categoryBlock[_i4]).find('.text-question').attr('data-id', question[_i4]['id']);
							} else {
								$(categoryBlock[_i4]).find('.question-name').val(question[_i4]['text_question']);
								$(categoryBlock[_i4]).find('.question-name').attr('data-id', question[_i4]['id']);
							}

							imageWidth = question[_i4]['image_width'];

							if (imageWidth !== 'auto') {
								if (imageWidthChoices.includes(imageWidth)) {
									$(categoryBlock[_i4]).find('input[type="radio"][value="' + imageWidth + '"]').prop('checked', true);
								} else {
									$(categoryBlock[_i4]).find('input[type="radio"][value="custom"]').prop('checked', true);
									$(categoryBlock[_i4]).find('.customWidthInput').prop('disabled', false);
									$(categoryBlock[_i4]).find('.customWidthInput').val(imageWidth);
									$(categoryBlock[_i4]).find('.changeWidthBtn').prop('disabled', false);
								}
							} else {
								$(categoryBlock[_i4]).find('.enableAutoWidth').click();
							}

							for (var j = 0; j < answers[_i4].length; j++) {
								$($(categoryBlock[_i4]).find('.item-name')[j]).val(answers[_i4][j]['text_answer']);
								$($(categoryBlock[_i4]).find('.item-name')[j]).attr('data-id', answers[_i4][j]['id']);
								$($(categoryBlock[_i4]).find('input[type="checkbox"]')[j]).attr('checked', answers[_i4][j]['answer_right'] != 0);
							}
						}
					} else {
						$(questionBlock).find('.category').remove();
						$(questionBlock).find('.imageWidthContainer').after(questionMultiblock);
						$(questionBlock).find('.text-question').on('click', redactorMultiblock);

						if (imageWidth !== 'auto') {
							if (imageWidthChoices.includes(imageWidth)) {
								$(questionBlock).find('input[type="radio"][value="' + imageWidth + '"]').click();
							} else {
								$(questionBlock).find('input[type="radio"][value="custom"]').click();
								$(questionBlock).find('.customWidthInput').prop('disabled', false);
								$(questionBlock).find('.customWidthInput').val(imageWidth);
								$(questionBlock).find('.changeWidthBtn').prop('disabled', false);
							}
						} else {
							$(questionBlock).find('.enableAutoWidth').click();
						}

						$(questionBlock).find('.text-question').val(question);

						for (var _i5 = 0; _i5 < answers.length; _i5++) {
							$(questionBlock.find('.item-name')[_i5]).val(answers[_i5]['text_answer']);
							$(questionBlock.find('.item-name')[_i5]).attr('data-id', answers[_i5]['id']);
							$(questionBlock.find('input[type="checkbox"]')[_i5]).attr('checked', answers[_i5]['answer_right'] != 0);
						}
					}
				},
				error: function error(err) {
					createSwal({
						title: 'Помилка при спробі отримання данних',
						type: 'error'
					});
					console.log(err);
				}
			}).then(function () {
				$('#form_edit_task').show();
				$('#panel3 .multiblock').each(function () {
					if ($(this).is(':visible') || $(this).parent().hasClass('jHtmlArea') && $(this).parent().is(':visible')) {
						$(this).click();
					}
				});
			});
		});
	});
};

var updateEditEng = function updateEditEng() {
	settings = getSettings();

	var valid = validateEnglish();

	if (valid) {
		settings.then(function (result) {
			$(document).find('.bookshelf_wrapper').show();
			$(document).find('.hideBehind').show();
			$('#all').css("opacity", "0.5");

			var data = void 0,
			    title = $('#tasks_selector_for_edit option:selected'),
			    textBlock = $('#panel3 .text-block'),
			    listAnswers = $('#panel3 .list-answers-block'),
			    listBlock = $('#panel3 .list-block'),
			    questionBlock = $('#panel3 .questions-block'),
			    imageWidth = void 0,
			    block = textBlock.is(':visible') ? textBlock : questionBlock;

			if (block.find('input[type="radio"]:checked').val() !== 'custom') {
				imageWidth = block.find('input[type="radio"]:checked').val();
			} else {
				imageWidth = block.find('.customWidthInput').val();
			}

			if (block.find('.enableAutoWidth').hasClass('btn-success')) {
				imageWidth = 'auto';
			}

			if (result['isListAnswer']) {
				if (result['isText']) {
					var _getImageSrc3 = getImageSrc($(textBlock).find('.text-question').val()),
					    _getImageSrc4 = _slicedToArray(_getImageSrc3, 2),
					    content = _getImageSrc4[0],
					    images = _getImageSrc4[1];

					var editQuestion = {
						id: $(title).val(),
						text: { content: content, images: images },
						image_width: imageWidth
					},
					    editAnswers = [];

					for (var i = 0; i < $(listBlock).find('input[type="text"]').length; i++) {
						editAnswers[i] = {
							id: $($(listBlock).find('input[type="text"]')[i]).attr('data-id'),
							text_question: $($(listBlock).find('input[type="text"]')[i]).val(),
							right_answer: $($(listBlock).find('.indexCol')[i]).text(),
							position: $($(listBlock).find('select')[i]).find('option:selected').val()
						};
					}

					data = {
						settings: result,
						question: editQuestion,
						answers: editAnswers
					};
				} else {
					var _editQuestion = [],
					    _editAnswers = [],
					    inputs = $(listAnswers).find('input'),
					    ids = $(title).val().split(' ');

					for (var _i6 = 0; _i6 < inputs.length; _i6++) {
						_editQuestion[_i6] = {
							id: ids[_i6],
							text_question: $(inputs[_i6]).val()
						};
					}

					inputs = $(listBlock).find('input');

					for (var _i7 = 0; _i7 < inputs.length; _i7++) {
						_editAnswers[_i7] = {
							id: typeof $(inputs[_i7]).attr('data-id') !== 'undefined' ? $(inputs[_i7]).attr('data-id') : null,
							text_question: $(inputs[_i7]).val(),
							right_answer: $($(listBlock).find('select')[_i7]).find('option:selected').val()
						};
					}

					data = {
						settings: result,
						question: _editQuestion,
						answers: _editAnswers
					};
				}
			} else if (result['isText']) {
				var _getImageSrc5 = getImageSrc($(textBlock).find('.text-question').val()),
				    _getImageSrc6 = _slicedToArray(_getImageSrc5, 2),
				    _content = _getImageSrc6[0],
				    _images = _getImageSrc6[1],
				    editTitle = {
					id: $(title).val(),
					text: { content: _content, images: _images },
					image_width: imageWidth
				},
				    _editQuestion2 = [],
				    _editAnswers2 = [];

				for (var _i8 = 0; _i8 < $(questionBlock).find('.category-block').length; _i8++) {
					var _content2 = void 0,
					    _images2 = null,
					    categoryBlock = $(questionBlock).find('.category-block')[_i8],
					    temp = [];

					if ($(categoryBlock).find('input[type="radio"]:checked').val() !== 'custom') {
						imageWidth = $(categoryBlock).find('input[type="radio"]:checked').val();
					} else {
						imageWidth = $(categoryBlock).find('.customWidthInput').val();
					}

					if ($(categoryBlock).find('.enableAutoWidth').hasClass('btn-success')) {
						imageWidth = 'auto';
					}

					if (!_i8) {
						var _getImageSrc7 = getImageSrc($(categoryBlock).find('.text-question').val()),
						    _getImageSrc8 = _slicedToArray(_getImageSrc7, 2),
						    _content3 = _getImageSrc8[0],
						    _images3 = _getImageSrc8[1];

						_editQuestion2[_i8] = {
							id: $(categoryBlock).find('.text-question').attr('data-id'),
							text_question: { content: _content3, images: _images3 },
							image_width: imageWidth
						};
					} else {
						var _getImageSrc9 = getImageSrc($(categoryBlock).find('.question-name').val());

						var _getImageSrc10 = _slicedToArray(_getImageSrc9, 2);

						_content2 = _getImageSrc10[0];
						_images2 = _getImageSrc10[1];


						_editQuestion2[_i8] = {
							id: $(categoryBlock).find('.question-name').attr('data-id'),
							text_question: { content: _content2, images: _images2 },
							image_width: imageWidth
						};
					}

					for (var j = 0; j < $(categoryBlock).find('.item-name').length; j++) {
						var _getImageSrc11 = getImageSrc($($(categoryBlock).find('.item-name')[j]).val());

						var _getImageSrc12 = _slicedToArray(_getImageSrc11, 2);

						_content2 = _getImageSrc12[0];
						_images2 = _getImageSrc12[1];


						temp[j] = {
							id: $($(categoryBlock).find('.item-name')[j]).attr('data-id'),
							text_answer: { content: _content2, images: _images2 },
							answer_right: $($(categoryBlock).find('input[type="checkbox"]')[j]).is(':checked')
						};
					}

					_editAnswers2[_i8] = temp;
				}

				data = {
					settings: result,
					title: editTitle,
					question: _editQuestion2,
					answers: _editAnswers2
				};
			} else {
				var _getImageSrc13 = getImageSrc($(questionBlock).find('.text-question').val()),
				    _getImageSrc14 = _slicedToArray(_getImageSrc13, 2),
				    _content4 = _getImageSrc14[0],
				    _images4 = _getImageSrc14[1],
				    _editQuestion3 = {
					id: $(title).val(),
					text_question: { content: _content4, images: _images4 },
					image_width: imageWidth
				},
				    _editAnswers3 = [];

				for (var _i9 = 0; _i9 < $(questionBlock).find('.item-name').length; _i9++) {
					var _getImageSrc15 = getImageSrc($($(questionBlock).find('.item-name')[_i9]).val()),
					    _getImageSrc16 = _slicedToArray(_getImageSrc15, 2),
					    _content5 = _getImageSrc16[0],
					    _images5 = _getImageSrc16[1];

					_editAnswers3[_i9] = {
						id: $($(questionBlock).find('.item-name')[_i9]).attr('data-id'),
						text_answer: { content: _content5, images: _images5 },
						answer_right: $($(questionBlock).find('input[type="checkbox"]')[_i9]).is(':checked')
					};
				}
				data = {
					settings: result,
					question: _editQuestion3,
					answers: _editAnswers3
				};
			}
			$.ajax({
				url: '/update_edit_eng',
				method: 'post',
				data: data,
				success: function success() {
					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					$('#form_edit_task button:submit').prop('disabled', false);

					getEngQuestions();
					resetEnglishTasks();

					createSwal({
						title: 'Завдання успішно змінено',
						type: 'success'
					});
				},
				error: function error(err) {
					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					$('#form_edit_task button:submit').prop('disabled', false);

					createSwal({
						title: 'Помилка при збереженні змін завдання',
						type: 'error'
					});
					console.log(err);
				}
			});
		});
	} else {
		$('#form_edit_task button:submit').prop('disabled', false);
	}
};

function removeHTML(str) {
	if (str) {
		while (str.indexOf('<') != -1 && str.indexOf('>') != -1 && str.indexOf('>') > str.indexOf('<')) {
			str = str.slice(0, str.indexOf('<')) + str.slice(str.indexOf('>') + 1, str.length);
		}
	}
	return str;
}

module.exports = {
	renderEnglishTask: renderEnglishTask,
	saveEnglishTask: saveEnglishTask,
	getEngQuestions: getEngQuestions,
	deleteEngQuestion: deleteEngQuestion,
	getEngQuestion: getEngQuestion,
	updateEditEng: updateEditEng,
	onChangeListSelect: onChangeListSelect
};

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var createSwal = function createSwal(params, callback) {
	var time = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 3000;

	swal(_extends({}, params, {
		closeOnClickOutside: true,
		allowOutsideClick: true
	}), callback);

	setTimeout(function () {
		return swal.close;
	}, time);
};

module.exports = {
	createSwal: createSwal
};

/***/ }),

/***/ 25:
/***/ (function(module, exports) {

var newTaskContainer = '<div class="task-container"></div>';

var booleanAnswer = '\n\t<div class="row booleanAnswer">\n\t\t<label>\n\t\t\t\u041F\u0440\u0430\u0432\u0438\u043B\u044C\u043D\u0430 \u0432\u0456\u0434\u043F\u043E\u0432\u0456\u0434\u044C\n\t\t\t<input type="checkbox">\n\t\t</label>\t\t\t\n\t</div>\n';

var listColContentChoices = {
	'bool': 'Відповідь вірна?',
	'list': 'Відповідний номер в таблиці',
	'default': 'Відповідний номер в тексті'
};

var fillOptions = function fillOptions(isBoolAnswers, nmbQuestions, indexRow, typeSymbolSelect, extraAnswers) {
	var result = [];

	if (isBoolAnswers) {
		var basicChoice = ['<option value="1">\u0422\u0430\u043A</option>', '<option value="0">\u041D\u0456</option>'],
		    currRow = indexRow + 1;
		result.push(basicChoice);

		var optionContent = void 0,
		    optionValue = void 0;

		for (var index = 1; index <= extraAnswers; index++) {
			optionContent = '\u0414\u043E\u0434\u0430\u0442\u043A\u043E\u0432\u0438\u0435 \u043F\u0438\u0442\u0430\u043D\u043D\u044F - ' + index;
			optionValue = 'extra-answer - ' + index;

			result.push('\n\t\t\t\t<option \n\t\t\t\t\tvalue="' + optionValue + '"\n\t\t\t\t\t' + (currRow - nmbQuestions === index ? 'selected' : '') + '\n\t\t\t\t\t>\n\t\t\t\t\t\t' + optionContent + '\n\t\t\t\t\t</option>');
		}
	} else {
		for (var _index = 1; _index <= nmbQuestions + extraAnswers; _index++) {
			var firstLetterAskii = 64,
			    _optionContent = void 0,
			    _optionValue = void 0,
			    currIndex = void 0;
			if (_index > nmbQuestions) {
				currIndex = _index - nmbQuestions;

				_optionContent = '\u0414\u043E\u0434\u0430\u0442\u043A\u043E\u0432\u0438\u0435 \u043F\u0438\u0442\u0430\u043D\u043D\u044F - ' + currIndex;
				_optionValue = 'extra-answer - ' + currIndex;
			} else {
				_optionContent = typeSymbolSelect === 'int' ? _index : String.fromCharCode(firstLetterAskii + _index);
			}

			result.push('\n\t\t\t<option \n\t\t\t\tvalue="' + (_optionValue ? _optionValue : _optionContent) + '"\n\t\t\t\t' + (indexRow + 1 === _index ? 'selected' : '') + '\n\t\t\t>\n\t\t\t\t' + _optionContent + '\n\t\t\t</option>\n\t\t');
		}
	}

	return result;
};

var insertContentCol = function insertContentCol(isBoolAnswers, nmbQuestions, indexRow, typeSymbolSelect, extraAnswers) {
	var firstLetterAskii = 65,
	    currIndex = indexRow + 1;
	var prevVal = void 0,
	    extraIndex = void 0;

	if (isBoolAnswers) {
		var indexExtraAnswers = currIndex - nmbQuestions;
		prevVal = indexExtraAnswers > 0 ? 'extra-answer - ' + indexExtraAnswers : '1';
	} else {
		if (indexRow >= nmbQuestions) {
			extraIndex = currIndex - nmbQuestions;
			prevVal = 'extra-answer - ' + extraIndex;
		} else {
			prevVal = typeSymbolSelect === 'int' ? currIndex : String.fromCharCode(firstLetterAskii + indexRow);
		}
	}

	return '\n\t\t<select data-previous-val="' + prevVal + '">\n\t\t\t' + fillOptions(isBoolAnswers, nmbQuestions, indexRow, typeSymbolSelect, extraAnswers) + '\n\t\t</select>\n\t';
};

var newRowAnswerList = function newRowAnswerList(indexRow) {
	var firstLetterAskii = 65,
	    symbolIndex = String.fromCharCode(firstLetterAskii + indexRow);
	return '<tr>\n\t\t\t<td>' + symbolIndex + '</td>\n\t\t\t<td class="textCol"><input type="text" maxlength="100" required></td>\n\t\t</tr>';
};

var newRowList = function newRowList(nmbQuestions, indexRow, isBooleanAnswers, extraAnswers) {
	var typeSymbolSelect = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 'int';

	var firstLetterAskii = 65,
	    startIndex = 1,
	    symbolIndex = isBooleanAnswers || typeSymbolSelect === 'askii' ? startIndex + indexRow : String.fromCharCode(firstLetterAskii + indexRow);

	return '<tr>\n\t\t\t<td class="indexCol">' + symbolIndex + '</td>\n\t\t\t<td class="textCol"><input type="text" maxlength="100" required></td>\n\t\t\t<td class="resultCol">\n\t\t\t\t' + insertContentCol(isBooleanAnswers, nmbQuestions, indexRow, typeSymbolSelect, extraAnswers) + '\n\t\t\t</td>\n\t\t</tr>';
};

var newManageRowBlock = function newManageRowBlock() {
	return '<tr class="adding-row">\n\t\t\t<td colspan="2">\n\t\t\t\t<buttton class="btn btn-default addQuestionRow">\n\t\t\t\t\t<i class="fas fa-plus-circle"></i>\n\t\t\t\t\t\u0414\u043E\u0434\u0430\u0442\u0438 \u0449\u0435 \u043E\u0434\u0438\u043D \u0432\u0430\u0440\u0456\u0430\u043D\u0442\t\t\n\t\t\t\t</buttton>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<buttton class="btn btn-danger rmQuestionRow" disabled>\n\t\t\t\t\t<i class="fas fa-times-circle"></i>\n\t\t\t\t\t\u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u043E\u0441\u0442\u0430\u043D\u043D\u0456\u0439 \u0432\u0430\u0440\u0456\u0430\u043D\u0442\t\t\n\t\t\t\t</buttton>\n\t\t\t</td>\n\t\t</tr>';
};

var newTextBlock = function newTextBlock() {
	var taskIndex = $('#panel2 .task-container').length + 1;
	return '<div class="text-block">\n\t\t\t<h2>\u0422\u0435\u043A\u0441\u0442 \u0434\u043E \u043F\u0438\u0442\u0430\u043D\u043D\u044F:</h2>\n\t\t\t<div class="imageWidthContainer im_regculation" id="0">\n\t\t\t\t<div class="row col-xs-10">\n\t\t\t\t\t<span>\u0412\u043A\u0430\u0436\u0456\u0442\u044C \u0448\u0438\u0440\u0438\u043D\u0443 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u044C:</span>\n\t\t\t\t\t<label><input type="radio" name="image-width-radio-text-' + taskIndex + '" value="50">50px</label>\n\t\t\t\t\t<label><input type="radio" name="image-width-radio-text-' + taskIndex + '" value="75">75px</label>\n\t\t\t\t\t<label><input type="radio" name="image-width-radio-text-' + taskIndex + '" value="100" checked>100px</label>\n\t\t\t\t\t<label><input type="radio" name="image-width-radio-text-' + taskIndex + '" value="125">125px</label>\n\t\t\t\t\t<label><input type="radio" name="image-width-radio-text-' + taskIndex + '" value="150">150px</label>\n\t\t\t\t\t<label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-text-' + taskIndex + '" aria-label="image-width"></label>\n\t\t\t\t\t<input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>\n\t\t\t\t\t<button class = "changeWidthBtn btn btn-info" disabled>\u0417\u043C\u0456\u043D\u0438\u0442\u0438</button>\n\t\t\t\t\t<button class="enableAutoWidth btn btn-danger">Auto/off</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class="row category" style="margin-bottom: 5px">\n\t\t\t\t<div class="col-xs-12 form-inline" style="width: 100%">\n\t\t\t\t\t<div class="popupImageConvertor">\n\t\t\t\t\t\t<div class="popup-container">\n\t\t\t\t\t\t\t<h3 class="resize-title">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n\t\t\t\t\t\t\t<div class="resizer" >\n\t\t\t\t\t\t\t\t<span class="addition">ctrl+v</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<button class="btn btn-info resizer-result">\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n\t\t\t\t\t\t\t<button class="closePopup">&#10006;</button>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<textarea class="multiblock text-question"></textarea>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>';
};

var newListAnswerBlock = '\n\t<div class="list-answers-block">\n\t\t<table>\n\t\t\t<thead>\n\t\t\t\t<tr>\n\t\t\t\t\t<th>\u2116</th>\n\t\t\t\t\t<th>\u0412\u0430\u0440\u0456\u0430\u043D\u0442</th>\n\t\t\t\t</tr>\n\t\t\t</thead>\n\t\t\t<tbody></tbody>\n\t\t</table>\n\t</div>';

var newListBlock = '\n\t<div class="list-block">\n\t\t<table>\n\t\t\t<thead>\n\t\t\t<tr>\n\t\t\t\t<th>\u2116</th>\n\t\t\t\t<th>\u0422\u0435\u043A\u0441\u0442 \u043F\u0438\u0442\u0430\u043D\u043D\u044F</th>\n\t\t\t\t<th class="change-content-col"></th>\n\t\t\t</tr>\n\t\t\t</thead>\n\t\t\t<tbody></tbody>\n\t\t</table>\n\t</div>';

var questionContainer = '<div class="questions-block"></div>';

var newTestBlock = function newTestBlock(activeTab) {
	var taskContainer = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

	/*
 	find last task container and count nmb questions, we can't add question on existing task
 	if it's adding new task we couldn't find new container
 */
	var indexQuestion = void 0;

	if (activeTab === 'add') {
		var currTaskContainer = taskContainer ? taskContainer : $('#panel2 .task-container:last'),
		    nmbQuestions = currTaskContainer.find('.category-block').length;

		indexQuestion = taskContainer ? $('#panel2 .category-block').length + nmbQuestions + 1 : nmbQuestions + 1;
	} else {
		var _currTaskContainer = $('#panel3 .task-container'),
		    _nmbQuestions = _currTaskContainer.find('.questions-block .category-block').length;

		indexQuestion = _nmbQuestions + 1;
	}

	return '<div class="category-block">\n\t\t\t\t<div class="imageWidthContainer im_regculation">\n\t\t\t\t\t<div class="row col-xs-10">\n\t\t\t\t\t\t<span>\u0412\u043A\u0430\u0436\u0456\u0442\u044C \u0448\u0438\u0440\u0438\u043D\u0443 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u044C:</span>\n\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + indexQuestion + '" value="50">50px</label>\n\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + indexQuestion + '" value="75">75px</label>\n\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + indexQuestion + '" value="100" checked>100px</label>\n\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + indexQuestion + '" value="125">125px</label>\n\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-' + indexQuestion + '" value="150">150px</label>\n\t\t\t\t\t\t<label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-' + indexQuestion + '" aria-label="image-with"></label>\n\t\t\t\t\t\t<input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>\n\t\t\t\t\t\t<button class = "changeWidthBtn btn btn-info" disabled>\u0417\u043C\u0456\u043D\u0438\u0442\u0438</button>\n\t\t\t\t\t\t<button class="enableAutoWidth btn btn-danger">Auto/off</button>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class="row category" style="margin-bottom: 5px">\n\t\t\t\t\t<div class="col-xs-12 form-inline" style="width: 100%">\n\t\t\t\t\t\t<div class="popupImageConvertor">\n\t\t\t\t\t\t\t<div class="popup-container">\n\t\t\t\t\t\t\t\t<h3 class="resize-title">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n\t\t\t\t\t\t\t\t<div class="resizer" >\n\t\t\t\t\t\t\t\t\t<span class="addition">ctrl+v</span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<button class="btn btn-info resizer-result">\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n\t\t\t\t\t\t\t\t<button class="closePopup">&#10006;</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<textarea class="multiblock question-name"></textarea>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\n\t\t\t\t<div class="items answers-container"></div>\n\t\t\t</div>\n\t\t';
};

var manageButtonsBlock = function manageButtonsBlock() {
	var isFirstRender = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

	return '\n\t\t<div class="manage-buttons-block">\n\t\t\t<button class="btn btn-info add-english-task">\u0414\u043E\u0434\u0430\u0442\u0438 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F</button>\n\t\t\t<button class="btn btn-danger rm-english-task" ' + (isFirstRender ? 'style="display: none;"' : '') + '>\u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u0437\u0430\u0432\u0434\u0430\u043D\u043D\u044F</button>\n\t\t</div>';
};

var questionMultiblock = function questionMultiblock() {
	return '<div class="row category" style="margin-bottom: 5px">\n\t\t\t\t\t<div class="col-xs-12 form-inline" style="width: 100%">\n\t\t\t\t\t\t<div class="popupImageConvertor">\n\t\t\t\t\t\t\t<div class="popup-container">\n\t\t\t\t\t\t\t\t<h3 class="resize-title">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n\t\t\t\t\t\t\t\t<div class="resizer" >\n\t\t\t\t\t\t\t\t\t<span class="addition">ctrl+v</span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<button class="btn btn-info resizer-result">\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n\t\t\t\t\t\t\t\t<button class="closePopup">&#10006;</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<textarea class="multiblock text-question"></textarea>\n\t\t\t\t\t</div>\n\t\t\t\t</div>';
};

var normalMultiblock = function normalMultiblock() {
	return '<div class="row category" style="margin-bottom: 5px">\n\t\t\t\t\t<div class="col-xs-12 form-inline" style="width: 100%">\n\t\t\t\t\t\t<div class="popupImageConvertor">\n\t\t\t\t\t\t\t<div class="popup-container">\n\t\t\t\t\t\t\t\t<h3 class="resize-title">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n\t\t\t\t\t\t\t\t<div class="resizer" >\n\t\t\t\t\t\t\t\t\t<span class="addition">ctrl+v</span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<button class="btn btn-info resizer-result">\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n\t\t\t\t\t\t\t\t<button class="closePopup">&#10006;</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<textarea class="multiblock text-question"></textarea>\n\t\t\t\t\t\t<div class="adding-btns">\n\t\t\t\t\t\t\t<button type="button" class="btn btn-default addNewAnswer">\n\t\t\t\t\t\t\t\t<span class="glyphicon glyphicon-plus"></span>\n\t\t\t\t\t\t\t\t\u0414\u043E\u0434\u0430\u0442\u0438 \u0432\u0430\u0440\u0456\u0430\u043D\u0442\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>';
};

module.exports = {
	listColContentChoices: listColContentChoices,
	booleanAnswer: booleanAnswer,
	newTaskContainer: newTaskContainer,
	newRowAnswerList: newRowAnswerList,
	newRowList: newRowList,
	newManageRowBlock: newManageRowBlock,
	newTextBlock: newTextBlock,
	newListAnswerBlock: newListAnswerBlock,
	newListBlock: newListBlock,
	questionContainer: questionContainer,
	newTestBlock: newTestBlock,
	manageButtonsBlock: manageButtonsBlock,
	questionMultiblock: questionMultiblock,
	normalMultiblock: normalMultiblock
};

/***/ }),

/***/ 27:
/***/ (function(module, exports, __webpack_require__) {

var _require = __webpack_require__(4),
    redactorMultiblock = _require.redactorMultiblock;

var _require2 = __webpack_require__(1),
    addListenersToImages = _require2.addListenersToImages;

var _require3 = __webpack_require__(11),
    addNewAnswer = _require3.addNewAnswer;

var _require4 = __webpack_require__(8),
    deleteAnswer = _require4.deleteAnswer;

var _require5 = __webpack_require__(3),
    createSwal = _require5.createSwal;

function renderFirstQuestion() {
	resetTasks();

	var questionContainer = $('#categories'),
	    selectBlock = $('#block_selector'),
	    addingButtons = questionContainer.find('.adding-btns'),
	    idBlock = $(selectBlock).val();

	$.ajax({
		url: '/get-nmb-answers',
		method: 'post',
		data: { idBlock: idBlock },
		success: function success(response) {
			var countQuestions = response['countQuestions'],
			    weightQuestion = response['weightQuestion'],
			    maxNmbQuestion = response['allowedNmbNewQuestion'],
			    nmbAnswers = response['nmbAnswers'];

			selectBlock.attr('data-nmb_questions', countQuestions);
			selectBlock.attr('data-weight-question', weightQuestion);
			selectBlock.attr('data-max-question', maxNmbQuestion);

			renderFirstAnswers(nmbAnswers);
			questionContainer.show();
			addingButtons.show();

			addListenersToImages();

			if ($('.question-name').closest('.category-block').is(':visible')) {
				var questionBlock = $('#panel2 .questions-block:first .html');
				questionBlock.click();
				$('#panel2 .questions-block:first .question-name').val('');
				questionBlock.click();
			}
		},
		error: function error(err) {
			createSwal({
				title: 'Помилка при спробі створенні нового завдання',
				type: 'error'
			});
			console.log(err);
		}
	});
}

function renderFirstAnswers(nmbAnswers) {
	var answersContainer = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

	var nmbAnswersInput = $('#hidden_countAnswers');
	nmbAnswersInput.val(nmbAnswers);

	answersContainer = answersContainer ? answersContainer : $('#first_answer_item');

	answersContainer.html('');
	for (var countAnswer = 0; countAnswer < nmbAnswers; countAnswer++) {
		answersContainer.append(addNewAnswer(null, false, false, false, true, true));
		answersContainer.find('.deleteAnswer').off('click');
		answersContainer.find('.deleteAnswer').on('click', deleteAnswer);
	}

	var multiblocks = $(answersContainer).parent().find('.multiblock');

	multiblocks.off('click');
	multiblocks.on('click', redactorMultiblock);
}

function resetTasks() {
	$('#panel2 .category-block').show();
	$('.item-category').first().val('');
	$('.category-blocks:not(:first)').remove();

	// clean eng tasks
	$('#panel2 .list-block').hide();
	$('#panel2 .text-block').hide();
	$('#panel2 .manage-buttons-block').hide();
}

module.exports = {
	renderFirstQuestion: renderFirstQuestion,
	renderFirstAnswers: renderFirstAnswers
};

/***/ }),

/***/ 28:
/***/ (function(module, exports) {

module.exports = resetEnglishTasks = function resetEnglishTasks() {
	$('.enableAutoWidth.btn-success').click();

	if ($('#block_selector_for_edit').is(':visible')) {
		var categoryBlock = $('#panel3 .category-block:not(:first)');

		categoryBlock.remove();
		$('#form_edit_task').hide();

		$('#panel3 .list-answers-block').hide();
		$('#panel3 .list-block').hide();
		$('#panel3 button[type="submit"]').hide();
		$('#panel3 .text-block').hide();
		$('#panel3 .questions-block').hide();
		$('#panel3 .customWidthInput').attr('disabled', true);
		$('#panel3 .customWidthInput').val('');
		$('#panel3 .changeWidthBtn').attr('disabled', true);
	} else {
		var tasksToRemove = $('#panel2 .task-container:not(:first)'),
		    textBlock = $('#panel2 .text-block'),
		    listBlock = $('#panel2 .list-block'),
		    rowsListToDelete = listBlock.find('table tbody tr'),
		    listAnswerBlock = $('#panel2 .list-answers-block'),
		    rowsListAnswerToDelete = listAnswerBlock.find('table tbody tr'),
		    testBlock = $('#panel2 .category-block'),
		    testQuestion = testBlock.find('.category .question-name'),
		    answersContainer = testBlock.find('.items'),
		    manageButtonsBlock = $('#panel2 .manage-buttons-block'),
		    deleteTaskBtn = $('#panel2 .task-container .rm-english-task'),
		    questionToRemove = $('#panel2 .questions-block .category-block:not(:first)');

		$('#panel2 .customWidthInput').prop('disabled', true);
		$('#panel2 .customWidthInput').val('');
		$('#panel2 .changeWidthBtn').prop('disabled', true);

		questionToRemove.remove();
		tasksToRemove.remove();
		textBlock.hide();

		listAnswerBlock.hide();
		listBlock.hide();
		rowsListToDelete.remove();
		rowsListAnswerToDelete.remove();

		answersContainer.empty();

		testQuestion.val('');
		testBlock.hide();

		manageButtonsBlock.hide();
		deleteTaskBtn.hide();
	}
};

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function createSwal(params) {
	swal(_extends({}, params, {
		closeOnClickOutside: true,
		allowOutsideClick: true
	}));
}

module.exports = {
	createSwal: createSwal
};

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function createSwal(params) {
	swal(_extends({}, params, {
		closeOnClickOutside: true,
		allowOutsideClick: true
	}));
}

function redactorMultiblock() {

	var _this = this;

	$(this).htmlarea({
		// Override/Specify the Toolbar buttons to show
		toolbar: [["html", "bold", "italic", "underline", "strikethrough"], ["justifyleft", "justifycenter", "justifyright"], [{
			css: "justifyFull",
			text: "по ширині",
			action: function action(btn) {
				this.justifyFull();
			}

		}], [{
			css: "screen",
			text: "Додати зображення",
			action: function action(btn) {
				var _this2 = this;

				var popupImageConvertor = $(this)[0].container.prev();
				popupImageConvertor.css('display', 'flex');

				var el = popupImageConvertor[0].querySelector('.resizer');
				var getRes = popupImageConvertor[0].querySelector('.resizer-result');
				var close = popupImageConvertor.find('.closePopup');
				close.off('click');
				close.on('click', function (e) {
					e.preventDefault();
					el.innerHtml = '';
					popupImageConvertor.css('display', 'none');
				});

				if (el) {
					el.addEventListener('paste', function (e) {

						e.preventDefault();
						var clipboard = e.clipboardData;

						if (clipboard && clipboard.items) {

							var item = clipboard.items[0];

							if (item && item.type.indexOf('image/') > -1) {

								var blob = item.getAsFile();

								if (blob) {

									var reader = new FileReader();
									reader.readAsDataURL(blob);

									reader.onload = function (event) {
										var img = new Image();
										img.src = event.target.result;

										el.innerText = '';

										el.append(img);
										getRes.onclick = function (e) {

											var imageWidth = $(_this).closest('.category-block').find('input[type="radio"]:checked');

											imageWidth = typeof $(imageWidth).val() === 'undefined' ? $(_this).closest('.task-container').find('input[type="radio"]:checked') : imageWidth;

											imageWidth = typeof $(imageWidth).val() === 'undefined' ? $(_this).closest('.text-block').find('input[type="radio"]:checked') : imageWidth;

											imageWidth = typeof $(imageWidth).val() === 'undefined' ? $(_this).closest('.question_name').find('input[type="radio"]:checked') : imageWidth;

											if ($(imageWidth).val() === 'custom') {
												imageWidth = $(imageWidth).parent().next();
											}

											if (imageWidth.closest('.imageWidthContainer').find('.enableAutoWidth').hasClass('btn-success')) {
												imageWidth = 'auto';
											} else {
												imageWidth = $(imageWidth).val();
											}

											e.preventDefault();

											imageWidth = imageWidth ? imageWidth : 10;
											img.setAttribute("style", "width:" + imageWidth + "px");
											var iframe = popupImageConvertor.parent().find('.jHtmlArea iframe').contents().find('body');

											iframe.append(img);

											iframe.focus();

											el.innerHtml = '';

											popupImageConvertor[0].style.display = 'none';

											var parent = $(el).closest('.popup-container').parent();
											$(el).closest('.popup-container').remove();

											parent.prepend("<div class=\"popup-container\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h3 class=\"resize-title\">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"resizer\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"addition\">ctrl+v</span>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"resizer-result btn btn-info\">\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"closePopup\">\u2716</button>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>");
										};

										close.off('click');
										close.on('click', function (e) {
											var parent = $(el).closest('.popup-container').parent();
											$(el).closest('.popup-container').remove();

											parent.prepend("<div class=\"popup-container\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h3 class=\"resize-title\">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"resizer\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"addition\">ctrl+v</span>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"resizer-result btn btn-info\">\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"closePopup\">\u2716</button>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>");

											e.preventDefault();
											try {
												el.innerHtml = '';
											} catch (err) {
												popupImageConvertor = $(_this2)[0].container.prev();
											}
											el.innerHtml = '';
											popupImageConvertor.css('display', 'none');
										});
									};
								}
							}
						}
					});
					getRes.onclick = function (e) {
						e.preventDefault();
						createSwal({
							title: 'Вставте зображення',
							type: 'error'
						});
					};
				}
				$(this).focus();
			}
		}], ["subscript", "superscript"], ["orderedlist", "unorderedlist"]]
	});

	if ($('#edit-block .ToolBar')) {
		$('#edit-block .ToolBar .screen').closest('ul').css("display", "none");
	}
};

module.exports = {
	redactorMultiblock: redactorMultiblock
};

/***/ }),

/***/ 59:
/***/ (function(module, exports) {

module.exports = getSettings = function getSettings() {
	var nmbAnswers = 4;

	var id = $('#block_selector option:selected').val();

	if ($('#block_selector_for_edit').is(':visible')) {
		id = $('#block_selector_for_edit option:selected').val();
	}

	var data = {
		'block_id': id
	};

	return new Promise(function (resolve) {
		$.ajax({
			url: '/get_settings',
			method: 'post',
			data: data,
			success: function success(response) {
				var setting = response['settings'];

				if (setting.true_false == 0 && setting.text == 1) $('#answersCount').val(response.countAnswers);else $('#answersCount').val(0);
				var englishConfiguration = {
					isText: !!setting['text'],
					isListAnswer: !!setting['answer_list'],
					isBooleanAnswers: !!setting['true_false'],
					extraAnswers: setting['extra_answers'],
					nmbQuestions: response['countAnswers'],
					nmbAnswers: nmbAnswers,
					blockId: data['block_id']
				};

				resolve(englishConfiguration);
			},
			error: function error(err) {
				console.log(err);
			}
		});
	});
};

/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

var _require = __webpack_require__(1),
    setWidthImage = _require.setWidthImage;

module.exports = function () {
	var autoWidth = $('.enableAutoWidth');

	autoWidth.off('click');
	autoWidth.on('click', function (e) {
		e.preventDefault();

		var curBtn = $(e.currentTarget),
		    widthContainer = curBtn.closest('.imageWidthContainer');

		if (curBtn.hasClass('btn-danger')) {
			widthContainer.find('input').prop('disabled', true);
			widthContainer.find('.changeWidthBtn').prop('disabled', true);

			curBtn.removeClass('btn-danger');
			curBtn.addClass('btn-success');
			curBtn.text('Auto/on');

			setWidthImage('auto', widthContainer.find('input').first());
		} else {
			widthContainer.find('input[type="radio"]').prop('disabled', false);

			curBtn.addClass('btn-danger');
			curBtn.removeClass('btn-success');
			curBtn.text('Auto/off');

			widthContainer.find('input:radio[value="100"]').prop('checked', true);
			widthContainer.find('.customWidthInput').val('');

			setWidthImage(100, widthContainer.find('input').first());
		}
	});
};

/***/ }),

/***/ 63:
/***/ (function(module, exports, __webpack_require__) {

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var redactorMultiblock = __webpack_require__(4).redactorMultiblock;
var getImageSrc = __webpack_require__(7);
var createSwal = __webpack_require__(2).createSwal;

var _require = __webpack_require__(1),
    handleChangeRadio = _require.handleChangeRadio,
    setWidthImage = _require.setWidthImage;

var setWidthListeners = __webpack_require__(6);

window.onload = function () {

	$('#all').css('opacity', 1);
	var multiblocks = $('.multiblock');

	multiblocks.on('click', redactorMultiblock);

	var arrMultiblocks = [].concat(_toConsumableArray(multiblocks));
	$(document).find('.bookshelf_wrapper').show();
	arrMultiblocks.forEach(function (multiblock) {
		multiblock.click();
	});
	$(document).find('.bookshelf_wrapper').hide();

	var saveBtn = $('#statuses .btn-success');
	update();
	saveBtn.on('click', updateBlank);

	var addImageWidthRadio = $('.editBlockOmu .imageWidthContainer input[type="radio"]'),
	    addChangeWidthBtn = $('.editBlockOmu .changeWidthBtn');

	addImageWidthRadio.off();
	addImageWidthRadio.on('change', function (e) {
		return handleChangeRadio(e);
	});
	addChangeWidthBtn.off();
	addChangeWidthBtn.on('click', function (e) {
		e.preventDefault();
		var imageWidth = $(e.currentTarget).prev().val();
		setWidthImage(imageWidth, $(e.currentTarget).prev().prev().find('input'));
	});

	setWidthListeners();
};

function update() {
	var newComm = '<div class="category-block" style="display: block;">\n    <div class="row" style="margin-bottom: 5px">\n\t\t<div class="col-xs-12 form-inline">\n\t\t\t<div class = "hidden idsComm">0</div>\n\t\t\t<input class="item-category form-control commPosition" style="width: 45%" type="text" required placeholder="\u041F\u043E\u0441\u0430\u0434\u0430 \u0447\u043B\u0435\u043D\u0430 \u043A\u043E\u043C\u0456\u0441\u0456\u0457" >\n\t\t\t<input class="item-category form-control commName" style="width: 45%" type="text" required placeholder="\u041F\u0406\u0411 \u0447\u043B\u0435\u043D\u0430 \u043A\u043E\u043C\u0456\u0441\u0456\u0457" >\n\t\t<span  class = "category-add-comm" data-toggle="tooltip" title="\u0414\u043E\u0434\u0430\u0442\u0438 \u0447\u043B\u0435\u043D\u0430 \u043A\u043E\u043C\u0456\u0441\u0456\u0457">\n\t\t<button type="button"  class="btn btn-default">\n\t\t<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>\n\t\t</button>\n\t\t</span>\n\t\t<span class="category-delete-comm" data-toggle="tooltip" title="\u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u0447\u043B\u0435\u043D\u0430 \u043A\u043E\u043C\u0456\u0441\u0456\u0457" style="display: none;">\n\t\t<button type="button" class="btn btn-default">\n\t\t<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>\n\t\t\t</button>\n\t\t\t</span>\n\t\t</div> \n    </div>';
	var rmCommBtn = $('.category-delete-comm'),
	    addCommBtn = $('.category-add-comm'),
	    commContainer = $('#comm');
	updateListeners();

	var nmbComm = $(commContainer).find('.category-block').length;
	if (nmbComm > 1) {
		commContainer.find('.category-delete-comm').show();
	}

	function updateListeners() {
		rmCommBtn = $('.category-delete-comm');
		addCommBtn = $('.category-add-comm');

		addCommBtn.off('click');
		addCommBtn.on('click', addComm);

		rmCommBtn.off('click');
		rmCommBtn.on('click', rmComm);
	}

	function addComm() {

		var nmbComm = $(commContainer).find('.category-block').length;

		if (nmbComm < 5) {
			$('#comm').append(newComm);
			updateListeners();
			if (nmbComm > 0) {
				commContainer.find('.category-delete-comm').show();
			}
		} else {
			createSwal({
				title: 'Максимальна к-ть членів комісії',
				type: 'error'
			});
		}

		if (nmbComm == 1) {
			commContainer.first().find('.category-delete-comm').show();
		}
	}

	function rmComm() {
		var nmbComm = $(commContainer).find('.category-block').length,
		    comm = $(this).parent().parent().parent();

		if (nmbComm > 1) {
			comm.remove();
			updateListeners();
		}
		if (nmbComm == 2) {
			commContainer.first().find('.category-delete-comm').hide();
		}

		// show all adding buttons
		commContainer.find('.category-add-comm').toArray().map(function (el) {
			return $(el).show();
		});
	}
}

function validateQuestionsAndAnswers(element) {
	var valid = false,
	    currentValue = $(element).val().trim();

	if (currentValue.length > 1000000) {
		if ($(element).hasClass('item-name')) {
			$(element).parent().parent().after("<div class=\"validate-alert\">Поле не має перевищувати 1000000 символів</div>");
		} else {
			$(element).parent().after("<div class=\"validate-alert\">Поле не має перевищувати 1000000 символів</div>");
		}
		$(element).parent().removeClass("access-validate");
		$(element).parent().addClass("not-validate");
	} else if (currentValue.length < 1) {
		if ($(element).hasClass('item-name')) {
			$(element).parent().parent().after("<div class=\"validate-alert\">Поле не має бути пустим</div>");
		} else {
			$(element).parent().after("<div class=\"validate-alert\">Поле не має бути пустим</div>");
		}
		$(element).parent().removeClass("access-validate");
		$(element).parent().addClass("not-validate");
	} else {
		$(element).parent().removeClass("not-validate");
		$(element).parent().addClass("access-validate");
		valid = true;
	}
	return valid;
}

function cleanValidation(element) {
	$(element).parent().removeClass("access-validate");
	$(element).parent().removeClass("not-validate");
}

function updateBlank(e) {
	var submitBtn = $(e.currentTarget);
	$(document).find('.bookshelf_wrapper').show();
	$(document).find('.hideBehind').show();
	$('#all').css("opacity", "0.5");
	var typeSpeciality = $('#type_speciality').val();

	var valid = true;

	cleanValidation($('.multiblock'));

	$(".validate-alert").remove();

	$('.multiblock').each(function () {
		if (!validateQuestionsAndAnswers(this)) {
			valid = false;
		}
	});

	if (!valid) {
		$(document).find('.bookshelf_wrapper').hide();
		$(document).find('.hideBehind').hide();
		$('#all').css("opacity", "1");
		createSwal({
			title: 'Питання не пройшли валідацію',
			type: 'error'
		});
		return;
	}
	submitBtn.off('click');

	var blocksContent = $('.task-block'),
	    questionsContent = $('.question_name'),
	    answersContent = parseInt(typeSpeciality) === 0 ? $('.answer') : $('.answerContent');

	var tableNames = {
		blocks: 'blocks',
		questions: 'questions',
		answers: 'answers'
	};
	var imageNames = {
		questions: 'Question',
		answers: 'Answer'
	};

	var filteredBlocks = filterChangedElements([].concat(_toConsumableArray(blocksContent)), null, 'blocks');
	var filteredQuestions = filterChangedElements([].concat(_toConsumableArray(questionsContent)), null, 'questions');
	var filteredAnswers = filterChangedElements([].concat(_toConsumableArray(answersContent)), typeSpeciality);

	var elementsToSend = [{ elements: filteredBlocks, tableName: tableNames.blocks }, { elements: filteredQuestions, tableName: tableNames.questions, typeImage: imageNames.questions }, { elements: filteredAnswers, tableName: tableNames.answers, typeImage: imageNames.answers }];

	var commissionersContainers = [].concat(_toConsumableArray($('.category-block')));
	var commissioners = commissionersContainers.map(function (commissionerContainer) {
		var id = $(commissionerContainer).attr('data-id');
		var position = $(commissionerContainer).find('.commPosition').val();
		var initials = $(commissionerContainer).find('.commName').val();

		return { id: id, position: position, initials: initials };
	});

	var idSpeciality = $('#id_speciality').val();

	Promise.all([].concat(_toConsumableArray(elementsToSend.map(function (collectionElements) {
		return collectionElements.elements.length > 0 ? Promise.all(saveElements(collectionElements)) : [];
	})), [saveCommissioners(commissioners, idSpeciality)])).then(function () {
		$(document).find('.bookshelf_wrapper').hide();
		$(document).find('.hideBehind').hide();
		$('#all').css("opacity", "1");
		createSwal({
			title: 'Зміни збережено',
			type: 'success'
		}, function () {
			window.location.href = "/omu";
		});
	}).catch(function (err) {
		$(document).find('.bookshelf_wrapper').hide();
		$(document).find('.hideBehind').hide();
		$('#all').css("opacity", "1");
		createSwal({
			title: 'Виникла помилка при збереженні питань',
			type: 'error'
		});
		console.log(err);
	});
}

function filterChangedElements(collectionElements) {
	var typeSpeciality = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
	var collectionType = arguments[2];

	var filteredCollection = void 0;

	if (typeSpeciality) {
		filteredCollection = collectionElements.filter(function (element) {
			var multiblock = parseInt(typeSpeciality) === 0 ? $(element).find('.multiblock') : $(element),
			    checkbox = $(element).find('.correctAnswer'),
			    widthImageContainer = $(element).parent().parent().parent().parent().find('.imageWidthContainer'),
			    prevImageWidth = $(widthImageContainer).attr('data-prev-width'),
			    checkedWidthRadio = $(widthImageContainer).find('input[type="radio"]:checked').val(),
			    customWidthInput = $(widthImageContainer).find('.customWidthInput').val(),
			    currImageWidth = checkedWidthRadio === 'custom' ? customWidthInput : checkedWidthRadio,
			    isChangeMultiblock = $(multiblock).attr('data-prev-content') !== $(multiblock).val(),
			    isChangeCheckbox = !!parseInt($(checkbox).attr('data-prev-content')) !== $(checkbox).is(':checked'),
			    isChangeImageWidth = prevImageWidth !== currImageWidth;

			return isChangeCheckbox || isChangeMultiblock || isChangeImageWidth;
		});
	} else {
		filteredCollection = collectionElements.filter(function (container) {
			var element = null,
			    result = false;

			if (collectionType === 'blocks') {
				element = $(container).find('.blockName');

				result = $(element).attr('data-prev-content') !== $(element).val();
			} else if (collectionType === 'questions') {
				element = $(container).find('.questionContent');

				var prevImageWidth = $(container).find('.imageWidthContainer').attr('data-prev-width'),
				    autoImageBtn = $(container).find('.imageWidthContainer .enableAutoWidth'),
				    currImageWidth = void 0;

				if (autoImageBtn.hasClass('btn-success')) {
					currImageWidth = 'auto';
				} else {
					var checkedWidthRadio = $(container).find('.imageWidthContainer input[type="radio"]:checked').val(),
					    customWidthInput = $(container).find('.imageWidthContainer .customWidthInput').val(),
					    _currImageWidth = checkedWidthRadio === 'custom' ? customWidthInput : checkedWidthRadio;
				}

				var isChangeImageWidth = prevImageWidth !== currImageWidth,
				    isChangeContent = $(element).attr('data-prev-content') !== $(element).val();

				result = isChangeImageWidth || isChangeContent;
			} else {
				throw Error('You must specify correct parameter collectionType');
			}

			return result;
		});
	}

	return filteredCollection;
}

function saveElements(collection) {
	var typeSpeciality = $('#type_speciality').val(),
	    tableName = collection.tableName,
	    typeImage = collection.typeImage;

	return collection.elements.map(function (element) {
		var dataToSave = void 0,
		    blockId = $(document).find('.blockName').attr('data-id');
		if (tableName === 'answers' && typeSpeciality == 0) {
			var id = $(element).find('.multiblock').attr('data-id'),
			    contentData = $(element).find('.multiblock').val(),
			    isChecked = $(element).find('.correctAnswer').is(':checked'),
			    _getImageSrc = getImageSrc(contentData),
			    _getImageSrc2 = _slicedToArray(_getImageSrc, 2),
			    elementContent = _getImageSrc2[0],
			    images = _getImageSrc2[1];


			dataToSave = { tableName: tableName, id: id, elementContent: elementContent, images: images, typeImage: typeImage, isChecked: isChecked };
		} else {
			var _id = void 0,
			    _contentData = null;

			if (tableName === 'blocks' && typeSpeciality === 1) {
				_id = $(element).find('.blockName').attr('data-id');
				_contentData = $(element).find('.blockName').val();

				dataToSave = { tableName: tableName, id: _id, contentData: _contentData };
			} else if (tableName === 'answers' && parseInt(typeSpeciality) === 1) {
				_id = $(element).attr('data-id');
				_contentData = $(element).val();

				var _getImageSrc3 = getImageSrc(_contentData),
				    _getImageSrc4 = _slicedToArray(_getImageSrc3, 2),
				    _elementContent = _getImageSrc4[0],
				    _images = _getImageSrc4[1];

				dataToSave = { tableName: tableName, id: _id, elementContent: _elementContent, images: _images, typeImage: typeImage };
			} else {
				var imageWidth = void 0;
				if (tableName === 'blocks') {
					_id = $(element).find('.blockName').attr('data-id');
					_contentData = $(element).find('.blockName').val();
				} else {
					var autoWidthBtn = $(element).find('.imageWidthContainer .enableAutoWidth');
					if (autoWidthBtn.hasClass('btn-success')) {
						imageWidth = 'auto';
					} else {
						var checkedWidthRadio = $(element).find('.imageWidthContainer input[type="radio"]:checked').val(),
						    customWidthInput = $(element).find('.imageWidthContainer .customWidthInput').val();
						imageWidth = checkedWidthRadio === 'custom' ? customWidthInput : checkedWidthRadio;
					}

					_id = $(element).find('.questionContent').attr('data-id');
					_contentData = $(element).find('.questionContent').val();
				}

				var _getImageSrc5 = getImageSrc(_contentData),
				    _getImageSrc6 = _slicedToArray(_getImageSrc5, 2),
				    _elementContent2 = _getImageSrc6[0],
				    _images2 = _getImageSrc6[1];

				dataToSave = { tableName: tableName, id: _id, elementContent: _elementContent2, images: _images2, typeImage: typeImage, imageWidth: imageWidth };
			}
		}
		dataToSave.blockId = blockId;
		return $.ajax({
			url: '/update-blank',
			method: 'post',
			data: dataToSave
		});
	});
}

function saveCommissioners(commissioners, idSpeciality) {
	return $.ajax({
		url: '/update-commissioners',
		method: 'post',
		data: { commissioners: commissioners, idSpeciality: idSpeciality }
	});
}

module.exports = {
	validateQuestionsAndAnswers: validateQuestionsAndAnswers,
	cleanValidation: cleanValidation
};

/***/ }),

/***/ 68:
/***/ (function(module, exports) {

var typeElements = {
	'textList': 'textList',
	'textQuestions': 'textQuestions',
	'listAnswers': 'listAnswers',
	'question': 'question'
};

var regex = /&(|nbsp|amp|quot|lt|gt);/g;
var entityMap = {
	"nbsp": " ",
	"amp": "&",
	"quot": "\"",
	"lt": "<",
	"gt": ">"
};
var escapeHtml = function escapeHtml(string) {
	return String(string).replace(regex, function (match, entity) {
		return entityMap[entity];
	});
};

var checkChangeImageWidth = function checkChangeImageWidth(container) {
	var prevImageWidth = container.attr('data-prev-image-width'),
	    autoWidthBtn = container.find('.enableAutoWidth ');
	var currImageWidth = void 0;

	if (autoWidthBtn.hasClass('btn-success')) {
		currImageWidth = 'auto';
	} else {
		var checkedRadio = container.find('input:radio:checked').val(),
		    customWidthInput = container.find('.customWidthInput');

		currImageWidth = checkedRadio !== 'custom' ? checkedRadio : customWidthInput.val();
	}

	return prevImageWidth !== currImageWidth;
};

var checkChangeAnswers = function checkChangeAnswers(answers) {
	return answers.some(function (answer) {
		var prevContent = escapeHtml($(answer).attr('data-prev-answer-value')),
		    currContent = escapeHtml($(answer).find('.item-name').val()),
		    prevStateChecked = parseInt($(answer).attr('data-is-checked')),
		    currStateChecked = $(answer).find('.correct-answer').is(':checked') ? 1 : 0,
		    isChangeContent = prevContent !== currContent,
		    isChangeStateChecked = prevStateChecked !== currStateChecked;

		return isChangeContent || isChangeStateChecked;
	});
};

var checkChangeQuestions = function checkChangeQuestions(questions) {
	return questions.some(function (question) {
		var imageWidthContainer = $(question).find('.imageWidthContainer'),
		    answersContainer = $(question).find('.first-answers-block .item').toArray(),
		    prevContent = escapeHtml($(question).attr('data-prev-question-value')),
		    currContent = escapeHtml($(question).find('.category .question-name').val()),
		    isChangeContent = prevContent !== currContent,
		    isChangeImageWidth = checkChangeImageWidth(imageWidthContainer),
		    isChangeAnswers = checkChangeAnswers(answersContainer);

		return isChangeImageWidth || isChangeContent || isChangeAnswers;
	});
};

var checkChangeListElements = function checkChangeListElements(listRows) {
	return listRows.some(function (row) {
		var textCol = $(row).find('.textCol'),
		    positionCol = $(row).find('.positionCol'),
		    prevText = textCol.attr('data-prev-value'),
		    prevPosition = positionCol.attr('data-prev-value'),
		    currText = textCol.find('input').val(),
		    currPosition = positionCol.find('select').val(),
		    isChangedText = prevText !== currText,
		    isChangedPosition = prevPosition !== currPosition;

		return isChangedText || isChangedPosition;
	});
};

var filterChangedElements = function filterChangedElements(collectionElements, typeElement) {
	var textList = typeElements.textList,
	    textQuestions = typeElements.textQuestions,
	    listAnswers = typeElements.listAnswers,
	    question = typeElements.question;

	switch (typeElement) {
		case textList:
			return collectionElements.filter(function (element) {
				var textContainer = $(element).find('.text-container'),
				    listContainer = $(element).find('.list-container'),
				    listRows = listContainer.find('tbody tr').toArray(),
				    imageWidthContainer = textContainer.find('.imageWidthContainer'),
				    prevText = escapeHtml(textContainer.attr('data-prev-value')),
				    currText = escapeHtml(textContainer.find('.multiblock').val()),
				    isChangeImageWidth = checkChangeImageWidth(imageWidthContainer),
				    isChangeText = prevText !== currText,
				    isChangeListElements = checkChangeListElements(listRows);

				return isChangeImageWidth || isChangeText || isChangeListElements;
			});
		case textQuestions:
			return collectionElements.filter(function (element) {
				var textContainer = $(element).find('.text-container'),
				    questionContainer = $(element).find('.questions-container'),
				    questions = questionContainer.find('.category-block').toArray(),
				    imageWidthContainer = textContainer.find('.imageWidthContainer'),
				    prevText = escapeHtml(textContainer.attr('data-prev-value')),
				    currText = escapeHtml(textContainer.find('.multiblock').val()),
				    isChangeTextImageWidth = checkChangeImageWidth(imageWidthContainer),
				    isChangedText = prevText !== currText,
				    isChangedQuestions = checkChangeQuestions(questions);

				return isChangeTextImageWidth || isChangedText || isChangedQuestions;
			});
		case listAnswers:
			return collectionElements.filter(function (element) {
				var answerListContainer = $(element).find('.list-answers-container'),
				    listContainer = $(element).find('.list-container'),
				    answerListRows = answerListContainer.find('tbody tr').toArray(),
				    listRows = listContainer.find('tbody tr').toArray(),
				    isChangedListElements = checkChangeListElements(listRows),
				    isChangedAnswerListElements = answerListRows.some(function (answerListRow) {
					var textCol = $(answerListRow).find('.textCol'),
					    prevContent = textCol.attr('data-prev-value'),
					    currContent = textCol.find('input').val();

					return prevContent !== currContent;
				});

				return isChangedAnswerListElements || isChangedListElements;
			});
		case question:
			return collectionElements.filter(function (questionsContainer) {
				var questions = $(questionsContainer).find('.category-block').toArray();

				return checkChangeQuestions(questions);
			});
		default:
			return [];
	}
};

module.exports = {
	filterChangedElements: filterChangedElements,
	typeElements: typeElements
};

/***/ }),

/***/ 7:
/***/ (function(module, exports) {

function getImageSrc(str) {
	var arraySrc = str.match(/src="(.*?)"/g);

	if (arraySrc != null) {
		var newarray = [];
		for (var i = 0; i < arraySrc.length; i++) {
			newarray.push(arraySrc[i].match(/"(.*?)"/)[1]);
		}
		str = str.replace(/src="(.*?)"/g, 'src="URLPATH"');
		return [str, newarray];
	} else {
		return [str, ''];
	}
}

module.exports = getImageSrc;

/***/ }),

/***/ 8:
/***/ (function(module, exports, __webpack_require__) {

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var createSwal = __webpack_require__(3).createSwal;
var redactorMultiblock = __webpack_require__(4).redactorMultiblock;
var addNewAnswer = __webpack_require__(11).addNewAnswer;
var getImageSrc = __webpack_require__(7);

var _require = __webpack_require__(1),
    setDefaultImageWidth = _require.setDefaultImageWidth,
    setWidthImage = _require.setWidthImage,
    addListenersToImages = _require.addListenersToImages;

var _require2 = __webpack_require__(12),
    renderEnglishTask = _require2.renderEnglishTask,
    updateEditEng = _require2.updateEditEng;

var _require3 = __webpack_require__(25),
    normalMultiblock = _require3.normalMultiblock;

var setWidthListeners = __webpack_require__(6);

var langBlock = '<div class="category-block" style="display: block;">\n\t\t\t\t\t\t<div class="imageWidthContainer im_regculation">\n\t\t\t\t\t\t\t<div class="row col-xs-10">\n\t\t\t\t\t\t\t\t<span>\u0412\u043A\u0430\u0436\u0456\u0442\u044C \u0448\u0438\u0440\u0438\u043D\u0443 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u044C:</span>\n\t\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-2" value="50">50px</label>\n\t\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-2" value="75">75px</label>\n\t\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-2" value="100" checked>100px</label>\n\t\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-2" value="125">125px</label>\n\t\t\t\t\t\t\t\t<label><input type="radio" name="image-width-radio-2" value="150">150px</label>\n\t\t\t\t\t\t\t\t<label><input type="radio" class="customWithRadio" value="custom" name="image-width-radio-2" aria-label="image-with"></label>\n\t\t\t\t\t\t\t\t<input type="number" class="form-control customWidthInput" aria-label="custom-image-with" min="10" max="350" disabled>\n\t\t\t\t\t\t\t\t<button class = "changeWidthBtn btn btn-info" disabled>\u0417\u043C\u0456\u043D\u0438\u0442\u0438</button>\n\t\t\t\t\t\t\t\t<button class="enableAutoWidth btn btn-danger">Auto/off</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n                        <div class="row category" style="margin-bottom: 5px">\n                            <div class="col-xs-12 form-inline" style="width: 100%">\n                                    <div class="lang-item">\n                                        <div class="lang-area">\n                                            <div class="popupImageConvertor">\n                                                <div class="popup-container">\n                                                    <h3 class="resize-title">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n                                                    <div class="resizer" >\n                                                        <span class="addition">ctrl+v</span>\n                                                    </div>\n                    \n                                                    <button class="resizer-result btn btn-info" class="btn btn-info">\u0412\u0441\u0442\u0430\u0432\u0438\u0442\u0438 \u0432\u0438\u0434\u0456\u043B\u0435\u043D\u0435 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n                                                    <button class="closePopup">&#10006;</button>\n                                                   \n                                                </div>\n                                            </div>\n                                    \n                                            <textarea id="lang_question" maxlength="1000" class="multiblock form-control text_area_lang"></textarea>\n                                         </div>\n                                         <div class="lang-area">\n                                            <div class="popupImageConvertor">\n                                                <div class="popup-container">\n                                                    <h3 class="resize-title">\u0412\u0441\u0442\u0430\u0432\u043A\u0430 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</h3>\n                                                    <div class="resizer" >\n                                                        <span class="addition">ctrl+v</span>\n                                                    </div>\n                    \n                                                    <button class="resizer-result btn btn-info" class="btn btn-info">\u0412\u0441\u0442\u0430\u0432\u0438\u0442\u0438 \u0432\u0438\u0434\u0456\u043B\u0435\u043D\u0435 \u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</button>\n                                                    <button class="closePopup">&#10006;</button>\n                                                   \n                                                </div>\n                                            </div>\n                                            <textarea id="lang_answers" maxlength="1000" class="multiblock form-control text_area_lang"></textarea>\n                                        </div>\n                                    </div>\n                                <div class="btn-space">\n                                    <span class="category-delete" data-toggle="tooltip" title="" data-original-title="\u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u043F\u0438\u0442\u0430\u043D\u043D\u044F">\n                                      <button id="deleteLang" type="button" class="btn btn-default">\n                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"> \u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u043F\u0438\u0442\u0430\u043D\u043D\u044F</span>\n                                      </button>\n                                    </span>\n\t\t\t\t\t\t\t\t</div>\n                            </div>\n                        </div>\n                    </div>';

$('#panel3 .multiblock').off('click');
$('#panel3 .multiblock').on('click', redactorMultiblock);

var countAnswers = $('#block_selector_for_edit option:selected').attr('name');

var setCustomImageWidth = function setCustomImageWidth() {
	var addChangeWidthBtn = $('#panel3 .changeWidthBtn');

	addChangeWidthBtn.off('click');
	addChangeWidthBtn.on('click', function (e) {
		e.preventDefault();

		var imageWidth = $(e.currentTarget).prev().val();

		setWidthImage(imageWidth, $(e.currentTarget).prev().prev().find('input'));
	});
};

function getLangQuestions() {
	var spec_id = $('#id_speciality').val();

	$.ajax({
		url: '/get-ukrainian-tasks',
		method: 'post',
		data: { spec_id: spec_id },
		success: function success(response) {
			$('#get_lang_edit').html('');

			if (response.size > 0) {
				for (var i = 0; i < response.size; i++) {
					var text = response.questions[i].text_question;
					text = removeHTML(text);
					if (text == "") {
						text = "Зображення";
					}

					$('#get_lang_edit').append($('<option>', {
						id: response.questions[i].text_question,
						value: response.questions[i].id,
						text: text.substr(0, 120) + '...',
						name: response.answers[i],
						'data-width': response.questions[i].image_width
					}));
				}

				$('#panel3 .empty-task').addClass('hidden');
				$('#lang_edit_block_element').removeClass('hidden');
			} else {
				$('#langEmpty').show();
				$('#panel3 .empty-task').removeClass('hidden');
				$('#lang_edit_block_element').addClass('hidden');
			}

			$('#get_lang_edit').val('');
		},
		error: function error(err) {
			console.log(err);
		}
	});
}

function getQuestionLang() {
	if ($('#changingLang .category-block').length === 0) {
		$('#changingLang').prepend(langBlock);
		$('#changingLang .multiblock').on('click', redactorMultiblock);
		setWidthListeners();
		addListenersToImages();
	}

	setCustomImageWidth();
	$('.enableAutoWidth.btn-success').click();

	$('#changingLang').show();

	var imageWidth = $('#get_lang_edit option:selected').attr('data-width'),
	    radio = $('#changingLang').find('input[type="radio"]');

	if (imageWidth !== 'auto') {
		$(radio[5]).parent().next().prop('disabled', true);
		$(radio[5]).parent().next().next().prop('disabled', true);
		$('#changingLang .customWidthInput').val('');
		switch (imageWidth) {
			case '50':
				$(radio[0]).prop('checked', true);
				break;
			case '75':
				$(radio[1]).prop('checked', true);
				break;
			case '100':
				$(radio[2]).prop('checked', true);
				break;
			case '125':
				$(radio[3]).prop('checked', true);
				break;
			case '150':
				$(radio[4]).prop('checked', true);
				break;
			default:
				$(radio[5]).prop('checked', true);
				$(radio[5]).parent().next().prop('disabled', false);
				$(radio[5]).parent().next().next().prop('disabled', false);
				$(radio[5]).parent().next().val(imageWidth);
		}
	} else {
		$('#changingLang .enableAutoWidth').click();
	}

	$('#lang_question').val($('#get_lang_edit option:selected').attr('id'));
	$('#lang_answers').val($('#get_lang_edit option:selected').attr("name"));
	$('#panel3 #lang_answers').click();
	$('#panel3 #lang_question').click();

	$('#panel3 .html.highlighted').click();
}

function getQuestions() {
	countAnswers = $('#block_selector_for_edit option:selected').attr('name');

	$('#tasks_selector_for_edit').html("");

	var editTaskSelect = $('#block_selector_for_edit'),
	    idBlock = editTaskSelect.val(),
	    selectQuestionContainer = $('#questions_edit_block_elements');

	$.ajax({
		url: '/get-questions',
		method: 'post',
		data: { idBlock: idBlock },
		success: function success(response) {
			if (response.length > 0) {
				$('#questions_edit_block_elements [data-toggle="tooltip"] button').prop("disabled", false);
				var _iteratorNormalCompletion = true;
				var _didIteratorError = false;
				var _iteratorError = undefined;

				try {
					for (var _iterator = response[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
						var question = _step.value;

						question['text_question'] = removeHTML(question['text_question']);
						if (question['text_question'] == "") {
							$('#tasks_selector_for_edit').append('<option value="' + question['id'] + '" name = "image" >\u0417\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u043D\u044F</option>');
						} else {
							$('#tasks_selector_for_edit').append('<option value="' + question['id'] + '" name = "normal">' + question['text_question'] + '</option>');
						}
					}
				} catch (err) {
					_didIteratorError = true;
					_iteratorError = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion && _iterator.return) {
							_iterator.return();
						}
					} finally {
						if (_didIteratorError) {
							throw _iteratorError;
						}
					}
				}

				selectQuestionContainer.show();
			} else {
				selectQuestionContainer.hide();

				createSwal({
					title: 'Питання до даного блоку відсутні',
					type: 'info'
				});
				editTaskSelect.val('');
			}
		},
		error: function error(err) {
			createSwal({
				title: 'Помилка при спробі отримати дані блоку',
				type: 'error'
			});
		}
	});
}

function getQuestion() {
	$('#panel3 .imageWidthContainer .customWidthInput').attr('disabled', true);
	$('#panel3 .imageWidthContainer .changeWidthBtn').attr('disabled', true);
	$('#panel3 .imageWidthContainer .customWidthInput').val('');

	$('#first_edit_item').empty();

	$('#panel3 .text-block').hide();
	$('#panel3 .list-block').hide();
	$('#panel3 .list-answers-block').hide();
	$('#panel3 .category-block:not(:first)').remove();

	$('#panel3 .category-block .category').remove();
	$('#panel3 .category-block .imageWidthContainer').after(normalMultiblock);
	$('#panel3 .category-block .text-question').on('click', redactorMultiblock);
	$('#panel3 .category-block .addNewAnswer').on('click', addAnswerField);

	if ($('#type') == 2) {
		$('#insert_question_thing').show();
	} else {
		$('#insert_question_thing').hide();
	}

	$('#questions_edit_block_elements [data-toggle="tooltip"] button').prop("disabled", false);

	var idQuestion = $("#tasks_selector_for_edit").val();

	$.ajax({
		url: '/get-question',
		method: 'post',
		data: { idQuestion: idQuestion },
		success: function success(response) {
			var imageWidth = response.question[0].image_width;

			setDefaultImageWidth(imageWidth);

			$('#answer_items_edit').html('');
			var questionBlock = $('#form_edit_task .questions-block'),
			    question = questionBlock.find('.category .text-question');

			question.val(response['question'][0].text_question);
			insertAnswers(response['answers']);

			var addAnswer = $('#panel3 .addNewAnswer');
			addAnswer.off('click');
			addAnswer.on('click', addAnswerField);
			addAnswer.show();

			$('#tasks_selector_for_edit option:selected').attr('name', response['answers'].length);

			var multiblocks = $('#panel3 .multiblock');
			multiblocks.off('click');
			multiblocks.on('click', redactorMultiblock);

			$('#form_edit_task').show();
			questionBlock.show();
			questionBlock.find('.category-block').show();

			addListenersToImages();

			$('#panel3 .text-question').click();
			$('#panel3 .item-name').click();

			setCustomImageWidth();
		},
		error: function error(err) {
			createSwal({
				title: 'Помилка при спробі отримання данних',
				type: 'error'
			});
			console.log(err);
		}
	});

	function insertAnswers(answers) {
		var answersContainer = $('#first_edit_item');
		var _iteratorNormalCompletion2 = true;
		var _didIteratorError2 = false;
		var _iteratorError2 = undefined;

		try {
			for (var _iterator2 = answers.entries()[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
				var _ref = _step2.value;

				var _ref2 = _slicedToArray(_ref, 2);

				var i = _ref2[0];
				var answer = _ref2[1];

				var ansName = answer.text_answer,
				    isRadio = true;

				if (typeof answer['text_answer'] === 'undefined') {
					isRadio = false;
				}

				answersContainer.append(addNewAnswer(answer, false, !isRadio, false, isRadio, isRadio));

				var multiblocks = $('#panel3 .multiblock');
				multiblocks.off('click');
				multiblocks.on('click', redactorMultiblock);

				if (countAnswers <= i) {
					answersContainer.find('.deleteAnswer').show();
				}
			}
		} catch (err) {
			_didIteratorError2 = true;
			_iteratorError2 = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion2 && _iterator2.return) {
					_iterator2.return();
				}
			} finally {
				if (_didIteratorError2) {
					throw _iteratorError2;
				}
			}
		}

		answersContainer.find('.deleteAnswer').off('click');
		answersContainer.find('.deleteAnswer').on('click', deleteAnswer);

		answersContainer.find('input:file').on('click', function (e) {
			var input = $(e.currentTarget);
			input.attr('data-is-change', true);
			input.off('click');
		});
	}
}

function addAnswerField() {
	var answersContainer = $('#answer_items_edit');
	answersContainer.append(addNewAnswer(null, false, false, false, true, true));

	var multiblocks = $('#panel3 .multiblock');
	multiblocks.off('click');
	multiblocks.on('click', redactorMultiblock);

	$('#panel3 .item-name').last().click();

	$('.deleteAnswer').show();

	answersContainer.find('.deleteAnswer').off('click');
	answersContainer.find('.deleteAnswer').on('click', deleteAnswer);
	addListenersToImages();
}

function deleteAnswer() {
	var answersContainer = $(this).parent().parent().parent().parent().parent(),
	    nmbTask = answersContainer.find('.item').length - 1;

	countAnswers = $('#block_selector option:selected').attr('name');

	if ($('#tasks_selector_for_edit').is(':visible')) {
		answersContainer = $(this).closest('#form_edit_task');
		countAnswers = $('#tasks_selector_for_edit option:selected').attr('name');
		nmbTask = answersContainer.find('.answer-item').length / 2 - 1;
	}

	if (countAnswers === nmbTask.toString()) {
		answersContainer.find('.deleteAnswer').hide();
	}

	var id = $(this).parent().find('.item-name').attr('data-id-answer');
	$.ajax({
		url: 'delete_answer',
		method: 'post',
		data: {
			'id': id
		},
		error: function error(err) {
			createSwal({
				title: 'Помилка при видаленні відповіді',
				type: 'error'
			});
		}
	});

	$(this).parent().parent().parent().parent().remove();
}

function handleEditTask() {
	var editFormTask = $('#form_edit_task');
	$(editFormTask).off('submit');

	$(editFormTask).on('submit', function (e) {
		e.preventDefault();

		$('#form_edit_task button:submit').prop('disabled', true);

		var typeSpeciality = $('#type').text();

		if (typeSpeciality === '2') {
			updateEditEng();
		} else {
			EditTasks();
		}
		$('#form_edit_task button:submit').prop('disabled', false);
	});

	$('#updateLang').off('click');
	$('#updateLang').on('click', EditLangTask);

	$('#deleteLang').off('click');
	$('#deleteLang').on('click', DeleteLangTask);

	function DeleteLangTask() {
		var isConfirm = confirm('Ви дійсно хочете видати дане завдання?');
		if (isConfirm) {
			$(document).find('.bookshelf_wrapper').show();
			$(document).find('.hideBehind').show();
			$('#all').css("opacity", "0.5");

			$.ajax({
				url: '/delete-ukrainian-task',
				method: 'post',
				data: {
					'spec_id': $('#id_speciality').val(),
					'question_id': $('#get_lang_edit').val()
				},
				success: function success() {
					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					createSwal({
						title: 'Завдання видалено',
						type: 'success'
					});

					$('#changingLang').hide();

					getLangQuestions();

					$('#lang_question').val('');
					$('#lang_answers').val('');
				},
				error: function error(err) {
					$(document).find('.bookshelf_wrapper').hide();
					$(document).find('.hideBehind').hide();
					$('#all').css("opacity", "1");

					createSwal({
						title: 'Помилка при видаленні завдання',
						type: 'error'
					});
					console.log(err);
				}
			});
		}
	}

	function EditLangTask() {
		$(document).find('.bookshelf_wrapper').show();
		$(document).find('.hideBehind').show();
		$('#all').css("opacity", "0.5");

		var questionContent = $('#lang_question').val(),
		    answerContent = $('#lang_answers').val(),
		    imageWidth = $('#lang_question').closest('.category-block').find('input[type="radio"]:checked');

		if ($(imageWidth).val() === 'custom') {
			imageWidth = $(imageWidth).parent().next();
		}

		if ($('#lang_question').closest('.category-block').find('.enableAutoWidth').hasClass('btn-success')) {
			imageWidth = 'auto';
		} else {
			imageWidth = $(imageWidth).val();
		}

		var _getImageSrc = getImageSrc(questionContent);

		var _getImageSrc2 = _slicedToArray(_getImageSrc, 2);

		questionContent = _getImageSrc2[0];
		questionImages = _getImageSrc2[1];

		var _getImageSrc3 = getImageSrc(answerContent);

		var _getImageSrc4 = _slicedToArray(_getImageSrc3, 2);

		answerContent = _getImageSrc4[0];
		answerImages = _getImageSrc4[1];


		$.ajax({
			url: '/update-ukrainian-task',
			method: 'post',
			data: {
				'questionContent': questionContent,
				'answerContent': answerContent,
				'question_id': $('#get_lang_edit').val(),
				'questionImages': questionImages,
				'answerImages': answerImages,
				'image_width': imageWidth
			},
			success: function success(response) {
				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");

				getLangQuestions();

				$('#changingLang').hide();

				$('#lang_question').val('');
				$('#lang_answers').val('');

				$('#get_lang_edit').val('');
				createSwal({
					title: 'Завдання оновлено',
					type: 'success'
				});
			},
			error: function error(err) {
				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");
				createSwal({
					title: 'Помилка при запису завдання',
					type: 'error'
				});

				console.log(err);
			}
		});
	}

	function EditTasks() {
		var textQuestion = $('#form_edit_task .question-name').val(),
		    answers = $('#first_edit_item .item-name'),
		    answersTexts = [],
		    answersImages = [],
		    answersId = [];

		var _iteratorNormalCompletion3 = true;
		var _didIteratorError3 = false;
		var _iteratorError3 = undefined;

		try {
			for (var _iterator3 = answers[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
				var ans = _step3.value;

				var text = $(ans).val();

				var _getImageSrc7 = getImageSrc(text);

				var _getImageSrc8 = _slicedToArray(_getImageSrc7, 2);

				text = _getImageSrc8[0];
				answerImage = _getImageSrc8[1];

				answersTexts.push(text);
				answersImages.push(answerImage);
				answersId.push($(ans).attr('data-id-answer'));
			}
		} catch (err) {
			_didIteratorError3 = true;
			_iteratorError3 = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion3 && _iterator3.return) {
					_iterator3.return();
				}
			} finally {
				if (_didIteratorError3) {
					throw _iteratorError3;
				}
			}
		}

		var err = 0,
		    error = 1;

		if (textQuestion == '' || textQuestion == ' ') {
			err = 1;
		}

		for (var i = 0; i < answersTexts.length; i++) {
			if (answersTexts[i] == '' || answersTexts[i] == ' ') {
				err = 1;
			}
		}

		$('input[name=cat-radio]').each(function () {
			if ($(this).prop('checked')) {
				error = 0;
			}
		});

		if (error === 1) {
			createSwal({
				title: 'Оберіть правильний варіант',
				type: 'error'
			});
		} else if (err == 1) {
			createSwal({
				title: 'Має бути заповнений текст завданнь або додане зображення',
				type: 'error'
			});
		} else {
			var saveEditedQuestion = function saveEditedQuestion() {
				var questionBlock = $('#form_edit_task .questions-block'),
				    contentData = questionBlock.find('.text-question').val(),
				    checkedRadio = questionBlock.find('.imageWidthContainer input[type="radio"]:checked'),
				    imageWidthInput = questionBlock.find('.imageWidthContainer .customWidthInput'),
				    imageWidth = checkedRadio.val() !== 'custom' ? checkedRadio.val() : imageWidthInput.val();

				if (questionBlock.find('.enableAutoWidth').hasClass('btn-success')) {
					imageWidth = 'auto';
				}

				var questionData = {
					id: questionId,
					contentData: contentData,
					imageWidth: imageWidth
				};

				saveElement(questionData, 'Question');
			};

			var saveEditedAnswers = function saveEditedAnswers() {
				var dataAnswers = _extends({}, answersData[_countAnswers], {
					isChecked: isChecked[_countAnswers],
					questionId: questionId
				});

				saveElement(dataAnswers, 'Answer');
				_countAnswers++;
			};

			var saveElement = function saveElement(dataElement, elementType) {
				var url = elementType === 'Question' ? '/update-question' : '/update-answer';

				var _getImageSrc5 = getImageSrc(dataElement.contentData),
				    _getImageSrc6 = _slicedToArray(_getImageSrc5, 2),
				    contentData = _getImageSrc6[0],
				    images = _getImageSrc6[1];

				var data = _extends({}, dataElement, {
					contentData: contentData,
					images: images
				});

				$.ajax({
					url: url,
					method: 'post',
					data: data,
					success: function success() {
						$('#form_edit_task button:submit').prop('disabled', false);

						if (_countAnswers < nmbAnswers) {
							saveEditedAnswers();
						} else {
							$(document).find('.bookshelf_wrapper').hide();
							$(document).find('.hideBehind').hide();
							$('#all').css("opacity", "1");

							$('#form_edit_task').hide();

							var questionText = $('#form_edit_task .questions-block .text-question').val();

							updateEditQuestionSelect('update', questionText);

							createSwal({
								title: 'Завдання оновлено',
								type: 'success'
							});

							$('.enableAutoWidth.btn-success').click();

							$('#form_edit_task input:file').map(function (index, input) {
								$(input).val('');
							});
						}
					},
					error: function error(err) {
						$(document).find('.bookshelf_wrapper').hide();
						$(document).find('.hideBehind').hide();
						$('#all').css("opacity", "1");

						$('#form_edit_task button:submit').prop('disabled', false);

						createSwal({
							title: 'Помилка при запису завдання',
							type: 'error'
						});
					}
				});
			};

			$(document).find('.bookshelf_wrapper').show();
			$(document).find('.hideBehind').show();
			$('#all').css("opacity", "0.5");

			var form = this,
			    token = $(this).find('>:first-child').val(),
			    questionId = $("#tasks_selector_for_edit").val();

			saveEditedQuestion();
			var questionBlock = $('#form_edit_task .questions-block'),
			    answersData = questionBlock.find('.item-name').toArray().map(function (el) {

				return { contentData: $(el).val() };
			});

			var isChecked = questionBlock.find('input[type=checkbox]').toArray().map(function (input) {
				return $(input).is(':checked') ? 1 : 0;
			});

			var _countAnswers = 0;
			var nmbAnswers = answersId.length;
		}
	}
}

function deleteQuestion() {
	var isConfirm = confirm('Ви дійсно хочете видати дане завдання?'),
	    idQuestion = $('#tasks_selector_for_edit').val();

	if (isConfirm) {
		$(document).find('.bookshelf_wrapper').show();
		$(document).find('.hideBehind').show();
		$('#all').css("opacity", "0.5");

		$.ajax({
			url: '/delete-question',
			method: 'post',
			data: {
				idQuestion: idQuestion
			},
			success: function success() {
				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");

				$('#form_edit_task').hide();
				updateEditQuestionSelect('delete');

				createSwal({
					title: 'Завдання успішно видалено',
					type: 'success'
				});
			},
			error: function error(err) {
				$(document).find('.bookshelf_wrapper').hide();
				$(document).find('.hideBehind').hide();
				$('#all').css("opacity", "1");

				createSwal({
					title: 'Помилка при видаленні завдання',
					type: 'error'
				});
				console.log(err);
			}
		});
	}
}

function updateEditQuestionSelect(action) {
	var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

	var select = $('#tasks_selector_for_edit'),
	    selectContainer = $('#questions_edit_block_elements'),
	    idQuestion = $(select).val();

	data = removeHTML(data);

	if (action === 'update') {
		if (data == '') {
			$(select).find('option[value="' + idQuestion + '"]').text('Зображення');
			$(select).find('option[value="' + idQuestion + '"]').attr('name', 'image');
		} else {
			$(select).find('option[value="' + idQuestion + '"]').text(data);
			$(select).find('option[value="' + idQuestion + '"]').attr('name', 'normal');
		}
	} else if (action === 'delete') {
		$(select).find('option[value="' + idQuestion + '"]').remove();

		var nmbOptions = select.find('option').length;
		if (nmbOptions < 1) {
			selectContainer.hide();
		}
	}
	$(select).val('');

	$('#questions_edit_block_elements [data-toggle="tooltip"] button').prop("disabled", true);
}

function removeHTML(str) {
	if (str) {
		while (str.indexOf('<') != -1 && str.indexOf('>') != -1 && str.indexOf('>') > str.indexOf('<')) {
			str = str.slice(0, str.indexOf('<')) + str.slice(str.indexOf('>') + 1, str.length);
		}
	}
	return str;
}

module.exports = {
	getQuestion: getQuestion,
	getQuestions: getQuestions,
	getQuestionLang: getQuestionLang,
	getLangQuestions: getLangQuestions,
	handleEditTask: handleEditTask,
	deleteQuestion: deleteQuestion,
	addNewAnswer: addNewAnswer,
	addAnswerField: addAnswerField,
	deleteAnswer: deleteAnswer,
	removeHTML: removeHTML
};

/***/ }),

/***/ 96:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(97);


/***/ }),

/***/ 97:
/***/ (function(module, exports, __webpack_require__) {

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var redactorMultiblock = __webpack_require__(4).redactorMultiblock;
var createSwal = __webpack_require__(2).createSwal;

var _require = __webpack_require__(1),
    handleChangeRadio = _require.handleChangeRadio,
    setWidthImage = _require.setWidthImage;

var _require2 = __webpack_require__(12),
    onChangeListSelect = _require2.onChangeListSelect;

var _require3 = __webpack_require__(68),
    filterChangedElements = _require3.filterChangedElements,
    typeElements = _require3.typeElements;

var saveEnglishTasks = __webpack_require__(98);

var _require4 = __webpack_require__(63),
    validateQuestionsAndAnswers = _require4.validateQuestionsAndAnswers,
    cleanValidation = _require4.cleanValidation;

var setWidthListeners = __webpack_require__(6);

window.onload = function () {
	var multiblocks = $('.multiblock');
	$('.list-block').find('select').on('change', onChangeListSelect);
	$('.list-container').find('select').on('change', onChangeListSelect);

	multiblocks.on('click', redactorMultiblock);
	multiblocks.click();

	var addImageWidthRadio = $('.imageWidthContainer input[type="radio"]'),
	    addChangeWidthBtn = $('.changeWidthBtn');

	addImageWidthRadio.off();
	addImageWidthRadio.on('change', function (e) {
		return handleChangeRadio(e);
	});
	addChangeWidthBtn.off();
	addChangeWidthBtn.on('click', function (e) {
		e.preventDefault();
		var imageWidth = $(e.currentTarget).prev().val();
		setWidthImage(imageWidth, $(e.currentTarget).prev().prev().find('input'));
	});
	var approveBtn = $('#approve');

	var handleSubmit = function handleSubmit(e) {
		var valid = true;

		cleanValidation($('.multiblock'));

		$(".validate-alert").remove();

		$('.multiblock').each(function () {
			if (!validateQuestionsAndAnswers(this)) {
				valid = false;
			}
		});

		$('input[type="text"]').removeClass('input-non-valid');

		$('.category-block').each(function () {
			if ($(this).find('input[type="checkbox"]:checked').length === 0 && $(this).find('input[type="checkbox"]').length !== 0) {
				valid = false;
			}
		});

		$('.task-container input[type="text"]').each(function () {
			if ($(this).val() == "") {
				valid = false;
				$(this).addClass('input-non-valid');
			}
		});

		if (!valid) {
			$(document).find('.bookshelf_wrapper').hide();
			$(document).find('.hideBehind').hide();
			$('#all').css("opacity", "1");
			createSwal({
				title: 'Питання не пройшли валідацію',
				type: 'error'
			});
			e.preventDefault();
			return;
		}

		$(document).find('.bookshelf_wrapper').show();
		$(document).find('.hideBehind').show();
		$('#all').css("opacity", "0.5");

		e.preventDefault();

		var submitBtn = $(e.currentTarget);
		submitBtn.off('click');

		$(document).find('.bookshelf_wrapper').show();
		$(document).find('.hideBehind').show();
		$('#edit-container').css("opacity", "0.3");

		var textList = typeElements.textList,
		    textQuestions = typeElements.textQuestions,
		    listAnswers = typeElements.listAnswers,
		    question = typeElements.question;


		var container = $('.task-container'),
		    textsWithList = container.find('.text-list-block').toArray(),
		    textsWithQuestions = container.find('.text-with-question').toArray(),
		    listsWithAnswers = container.find('.list-with-answers-block').toArray(),
		    questionsBlock = container.find('.question-block').toArray(),
		    commissionersContainers = [].concat(_toConsumableArray($('.verifyTableBlock .category-block')));

		var commissioners = commissionersContainers.map(function (commissionerContainer) {
			var id = $(commissionerContainer).attr('data-id');
			var position = $(commissionerContainer).find('.commPosition').val();
			var initials = $(commissionerContainer).find('.commName').val();

			return { id: id, position: position, initials: initials };
		});

		var filteredTextsWithList = filterChangedElements(textsWithList, textList),
		    filteredTextsWithQuestions = filterChangedElements(textsWithQuestions, textQuestions),
		    filteredListAnswers = filterChangedElements(listsWithAnswers, listAnswers),
		    filteredQuestions = filterChangedElements(questionsBlock, question);

		var allTasks = [{ collection: filteredTextsWithList, typeElements: textList }, { collection: filteredTextsWithQuestions, typeElements: textQuestions }, { collection: filteredListAnswers, typeElements: listAnswers }, { collection: filteredQuestions, typeElements: question }],
		    filteredTasks = allTasks.filter(function (task) {
			return task.collection.length > 0;
		}),
		    idSpeciality = $('#edit-container').attr('data-id-speciality');

		saveEnglishTasks(filteredTasks, commissioners, idSpeciality);

		$(document).find('.bookshelf_wrapper').hide();
		$(document).find('.hideBehind').hide();
		$('#all').css("opacity", "1");
	};
	approveBtn.on('click', handleSubmit);
	update();

	setWidthListeners();
};
function update() {
	var newComm = '<div class="category-block" style="display: block;">\n    <div class="row" style="margin-bottom: 5px">\n\t\t<div class="col-xs-12 form-inline">\n\t\t\t<div class = "hidden idsComm">0</div>\n\t\t\t<input class="item-category form-control commPosition" style="width: 45%" type="text" required placeholder="\u041F\u043E\u0441\u0430\u0434\u0430 \u0447\u043B\u0435\u043D\u0430 \u043A\u043E\u043C\u0456\u0441\u0456\u0457" >\n\t\t\t<input class="item-category form-control commName" style="width: 45%" type="text" required placeholder="\u041F\u0406\u0411 \u0447\u043B\u0435\u043D\u0430 \u043A\u043E\u043C\u0456\u0441\u0456\u0457" >\n\t\t<span  class = "category-add-comm" data-toggle="tooltip" title="\u0414\u043E\u0434\u0430\u0442\u0438 \u0447\u043B\u0435\u043D\u0430 \u043A\u043E\u043C\u0456\u0441\u0456\u0457">\n\t\t<button type="button"  class="btn btn-default">\n\t\t<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>\n\t\t</button>\n\t\t</span>\n\t\t<span class="category-delete-comm" data-toggle="tooltip" title="\u0412\u0438\u0434\u0430\u043B\u0438\u0442\u0438 \u0447\u043B\u0435\u043D\u0430 \u043A\u043E\u043C\u0456\u0441\u0456\u0457" style="display: none;">\n\t\t<button type="button" class="btn btn-default">\n\t\t<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>\n\t\t\t</button>\n\t\t\t</span>\n\t\t</div> \n    </div>';
	var rmCommBtn = $('.category-delete-comm'),
	    addCommBtn = $('.category-add-comm'),
	    commContainer = $('#comm');
	updateListeners();

	var nmbComm = $(commContainer).find('.category-block').length;
	if (nmbComm > 1) {
		commContainer.find('.category-delete-comm').show();
	}

	function updateListeners() {
		rmCommBtn = $('.category-delete-comm');
		addCommBtn = $('.category-add-comm');

		addCommBtn.off('click');
		addCommBtn.on('click', addComm);

		rmCommBtn.off('click');
		rmCommBtn.on('click', rmComm);
	}

	function addComm() {

		var nmbComm = $(commContainer).find('.category-block').length;

		if (nmbComm < 5) {
			$('#comm').append(newComm);
			updateListeners();
			if (nmbComm > 0) {
				commContainer.find('.category-delete-comm').show();
			}
		} else {
			createSwal({
				title: 'Максимальна к-ть членів комісії',
				type: 'error'
			});
		}

		if (nmbComm == 1) {
			commContainer.first().find('.category-delete-comm').show();
		}
	}

	function rmComm() {
		var nmbComm = $(commContainer).find('.category-block').length,
		    comm = $(this).parent().parent().parent();

		if (nmbComm > 1) {
			comm.remove();
			updateListeners();
		}
		if (nmbComm == 2) {
			commContainer.first().find('.category-delete-comm').hide();
		}

		// show all adding buttons
		commContainer.find('.category-add-comm').toArray().map(function (el) {
			return $(el).show();
		});
	}
}

/***/ }),

/***/ 98:
/***/ (function(module, exports, __webpack_require__) {

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _require = __webpack_require__(68),
    typeElements = _require.typeElements;

var getImageSrc = __webpack_require__(7);
var createSwal = __webpack_require__(2).createSwal;

var getImageWidth = function getImageWidth(container) {
	var imageWidth = void 0;
	var autoWidthBtn = container.find('.enableAutoWidth ');

	if (autoWidthBtn.hasClass('btn-success')) {
		imageWidth = 'auto';
	} else {
		var checkedRadio = container.find("input:radio:checked").val(),
		    customWidthInput = container.find('.customWidthInput');
		imageWidth = checkedRadio !== 'custom' ? checkedRadio : customWidthInput.val();
	}

	return imageWidth;
};

var getListData = function getListData(listRows) {
	return listRows.map(function (row) {
		var id = $(row).attr('data-id'),
		    textCol = $(row).find('.textCol'),
		    positionCol = $(row).find('.positionCol'),
		    content = textCol.find('input').val(),
		    rightAnswer = positionCol.find('select').val();

		return {
			id: id,
			content: content,
			rightAnswer: rightAnswer
		};
	});
};

var getAnswersData = function getAnswersData(answers) {
	return answers.map(function (answer) {
		var id = $(answer).attr('data-answer-id'),
		    _getImageSrc = getImageSrc($(answer).find('.item-name').val()),
		    _getImageSrc2 = _slicedToArray(_getImageSrc, 2),
		    content = _getImageSrc2[0],
		    images = _getImageSrc2[1],
		    isChecked = $(answer).find('.correct-answer').is(':checked') ? 1 : 0;


		return {
			id: id,
			content: content,
			images: images,
			isChecked: isChecked
		};
	});
};

var getQuestionsData = function getQuestionsData(questions) {
	return questions.map(function (question) {
		var imageWidthContainer = $(question).find('.imageWidthContainer'),
		    answers = $(question).find('.first-answers-block .item').toArray(),
		    id = $(question).attr('data-question-id'),
		    imageWidth = getImageWidth(imageWidthContainer),
		    _getImageSrc3 = getImageSrc($(question).find('.category .question-name').val()),
		    _getImageSrc4 = _slicedToArray(_getImageSrc3, 2),
		    content = _getImageSrc4[0],
		    images = _getImageSrc4[1],
		    answersData = getAnswersData(answers);

		return {
			id: id,
			imageWidth: imageWidth,
			content: content,
			images: images,
			answersData: answersData
		};
	});
};

var textList = typeElements.textList,
    textQuestions = typeElements.textQuestions,
    listAnswers = typeElements.listAnswers,
    question = typeElements.question;


var saveEnglishTasks = function saveEnglishTasks(tasks, commissioners, idSpeciality) {

	$.ajax({
		'url': '/update-commissioners',
		'method': 'post',
		'data': { commissioners: commissioners, idSpeciality: idSpeciality }
	});

	var promiseTasks = tasks.map(function (task) {
		var collection = task.collection,
		    typeElements = task.typeElements;


		switch (typeElements) {
			case textList:
				return collection.map(function (element) {
					var textContainer = $(element).find('.text-container'),
					    textId = textContainer.attr('data-id'),
					    imageWidthContainer = textContainer.find('.imageWidthContainer'),
					    imageWidth = getImageWidth(imageWidthContainer),
					    _getImageSrc5 = getImageSrc(textContainer.find('.text-question').val()),
					    _getImageSrc6 = _slicedToArray(_getImageSrc5, 2),
					    content = _getImageSrc6[0],
					    images = _getImageSrc6[1],
					    listRows = $(element).find('.list-container tbody tr').toArray(),
					    listData = getListData(listRows);


					var dataToSend = {
						textContent: {
							content: content,
							images: images
						},
						textId: textId,
						idSpeciality: idSpeciality,
						imageWidth: imageWidth,
						listData: listData,
						typeElements: typeElements
					};

					return $.ajax({
						'url': '/update-english-blank',
						'method': 'post',
						'data': dataToSend
					});
				});
			case textQuestions:
				return collection.map(function (element) {
					var textContainer = $(element).find('.text-container'),
					    textId = textContainer.attr('data-id'),
					    imageWidthContainer = textContainer.find('.imageWidthContainer'),
					    imageWidth = getImageWidth(imageWidthContainer),
					    _getImageSrc7 = getImageSrc(textContainer.find('.text-question').val()),
					    _getImageSrc8 = _slicedToArray(_getImageSrc7, 2),
					    content = _getImageSrc8[0],
					    images = _getImageSrc8[1],
					    questions = $(element).find('.questions-container .category-block').toArray(),
					    questionData = getQuestionsData(questions);


					var dataToSend = {
						textContent: {
							content: content,
							images: images
						},
						textId: textId,
						imageWidth: imageWidth,
						idSpeciality: idSpeciality,
						questionData: questionData,
						typeElements: typeElements
					};

					return $.ajax({
						'url': '/update-english-blank',
						'method': 'post',
						'data': dataToSend
					});
				});
			case listAnswers:
				return collection.map(function (element) {
					var answerListContainer = $(element).find('.list-answers-container'),
					    listContainer = $(element).find('.list-container'),
					    answerListRows = answerListContainer.find('tbody tr').toArray(),
					    listRows = listContainer.find('tbody tr').toArray(),
					    listData = getListData(listRows),
					    listAnswersData = answerListRows.map(function (answerListRow) {
						var textCol = $(answerListRow).find('.textCol'),
						    id = $(answerListRow).attr('data-id'),
						    content = textCol.find('input').val();

						return {
							id: id,
							content: content
						};
					});

					var dataToSend = {
						idSpeciality: idSpeciality,
						listData: listData,
						listAnswersData: listAnswersData,
						typeElements: typeElements
					};

					return $.ajax({
						'url': '/update-english-blank',
						'method': 'post',
						'data': dataToSend
					});
				});
			case question:
				return collection.map(function (element) {
					var questions = $(element).find('.category-block').toArray(),
					    questionPromises = getQuestionsData(questions).map(function (question) {
						return $.ajax({
							'url': '/update-english-blank',
							'method': 'post',
							'data': { question: question, typeElements: typeElements, idSpeciality: idSpeciality }
						});
					});

					return Promise.all(questionPromises);
				});
			default:
				return [];
		}
	});

	Promise.all(promiseTasks).then(function () {
		$(document).find('.bookshelf_wrapper').hide();
		$(document).find('.hideBehind').hide();
		$('#edit-container').css("opacity", "1");
		createSwal({
			title: 'Зміни збережено',
			type: 'success'
		}, function () {
			window.location.href = "/omu";
		});
	}).catch(function (err) {
		$(document).find('.bookshelf_wrapper').hide();
		$(document).find('.hideBehind').hide();
		$('#edit-container').css("opacity", "1");
		createSwal({
			title: 'Виникла помилка при збереженні питань',
			type: 'error'
		});
		console.log(err);
	});
};

module.exports = saveEnglishTasks;

/***/ })

/******/ });