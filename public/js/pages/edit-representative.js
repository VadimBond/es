/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 91);
/******/ })
/************************************************************************/
/******/ ({

/***/ 2:
/***/ (function(module, exports) {

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var createSwal = function createSwal(params, callback) {
	var time = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 3000;

	swal(_extends({}, params, {
		closeOnClickOutside: true,
		allowOutsideClick: true
	}), callback);

	setTimeout(function () {
		return swal.close;
	}, time);
};

module.exports = {
	createSwal: createSwal
};

/***/ }),

/***/ 91:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(92);


/***/ }),

/***/ 92:
/***/ (function(module, exports, __webpack_require__) {

var createSwal = __webpack_require__(2).createSwal;

window.onload = function () {
	$(".toggle-password").click(function () {

		$(this).toggleClass("fa-eye fa-eye-slash");
		var input = $($(this).attr("toggle"));
		if (input.attr("type") == "password") {
			input.attr("type", "text");
		} else {
			input.attr("type", "password");
		}
	});

	var saveBtn = $('#save');

	var id = saveBtn.attr('data-id');

	saveBtn.on('click', function (e) {
		return save_representative(e, id);
	});

	$('#roleVal').on('change', roleChange);
};

function roleChange() {
	$("#hidden").hide();
	$("#dep").val('');
	if ($("#roleVal").val() == 3 || $("#roleVal").val() == 5) {
		$("#hidden").show();
	}
}

function save_representative(e, id) {

	var firstName = $("#firstName").val();
	var secondName = $("#secondName").val();
	var email = $("#email").val();
	var originEmail = $("#originEmail").text();
	var role = $("#roleVal").val();
	var pass = $("#pass").val();
	var confirm = $("#confirm").val();
	var dep = "";
	var haveSpec = 0;

	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var data = {
		'firstName': firstName,
		'secondName': secondName,
		'email': email,
		'originEmail': originEmail,
		'role': role,
		'dep': dep,
		'id': id,
		'pass': pass
	};

	if (firstName == "" || secondName == "" || email == "" || role == "") {
		createSwal({
			title: "Заповніть поля",
			type: 'error'
		});
	} else if (pass != confirm) {
		createSwal({
			title: "Поля пароль та підтвердження пароля не співпадаюсть",
			type: 'error'
		});
	} else if (pass === "" || pass.length < 6) {
		createSwal({
			title: "Довжна пароля має бути більше 6 символів",
			type: 'error'
		});
	} else if (!re.test(email)) {
		createSwal({
			title: "Невірний емайл",
			type: 'error'
		});
	} else {
		if (role == 3) {
			dep = $("#dep").val();
			data['dep'] = dep;
			$.ajax({
				method: 'POST',
				url: '/get_specialties',
				success: function success(response) {
					response.forEach(function (speciality) {
						if (speciality.id_cafedres == dep) {
							haveSpec = 1;
						}
					});
					if (!dep) {
						createSwal({
							title: "Оберіть кафедру",
							type: 'error'
						});
					} else if (haveSpec == 1) {

						if (id == 0) {
							ajaxUser('/add_representative', data);
						} else {
							ajaxUser('/update_representative', data);
						}
					} else {
						createSwal({
							title: "Не можна прив\'язати користувача до кафедри, у якої немає спеціальностей",
							type: 'error'
						});
					}
				},
				error: function error(_error) {
					console.log(_error);
				}
			});
		} else {

			if (id == 0) {
				ajaxUser('/add_representative', data);
			} else {
				ajaxUser('/update_representative', data);
			}
		}
	}
}

function ajaxUser(url, data) {
	$('#save').prop('disabled', true);

	$.ajax({
		method: 'POST',
		url: url,
		data: {
			'id': data['id'],
			'firstName': data['firstName'],
			'secondName': data['secondName'],
			'email': data['email'],
			'lastEmail': url == '/update_representative' ? data['email'] === data['originEmail'] ? null : "not null" : "1",
			'role': data['role'],
			'dep': data['dep'],
			'pass': data['pass']
		},
		success: function success() {
			swal({
				title: url == '/update_representative' ? "Користувач був успішно оновлений" : 'Користувач був успішно створений',
				type: "success"
			}, function () {
				$('#save').prop('disabled', false);

				window.location.href = "/omu";
			});
		},
		error: function error(_error2) {
			$('#save').prop('disabled', false);

			console.log(_error2);
			swal({
				title: url == '/update_representative' ? 'Помилка при збереженні користувача' : 'Емайл вже існує',
				type: 'error'
			});
		}
	});
}

/***/ })

/******/ });