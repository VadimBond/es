/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 93);
/******/ })
/************************************************************************/
/******/ ({

/***/ 93:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(94);


/***/ }),

/***/ 94:
/***/ (function(module, exports) {

window.onload = function () {
	var saveBtn = $('#saving .depart .save-btn');
	var id = saveBtn.attr('data-id');
	saveBtn.off('click');
	saveBtn.on('click', function (e) {
		return save_department(e, id);
	});

	$(document).ready(function () {
		$("#name").blur(function () {
			validate_departments_forms(this, 190, true);
		});

		$("#address").blur(function () {
			validate_departments_forms(this, 500, true);
		});

		$("#phone").blur(function () {
			validate_departments_forms(this, 20, true);
		});

		$("#site").blur(function () {
			validate_departments_forms(this, 300, false);
		});
	});

	function validate_departments_forms(id, valLength, req) {
		$(id).siblings().remove();
		var value = $(id).val();
		if (value.length > valLength) {
			$(id).after("<div class=\"validate-alert\">Поле не має перевищувати " + valLength + " символів</div>");
			$(id).css("border", "1px solid red");
		} else if (value.trim() == 0 && req) {
			$(id).after("<div class=\"validate-alert\">Поле не має бути пустим</div>");
			$(id).css("border", "1px solid red");
		} else {
			$(id).css("border", "1px solid green");
			return true;
		}

		return false;
	}

	function save_department(e, id) {
		var submitBtn = $(e.currentTarget);

		var name = validate_departments_forms("#name", 191, true);
		var address = validate_departments_forms("#address", 500, true);
		var phone = validate_departments_forms("#phone", 20, true);
		var site = validate_departments_forms("#site", 300, false);

		submitBtn.prop('disabled', true);

		if (name && address && phone && site) {
			submitBtn.off('click');

			if (id == 'undefined' || id == null || id == "") {
				ajaxDepartment('/add_department');
			} else {
				ajaxDepartment('/update_department');
			}
		} else {
			submitBtn.prop('disabled', false);
		}
	}

	function ajaxDepartment(url) {
		$.ajax({
			method: 'POST',
			url: url,
			data: {
				'id': id,
				'department_name': $("#name").val(),
				'address': $("#address").val(),
				'phone': $("#phone").val(),
				'site': $("#site").val()
			},
			success: function success(response) {
				$('#saving .save-btn').prop('disabled', false);
				if (response == 'ok') {
					swal({
						title: "Кафедра була успішно " + (url == '/update_department' ? "оновлена" : "створена"),
						type: "success"
					}, function () {
						window.location.href = "/omu";
					});
				} else {
					swal({
						title: "Кафедра не була " + (url == '/update_department' ? "оновлена" : "створена"),
						type: "error"
					}, function () {
						window.location.href = "/omu";
					});
				}
			},
			error: function error(_error) {
				$('#saving .save-btn').prop('disabled', false);

				console.log(_error);
			}
		});
	}
};

/***/ })

/******/ });